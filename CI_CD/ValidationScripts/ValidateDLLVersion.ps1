﻿#Validate DLL Version
#the database name should be listed in <sybase>sql.ini
Param(
  [string]$verParm="12.0.0.33",
  [string]$DllPath=""
)
$verAll=Get-ChildItem $DllPath | Select-Object -ExpandProperty VersionInfo
$ver= $verAll.FileVersion
$verMajor=$verParm.Split(".")[0] + "." + $verParm.Split(".")[1]

if($ver.StartsWith($verMajor)){   
   exit(0)
   }
else{
   echo("Invalid Version Found : expected: " + $verMajor, ", Found:" + $ver  )
   exit(1)
   }

