#check SQL Version
#the database name should be listed in <sybase>sql.ini
Param(
  [string]$verParm="12.0.0.33",
  [string]$dbScript="c:\temp\dbVersionCheck.sql"
)
 
$dbLog="c:\temp\dbVersionCheck.log"
isql -S CICDDB -U sa -P sybase -i $dbScript -o $dbLog
$verAll=Select-String -path $dbLog -Pattern "\d+.\d+.\d+.\d+"
if (([string]::IsNullOrEmpty($verAll))){
 echo("Invalid connection or error has occured check iSQL log file"  )
 exit(1)
}
$ver= $verAll.ToString().Split("{:}")[3]
echo("The database vesion found is " + $ver)
$verMajor=$verParm.Split(".")[0] + "." + $verParm.Split(".")[1]

if($ver.StartsWith($verMajor)){   
   exit(0)
   }
else{
   echo("Invalid Databse Version Found, expected: " + $verMajor, ", Found:" + $ver  )
   exit(1)
   }