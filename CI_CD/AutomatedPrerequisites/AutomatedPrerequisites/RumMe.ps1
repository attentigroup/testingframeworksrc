<# Parameters usage:
for Directory creation: [0] - [DirectoryFullPath]
for DB query: [0] - [ConnectionString] [1] - [Query]
 for File Config: [0] - [ConfigFilePath] [1] - [xPath] [2] - [KeyToSearchFor] [3] - [ValueToAssign]
 #>
 Param(
 [string]$connString = "Data Source='10.10.12.182';Port=2025;uid='sa';PWD='sybase';Database='ems';Min Pool Size=20;Max Pool Size=100;Pooling=True;ExtendedErrorInfo=TRUE;language=us_english;APP=WebApp;charset=iso_1",
 [string]$Executable = "AutomatedPrerequisites.exe"
 )

 $Queries = 
 "update ems_ws_column_data set show = 1 where user_id = -1 and column_id in(31, 343, 348, 370, 374, 377, 383, 391, 397)",
 "update ems_ws_security set res_id='V' where res_id in('OffenderLocation_PointToolTip_Label_PointStoredTime','OffendersList_List_GridHeader_OID','Monitor_EventGrid_Button_CustomFilterByTime')",
 "update ems_cms_parameters set value_code= 'Y' where param_name = 'is_kosher_enable'",
 "INSERT INTO ems_ws_security (res_id, sec_user, sec_status) VALUES ('OffenderLocation_PointToolTip_Label_PointStoredTime', 'SA', 'V')",
 "update ems_event SET is_monitor_beep = 'N' where is_monitor_beep = 'Y'"
 
 #Run queries
 Foreach($i in $Queries)
 {
 & .\$Executable ($connString) ($i)
 }

 # Edit config files
 & .\$Executable "C:\WebApp\Mapping.config" "appSettings/add" "GisMode" "DYNAMIC_WITH_TILES"
 & .\$Executable "C:\WebApp\Web.config" "configuration/appSettings/add" "UploadFilesPath" "\\10.10.12.183\UploadFiles"

   # Create config Folder
   & .\$Executable "C:\UploadFilesPath"

Read-Host -Prompt "[Press Enter to continue]"