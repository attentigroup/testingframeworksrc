﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPrerequisites
{
    public static class Print
    {
        private static List<string> vault = new List<string>();
        public static void Line(string line, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ForegroundColor = ConsoleColor.White;
            vault.Add(line);
        }
        
        public static void Line (string line)
        {
            Line(line, ConsoleColor.White);
        }

        public static void Dispose()
        {
            var dir = Environment.CurrentDirectory;
            Directory.CreateDirectory(dir);
            var filePath = Path.Combine(dir, "Log.txt");
            try
            {
                File.AppendAllLines(filePath, vault);
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Writing to the log file failed: {ex.ToString()}");
                Line($"Print to logger failed: {ex.ToString()}");
            }
        }
    }
}
