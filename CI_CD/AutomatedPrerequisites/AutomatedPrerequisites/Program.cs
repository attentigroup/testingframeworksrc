﻿using System;
using System.Collections.Generic;
using System.Data;
using Sybase.Data.AseClient;

namespace AutomatedPrerequisites
{
    public class Program
    {
        static void Main(string[] args)
        {
            Print.Line($"{Environment.NewLine}");
            Print.Line($"{DateTime.Now}(c) Automated prerequesites app", ConsoleColor.Cyan);
            Print.Line("===================================", ConsoleColor.Cyan);
            if (args != null && args.Length > 0)
            {
                try
                {
                    new BL().CreateActionItem(args);
                }
                catch (Exception ex)
                {
                    Print.Line(ex.ToString(), ConsoleColor.Red);
                }
                finally
                {
                    Print.Dispose();
                }
            }
            else
                Print.Line("You must add args to use this app", ConsoleColor.Red);
        }
    }

    public class BL
    {
        internal void CreateActionItem(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    DirectoryAction(args);

                    break;
                case 2:
                    DBAction(args);
                    break;

                case 4:
                    WebAction(args);
                    break;

                default:
                    Print.Line("The number of arguments is not compatible with any procedure", ConsoleColor.Red);
                    break;
            }
        }

        private void DBAction(string[] args)
        {
            new DBConnect(args[0], args[1]).Run();
        }

        private void WebAction(string[] args)
        {
            var bl = new XMLEditor();
            try

            {
                bl.EditXmlDoc(args[0], args[1], args[2], args[3]);

                Print.Line($"Changed value for key {args[2]} to {args[3]} in file {args[0]}", ConsoleColor.Gray);
                Print.Line("***  Passed without exceptions ***", ConsoleColor.Green);

            }
            catch (Exception ex)
            {
                Print.Line($"!!! {ex.ToString()}", ConsoleColor.Red);
            }
        }

        private void DirectoryAction(string[] args)
        {
            new DirectoryServices().CreateDir(args[0]);
        }
    }
}


