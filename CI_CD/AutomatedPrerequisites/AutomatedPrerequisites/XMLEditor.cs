﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AutomatedPrerequisites
{
    class XMLEditor
    {
        public void EditXmlDoc(string filePath, string xPath, string keyAttribute, string newValue)
        {

            // instantiate XmlDocument and load XML from file
            XmlDocument doc = new XmlDocument();

            doc.Load(filePath);

            // get a list of nodes
            XmlNodeList aNodes = doc.SelectNodes(@xPath);

            XmlNode selectedaNode = null;
            // loop through all aNodes
            foreach (XmlNode aNode in aNodes)
            {
                // grab the attribute
                XmlAttribute elmAttribute = aNode.Attributes["key"];

                // check if that attribute even exists...
                if (elmAttribute?.InnerText == keyAttribute)
                {
                    //elmAttribute.Attributes["value"].Value = newValue;
                    aNode.Attributes["value"].Value = newValue;
                    selectedaNode = aNode;
                    break;
                }
            }
            if (selectedaNode.Attributes["value"].Value == newValue)
                Print.Line($" the value of key Attribute {keyAttribute} was changed succssesfully to {newValue}", ConsoleColor.Green);
            else
                Print.Line("Changing value failed!", ConsoleColor.Red);

            // save the XmlDocument back to disk
            try
            {
                doc.Save(filePath);
                Print.Line($"{filePath} saved successfully", ConsoleColor.Green);
            }
            catch
            {
                Print.Line($"Failed to save {filePath}", ConsoleColor.Red);
            }

        }
    }
}
