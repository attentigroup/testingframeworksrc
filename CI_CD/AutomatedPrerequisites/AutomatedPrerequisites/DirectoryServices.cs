﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPrerequisites
{
    public class DirectoryServices
    {
        public void CreateDir(string directoryFullPath)
        {
            if (directoryFullPath.Trim() != "")
            {
                try
                {
                    Directory.CreateDirectory(directoryFullPath);
                    Print.Line($"Created folder in path {directoryFullPath}", ConsoleColor.Green);
                }
                catch (Exception ex)
                {
                    Print.Line($"Folder creation of {directoryFullPath} failed + {ex}", ConsoleColor.Red);
                }
            }
        }
    }
}
