﻿using Sybase.Data.AseClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPrerequisites
{
    public class DBConnect
    {
        string _conString;// "Data Source='10.10.12.182';Port=2025;uid='sa';PWD='sybase';Database='ems';Min Pool Size=20;Max Pool Size=100;Pooling=True;ExtendedErrorInfo=TRUE;language=us_english;APP=WebApp;charset=iso_1";
        string _commandText;
        public DBConnect(string constring, string commandText)
        {
            _conString = constring;
            _commandText = commandText.Replace("\r","").Replace("\n","");
            Print.Line($"Connection string: {constring}", ConsoleColor.Gray);
            Print.Line($"Executing: {_commandText}", ConsoleColor.Gray);
        }

        internal void Run()
        {
            using (var connection = CreateConnection())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Print.Line($"DB connection faile: {ex}", ConsoleColor.Red);
                }

                using (var command = connection.CreateCommand() as AseCommand)
                {
                    command.CommandText = _commandText;
                    var firstCommand = _commandText.Split(' ')[0].ToLower();
                    firstCommand = (firstCommand == "update" || firstCommand == "insert" || firstCommand == "delete") ? "update" : firstCommand;
                    switch (firstCommand.ToLower())
                    {
                        case "update":
                            RunAddUpdateDeleteCommand(command);
                            break;

                        case "select":
                            RunGetCommand(command);
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        private void RunAddUpdateDeleteCommand(AseCommand command)
        {
            int recordsAffected = 0;
            try
            {
                recordsAffected = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Print.Line($"Command execution failed: {ex.ToString()}", ConsoleColor.Red);
            }

            if (recordsAffected <= 0)
                Print.Line("No records affected!!!", ConsoleColor.Yellow);
            else
                Print.Line($"(OK) {recordsAffected} records affected", ConsoleColor.Green);

        }

        private void RunGetCommand(AseCommand command)
        {
            AseDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Print.Line(reader.GetString(0), ConsoleColor.Gray);
            }
        }

        public IDbConnection CreateConnection()
        {
            var conn = new AseConnection(_conString);

            return conn;
        }
    }
}
