Param(
	[string]$serverName = "10.10.12.111",
	[string]$commBoxName = "10.10.12.111",
	[string]$commBoxPort = "8080",
	[string]$dbHostName = "10.10.10.100"
)


[XML]$xml = Get-Content "C:\Releases\Tests\bin\Release\TestingFramework.dll.config"

$nodeServer = $xml.SelectSingleNode("/configuration/appSettings/add[@key='SvcUri']")
$nodeCommbox = $xml.SelectSingleNode("/configuration/appSettings/add[@key='GpsDeviceServerIP']")
$nodesCommboxPort = $xml.SelectSingleNode("/configuration/appSettings/add[@key='GpsDeviceServerPort']")
$nodeDbHostName = $xml.SelectSingleNode("/configuration/appSettings/add[@key='DB_HostName']")

$nodeServer.Attributes['value'].value = "https://" + $serverName + ":1443"
$nodeCommbox.Attributes['value'].value = $commBoxName
$nodesCommboxPort.Attributes['value'].value = $commBoxPort
$nodeDbHostName.Attributes['value'].value = $dbHostName


$xml.save("C:\Releases\Tests\bin\Release\TestingFramework.dll.config")
