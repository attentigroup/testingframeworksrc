﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Categories
{
    public class CategoriesNames
    {
        public const string TestingFrameworkUntiTests = "TestingFrameworkUnitTests";

        #region DV

        public const string DvBasicFlow = "DvBasicFlow";

        #endregion DV

        #region BI
        public const string BI_BiApiSanity = "BI_API_Sanity_Tests";
        public const string BI_HandlingEvents = "BI_HandlingEvents";
        public const string BI_UsersTests = "BI_UsersTests";



        #endregion BI

        #region Mobile
        public const string MobileWebService = "Mws_BasicFlow";
        #endregion Mobile

        #region API
        public const string Equipment_Service = "Equipment_Service";
        public const string Equipment_Service_Load = "Equipment_Service_Load";
        public const string Offenders_Service = "Offenders_Service";
        public const string Offenders_Service_Load = "Offenders_Service_Load";
        public const string Zones_Service = "Zones_Service";
        public const string Schedule_Service = "Schedule_Service";
        public const string ProgramActions_Service = "ProgramActions_Service";
        public const string ProgramActions_Service_Regression = "ProgramActions_Service_Regression";
        public const string Configuration_Service = "Configuration_Service";
        public const string Configuration_Regression = "Configuration_Regression";
        public const string Events_Service = "Events_Service";
        public const string Trails_Service = "Trails_Service";
        public const string Trails_Service_Load = "Trails_Service_Load";
        public const string Offenders_Regression = "Offenders_Regression";
        public const string Equipment_Regression = "Equipment_Regression";
        public const string Setup_Offenders = "Setup_Offenders";
        public const string Zones_Regression = "Zones_Regression";
        public const string Schedule_Regression = "Schedule_Regression";
        public const string Under_Construction = "Under_Construction";
        public const string Events_Regression = "Events_Regression";
        public const string Trails_Regression = "Trails_Regression";
        public const string MaskingPrivateData = "MaskingPrivateData";
        #endregion API

        #region Devices
        public const string Devices_Tests = "Devices_Tests";
        public const string Devices_Network_Issues_Tests = "Devices_Network_Issues_Tests";
        public const string Devices_Tests_Load = "Devices_Tests_Load";
        public const string Real_Devices_Tests_Load = "Real_Devices_Tests_Load";
        #endregion

        #region Web
        public const string Web_Sanity = "Web_Sanity";
        public const string Web_Offenders_Sanity = "Web_Offenders_Sanity";
        public const string Web_ImportEquipmentAddOffender_Sanity = "Web_ImportEquipmentAddOffender_Sanity";
        public const string Web_SecurityConfiguration = "Web_SecurityConfiguration";
        public const string Web_Monitor_Sanity = "Web_Monitor_Sanity";
        public const string Web_ZonesAndSchedule_Sanity = "Web_ZonesAndSchedule_Sanity";
        public const string Web_CustomFields_Sanity = "Web_CustomFields_Sanity";
        public const string Web_UpdateOffendersProgram_Sanity = "Web_UpdateOffendersProgram_Sanity";
        public const string Web_UpdateConfiguration_Sanity = "Web_UpdateConfiguration_Sanity";
        #endregion

        #region Environment preparation

        public const string EnvironmentPreparation_Users_Tests = "EnvironmentPreparation_Users_Tests";
        public const string EnvironmentPreparation_Offenders_Tests = "EnvironmentPreparation_Offenders_Tests";

        #endregion Environment preparation



    }

}
