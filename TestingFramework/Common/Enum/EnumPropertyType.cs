﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enum
{
    /// <summary>
    /// the type of the property. used for the data genration and passing properties between blocks
    /// </summary>
    public enum EnumPropertyType
    {
        /// <summary>
        /// default property type value
        /// </summary>
        None,

        /// <summary>
        /// for testing of data generators
        /// </summary>
        ControlledDataGenerator,

        /// <summary>
        /// property type string
        /// </summary>
        String,
        
        /// <summary>
        /// property type int
        /// </summary>
        Int,
        
        /// <summary>
        /// property type Nullable int
        /// </summary>
        NullableInt32,
        
        /// <summary>
        /// property type double
        /// </summary>
        Double,
        
        /// <summary>
        /// property type bool
        /// </summary>
        Bool,
        
        /// <summary>
        /// property type GUID
        /// </summary>
        GUID,

        /// <summary>
        /// generate random bytes in buffer
        /// </summary>
        Bytes,

        /// <summary>
        /// Random First Name (String)
        /// </summary>
        FirstName,

        #region Equipment entities

        /// <summary>
        /// generate default EntMsgGetEquipmentListRequest class
        /// </summary>
        EntMsgGetEquipmentListRequest,
        
        /// <summary>
        /// property from type manufacturer - Maximum length of Manufacturer is 10 characters
        /// </summary>
        Manufacturer,

        /// <summary>
        /// Unique ID of the mobile device.
        /// </summary>
        MobileID,

        /// <summary>
        /// EquipmentSerialNumber for TwoPiece Generation 6
        /// </summary>
        EquipmentSerialNumber_TwoPiece_Generation_6,

        /// <summary>
        /// EquipmentSerialNumber for TwoPiece Generation 4
        /// </summary>
        EquipmentSerialNumber_TwoPiece_Generation_4,

        /// <summary>
        /// EquipmentSerialNumber for TwoPiece Generation 06
        /// </summary>
        EquipmentSerialNumber_TwoPiece_Generation_0_6,

        /// <summary>
        /// EquipmentSerialNumber for OnePiece Generation 0
        /// </summary>
        EquipmentSerialNumber_OnePiece_Generation_0,

        /// <summary>
        /// EquipmentSerialNumber for OnePiece Generation 2
        /// </summary>
        EquipmentSerialNumber_OnePiece_Generation_2,

        /// <summary>
        /// EquipmentSerialNumber for OnePiece Generation 3.9
        /// </summary>
        EquipmentSerialNumber_OnePiece_Generation_3_9,

        /// <summary>
        /// EquipmentSerialNumber for OnePiece Generation 4i
        /// </summary>
        EquipmentSerialNumber_OnePiece_Generation_4_i,

        /// <summary>
        /// EquipmentSerialNumber for OnePiece Generation 7
        /// </summary>
        EquipmentSerialNumber_TwoPiece_Generation_7,

        /// <summary>
        /// EquipmentSerialNumber for BTX
        /// </summary>
        EquipmentSerialNumber_BTX,

        /// <summary>
        /// EquipmentSerialNumber for Beacon_Curfew_Unit
        /// </summary>
        EquipmentSerialNumber_Beacon_Curfew_Unit,

        /// <summary>
        /// EquipmentSerialNumber for Transmitter_DV
        /// </summary>
        EquipmentSerialNumber_Transmitter_DV,

        /// <summary>
        /// EquipmentSerialNumber for RF_Curfew_Dual_E4
        /// </summary>
        EquipmentSerialNumber_RF_Curfew_Dual_E4,

        /// <summary>
        /// EquipmentSerialNumber for RF_Curfew_Cell_E3
        /// </summary>
        EquipmentSerialNumber_RF_Curfew_Cell_E3,

        /// <summary>
        /// EquipmentSerialNumber for Transmitter_860
        /// </summary>
        EquipmentSerialNumber_Transmitter_860,

        /// <summary>
        /// EquipmentSerialNumber for Transmitter_890 Protocol_6
        /// </summary>
        EquipmentSerialNumber_Transmitter_890_P6,

        /// <summary>
        /// EquipmentSerialNumber for Transmitter_890 Protocol_2 and 0
        /// </summary>
        EquipmentSerialNumber_Transmitter_890_P2_P0,

        /// <summary>
        /// EquipmentSerialNumber for Transmitter_Triple_Tamper
        /// </summary>
        EquipmentSerialNumber_Transmitter_Triple_Tamper,

        #endregion Equipment entities

        /// <summary>
        /// random generator for agency id
        /// </summary>
        AgencyId,

        /// <summary>
        /// mobile app version
        /// </summary>
        AppVersion,

        #region Offenders Entities
        /// <summary>
        /// offender ref id - max 12 charecters
        /// </summary>
        OffenderRefID,

        /// <summary>
        /// Offender program start time
        /// </summary>
        ProgramStart,

        /// <summary>
        /// Offender program end time
        /// </summary>
        ProgramEnd,

        /// <summary>
        /// Offender gender - 12.0 version
        /// </summary>
        Gender,

        /// <summary>
        /// Offender gender - 3.10 version
        /// </summary>
        Gender_1,

        /// <summary>
        /// Offender gender - 3.9 version
        /// </summary>
        Gender_2,

        /// <summary>
        /// Offender title (Mr./Ms.) - 12.0 version
        /// </summary>
        Title,

        /// <summary>
        /// Offender title (Mr./Ms.) - 3.10 version
        /// </summary>
        Title_1,

        /// <summary>
        /// Offender title (Mr./Ms.) - 3.9 version
        /// </summary>
        Title_2,

        /// <summary>
        /// Offender phone number
        /// </summary>
        ContactPhoneNumber,

        /// <summary>
        /// Offender date of birth
        /// </summary>
        DateOfBirth,

        /// <summary>
        /// Offender last name
        /// </summary>
        LastName,

        /// <summary>
        /// Offender prefix phone
        /// </summary>
        ContactPrefixPhone,

        /// <summary>
        /// UpdateOffenderRequest - Offender first name
        /// </summary>
        UpdatedFirstName,

        /// <summary>
        /// Offender court order number
        /// </summary>
        CourtOrderNumber,

        /// <summary>
        /// Offender middle name
        /// </summary>
        MiddleName,

        /// <summary>
        /// Offender zip code
        /// </summary>
        ZipCode,

        /// <summary>
        /// AddContactRequest - relation
        /// </summary>
        ContactRelation,

        /// <summary>
        /// AddAddressRequest - description property
        /// </summary>
        AddressDescription,

        /// <summary>
        /// Offender social security
        /// </summary>
        SocialSecurity,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Grace time allowed to be inside an exclusion zone, or to be early/late in an inclusion zone, in minutes (optional)
        /// </summary>
        GraceTime_1,

        /// <summary>
        /// Offender department of correction
        /// </summary>
        DepartmentOfCorrection,

        /// <summary>
        /// Offender StateID
        /// </summary>
        StateId,

        /// <summary>
        /// Offender address phone number
        /// </summary>
        AddressPhoneNumber,

        /// <summary>
        /// Offender language
        /// </summary>
        Language,

        /// <summary>
        /// equipment voice/data phone number
        /// </summary>
        PhoneNumber,

        /// <summary>
        /// equipment voice/data prefix phone number
        /// </summary>
        PrefixPhoneNumber,

        /// <summary>
        /// equipment provider ID 
        /// </summary>
        ProviderID,
        /// <summary>
        /// UpdateEquipment - Failure date
        /// </summary>
        FailureDate,
        /// <summary>
        /// UpdateEquipment - sent to repair date
        /// </summary>
        SentToRepairDate,
        /// <summary>
        ///  UpdateEquipment - failure type
        /// </summary>
        FailureType,
        /// <summary>
        /// AddZone - Grace time
        /// </summary>
        GraceTime,
        /// <summary>
        /// AddZone - real radius
        /// </summary>
        RealRadius,
        /// <summary>
        /// AddZone - buffer radius
        /// </summary>
        BufferRadius,
        UserDefinedActionCode,
        ThirdPartyActionCode,
        PagerProviderID,
        PagerType,


        /// <summary>
        /// GPSDisappearTime used to generate GPS dissappear time for add\update
        /// </summary>
        GPSDisappearTime,

        /// <summary>
        /// GPSProximity used to generate  GPSProximity for add\update
        /// </summary>
        GPSProximity,

        /// <summary>
        /// LBSNoPositionTimeout used to generate  LBSNoPositionTimeout for add\update
        /// </summary>
        LBSNoPositionTimeout,

        /// <summary>
        /// LBSRequestTimeout used to generate  LBSRequestTimeout for add\update
        /// </summary>
        LBSRequestTimeout,

        /// <summary>
        /// NormalLoggingRate used to generate  NormalLoggingRate for add\update
        /// </summary>
        NormalLoggingRate,

        /// <summary>
        /// TimeBetweenUploads used to generate  TimeBetweenUploads for add\update
        /// </summary>
        TimeBetweenUploads,

        /// <summary>
        /// TimeBetweenBaseUnitUploads used to generate  TimeBetweenBaseUnitUploads for add\update
        /// </summary>
        TimeBetweenBaseUnitUploads,

        /// <summary>
        /// ViolationLoggingRate used to generate  ViolationLoggingRate for add\update
        /// </summary>
        ViolationLoggingRate,

        /// <summary>
        /// NumberOfRings used to generate  NumberOfRings for add\update
        /// </summary>
        NumberOfRings,

        /// <summary>
        /// NumberOfPhrases used to generate  NumberOfPhrases for add\update
        /// </summary>
        NumberOfPhrases,
        /// <summary>
        /// AddHandlingAction - email address
        /// </summary>
        EmailAddress,
        /// <summary>
        /// AddHandlingAction - schedule task type
        /// </summary>
        ScheduleTaskType,
        /// <summary>
        /// AddHandlingAction - schedule task time
        /// </summary>
        ScheduleTaskTime,
        /// <summary>
        /// AddHandlingAction - schedule task reminder time
        /// </summary>
        ReminderTime,
        /// <summary>
        /// AddHandlingAction - comment
        /// </summary>
        Comment,
        /// <summary>
        /// AddHandlingAction - pager code
        /// </summary>
        PagerCode,
        /// <summary>
        /// AddHandlingAction - number of tests (alcohol)
        /// </summary>
        NumberOfTests,
        /// <summary>
        /// Zone grace time
        /// </summary>
        ZoneGraceTime,
        /// <summary>
        /// Zone name
        /// </summary>
        ZoneName,
        /// <summary>
        /// TransmitterDisappearTime relate to configuration Tests
        /// </summary>
        TransmitterDisappearTime,
        /// <summary>
        /// MinimumSeconds relate to Trails Tests
        /// </summary>
        MinimumSeconds,

        #endregion Offenders Entities
    }
}
