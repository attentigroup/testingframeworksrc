﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enum
{
    /// <summary>
    /// the name of the property as the framework will use
    /// </summary>
    public enum EnumPropertyName
    {
        #region Testing framework enums

        /// <summary>
        /// the name of the block 
        /// </summary>
        BlockName,
        /// <summary>
        /// block description - will be shown in logs - importemnt when using several block from the same type
        /// </summary>
        BlockDescription,
        /// <summary>
        /// the block tun time - calc from start and end time properties
        /// </summary>
        BlockRunTime,
        /// <summary>
        /// block start time
        /// </summary>
        BlockStartTime,
        /// <summary>
        /// block end time
        /// </summary>
        BlockEndTime,
        /// <summary>
        /// the exception thrown by the block
        /// </summary>
        BlockException,
        /// <summary>
        /// used to test 'set property' method
        /// </summary>
        PropertyValue,
        /// <summary>
        /// used to test 'set property' method for property with midifier 'output'
        /// </summary>
        OutputPropertyValue,
        /// <summary>
        /// indext to set in the array. used to test set property method to array
        /// </summary>
        Index2Set,
        /// <summary>
        /// value to set in the array. used to test set property method to array
        /// </summary>
        Value2Set,
        /// <summary>
        /// the array of strings to set to. used to test set property method to array
        /// </summary>
        StringsArray,
        /// <summary>
        /// the list of strings to set to. used to test set property method to array
        /// </summary>
        StringsList,
        /// <summary>
        /// the size Length of list to set to. used to test set property method to array
        /// </summary>
        ListLength,
        /// <summary>
        /// deep property for tewsting assignment to deep properties
        /// </summary>
        DeepProperty,
        /// <summary>
        /// the sort direction of lists
        /// </summary>
        ListSortDirection,
        /// <summary>
        /// the limit of the items in list
        /// </summary>
        ListRequestLimit,

        #endregion Testing framework enums

        /// <summary>
        /// the number of seconds to sleep between blocks that must wait predefine value of seconds.
        /// </summary>
        SleepDurationSeconds,

        #region Equipment entities


        /// <summary>
        /// Add Equipment Request entity.used for IEquipment.AddEquipment method
        /// </summary>
        EntMsgAddEquipmentRequest,

        /// <summary>
        /// Add Equipment Response entity.used for IEquipment.AddEquipment method
        /// </summary>
        EntMsgAddEquipmentResponse,

        /// <summary>
        /// Delete Equipment Request entity.used for IEquipment.DeleteEquipment method
        /// </summary>
        EntMsgDeleteEquipmentRequest,

        /// <summary>
        /// Update Equipment Request entity.used for IEquipment.UpdateEquipment method
        /// </summary>
        EntMsgUpdateEquipmentRequest,

        /// <summary>
        /// Get Equipment List request. Used for IEquipment.GetEquipmentList method
        /// </summary>
        EntMsgGetEquipmentListRequest,

        /// <summary>
        /// Get Equipment List response. Used for IEquipment.GetEquipmentList method
        /// </summary>
        EntMsgGetEquipmentListResponse,

        /// <summary>
        /// Get Authorized Mobile List Request. Used for IEquipment.GetAuthorizedMobileList method
        /// </summary>
        EntMsgGetAuthorizedMobileListRequest,

        /// <summary>
        /// Get Authorized Mobile List Response. Used for IEquipment.GetAuthorizedMobileList method
        /// </summary>
        EntMsgGetAuthorizedMobileListResponse,

        /// <summary>
        /// Update Authorized Mobile Request. Used for IEquipment.UpdateAuthorizedMobile method
        /// </summary>
        EntMsgUpdateAuthorizedMobileRequest,

        /// <summary>
        /// Add Authorized Mobile Request. Used for IEquipment.AddAuthorizedMobile method
        /// </summary>
        EntMsgAddAuthorizedMobileRequest,

        /// <summary>
        /// Delete Authorized Mobile Request. Used for IEquipment.DeleteAuthorizedMobile method
        /// </summary>
        EntMsgDeleteAuthorizedMobileRequest,

        /// <summary>
        /// Get Cellular Data Request. Used for IEquipment.GetCellularData method
        /// </summary>
        EntMsgGetCellularDataRequest,

        /// <summary>
        /// Add Cellular Data Request. Used for IEquipment.AddCellularData method
        /// </summary>
        EntMsgAddCellularDataRequest,

        /// <summary>
        /// Add Cellular Data Response. Used for IEquipment.AddCellularData method
        /// </summary>
        EntMsgAddCellularDataResponse,

        /// <summary>
        /// Get Cellular Data Response. Used for IEquipment.GetCellularData method
        /// </summary>
        EntMsgGetCellularDataResponse,

        /// <summary>
        /// Update Cellular Data Request. Used for IEquipment.UpdateCellularData method
        /// </summary>
        EntMsgUpdateCellularDataRequest,

        /// <summary>
        /// Delete Cellular Data Request. Used for IEquipment.DeleteCellularData method
        /// </summary>
        EntMsgDeleteCellularDataRequest,

        /// <summary>
        /// Update scedule contain 3 type : insert / delete / edit.
        /// </summary>
        UpdateScheduleRequest,

        /// <summary>
        /// Receiver Serial Number
        /// </summary>
        ReceiverSN,

        /// <summary>
        /// Enum Equipment Model
        /// </summary>
        EnmEquipmentModel,

        /// <summary>
        /// the modem type of the equipment
        /// </summary>
        EnmModemType,

        /// <summary>
        /// agency id
        /// </summary>
        AgencyId,

        /// <summary>
        /// equipment serial number
        /// </summary>
        EquipmentSerialNumber,

        /// <summary>
        /// equipment id
        /// </summary>
        EquipmentID,

        /// <summary>
        /// equipment location
        /// </summary>
        EquipmentLocation,

        /// <summary>
        /// Date of failure
        /// </summary>
        FailureDate,

        /// <summary>
        /// Description of failure
        /// </summary>
        FailureDescription,

        /// <summary>
        /// Type of failure
        /// </summary>
        FailureType,

        /// <summary>
        /// Is Equipment Damaged
        /// </summary>
        IsDamaged,

        /// <summary>
        ///  Equipment Return From Repair Date
        /// </summary>
        ReturnFromRepairDate,

        /// <summary>
        ///  Equipment Sent To Repair Date
        /// </summary>
        SentToRepairDate,

        /// <summary>
        /// the description of equipment in add request
        /// </summary>
        EquipmentDescription,

        /// <summary>
        /// the Encryption-GSM type of equipment in add request
        /// </summary>
        EquipmentEncryptionGSM,

        /// <summary>
        /// the Encryption-RF type of equipment in add request
        /// </summary>
        EquipmentEncryptionRF,

        /// <summary>
        /// the Manufacturer of equipment
        /// </summary>
        Manufacturer,

        /// <summary>
        /// the ProtocolType of equipment
        /// </summary>
        EnmProtocolType,

        /// <summary>
        /// indicate if the equipment support encryption
        /// </summary>
        EquipmentSupportEncryption,

        ///<summary>
        /// Application ID, Indicates which application is relevant for the authorized mobile : 3 = Mobile Officer tools, 5 = Mobile Installer 
        /// </summary>
        ApplicationID,

        ///<summary>
        /// Unique ID of the mobile device. Generated by the device 
        /// </summary>
        MobileID,

        ///<summary>
        /// IsActive Indicates whether the mobile is approved to login (IsActive = true), or pending for approval (IsActive = false)
        /// </summary>
        IsActive,

        ///<summary>
        /// CellularNumber Contains cellular number of the mobile device (optional)
        /// </summary>
        CellularNumber,

        ///<summary>
        /// Model of the mobile device (optional)
        /// </summary>
        Model,

        ///<summary>
        /// Operating system of the mobile device (optional)
        /// </summary>
        OperatingSystem,

        ///<summary>
        /// Comment for the authoirized mobile (optional)
        /// </summary>
        Comment,

        ///<summary>
        ///GetCellularDataRequest
        /// Use to return the SIM data for a list of devices by ID (optional)
        /// </summary>
        DeviceIDList,

        ///<summary>
        ///GetCellularDataRequest
        /// Use to return the SIM data for a specific device by serial number (optional)
        /// </summary>
        SerialNumber,

        ///<summary>
        ///GetCellularDataRequest
        /// Return a specific SIM by ID 
        /// </summary>
        SIMID,

        ///<summary>
        /// UpdateCellularDataRequest and AddCellularDataRequest 
        /// Phone number for data (relevant only when using different numbers for voice/data) (optional)
        /// </summary>
        DataPhoneNumber,

        ///<summary>
        /// UpdateCellularDataRequest and AddCellularDataRequest 
        /// Prefix phone for data (relevant only when using different numbers for voice/data) (optional)
        /// </summary>
        DataPrefixPhone,

        ///<summary>
        ///UpdateCellularDataRequest
        /// SIM ID - unique identity of the SIM (mandatory)
        /// </summary>
        ID,

        ///<summary>
        /// UpdateCellularDataRequest and AddCellularDataRequest 
        /// Indicates whether this is the primary SIM used by the device (relevant when device supports dual SIM) 
        /// </summary>
        IsPrimarySIM,

        ///<summary>
        /// UpdateCellularDataRequest and AddCellularDataRequest 
        /// Communication provider ID (mandatory)
        /// </summary>
        ProviderID,

        ///<summary>
        /// UpdateCellularDataRequest and AddCellularDataRequest 
        /// Phone No. of Voice (mandatory)
        /// </summary>
        VoicePhoneNumber,

        ///<summary>
        /// UpdateCellularDataRequest and AddCellularDataRequest 
        /// Prefix phone of voice (optional)
        /// </summary>
        VoicePrefixPhone,

        ///<summary>
        /// AddCellularDataRequest 
        /// Device serial number (mandatory)
        /// </summary>
        DeviceSerialNumber,

        /// <summary>
        /// UpdateCellularDataRequest Version 3.10
        /// Phone No. of CSD (relevant only for cellular unit that uses CSD communication method) (mandatory)
        /// </summary>
        CSDPhoneNumber,

        /// <summary>
        /// UpdateCellularDataRequest Version 3.10
        /// Prefix phone of CSD (relevant only for cellular unit that uses CSD communication method) (optional)
        /// </summary>
        CSDPrefixPhone,

        /// <summary>
        /// UpdateCellularDataRequest Version 3.10
        /// Receiver serial number (mandatory)
        /// </summary>
        ReceiverSerialNumber,

        /// <summary>
        /// EntMsgGetCellularDataRequest Version 3.10
        /// Optional
        /// </summary>
        ReceiverIDList,
        /// <summary>
        /// used to pass array of models type
        /// </summary>
        EquipmentModelArray,

        /// <summary>
        /// used to use Equipment Type
        /// </summary>
        EquipmentType,

        /// <summary>
        /// list of equipoments
        /// </summary>
        EntEquipmentBaseList,

        /// <summary>
        /// the encoding that the device working with.
        /// </summary>
        Encoding,

        /// <summary>
        /// the device application version. for example "V5.12.9.2"
        /// </summary>
        DeviceApplication,

        #endregion Equipment entities

        #region Offenders entities

        /// <summary>
        /// Offender entity
        /// </summary>
        Offender,

        /// <summary>
        /// request data for GetOffenders method
        /// </summary>
        GetOffendersListRequest,

        /// <summary>
        /// response data for GetOffenders method
        /// </summary>
        GetOffendersResponse,

        /// <summary>
        /// request for add offender method
        /// </summary>
        AddOffenderRequest,

        /// <summary>
        /// response for add offender method
        /// </summary>
        AddOffenderResponse,

        /// <summary>
        /// request for delete offender method
        /// </summary>
        DeleteOffenderRequest,

        /// <summary>
        /// request for add offender address method
        /// </summary>
        AddOffenderAddressRequest,

        /// <summary>
        /// Response for add offender address method
        /// </summary>
        AddOffenderAddressResponse,

        /// <summary>
        /// request for Delete offender address method
        /// </summary>
        DeleteOffenderAddressRequest,

        /// <summary>
        /// Response for Delete offender address method
        /// </summary>
        GetDeletedOffendersResponse,


        /// <summary>
        /// Response for Get offender Counters method
        /// </summary>
        GetOffendersCountersResponse,


        /// <summary>
        /// request for update offender method
        /// </summary>
        UpdateOffenderRequest,

        /// <summary>
        /// request for update offender address method
        /// </summary>
        UpdateOffenderAddressRequest,

        /// <summary>
        /// request for get deleted offenders method
        /// </summary>
        GetDeletedOffendersRequest,

        /// <summary>
        /// request to add offender contact
        /// </summary>
        AddOffenderContactRequest,

        /// <summary>
        /// Response to add offender contact
        /// </summary>
        AddOffenderContactResponse,

        /// <summary>
        /// Request to delete offender contact
        /// </summary>
        DeleteOffenderContactRequest,

        /// <summary>
        /// Request to update offender contact
        /// </summary>
        UpdateOffenderContactRequest,

        /// <summary>
        /// Request to add offender picture
        /// </summary>
        AddOffenderPictureRequest,

        /// <summary>
        /// Response to add offender picture
        /// </summary>
        AddOffenderPictureResponse,

        /// <summary>
        /// Request to get offender contact list
        /// </summary>
        GetOffenderContactsListRequest,

        /// <summary>
        /// Response to get offender contact list
        /// </summary>
        GetOffenderContactsListResponse,

        /// <summary>
        /// Request to delete offender picture
        /// </summary>
        DeleteOffenderPicture,

        /// <summary>
        /// Request to get offender's picture list ID's
        /// </summary>
        GetOffenderPictureIDListRequest,

        /// <summary>
        /// Response to get offender's picture list ID's
        /// </summary>
        GetOffenderPictureIDListResponse,

        /// <summary>
        /// Request to reallocate active offender
        /// </summary>
        ReallocateActiveOffenderRequest,

        /// <summary>
        /// Request to get current status for offender
        /// </summary>
        GetOffenderCurrentStatusRequest,

        /// <summary>
        /// Request to delete recurring time frame 
        /// </summary>
        DeleteRecurringTimeFrameRequest,

        /// <summary>
        /// Response to get current status for offender
        /// </summary>
        GetOffenderCurrentStatusResponse,

        /// <summary>
        /// enum for program status
        /// </summary>
        EnmProgramStatus,
        /// <summary>
        /// enum for program type
        /// </summary>
        EnmProgramType,

        /// <summary>
        /// the field that the list will return with
        /// </summary>
        OffendersSortOptionsSortField,

        /// <summary>
        /// AddOffender - Agency ID (mandatory) List of options can be found in Agency module, by function GetAgenciesList() 
        /// </summary>
        AgencyID,

        /// <summary>
        /// AddOffender - Indicates whether this RF program also allows voice tests (optional) (relevant only for RF programs) 
        /// </summary>
        AllowVoiceTests,

        /// <summary>
        /// AddOffender - City ID of offender's main address (mandatory) List of options can be found in Resources module, by function GetCitiesList( StateID = 'selected state' )  
        /// </summary>
        CityID,

        /// <summary>
        /// AddOffender - Phone No. for offender contact (optional - not relevant for programs E3.5/E4)
        /// </summary>
        ContactPhoneNumber,

        /// <summary>
        /// AddOffender - Prefix phone for offender contact (optional - not relevant for programs E3.5/E4)
        /// </summary>
        ContactPrefixPhone,

        /// <summary>
        /// AddOffender - Court order number (optional)
        /// </summary>
        CourtOrderNumber,

        /// <summary>
        /// AddOffender - Date of birth (optional)
        /// </summary>
        DateOfBirth,

        /// <summary>
        /// AddOffender - Department of correction (optional)
        /// </summary>
        DepartmentOfCorrection,

        /// <summary>
        /// AddOffender - First name (mandatory)
        /// </summary>
        FirstName,

        /// <summary>
        /// AddOffender - Gender (mandatory)
        /// </summary>
        Gender,

        /// <summary>
        /// AddOffender - Curfew unit serial No. (optional - relevant only for GPS program with curfew unit) 
        /// </summary>
        HomeUnit,

        /// <summary>
        /// AddOffender - Indicates whether the new offender record is representing a second location of another offender record Relevant only for RF programs 
        /// </summary>
        IsSecondaryLocation,

        /// <summary>
        /// AddOffender - Dial out prefix for landline (optional - relevant only for unit that uses landline communication method) 
        /// </summary>
        LandlineDialOutPrefix,

        /// <summary>
        /// AddOffender - Phone No. for landline (optional - relevant only for unit that uses landline communication method) 
        /// </summary>
        LandlinePhoneNumber,

        /// <summary>
        /// AddOffender - Prefix phone for landline (optional - relevant only for unit that uses landline communication method) 
        /// </summary>
        LandlinePrefixPhone,

        /// <summary>
        /// AddOffender - Offender language (mandatory) List of options can be found in Resources module, by function GetLookup( LookupName = "LANGUAGE" ) 
        /// </summary>
        Language,

        /// <summary>
        /// AddOffender - Last name (mandatory)
        /// </summary>
        LastName,

        /// <summary>
        /// AddOffender - Middle name (optional)
        /// </summary>
        MiddleName,

        /// <summary>
        /// AddOffender - Officer ID (mandatory) List of options can be found in Users module, by function GetOfficersList( AgencyID = 'selected agency' ) 
        /// </summary>
        OfficerID,

        /// <summary>
        /// AddOffender - Program concept (optional) (relevant only for program type 2 Piece GPS) 
        /// </summary>
        ProgramConcept,

        /// <summary>
        /// AddOffender - Program end time (mandatory)
        /// </summary>
        ProgramEnd,

        /// <summary>
        /// AddOffender - Program start time (mandatory)
        /// </summary>
        ProgramStart,

        /// <summary>
        /// AddOffender - Program type (mandatory)
        /// </summary>
        ProgramType,

        /// <summary>
        /// AddOffender - Receiver serial No. (optional)
        /// </summary>
        Receiver,

        /// <summary>
        /// AddOffender - Offender ID, chosen by user (mandatory)
        /// </summary>
        RefID,

        /// <summary>
        /// AddOffender - Related offender ID (optional) (relevant only for Domestic Violence program) 
        /// </summary>
        RelatedOffenderID,

        /// <summary>
        /// AddOffender - Social security No. (optional)
        /// </summary>
        SocialSecurity,

        /// <summary>
        /// AddOffender - State ID of offender's main address (mandatory) List of options can be found in Resources module, by function GetStatesList( CountryID = null ) 
        /// </summary>
        StateID,

        /// <summary>
        /// AddOffender - Street and house No. of offender's main address (mandatory)
        /// </summary>
        Street,

        /// <summary>
        /// AddOffender - Title (mandatory)
        /// </summary>
        Title,

        /// <summary>
        /// AddOffender - Transmitter serial No. (optional)
        /// </summary>
        Transmitter,

        /// <summary>
        /// AddOffender - Zip code of offender's main address (optional)
        /// </summary>
        ZipCode,

        /// <summary>
        /// GetOffendersList - Filter the offenders by End of Service code (optional) List of options can be found in Resources module, by function GetEOSOptions() 
        /// </summary>
        EndOfServiceCode,

        /// <summary>
        /// GetOffendersList - Return all offenders belonging to a specific group (optional) List of options can be found in Groups module, by function GetData( Group = null , OffenderID = null ) 
        /// </summary>
        GroupID,

        /// <summary>
        /// GetOffendersList - Indicates whether to include in the result a list of addresses and phones (in very long offenders list, retrieving all addresses/phones, may have an effect on performance) 
        /// </summary>
        IncludeAddressList,

        /// <summary>
        /// GetOffendersList - Return all offenders who have a higher timestamp than the parameter (optional) (use to find all offenders that were added or updated after this timestamp) 
        /// </summary>
        LastTimeStamp,

        /// <summary>
        /// GetDeletedOffenders- Last timeStamp (optional - use null to return all)
        /// </summary>
        LastTimestamp,


        /// <summary>
        /// GetOffendersList - Filter the offenders by Offender ID (optional)
        /// </summary>
        OffenderID,

        /// <summary>
        /// GetOffendersList - Filter the offenders by Offender RefID (optional)
        /// </summary>
        OffenderRefID,

        /// <summary>
        /// GetOffendersList - Return all offenders of program status same as program status parameter (optional)
        /// </summary>
        ProgramStatus,

        /// <summary>
        /// GetOffendersList - Filter the offenders by Social Security number (optional)
        /// </summary>
        SocialSecurityNumber,

        /// <summary>
        /// GetOffendersList - Sort order ascending or descending (optional)
        /// </summary>
        SortDirection,

        /// <summary>
        /// GetOffendersList - Field for order of the result (optional)
        /// </summary>
        SortField,

        /// <summary>
        /// GetOffendersList - Indicates whether to include in the result all program status parameters 
        /// </summary>
        IncludeProgramStatus,

        /// <summary>
        /// GetOffendersList - Filter the offenders by phone number (receiver phone number for all programs or home phone for Voice program) (optional)
        /// </summary>
        ProgramPhoneNumber,

        /// <summary>
        /// DeleteOffenderRequest - Timestamp of the last time offender's data was updated (mandatory) Current timestamp can be retrieved from GetOffenders function 
        /// </summary>
        Timestamp,

        /// <summary>
        /// GetOffenderRequest - Use to convert int(long) to EntNumericQueryParameter
        /// </summary>
        OffenderIDQueryParameter,

        /// <summary>
        /// GetOffenderRequest Operator inside OfficerIDQueryParameter
        /// </summary>
        OfficerIDNumericOperator,

        /// <summary>
        /// GetOffenderRequest the value of the Officer ID inside OffenderIDQueryParameter
        /// </summary>
        OfficerIDValue,

        /// <summary>
        /// GetOffenderRequest - Use to convert int(long) to EntNumericQueryParameter
        /// </summary>
        AgencyIDQueryParameter,

        /// <summary>
        /// GetOffenderRequest Operator inside AgencyIDQueryParameter
        /// </summary>
        AgencyIDNumericOperator,

        /// <summary>
        ///  GetOffenderRequest the value of the Agency ID inside AgencyIDQueryParameter
        /// </summary>
        AgencyIDValue,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        EndOfServiceCodeQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        FirstNameQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        LastNameQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        OffenderRefIDQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert int(long) to EntNumericQueryParameter
        /// </summary>
        OfficerIDQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert int(long) to EntNumericQueryParameter
        /// </summary>
        ReceiverQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        SocialSecurityNumberQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        TransmitterQueryParameter,

        /// <summary>
        /// GetOffenderRequest - Use to convert string to EntStringQueryParameter
        /// </summary>
        ProgramPhoneNumberQueryParameter,

        /// <summary>
        /// AddOffenderAddressRequest - Address description (optional)
        /// </summary>
        Description,

        /// <summary>
        /// AddOffenderAddressRequest - list of phones related to the address (optional)
        /// </summary>
        Phones,

        /// <summary>
        /// AddOffenderAddressRequest - ID of restriction zone that was defined around this address (optional)
        /// </summary>
        ZoneID,

        /// <summary>
        /// DeleteOffenderAddressRequest - Address ID (mandatory)
        /// </summary>
        AddressID,

        /// <summary>
        /// UpdateOffenderRequest - Offender photo ID (optional) List of photo IDs can be found by function GetOffenderPictureIDList() 
        /// </summary>
        PhotoID,

        /// <summary>
        /// Offnders Ids in a list
        /// </summary>
        OffendersIdsList,

        /// <summary>
        /// Officers Ids in a list
        /// </summary>
        OfficersIdsList,

        /// <summary>
        /// AddOffenderContactRequest - Phone number 
        /// </summary>
        Phone,

        /// <summary>
        /// AddOffenderContactRequest - PrefixPhone number 
        /// </summary>
        PrefixPhone,

        /// <summary>
        /// AddOffenderContactRequest - Priority of the contact 
        /// </summary>
        Priority,

        /// <summary>
        /// AddOffenderContactRequest - Relation of the contact 
        /// </summary>
        Relation,

        /// <summary>
        /// DeleteOffenderContactRequest - Contact ID
        /// </summary>
        ContactID,

        /// <summary>
        /// AddOffenderPictureRequest - Photo in byte[]
        /// </summary>
        Photo,

        /// <summary>
        /// DeleteOffenderPicture - delete picture by picture id (same as PhotoID)
        /// </summary>
        PictureID,

        /// <summary>
        /// Agencies Id in a list
        /// </summary>
        AgenciesIdList,

        /// <summary>
        /// ReallocateActiveOffender - new agency for the offender
        /// </summary>
        NewAgencyID,

        /// <summary>
        /// ReallocateActiveOffender - new officer for the offender
        /// </summary>
        NewOfficerID,

        /// <summary>
        /// ReallocateActiveOffender - Reason for the reallocation of the offender
        /// </summary>
        Reason,

        /// <summary>
        /// AddContactRequest - contact first name
        /// </summary>
        ContactFirstName,

        /// <summary>
        /// AddContactRequest - contact first name
        /// </summary>
        ContactLastName,

        /// <summary>
        /// AddOffenderRequest - phone property 
        /// </summary>
        ContactPhone,

        /// <summary>
        /// AddContactRequest - contact middle name
        /// </summary>
        ContactMiddleName,

        /// <summary>
        /// AddAddressRequest - zip code property for new address
        /// </summary>
        AddressZipCode,

        /// <summary>
        /// AddAddressRequest - street property for new address
        /// </summary>
        AddressStreet,

        /// <summary>
        /// amountOfOffenders for UI tests
        /// </summary>
        amountOfOffenders,

        /// <summary>
        /// Current Status of Table 
        /// </summary>
        CurrentStatusTableRow,

        #endregion Offenders

        #region Zones entites

        /// <summary>
        /// Request Add Circular Zone
        /// </summary>
        AddCircularZoneRequest,

        /// <summary>
        /// Request Add Polygon Zone
        /// </summary>
        AddPolygonZoneRequest,

        /// <summary>
        /// Response for Add new Zone
        /// </summary>
        AddZoneResponse,

        /// <summary>
        /// Request Delete Zone
        /// </summary>
        DeleteZoneRequest,

        /// <summary>
        /// Request Get Zones By Entity ID
        /// </summary>
        GetZonesByEntityIDRequest,

        /// <summary>
        /// Response Get Zones By Entity ID
        /// </summary>
        GetZonesByEntityIDResponse,

        /// <summary>
        /// Request Update Circular Zone 
        /// </summary>
        UpdateCircularZoneRequest,

        /// <summary>
        /// Request Polygon Circular Zone 
        /// </summary>
        UpdatePolygonZoneRequest,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Base location point - used to define a specific location for the curfew unit within a geographic zone (optional) 
        /// (relevant only for GPS programs with RF unit, and IsCurfewZone flag equals true) 
        /// </summary>
        BLP,

        /// <summary>
        /// AddCircularZone - Radius (in meters) of warning buffer around the zone (mandatory - relevant only for exclusion zone)) possible values are between 10 to 65000 
        /// </summary>
        BufferRadius,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Entity ID (mandatory)
        /// Schedule Service AddTimeFrameRequest - Unique ID of offender / group (mandatory)
        /// </summary>
        EntityID,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Entity type 
        /// /// Schedule Service AddTimeFrameRequest - Entity type (mandatory)
        /// </summary>
        EntityType,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Indicates whether zone is used as inclusion zone during whole program range 
        /// </summary>
        FullSchedule,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Grace time allowed to be inside an exclusion zone, or to be early/late in an inclusion zone, in minutes (optional)
        /// </summary>
        GraceTime,


        /// <summary>
        /// AddCircularZone & AddPolygonZone - Indicates whether current zone is defined for curfew, and receiver should search for RF unit reception (relevant only for GPS programs with RF unit) 
        /// </summary>
        IsCurfewZone,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Zone limitation rule (inclusion / exclusion) 
        /// </summary>
        Limitation,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Zone name (mandatory)
        /// </summary>
        ZoneName,

        /// <summary>
        /// AddCircularZone & AddPolygonZone - Location of center point (Lat/Lon) (mandatory)
        /// </summary>
        Point,

        /// <summary>
        /// AddCircularZone - Zone radius in meters (mandatory) pssible values are between 10 to 65000 
        /// </summary>
        RealRadius,

        /// <summary>
        /// AddPolygonZone - Coordinates list of polygon vertices (mandatory minimum 3 points) 
        /// </summary>
        Points,

        /// <summary>
        /// GetZonesByEntityID - Download version No. (mandatory)
        /// Parameter could be the version number or -1/-2 as described below :
        ///-1 - Current version used by the device
        ///-2 - Planned version to be downloaded to the device in next download
        ///-3 - All zones from all versions
        /// </summary>
        Version,

        #endregion

        #region Schedule entities

        /// <summary>
        /// Request Add Time Frame
        /// </summary>
        AddTimeFrameRequest,

        /// <summary>
        /// Add Time Frame Response
        /// </summary>
        AddTimeFrameResponse,

        /// <summary>
        /// Delete Time Frame Request
        /// </summary>
        DeleteTimeFrameRequest,

        /// <summary>
        /// Get Schedule Request
        /// </summary>
        GetScheduleRequest,

        /// <summary>
        /// Repeat Schedule Request
        /// </summary>
        RepeatScheduleRequest,

        /// <summary>
        /// Get Schedule Response
        /// </summary>
        GetScheduleResponse,

        /// <summary>
        /// Update Time Frame Request
        /// </summary>
        UpdateTimeFrameRequest,

        /// <summary>
        /// AddTimeFrameRequest - Time frame end time (mandatory)
        /// </summary>
        EndTime,

        /// <summary>
        /// AddTimeFrameRequest - Indicates whether this time frame is repeated every week (mandatory - not relevant for alcohol or voice tests) 
        /// </summary>
        IsWeekly,

        /// <summary>
        /// AddTimeFrameRequest - Location ID (mandatory relevant only for GPS programs) List of options can be found by function GetScheduleParameters() 
        /// </summary>
        LocationID,

        /// <summary>
        /// AddTimeFrameRequest - Number of tests (mandatory - relevant only for alcohol or voice tests) 
        /// </summary>
        NumberOfTests,

        /// <summary>
        /// AddTimeFrameRequest - Time frame start time (mandatory)
        /// </summary>
        StartTime,

        /// <summary>
        /// AddTimeFrameRequest - Time frame type (mandatory)
        /// </summary>
        TimeFrameType,

        /// <summary>
        /// AddTimeFrameRequest - Voice test call type (mandatory - relevant only for voice test) 
        /// </summary>
        VoiceTestCallType,

        /// <summary>
        /// Delete Time Frame Request - Time frame ID (mandatory)
        /// </summary>
        TimeframeID,

        /// <summary>
        /// Time Frame ID List
        /// </summary>
        TimeframeIDsList,

        /// <summary>
        /// Repeat schedule Request - Date (mandatory)
        /// </summary>
        Date,

        /// <summary>
        /// Timestamp of the last time schedule data was updated (mandatory - relevant only for alcohol test program)
        /// </summary>
        AlcoholTestTimeStamp,

        /// <summary>
        /// Timestamp of the last time schedule data was updated (mandatory - relevant only for E4 program)
        /// </summary>
        E4TimeStamp,

        /// <summary>
        /// List of dates to skip weekly time frames, (optional - supported only for E4 program)
        /// </summary>
        SkipDates,

        /// <summary>
        /// Timestamp of the last time schedule data was updated (mandatory - relevant only for RF-E3 programs) 
        /// </summary>
        RFTimeStamp,

        /// <summary>
        /// List of time frames to add / update (optional - sending an empty list, will delete whole schedule)
        /// </summary>
        Timeframes,


        /// <summary>
        /// Update end time - time when to finish override the schedule (optional - defualt is program end time)
        /// </summary>
        UpdateEndTime,

        /// <summary>
        /// Update start time - time when to begin override the schedule (optional - defualt is program start time) 
        /// </summary>
        UpdateStartTime,


        /// <summary>
        /// Timestamp of the last time schedule data was updated (mandatory - relevant only for voice test program)
        /// </summary>
        VoiceTestTimeStamp,

        #endregion

        #region Mobile Web Service entities

        /// <summary>
        /// Entity Get Mobile Status request
        /// </summary>
        EntMsgMwsGetMobileStatusRequest,

        /// <summary>
        /// Entity validate login request
        /// </summary>
        EntMsgValidateLoginRequest,

        /// <summary>
        /// Entity validate login response
        /// </summary>
        EntMsgMwsValidateLoginResponse,

        /// <summary>
        /// Entity Logout Request
        /// </summary>
        EntMsgMwsLogoutRequest,

        /// <summary>
        /// Entity Register Request
        /// </summary>
        EntMsgMwsRegisterRequest,

        /// <summary>
        /// Get Translations Request
        /// </summary>
        EntMsgMwsGetTranslationsRequest,

        /// <summary>
        /// Get Meta Data Request
        /// </summary>
        EntMsgMwsGetMetaDataRequest,

        /// <summary>
        /// Get Offenders List Request
        /// </summary>
        EntMsgMwsGetOffendersListRequest,

        /// <summary>
        /// Username for validate login method 
        /// </summary>
        Username,

        /// <summary>
        /// Password for validate login method 
        /// </summary>
        Password,

        /// <summary>
        /// NewPassword
        /// </summary>
        NewPassword,

        /// <summary>
        /// SessionToken for mobile
        /// </summary>
        SessionToken,

        /// <summary>
        /// STT for mobile
        /// </summary>
        STT,

        /// <summary>
        /// Application Version
        /// </summary>
        AppVersion,

        /// <summary>
        /// GetOffendersList - filter by active or all
        /// </summary>
        isOnlyActive,

        /// <summary>
        /// GetOffendersList - filter by violation or all
        /// </summary>
        isOnlyViolation,

        /// <summary>
        /// In case you can't use EnmMobileApplication use this as string
        /// </summary>
        AppID,


        #endregion

        #region Events
        /// <summary>
        /// Get event request
        /// </summary>
        GetEventsRequest,
        /// <summary>
        /// Get event response
        /// </summary>
        GetEventsResponse,
        /// <summary>
        /// Get handling action by query request
        /// </summary>
        GetHandlingActionsByQueryRequest,
        /// <summary>
        /// Get handling action by query response
        /// </summary>
        GetHandlingActionsByQueryResponse,
        /// <summary>
        /// Get offender event status request
        /// </summary>
        GetOffenderEventStatusRequest,
        /// <summary>
        /// Get offender event status response
        /// </summary>
        GetOffenderEventStatusResponse,
        /// <summary>
        /// Get open panic call request
        /// </summary>
        GetOpenPanicCallsRequest,
        /// <summary>
        /// Get open panic call response
        /// </summary>
        GetOpenPanicCallsResponse,
        /// <summary>
        /// Handling all event request
        /// </summary>
        HandleAllEventsRequest,
        /// <summary>
        /// Add Handling Action Request
        /// </summary>
        AddHandlingActionRequest,
        /// <summary>
        /// Get Alcohol Test Result Request
        /// </summary>
        GetAlcoholTestResultRequest,
        /// <summary>
        /// Get Alcohol Test Result Response
        /// </summary>
        GetAlcoholTestResultResponse,

        IncludeOffenderDetails,
        /// <summary>
        /// Handling Status Type - used for query to get events with specific handling status
        /// </summary>
        HandlingStatusType,
        /// <summary>
        /// Handling Action Type
        /// </summary>
        HandlingActionType,
        /// <summary>
        /// events list
        /// </summary>
        EventsList,
        /// <summary>
        /// Events count to handle
        /// </summary>
        EventsCountToHandle,
        /// <summary>
        /// Events percentage to handle
        /// </summary>
        EventsPercentageToHandle,
        /// <summary>
        /// dictionary of offenders and List of events
        /// </summary>
        OffenderId2EventsDictionary,
        /// <summary>
        /// GetEventsRequest- Filter the events by code (event type)
        /// </summary>
        EventCodes,
        /// <summary>
        /// event ID
        /// </summary>
        EventID,
        /// <summary>
        /// GetEventsRequest- Indicates whether to retrieve events that were moved to history 
        /// </summary>
        IncludeHistory,
        /// <summary>
        /// GetEventsRequest- Return only new events with ID higher than LastEventID parameter 
        /// </summary>
        LastEventID,
        /// <summary>
        /// GetEventRequest - Maximum number of rows to retrieve 
        /// </summary>
        Limit,
        /// <summary>
        /// Filter the events by event message 
        /// </summary>
        Message,
        /// <summary>
        /// Filter the events by severity 
        /// </summary>
        Severity,
        /// <summary>
        /// Filter the events by handling status - New, In Process, Handled etc.
        /// </summary>
        Status,

        Parameter,
        /// <summary>
        /// AddHandlingActionRequest- Type of the action's target contact
        /// </summary>
        ContactType,
        /// <summary>
        /// Email address 
        /// </summary>
        EmailAddress,
        /// <summary>
        /// Pager code 
        /// </summary>
        PagerCode,
        /// <summary>
        /// Pager Type 
        /// </summary>
        PagerType,
        /// <summary>
        /// Reminder time (in seconds) 
        /// </summary>
        ReminderTime,
        /// <summary>
        /// Scheduled Task Sub Type 
        /// </summary>
        ScheduledTaskSubType,
        /// <summary>
        /// Scheduled task time 
        /// </summary>
        ScheduledTaskTime,
        /// <summary>
        /// Scheduled task type 
        /// </summary>
        ScheduledTaskType,
        /// <summary>
        /// Predefined code for third party action 
        /// </summary>
        ThirdPartyActionCode,
        /// <summary>
        /// Action type 
        /// </summary>
        Type,
        /// <summary>
        /// Predefined code for custom user action 
        /// </summary>
        UserDefinedActionCode,

        /// <summary>
        /// GetHandlingActionByQuery
        /// </summary>
        ActionTypeList,
        /// <summary>
        /// Filter actions by one or more Event ID 
        /// </summary>
        EventIDList,
        /// <summary>
        /// GetOpenPanicCallRequest
        /// </summary>
        MaximumRows,

        StartRowIndex,

        /// <summary>
        /// Amount Of events
        /// </summary>
        AmountOfEvents,

        /// <summary>
        /// Get event by SearchField
        /// </summary>
        SearchField,


        #endregion

        #region APIExtensions
        /// <summary>
        /// using for privat API method GetOffendersByFilterID
        /// </summary>
        GetOffendersByFilterIDRequest,
        /// <summary>
        /// using for privat API method GetOffendersByFilterID
        /// </summary>
        GetOffendersByFilterIDResponse,

        GetEventsByFilterID_StartRowIndex,

        GetEventsByFilterID_MaximumRows,

        GetEventsByFilterID_FilterID,

        GetEventsByFilterID_ScreenName,

        GetEventsByFilterID_PreviousTimeStamp,

        GetEventsByFilterID_PageUpperLimitEventId,

        GetEventsByFilterID_SortExpression,

        GetEventsByFilterID_SearchString,

        GetEventsByFilterID_FromTime,

        GetEventsByFilterID_ToTime,

        GetEventsByFilterID_ForceRefresh,

        EntEventDataResult,

        IsPrimaryLocation,

        GetUnallocatedOffenderListResponse,

        /// <summary>
        /// list of meta-data for GPS rules
        /// </summary>
        MetadataForGPSRules,

        #endregion APIExtensions

        #region Devices Tests Properties

        /// <summary>
        /// the object that implement the connection to the communication server.
        /// </summary>
        GpsDeviceProxy,
        /// <summary>
        /// the amount of points that device proxy will send
        /// </summary>
        GpsDeviceProxyPointsCount,
        /// <summary>
        /// indicate if the gps device proxy need to send HW events
        /// </summary>
        GpsDeviceProxySendHwEvents,
        /// <summary>
        /// indicate if the device support AGPS or not.
        /// </summary>
        GpsDeviceProxyAGPSSupport,
        /// <summary>
        /// indicate the age of the file that the device will send to the server to initiate AGPS download process.
        /// </summary>
        GpsDeviceProxyDeviceAGPSFileAgeOffsetMinutes,
        /// <summary>
        /// ExitCode for xt_simulator
        /// </summary>
        ExitCode,
        /// <summary>
        /// description of network issues need to be generated by the client
        /// </summary>
        GpsDeviceProxyNetworkIssuesDescription,
        /// <summary>
        /// the status of the execution of the command
        /// </summary>
        CommandExecutedStatus,

        #endregion Devices Tests Properties

        #region ProgramActions
        /// <summary>
        /// Send Download Request
        /// </summary>
        EntMsgSendDownloadRequest,

        EntMsgSendVibrationRequest,

        EntMsgSendEndOfServiceRequest,

        EntMsgSendUploadRequest,

        EntMsgSendMessageRequest,

        EntMsgSuspendProgramRequest,

        EntMsgReactivateProgramRequest,

        EntMsgGetSuspendProgramOptionsResponse,

        EntMsgGetProgramSuspensionsDataRequest,

        EntMsgGetProgramSuspensionsDataResponse,

        EntMsgGetEndOfServiceOptionsResponse,

        EntMsgGetActionsListResponse,

        EntMsgGetManualEventOptionsResponse,

        EntMsgGetEndOfServiceDataRequest,

        EntMsgGetEndOfServiceDataResponse,

        EntMsgClearCurfewUnitTamperRequest,

        StopProgramWithoutDeactivation,

        PendingRequestID,

        EndOfServiceType,

        //Message,
        //Severity,
        //Status,
        //GetEventsRequest,
        //GetEventsResponse,
        //ContactType,
        //EmailAddress,
        //PagerCode,
        //PagerType,
        //ReminderTime,
        //ScheduledTaskSubType,
        //ScheduledTaskTime,
        //ScheduledTaskType,
        //ThirdPartyActionCode,
        //Type,
        //UserDefinedActionCode,
        //AddHandlingActionRequest,
        //ActionTypeList,
        //EventIDList,
        //GetHandlingActionsByQueryRequest,
        //GetHandlingActionsByQueryResponse,
        //GetOffenderEventStatusRequest,
        //GetOffenderEventStatusResponse,
        //MaximumRows,
        //StartRowIndex,
        //GetOpenPanicCallsRequest,
        //GetOpenPanicCallsResponse,
        //HandleAllEventsRequest,
        //IncludeOffenderDetails,
        //Parameter,
        Expected,

        SuspendOptions,

        ReactivateOptions,

        SuspendID,

        ReactivateReason,

        SuspendReason,


        /// <summary>
        /// EntMsgSendDownloadRequest - Indicates whether to download the data to the device immediately or during device next call (optional)
        /// </summary>
        Immediate,

        #endregion ProgramActions

        #region Queue
        /// <summary>
        /// Get Queue Response
        /// </summary>
        GetQueueResponse,

        /// <summary>
        /// Ent Queue Program Tracker
        /// </summary>
        EntQueueProgramTracker,

        /// <summary>
        /// filter ID
        /// </summary>
        filterID,

        /// <summary>
        /// resultCode
        /// </summary>
        resultCode,

        /// <summary>
        /// GetQueueRequest
        /// </summary>
        GetQueueRequest,

        /// <summary>
        /// DB_IP
        /// </summary>
        DB_IP,

        /// <summary>
        /// DB_Port
        /// </summary>
        DB_Port,

        /// <summary>
        /// Vcomm_IP
        /// </summary>
        Vcomm_IP,

        /// <summary>
        /// Vcomm_Port
        /// </summary>
        Vcomm_Port,

        /// <summary>
        /// XT Simulator - SHUTDOWNTIMER
        /// </summary>
        SHUTDOWNTIMER,

        /// <summary>
        /// XT Simulator - Rule
        /// </summary>
        Rule,

        /// <summary>
        /// XT Simulator - RuleState : on (Create violation) or off (Restore violation)
        /// </summary>
        RuleState,

        /// <summary>
        /// XT Simulator - LBS
        /// </summary>
        LBS,

        /// <summary>
        /// XT Simulator - NUMPOINTS
        /// </summary>
        NUMPOINTS,

        /// <summary>
        /// XT Simulator - ACKCOUNT
        /// </summary>
        ACKCOUNT,


        #endregion

        #region Trail
        /// <summary>
        /// Get Last Location Request
        /// </summary>
        GetLastLocationRequest,
        /// <summary>
        /// Get Last Location Response
        /// </summary>
        GetLastLocationResponse,
        /// <summary>
        /// Get Geographic Locations Request
        /// </summary>
        GetGeographicLocationsRequest,
        /// <summary>
        /// Get Geographic Locations Response
        /// </summary>
        GetGeographicLocationsResponse,
        /// <summary>
        /// Get Trail Request
        /// </summary>
        GetTrailRequest,
        /// <summary>
        /// Get Trail Response
        /// </summary>
        GetTrailResponse,
        /// <summary>
        /// Determines the format of the GPS coordinates in the result
        /// </summary>
        CoordinatesFormat,
        /// <summary>
        /// Return last location data only for offenders in unknown location status
        /// </summary>
        OnlyInUnknownLocation,
        /// <summary>
        /// Return last location data only for offenders in violation status
        /// </summary>
        OnlyInViolation,
        /// <summary>
        /// Filter the positions by a list of offender IDs 
        /// </summary>
        OffendersIDs,
        /// <summary>
        /// Return a list of specific positions by ID 
        /// </summary>
        PositionsIDs,
        /// <summary>
        /// Filtering GPS points to retrieve only points in violation mode
        /// </summary>
        OnlyViolation,
        /// <summary>
        /// Filtering GPS points by minimum interval between points in seconds
        /// </summary>
        MinimumSeconds,
        /// <summary>
        /// Filtering GPS points by minimum interval between points in meters 
        /// </summary>
        MinimumMeters,

        OffendersIdWithTrail,

        /// <summary>
        /// EntMsgSendDownloadRequest - Indicates whether to download the new data for all offenders monitored by the device - relevant only for E4 device when using one unit to monitor multiple offenders (optional)
        /// </summary>
        IsMultipleDownload,
        GetOffenderWithTrail,

        ProgramTypes,

        #endregion ProgramActions

        #region Configuration


        /// <summary>
        /// Indicates whether rule triggers a red led on the device (for 1 Piece unit only) 
        /// </summary>
        IsGenerateLED,

        /// <summary>
        /// Indicates whether rule generates a vibration on the device (for 1 Piece unit only) 
        /// </summary>
        IsGenerateVibration,

        /// <summary>
        /// Indicates whether rule generates a warning message on device screen (for 2 Piece unit only) 
        /// </summary>
        IsGenerateWarningMessage,

        /// <summary>
        /// Indicates whether event of current rule triggers an immediate upload 
        /// </summary>
        IsImmediateUpload,

        /// <summary>
        /// ID of rule (Mandatory)
        /// </summary>
        RuleID,

        /// <summary>
        /// Text to be displayed on device screen as a warning message (for 2 Piece unit only)  
        /// </summary>
        WarningMessage,

        /// <summary>
        /// Download version number (mandatory - relevant only when using offender ID)
        /// </summary>
        VersionNumber,

        /// <summary>
        /// Priority of CSD mode when more then one communication mode is supported
        /// </summary>
        CSDPriority,

        /// <summary>
        /// Indicates whether service receives an unrecognised call, when offender is calling to a voice test
        /// </summary>
        CallerIDRequired,

        /// <summary>
        /// Indicates the the default communication method for voice tests Relevant only when using E4 RF device with voice tests
        /// </summary>
        CommunicationMethodForVoiceTests,

        /// <summary>
        /// Indicates the the default communication method for voice tests Relevant only when using E4 RF device with voice tests
        /// </summary>
        GPRSPriority,

        /// <summary>
        /// Grace time allowed after leaving a time frame (mandatory) (relevant only for RF or Voice programs)
        /// </summary>
        GraceAfterTimeFrameEnd,

        /// <summary>
        /// Grace time allowed after entering a time frame (mandatory) (relevant only for RF or Voice programs) 
        /// </summary>
        GraceAfterTimeFrameStart,


        /// <summary>
        /// Minimum grace time inside the time frame, after performing a voice test (mandatory)
        /// </summary>
        GraceAfterVoiceTestEnd,


        /// <summary>
        /// Grace time allowed to leave a time frame early (in minutes) (mandatory) (relevant only for RF or Voice programs)
        /// </summary>
        GraceBeforeTimeFrameEnd,

        /// <summary>
        /// Grace time allowed to enter a time frame early (mandatory) (relevant only for RF or Voice programs)
        /// </summary>
        GraceBeforeTimeFrameStart,

        /// <summary>
        /// Minimum grace time inside the time frame, before performing a voice test (mandatory)
        /// </summary>
        GraceBeforeVoiceTestStart,

        /// <summary>
        /// Indicates whether service will hang up when offender is calling outside the schedule (relevant only for Voice programs)
        /// </summary>
        HangupOutOfSchedule,

        /// <summary>
        /// Indicates whether to enable Kosher mode on E4 device (relevant only for E4 devices) 
        /// </summary>
        IsKosher,

        /// <summary>
        /// Priority of LandLine mode when more then one communication mode is supported (optional) 
        /// </summary>
        LandlinePriority,

        /// <summary>
        /// Minimum time between alcohol tests (mandatory) (relevant only for alcohol test programs) 
        /// </summary>
        MinimumTimeBetweenAlcoholTests,

        /// <summary>
        /// How many times to repeat the phrases during the Voice test (mandatory)
        /// </summary>
        NumberOfPhrases,

        /// <summary>
        /// How many rings receiver will wait when DCC service is calling (mandatory)
        /// </summary>
        NumberOfRings,

        /// <summary>
        /// Password to access the officer screen on E4 device (optional) (relevant only for E4 devices)
        /// </summary>
        OfficerPassword,

        /// <summary>
        /// Indicates whether to enable access to the officer screen on E4 device (relevant only for E4 devices)
        /// </summary>
        OfficerScreenActive,

        /// <summary>
        /// RF range for the receiver (mandatory)
        /// </summary>
        ReceiverRange,

        /// <summary>
        /// E4 Siren volume level, number between 1-3 (relevant only for E4 devices) 
        /// </summary>
        RingVolume,

        /// <summary>
        /// Indicates whether receiver works on CSD mode (not relevant for program type Voice Verification)
        /// </summary>
        SupportCSD,

        /// <summary>
        /// Indicates whether receiver works in GPRS mode (not relevant for program type Voice Verification)
        /// </summary>
        SupportGPRS,

        /// <summary>
        /// Indicates whether receiver works in Land Line mode (not relevant for program type Voice Verification)
        /// </summary>
        SupportLandline,

        /// <summary>
        /// Indicates whether download by SMS (sending SMS to device to callback to the DCC service) is supported
        /// </summary>
        SupportSMSCallback,

        /// <summary>
        /// Maximum time (in hours) between receiver uploads (mandatory)
        /// </summary>
        TimeBetweenUploads,

        /// <summary>
        /// How long (in minutes) the system can lose detection of the transmitter, before the receiver will report (not relevant for program type Voice Verification)
        /// </summary>
        TransmitterDisappearTime,

        /// <summary>
        /// Indicates whether to send a Voice test immediately after enrollment
        /// </summary>
        VerifyAfterEnroll,

        /// <summary>
        ///   Contains parameters to update configuration of a GPS rule
        /// </summary>
        UpdateGPSRuleConfigurationRequest,


        /// <summary>
        ///   Contains parameters to reset the configuration of a GPS rule of an offender to default system level
        /// </summary>
        ResetGPSRuleConfigurationRequest,

        /// <summary>
        /// Contains parameters to get a list of GPS rules configuration
        /// </summary>
        GetGPSRuleConfigurationListRequest,

        /// <summary>
        /// Get GPS Rule Configuration List Response Message
        /// </summary>
        GetGPSRuleConfigurationListResponse,

        /// <summary>
        /// Get GPS Rule Configuration List for Default Response Message
        /// </summary>
        GetGPSRuleConfigurationListDefaultResponse,


        /// <summary>
        /// Get Offender Configuration Data Request
        /// </summary>
        GetOffenderConfigurationDataRequest,
        /// <summary>
        /// Get Offender Configuration Data Response
        /// </summary>
        GetOffenderConfigurationDataResponse,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether to use the Base Unit to communicate with the server while cellular unit is in charging relevant only for program 2 Piece
        /// </summary>
        BaseUnitActive,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- RF range between Base Unit and transmitter (while cellular unit is in charging) - relevant only for program 2 Piece 
        /// </summary>
        BaseUnitRange,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Default message to be displayed on receiver screen (2 Piece only), relevant only when 'DisplayOffenderName' equals false 
        /// </summary>
        DefaultMessage,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether receiver will record other transmitters in the area 
        /// </summary>
        DetectOtherTransmitters,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether offender name should be displayed on the receiver screen (for 2 Piece unit only) 
        /// </summary>
        DisplayOffenderName,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- How long (in seconds) the receiver waits to start using LBS tracking after failing to acquire a GPS signal 
        /// </summary>
        GPSDisappearTime,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Allowed proximity distance (in meters) between the aggressor and victim. relevant only for Domestic Violence programs 
        /// </summary>
        GPSProximity,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Buffer defined (in meters) around the 'GPS Proximity'. relevant only for Domestic Violence programs 
        /// </summary>
        GPSProximityBuffer,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether the Base Unit uses landline communication (if available) instead of communicating only through the cellular unit relevant only for program 2 Piece 
        /// </summary>
        LandlineActive,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether communication server receives an unrecognized call from Base Unit (when 'Landline Active' equals true) relevant only for program 2 Piece 
        /// </summary>
        LandlineCallerIDRequired,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether to use cellular LBS location detection in cases where no GPS signal was found 
        /// </summary>
        LBSActive,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- How long (in seconds) the receiver waits before loading a 'LBS No Location' event 
        /// </summary>
        LBSNoPositionTimeout,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- How long (in seconds) the receiver waits for a PLG request, to get a LBS point
        /// </summary>
        LBSRequestTimeout,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- How often (in seconds) the receiver will record a LBS point 
        /// </summary>
        LBSSamplingRate,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- How often (in seconds) receiver will record a GPS point 
        /// </summary>
        NormalLoggingRate,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether receiver is in passive mode (low sanity and not immediate calls) 
        /// </summary>
        PassiveMode,

        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether reception icons (GPS/Cellular) are active 
        /// </summary>
        ReceptionIconsActive,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Indicates whether the timeframes in the schedule are active 
        /// </summary>
        ScheduleActive,

        /// <summary>
        /// Used in ConvertConfigurationListToUpdatedListTestBlocks
        /// </summary>
        ElementsList,

        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- Maximum time (in minutes) between Base Unit uploads. relevant only for program 2 Piece
        /// </summary>
        TimeBetweenBaseUnitUploads,


        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest- How often (in seconds) the receiver will record a GPS point in violation mode 
        /// </summary>
        ViolationLoggingRate,
        /// <summary>
        /// GetO ffender Configuration Data Response - default configuration
        /// </summary>
        GetOffenderConfigurationDataDefaultResponse,
        /// <summary>
        /// UpdateGPSOffenderConfigurationDataRequest
        /// </summary>
        LBSForViolations,
        /// <summary>
        /// Update GPS Offender Configuration Data Request
        /// </summary>
        UpdateGPSOffenderConfigurationDataRequest,
        /// <summary>
        /// Get Handling Procedure Request
        /// </summary>
        GetHandlingProcedureRequest,
        /// <summary>
        /// Get Handling Procedure Response
        /// </summary>
        GetHandlingProcedureResponse,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to send an automatic email notification to the agency, when an automatic event occurred 
        /// </summary>
        AutoEmailNotificationToAgency,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to send an automatic email notification to the officer, when an automatic event occurred 
        /// </summary>
        AutoEmailNotificationToOfficer,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to send an automatic fax notification to the agency, when an automatic event occurred 
        /// </summary>
        AutoFaxNotificationToAgency,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to send an automatic fax notification to the officer, when an automatic event occurred 
        /// </summary>
        AutoFaxNotificationToOfficer,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to send an automatic pager notification to the agency, when an automatic event occurred 
        /// </summary>
        AutoPagerNotificationToAgency,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to send an automatic pager notification to the officer, when an automatic event occurred 
        /// </summary>
        AutoPagerNotificationToOfficer,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to apply agency's procedure on offender's procedure 
        /// </summary>
        InheritAgency,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Comments for handling procedure - in offender level 
        /// </summary>
        Comments,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Indicates whether to apply officer's procedure on offender's procedure 
        /// </summary>
        InheritOfficer,
        /// <summary>
        /// UpdateHandlingProcedureRequest- Handling procedure for violation - in offender level 
        /// </summary>
        ViolationHandling,
        /// <summary>
        /// Update Handling Procedure Request
        /// </summary>
        UpdateHandlingProcedureRequest,

        /// <summary>
        /// Contains parameters to update configuration of RF program for offender 
        /// </summary>
        UpdateRFOffenderConfigurationDataRequest,
        IsEquipmentExists,
        EqipmentID,
        Equipment,
        IsElementVisible,
        IsExpectedVisible,
        ExpectedVersions,
        UserType,
        GuiSecurityResponse,
        ModifyOption,
        ModifyStatus,
        State,
        ResourceID,
        FileName,
        MenuItem,
        MenuButtonName,



        #endregion Configuration

        #region Users

        /// <summary>
        /// EntMsgGetUsersListRequest
        /// </summary>
        EntMsgGetUsersListRequest,

        /// <summary>
        /// EntMsgGetUsersListResponse
        /// </summary>
        EntMsgGetUsersListResponse,

        /// <summary>
        /// EntMsgGetOfficerRequest
        /// </summary>
        EntMsgGetOfficerRequest,

        /// <summary>
        /// EntMsgGetOfficerResponse
        /// </summary>
        EntMsgGetOfficerResponse,

        /// <summary>
        /// UserID
        /// </summary>
        UserID,

        /// <summary>
        /// UserName
        /// </summary>
        UserName,

        /// <summary>
        /// Return only users with a specific status
        /// </summary>
        UserStatus,

        /// <summary>
        /// Include Deleted Users
        /// </summary>
        IncludeDeletedUsers,

        /// <summary>
        /// Email
        /// </summary>
        Email,

        /// <summary>
        /// MRD Serial Number
        /// </summary>
        MrdSN,

        /// <summary>
        /// Search String (on text and numuric parameters only) - Not in Use on API
        /// </summary>
        SearchString,

        /// <summary>
        /// the response for GetAgenciesListForMC
        /// </summary>
        GetAgenciesListForMCResponse,
        /// <summary>
        /// Equipment serial number
        /// </summary>
        EquipmentSN,

        #endregion

        #region Web_Offenders

        /// <summary>
        /// Filter title from web in offenders list screen
        /// </summary>
        ActualTitle,

        /// <summary>
        /// Expected Filter title from web in offenders list screen
        /// </summary>
        ExpectedTitle,

        /// <summary>
        /// Expected Amount of offenders by filter
        /// </summary>
        ExpectedAmount,

        /// <summary>
        /// for ClickOnOffendersFilter in offenders list screen
        /// </summary>
        sectionNumber,

        /// <summary>
        /// get the amount of rows from offenders list
        /// </summary>
        AmountOfRows,

        /// <summary>
        /// Amount Of Rows from the web
        /// </summary>
        WebAmountOfRows,

        /// <summary>
        /// Amount Of Rows from the API
        /// </summary>
        APIAmountOfRows,

        /// <summary>
        /// About Tool Tip ChangePassword
        /// </summary>
        ChangePassword,

        /// <summary>
        /// About Tool Tip Logout
        /// </summary>
        Logout,
        /// <summary>
        /// Agency name (for web)
        /// </summary>
        AgencyName,

        /// <summary>
        /// Boolean for EOS popup form
        /// </summary>
        noReceiver,

        /// <summary>
        /// Boolean for EOS popup form
        /// </summary>
        isAutoEOS,

        /// <summary>
        /// Boolean for EOS popup form
        /// </summary>
        isSuccessful,

        /// <summary>
        /// Boolean for EOS popup form
        /// </summary>
        isCurfewViolations,
        /// <summary>
        /// AddSchedule - timeframe object
        /// </summary>
        TimeFrame,
        /// <summary>
        /// AddNewOffender - program subType
        /// </summary>
        SubType,
        /// <summary>
        /// AddNewOffender - transmitter
        /// </summary>
        Transmitters,
        /// <summary>
        /// AddNewOffender - receiver
        /// </summary>
        Receivers,
        /// <summary>
        /// AddSchedule - StartDate
        /// </summary>
        StartDate,
        /// <summary>
        /// AddSchedule - EndDate
        /// </summary>
        EndDate,
        /// <summary>
        /// AddSchedule - RecurseEveryWeek boolean
        /// </summary>
        RecurseEveryWeek,
        /// <summary>
        /// AddSchedule - location
        /// </summary>
        Location,

        IsElementExist,
        /// <summary>
        /// Boolean for Offender's trail point existense
        /// </summary>
        IsLastTrailPointExist,
        /// <summary>
        /// Boolean for Offender's trail point existense on map
        /// </summary>
        IsLastTrailPointDisplayedOnMap,
        /// <summary>
        /// Use to move from Offender Details Tab to Current Status Tab
        /// </summary>
        DoNavigateToCurrentStatus,
        #endregion

        #region Web_Monitor
        /// <summary>
        /// HandleEvent - handlingOption
        /// </summary>
        HandlingOption,
        /// <summary>
        /// HandleEvent - eventStatus
        /// </summary>
        EventStatus,
        /// <summary>
        /// expected value for assert block
        /// </summary>
        expected,
        /// <summary>
        /// monitor table row
        /// </summary>
        MonitorTableRow,
        /// <summary>
        /// FilterByPeriod - recent
        /// </summary>
        Recent,

        /// <summary>
        /// Used to get Download Recommended flag
        /// </summary>
        OffenderLabel,

        /// <summary>
        /// Used in Web_OffenderHeaderDetailsLabelAssertBlock to compare expected with actual offender label
        /// </summary>
        ExpectedOffenderLabel,

        /// <summary>
        /// Used to get offender status
        /// </summary>
        OffenderStatus,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndVerifyOffenderStatusTestBlock  to compare expected with actual offender status
        /// </summary>
        ExpectedOffenderStatus,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndVerifyOffenderStatusTestBlock
        /// </summary>
        SearchTerm,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndVerifyOffenderStatusTestBlock
        /// </summary>
        SecondarySearchTerm,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndVerifyOffenderContactInformationTestBlock
        /// </summary>
        OffenderContact,

        /// <summary>
        /// Used in Web_OffenderContactInformationAssertBlock
        /// </summary>
        ExpectedOffenderContactPhone,


        /// <summary>
        /// Used in Web_OffenderContactInformationAssertBlock
        /// </summary>
        ExpectedOffenderContactPrefixPhone,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndVerifyVoiceSimNumberTestBlock
        /// </summary>
        voiceSimNumber,

        /// <summary>
        /// Used in Web_VoiceSimNumberAssertBlock
        /// </summary>
        ExpectedVoiceSimNumber,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndVerifyPictureDisplayedTestBlock
        /// </summary>
        Displayed,

        /// <summary>
        /// Used in Web_VerifyDVLinkAssertBlock
        /// </summary>
        isDVLinkWorks,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndGetTxFromDVPairTestBlock
        /// </summary>
        VicTX,

        /// <summary>
        /// Used in Web_OffendersListScreenSearchOffenderAndGetTxFromDVPairTestBlock
        /// </summary>
        AggTX,

        /// <summary>
        /// filter by event time
        /// </summary>
        Time,

        /// <summary>
        /// Used in Web_NavigateToGroupsListAddNewGroupTestBlock
        /// </summary>
        GroupType,

        /// <summary>
        /// Used in Web_NavigateToGroupsListAddNewGroupTestBlock
        /// </summary>
        GroupName,

        /// <summary>
        /// Used in Web_NavigateToGroupsListAddNewGroupTestBlock
        /// </summary>
        GroupDescription,

        /// <summary>
        /// Used in Web_NavigateToGroupsListAddNewGroupTestBlock
        /// </summary>
        NumOfOffenders,

        /// <summary>
        /// Used in Web_SelectGroupAndSelectOffenderTestBlock
        /// </summary>
        isZoneExist,

        /// <summary>
        /// Used in Web_SelectGroupAndSelectOffenderTestBlock
        /// </summary>
        isTimeFrameExist,

        /// <summary>
        /// Used in Web_SelectGroupAndSelectOffenderTestBlock
        /// </summary>
        TimeFrameName,

        #endregion

        #region MTD Action related properties
        /// <summary>
        /// the string representation of MTD action
        /// </summary>
        MtdAction,
        /// <summary>
        /// MTD action command number
        /// </summary>
        MtdActionCommandNumber,
        /// <summary>
        /// MTD action command parameters
        /// </summary>
        MtdActionCommandParam,
        /// <summary>
        /// 
        /// </summary>
        MtdActionSoftwareType,
        /// <summary>
        /// List of lists of SecurityConfigObject. Web Security Configuration results
        /// </summary>
        SecurityConfigResults,
        /// <summary>
        /// TrailReport object
        /// </summary>
        TrailReport,
        /// <summary>
        /// Offender location details
        /// </summary>
        OffenderLocation,

        #endregion MTD Action related properties

        #region Agency
        /// <summary>
        /// GetAgency Request
        /// </summary>
        GetAgenciesRequest,
        /// <summary>
        /// GetAgency response
        /// </summary>
        GetAgenciesResponse,
        /// <summary>
        /// int. The number of results to show in a page
        /// </summary>
        ShowResults,
        SystemFilters,
        #endregion

        #region
        ScreenName,
        section,
        response,
        entSecurityItem,
        securityList,
        MTDActionrelatedproperties,
        #endregion 


        #region Security Configurartion

        /// <summary>
        /// Used in Web_SelectOffenderAndNavigateToTabTestBlock
        /// </summary>
        FirstSearchTerm,

        /// <summary>
        /// Used in Web_SelectOffenderAndNavigateToTabTestBlock
        /// </summary>
        SecondSearchTerm,

        /// <summary>
        /// Used in Web_SelectOffenderAndNavigateToTabTestBlock
        /// </summary>
        FilterTitle,

        /// <summary>
        /// used in SetGUISecurityDataTestBlock
        /// </summary>
        Refine,

        /// <summary>
        /// used in SetGUISecurityDataTestBlock
        /// </summary>
        RefineNewRequest,

        /// <summary>
        /// used in SetGUISecurityDataTestBlock
        /// </summary>
        ExcludeItem,

        /// <summary>
        /// used in SetGUISecurityDataTestBlock
        /// </summary>
        Response,

        /// <summary>
        /// Used in Web_SecurityConfigFastTestBlock
        /// </summary>
        isElementShown,

        /// <summary>
        /// Used in Web_SecurityConfigAssertBlock
        /// </summary>
        ExpectedValue,

        /// <summary>
        /// Used in Web_SecurityConfigAssertBlock
        /// </summary>
        VisibilityState,
        Event,
        PrimaryTabLocatorString,
        SecondaryTabLocatorString,
        EntMsgAddAuthorizedAbsenceDataRequest,
        /// <summary>
        /// AddAuthorizedAbsenceDataRequest - used at InformationProxy at AddAuthorizedAbsenceData method
        /// </summary>
        AddAuthorizedAbsenceDataRequest,
        /// <summary>
        /// Visible Custom Fields Definitions - retrive from IOffender API
        /// </summary>
        VisibleCustomFieldsDefinitions,
        NonApprovalDescription,
        IsFollowUpCompleted,
        ActionIfAbsenceConfirmViolated,
        ActionToConfirmFollowUp,
        Code,
        IsActionAuthenticationConfirmed,
        IsApproved,
        FilterName,
        AddOffenderWarningRequest,
        AuthorizedAbsenceRequestID,
        AddScheduleChangeRequest,
        /// <summary>
        /// Used in Web_SecurityConfigUser_Main_Block
        /// </summary>
        AccountStatus,
        /// <summary>
        /// bool; IsExact comparer for API security tests
        /// </summary>
        IsExact,


        /// <summary>
        /// Int32 Filter counter
        /// </summary>
        FilterCounter,

        /// <summary>
        /// Get officer response
        /// </summary>
        GetOfficerResponse,
        /// <summary>
        /// Officer name
        /// </summary>
        OfficerName,
        /// <summary>
        /// monitor table row array
        /// </summary>
        MonitorTableRowArray,
        /// <summary>
        /// current status array
        /// </summary>
        CurrentStatusTableRowArray,
        /// <summary>
        /// get event response- copy
        /// </summary>
        GetEventsResponseCopy,
        /// <summary>
        /// custom fields
        /// </summary>
        CustomFields,
        /// <summary>
        /// get picture request
        /// </summary>
        GetPicturesRequest,
        /// <summary>
        /// get picture response
        /// </summary>
        GetPicturesResponse,
        /// <summary>
        /// Expected offender ID
        /// </summary>
        ExpectedOffenderID,
        /// <summary>
        /// Expected contact ID
        /// </summary>
        ExpectedContactID,
        /// <summary>
        /// Element to convert
        /// </summary>
        ElementToConvert,
        /// <summary>
        /// Converted value
        /// </summary>
        ConvertedValue,
        /// <summary>
        /// Custom fields definitions
        /// </summary>
        CustomFieldDefinition,
        /// <summary>
        /// Get group details response
        /// </summary>
        GetGroupDetailsResponse,
        /// <summary>
        /// Get group details request
        /// </summary>
        GetGroupDetailsRequest,

        /// <summary>
        /// Delete group details request
        /// </summary>
        DeleteGroupDetailsRequest,

        /// <summary>
        /// Detach Offender From Group Request
        /// </summary>
        DetachOffenderFromGroupRequest,

        /// <summary>
        /// Get group data request
        /// </summary>
        GetGroupDataRequest,

        /// <summary>
        /// Groups with offenders
        /// </summary>
        GroupsWithOffenders,
        /// <summary>
        /// Group object
        /// </summary>
        Group,
        /// <summary>
        /// Expected picture id
        /// </summary>
        ExpectedPictureID,
        /// <summary>
        /// Operator
        /// </summary>
        Operator,
        /// <summary>
        /// AddOffenderAddress- amount of phone
        /// </summary>
        AmountOfPhones,
        /// <summary>
        /// GetEquipment- Return Damage Data
        /// </summary>
        ReturnDamageData,
        /// <summary>
        /// GetEquipment - Return Only Not Allocated
        /// </summary>
        ReturnOnlyNotAllocated,
        /// <summary>
        /// UpdateEquipment - DeviceFCMToken
        /// </summary>
        DeviceFCMToken,
        /// <summary>
        /// GetEquipment - Return only deletable
        /// </summary>
        ReturnOnlyDeletable,
        /// <summary>
        /// Groups - Add group details request
        /// </summary>
        AddGroupDetailsRequest,
        /// <summary>
        /// AddGroup - Offenders list
        /// </summary>
        OffendersList,
        /// <summary>
        /// Add Group Offenders List IDs (int)
        /// </summary>
        OffendersListIDs,
        /// <summary>
        /// AddGroup - Program group
        /// </summary>
        ProgramGroup,
        /// <summary>
        /// AddGroup - Group details ID
        /// </summary>
        GroupDetailsID,
        /// <summary>
        /// Get Schedule Parameters Request
        /// </summary>
        GetScheduleParametersRequest,
        /// <summary>
        /// Get Schedule Parameters Responce
        /// </summary>
        GetScheduleParametersResponse,
        /// <summary>
        /// Action from time
        /// </summary>
        ActionFromTime,
        /// <summary>
        /// Action to time
        /// </summary>
        ActionToTime,
        /// <summary>
        /// Contains action type
        /// </summary>
        ContainsActionType,
        /// <summary>
        /// Include no location
        /// </summary>
        IncludeNoLocation,

        /// <summary>
        /// Timeout for Wait.Until method
        /// </summary>
        Timeout,
        /// <summary>
        /// Insert manual event request
        /// </summary>
        InsertManualEventRequest,
        /// <summary>
        /// Handling status
        /// </summary>
        HandlingStatus,
        /// <summary>
        /// Event sub type code
        /// </summary>
        EventSubTypeCode,
        /// <summary>
        /// Event code
        /// </summary>
        EventCode,
        /// <summary>
        /// Start online mode request
        /// </summary>
        StartOnlineModeRequest,
        /// <summary>
        /// Stop online mode request
        /// </summary>
        StopOnlineModeRequest,
        /// <summary>
        /// Send start range test request
        /// </summary>
        EntMsgSendStartRangeTestRequest,
        /// <summary>
        /// Send end range test request
        /// </summary>
        EntMsgSendEndRangeTestRequest,
        /// <summary>
        /// Buzzer
        /// </summary>
        Buzzer,
        /// <summary>
        /// Range
        /// </summary>
        Range,
        /// <summary>
        /// Expected status
        /// </summary>
        ExpectedStatus,
        /// <summary>
        /// RFProximity
        /// </summary>
        RFProximity,
        /// <summary>
        /// Devices count (simulator param)
        /// </summary>
        DevicesCount,
        /// <summary>
        /// Call Interval (simulator param)
        /// </summary>
        CallInterval,
        /// <summary>
        /// loop sleep (simulator param)
        /// </summary>
        LoopSleep,
        /// <summary>
        /// DV mode (simulator param)
        /// </summary>
        DV,
        /// <summary>
        /// Debug (simulator param)
        /// </summary>
        isDebug,
        /// <summary>
        /// Console debug (simulator param)
        /// </summary>
        ConsoleDebug,
        DataPrefixPhoneSecondary,
        DataPhoneNumberSecondary,
        VoicePhoneNumberSecondary,
        VoicePrefixPhoneSecondary,
        /// <summary>
        /// Offender created date
        /// </summary>
        OffenderDateCreated,
        /// <summary>
        /// RefId minimum length
        /// </summary>
        RefIdStringMinLength,
        /// <summary>
        /// Get log response
        /// </summary>
        GetLogResponse,
        /// <summary>
        /// Request ID
        /// </summary>
        RequestID,
        /// <summary>
        /// Include archived
        /// </summary>
        IncludeArchived,
        /// <summary>
        /// Aggressor (entOffender)
        /// </summary>
        Aggressor,
        /// <summary>
        /// Victim (entOffender)
        /// </summary>
        Victim,
        /// <summary>
        /// Web_AddZoneAssertBlock - Zone entity 
        /// </summary>
        Zone,

        /// <summary>
        /// for block Web_CurrentStatusAllPages
        /// </summary>
        IsPagingWorks,
        /// <summary>
        /// IsE4WeeklyTimeframe- is timeframe weekly from program type E4 (Web Delete timeframe) 
        /// </summary>
        IsE4WeeklyTimeframe,
        /// <summary>
        /// CustomFieldsVisibilityList- list of custom fields' types to show
        /// </summary>
        CustomFieldsVisibilityList,
        /// <summary>
        /// ShowInOffendersList- custom fields to show in offenders list
        /// </summary>
        ShowInOffendersList,
        /// <summary>
        /// ShowInOffenderDetails- custom fields to show in offender details
        /// </summary>
        ShowInOffenderDetails,
        /// <summary>
        /// FieldType- custom field' field type
        /// </summary>
        FieldType,
        /// <summary>
        /// CustomFieldsDefinitions- custom fields definitions
        /// </summary>
        CustomFieldsDefinitions,
        /// <summary>
        /// CustomFieldsByTypeLst- list of custom fields by field type
        /// </summary>
        CustomFieldsByTypeLst,
        /// <summary>
        /// Custom Fields System Definitions (GetCustomFieldsSystemDefinitions method)
        /// </summary>
        CustomFieldsSystemDefinitions,
        /// <summary>
        /// Table ID of custom field list value
        /// </summary>
        TableID,
        /// <summary>
        /// GetPredefineCustomFieldsSystemDefinitionsRequestBlock - custom field system definition from type 'Predefine' (list)
        /// </summary>
        PredefineCustomFields,
        /// <summary>
        /// Action type to performe on custom field list value: I/U/D/R (insert/update/delete/read) 
        /// </summary>
        EnmDBAction,
        /// <summary>
        /// Custom field list value' value code(value code in the ems_systable)
        /// </summary>
        ValueCode,
        /// <summary>
        /// Custom field list value' name
        /// </summary>
        TableIdName,
        /// <summary>
        /// UpdateEmsSystableTestBlock - dictionary input parameter
        /// </summary>
        EmsSystableDictionary,
        /// <summary>
        /// Custom field list value' updated value (for updating value in web)
        /// </summary>
        UpdatedDescription,
        /// <summary>
        /// Custom field list visibility list- related to visibility in web
        /// </summary>
        CustomFieldsVisibility,

        /// <summary>
        /// for block Web_NavigateToOperationalReportsTestBlock
        /// </summary>
        ReportName,

        /// <summary>
        /// for block Web_NavigateToOperationalReportsTestBlock
        /// </summary>
        ReportSelection,

        /// <summary>
        /// for block Web_FileDownloadAssertBlock
        /// </summary>
        GenerateReportDateTime,

        /// <summary>
        /// using as parametr to caluculate value after delta
        /// </summary>
        BeforeDelta,

        /// <summary>
        /// using as parametr to caluculate delta of value
        /// </summary>
        Delta,

        /// <summary>
        /// using to convert String evet id to integer 
        /// </summary>
        intEventID,
        /// <summary>
        /// using to calculate amount of events 
        /// </summary>

        AmountOfEvents2,
        /// <summary>
        /// Get custom fields list values of specific table ID
        /// </summary>
        CustomFieldsByTableID,
        /// <summary>
        /// Custom fields dictionary of id and visibility status (bool) 
        /// </summary>
        CustomFieldsListValuesVisibility,
        /// <summary>
        /// Is custom field can be deleted
        /// </summary>
        IsNotDeletable,
        /// <summary>
        /// Custom field ID
        /// </summary>
        CustomFieldID,
        /// <summary>
        /// Custom field value
        /// </summary>
        CustomFieldValue,
        /// <summary>
        /// Get all resource data 
        /// </summary>
        GetAllResourcesData,
        /// <summary>
        /// GetAllResourcesData- resource platform
        /// </summary>
        ResourcePlatform,
        /// <summary>
        /// Verify if custom field (with updated name) exist in web
        /// </summary>
        IsUpdatedCustomFieldNameExist,
        /// <summary>
        /// GetDataRequest- using for filter service, getData() request (private API)
        /// </summary>
        GetDataRequest,
        /// <summary>
        /// GetDataResponse- using for filter service, getData() request (private API)
        /// </summary>
        GetDataResponse,
        /// <summary>
        /// Linked events table row while expand event and press on the linked event check box
        /// </summary>
        LinkedEventsTableRow,
        /// <summary>
        /// EventLogID - using as input for APIExtentions service, getRelatedeEvents() request
        /// </summary>
        EventLogID,
        /// <summary>
        /// RowCount - using as input for APIExtentions service, getRelatedeEvents() request
        /// this parameter defined how many rows we will get
        /// </summary>
        RowCount,
        /// <summary>
        /// GetRelatedEventsResponse - using as output for APIExtentions service, getRelatedeEvents() request
        /// </summary>
        GetRelatedEventsResponse,
        /// <summary>
        /// APIAmountOfRowsAfter - using for WaitForRelatedEventTestBlock 
        /// </summary>
        APIAmountOfRowsAfter,
        /// <summary>
        /// APIAmountOfRowsBefore - using for WaitForRelatedEventTestBlock 
        /// </summary>
        APIAmountOfRowsBefore,
        /// <summary>
        /// LinkedEventID- using for save the event_id of the schedule task in linked to the EventID (WaitForRelatedEventTestBlock)  
        /// </summary>
        LinkedEventID,
        /// <summary>
        /// DelayTimeForLinkedEvent- using as delay time for wait for new schedule task event 
        /// </summary>
        DelayTimeForLinkedEvent,
        /// <summary>
        /// ArrayLength- using to defined the array length of linked events
        /// </summary>
        ArrayLength,
        /// <summary>
        /// Offender additional data
        /// </summary>
        OffenderAdditionalData,
        /// <summary>
        /// Using for web Security, checking if navigation to page is already done
        /// </summary>
        Prepration,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        UploadFileRequest,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        FileDataField,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        AddFileRequest,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        FilePath,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        DataMemberAttribute,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        FileData,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        FileDisplayName,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        OwnerEntity,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        GetFileList,
        /// <summary>
        /// Using for web Security, Add File to Additional Data
        /// </summary>
        request,
        /// <summary>
        /// using to save the filter name that going to be set
        /// </summary>
        FilterTitleNameToSet,      
        EntConfigurationGPS,
        EquipmentListData,
        GetEquipmentListRequestForEquipmentScreen,
        GetVictimAggressorPairsResponse,
        ShowOnlyActive,
        OffendersProxy,
        MapProvider,
        OffenderSummaryData,
        LogProgram,
        QueueProgramTracker,
        EventSeverity,
        IsHistory,
        EventVersion,
        IsAlcoholEvent,
        EventAdditionalData,
        ContactList,

        #endregion


    }
}