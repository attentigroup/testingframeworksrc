﻿namespace Common.Enum
{

    public enum EnumApiExtentionSortOptions
    {
        SeverityId,
        EventTime,
        UpdateTime,
        Timestamp,
        AgencyName,
        EventlogId
    }
}