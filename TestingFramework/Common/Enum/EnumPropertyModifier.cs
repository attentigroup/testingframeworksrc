﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enum
{
    /// <summary>
    /// enumaration for the modification type of the property
    /// </summary>
    public enum EnumPropertyModifier
    {
        /// <summary>
        /// default property modifier - framework will not do anything with this property
        /// </summary>
        None = 0,
        /// <summary>
        /// property mandatory - instruct the framework to raise error when this property isn't set
        /// </summary>
        Mandatory,
        /// <summary>
        /// property output - instruct the framework to return this property from the block to the flow 
        /// and then this value can be available for ather blockes
        /// </summary>
        Output,
        /// <summary>
        /// property don't generate random values - instruct the framework to not generate random value for that property 
        /// for example start time and end timne of block execution or identifiers of systems that not need to be generate
        /// </summary>
        DontGenerateRandomValues,
        /// <summary>
        /// property don't receive - instruct the framework to not set value to that property from previous block
        /// for example start time and end timne of block execution
        /// </summary>
        DontReceiveFromPreviousBlock,
    }
}
