﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Base
{
    /// <summary>
    /// implementation of singleton pattern
    /// </summary>
    /// <typeparam name="T">Lowest sub-class type.</typeparam>
    public abstract class SingletonBase<T> where T : class, new()
    {
        /// <summary>
        /// protected method to create the one and only instance of the class
        /// </summary>
        /// <returns></returns>
        protected T GetInstance()
        {
            if (_instance == null)
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                        _instance = new T();
                }
            }
            return _instance;
        }
        /// <summary>
        /// property to get the one and only instance of the class
        /// </summary>
        public T Instance
        {
            get { return GetInstance(); }
        }

        private volatile static T _instance;
        private object _lockObj = new object();
    }
}
