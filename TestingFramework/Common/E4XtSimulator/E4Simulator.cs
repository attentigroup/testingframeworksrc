﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.E4XtSimulator
{
    public class E4Simulator
    {
        string simulator = "E4Simulator.exe";
        private string _args = String.Empty;
        private readonly string _hmruId;
        string DCCIp = "10.10.8.22";//take from config
        string DCCPort = "8001";//take from config
        string aliveTime = "20";
        //ConfigurationManager.AppSettings["DB_HostName"]
        public E4Simulator(string hmruId)
        {
            _hmruId = hmruId;
            _args = " -DCC " + DCCIp + " -PORT " + DCCPort + " -UNIT " + _hmruId;// + "; -SHUTDOWNTIMER " + aliveTime;
        }
        
        //String xt_simulator = xt_simulatorLocation + "E4Simulator.exe" + " -DCC " + DCCIp + " -PORT " + DCCPort + " -UNIT " + hmruid + " -SHUTDOWNTIMER " + aliveTime;
        public int RunSimulator()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo(simulator);
            startInfo.WorkingDirectory = Environment.CurrentDirectory + "\\E4Simulator";
            startInfo.Arguments = _args;
            var p = Process.Start(startInfo);

            p.WaitForExit();

            WebTests.SeleniumWrapper.Logger.WriteLine($"Running E4Simulator with args: {_args} ; Exid code is {p.ExitCode}");

            return p.ExitCode;
        }
    }
}
