﻿using System;
using System.Data;
using System.Data.Odbc;

namespace Common.CustomAttributes
{
    public class OdbcColumnNameAttribute : Attribute
    {
        public OdbcColumnNameAttribute(string columnName)
        {
            ColumnName = columnName;
        }
        public string ColumnName { get; set; }
    }
    public class OdbcColumnIgnoreAttribute : Attribute
    {
        public OdbcColumnIgnoreAttribute(bool ignore)
        {
            Ignore = ignore;
        }
        public bool Ignore{ get; set; }
    }

    public class OdbcSpParamAttribute : Attribute
    {
        public OdbcSpParamAttribute(ParameterDirection parameterDirection, OdbcType odbcType)
        {
            ParameterDirection = parameterDirection;
            OdbcType = odbcType;
            Size = 1;
        }

        public OdbcSpParamAttribute(ParameterDirection parameterDirection, OdbcType odbcType, int size)
        {
            ParameterDirection = parameterDirection;
            OdbcType = odbcType;
            Size = size;
        }
        public ParameterDirection ParameterDirection { get; set; }
        public OdbcType OdbcType { get; set; }
        public int Size{ get; set; }

    }


}
