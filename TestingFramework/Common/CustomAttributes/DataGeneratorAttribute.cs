﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomAttributes
{
    /// <summary>
    /// data generator attrubute will use to identify the data generator property type that the generator will generate
    /// </summary>
    public class DataGeneratorAttribute : Attribute
    {
        /// <summary>
        /// constractor for the data generator attribute
        /// </summary>
        /// <param name="propertyType"></param>
        public DataGeneratorAttribute(EnumPropertyType propertyType)
        {
            PropertyType = propertyType;
        }

        /// <summary>
        /// the property type of the data generator
        /// </summary>
        public EnumPropertyType PropertyType { get; set; }

    }
}
