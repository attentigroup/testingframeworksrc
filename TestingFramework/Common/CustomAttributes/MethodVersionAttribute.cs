﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MethodVersionAttribute : Attribute
    {
        public const string EARLIEST_VERSION = "Earliest";
        public const string LATEST_VERSION = "Latest";

        public MethodVersionAttribute( string minVersion, string maxVersion)
        {
            MinVersion = minVersion;
            MaxVersion = maxVersion;
        }

        public string MinVersion { get; set; }
        public string MaxVersion { get; set; }

        private static int SIZE_OF_BYTE = 8;

        private uint _minVersionInt = 0;
        public uint MinVersionInt
        {
            get
            {
                if (_minVersionInt == 0)
                {
                    _minVersionInt = GetIntVersion(MinVersion);
                }
                return _minVersionInt;
            }
            private set { }
        }

        private uint _maxVersionInt = 0;
        public uint MaxVersionInt
        {
            get
            {
                if (_maxVersionInt == 0)
                {
                    _maxVersionInt = GetIntVersion(MaxVersion);
                }
                return _maxVersionInt;
            }
            private set { }
        }



        public static uint GetIntVersion(string versionString)
        {
            uint versionInt = 0;

            switch (versionString)
            {
                case EARLIEST_VERSION:
                    versionInt = uint.MinValue;
                    break;
                case LATEST_VERSION:
                    versionInt = uint.MaxValue;
                    break;
                default://real version
                    {
                        var version = new Version(versionString);
                        byte[] bytes = { (byte)version.Major, (byte)version.Minor, (byte)version.Build, (byte)version.Revision };
                        // If the system architecture is little-endian (that is, little end first),
                        // reverse the byte array.
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(bytes);

                        versionInt = BitConverter.ToUInt32(bytes, 0);
                    }
                    break;
            }
            return versionInt;
        }
    }
}
