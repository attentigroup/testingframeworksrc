﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomAttributes
{
    /// <summary>
    /// An attribute for the blocks properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PropertyTestAttribute : Attribute
    {
        /// <summary>
        /// constractor for the property test attribute
        /// </summary>
        /// <param name="name"></param>
        /// <param name="propertyType"></param>
        /// <param name="propertyModifier"></param>
        public PropertyTestAttribute(   EnumPropertyName name, 
                                        EnumPropertyType propertyType =         EnumPropertyType.None,
                                        EnumPropertyModifier propertyModifier = EnumPropertyModifier.None)
        {
            Name = name;
            PropertyType = propertyType;
            PropertyModifier = propertyModifier;
        }
        /// <summary>
        /// The PropertyName - the framework identify the properties when passing the data between the blocks and within the blocks.
        /// </summary>
        public EnumPropertyName Name { get; private set; }
        /// <summary>
        /// The PropertyType is for the Data Generator process to match the property the right data generator.
        /// </summary>
        public EnumPropertyType PropertyType { get; private set; }
        /// The PropertyModifier is for determining how the framework handle the property regarding setting a value for it
        public EnumPropertyModifier PropertyModifier { get; set; }
    }
}
