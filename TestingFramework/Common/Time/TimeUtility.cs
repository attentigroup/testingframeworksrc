﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Time
{
    public class TimeUtility
    {
        public static DateTime BaseEPocTime = DateTime.Parse("2000-01-01").ToUniversalTime();

        /// <summary>
        /// return EPOC time in seconds
        /// </summary>
        /// <returns></returns>
        public static uint GetEpoch()
        {
            return (uint)(DateTime.UtcNow - BaseEPocTime).TotalSeconds;
        }
    }
}
