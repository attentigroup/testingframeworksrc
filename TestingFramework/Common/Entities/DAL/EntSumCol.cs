﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities.DAL
{
    public class SUM_COL
    {
        [OdbcColumnName("Total Violations")]
        public int TotalViolations { get; set; }
    }
}
