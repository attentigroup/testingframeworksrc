﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Versioning
{
    /// <summary>
    /// the values of all web version that automation support
    /// </summary>
    public class WebVersions
    {
        public const string WEB_VERSION_11_10_0_99 = "11.10.0.99";
        public const string WEB_VERSION_11_10_0_1 = "11.10.0.1";
        public const string WEB_VERSION_11_10_0_2 = "11.10.0.2";
        public const string WEB_VERSION_11_11_0_0 = "11.11.0.0";
    }
}
