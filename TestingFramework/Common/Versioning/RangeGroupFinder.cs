﻿using System.Collections.Generic;
using System.Reflection;

namespace Common.Versioning
{
    /// <summary>
    /// Range group base class describe group by it's low anf high values
    /// </summary>
    public class RangeGroupBase
    {
        /// <summary>
        /// range group id 
        /// </summary>
        public uint RangeGroupId { get; set; }
        /// <summary>
        /// low value of the group
        /// </summary>
        public uint Low { get; set; }
        /// <summary>
        /// high value of the group
        /// </summary>
        public uint High { get; set; }

    }

    /// <summary>
    /// implementation of range group for web tests(can fit every test)
    /// </summary>
    public class WebRangeGroup : RangeGroupBase
    {
        /// <summary>
        /// method to invoke for specific version.
        /// </summary>
        public MethodInfo Method4Version { get; set; }
    }

    /// <summary>
    /// implementation of the algorithem to find the specific range for specific value
    /// </summary>
    public class RangeGroupFinder
    {
        private List<RangeGroupBase> _rangeGroups = new List<RangeGroupBase>();

        static RangeGroupFinder(){}

        /// <summary>
        /// add group to the list of groups that the algorithem will look the range in.
        /// </summary>
        /// <param name="rgb">range group base class to add to the list</param>
        public void AddGroup(RangeGroupBase rgb)
        {
            _rangeGroups.Add(rgb);
        }

        /// <summary>
        /// return the suitable RangeGroupBase instance for the number value supplied or null
        /// </summary>
        /// <param name="number">value to find range that suitable for</param>
        /// <returns>RangeGroupBase instance of null</returns>
        public RangeGroupBase Find(uint number)
        {
            _rangeGroups.Sort((x, y) => x.Low.CompareTo(y.Low));

            RangeGroupBase rangeGroupBase = null;

            for (int i = 0; i < _rangeGroups.Count; i++)
            {
                if (_rangeGroups[i].High > number)
                {
                    rangeGroupBase = _rangeGroups[i];
                    break;
                }
            }
            return rangeGroupBase;
        }
    }
}
