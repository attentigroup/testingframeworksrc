﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Common.XTSimulator
{
    public class XTSimulator
    {
        private readonly string _args;
        private readonly string EquipmentID;

        /// <summary>
        /// Pass null for the non-needed parameter
        /// </summary>
        /// <param name="args"></param>
        /// <param name="equipmentID"></param>
        public XTSimulator(string args, string equipmentID)
        {
            _args = args;
            if (_args == null)
                _args = GetDefaultArgs(equipmentID);
        }
        

        private string GetDefaultArgs(string equipmentID)
        {
            //TODO return default state args
            var DB_IP = ConfigurationManager.AppSettings["DB_HostName"];//Server in simulator
            var DB_Port = ConfigurationManager.AppSettings["DB_Port"];
            var Vcomm_IP = ConfigurationManager.AppSettings["GpsDeviceServerIP"];
            var Vcomm_Port = ConfigurationManager.AppSettings["GpsDeviceServerPort"];
            var SHUTDOWNTIMER = "60000";
            var Rule = "Device_Tamper-first_violation;Device_Tamper-restore";
            var LBS = "false";
            var NUMPOINTS = "5";
            var ACKCOUNT = "2";
            var EquipmentID = equipmentID;
            var args = string.Format("-DBSERVERIP {0} -DBPORT {1} -VCOMM {2} -PORT {3} -UNIT {4} -SHUTDOWNTIMER {5} -RULE {6} -LBS {7} -NUMPOINTS {8} -ACKCOUNT {9}", DB_IP, DB_Port, Vcomm_IP, Vcomm_Port, EquipmentID, SHUTDOWNTIMER, Rule, LBS, NUMPOINTS, ACKCOUNT);
            return args;
        }

        public int RunSimulator()
        {
            string dir = System.AppDomain.CurrentDomain.BaseDirectory;

            dir = Path.Combine(dir, @"DevicesBlocks\xt_simulator");

            var simulator = "XT_Simulator.exe";
            
            Process process = new Process();
            process.StartInfo.WorkingDirectory = dir;
            process.StartInfo.FileName = simulator;
            process.StartInfo.Arguments = _args;
            process.Start();
            process.WaitForExit();

            WebTests.SeleniumWrapper.Logger.WriteLine($"Running XTSimulator with args: {_args} ; Exid code is {process.ExitCode}");
            
            return process.ExitCode;

        }
    }
}
