﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebTests.SeleniumWrapper
{
    public class Wait
    {
        public static void Until(Func<bool> func, string message = "", int seconds = 30, bool failIfConditionNotMet = true, int millisecondInterval = 50)
        {
            var toTime = DateTime.Now.AddSeconds(seconds);
            bool result = false;
            do
            {
                result = func.Invoke();
                Thread.Sleep(millisecondInterval);
            } while (result == false && DateTime.Now < toTime);

            if (result == true)
                return;
            
            if(failIfConditionNotMet == true)
            throw new Exception($"Wait condition was not met for {seconds} seconds; " + message);
        }
    }
}
