﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace WebTests.SeleniumWrapper
{
    public abstract class ElementWrapper
    {
        protected IWebDriver _webDriver;
        protected IWebElement _webElement;
        private readonly BrowserName _browserName;
        private readonly string _elementDescription;
        protected const int _defaultTimeout = 30;

        public string ElementDescription => _elementDescription;

        public BrowserName BrowserName => _browserName;

        public ElementWrapper(IWebDriver webDriver, BrowserName browserName, int defaultTimeout)
        {
            _webDriver = webDriver;
            _browserName = browserName;
        }

        public ElementWrapper(IWebElement webElement, string elementDescription, BrowserName browserName, int defaultTimeout)
        {
            _webElement = webElement;
            _elementDescription = elementDescription;
            _browserName = browserName;
        }

        public BrowserElement WaitForElement(By by, string elementDescription, int seconds = _defaultTimeout)
        {
            IWebElement webElement = null;

            var wait = GetWebDriverWait(seconds);

            if (wait == null)
                throw new ArgumentNullException("WebDriver");

            try
            {
                webElement = (_webElement == null) ? wait.Until(d => d.FindElement(by)) : wait.Until(d => _webElement.FindElement(by));
            }
            catch (Exception e)
            {
                Logger.WriteLine($"{elementDescription} not found");
                throw e;
            }
            return new BrowserElement(_webDriver, webElement, elementDescription, _browserName);
        }

        /// <summary>
        /// Set minElementsToGet:0 to avoid fail exception when list is empty
        /// </summary>
        /// <param name="by"></param>
        /// <param name="description"></param>
        /// <param name="loadElementsTimeout"></param>
        /// <param name="explicitWaitForSingleElement"></param>
        /// <param name="minElementsToGet"></param>
        /// <returns></returns>
        public List<BrowserElement> WaitForElements(By by, string description, int loadElementsTimeout = _defaultTimeout, int explicitWaitForSingleElement = 0, int minElementsToGet = 1)
        {
            explicitWaitForSingleElement = (minElementsToGet == 0 && explicitWaitForSingleElement == 0) ? 1 : explicitWaitForSingleElement;
            var wait = GetWebDriverWait(explicitWaitForSingleElement);

            if (wait == null)
                throw new ArgumentNullException("WebDriver");

            var toTime = DateTime.Now.AddSeconds(loadElementsTimeout);
            ReadOnlyCollection<IWebElement> webElements;
            List<BrowserElement> browserElements;

            do
            {
                //This might be the only place that 'FindElements' is required. Don't replace it! 
                webElements = (_webElement == null)
                    ? wait.Until(d => d.FindElements(by))
                    : wait.Until(d => _webElement.FindElements(by));

                Thread.Sleep(50);
            } while (webElements.Count < minElementsToGet && DateTime.Now < toTime);

            browserElements = webElements.Select(w => new BrowserElement(_webDriver, w, description, _browserName)).ToList();

            if (browserElements.Any() && browserElements.Count < minElementsToGet)
            {
                string message = $"Found {browserElements.Count} out of the expected {minElementsToGet} for {description}";
                Logger.Fail(message);
                throw new Exception(message);
            }
            else if (!browserElements.Any() && minElementsToGet > 0)
                throw new Exception($"Element {description} not found");

            return browserElements;
        }

        protected WebDriverWait GetWebDriverWait(int seconds = _defaultTimeout)
        {
            return new WebDriverWait(_webDriver, TimeSpan.FromSeconds(seconds));
        }

        public BrowserElement WaitForElementByAttributeValue(By by, string attributeName, string containsAttributeValue, bool IsExactValue = false, bool failIfNoValue = true, int seconds = _defaultTimeout)
        {
            BrowserElement element;
            List<BrowserElement> elements = WaitForElements(by, "Element with attribute: " + attributeName);

            if (IsExactValue == true)
                element = elements.Find(e => e.WaitGetAttribute(attributeName, seconds: seconds, failIfNoValue: failIfNoValue).Equals(containsAttributeValue));
            else
                element = elements.Find(e => e.WaitGetAttribute(attributeName.ToLower(), seconds: seconds, failIfNoValue: failIfNoValue).Contains(containsAttributeValue.ToLower()));

            return element;
        }

        public virtual void SwitchToMainWIndow()
        {
            var el = (IWebElement)((IJavaScriptExecutor)_webDriver).ExecuteScript("return window.frameElement");
            if (el != null)
            {
                Logger.WriteLine($"Switching to Main Window");
                _webDriver.SwitchTo().DefaultContent();
            }
        }

        public void SwitchToLastWindow()
        {
            Logger.WriteLine($"Switching to Last Window");
            Wait.Until(() => GetAllWindows().Count > 1, "There is only a single window for this browser");
            _webDriver.SwitchTo().Window(GetAllWindows().Last());
        }

        private ReadOnlyCollection<string> GetAllWindows()
        {
            return _webDriver.WindowHandles;
        }

        protected Actions GetActionsInteraction()
        {
            Actions actions = new Actions(_webDriver);

            return actions;
        }

        /// <summary>
        /// Waits for the cursor to return from the 'wait' state. When timeout is reached only a warning is registered
        /// </summary>
        /// <param name="seconds"></param>
        public void WaitForNotBusyCursor(int seconds = 30)
        {
            try
            {
                Wait.Until(() => GetCursorState() != "wait", "Cursor is busy", seconds);
            }
            catch
            {
                Logger.Warning($"Cursor was still in *Wait* state after {seconds} seconds");
            }
        }

        /// <summary>
        /// returns the cursor state: wait, pointer etc.
        /// </summary>
        /// <returns></returns>
        private string GetCursorState()
        {
            var cursor = _webDriver.FindElement(By.TagName("body")).GetCssValue("cursor");
            return cursor;
        }
    }
}
