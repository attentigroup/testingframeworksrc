﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;

namespace WebTests.SeleniumWrapper
{
    public class Browser : ElementWrapper, IWebDriver, IDisposable
    {
        //private readonly string _browserDescription;
        public Browser(IWebDriver webDriver, BrowserName browserName, int defaultTimeout) : base(webDriver, browserName, defaultTimeout)
        {
        }

        public IWebDriver GetWebDriver()
        {
            return _webDriver;
        }

        public string Url
        {
            get
            {
                return _webDriver.Url;
            }

            set
            {
                _webDriver.Url = value;
            }
        }

        public string Title
        {
            get
            {
                return _webDriver.Title;
            }
        }

        public string PageSource
        {
            get
            {
                return _webDriver.PageSource;
            }
        }

        public string CurrentWindowHandle
        {
            get
            {
                return _webDriver.CurrentWindowHandle;
            }
        }

        public ReadOnlyCollection<string> WindowHandles
        {
            get
            {
                return _webDriver.WindowHandles;
            }
        }

        public void NavigateToUrl(string url)
        {
            _webDriver.Navigate().GoToUrl(url);
            Logger.WriteLine($"Navigating to: {url}");
        }

        public void RefreshPage()
        {
            Logger.WriteLine($"Refreshing {_webDriver.Title}");
            _webDriver.Navigate().Refresh();
        }

        INavigation IWebDriver.Navigate()
        {
            return _webDriver.Navigate();
        }

        public void Close()
        {
            _webDriver.Close();
        }

        public void Quit()
        {
            _webDriver.Quit();
        }

        public void DeleteAllCookies()
        {
            _webDriver.Manage().Cookies.DeleteAllCookies();
        }

        public IOptions Manage()
        {
            return _webDriver.Manage();
        }

        public void SwitchToDefaultFrame()
        {
            _webDriver.SwitchTo().DefaultContent();
        }

        [Obsolete("Use specific SwithTo methods like 'SwitchToDefaultFrame' 'SwitchToThisFrame' etc. instead")]
        public ITargetLocator SwitchTo()
        {
            return _webDriver.SwitchTo();
        }

        [Obsolete("Use 'WaitForElement' instead of 'FindElement'")]
        IWebElement ISearchContext.FindElement(By by)
        {
            return _webDriver.FindElement(by);
        }

        [Obsolete("Using 'WaitForElements' is preferable than 'FindElements'.")]
        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return _webDriver.FindElements(by);
        }
        
        public void Dispose()
        {
            _webDriver.Quit();
            _webDriver.Dispose();
        }
    }
}
