﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Threading;

namespace WebTests.SeleniumWrapper
{
    public class BrowserElement : ElementWrapper, IWebElement
    {
        public BrowserElement(IWebDriver webDriver, IWebElement webElement, string elementDescription, BrowserName browserName, int defaultTimeout = _defaultTimeout) : base(webElement, elementDescription, browserName, defaultTimeout)
        {
            _webElement = webElement;
            _webDriver = webDriver;
        }


        /// <summary>
        /// Get the Selenium object of type IWebElement
        /// </summary>
        /// <returns></returns>
        public IWebElement GetWebElement()
        {
            return _webElement;
        }

        public void WaitToDisappear(int seconds = _defaultTimeout)
        {
            WebDriverWait wait = GetWebDriverWait();

            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));

            var elementNotVisible = new Func<IWebElement, bool>((IWebElement Web) =>
            {
                if (_webElement == null || !_webElement.Displayed)
                    return true;
                else
                    return false;
            });

            wait.Until(d => elementNotVisible);
        }

        public void WaitToBeClickable(int seconds = 7)
        {
            try
            {
                GetWebDriverWait(seconds).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(_webElement));
            }
            catch
            {
                Logger.WriteLine($"WaitToBeClickable on {ElementDescription} faild (timed-out aftrer {seconds} seconds)");
            }
        }

        public void WaitForStailnessOfElement(int seconds = _defaultTimeout)
        {
            GetWebDriverWait().Until(SeleniumExtras.WaitHelpers.ExpectedConditions.StalenessOf(_webElement));
        }

        public IWebDriver GetWebDriverFromElement(IWebElement e)
        {
            //return ((IWrapsDriver)e).WrappedDriver;
            return _webDriver;
        }

        public bool Displayed
        {
            get
            {
                return _webElement.Displayed;
            }
        }

        public bool Enabled
        {
            get
            {
                return _webElement.Enabled;
            }
        }

        public Point Location
        {
            get
            {
                return _webElement.Location;
            }
        }

        public bool Selected
        {
            get
            {
                return _webElement.Selected;
            }
        }

        public Size Size
        {
            get
            {
                return _webElement.Size;
            }
        }

        public string TagName
        {
            get
            {
                return _webElement.TagName;
            }
        }

        public string Text
        {
            set
            {
                _webElement.Clear();
                _webElement.SendKeys(value);
                Logger.WriteLine($"Writing '{value}' into {ElementDescription}");
            }
            get
            {
                return _webElement.Text;
            }
        }

        public void Clear()
        {
            _webElement.Clear();
        }

        /// <summary>
        /// Sends number of Backspace strokes when 'Clear()' is not working
        /// </summary>
        /// <param name="numOfChars"></param>
        public void Clear(int numOfChars)
        {
            for (int i = 0; i < numOfChars; i++)
            {
                _webElement.SendKeys(Keys.Backspace);
            }
        }

        public void Click()
        {
            //if (Browser.GetBrowserName().Equals(BrowserName.Edge))
            //{
            //    IJavaScriptExecutor jse = _webDriver as IJavaScriptExecutor;
            //    jse.ExecuteScript("arguments[0].click();", _webElement);
            //}
            //else
            _webElement.Click();
            Logger.WriteLine($"Clicked on: {ElementDescription}");
            //Taking a screenshot after every Click
            //Logger.TakeScreenshot();
        }

        /// <summary>
        /// Use only when regular Click() succeeding but throwing an exception 
        /// </summary>
        /// <param name="seconds"></param>
        public void ClickSafely()
        {
            var toTime = DateTime.Now.AddSeconds(5);
            WaitToBeClickable();

            do
            {
                try
                {
                    if (BrowserName == BrowserName.Firefox)
                    {
                        GetActionsInteraction().MoveToElement(_webElement).Click().Build().Perform();
                    }
                    else
                        _webElement.Click();
                    //Logger.TakeScreenshot();
                    Logger.WriteLine($"Clicked Safely on '{ElementDescription}': success!");
                    break;
                }
                catch (Exception ex)
                {
                    //Logger.TakeScreenshot();
                    Logger.WriteLine($"ClickSafely message: clicking on '{ElementDescription}' throw exception: {ex}");
                }
                Thread.Sleep(250);
            } while (DateTime.Now < toTime);
        }

        public void DoubleClick()
        {
            var actions = GetActionsInteraction();

            actions.MoveToElement(_webElement).DoubleClick().Build().Perform();

            Logger.TakeScreenshot();
            Logger.WriteLine($"Double clicked on {ElementDescription}");
        }

        public void MoveCursorToElement(bool isClick = false)
        {
            IAction moveAction;
            var actionsWrapper = GetActionsInteraction();

            if (isClick)
            {
                moveAction = actionsWrapper.MoveToElement(_webElement).Click().Build();
            }
            else
                moveAction = actionsWrapper.MoveToElement(_webElement).Build();

            moveAction.Perform();

            if (isClick == true)
                Logger.WriteLine($"Clicked on: {ElementDescription}");
            else
                Logger.WriteLine($"Mouse moved to: {ElementDescription}");
        }

        public void SwitchToThisFrame()
        {
            _webDriver.SwitchTo().Frame(_webElement);
        }

        [Obsolete("Use 'WaitForElement' instead of 'FindElement")]
        IWebElement ISearchContext.FindElement(By by)
        {
            return _webElement.FindElement(by);
        }

        [Obsolete("Use 'WaitForElements' instead of 'FindElements'")]
        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return _webElement.FindElements(by);
        }

        /// <summary>
        /// If emtpty result is acceptable change failIfNoValue to false
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public string WaitGetAttribute(string attributeName, int seconds = _defaultTimeout, bool failIfNoValue = true)
        {
            seconds = (failIfNoValue == false && seconds == _defaultTimeout) ? 1 : seconds;

            var toTime = DateTime.Now.AddSeconds(seconds);
            string attrVal = "";
            do
            {
                attrVal = _webElement.GetAttribute(attributeName);
            } while (string.IsNullOrEmpty(attrVal) && DateTime.Now < toTime);

            if (string.IsNullOrEmpty(attrVal) && failIfNoValue == true)
                throw new Exception($"Attribute '{attributeName}' not exists or empty");

            if (failIfNoValue)
                Logger.WriteLine($"Attribute {attributeName} value is {attrVal}");

            return attrVal ?? "";
        }


        [Obsolete("Use WaitGetAttribute instead")]
        public string GetAttribute(string attributeName)
        {
            return _webElement.GetAttribute(attributeName);
        }

        public string GetCssValue(string propertyName)
        {
            return _webElement.GetCssValue(propertyName);
        }

        [Obsolete("Use 'element.Text =' instead")]
        public void SendKeys(string text)
        {
            _webElement.SendKeys(text);
        }

        public void Submit()
        {
            _webElement.Submit();
        }

        public string GetProperty(string propertyName)
        {
            throw new NotImplementedException();
        }
    }
}
