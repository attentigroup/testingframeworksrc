﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;

namespace WebTests.SeleniumWrapper
{
    public class AssertLogger
    {
        private static void WriteMessageToLog(string message)
        {
            Logger.WriteLine(message);
        }


        public static void AreEqual(double expected, double actual, double delta, string message, params object[] parameters)
        {
            Assert.AreEqual(expected, actual, delta, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreEqual(float expected, float actual, float delta, string message, params object[] parameters)
        {
            Assert.AreEqual(expected, actual, delta, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreEqual(object expected, object actual, string message, params object[] parameters)
        {
            Assert.AreEqual(expected, actual, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreEqual(object expected, object actual, string message)
        {
            Assert.AreEqual(expected, actual, message);
            WriteMessageToLog(message);
        }

        public static void AreEqual<T>(T expected, T actual, string message, params object[] parameters)
        {
            Assert.AreEqual<T>(expected, actual, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreEqual<T>(T expected, T actual, string message)
        {
            Assert.AreEqual<T>(expected, actual, message);
            WriteMessageToLog(message);
        }

        public static void AreEqual(double expected, double actual, double delta, string message)
        {
            Assert.AreEqual(expected, actual, delta, message);
            WriteMessageToLog(message);
        }

        public static void AreEqual(float expected, float actual, float delta, string message)
        {
            Assert.AreEqual(expected, actual, delta, message);
            WriteMessageToLog(message);
        }



        public static void AreEqual(string expected, string actual, bool ignoreCase, string message)
        {
            Assert.AreEqual(expected, actual, ignoreCase, message);
            WriteMessageToLog(message);
        }

        public static void AreEqual(string expected, string actual, bool ignoreCase, string message, params object[] parameters)
        {
            Assert.AreEqual(expected, actual, ignoreCase, message, parameters);
            WriteMessageToLog(message);
        }


        public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo culture, string message)
        {
            Assert.AreEqual(expected, actual, ignoreCase, culture, message);
            WriteMessageToLog(message);
        }

        public static void AreEqual(string expected, string actual, bool ignoreCase, CultureInfo culture, string message, params object[] parameters)
        {
            Assert.AreEqual(expected, actual, ignoreCase, culture, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(double notExpected, double actual, double delta, string message)
        {
            Assert.AreNotEqual(notExpected, actual, delta, message);
            WriteMessageToLog(message);
        }



        public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, string message, params object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, string message)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, message);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(float notExpected, float actual, float delta, string message, params object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, delta, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(float notExpected, float actual, float delta, string message)
        {
            Assert.AreNotEqual(notExpected, actual, delta, message);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(double notExpected, double actual, double delta, string message, params object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, delta, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo culture, string message, params object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, culture, message, parameters);
            WriteMessageToLog(message);
        }


        public static void AreNotEqual(string notExpected, string actual, bool ignoreCase, CultureInfo culture, string message)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, culture, message);
            WriteMessageToLog(message);
        }


        public static void AreNotEqual(object notExpected, object actual, string message)
        {
            Assert.AreNotEqual(notExpected, actual, message);
            WriteMessageToLog(message);
        }


        public static void AreNotEqual<T>(T notExpected, T actual, string message)
        {
            Assert.AreNotEqual<T>(notExpected, actual, message);
            WriteMessageToLog(message);
        }

        public static void AreNotEqual(object notExpected, object actual, string message, params object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, message, parameters);
            WriteMessageToLog(message);
        }


        public static void AreNotEqual<T>(T notExpected, T actual, string message, params object[] parameters)
        {
            Assert.AreNotEqual<T>(notExpected, actual, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreNotSame(object notExpected, object actual, string message, params object[] parameters)
        {
            Assert.AreNotSame(notExpected, actual, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreNotSame(object notExpected, object actual, string message)
        {
            Assert.AreNotSame(notExpected, actual, message);
            WriteMessageToLog(message);
        }


        public static void AreSame(object expected, object actual, string message, params object[] parameters)
        {
            Assert.AreSame(expected, actual, message, parameters);
            WriteMessageToLog(message);
        }

        public static void AreSame(object expected, object actual, string message)
        {
            Assert.AreSame(expected, actual, message);
            WriteMessageToLog(message);
        }



        public static void Fail(string message, params object[] parameters)
        {
            Assert.Fail(message, parameters);
            WriteMessageToLog(message);
        }

        public static void Fail(string message)
        {
            Assert.Fail(message);
            WriteMessageToLog(message);
        }


        public static void Inconclusive(string message, params object[] parameters)
        {
            Assert.Inconclusive(message, parameters);
            WriteMessageToLog(message);
        }

        public static void Inconclusive(string message)
        {
            Inconclusive(message);
            WriteMessageToLog(message);
        }

        public static void IsFalse(bool condition, string message, params object[] parameters)
        {
            Assert.IsFalse(condition, message, parameters);
            WriteMessageToLog(message);
        }

        public static void IsFalse(bool condition, string message)
        {
            Assert.IsFalse(condition, message);
            WriteMessageToLog(message);
        }


        public static void IsInstanceOfType(object value, Type expectedType, string message, params object[] parameters)
        {
            Assert.IsInstanceOfType(value, expectedType, message, parameters);
            WriteMessageToLog(message);
        }

        public static void IsInstanceOfType(object value, Type expectedType, string message)
        {
            Assert.IsInstanceOfType(value, expectedType, message);
            WriteMessageToLog(message);
        }



        public static void IsNotInstanceOfType(object value, Type wrongType, string message)
        {
            Assert.IsNotInstanceOfType(value, wrongType, message);
            WriteMessageToLog(message);
        }

        public static void IsNotInstanceOfType(object value, Type wrongType, string message, params object[] parameters)
        {
            Assert.IsNotInstanceOfType(value, wrongType, message, parameters);
            WriteMessageToLog(message);
        }


        public static void IsNotNull(object value, string message)
        {
            Assert.IsNotNull(value, message);
            WriteMessageToLog(message);
        }

        public static void IsNotNull(object value, string message, params object[] parameters)
        {
            Assert.IsNotNull(value, message, parameters);
            WriteMessageToLog(message);
        }

        public static void IsNull(object value, string message)
        {
            Assert.IsNull(value, message);
            WriteMessageToLog(message);
        }

        public static void IsNull(object value, string message, params object[] parameters)
        {
            Assert.IsNull(value, message, parameters);
            WriteMessageToLog(message);
        }


        public static void IsTrue(bool condition, string message)
        {
            Assert.IsTrue(condition, message);
            WriteMessageToLog(message);
        }


        public static void IsTrue(bool condition, string message, params object[] parameters)
        {
            Assert.IsTrue(condition, message, parameters);
            WriteMessageToLog(message);
        }


    }
}
