﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.SeleniumWrapper
{
    public static class Performance
    {
        public static void StartPoint(PerformanceTag performanceTag)
        {
            Logger.WriteLine("@>" + performanceTag);
        }

        public static void EndPoint(PerformanceTag performanceTag)
        {
            Logger.WriteLine("@|" + performanceTag);
        }
    }

    public enum PerformanceTag
    {
        Startup,
        Login,
        GotoOffender,
        UsersList
    }
}

