﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.SeleniumWrapper
{
    public static class Logger
    {
        static Action<string> _LoggerImpl;
        static Action<string> _LoggerFailImpl;
        static Action<string> _LoggerWarningImpl;
        static Action _LoggerTakeScreenshotImpl;
        static Action<string,string> _LoggerSysInfoImpl;

        private static List<string> LogStack;


        public static void Initialize(Action<object> loggerImpl)
        {
            _LoggerImpl = loggerImpl;
        }

        public static void InitializeTakeScreenshot(Action loggerTakeScreenshotImpl)
        {
            _LoggerTakeScreenshotImpl = loggerTakeScreenshotImpl;
        }

        public static void InitializeFail(Action<object> loggerFailImpl)
        {
            _LoggerFailImpl = loggerFailImpl;
        }

        public static void InitializeWarning(Action<object> loggerWarningImpl)
        {
            _LoggerWarningImpl = loggerWarningImpl;
        }

        public static void InitializeSysInfo(Action<string,string>loggerSysInfoImpl)
        {
            _LoggerSysInfoImpl = loggerSysInfoImpl;
        }

        public static void WriteLine(string line)
        {
            if (_LoggerImpl == null)
                WriteToLoggerStack(line);
            else if(LogStack != null)
                FlushLogStack();

            _LoggerImpl?.Invoke(line);
        }
        
        public static void WriteLine(object text)
        {
            WriteLine(text.ToString());
        }

        public static void WriteLine(object text, params object[] args)
        {
            var line = string.Format(text.ToString(), args);
            WriteLine(line);
        }

        public static void Fail(string line)
        {
            _LoggerFailImpl?.Invoke(line);
        }

        public static void Fail(object text, params object[] args)
        {
            var line = string.Format(text.ToString(), args);
            Fail(line);
        }

        public static void Fail(object text)
        {
            Fail(text.ToString());
        }

        public static void Warning(string line)
        {
            _LoggerWarningImpl?.Invoke(line);
        }

        public static void SysInfo(string parameter, string value)
        {
            _LoggerSysInfoImpl?.Invoke(parameter, value);
        }

        public static void Warning(string text, params object[] args)
        {
            Warning(string.Format(text, args));
        }

        public static void TakeScreenshot()
        {
            _LoggerTakeScreenshotImpl?.Invoke();
        }

        public static void Dispose()
        {
            _LoggerImpl = null;
            _LoggerFailImpl = null;
            _LoggerWarningImpl = null;
            _LoggerTakeScreenshotImpl = null;
        }

        private static void FlushLogStack()
        {
            if (LogStack != null && LogStack.Any())
            {
                foreach (var line in LogStack)
                {
                    _LoggerImpl?.Invoke(line);
                }
                LogStack = null;
            }
        }

        private static void WriteToLoggerStack(string line)
        {
            if (LogStack == null)
                LogStack = new List<string>();

            LogStack.Add($"{DateTime.Now.ToString("HH:mm:ss")}  {line}");
        }
    }
}
