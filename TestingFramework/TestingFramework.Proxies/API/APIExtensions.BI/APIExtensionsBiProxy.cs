﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.BI;

namespace TestingFramework.Proxies.API.APIExtensions.BI
{
    public class APIExtensionsBiProxy : BaseProxy, IBI
    {
        #region Consts
        protected const string APIExtensionsServicesToken = "APIExtensionsServices";
        protected const string APIExtensionsBiSvcPathToken = "BiSvcPath";
        protected const string BiSvcEndpointConfigurationNameToken = "BiSvcEndpointConfigurationName";

        protected const string BiTokenHeaderName = "BIToken";
        #endregion Consts

        #region Properties
        private BIClient BiClient { get; set; }

        #endregion Properties

        #region Singleton
        private static APIExtensionsBiProxy _biProxy = null;
        public static APIExtensionsBiProxy Instance
        {
            get
            {
                if (_biProxy == null)
                {
                    _biProxy = new APIExtensionsBiProxy();
                }

                return _biProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private APIExtensionsBiProxy()
        {
            Init();
        }

        private void Init()
        {
            string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
            string APIExtensionsServices = ConfigurationManager.AppSettings[APIExtensionsServicesToken];
            string apiExtensionsSvcPath = ConfigurationManager.AppSettings[APIExtensionsBiSvcPathToken];
            string apiExtensionsSvcEndpointConfigurationName = ConfigurationManager.AppSettings[BiSvcEndpointConfigurationNameToken];

            string fullUri = svcUri + APIExtensionsServices + apiExtensionsSvcPath;

            EndpointAddress fullSvcEndpoint = new EndpointAddress(fullUri);
            BiClient = new BIClient(apiExtensionsSvcEndpointConfigurationName, fullSvcEndpoint);


            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };

            #region add headers to every call

            var biTokenFromConfig = ConfigurationManager.AppSettings[BiTokenHeaderName];
            var biToken = string.IsNullOrEmpty(biTokenFromConfig) == true ? "34BE4588BF4BF9AC8C7A3EA5EA532" : biTokenFromConfig;
            Headers.Add(BiTokenHeaderName, biToken);

            #endregion add headers to every call

            CheckConectivity();
        }


        private void CheckConectivity()
        {
            try
            {
                GetStates();
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException != null)
                {
                    if (mse.InnerException is FaultException)
                    {
                        FaultException fe = mse.InnerException as FaultException;
                        throw new Exception(fe.Message, fe);
                    }
                }
            }
        }
        #endregion Proxy init

        #region BI API Implementation

        public List<EntBICellularServiceProvider> GetCellularServiceProvider()
        {
            return ((IBI)BiClient).GetCellularServiceProvider();
        }

        public List<EntBISmsgatewayProviders> GetSmsgatewayProviders()
        {
            return ((IBI)BiClient).GetSmsgatewayProviders();
        }

        public List<EntBILanguage> GetLanguages()
        {
            return ((IBI)BiClient).GetLanguages();
        }

        public List<EntBICity> GetCities()
        {
            return ((IBI)BiClient).GetCities();
        }

        public List<EntBIState> GetStates()
        {
            OperationContextScopeWithHeaders(BiClient.InnerChannel, Headers);
            return ((IBI)BiClient).GetStates();
        }

        public List<EntBITimeZone> GetTimeZones()
        {
            return ((IBI)BiClient).GetTimeZones();
        }

        public List<EntBISysTableItem> GetSysTableData()
        {
            return ((IBI)BiClient).GetSysTableData();
        }

        public List<EntBIRuleDetails> GetRuleConfigurations()
        {
            return ((IBI)BiClient).GetRuleConfigurations();
        }

        public List<EntBIRuleModel> GetRuleModels()
        {
            return ((IBI)BiClient).GetRuleModels();
        }

        public List<EntBIEndOfServiceMetaData> GetEndOfServiceMetaData()
        {
            return ((IBI)BiClient).GetEndOfServiceMetaData();
        }

        public List<EntBICellularData> GetCellularData()
        {
            return ((IBI)BiClient).GetCellularData();
        }

        public List<EntBIEquipment> GetEquipment()
        {
            return ((IBI)BiClient).GetEquipment();
        }

        public List<EntBIEquipmentAllocationHistory> GetEquipmentAllocation()
        {
            return ((IBI)BiClient).GetEquipmentAllocation();
        }

        public List<EntBIEquipmentAllocationHistory> GetEquipmentAllocationHist()
        {
            return ((IBI)BiClient).GetEquipmentAllocationHist();
        }

        public List<EntBIReceiverDetails> GetReceiverDetails()
        {
            return ((IBI)BiClient).GetReceiverDetails();
        }

        public List<EntBIEquipmentModel> GetEquipmentModels()
        {
            return ((IBI)BiClient).GetEquipmentModels();
        }

        public List<EntBIEquipmentModelDevice> GetEquipmentModelDevices()
        {
            return ((IBI)BiClient).GetEquipmentModelDevices();
        }

        public List<EntBIRfReceiverDetails> GetRfReceiverDetails()
        {
            return ((IBI)BiClient).GetRfReceiverDetails();
        }

        public List<EntBIRfReceiverStatus> GetRfReceiverStatus()
        {
            return ((IBI)BiClient).GetRfReceiverStatus();
        }

        public List<EntBIEquipmentFailure> GetEquipmentFailures()
        {
            return ((IBI)BiClient).GetEquipmentFailures();
        }

        public List<EntBIReceiverContentVersion> GetReceiverContentVersions()
        {
            return ((IBI)BiClient).GetReceiverContentVersions();
        }

        public List<EntBIReceiverVersion> GetReceiverVersion()
        {
            return ((IBI)BiClient).GetReceiverVersion();
        }

        public List<EntBIReceiverVersionDetails> GetReceiverVersionDetails()
        {
            return ((IBI)BiClient).GetReceiverVersionDetails();
        }

        public List<EntBIEventMetadata> GetEventsMetadata()
        {
            return ((IBI)BiClient).GetEventsMetadata();
        }

        public List<EntBIAgencyEventMetadata> GetAgencyEventsMetadata()
        {
            return ((IBI)BiClient).GetAgencyEventsMetadata();
        }

        public List<EntBIEventDeviceDefaultConfig> GetEventDeviceDefaultConfiguration()
        {
            return ((IBI)BiClient).GetEventDeviceDefaultConfiguration();
        }

        public List<EntBIEventDeviceConfig> GetEventDeviceConfiguration()
        {
            return ((IBI)BiClient).GetEventDeviceConfiguration();
        }

        public List<EntBIEventProgram> GetEventPrograms()
        {
            return ((IBI)BiClient).GetEventPrograms();
        }

        public List<EntBIEventGroup> GetEventGroups()
        {
            return ((IBI)BiClient).GetEventGroups();
        }

        public List<EntBIEventGroupRelation> GetEventGroupRelations()
        {
            return ((IBI)BiClient).GetEventGroupRelations();
        }

        public List<EntBIEventOpenClosePair> GetEventsOpenClosePairs()
        {
            return ((IBI)BiClient).GetEventsOpenClosePairs();
        }

        public List<EntBIEventFeature> GetEventFeatures()
        {
            return ((IBI)BiClient).GetEventFeatures();
        }

        public List<EntBIEventLog> GetEventLog(int? limit, ulong? DBtimestamp, bool getFromHistory, ulong? DBtimestampMax)
        {
            return ((IBI)BiClient).GetEventLog(limit, DBtimestamp, getFromHistory, DBtimestampMax);
        }

        public List<EntEventParameterConfig> GetEventParamsConfig()
        {
            return ((IBI)BiClient).GetEventParamsConfig();
        }

        public List<EntBIEventParameter> GetEventLogParams(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventLogID, ulong? DBtimestampMax)
        {
            return ((IBI)BiClient).GetEventLogParams(limit, DBtimestamp, getFromHistory, EventLogID, DBtimestampMax);
        }

        public List<EntBIAppColor> GetApplicationColors()
        {
            return ((IBI)BiClient).GetApplicationColors();
        }

        public List<EntBIHandlingAction> GetEventLogActions(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventID, ulong? DBtimestampMax)
        {
            return ((IBI)BiClient).GetEventLogActions(limit, DBtimestamp, getFromHistory, EventID, DBtimestampMax);
        }

        public List<EntBIEventLogPoint> GetEventLogPositions(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventLogID, ulong? DBtimestampMax)
        {
            return ((IBI)BiClient).GetEventLogPositions(limit, DBtimestamp, getFromHistory, EventLogID, DBtimestampMax);
        }

        public List<EntBIAlcoholStatus> GetAlcoholStatus(bool getFromHistory)
        {
            return ((IBI)BiClient).GetAlcoholStatus(getFromHistory);
        }

        public List<EntBIEventLogGroup> GetEventLogGroup(bool getFromHistory)
        {
            return ((IBI)BiClient).GetEventLogGroup(getFromHistory);
        }

        public List<EntBIReceiverEvents> GetReceiverEventsLog(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventLogID, ulong? DBtimestampMax)
        {
            return ((IBI)BiClient).GetReceiverEventsLog(limit, DBtimestamp, getFromHistory, EventLogID, DBtimestampMax);
        }

        public List<EntBIReceiverEvents> GetE3ReceiverEventsLog(int? limit, ulong? DBtimestamp, bool getFromHistory, ulong? DBtimestampMax)
        {
            return ((IBI)BiClient).GetE3ReceiverEventsLog(limit, DBtimestamp, getFromHistory, DBtimestampMax);
        }

        public List<EntBIReceiverCommRetries> GetReceiverCommRetries()
        {
            return ((IBI)BiClient).GetReceiverCommRetries();
        }

        public List<EntBIE4UploadEventReject> GetE4UploadEventReject()
        {
            return ((IBI)BiClient).GetE4UploadEventReject();
        }

        public List<EntBIE4UploadEventRedirect> GetE4UploadEventRedirect()
        {
            return ((IBI)BiClient).GetE4UploadEventRedirect();
        }

        public List<EntBIE4UploadCall> GetE4UploadCalls(int? limit, ulong? DBtimestamp)
        {
            return ((IBI)BiClient).GetE4UploadCalls(limit, DBtimestamp);
        }

        public List<EntBIOffender> GetOffenders(int? limit, ulong? DBtimestamp, bool getFromHistory, int? OffenderID)
        {
            return ((IBI)BiClient).GetOffenders(limit, DBtimestamp, getFromHistory, OffenderID);
        }

        public List<EntBIOffenderContact> GetOffendersContacts(bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffendersContacts(getFromHistory);
        }

        public List<EntBIAddress> GetOffenderAddresses(int? limit, ulong? DBtimestamp, bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffenderAddresses(limit, DBtimestamp, getFromHistory);
        }

        public List<EntBIPhones> GetOffenderPhones(int? limit, ulong? DBtimestamp)
        {
            return ((IBI)BiClient).GetOffenderPhones(limit, DBtimestamp);
        }

        public List<EntBIOffenderProceures> GetOffenderProcedures(bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffenderProcedures(getFromHistory);
        }

        public List<EntBIOffenderWarning> GetOffenderWarnings(int? limit, ulong? DBtimestamp, bool getFromHistory, DateTime? WarningDate)
        {
            return ((IBI)BiClient).GetOffenderWarnings(limit, DBtimestamp, getFromHistory, WarningDate);
        }

        public List<EntBIOffenderEOSData> GetOffenderEosData(bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffenderEosData(getFromHistory);
        }

        public List<EntBIHardwareRuleVersions> GetHardwareRuleVersions()
        {
            return ((IBI)BiClient).GetHardwareRuleVersions();
        }

        public List<EntBIOffenderPicture> GetOffenderPictures(bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffenderPictures(getFromHistory);
        }

        public List<EntBIOffenderRelation> GetOffenderRelations(bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffenderRelations(getFromHistory);
        }

        public List<EntBIGpsOffenderStatus> GetGpsOffenderStatus()
        {
            return ((IBI)BiClient).GetGpsOffenderStatus();
        }

        public List<EntBIRfOffenderStatus> GetRfOffenderStatus()
        {
            return ((IBI)BiClient).GetRfOffenderStatus();
        }

        public List<EntBIOffenderTestParameters> GetOffenderTestParameters()
        {
            return ((IBI)BiClient).GetOffenderTestParameters();
        }

        public List<EntBIOffenderMovedToHist> GetOffenderMovedToHist()
        {
            return ((IBI)BiClient).GetOffenderMovedToHist();
        }

        public List<EntBIUserSecurity> GetUserSecurity(int? limit, ulong? DBtimestamp)
        {
            return ((IBI)BiClient).GetUserSecurity(limit, DBtimestamp);
        }

        public List<EntBIAgency> GetAgencies()
        {
            return ((IBI)BiClient).GetAgencies();
        }

        public List<EntBIUser> GetUsers()
        {
            return ((IBI)BiClient).GetUsers();
        }

        public List<EntBIShifts> GetBIShifts()
        {
            return ((IBI)BiClient).GetBIShifts();
        }

        public List<EntBIShiftsReoccurring> GetBIReoccurringShifts()
        {
            return ((IBI)BiClient).GetBIReoccurringShifts();
        }

        public List<EntBIShiftsOneTime> GetBIOneTimeShifts()
        {
            return ((IBI)BiClient).GetBIOneTimeShifts();
        }

        public List<EntBIDataVersion> GetDataVersions(int? limit, ulong? DBtimestamp, bool getFromHistory, int? OffenderID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetDataVersions(limit, DBtimestamp, getFromHistory, OffenderID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIProgramVersion> GetProgramVersions(int? limit, ulong? DBtimestamp, bool getFromHistory, int? OffenderID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetProgramVersions(limit, DBtimestamp, getFromHistory, OffenderID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIColor> GetBIColors()
        {
            return ((IBI)BiClient).GetBIColors();
        }

        public List<EntBISerialGroups> GetBISerialGroups()
        {
            return ((IBI)BiClient).GetBISerialGroups();
        }

        public List<EntBISerialGroupSegments> GetBISerialGroupSegments()
        {
            return ((IBI)BiClient).GetBISerialGroupSegments();
        }

        public List<EntBIOffenderUnitConfig> GetOffenderUnitConfig(bool getFromHistory)
        {
            return ((IBI)BiClient).GetOffenderUnitConfig(getFromHistory);
        }

        public List<EntBIPoint> GetPoints(int? limit, bool getFromHistory, long? pointID, long? pointIDMax)
        {
            return ((IBI)BiClient).GetPoints(limit, getFromHistory, pointID, pointIDMax);
        }

        public List<EntBIMtdDebug> GetMtdDebug(int? limit, int? mtdDebugRecID)
        {
            return ((IBI)BiClient).GetMtdDebug(limit, mtdDebugRecID);
        }

        public List<EntBIMtdCall> GetMtdCalls(int? limit, int? callRecID)
        {
            return ((IBI)BiClient).GetMtdCalls(limit, callRecID);
        }

        public List<EntBIOffenderSchedule> GetSchedules(int? limit, ulong? DBtimestamp, bool getFromHistory, int? ScheduleID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetSchedules(limit, DBtimestamp, getFromHistory, ScheduleID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIOffenderSchedule> GetE3Schedules(int? limit, ulong? DBtimestamp, bool getFromHistory, int? ScheduleID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetE3Schedules(limit, DBtimestamp, getFromHistory, ScheduleID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIMEMSTimeFrame> GetAlcoholTimeFrames(int? limit, ulong? DBtimestamp, bool getFromHistory, int? OffenderID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetAlcoholTimeFrames(limit, DBtimestamp, getFromHistory, OffenderID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBITimeFrame> GetScheduleTimeFrames(int? limit, ulong? DBtimestamp, bool getFromHistory, int? TimeFrameID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetScheduleTimeFrames(limit, DBtimestamp, getFromHistory, TimeFrameID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIIgnoreTimeFrame> GetIgnoreTimeFrames(int? weekNo, bool getFromHistory)
        {
            return ((IBI)BiClient).GetIgnoreTimeFrames(weekNo, getFromHistory);
        }

        public List<EntBIE3TimeFrame> GetE3TimeFrames(int? limit, ulong? DBtimestamp, bool getFromHistory, int? ScheduleID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetE3TimeFrames(limit, DBtimestamp, getFromHistory, ScheduleID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIGPSTimeFrame> GetGPSTimeFrames(int? limit, ulong? DBtimestamp, bool getFromHistory, int? TimeFrameID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetGPSTimeFrames(limit, DBtimestamp, getFromHistory, TimeFrameID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBICurfewChangeReason> GetCurfewChangeReasons()
        {
            return ((IBI)BiClient).GetCurfewChangeReasons();
        }

        public List<EntBIBackendVersions> GetBackendVersions()
        {
            return ((IBI)BiClient).GetBackendVersions();
        }

        public List<EntBIEmsVersion> GetEmsVersions()
        {
            return ((IBI)BiClient).GetEmsVersions();
        }

        public List<EntBIEtlJobStatus> GetEtlJobStatus()
        {
            return ((IBI)BiClient).GetEtlJobStatus();
        }

        public List<EntBIDataBaseTime> GetDataBaseDateTime()
        {
            return ((IBI)BiClient).GetDataBaseDateTime();
        }

        public List<EntBIEtlResult> ETLRequestReadAccess(int jobID)
        {
            return ((IBI)BiClient).ETLRequestReadAccess(jobID);
        }

        public List<EntBIEtlResult> ETLGetAccessResponse(long jobRunID, int jobID)
        {
            return ((IBI)BiClient).ETLGetAccessResponse(jobRunID, jobID);
        }

        public void ETLNotifyJobStart(long jobRunID, int jobID)
        {
            ((IBI)BiClient).ETLNotifyJobStart(jobRunID, jobID);
        }

        public void ETLNotifyJobEnd(long jobRunID, int jobID, string errorMessage)
        {
            ((IBI)BiClient).ETLNotifyJobEnd(jobRunID, jobID, errorMessage);
        }

        public void ETLNotifyOdsStart(long jobRunID, int jobID)
        {
            ((IBI)BiClient).ETLNotifyOdsStart(jobRunID, jobID);
        }

        public void ETLNotifyOdsEnd(long jobRunID, int jobID, string errorMessage)
        {
            ((IBI)BiClient).ETLNotifyOdsEnd(jobRunID, jobID, errorMessage);
        }

        public void ETLNotifyApiStart(long jobRunID, int jobID, string apiName)
        {
            ((IBI)BiClient).ETLNotifyApiStart(jobRunID, jobID, apiName);
        }

        public void ETLNotifyApiEnd(long jobRunID, int jobID, string apiName, string errorMessage)
        {
            ((IBI)BiClient).ETLNotifyApiEnd(jobRunID, jobID, apiName, errorMessage);
        }

        public List<EntBIDataGapsForHistoryDB> GetDataGapsForHistoryDB()
        {
            return ((IBI)BiClient).GetDataGapsForHistoryDB();
        }

        public List<EntBIEtlResult> ETLHasStopRequest(long jobRunID, int jobID)
        {
            return ((IBI)BiClient).ETLHasStopRequest(jobRunID, jobID);
        }

        public List<EntBIEquipTypesDepend> GetEquipTypesDepend()
        {
            return ((IBI)BiClient).GetEquipTypesDepend();
        }

        public List<EntBIResource> GetResources()
        {
            return ((IBI)BiClient).GetResources();
        }

        public List<EntBITranslation> GetTranslations()
        {
            return ((IBI)BiClient).GetTranslations();
        }

        public List<EntBIReportsList> GetBIReportsList()
        {
            return ((IBI)BiClient).GetBIReportsList();
        }

        public List<EntBIReportsDefaultParams> GetBIReportsDefaultValues()
        {
            return ((IBI)BiClient).GetBIReportsDefaultValues();
        }

        public List<EntBIMtdDebugDictionary> GetBIMtdDebugDictionary()
        {
            return ((IBI)BiClient).GetBIMtdDebugDictionary();
        }

        public List<EntBIAPIFunctionConfigData> GetAPIFunctionConfigData()
        {
            return ((IBI)BiClient).GetAPIFunctionConfigData();
        }

        public List<EntBIAPIEntity> GetAPIEntities()
        {
            return ((IBI)BiClient).GetAPIEntities();
        }

        public List<EntBIParameter> GetSiteBusinessParameters()
        {
            return ((IBI)BiClient).GetSiteBusinessParameters();
        }

        public List<EntBIAPIModules> GetAPIModules()
        {
            return ((IBI)BiClient).GetAPIModules();
        }

        public List<EntBIGroupDetails> GetGroupDetails()
        {
            return ((IBI)BiClient).GetGroupDetails();
        }

        public List<EntBIGroupOffenderRelation> GetGroupOffenderRelation()
        {
            return ((IBI)BiClient).GetGroupOffenderRelation();
        }

        public List<EntBIGroupScheduleRelation> GetGroupScheduleRelation()
        {
            return ((IBI)BiClient).GetGroupScheduleRelation();
        }

        public List<EntBIGroupZoneRelation> GetGroupZoneRelation()
        {
            return ((IBI)BiClient).GetGroupZoneRelation();
        }

        public List<EntBIDynamicFieldValue> GetDynamicFieldsValues(bool getFromHistory)
        {
            return ((IBI)BiClient).GetDynamicFieldsValues(getFromHistory);
        }

        public List<EntBIZone> GetZones(bool getFromHistory)
        {
            return ((IBI)BiClient).GetZones(getFromHistory);
        }

        public List<EntBIOffednerZones> GetOffenderZones(int? limit, ulong? DBtimestamp, bool getFromHistory, int? ZoneID, ulong? DBtimestampMax, char wasCurrentVersion)
        {
            return ((IBI)BiClient).GetOffenderZones(limit, DBtimestamp, getFromHistory, ZoneID, DBtimestampMax, wasCurrentVersion);
        }

        public List<EntBIPolygonZonePoints> GetPolygonZonePoints(bool getFromHistory)
        {
            return ((IBI)BiClient).GetPolygonZonePoints(getFromHistory);
        }

        public List<EntBIScheduleChange> GetScheduleChanges(bool getFromHistory)
        {
            return ((IBI)BiClient).GetScheduleChanges(getFromHistory);
        }

        public List<EntBIAuthorizedAbsenceRequest> GetRequestAuthorizedAbsence(bool getFromHistory)
        {
            return ((IBI)BiClient).GetRequestAuthorizedAbsence(getFromHistory);
        }

        public List<EntBISuspendProgramData> GetSuspendProgramLog(bool getFromHistory)
        {
            return ((IBI)BiClient).GetSuspendProgramLog(getFromHistory);
        }

        public List<EntBIReallocateOffenderData> GetReallocateOffenderLog()
        {
            return ((IBI)BiClient).GetReallocateOffenderLog();
        }

        public List<EntBIWebAuditSession> GetWebAuditSession(long sessionID, int? limit)
        {
            return ((IBI)BiClient).GetWebAuditSession(sessionID, limit);
        }

        public List<EntBIWebAuditLog> GetWebAuditLog(long AuditLogID, int limit)
        {
            return ((IBI)BiClient).GetWebAuditLog(AuditLogID, limit);
        }

        public List<EntBIAlcoholPicture> GetAlcoholPictures(int? limit, int? eventLogId)
        {
            return ((IBI)BiClient).GetAlcoholPictures(limit, eventLogId);
        }

        public List<EntBILogDeletedEntities> GetDeletedEntitiesLog(int? limit, ulong? DBtimestamp)
        {
            return ((IBI)BiClient).GetDeletedEntitiesLog(limit, DBtimestamp);
        }

        public List<EntBISyslog> GetSyslog()
        {
            return ((IBI)BiClient).GetSyslog();
        }

        public List<EntBIMtdQueue> GetMtdQueue()
        {
            return ((IBI)BiClient).GetMtdQueue();
        }
        #endregion BI API Implementation

        #region Helper Methods
        /// <summary>
        /// run all methods without parameters
        /// </summary>
        public void ExecuteAllNonParmetersMethods()
        {
            int methodsExecutedSuccessfully = 0, methodsNotExecuted = 0;
            var sbMethodsExecutedSuccessfully = new StringBuilder();
            var sbMethodsNotExecuted = new StringBuilder();
            var exceptions = new List<Exception>();

            var sbCsv = new StringBuilder();
            sbCsv.AppendLine("method_parameter, type");

            var methods = typeof(IBI).GetMethods();
            methods = ExcludeMethods(methods, "GetE4UploadEventReject", "GetBIMtdDebugDictionary");
            foreach (var method in methods)
            {
                Log.DebugFormat("Start analyzing method {0}", method.Name);
                try
                {
                    var parameters = method.GetParameters();
                    if (parameters.Length == 0)
                    {//method without parameters - run it
                        method.Invoke(this, null);
                        methodsExecutedSuccessfully++;
                    }
                    else
                    {//method with parameterts - ignore it
                        var IgnoreMsg = string.Format("{0} Ignored because it have {1} parameters", method.Name, parameters.Length);
                        Log.Debug(IgnoreMsg);

                        //Log.DebugFormat("{0, -20}|{1, -20}|", "Name", "ParameterType");
                        //Log.Debug(new String('-', 40));
                        foreach (var parameter in parameters)
                        {
                            Log.DebugFormat("{0, -20}|{1, -20}|", parameter.Name, parameter.ParameterType);
                            var csvLine = $"{method.Name}_{parameter.Name}, {parameter.ParameterType}";
                            sbCsv.AppendLine(csvLine);
                        }
                        //Log.Debug(new String('-', 40));
                        sbMethodsNotExecuted.Append(IgnoreMsg).AppendLine();
                        methodsNotExecuted++;
                    }
                }
                catch (FaultException<ValidationFault> fe)
                {
                    var fullMsg = string.Format("method {0} was failed with error: {1}", method.Name, fe.Detail.Errors[0].Message);
                    Log.DebugFormat(fullMsg);
                    exceptions.Add(new Exception(fullMsg, fe));
                }
                catch (Exception exception)
                {
                    //get the inner Most Exception message
                    var innerMostException = exception;
                    while (innerMostException.InnerException != null)
                    {
                        innerMostException = innerMostException.InnerException;
                    }
                    var fullMsg = string.Format("method {0} was failed with error {1} innerMostException = {2}", method.Name, exception.Message, innerMostException.Message);
                    Log.DebugFormat(fullMsg);
                    exceptions.Add(new Exception(fullMsg, exception));
                }
                finally
                {
                    Log.DebugFormat("Finish analyzing method {0}", method.Name);
                    sbMethodsExecutedSuccessfully.Append(method.Name).AppendLine();
                }
            }

            //print summery of the tests
            string joinedMessages = string.Join("\n", exceptions.Select(x => x.Message));
            var msg = string.Format("total methods: {0}. passed {1}. failed {2}. didn't run {3} \n aggregate errors:\n{4}",
                methods.Length, methodsExecutedSuccessfully, exceptions.Count, methodsNotExecuted, joinedMessages);

            Console.WriteLine(msg);
            Console.WriteLine("\nMethod Executed Successfully:\n{0}", sbMethodsExecutedSuccessfully.ToString());
            Console.WriteLine("\nMethod Not Executed:\n{0}", sbMethodsNotExecuted.ToString());
            //write csv file of methods and parameters
            string path = Path.GetPathRoot(Environment.SystemDirectory);
            var fullFilePath = Path.Combine(path, "BiApiMethodsAndPamaeters.csv");
            File.WriteAllText(fullFilePath, sbCsv.ToString());
            if (exceptions.Count > 0)
            {
                var aggregateException = new AggregateException(msg, exceptions);
                throw new Exception(msg, aggregateException);
            }
        }

        private MethodInfo[] ExcludeMethods(MethodInfo[] methods, params string[] MethodsNameToExclude)
        {
            var methodsList = new List<MethodInfo>(methods);
            var newMethodList = new List<MethodInfo>();

            foreach (var method in methodsList)
            {
                if (MethodsNameToExclude.Any(x => x == method.Name))
                {
                    //Do nothing
                }
                else
                    newMethodList.Add(method);
            }

            return newMethodList.ToArray();
        }

        public void ExecuteAllMethodsWithParameters()
        {
            int methodsExecutedSuccessfully = 0, methodsNotExecuted = 0;
            var sbMethodsExecutedSuccessfully = new StringBuilder();
            var sbMethodsNotExecuted = new StringBuilder();
            var exceptions = new List<Exception>();

            log4net.ThreadContext.Properties["ForceAddResultFile"] = true;//to save the files in CI

            //use default
            var fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DataSource\\BiApiMethodsAndParameters.csv");
            if (File.Exists(fullPath) == false)
            {
                throw new FileNotFoundException("ExecuteAllMethodsWithParameters method can't work without file", fullPath);
            }
            //read the file to dictionary of method and parameters as the key and the value is the value that need to be set.
            var dicMethdsParametersAndValues = MethdsParametersAndValues(fullPath);

            var sbCsv = new StringBuilder();
            sbCsv.AppendLine("method_parameter, type");

            var methods = typeof(IBI).GetMethods();
            foreach (var method in methods)
            {
                Log.DebugFormat("Start analyzing method {0}", method.Name);
                try
                {
                    var parametersAndValues = new StringBuilder();
                    var parameters = method.GetParameters();
                    object[] parametersValuesArrey = null;
                    if (parameters.Length > 0)
                    {//method with parameterts
                        Log.DebugFormat("{0} have {1} parameters", method.Name, parameters.Length);
                        //prepare array of values
                        parametersValuesArrey = new object[parameters.Length];
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            var parameterInfo = parameters[i];
                            parametersValuesArrey[i] = GetParameterValueFromCsv(dicMethdsParametersAndValues, method.Name, parameterInfo.Name);
                            parametersAndValues.AppendFormat(", {0} - {1}", parameterInfo.Name, parametersValuesArrey[i] == null ? "null" : parametersValuesArrey[i].ToString());
                        }

                        //invoke the method
                        method.Invoke(this, parametersValuesArrey);
                        methodsExecutedSuccessfully++;
                        sbMethodsExecutedSuccessfully.AppendFormat("{0} - {1}", method.Name, parametersAndValues).AppendLine();
                    }
                }
                catch (FaultException<ValidationFault> fe)
                {
                    var fullMsg = string.Format("method {0} was failed with error: {1}", method.Name, fe.Detail.Errors[0].Message);
                    Log.DebugFormat(fullMsg);
                    exceptions.Add(new Exception(fullMsg, fe));
                }
                catch (KeyNotFoundException knfe)
                {
                    var fullMsg = string.Format("method {0} was failed with error {1}.", method.Name, knfe.Message);
                    Log.DebugFormat(fullMsg);
                    sbMethodsNotExecuted.AppendFormat("{0} - {1}", method.Name, fullMsg).AppendLine();
                    methodsNotExecuted++;
                }
                catch (Exception exception)
                {
                    //get the inner Most Exception message
                    var innerMostException = exception;
                    while (innerMostException.InnerException != null)
                    {
                        innerMostException = innerMostException.InnerException;
                    }
                    var fullMsg = string.Format("method {0} was failed with error {1} innerMostException = {2}", method.Name, exception.Message, innerMostException.Message);
                    Log.DebugFormat(fullMsg);
                    exceptions.Add(new Exception(fullMsg, exception));
                }
                finally
                {
                    Log.DebugFormat("Finish analyzing method {0}", method.Name);

                }
            }

            //print summery of the tests
            string joinedMessages = string.Join("\n", exceptions.Select(x => x.Message));
            var msg = string.Format("total methods: {0}. passed {1}. failed {2} not executed because missing parameter values {3}. \n aggregate errors:\n{3}",
                methods.Length, methodsExecutedSuccessfully, exceptions.Count, methodsNotExecuted, joinedMessages);

            Console.WriteLine(msg);
            Console.WriteLine("Method Executed Successfully:");
            Console.WriteLine(sbMethodsExecutedSuccessfully.ToString());
            Console.WriteLine("Method Not Executed:");
            Console.WriteLine(sbMethodsNotExecuted.ToString());
            if (exceptions.Count > 0)
            {
                var aggregateException = new AggregateException(msg, exceptions);
                throw new Exception(msg, aggregateException);
            }
            log4net.ThreadContext.Properties["ForceAddResultFile"] = true;//to save the files in CI
        }

        public static string GenerateKey4MethodAndParameter(string methodName, string parameterName)
        {
            return string.Format("{0}_{1}", methodName, parameterName);
        }

        private object GetParameterValueFromCsv(Dictionary<string, MethodParameterData> dictionary, string methodName, string parameterName)
        {
            object result = null;
            var key = GenerateKey4MethodAndParameter(methodName, parameterName);
            MethodParameterData mpd = null;
            if (dictionary.TryGetValue(key, out mpd) == true)
            {
                result = mpd.Value;
            }
            else
            {
                throw new KeyNotFoundException(string.Format("key {0} wasn't found in dictionary", key));
            }
            return result;
        }

        public static char CSV_Delimiter = ',';

        public enum CSV_Fields
        {
            CSV_Fields_method,
            CSV_Fields_parameter,
            CSV_Fields_type,
            CSV_Fields_value,
            CSV_Fields_Last
        };

        /// <summary>
        /// generate dictionary of methods parameters and value from CSV file.
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns><see cref="Dictionary<string, object>"/> that represent the method parameters and values</returns>
        private Dictionary<string, MethodParameterData> MethdsParametersAndValues(string fullPath)
        {
            var result = new Dictionary<string, MethodParameterData>();

            using (var reader = new StreamReader(fullPath))
            {
                int lineCount = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    lineCount++;
                    var values = line.Split(CSV_Delimiter);
                    if (values.Length != (int)CSV_Fields.CSV_Fields_Last)
                    {
                        var error = string.Format(
                            "Csv file {0} not in the correct format. expecting {1} fields and actualy {2} fields (error in line : {3})",
                            fullPath, (int)CSV_Fields.CSV_Fields_Last, values.Length, line);
                        throw new Exception(error);
                    }

                    if (string.IsNullOrEmpty(values[(int)CSV_Fields.CSV_Fields_value]))
                    {//skip this . no need to load
                        continue;
                    }
                    var key = GenerateKey4MethodAndParameter(values[(int)CSV_Fields.CSV_Fields_method], values[(int)CSV_Fields.CSV_Fields_parameter]);

                    //generate type base on the string from csv
                    var typeName = values[(int)CSV_Fields.CSV_Fields_type];
                    var type = Type.GetType(typeName);
                    if (type == null)
                    {//might be first line.skipp it
                        continue;
                    }

                    try
                    {
                        var parameterValue = Activator.CreateInstance(type);
                        parameterValue = ConvertValueToSpecificType(values[(int)CSV_Fields.CSV_Fields_type], values[(int)CSV_Fields.CSV_Fields_value]);
                        result.Add(key, new MethodParameterData() { ParameterType = type, Value = parameterValue });
                    }
                    catch (Exception exception)
                    {
                        Log.ErrorFormat("while try to convert value from line {0}. the line {1}. Error Message: {2}", lineCount, line, exception.Message);
                    }
                }
            }
            return result;
        }

        private object ConvertValueToSpecificType(string type, string value)
        {
            object result = null;

            if (string.IsNullOrEmpty(type))
            {
                Log.WarnFormat("Convert value method got null or empty string as type and value {0} .returning new object as the value", value ?? "null value");
                result = new object();
            }
            else if (value.ToLower() == "null")
            {
                Log.WarnFormat("Convert value method got null value {0} .returning null as the value", value);
                result = null;
            }
            else
            {
                type = type.Trim(' ');
                switch (type)
                {
                    case "System.Int64": result = System.Int64.Parse(value); break;
                    case "System.Int32": result = System.Int32.Parse(value); break;
                    case "System.String": result = value.Clone(); break;
                    case "System.Char": result = System.Char.Parse(value); break;
                    case "System.Boolean": result = System.Boolean.Parse(value); break;
                    case "System.Nullable`1[System.Int32]":
                        result = value.ToLower() == "null" ? new System.Int32?() : new System.Int32?(System.Int32.Parse(value)); break;
                    case "System.Nullable`1[System.Int64]":
                        result = value.ToLower() == "null" ? new System.Int64?() : new System.Int64?(System.Int64.Parse(value)); break;
                    case "System.Nullable`1[System.UInt64]":
                        result = value.ToLower() == "null" ? new System.UInt64?() : new System.UInt64?(System.UInt64.Parse(value)); break;
                    case "System.Nullable`1[System.DateTime]":
                        result = value.ToLower() == "null" ? new System.DateTime?() : new System.DateTime?(System.DateTime.Parse(value)); break;
                    default:
                        Log.WarnFormat("Convert value method got unsupported type {0} with value {1}. returning new object as the value", type ?? "null type", value ?? "null value");
                        result = new object();
                        break;
                }
            }
            return result;
        }

        
        public List<EntBIEventLog> GetEventLog(int? limit, ulong? DBtimestamp, bool getFromHistory, ulong? DBtimestampMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public List<EntBIEventParameter> GetEventLogParams(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventLogID, ulong? DBtimestampMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public List<EntBIHandlingAction> GetEventLogActions(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventID, ulong? DBtimestampMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public List<EntBIEventLogPoint> GetEventLogPositions(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventLogID, ulong? DBtimestampMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public List<EntBIAlcoholStatus> GetAlcoholStatus(bool getFromHistory, long? fromDateTime, long? toDateTime, int? limit, int? eventLogID)
        {
            throw new NotImplementedException();
        }

        public List<EntBIReceiverEvents> GetReceiverEventsLog(int? limit, ulong? DBtimestamp, bool getFromHistory, int? EventLogID, ulong? DBtimestampMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public List<EntBIReceiverEvents> GetE3ReceiverEventsLog(int? limit, ulong? DBtimestamp, bool getFromHistory, ulong? DBtimestampMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public List<EntBIOffenderPicture> GetOffenderPictures(int? limit, ulong? DBtimestamp, bool getFromHistory)
        {
            throw new NotImplementedException();
        }

        public List<EntBIPoint> GetPoints(int? limit, bool getFromHistory, long? pointID, long? pointIDMax, long? fromDateTime, long? toDateTime)
        {
            throw new NotImplementedException();
        }

        public class MethodParameterData
        {
            public object Value { get; set; }
            public Type ParameterType { get; set; }
        }

        #endregion Helper Methods
    }


}
