﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.API.Offenders;

using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestingFramework.Proxies.API.Trails
{
    /// <summary>
    /// trails proxy for version 0
    /// </summary>
    public class TrailsProxy : BaseProxy, Trails0.ITrails
    {
        #region Consts
        protected const string TrailsSvcPathToken = "TrailsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "TrailsSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Trails0.TrailsClient TrailsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private TrailsProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, TrailsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            TrailsClient = new Trails0.TrailsClient(SvcEndpointConfigurationName, endpointAddress);

            TrailsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            TrailsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var getOffendersRequest = new Offenders0.EntMsgGetOffendersListRequest()
            {
                Limit = 1,
                ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active },
                SortField = Offenders0.EnmOffendersSortOptions.LastTimeStamp,
                SortDirection = Offenders0.ListSortDirection.Descending
            };
            var getOffendersResponse = OffendersProxy.Instance.GetOffenders(getOffendersRequest);
            var getTrailRequest = new Trails0.EntMsgGetTrailRequest()
            {
                OffenderID = getOffendersResponse.OffendersList[0].ID,
                StartTime = DateTime.Now.AddMinutes(-10),
                EndTime = DateTime.Now,
                MinimumSeconds = 50,
                CoordinatesFormat = Trails0.EnmCoordinatesFormat.WebMercator_Meters
            };
            var getTrailResponse = GetTrail(getTrailRequest);
        }

        #endregion Proxy init

        #region Singleton
        private static TrailsProxy trailsProxy = null;
        public static TrailsProxy Instance
        {
            get
            {
                if (trailsProxy == null)
                {
                    trailsProxy = new TrailsProxy();
                }

                return trailsProxy;
            }
        }
        #endregion Singleton
        public Trails0.EntMsgGetGeographicLocationsResponse GetGeographicLocations(Trails0.EntMsgGetGeographicLocationsRequest entMsgGetGeographicLocationsRequest)
        {
            try
            {
                return TrailsClient.GetGeographicLocations(entMsgGetGeographicLocationsRequest);
            }
            catch (FaultException<Trails0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails0.EntMsgGetGeographicLocationsResponse> GetGeographicLocationsAsync(Trails0.EntMsgGetGeographicLocationsRequest entMsgGetGeographicLocationsRequest)
        {
            throw new NotImplementedException();
        }

        public Trails0.EntMsgGetLastLocationResponse GetLastLocation(Trails0.EntMsgGetLastLocationRequest entMsgGetLastLocationRequest)
        {
            try
            {
                return TrailsClient.GetLastLocation(entMsgGetLastLocationRequest);
            }
            catch (FaultException<Trails0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails0.EntMsgGetLastLocationResponse> GetLastLocationAsync(Trails0.EntMsgGetLastLocationRequest entMsgGetLastLocationRequest)
        {
            throw new NotImplementedException();
        }

        public Trails0.EntMsgGetTrailResponse GetTrail(Trails0.EntMsgGetTrailRequest entMsgGetTrailRequest)
        {
            try
            {
                return TrailsClient.GetTrail(entMsgGetTrailRequest);
            }
            catch (FaultException<Trails0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails0.EntMsgGetTrailResponse> GetTrailAsync(Trails0.EntMsgGetTrailRequest entMsgGetTrailRequest)
        {
            throw new NotImplementedException();
        }
    }
}
