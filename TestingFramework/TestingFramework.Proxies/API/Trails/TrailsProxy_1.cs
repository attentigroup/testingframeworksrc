﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;


using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;


namespace TestingFramework.Proxies.API.Trails
{
    /// <summary>
    /// trails proxy for version -1
    /// </summary>
    public class TrailsProxy_1 : BaseProxy, Trails1.ITrails
    {
        #region Consts
        protected const string TrailsSvcPathToken = "TrailsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "TrailsSvc-1_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Trails1.TrailsClient TrailsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private TrailsProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, TrailsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            TrailsClient = new Trails1.TrailsClient(SvcEndpointConfigurationName, endpointAddress);

            TrailsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            TrailsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var getOffendersRequest = new Offenders1.EntMsgGetOffendersListRequest()
            {
                Limit = 1,
                ProgramStatus = new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active },
                SortField = Offenders1.EnmOffendersSortOptions.LastTimeStamp,
                SortDirection = System.ComponentModel.ListSortDirection.Descending};
            var getOffendersResponse = OffendersProxy_1.Instance.GetOffenders(getOffendersRequest);
            var getTrailRequest = new Trails1.EntMsgGetTrailRequest()
            {
                OffenderID = getOffendersResponse.OffendersList[0].ID,
                StartTime = DateTime.Now.AddMinutes(-10),
                EndTime = DateTime.Now,
                MinimumSeconds = 50,
                CoordinatesFormat = Trails1.EnmCoordinatesFormat.WebMercator_Meters
            };
            var getTrailResponse = GetTrail(getTrailRequest);
        }

        #endregion Proxy init

        #region Singleton
        private static TrailsProxy_1 trailsProxy_1 = null;
        public static TrailsProxy_1 Instance
        {
            get
            {
                if (trailsProxy_1 == null)
                {
                    trailsProxy_1 = new TrailsProxy_1();
                }

                return trailsProxy_1;
            }
        }
        #endregion Singleton
        public Trails1.EntMsgGetLastLocationResponse GetLastLocation(Trails1.EntMsgGetLastLocationRequest entMsgGetLastLocationRequest)
        {
            try
            {
                return TrailsClient.GetLastLocation(entMsgGetLastLocationRequest);
            }
            catch (FaultException<Trails1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails1.EntMsgGetLastLocationResponse> GetLastLocationAsync(Trails1.EntMsgGetLastLocationRequest entMsgGetLastLocationRequest)
        {
            throw new NotImplementedException();
        }

        public Trails1.EntMsgGetTrailResponse GetTrail(Trails1.EntMsgGetTrailRequest entMsgGetTrailRequest)
        {
            try
            {
                return TrailsClient.GetTrail(entMsgGetTrailRequest);
            }
            catch (FaultException<Trails1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails1.EntMsgGetTrailResponse> GetTrailAsync(Trails1.EntMsgGetTrailRequest entMsgGetTrailRequest)
        {
            throw new NotImplementedException();
        }
    }
}
