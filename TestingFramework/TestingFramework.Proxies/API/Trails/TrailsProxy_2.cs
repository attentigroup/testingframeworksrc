﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;

using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;


namespace TestingFramework.Proxies.API.Trails
{
    /// <summary>
    /// trails proxy for version -2
    /// </summary>
    public class TrailsProxy_2 : BaseProxy, Trails2.ITrails
    {
        #region Consts
        protected const string TrailsSvcPathToken = "TrailsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "TrailsSvc-2_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Trails2.TrailsClient TrailsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private TrailsProxy_2()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, TrailsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            TrailsClient = new Trails2.TrailsClient(SvcEndpointConfigurationName, endpointAddress);

            TrailsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            TrailsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var getOffendersRequest = new Offenders2.EntMsgGetOffendersListRequest()
            {
                MaximumRows = 1,
                ProgramStatus = new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active },
                SortField = Offenders2.EnmOffendersSortOptions.LastTimeStamp,
                SortDirection = System.ComponentModel.ListSortDirection.Descending
            };
            var getOffendersResponse = OffendersProxy_2.Instance.GetOffenders(getOffendersRequest);
            var getTrailRequest = new Trails2.EntMsgGetTrailRequest()
            {
                OffenderID = getOffendersResponse.OffendersList[0].ID,
                StartTime = DateTime.Now.AddMinutes(-10),
                EndTime = DateTime.Now,
                MinimumSeconds = 50,
                CoordinatesFormat = Trails2.EnmCoordinatesFormat.WebMercator_Meters
            };
            var getTrailResponse = GetTrail(getTrailRequest);
        }

        #endregion Proxy init

        #region Singleton
        private static TrailsProxy_2 trailsProxy_2 = null;
        public static TrailsProxy_2 Instance
        {
            get
            {
                if (trailsProxy_2 == null)
                {
                    trailsProxy_2 = new TrailsProxy_2();
                }

                return trailsProxy_2;
            }
        }
        #endregion Singleton
        public Trails2.EntMsgGetLastLocationResponse GetLastLocation(Trails2.EntMsgGetLastLocationRequest entMsgGetLastLocationRequest)
        {
            try
            {
                return TrailsClient.GetLastLocation(entMsgGetLastLocationRequest);
            }
            catch (FaultException<Trails2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails2.EntMsgGetLastLocationResponse> GetLastLocationAsync(Trails2.EntMsgGetLastLocationRequest entMsgGetLastLocationRequest)
        {
            throw new NotImplementedException();
        }

        public Trails2.EntMsgGetTrailResponse GetTrail(Trails2.EntMsgGetTrailRequest entMsgGetTrailRequest)
        {
            try
            {
                return TrailsClient.GetTrail(entMsgGetTrailRequest);
            }
            catch (FaultException<Trails2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Trails2.EntMsgGetTrailResponse> GetTrailAsync(Trails2.EntMsgGetTrailRequest entMsgGetTrailRequest)
        {
            throw new NotImplementedException();
        }
    }
}
