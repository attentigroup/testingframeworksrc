﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.API
{
    public class BaseProxy
    {
        protected const string SvcUriToken = "SvcUri";
        protected const string SvcEndpointToken = "SvcEndpointUri";
        protected const string Svc_1_EndpointToken = "SvcEndpointUri-1";
        protected const string Svc_2_EndpointToken = "SvcEndpointUri-2";

        protected const string SvcApiUserNameToken = "ApiUserName";
        protected const string SvcApiPasswordToken = "ApiPassword";

        protected const string EquipmentSvcPathToken = "EquipmentSvcPath";

        protected const string SecuritySvcPathToken = "SecuritySvcPath";

        protected const string EquipmentSvcEndpointConfigurationNameToken = "EquipmentSvcEndpointConfigurationName";

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public EndpointAddress GetEndpointAddressByConfig(string svcEndpointUri = null, string svcPathToken = null)
        {
            string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
            string svcCurrentVerEndpoint = ConfigurationManager.AppSettings[svcEndpointUri];
            string svcPath = ConfigurationManager.AppSettings[svcPathToken];

            string fullUri = svcUri + svcCurrentVerEndpoint + svcPath;

            Log.InfoFormat("Proxy going to connect to URI:{0} ", fullUri);
            EndpointAddress fullSvcEndpoint = new EndpointAddress(fullUri);
            return fullSvcEndpoint;
        }

        private Dictionary<string, string> _headers = new Dictionary<string, string>();
        public Dictionary<string, string> Headers { get { return _headers; } set { _headers = value; } }



        protected static OperationContextScope OperationContextScopeWithHeaders(IClientChannel channel, Dictionary<string, string> Headers)
        {

            if (OperationContext.Current == null)
            {
                OperationContext.Current = new OperationContext(channel);
            }

            var scope = new OperationContextScope(channel);
            var httpRequestProperty = new HttpRequestMessageProperty();

            //add headers and add them to the scope
            foreach (KeyValuePair<string, string> entry in Headers)
            {
                httpRequestProperty.Headers.Add(entry.Key, entry.Value);
            }
            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

            return scope;
        }

        private string _ipAddress = string.Empty;

        public string LocalIpAddress
        {
            get {
                lock (_ipAddress)
                {
                    if (string.IsNullOrEmpty(_ipAddress))
                    {
                        string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                                                             // Get the IP  
                        _ipAddress = Dns.GetHostEntry(hostName).AddressList[0].ToString();
                    } 
                }

                return _ipAddress;
            }
            set { _ipAddress = value; }
        }

    }
}
