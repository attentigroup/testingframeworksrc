﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestingFramework.Proxies.API.Information
{
    public class InformationProxy : BaseProxy, Information0.IInformation
    {
        #region Consts
        protected const string InformationSvcPathToken = "InformationSvcPath";
        protected const string InformationSvcEndpointConfigurationNameToken = "InformationSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Information0.InformationClient InformationClient { get; set; }

        #endregion Properties

        #region Singleton
        private static InformationProxy informationProxy = null;
        public static InformationProxy Instance
        {
            get
            {
                if (informationProxy == null)
                {
                    informationProxy = new InformationProxy();
                }

                return informationProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private InformationProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, InformationSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[InformationSvcEndpointConfigurationNameToken];
            InformationClient = new Information0.InformationClient(SvcEndpointConfigurationName, endpointAddress);

            InformationClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            InformationClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }


        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init
        public void AddAuthorizedAbsenceData(Information0.EntMsgAddAuthorizedAbsenceDataRequest entMsgAddAuthorizedAbsenceDataRequest)
        {
            try
            {
                InformationClient.AddAuthorizedAbsenceData(entMsgAddAuthorizedAbsenceDataRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public bool AddFile(Information0.EntMsgUploadFileRequest fileInfo)
        {
            try
            {
                return InformationClient.AddFile(fileInfo);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void AddOffenderWarning(Information0.EntMsgAddOffenderWarningRequest entMsgAddOffenderWarningRequest)
        {
            try
            {
                InformationClient.AddOffenderWarning(entMsgAddOffenderWarningRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void AddScheduleChange(Information0.EntMsgAddScheduleChangeRequest entMsgAddScheduleChangeRequest)
        {
            try
            {
                InformationClient.AddScheduleChange(entMsgAddScheduleChangeRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void AddSoftwareFile(byte[] file, Information0.EnmProgramGroup group, string name, string description, bool isUpdate)
        {
            try
            {
                InformationClient.AddSoftwareFile(file, group, name, description, isUpdate);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        
        public bool DeleteFile(int fileId, Information0.EnmOwnerEntity ownerEntity, int ownerEntityID)
        {
            try
            {
                return InformationClient.DeleteFile(fileId, ownerEntity, ownerEntityID);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffenderWarning(Information0.EntMsgDeleteOffenderWarningRequest entMsgDeleteOffenderWarningRequest)
        {
            try
            {
                InformationClient.DeleteOffenderWarning(entMsgDeleteOffenderWarningRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteScheduleChange(Information0.EntMsgDeleteScheduleChangeRequest entMsgDeleteScheduleChangeRequest)
        {
            try
            {
                InformationClient.DeleteScheduleChange(entMsgDeleteScheduleChangeRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        
        public Information0.EntMsgGetAuthorizedAbsenceRequestsListResponse GetAuthorizedAbsenceRequestsList(Information0.EntMsgGetAuthorizedAbsenceRequestsListRequest entMsgGetAbsenceRequestsListRequest)
        {
            try
            {
                return InformationClient.GetAuthorizedAbsenceRequestsList(entMsgGetAbsenceRequestsListRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Information0.EntMsgGetCustomFieldsDefinitions GetCustomFieldsSystemDefinitions()
        {
            try
            {
                return InformationClient.GetCustomFieldsSystemDefinitions();
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Information0.EntFileMetadata GetFile(Information0.EntMsgGetFilesMetadataRequest request)
        {
            try
            {
                return InformationClient.GetFile(request);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Information0.GetDataResultOfEntFileMetadataypU6tq2N GetFileList(Information0.EntMsgGetFilesMetadataRequest request)
        {
            try
            {
                return InformationClient.GetFileList(request);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Information0.EntOffenderAdditionalData GetOffenderAdditionalData(int offenderID)
        {
            return ((Information0.IInformation)InformationClient).GetOffenderAdditionalData(offenderID);
        }

        public Information0.EntMsgGetOffenderWarningsResponse GetOffenderWarnings(Information0.EntMsgGetOffenderWarningsRequest entMsgGetOffenderWarningsRequest)
        {
            try
            {
                return InformationClient.GetOffenderWarnings(entMsgGetOffenderWarningsRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        
        public Information0.EntMsgGetReallocateOffenderLogResponse GetReallocateOffenderLog(Information0.EntMsgGetReallocateOffenderLogRequest entMsgGetReallocateOffenderLogRequest)
        {
            throw new NotImplementedException();
        }

        public Information0.EntMsgGetScheduleChangesResponse GetScheduleChanges(Information0.EntMsgGetScheduleChangesRequest entMsgGetScheduleChangesRequest)
        {
            try
            {
                return InformationClient.GetScheduleChanges(entMsgGetScheduleChangesRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Information0.EntFileHeaderBase GetSoftwareFileData(byte[] file, Information0.EnmProgramGroup group)
        {
            throw new NotImplementedException();
        }
        
        public bool IsFileQuotaExceeded()
        {
            throw new NotImplementedException();
        }

        public void UpdateAuthorizedAbsenceData(Information0.EntMsgUpdateAuthorizedAbsenceDataRequest entMsgUpdateAuthorizedAbsenceDataRequest)
        {
            throw new NotImplementedException();
        }
        
        public bool UpdateFile(Information0.EntMsgUploadFileRequest fileInfo)
        {
            throw new NotImplementedException();
        }

        public void UpdateOffenderAdditionalData(Information0.EntOffenderAdditionalData additionalData)
        {
            throw new NotImplementedException();
        }

        public void UpdateScheduleChange(Information0.EntMsgUpdateScheduleChangeRequest entMsgUpdateScheduleChangeRequest)
        {
            throw new NotImplementedException();
        }
        
        public void UpdateCustomFieldsSystemDefinitions(List<Information0.EntCustomFieldSystemDefinition> customFieldsDefinitionsList)
        {
            ((Information0.IInformation)InformationClient).UpdateCustomFieldsSystemDefinitions(customFieldsDefinitionsList);
        }

        public List<Information0.EntCustomFiledListItem> GetPredefinedListsFromSysTable(List<string> tableIDs)
        {
            return ((Information0.IInformation)InformationClient).GetPredefinedListsFromSysTable(tableIDs);
        }

        public List<Information0.EntCustomFiledListItem> VerifyEmsSystableItemsDelete(List<Information0.EntCustomFiledListItem> listOfDeletedRows)
        {
            return ((Information0.IInformation)InformationClient).VerifyEmsSystableItemsDelete(listOfDeletedRows);
        }

        List<Information0.EntFileHeaderBase> Information0.IInformation.GetSoftwareVersions(Information0.EnmProgramGroup group, string version)
        {
            return ((Information0.IInformation)InformationClient).GetSoftwareVersions(group, version);
        }

        public void DeleteAuthorizedAbsenceData(Information0.EntMsgDeleteAuthorizedAbsenceDataRequest entMsgDeleteAuthorizedAbsenceDataRequest)
        {
            try
            {
                InformationClient.DeleteAuthorizedAbsenceData(entMsgDeleteAuthorizedAbsenceDataRequest);
            }
            catch (FaultException<Information0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            //((Information0.IInformation)InformationClient).DeleteAuthorizedAbsenceData(entMsgDeleteAuthorizedAbsenceDataRequest);
        }
    }
}
