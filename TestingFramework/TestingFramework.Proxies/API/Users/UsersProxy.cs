﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Users;

namespace TestingFramework.Proxies.API.Users
{
    public class UsersProxy : BaseProxy, EM.Interfaces.Users.IUsers
    {

        #region Consts

        protected const string UsersSvcEndpointConfigurationNameToken = "UsersSvcEndpointConfigurationName";
        protected const string UsersSvcPathToken = "UsersSvcPath";

        #endregion Costs
        public UsersClient UsersClient { get; set; }

        private static UsersProxy usersProxy;
        public static UsersProxy Instance
        {
            get
            {
                if (usersProxy == null)
                {
                    usersProxy = new UsersProxy();
                }

                return usersProxy;
            }
        }
        private UsersProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, UsersSvcPathToken);
            string UsersSvcEndpointConfigurationName = ConfigurationManager.AppSettings[UsersSvcEndpointConfigurationNameToken];
            UsersClient = new UsersClient(UsersSvcEndpointConfigurationName, endpointAddress);

            UsersClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            UsersClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                EntMsgGetUsersListRequest entMsgGetUsersListRequest = new EntMsgGetUsersListRequest();
                entMsgGetUsersListRequest.MaximumRows = 5;
                entMsgGetUsersListRequest.UserType = EnmUserType.Officer;    
                var officers = UsersClient.GetOfficers(new EntMsgGetOfficersListRequest());

            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public DataObjects.Officer GetOfficerById(int OfficerID)
        {
            EntMsgGetOfficerRequest entMsgGetOfficerRequest = new EntMsgGetOfficerRequest();
            entMsgGetOfficerRequest.OfficerID = OfficerID;

            DataObjects.Officer officer = null;
            try
            {
                var response = GetOfficer(entMsgGetOfficerRequest);
                officer = new DataObjects.Officer(response.Officer);
            }
            catch (Exception)
            {

                throw;
            }
            return officer;
        }


        #region IUsers implementation

        public int AddUser(EntMsgAddUserRequest entMsgAddUserRequest)
        {
            int newUserId = -1;
            try
            {
                newUserId = UsersClient.AddUser(entMsgAddUserRequest);

            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            return newUserId;
            
        }

        public int[] GetAgenciesListForMC(int userID)
        {
            try
            {
                return UsersClient.GetAgenciesListForMC(userID);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        
        public EntMsgGetOfficerResponse GetOfficer(EntMsgGetOfficerRequest entMsgGetOfficerRequest)
        {
            EntMsgGetOfficerResponse response = null;
            try
            {
                response = UsersClient.GetOfficer(entMsgGetOfficerRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            return response;
        }
        
        public EntMsgGetOfficersListResponse GetOfficers(EntMsgGetOfficersListRequest entMsgGetOfficersListRequest)
        {
            try
            {
                return UsersClient.GetOfficers(entMsgGetOfficersListRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        
        public EntUserConfig GetUserConfigData(EntMsgGetUserConfigDataRequest entMsgGetUserConfigDataRequest)
        {
            throw new NotImplementedException();
        }
        
        public EntMsgGetUsersListResponse GetUsersList(EntMsgGetUsersListRequest entMsgGetUsersListRequest)
        {
            EntMsgGetUsersListResponse response;
            try
            {
                response = UsersClient.GetUsersList(entMsgGetUsersListRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            return response;
        }
        
        public void UpdateAgenciesListForMC(int userID, int[] listOfAgenciesID)
        {
            throw new NotImplementedException();
        }
        
        public void UpdateUser(EntMsgUpdateUserRequest entMsgUpdateUserRequest)
        {
            try
            {
                UsersClient.UpdateUser(entMsgUpdateUserRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        
        public void UpdateUserConfigData(EntMsgUpdateUserConfigDataRequest entMsgUpdateUserConfigDataRequest)
        {
            throw new NotImplementedException();
        }
        
        #endregion IUsers implementation
    }
}
