﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using Audit0 = TestingFramework.Proxies.EM.Interfaces.Audit12_0;

namespace TestingFramework.Proxies.API.Audit
{
    /// <summary>
    /// audit proxy for latest version.
    /// </summary>
    public class AuditProxy : BaseProxy,Audit0.IAudit
    {
        #region Consts
        /// <summary>
        /// token for svc path
        /// </summary>
        protected const string AuditSvcPathToken = "AuditSvcPath";
        /// <summary>
        /// token for svc configuration name
        /// </summary>
        protected const string SvcEndpointConfigurationNameToken = "AuditSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Audit0.AuditClient AuditClient { get; set; }

        #endregion Properties

        #region Proxy init
        /// <summary>
        /// ctor for the proxy
        /// </summary>
        public AuditProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, AuditSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            AuditClient = new Audit0.AuditClient(SvcEndpointConfigurationName, endpointAddress);

            AuditClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            AuditClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                GetAPIFunctions();
            }
            catch (FaultException<Audit0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        #endregion Proxy init

        #region Singleton
        private static AuditProxy _auditProxy = null;
        /// <summary>
        /// the one and only instance of the proxy.
        /// </summary>
        public static AuditProxy Instance
        {
            get
            {
                if (_auditProxy == null)
                {
                    _auditProxy = new AuditProxy();
                }

                return _auditProxy;
            }
        }
        #endregion Singleton


        /// <summary>
        /// get the API functions for audit
        /// </summary>
        /// <returns></returns>
        public List<Audit0.EntAPIFunction> GetAPIFunctions()
        {
            return ((Audit0.IAudit)AuditClient).GetAPIFunctions();
        }

        /// <summary>
        /// get the API log
        /// </summary>
        /// <param name="entMsgGetAPILogRequest"></param>
        /// <returns></returns>
        public Audit0.EntMsgGetAPILogResponse GetAPILog(Audit0.EntMsgGetAPILogRequest entMsgGetAPILogRequest)
        {
            return ((Audit0.IAudit)AuditClient).GetAPILog(entMsgGetAPILogRequest);
        }

        /// <summary>
        /// get the API modules
        /// </summary>
        /// <returns></returns>
        public List<Audit0.EntAPIModule> GetAPIModules()
        {
            return ((Audit0.IAudit)AuditClient).GetAPIModules();
        }

        List<Audit0.EntAPIFunction> Audit0.IAudit.GetAPIFunctions()
        {
            throw new NotImplementedException();
        }

        List<Audit0.EntAPIModule> Audit0.IAudit.GetAPIModules()
        {
            throw new NotImplementedException();
        }
    }
}
