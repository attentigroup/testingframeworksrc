﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;

namespace TestingFramework.Proxies.API.APIExtensions.Security
{
    public class APIExtensionsSecurityProxy : BaseProxy, ISecurity
    {
        #region Consts
        string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
        //protected const string SecuritySvcPathToken = "SecuritySvcPath";
        //protected const string SecuritySvcEndpointConfigurationNameToken = "SecuritySvcEndpointConfigurationName";
        //protected const string SecurityServicesToken = "SecurityServices";


        protected const string APIExtensionsServicesToken = "APIExtensionsServices";
        protected const string APIExtensionsSecuritySvcPathToken = "SecuritySvcPath";
        protected const string SecuritySvcEndpointConfigurationNameToken = "SecuritySvcEndpointConfigurationName";


        #endregion Consts

        #region Properties
        private SecurityClient SecurityClient { get; set; }

        #endregion Properties

        #region Singleton
        private static APIExtensionsSecurityProxy securityProxy = null;
        public static APIExtensionsSecurityProxy Instance
        {
            get
            {
                if (securityProxy == null)
                {
                    securityProxy = new APIExtensionsSecurityProxy();
                }

                return securityProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private APIExtensionsSecurityProxy()
        {
            Init();
        }

        private void Init()
        {
            string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
            string APIExtensionsServices = ConfigurationManager.AppSettings[APIExtensionsServicesToken];
            string apiExtensionsSvcPath = ConfigurationManager.AppSettings[APIExtensionsSecuritySvcPathToken];
            string apiExtensionsSvcEndpointConfigurationName = ConfigurationManager.AppSettings[SecuritySvcEndpointConfigurationNameToken];

            string fullUri = svcUri + APIExtensionsServices + apiExtensionsSvcPath;

            EndpointAddress fullSvcEndpoint = new EndpointAddress(fullUri);
            SecurityClient = new SecurityClient(apiExtensionsSvcEndpointConfigurationName, fullSvcEndpoint);

            SecurityClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            SecurityClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }


        private void CheckConectivity()
        {
            try
            {
                GetAPISecurityData();
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException != null)
                {
                    if (mse.InnerException is FaultException)
                    {
                        FaultException fe = mse.InnerException as FaultException;
                        throw new Exception(fe.Message, fe);
                    }
                }
            }
        }

        public List<EM.Interfaces.APIExtensions.Security.EntSecurity> GetGUISecurityData(string username, EM.Interfaces.APIExtensions.Security.EnmUserType? userType)
        {
            return ((ISecurity)SecurityClient).GetGUISecurityData(username, userType);
        }

        public List<EM.Interfaces.APIExtensions.Security.EntSecurity> GetGUISecurityDataByUserID(int userID)
        {
            return ((ISecurity)SecurityClient).GetGUISecurityDataByUserID(userID);
        }

        public List<EntAPIFunction> GetAPISecurityData()
        {
            return ((ISecurity)SecurityClient).GetAPISecurityData();
        }

        public Dictionary<string, EntBlockedFunction> GetBlockedFunctionsList()
        {
            return ((ISecurity)SecurityClient).GetBlockedFunctionsList();
        }

        public void SetGUISecurityData(string userName, List<EM.Interfaces.APIExtensions.Security.EntSecurity> securityData)
        {
            ((ISecurity)SecurityClient).SetGUISecurityData(userName, securityData);
        }

        public void SetAPISecurityData(List<EntAPIFunction> securityData)
        {
            ((ISecurity)SecurityClient).SetAPISecurityData(securityData);
        }

        public void SetSecurityToDefault(string userName)
        {
            ((ISecurity)SecurityClient).SetSecurityToDefault(userName);
        }

        #endregion Proxy init

        //public CommunicationState GetCommunicationServicesStatus()
        //{
        //    CommunicationState result;
        //    try
        //    {
        //        OperationContextScopeWithHeaders(SecurityClient.InnerChannel, Headers);
        //        result = SecurityClient.State;
        //    }
        //    catch (FaultException<EM.Interfaces.APIExtensions.ValidationFault> fe)
        //    {
        //        throw new Exception(fe.Detail.Errors[0].Message, fe);
        //    }
        //    return result;
        //}

        //public EM.Interfaces.APIExtensions.Security.EntSecurity[] GetGUISecurityData(string username, EM.Interfaces.APIExtensions.Security.EnmUserType? userType)
        //{
        //    return ((ISecurity)SecurityClient).GetGUISecurityData(username, userType);
        //}

        //public Task<EM.Interfaces.APIExtensions.Security.EntSecurity[]> GetGUISecurityDataAsync(string username, EM.Interfaces.APIExtensions.Security.EnmUserType? userType)
        //{
        //    return ((ISecurity)SecurityClient).GetGUISecurityDataAsync(username, userType);
        //}

        //public EM.Interfaces.APIExtensions.Security.EntSecurity[] GetGUISecurityDataByUserID(int userID)
        //{
        //    return ((ISecurity)SecurityClient).GetGUISecurityDataByUserID(userID);
        //}

        //public Task<EM.Interfaces.APIExtensions.Security.EntSecurity[]> GetGUISecurityDataByUserIDAsync(int userID)
        //{
        //    return ((ISecurity)SecurityClient).GetGUISecurityDataByUserIDAsync(userID);
        //}

        //public EntAPIFunction[] GetAPISecurityData()
        //{
        //    return ((ISecurity)SecurityClient).GetAPISecurityData();
        //}


        //public Dictionary<string, EntBlockedFunction> GetBlockedFunctionsList()
        //{
        //    return ((ISecurity)SecurityClient).GetBlockedFunctionsList();
        //}


        //public void SetGUISecurityData(string userName, EM.Interfaces.APIExtensions.Security.EntSecurity[] securityData)
        //{
        //    ((ISecurity)SecurityClient).SetGUISecurityData(userName, securityData);
        //}

        //public Task SetGUISecurityDataAsync(string userName, EM.Interfaces.APIExtensions.Security.EntSecurity[] securityData)
        //{
        //    return ((ISecurity)SecurityClient).SetGUISecurityDataAsync(userName, securityData);
        //}

        //public void SetAPISecurityData(EntAPIFunction[] securityData)
        //{
        //    ((ISecurity)SecurityClient).SetAPISecurityData(securityData);
        //}

        //public Task SetAPISecurityDataAsync(EntAPIFunction[] securityData)
        //{
        //    return ((ISecurity)SecurityClient).SetAPISecurityDataAsync(securityData);
        //}

        //public void SetSecurityToDefault(string userName)
        //{
        //    ((ISecurity)SecurityClient).SetSecurityToDefault(userName);
        //}

        //public Task SetSecurityToDefaultAsync(string userName)
        //{
        //    return ((ISecurity)SecurityClient).SetSecurityToDefaultAsync(userName);
        //}

        //List<EM.Interfaces.APIExtensions.Security.EntSecurity> ISecurity.GetGUISecurityData(string username, EM.Interfaces.APIExtensions.Security.EnmUserType? userType)
        //{
        //    throw new NotImplementedException();
        //}

        //List<EM.Interfaces.APIExtensions.Security.EntSecurity> ISecurity.GetGUISecurityDataByUserID(int userID)
        //{
        //    throw new NotImplementedException();
        //}

        //List<EntAPIFunction> ISecurity.GetAPISecurityData()
        //{
        //    throw new NotImplementedException();
        //}

        //public void SetGUISecurityData(string userName, List<EM.Interfaces.APIExtensions.Security.EntSecurity> securityData)
        //{
        //    throw new NotImplementedException();
        //}

        //public void SetAPISecurityData(List<EntAPIFunction> securityData)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
