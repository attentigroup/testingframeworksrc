﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;

using System.ServiceModel;

namespace TestingFramework.Proxies.API.Configuration
{
    public class ConfigurationProxy_1 : BaseProxy, Configuration1.IConfiguration
    {

        #region Consts
        protected const string ConfigurationSvcPathToken = "ConfigurationSvcPath"; 
        protected const string SvcEndpointConfigurationNameToken = "ConfigurationSvc-1_EndpointConfigurationName";
  
        #endregion Consts

        #region Properties
        private Configuration1.ConfigurationClient ConfigurationClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ConfigurationProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, ConfigurationSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ConfigurationClient = new Configuration1.ConfigurationClient(SvcEndpointConfigurationName, endpointAddress);

            ConfigurationClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ConfigurationClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
           // var configurationClient = ConfigurationClient.GetGPSRuleConfigurationList();

            var request = new Configuration1.EntMsgGetGPSRuleConfigurationListRequest();
            Configuration1.EntMsgGetGPSRuleConfigurationListResponse respons = GetGPSRuleConfigurationList(request);
                
        }

        #endregion Proxy init

        #region Singleton
        private static ConfigurationProxy_1 configurationProxy = null;
        public static ConfigurationProxy_1 Instance
        {
            get
            {
                if (configurationProxy == null)
                {
                    configurationProxy = new ConfigurationProxy_1();
                }

                return configurationProxy;
            }
        }
        #endregion Singleton



        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationList(Configuration1.EntMsgGetGPSRuleConfigurationListRequest entMsgGetGPSRuleConfigurationListRequest)
        {
            try
            {
                return ConfigurationClient.GetGPSRuleConfigurationList(entMsgGetGPSRuleConfigurationListRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public Task<Configuration1.EntMsgGetGPSRuleConfigurationListResponse> GetGPSRuleConfigurationListAsync(Configuration1.EntMsgGetGPSRuleConfigurationListRequest entMsgGetGPSRuleConfigurationListRequest)
        {
            throw new NotImplementedException();
        }

        public Configuration1.EntMsgGetHandlingProcedureResponse GetHandlingProcedure(Configuration1.EntMsgGetHandlingProcedureRequest entMsgGetHandlingProcedureRequest)
        {
            try
            {
                return ConfigurationClient.GetHandlingProcedure(entMsgGetHandlingProcedureRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }


        public Configuration1.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationData(Configuration1.EntMsgGetOffenderConfigurationDataRequest entMsgGetConfigurationDataRequest)
        {
            try
            {
                return ConfigurationClient.GetOffenderConfigurationData(entMsgGetConfigurationDataRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }


        public void ResetGPSRuleConfiguration(Configuration1.EntMsgResetGPSRuleConfigurationnRequest entMsgResetGPSRuleConfigurationRequest)
        {
            try
            {
                ConfigurationClient.ResetGPSRuleConfiguration(entMsgResetGPSRuleConfigurationRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

      
        public void UpdateGPSOffenderConfigurationData(Configuration1.EntMsgUpdateGPSOffenderConfigurationDataRequest entMsgUpdateGPSOffenderConfigurationDataRequest)
        {
            try
            {
                ConfigurationClient.UpdateGPSOffenderConfigurationData(entMsgUpdateGPSOffenderConfigurationDataRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                var errorText = string.Join(". ", fe.Detail.Errors.Select(x => string.Join(",", 
                    new[] { x.Rule.ToString(), x.Message.ToString(), string.Join("-", x.Field) })));
                throw new Exception(errorText, fe);
            }
        }

      
        public void UpdateGPSRuleConfiguration(Configuration1.EntMsgUpdateGPSRuleConfigurationRequest entMsgUpdateGPSRuleConfigurationRequest)
        {
            try
            {
                 ConfigurationClient.UpdateGPSRuleConfiguration(entMsgUpdateGPSRuleConfigurationRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

      
        public void UpdateHandlingProcedure(Configuration1.EntMsgUpdateHandlingProcedureRequest EntMsgUpdateHandlingProcedureRequest)
        {
            try
            {
                ConfigurationClient.UpdateHandlingProcedure(EntMsgUpdateHandlingProcedureRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

     
        public void UpdateRFOffenderConfigurationData(Configuration1.EntMsgUpdateRFOffenderConfigurationDataRequest entMsgUpdateRFOffenderConfigurationDataRequest)
        {
            try
            {
                ConfigurationClient.UpdateRFOffenderConfigurationData(entMsgUpdateRFOffenderConfigurationDataRequest);
            }
            catch (FaultException<Configuration1.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }


    }
}
