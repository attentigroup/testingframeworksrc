﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using System.ServiceModel;

namespace TestingFramework.Proxies.API.Configuration
{
    public class ConfigurationProxy : BaseProxy, Configuration0.IConfiguration
    {

        #region Consts
        protected const string ConfigurationSvcPathToken = "ConfigurationSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ConfigurationSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Configuration0.ConfigurationClient ConfigurationClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ConfigurationProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, ConfigurationSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ConfigurationClient = new Configuration0.ConfigurationClient(SvcEndpointConfigurationName, endpointAddress);

            ConfigurationClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ConfigurationClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {

            var request = new EntMsgGetGPSRuleConfigurationListRequest();
            EntMsgGetGPSRuleConfigurationListResponse respons = GetGPSRuleConfigurationList(request);

        }

        #endregion Proxy init

        #region Singleton
        private static ConfigurationProxy configurationProxy = null;
        public static ConfigurationProxy Instance
        {
            get
            {
                if (configurationProxy == null)
                {
                    configurationProxy = new ConfigurationProxy();
                }

                return configurationProxy;
            }
        }
        #endregion Singleton



        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationList(Configuration0.EntMsgGetGPSRuleConfigurationListRequest entMsgGetGPSRuleConfigurationListRequest)
        {
            try
            {
                return ConfigurationClient.GetGPSRuleConfigurationList(entMsgGetGPSRuleConfigurationListRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public Configuration0.EntMsgGetHandlingProcedureResponse GetHandlingProcedure(Configuration0.EntMsgGetHandlingProcedureRequest entMsgGetHandlingProcedureRequest)
        {
            try
            {
                return ConfigurationClient.GetHandlingProcedure(entMsgGetHandlingProcedureRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }


        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationData(Configuration0.EntMsgGetOffenderConfigurationDataRequest entMsgGetConfigurationDataRequest)
        {
            try
            {
                return ConfigurationClient.GetOffenderConfigurationData(entMsgGetConfigurationDataRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public void ResetGPSRuleConfiguration(Configuration0.EntMsgResetGPSRuleConfigurationRequest entMsgResetGPSRuleConfigurationRequest)
        {
            try
            {
                ConfigurationClient.ResetGPSRuleConfiguration(entMsgResetGPSRuleConfigurationRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public void UpdateGPSOffenderConfigurationData(Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest entMsgUpdateGPSOffenderConfigurationDataRequest)
        {
            try
            {
                ConfigurationClient.UpdateGPSOffenderConfigurationData(entMsgUpdateGPSOffenderConfigurationDataRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }


        public void UpdateGPSRuleConfiguration(Configuration0.EntMsgUpdateGPSRuleConfigurationRequest entMsgUpdateGPSRuleConfigurationRequest)
        {
            try
            {
                ConfigurationClient.UpdateGPSRuleConfiguration(entMsgUpdateGPSRuleConfigurationRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public void UpdateHandlingProcedure(Configuration0.EntMsgUpdateHandlingProcedureRequest EntMsgUpdateHandlingProcedureRequest)
        {
            try
            {
                ConfigurationClient.UpdateHandlingProcedure(EntMsgUpdateHandlingProcedureRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public void UpdateRFOffenderConfigurationData(Configuration0.EntMsgUpdateRFOffenderConfigurationDataRequest entMsgUpdateRFOffenderConfigurationDataRequest)
        {
            try
            {
                ConfigurationClient.UpdateRFOffenderConfigurationData(entMsgUpdateRFOffenderConfigurationDataRequest);
            }
            catch (FaultException<Configuration0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }
    }
}
