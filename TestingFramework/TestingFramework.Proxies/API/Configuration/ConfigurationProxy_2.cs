﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using System.ServiceModel;

namespace TestingFramework.Proxies.API.Configuration
{
    public class ConfigurationProxy_2 : BaseProxy, Configuration2.IConfiguration
    {

        #region Consts
        protected const string ConfigurationSvcPathToken = "ConfigurationSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ConfigurationSvc-2_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Configuration2.ConfigurationClient ConfigurationClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ConfigurationProxy_2()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, ConfigurationSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ConfigurationClient = new Configuration2.ConfigurationClient(SvcEndpointConfigurationName, endpointAddress);

            ConfigurationClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ConfigurationClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {

            var request = new Configuration2.EntMsgGetGPSRuleConfigurationListRequest();
            Configuration2.EntMsgGetGPSRuleConfigurationListResponse respons = GetGPSRuleConfigurationList(request);
                
        }

        #endregion Proxy init

        #region Singleton
        private static ConfigurationProxy_2 configurationProxy = null;
        public static ConfigurationProxy_2 Instance
        {
            get
            {
                if (configurationProxy == null)
                {
                    configurationProxy = new ConfigurationProxy_2();
                }

                return configurationProxy;
            }
        }
        #endregion Singleton



        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationList(Configuration2.EntMsgGetGPSRuleConfigurationListRequest entMsgGetGPSRuleConfigurationListRequest)
        {
            try
            {
                return ConfigurationClient.GetGPSRuleConfigurationList(entMsgGetGPSRuleConfigurationListRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

      
        public Configuration2.EntMsgGetHandlingProcedureResponse GetHandlingProcedure(Configuration2.EntMsgGetHandlingProcedureRequest entMsgGetHandlingProcedureRequest)
        {
            try
            {
                return ConfigurationClient.GetHandlingProcedure(entMsgGetHandlingProcedureRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }


        public Configuration2.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationData(Configuration2.EntMsgGetOffenderConfigurationDataRequest entMsgGetConfigurationDataRequest)
        {
            try
            {
                return ConfigurationClient.GetOffenderConfigurationData(entMsgGetConfigurationDataRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

        public Task<Configuration2.EntMsgGetOffenderConfigurationDataResponse> GetOffenderConfigurationDataAsync(Configuration2.EntMsgGetOffenderConfigurationDataRequest entMsgGetConfigurationDataRequest)
        {
            throw new NotImplementedException();
        }

        public void ResetGPSRuleConfiguration(Configuration2.EntMsgResetGPSRuleConfigurationnRequest entMsgResetGPSRuleConfigurationRequest)
        {
            try
            {
                ConfigurationClient.ResetGPSRuleConfiguration(entMsgResetGPSRuleConfigurationRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

      
        public void UpdateGPSOffenderConfigurationData(Configuration2.EntMsgUpdateGPSOffenderConfigurationDataRequest entMsgUpdateGPSOffenderConfigurationDataRequest)
        {
            try
            {
                ConfigurationClient.UpdateGPSOffenderConfigurationData(entMsgUpdateGPSOffenderConfigurationDataRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

      
        public void UpdateGPSRuleConfiguration(Configuration2.EntMsgUpdateGPSRuleConfigurationRequest entMsgUpdateGPSRuleConfigurationRequest)
        {
            try
            {
                 ConfigurationClient.UpdateGPSRuleConfiguration(entMsgUpdateGPSRuleConfigurationRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

           public void UpdateHandlingProcedure(Configuration2.EntMsgUpdateHandlingProcedureRequest EntMsgUpdateHandlingProcedureRequest)
        {
            try
            {
                ConfigurationClient.UpdateHandlingProcedure(EntMsgUpdateHandlingProcedureRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

    
        public void UpdateRFOffenderConfigurationData(Configuration2.EntMsgUpdateRFOffenderConfigurationDataRequest entMsgUpdateRFOffenderConfigurationDataRequest)
        {
            try
            {
                ConfigurationClient.UpdateRFOffenderConfigurationData(entMsgUpdateRFOffenderConfigurationDataRequest);
            }
            catch (FaultException<Configuration2.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message, fe.Detail.Errors[0].Rule), fe);
            }
        }

    }
}
