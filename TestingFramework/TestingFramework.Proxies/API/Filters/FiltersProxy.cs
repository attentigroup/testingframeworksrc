﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Filters0 = TestingFramework.Proxies.EM.Interfaces.Filters12_0;

namespace TestingFramework.Proxies.API.Filters
{
    public class FiltersProxy : BaseProxy, Filters0.IFilters
    {
        #region Consts
        protected const string FiltersSvcPathToken = "FiltersSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "FiltersSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Filters0.FiltersClient FiltersClient { get; set; }

        #endregion Properties

        #region Proxy init

        private FiltersProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, FiltersSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            FiltersClient = new Filters0.FiltersClient(SvcEndpointConfigurationName, endpointAddress);

            FiltersClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            FiltersClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                var filtersMetadata = FiltersClient.GetFiltersMetadata(new Filters0.GetFiltersMetadataRequest() {ScreenName = "Offender" });
            }
            catch (FaultException<Filters0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Proxy init

        #region Singleton
        private static FiltersProxy filtersProxy = null;
        public static FiltersProxy Instance
        {
            get
            {
                if (filtersProxy == null)
                {
                    filtersProxy = new FiltersProxy();
                }

                return filtersProxy;
            }
        }
        #endregion Singleton
        public Filters0.AddResponse Add(Filters0.AddRequest request)
        {
            throw new NotImplementedException();
        }

        public Filters0.DeleteResponse Delete(Filters0.DeleteRequest request)
        {
            throw new NotImplementedException();
        }

        public Filters0.GetDataResponse GetData(Filters0.GetDataRequest request)
        {
            try
            {
                return FiltersClient.GetData(request);
            }
            catch (FaultException<Filters0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Filters0.GetDefaultFilterResponse GetDefaultFilter(Filters0.GetDefaultFilterRequest request)
        {
            throw new NotImplementedException();
        }

        public Filters0.GetFiltersMetadataResponse GetFiltersMetadata(Filters0.GetFiltersMetadataRequest request)
        {
            throw new NotImplementedException();
        }

        public Filters0.GetSanityGapsResponse GetSanityGaps(Filters0.GetSanityGapsRequest request)
        {
            throw new NotImplementedException();
        }

        public Filters0.SetDefaultFilterResponse SetDefaultFilter(Filters0.SetDefaultFilterRequest request)
        {
            throw new NotImplementedException();
        }

        public Filters0.UpdateResponse Update(Filters0.UpdateRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
