﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;


namespace TestingFramework.Proxies.API.Groups
{
    public class GroupsProxy : BaseProxy, Groups0.IGroups
    {
        #region Consts
        protected const string GroupsSvcPathToken = "GroupsSvcPath";
        protected const string GroupsSvcEndpointConfigurationNameToken = "GroupsSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Groups0.GroupsClient GroupsClient { get; set; }

        #endregion Properties

        #region Singleton
        private static GroupsProxy groupsProxy = null;
        public static GroupsProxy Instance
        {
            get
            {
                if (groupsProxy == null)
                {
                    groupsProxy = new GroupsProxy();
                }

                return groupsProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private GroupsProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, GroupsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[GroupsSvcEndpointConfigurationNameToken];
            GroupsClient = new Groups0.GroupsClient(SvcEndpointConfigurationName, endpointAddress);

            GroupsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            GroupsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }


        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init
        
        public int AddGroup(Groups0.EntMsgAddGroupDetailsRequest entMsgAddGroupDetailsRequest)
        {
            try
            {
                return GroupsClient.AddGroup(entMsgAddGroupDetailsRequest);
            }
            catch (FaultException<Groups0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public IAsyncResult BeginAddGroup(Groups0.EntMsgAddGroupDetailsRequest entMsgAddGroupDetailsRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginDeleteGroup(Groups0.EntMsgDeleteGroupDetailsRequest entMsgDeleteGroupDetailsRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginDetachOffenderFromGroup(Groups0.EntMsgDetachOffenderFromGroupRequest entMsgDetachOffenderFromGroupRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginGetGroupData(Groups0.EntMsgGetGroupDetailsRequest entMsgGetGroupDetailsRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginGetGroupParamaters(Groups0.EntMsgGetGroupParametersRequest entMsgGetGroupParametersRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginUpdateGroup(Groups0.EntMsgUpdateGroupDetailsRequest entMsgUpdateGroupDetailsRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginUpdateGroupParameters(Groups0.EntMsgUpdateParametersRequest entMsgUpdateParametersRequest, AsyncCallback callback, object asyncState)
        {
            throw new NotImplementedException();
        }

        public void DeleteGroup(Groups0.EntMsgDeleteGroupDetailsRequest entMsgDeleteGroupDetailsRequest)
        {
            try
            {
                GroupsClient.DeleteGroup(entMsgDeleteGroupDetailsRequest);
            }
            catch (FaultException<Groups0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DetachOffenderFromGroup(Groups0.EntMsgDetachOffenderFromGroupRequest entMsgDetachOffenderFromGroupRequest)
        {
            throw new NotImplementedException();
        }

        public int EndAddGroup(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public void EndDeleteGroup(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public void EndDetachOffenderFromGroup(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public Groups0.EntMsgGetGroupDetailsResponse EndGetGroupData(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public Groups0.EntMsgGetGroupParametersResponse EndGetGroupParamaters(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public void EndUpdateGroup(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public void EndUpdateGroupParameters(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public Groups0.EntMsgGetGroupDetailsResponse GetGroupData(Groups0.EntMsgGetGroupDetailsRequest entMsgGetGroupDetailsRequest)
        {
            try
            {
                return GroupsClient.GetGroupData(entMsgGetGroupDetailsRequest);
            }
            catch (FaultException<Groups0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Groups0.EntMsgGetGroupParametersResponse GetGroupParamaters(Groups0.EntMsgGetGroupParametersRequest entMsgGetGroupParametersRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateGroup(Groups0.EntMsgUpdateGroupDetailsRequest entMsgUpdateGroupDetailsRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateGroupParameters(Groups0.EntMsgUpdateParametersRequest entMsgUpdateParametersRequest)
        {
            throw new NotImplementedException();
        }
    }
}
