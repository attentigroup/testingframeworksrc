﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Intefaces.APIExtensions.Authentication;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;

namespace TestingFramework.Proxies.API.APIExtension.Authentication
{
    public class APIExtensionsAuthenticationProxy : BaseProxy, IAuthentication
    {
        #region Consts
        string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
        //protected const string SecuritySvcPathToken = "SecuritySvcPath";
        //protected const string SecuritySvcEndpointConfigurationNameToken = "SecuritySvcEndpointConfigurationName";
        //protected const string SecurityServicesToken = "SecurityServices";


        protected const string APIExtensionsServicesToken = "APIExtensionsServices";
        protected const string APIExtensionsAuthenticationSvcPathToken = "AuthenticationSvcPath";
        protected const string AuthenticationSvcEndpointConfigurationNameToken = "AuthenticationSvcEndpointConfigurationName";


        #endregion Consts

        #region Properties
        private AuthenticationClient AuthenticationClient { get; set; }

        #endregion Properties

        #region Singleton
        private static APIExtensionsAuthenticationProxy AuthenticationProxy = null;
        public static APIExtensionsAuthenticationProxy Instance
        {
            get
            {
                if (AuthenticationProxy == null)
                {
                    AuthenticationProxy = new APIExtensionsAuthenticationProxy();
                }

                return AuthenticationProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private APIExtensionsAuthenticationProxy()
        {
            Init();
        }

        private void Init()
        {
            string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
            string APIExtensionsServices = ConfigurationManager.AppSettings[APIExtensionsServicesToken];
            string apiExtensionsSvcPath = ConfigurationManager.AppSettings[APIExtensionsAuthenticationSvcPathToken];
            string apiExtensionsSvcEndpointConfigurationName = ConfigurationManager.AppSettings[AuthenticationSvcEndpointConfigurationNameToken];

            string fullUri = svcUri + APIExtensionsServices + apiExtensionsSvcPath;

            EndpointAddress fullSvcEndpoint = new EndpointAddress(fullUri);
            AuthenticationClient = new AuthenticationClient(apiExtensionsSvcEndpointConfigurationName, fullSvcEndpoint);

            AuthenticationClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            AuthenticationClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };
        }

        public EntAPIRequestData ValidateUser(string username, string password, string target)
        {
            return ((IAuthentication)AuthenticationClient).ValidateUser(username, password, target);
        }

        public bool ChangePassword(string username, string password, string newPassword, string confirmNewPassword)
        {
            return ((IAuthentication)AuthenticationClient).ChangePassword(username, password, newPassword, confirmNewPassword);
        }

        public bool ResetForgottenPassword(string username, string targetEmail)
        {
            return ((IAuthentication)AuthenticationClient).ResetForgottenPassword(username, targetEmail);
        }

        public bool UnlockUser(string username, int sender)
        {
            return ((IAuthentication)AuthenticationClient).UnlockUser(username, sender);
        }

        public bool EnableDisableUser(string username, bool enable, int sender)
        {
            return ((IAuthentication)AuthenticationClient).EnableDisableUser(username, enable, sender);
        }

        public List<EntLanguage> GetSupportedLanguages()
        {
            throw new NotImplementedException();
        }
    }
}
#endregion