﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestingFramework.Proxies.API.Offenders
{
    /// <summary>
    /// implement proxy for Offenders end point
    /// </summary>
    public class OffendersProxy : BaseProxy, Offenders0.IOffenders
    {
        #region Consts
        protected const string OffendersSvcPathToken = "OffendersSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "OffendersSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Offenders0.OffendersClient OffendersClient { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

        #endregion Properties

        #region Proxy init

        private OffendersProxy()
        {
            Init();
        }

        public OffendersProxy(string userName, string password)
        {
            UserName = userName;
            Password = password;
            Init();
        }


        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, OffendersSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            OffendersClient = new Offenders0.OffendersClient(SvcEndpointConfigurationName, endpointAddress);

            if (string.IsNullOrEmpty(UserName))
            {
                OffendersClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            }
            else
            {
                OffendersClient.ClientCredentials.UserName.UserName = UserName;
            }

            if (string.IsNullOrEmpty(Password))
            {
                OffendersClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];
            }
            else
            {
                OffendersClient.ClientCredentials.UserName.Password = Password;
            }

            //OffendersClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            //OffendersClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var OffendersCounters = OffendersClient.GetOffendersCounters();
        }

        #endregion Proxy init

        #region Singleton
        private static OffendersProxy offendersProxy = null;
        public static OffendersProxy Instance
        {
            get
            {
                if (offendersProxy == null)
                {
                    offendersProxy = new OffendersProxy();
                }

                return offendersProxy;
            }
        }
        #endregion Singleton

        #region Private methods

        private string ParseValidationFault(FaultException<Offenders0.ValidationFault> fe)
        {
            var errorText = string.Join(". ", fe.Detail.Errors.Select(x => string.Join(",",
                    new[] { x.Rule.ToString(), x.Message.ToString(), string.Join("-", x.Field) })));
            return errorText;
        }
        #endregion Private methods

        #region Interface implementation

        public Offenders0.EntMsgAddOffenderResponse AddOffender(Offenders0.EntMsgAddOffenderRequest entMsgAddOffenderRequest)
        {
            try
            {   
                return OffendersClient.AddOffender(entMsgAddOffenderRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                var errorText = ParseValidationFault(fe);
                throw new Exception(errorText, fe);
            }
            catch(FaultException fe )
            {
                var errorMessage = $"fe.Message = {fe.Message}, action = {fe.Action} code {fe.Code}, data {fe.Data.ToString()}";
                throw new Exception(errorMessage, fe);
            }
        }

        public Offenders0.EntMsgAddOffenderAddressResponse AddOffenderAddress(Offenders0.EntMsgAddOffenderAddressRequest entMsgAddAddressRequest)
        {
            try
            {
                return OffendersClient.AddOffenderAddress(entMsgAddAddressRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgAddContactResponse AddOffenderContact(Offenders0.EntMsgAddContactRequest entMsgAddOffenderContactRequest)
        {
            try
            {
                return OffendersClient.AddOffenderContact(entMsgAddOffenderContactRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgAddPictureResponse AddOffenderPicture(Offenders0.EntMsgAddPictureRequest entMsgAddOffenderPictureRequest)
        {
            try
            {
                return OffendersClient.AddOffenderPicture(entMsgAddOffenderPictureRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffender(Offenders0.EntMsgDeleteOffenderRequest entMsgDeleteOffenderRequest)
        {
            try
            {
                OffendersClient.DeleteOffender(entMsgDeleteOffenderRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffenderAddress(Offenders0.EntMsgDeleteOffenderAddressRequest entMsgDeleteAddressRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderAddress(entMsgDeleteAddressRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffenderContact(Offenders0.EntMsgDeleteContactRequest entMsgDeleteOffenderContactRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderContact(entMsgDeleteOffenderContactRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffenderPicture(Offenders0.EntMsgDeletePictureRequest entMsgDeletePictureRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderPicture(entMsgDeletePictureRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgGetDeletedOffendersResponse GetDeletedOffenders(Offenders0.EntMsgGetDeletedOffendersRequest entMsgGetDeletedOffendersRequest)
        {
            try
            {
                return OffendersClient.GetDeletedOffenders(entMsgGetDeletedOffendersRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsList(Offenders0.EntMsgGetContactsListRequest entMsgGetOffenderContactsListRequest)
        {
            try
            {
                return OffendersClient.GetOffenderContactsList(entMsgGetOffenderContactsListRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgGetOffenderCurrentStatusResponse GetOffenderCurrentStatus(Offenders0.EntMsgGetOffenderCurrentStatusRequest entMsgGetOffenderStatusRequest)
        {
            try
            {
                return OffendersClient.GetOffenderCurrentStatus(entMsgGetOffenderStatusRequest);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Offenders0.EntMsgGetPictureIDListResponse GetOffenderPictureIDList(Offenders0.EntMsgGetPictureIDListRequest entMsgGetOffenderPictureIDListRequest)
        {
            Offenders0.EntMsgGetPictureIDListResponse result = null;
            try
            {
                result = OffendersClient.GetOffenderPictureIDList(entMsgGetOffenderPictureIDListRequest);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public Offenders0.EntMsgGetPicturesResponse GetOffenderPictures(Offenders0.EntMsgGetPicturesRequest entMsgGetOffenderPicturesRequest)
        {
            try
            {
                return OffendersClient.GetOffenderPictures(entMsgGetOffenderPicturesRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgGetOffendersResponse GetOffenders(Offenders0.EntMsgGetOffendersListRequest entMsgGetOffendersRequest)
        {
            try
            {
                return OffendersClient.GetOffenders(entMsgGetOffendersRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders0.EntMsgGetOffendersCountersResponse GetOffendersCounters()
        {
            try
            {
                return OffendersClient.GetOffendersCounters();
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {

                throw new Exception(fe.Detail.Errors[0].Message + fe.Detail.Errors[0].Rule, fe);
            }
        }

        public void ReallocateActiveOffender(Offenders0.EntMsgReallocateActiveOffenderRequest entMsgReallocateActiveOffender)
        {
            try
            {
                OffendersClient.ReallocateActiveOffender(entMsgReallocateActiveOffender);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {

                throw new Exception(fe.Detail.Errors[0].Message + fe.Detail.Errors[0].Rule, fe);
            }
        }

        public void UpdateOffender(Offenders0.EntMsgUpdateOffenderRequest entMsgUpdateOffenderRequest)
        {
            try
            {
                OffendersClient.UpdateOffender(entMsgUpdateOffenderRequest);
            }
            catch (FaultException<Offenders0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void UpdateOffenderAddress(Offenders0.EntMsgUpdateOffenderAddressRequest entMsgUpdateAddressRequest)
        {
            try
            {
                OffendersClient.UpdateOffenderAddress(entMsgUpdateAddressRequest);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void UpdateOffenderContact(Offenders0.EntMsgUpdateContactRequest entMsgUpdateOffenderContactRequest)
        {
            try
            {
                OffendersClient.UpdateOffenderContact(entMsgUpdateOffenderContactRequest);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Offenders0.EntCustomFieldDefinition[] GetCustomFieldsDefinitions()
        {
            return ((Offenders0.IOffenders)OffendersClient).GetCustomFieldsDefinitions();
        }
        #endregion Interface implementation
    }
}
