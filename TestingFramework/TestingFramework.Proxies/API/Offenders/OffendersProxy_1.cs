﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;

#region API refs

using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;

#endregion

namespace TestingFramework.Proxies.API.Offenders
{
    public class OffendersProxy_1 : BaseProxy, Offenders1.IOffenders
    {

        #region Consts
        protected const string OffendersSvcPathToken = "OffendersSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "OffendersSvc-1_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Offenders1.OffendersClient OffendersClient { get; set; }

        #endregion Properties

        #region Proxy init

        private OffendersProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, OffendersSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            OffendersClient = new Offenders1.OffendersClient(SvcEndpointConfigurationName, endpointAddress);

            OffendersClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            OffendersClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var OffendersCounters = OffendersClient.GetOffendersCounters();
        }

        #endregion Proxy init

        #region Singleton
        private static OffendersProxy_1 offendersProxy_1 = null;
        public static OffendersProxy_1 Instance
        {
            get
            {
                if (offendersProxy_1 == null)
                {
                    offendersProxy_1 = new OffendersProxy_1();
                }

                return offendersProxy_1;
            }
        }
        #endregion Singleton

        public Offenders1.EntMsgAddOffenderResponse AddOffender(Offenders1.EntMsgAddOffenderRequest entMsgAddOffenderRequest)
        {
            try
            {
                return OffendersClient.AddOffender(entMsgAddOffenderRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders1.EntMsgAddOffenderAddressResponse AddOffenderAddress(Offenders1.EntMsgAddOffenderAddressRequest entMsgAddAddressRequest)
        {
            try
            {
                return OffendersClient.AddOffenderAddress(entMsgAddAddressRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders1.EntMsgAddOffenderAddressResponse> AddOffenderAddressAsync(Offenders1.EntMsgAddOffenderAddressRequest entMsgAddAddressRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Offenders1.EntMsgAddOffenderResponse> AddOffenderAsync(Offenders1.EntMsgAddOffenderRequest entMsgAddOffenderRequest)
        {
            throw new NotImplementedException();
        }

        public void AddOffenderContact(Offenders1.EntMsgAddOffenderContactRequest entMsgAddOffenderContactRequest)
        {
            try
            {
                OffendersClient.AddOffenderContact(entMsgAddOffenderContactRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddOffenderContactAsync(Offenders1.EntMsgAddOffenderContactRequest entMsgAddOffenderContactRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgAddOffenderPictureResponse AddOffenderPicture(Offenders1.EntMsgAddOffenderPictureRequest entMsgAddOffenderPictureRequest)
        {
            try
            {
                return OffendersClient.AddOffenderPicture(entMsgAddOffenderPictureRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders1.EntMsgAddOffenderPictureResponse> AddOffenderPictureAsync(Offenders1.EntMsgAddOffenderPictureRequest entMsgAddOffenderPictureRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteOffender(Offenders1.EntMsgDeleteOffenderRequest entMsgDeleteOffenderRequest)
        {
            try
            {
                OffendersClient.DeleteOffender(entMsgDeleteOffenderRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffenderAddress(Offenders1.EntMsgDeleteOffenderAddressRequest entMsgDeleteAddressRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderAddress(entMsgDeleteAddressRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteOffenderAddressAsync(Offenders1.EntMsgDeleteOffenderAddressRequest entMsgDeleteAddressRequest)
        {
            throw new NotImplementedException();
        }

        public Task DeleteOffenderAsync(Offenders1.EntMsgDeleteOffenderRequest entMsgDeleteOffenderRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteOffenderContact(Offenders1.EntMsgDeleteOffenderContactRequest entMsgDeleteOffenderContactRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderContact(entMsgDeleteOffenderContactRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteOffenderContactAsync(Offenders1.EntMsgDeleteOffenderContactRequest entMsgDeleteOffenderContactRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteOffenderPicture(Offenders1.EntMsgDeleteOffenderPicture entMsgDeletePictureRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderPicture(entMsgDeletePictureRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteOffenderPictureAsync(Offenders1.EntMsgDeleteOffenderPicture entMsgDeletePictureRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgGetDeletedOffendersResponse GetDeletedOffenders(Offenders1.EntMsgGetDeletedOffendersRequest entMsgGetDeletedOffendersRequest)
        {
            try
            {
                return OffendersClient.GetDeletedOffenders(entMsgGetDeletedOffendersRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders1.EntMsgGetDeletedOffendersResponse> GetDeletedOffendersAsync(Offenders1.EntMsgGetDeletedOffendersRequest entMsgGetDeletedOffendersRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgGetOffenderContactsListResponse GetOffenderContactsList(Offenders1.EntMsgGetOffenderContactsListRequest entMsgGetOffenderContactsListRequest)
        {
            try
            {
                return OffendersClient.GetOffenderContactsList(entMsgGetOffenderContactsListRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders1.EntMsgGetOffenderContactsListResponse> GetOffenderContactsListAsync(Offenders1.EntMsgGetOffenderContactsListRequest entMsgGetOffenderContactsListRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgGetOffenderPictureIDListResponse GetOffenderPictureIDList(Offenders1.EntMsgGetOffenderPictureIDListRequest entMsgGetOffenderPictureIDListRequest)
        {
            try
            {
                return OffendersClient.GetOffenderPictureIDList(entMsgGetOffenderPictureIDListRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders1.EntMsgGetOffenderPictureIDListResponse> GetOffenderPictureIDListAsync(Offenders1.EntMsgGetOffenderPictureIDListRequest entMsgGetOffenderPictureIDListRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgGetOffenderPicturesResponse GetOffenderPictures(Offenders1.EntMsgGetOffenderPicturesRequest entMsgGetOffenderPicturesRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Offenders1.EntMsgGetOffenderPicturesResponse> GetOffenderPicturesAsync(Offenders1.EntMsgGetOffenderPicturesRequest entMsgGetOffenderPicturesRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgGetOffendersResponse GetOffenders(Offenders1.EntMsgGetOffendersListRequest entMsgGetOffendersRequest)
        {
            try
            {
                return OffendersClient.GetOffenders(entMsgGetOffendersRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders1.EntMsgGetOffendersResponse> GetOffendersAsync(Offenders1.EntMsgGetOffendersListRequest entMsgGetOffendersRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders1.EntMsgGetOffendersCountersResponse GetOffendersCounters()
        {
            throw new NotImplementedException();
        }

        public Task<Offenders1.EntMsgGetOffendersCountersResponse> GetOffendersCountersAsync()
        {
            throw new NotImplementedException();
        }

        public void ReallocateActiveOffender(Offenders1.EntMsgReallocateActiveOffender entMsgReallocateActiveOffender)
        {
            throw new NotImplementedException();
        }

        public Task ReallocateActiveOffenderAsync(Offenders1.EntMsgReallocateActiveOffender entMsgReallocateActiveOffender)
        {
            throw new NotImplementedException();
        }

        public void UpdateOffender(Offenders1.EntMsgUpdateOffenderRequest entMsgUpdateOffenderRequest)
        {
            try
            {
                OffendersClient.UpdateOffender(entMsgUpdateOffenderRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void UpdateOffenderAddress(Offenders1.EntMsgUpdateOffenderAddressRequest entMsgUpdateAddressRequest)
        {
            try
            {
                OffendersClient.UpdateOffenderAddress(entMsgUpdateAddressRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateOffenderAddressAsync(Offenders1.EntMsgUpdateOffenderAddressRequest entMsgUpdateAddressRequest)
        {
            throw new NotImplementedException();
        }

        public Task UpdateOffenderAsync(Offenders1.EntMsgUpdateOffenderRequest entMsgUpdateOffenderRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateOffenderContact(Offenders1.EntMsgUpdateOffenderContactRequest entMsgUpdateOffenderContactRequest)
        {
            try
            {
                OffendersClient.UpdateOffenderContact(entMsgUpdateOffenderContactRequest);
            }
            catch (FaultException<Offenders1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateOffenderContactAsync(Offenders1.EntMsgUpdateOffenderContactRequest entMsgUpdateOffenderContactRequest)
        {
            throw new NotImplementedException();
        }
    }
}
