﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;

#region API refs

using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace TestingFramework.Proxies.API.Offenders
{
    public class OffendersProxy_2 : BaseProxy, Offenders2.IOffenders
    {
        #region Consts
        protected const string OffendersSvcPathToken = "OffendersSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "OffendersSvc-2_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Offenders2.OffendersClient OffendersClient { get; set; }

        #endregion Properties

        #region Proxy init

        private OffendersProxy_2()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, OffendersSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            OffendersClient = new Offenders2.OffendersClient(SvcEndpointConfigurationName, endpointAddress);

            OffendersClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            OffendersClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var OffendersCounters = OffendersClient.GetOffendersCounters();
        }

        #endregion Proxy init

        #region Singleton
        private static OffendersProxy_2 offendersProxy_2 = null;
        public static OffendersProxy_2 Instance
        {
            get
            {
                if (offendersProxy_2 == null)
                {
                    offendersProxy_2 = new OffendersProxy_2();
                }

                return offendersProxy_2;
            }
        }
        #endregion Singleton


        public Offenders2.EntMsgAddOffenderResponse AddOffender(Offenders2.EntMsgAddOffenderRequest entMsgAddOffenderRequest)
        {
            try
            {
                return OffendersClient.AddOffender(entMsgAddOffenderRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Offenders2.EntMsgAddOffenderAddressResponse AddOffenderAddress(Offenders2.EntMsgAddOffenderAddressRequest entMsgAddAddressRequest)
        {
            try
            {
                return OffendersClient.AddOffenderAddress(entMsgAddAddressRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders2.EntMsgAddOffenderAddressResponse> AddOffenderAddressAsync(Offenders2.EntMsgAddOffenderAddressRequest entMsgAddAddressRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Offenders2.EntMsgAddOffenderResponse> AddOffenderAsync(Offenders2.EntMsgAddOffenderRequest entMsgAddOffenderRequest)
        {
            throw new NotImplementedException();
        }

        public void AddOffenderContact(Offenders2.EntMsgAddOffenderContactRequest entMsgAddOffenderContactRequest)
        {
            try
            {
                OffendersClient.AddOffenderContact(entMsgAddOffenderContactRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddOffenderContactAsync(Offenders2.EntMsgAddOffenderContactRequest entMsgAddOffenderContactRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgAddOffenderPictureResponse AddOffenderPicture(Offenders2.EntMsgAddOffenderPictureRequest entMsgAddOffenderPictureRequest)
        {
            try
            {
                return OffendersClient.AddOffenderPicture(entMsgAddOffenderPictureRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders2.EntMsgAddOffenderPictureResponse> AddOffenderPictureAsync(Offenders2.EntMsgAddOffenderPictureRequest entMsgAddOffenderPictureRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteOffender(Offenders2.EntMsgDeleteOffenderRequest entMsgDeleteOffenderRequest)
        {
            try
            {
                OffendersClient.DeleteOffender(entMsgDeleteOffenderRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void DeleteOffenderAddress(Offenders2.EntMsgDeleteOffenderAddressRequest entMsgDeleteAddressRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderAddress(entMsgDeleteAddressRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteOffenderAddressAsync(Offenders2.EntMsgDeleteOffenderAddressRequest entMsgDeleteAddressRequest)
        {
            throw new NotImplementedException();
        }

        public Task DeleteOffenderAsync(Offenders2.EntMsgDeleteOffenderRequest entMsgDeleteOffenderRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteOffenderContact(Offenders2.EntMsgDeleteOffenderContactRequest entMsgDeleteOffenderContactRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderContact(entMsgDeleteOffenderContactRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteOffenderContactAsync(Offenders2.EntMsgDeleteOffenderContactRequest entMsgDeleteOffenderContactRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteOffenderPicture(Offenders2.EntMsgDeleteOffenderPicture entMsgDeletePictureRequest)
        {
            try
            {
                OffendersClient.DeleteOffenderPicture(entMsgDeletePictureRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteOffenderPictureAsync(Offenders2.EntMsgDeleteOffenderPicture entMsgDeletePictureRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgGetDeletedOffendersResponse GetDeletedOffenders(Offenders2.EntMsgGetDeletedOffendersRequest entMsgGetDeletedOffendersRequest)
        {
            try
            {
                return OffendersClient.GetDeletedOffenders(entMsgGetDeletedOffendersRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders2.EntMsgGetDeletedOffendersResponse> GetDeletedOffendersAsync(Offenders2.EntMsgGetDeletedOffendersRequest entMsgGetDeletedOffendersRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgGetOffenderContactsListResponse GetOffenderContactsList(Offenders2.EntMsgGetOffenderContactsListRequest entMsgGetOffenderContactsListRequest)
        {
            try
            {
                return OffendersClient.GetOffenderContactsList(entMsgGetOffenderContactsListRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders2.EntMsgGetOffenderContactsListResponse> GetOffenderContactsListAsync(Offenders2.EntMsgGetOffenderContactsListRequest entMsgGetOffenderContactsListRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgGetOffenderPictureIDListResponse GetOffenderPictureIDList(Offenders2.EntMsgGetOffenderPictureIDListRequest entMsgGetOffenderPictureIDListRequest)
        {
            try
            {
                return OffendersClient.GetOffenderPictureIDList(entMsgGetOffenderPictureIDListRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders2.EntMsgGetOffenderPictureIDListResponse> GetOffenderPictureIDListAsync(Offenders2.EntMsgGetOffenderPictureIDListRequest entMsgGetOffenderPictureIDListRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgGetOffenderPicturesResponse GetOffenderPictures(Offenders2.EntMsgGetOffenderPicturesRequest entMsgGetOffenderPicturesRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Offenders2.EntMsgGetOffenderPicturesResponse> GetOffenderPicturesAsync(Offenders2.EntMsgGetOffenderPicturesRequest entMsgGetOffenderPicturesRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgGetOffendersResponse GetOffenders(Offenders2.EntMsgGetOffendersListRequest entMsgGetOffendersRequest)
        {
            try
            {
                return OffendersClient.GetOffenders(entMsgGetOffendersRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Offenders2.EntMsgGetOffendersResponse> GetOffendersAsync(Offenders2.EntMsgGetOffendersListRequest entMsgGetOffendersRequest)
        {
            throw new NotImplementedException();
        }

        public Offenders2.EntMsgGetOffendersCountersResponse GetOffendersCounters()
        {
            throw new NotImplementedException();
        }

        public Task<Offenders2.EntMsgGetOffendersCountersResponse> GetOffendersCountersAsync()
        {
            throw new NotImplementedException();
        }

        public void UpdateOffender(Offenders2.EntMsgUpdateOffenderRequest entMsgUpdateOffenderRequest)
        {
            try
            {
                OffendersClient.UpdateOffender(entMsgUpdateOffenderRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void UpdateOffenderAddress(Offenders2.EntMsgUpdateOffenderAddressRequest entMsgUpdateAddressRequest)
        {
            try
            {
                OffendersClient.UpdateOffenderAddress(entMsgUpdateAddressRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateOffenderAddressAsync(Offenders2.EntMsgUpdateOffenderAddressRequest entMsgUpdateAddressRequest)
        {
            throw new NotImplementedException();
        }

        public Task UpdateOffenderAsync(Offenders2.EntMsgUpdateOffenderRequest entMsgUpdateOffenderRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateOffenderContact(Offenders2.EntMsgUpdateOffenderContactRequest entMsgUpdateOffenderContactRequest)
        {
            try
            {
                OffendersClient.UpdateOffenderContact(entMsgUpdateOffenderContactRequest);
            }
            catch (FaultException<Offenders2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateOffenderContactAsync(Offenders2.EntMsgUpdateOffenderContactRequest entMsgUpdateOffenderContactRequest)
        {
            throw new NotImplementedException();
        }
    }
}
