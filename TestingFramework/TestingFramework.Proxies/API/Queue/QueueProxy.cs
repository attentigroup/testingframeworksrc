﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace TestingFramework.Proxies.API.Queue
{
    public class QueueProxy : BaseProxy, Queue0.IQueue
    {
        #region Consts
        protected const string QueueSvcPathToken = "QueueSvcPath";
        protected const string QueueSvcEndpointConfigurationNameToken = "QueueSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Queue0.QueueClient QueueClient { get; set; }

        #endregion Properties

        #region Singleton
        private static QueueProxy queueProxy = null;
        public static QueueProxy Instance
        {
            get
            {
                if (queueProxy == null)
                {
                    queueProxy = new QueueProxy();
                }

                return queueProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private QueueProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, QueueSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[QueueSvcEndpointConfigurationNameToken];
            QueueClient = new Queue0.QueueClient(SvcEndpointConfigurationName, endpointAddress);

            QueueClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            QueueClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }


        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        public bool Delete(int requestID, int filterID, Queue0.EnmResultCode resultCode)
        {
            try
            {
                return QueueClient.Delete(requestID, filterID, resultCode);
            }
            catch (FaultException<Queue0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<bool> DeleteAsync(int requestID, int filterID, Queue0.EnmResultCode resultCode)
        {
            throw new NotImplementedException();
        }

        public string[] GetCountersForQueue(int[] filterIDs, Queue0.EnmResultCode[] resultCodes)
        {
            try
            {
                return QueueClient.GetCountersForQueue(filterIDs, resultCodes);
            }
            catch (FaultException<Queue0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<string[]> GetCountersForQueueAsync(int[] filterIDs, Queue0.EnmResultCode[] resultCodes)
        {
            throw new NotImplementedException();
        }

        public Queue0.EntMessageGetQueueResponse GetQueue(Queue0.EntMessageGetQueueRequest entMessageGetQueueRequest)
        {
            try
            {
                return QueueClient.GetQueue(entMessageGetQueueRequest);
            }
            catch (FaultException<Queue0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Queue0.EntMessageGetQueueResponse> GetQueueAsync(Queue0.EntMessageGetQueueRequest entMessageGetQueueRequest)
        {
            throw new NotImplementedException();
        }
    }
}

