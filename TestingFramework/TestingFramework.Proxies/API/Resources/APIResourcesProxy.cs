﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using Resources_0 = TestingFramework.Proxies.EM.Interfaces.Resources12_0;

namespace TestingFramework.Proxies.API.Resources
{
    public class APIResourcesProxy : BaseProxy, Resources_0.IResources
    {
        //https://localhost:8087/svc/Version12.0/Resources.svc?singleWsdl
        #region Consts

        protected const string ResourcesSvcEndpointConfigurationNameToken = "ResourcesSvcEndpointConfigurationName";
        protected const string ResourcesSvcPathToken = "ResourcesSvcPath";

        #endregion Costs

        #region Properties

        public Resources_0.ResourcesClient ApiResourcesClient { get; set; }

        #endregion Properties

        #region Singleton
        private APIResourcesProxy()
        {
            Init();
        }

        private static APIResourcesProxy _aPIResourcesProxy = null;
        public static APIResourcesProxy Instance
        {
            get
            {
                if (_aPIResourcesProxy == null)
                {
                    _aPIResourcesProxy = new APIResourcesProxy();
                }

                return _aPIResourcesProxy;
            }
        }
        #endregion Singleton

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, ResourcesSvcPathToken);
            string UsersSvcEndpointConfigurationName = ConfigurationManager.AppSettings[ResourcesSvcEndpointConfigurationNameToken];
            ApiResourcesClient = new Resources_0.ResourcesClient(UsersSvcEndpointConfigurationName, endpointAddress);

            ApiResourcesClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ApiResourcesClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var result = ApiResourcesClient.GetCitiesList(new Resources_0.EntMsgGetCitiesListRequest());

        }

        #region IResources Interfcae inplementation

        public Dictionary<string, Dictionary<string, string>> GetAllHardwareDefaults()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetAllHardwareDefaults();
        }

        public Resources_0.EntMsgGetAllLookupsResponse GetAllLookups()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetAllLookups();
        }

        public Resources_0.EntMsgGetAllParametersResponse GetAllParameters()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetAllParameters();
        }

        public List<Resources_0.EntResource> GetAllResourceData(Resources_0.EnmResourcePlatform platform)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetAllResourceData(platform);
        }

        public List<Resources_0.EntAppColor> GetApplicationColorList()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetApplicationColorList();
        }

        public Resources_0.EntMsgGetCommunicationProviderListResponse GetCellularCommunicationProviders()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetCellularCommunicationProviders();
        }

        public Resources_0.EntMsgGetCitiesListResponse GetCitiesList(Resources_0.EntMsgGetCitiesListRequest entMsgGetCitiesListRequest)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetCitiesList(entMsgGetCitiesListRequest);
        }

        public Resources_0.EntMsgGetLanguageListResponse GetLanguages()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetLanguages();
        }

        public DateTime GetLastTransUpdateDate()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetLastTransUpdateDate();
        }

        public Resources_0.EntMsgGetLookupResponse GetLookup(Resources_0.EntMsgGetLookupRequest entMsgGetLookupRequest)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetLookup(entMsgGetLookupRequest);
        }

        public Resources_0.EntMsgGetPagerDeviceTypesResponse GetPagerDeviceTypeList(Resources_0.EntMsgGetPagerDeviceTypesRequest entMsgGetPagerDeviceTypeListRequest)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetPagerDeviceTypeList(entMsgGetPagerDeviceTypeListRequest);
        }

        public Resources_0.EntMsgGetPagerProvidersListResponse GetPagerProvidersList()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetPagerProvidersList();
        }

        public Resources_0.EntMsgGetScheduledTaskSubTypesResponse GetScheduledTaskSubTypes(Resources_0.EntMsgGetScheduledTaskSubTypesRequest entMsgGetScheduledTaskSubTypesRequest)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetScheduledTaskSubTypes(entMsgGetScheduledTaskSubTypesRequest);
        }

        public Resources_0.EntMsgGetScheduledTaskTypesResponse GetScheduledTaskTypes()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetScheduledTaskTypes();
        }

        public Resources_0.EntMsgGetStateListResponse GetStatesList(Resources_0.EntMsgGetStateListRequest entMsgGetStateListRequest)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetStatesList(entMsgGetStateListRequest);
        }

        public Resources_0.EntMsgGetSupportedProgramTypesResponse GetSupportedProgramTypes()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetSupportedProgramTypes();
        }

        public Resources_0.EntMsgGetTimeZonesListResponse GetTimeZones()
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetTimeZones();
        }

        public Dictionary<string, Resources_0.Range> GetZonesConfigParameters(Resources_0.EnmProgramType program)
        {
            return ((Resources_0.IResources)ApiResourcesClient).GetZonesConfigParameters(program);
        }

        public void ResetApplicationColors()
        {
            ((Resources_0.IResources)ApiResourcesClient).ResetApplicationColors();
        }

        public void UpdateApplicationColors(List<Resources_0.EntAppColor> colors)
        {
            ((Resources_0.IResources)ApiResourcesClient).UpdateApplicationColors(colors);
        }

        public void UpdateEmsSystable(Dictionary<Resources_0.EnmDBAction, List<Resources_0.EntSysTableItem>> values)
        {
            ((Resources_0.IResources)ApiResourcesClient).UpdateEmsSystable(values);
        }

        public void UpdateResourceTranslation(string resourceID, string translationKey, string translation, bool isUpdateAll, string clientVersion)
        {
            ((Resources_0.IResources)ApiResourcesClient).UpdateResourceTranslation(resourceID, translationKey, translation, isUpdateAll, clientVersion);
        }

        #endregion IResources Interfcae inplementation


        public const string SYS_VERSION_ID_KEY = "sys_version_id";


        public string GetSystemVersion()
        {
            string returnValue = string.Empty;

            try
            {
                var response = GetAllParameters();
                response.ParametersList.TryGetValue(SYS_VERSION_ID_KEY, out returnValue);
            }
            catch (FaultException<Resources_0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (Exception ex)
            {
                throw;
            }

            return returnValue;
        }

        public void SaveCitiesList(Dictionary<Resources_0.EnmDBAction, List<Resources_0.EntCity>> citiesList)
        {
            throw new NotImplementedException();
        }

        public void SaveStatesList(Dictionary<Resources_0.EnmDBAction, List<Resources_0.EntState>> statesList)
        {
            throw new NotImplementedException();
        }

        public List<Resources_0.EntDeviceFeature> GetDeviceFeatureList()
        {
            throw new NotImplementedException();
        }

        public void SaveDevicetFeatures(List<Resources_0.EntDeviceFeature> list)
        {
            throw new NotImplementedException();
        }

        public Resources_0.EntMsgGetDeferredEventsListResponse DeferredEventList()
        {
            throw new NotImplementedException();
        }

        public void SaveDeferredEventList(Resources_0.EntMsgUpdateEventsRequest deferredEvents)
        {
            throw new NotImplementedException();
        }
    }
}
