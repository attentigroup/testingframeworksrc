﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

#region API refs
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestingFramework.Proxies.API.Events
{
    public class EventsProxy_2 : BaseProxy, Events2.IEvents
    {
        #region Consts
        protected const string EventsSvcPathToken = "EventsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "EventsSvc-2_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Events2.EventsClient EventsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private EventsProxy_2()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, EventsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            EventsClient = new Events2.EventsClient(SvcEndpointConfigurationName, endpointAddress);

            EventsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            EventsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                var eventsMetadata = EventsClient.GetEventsMetadata();
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Proxy init

        #region Singleton
        private static EventsProxy_2 eventsProxy_2 = null;
        public static EventsProxy_2 Instance
        {
            get
            {
                if (eventsProxy_2 == null)
                {
                    eventsProxy_2 = new EventsProxy_2();
                }

                return eventsProxy_2;
            }
        }
        #endregion Singleton

        public void AddHandlingAction(Events2.EntMsgAddHandlingActionRequest entMsgAddHandlingActionRequest)
        {
            try
            {
                EventsClient.AddHandlingAction(entMsgAddHandlingActionRequest);
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddHandlingActionAsync(Events2.EntMsgAddHandlingActionRequest entMsgAddHandlingActionRequest)
        {
            throw new NotImplementedException();
        }

        public Events2.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResult(Events2.EntMsgGetAlcoholTestResultRequest entMsgGetAlcoholTestResultRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Events2.EntMsgGetAlcoholTestResultResponse> GetAlcoholTestResultAsync(Events2.EntMsgGetAlcoholTestResultRequest entMsgGetAlcoholTestResultRequest)
        {
            throw new NotImplementedException();
        }

        public Events2.EntMsgGetEventsResponse GetEvents(Events2.EntMsgGetEventsRequest entMsgGetEventsRequest)
        {
            try
            {
                return EventsClient.GetEvents(entMsgGetEventsRequest);
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events2.EntMsgGetEventsResponse> GetEventsAsync(Events2.EntMsgGetEventsRequest entMsgGetEventsRequest)
        {
            throw new NotImplementedException();
        }

        public Events2.EntMsgGetEventsMetadataResponse GetEventsMetadata()
        {
            try
            {
                return EventsClient.GetEventsMetadata();
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events2.EntMsgGetEventsMetadataResponse> GetEventsMetadataAsync()
        {
            throw new NotImplementedException();
        }

        public Events2.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQuery(Events2.EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest)
        {
            try
            {
                return EventsClient.GetHandlingActionsByQuery(entMsgGetHandlingActionsRequest);
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events2.EntMsgGetHandlingActionsByQueryResponse> GetHandlingActionsByQueryAsync(Events2.EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest)
        {
            throw new NotImplementedException();
        }

        public Events2.EntMsgGetOffenderEventStatusResponse GetOffenderEventStatus(Events2.EntMsgGetOffenderEventStatusRequest entMsgGetOffenderEventStatusRequest)
        {
            try
            {
                return EventsClient.GetOffenderEventStatus(entMsgGetOffenderEventStatusRequest);
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events2.EntMsgGetOffenderEventStatusResponse> GetOffenderEventStatusAsync(Events2.EntMsgGetOffenderEventStatusRequest entMsgGetOffenderEventStatusRequest)
        {
            throw new NotImplementedException();
        }

        public Events2.EntMsgGetOpenPanicCallsResponse GetOpenPanicCalls(Events2.EntMsgGetOpenPanicCallsRequest entMsgGetOpenPanicCallsRequest)
        {
            try
            {
                return EventsClient.GetOpenPanicCalls(entMsgGetOpenPanicCallsRequest);
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events2.EntMsgGetOpenPanicCallsResponse> GetOpenPanicCallsAsync(Events2.EntMsgGetOpenPanicCallsRequest entMsgGetOpenPanicCallsRequest)
        {
            throw new NotImplementedException();
        }

        public void HandleAllEvents(Events2.EntMsgHandleAllEventsRequest entMsgHandleAllEventsRequest)
        {
            try
            {
                EventsClient.HandleAllEvents(entMsgHandleAllEventsRequest);
            }
            catch (FaultException<Events2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task HandleAllEventsAsync(Events2.EntMsgHandleAllEventsRequest entMsgHandleAllEventsRequest)
        {
            throw new NotImplementedException();
        }
    }
}
