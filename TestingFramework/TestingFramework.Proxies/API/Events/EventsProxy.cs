﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace TestingFramework.Proxies.API.Events
{
    public class EventsProxy : BaseProxy, Events0.IEvents
    {
        #region Consts
        protected const string EventsSvcPathToken = "EventsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "EventsSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Events0.EventsClient EventsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private EventsProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, EventsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            EventsClient = new Events0.EventsClient(SvcEndpointConfigurationName, endpointAddress);

            EventsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            EventsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                var eventsMetadata = EventsClient.GetEventsMetadata();
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        #endregion Proxy init

        #region Singleton
        private static EventsProxy eventsProxy = null;
        public static EventsProxy Instance
        {
            get
            {
                if (eventsProxy == null)
                {
                    eventsProxy = new EventsProxy();
                }

                return eventsProxy;
            }
        }
        #endregion Singleton

        public void AddHandlingAction(Events0.EntMsgAddHandlingActionRequest entMsgAddHandlingActionRequest)
        {
            try
            {
                EventsClient.AddHandlingAction(entMsgAddHandlingActionRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddHandlingActionAsync(Events0.EntMsgAddHandlingActionRequest entMsgAddHandlingActionRequest)
        {
            throw new NotImplementedException();
        }

        public Events0.EntMsgGetEventsResponse GetEvents(Events0.EntMsgGetEventsRequest entMsgGetEventsRequest)
        {
            try
            {
                return EventsClient.GetEvents(entMsgGetEventsRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events0.EntMsgGetEventsResponse> GetEventsAsync(Events0.EntMsgGetEventsRequest entMsgGetEventsRequest)
        {
            throw new NotImplementedException();
        }

        public Events0.EntMsgGetEventsMetadataResponse GetEventsMetadata()
        {
            try
            {
                return EventsClient.GetEventsMetadata();
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }

        }

        public Task<Events0.EntMsgGetEventsMetadataResponse> GetEventsMetadataAsync()
        {
            throw new NotImplementedException();
        }

        public Events0.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQuery(Events0.EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest)
        {
            try
            {
                return EventsClient.GetHandlingActionsByQuery(entMsgGetHandlingActionsRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events0.EntMsgGetHandlingActionsByQueryResponse> GetHandlingActionsByQueryAsync(Events0.EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest)
        {
            throw new NotImplementedException();
        }

        public Events0.EntMsgGetOffenderEventStatusResponse GetOffenderEventStatus(Events0.EntMsgGetOffenderEventStatusRequest entMsgGetOffenderEventStatusRequest)
        {            
            try
            {
                return EventsClient.GetOffenderEventStatus(entMsgGetOffenderEventStatusRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events0.EntMsgGetOffenderEventStatusResponse> GetOffenderEventStatusAsync(Events0.EntMsgGetOffenderEventStatusRequest entMsgGetOffenderEventStatusRequest)
        {
            throw new NotImplementedException();
        }

        public Events0.EntMsgGetOpenPanicCallsResponse GetOpenPanicCalls(Events0.EntMsgGetOpenPanicCallsRequest entMsgGetOpenPanicCallsRequest)
        {
            try
            {
                return EventsClient.GetOpenPanicCalls(entMsgGetOpenPanicCallsRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events0.EntMsgGetOpenPanicCallsResponse> GetOpenPanicCallsAsync(Events0.EntMsgGetOpenPanicCallsRequest entMsgGetOpenPanicCallsRequest)
        {
            throw new NotImplementedException();
        }

        public void HandleAllEvents(Events0.EntMsgHandleAllEventsRequest entMsgHandleAllEventsRequest)
        {
            try
            {
                EventsClient.HandleAllEvents(entMsgHandleAllEventsRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task HandleAllEventsAsync(Events0.EntMsgHandleAllEventsRequest entMsgHandleAllEventsRequest)
        {
            throw new NotImplementedException();
        }

        public Events0.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResult(Events0.EntMsgGetAlcoholTestResultRequest entMsgGetAlcoholTestResultRequest)
        {
            try
            {
                return EventsClient.GetAlcoholTestResult(entMsgGetAlcoholTestResultRequest);
            }
            catch (FaultException<Events0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events0.EntMsgGetAlcoholTestResultResponse> GetAlcoholTestResultAsync(Events0.EntMsgGetAlcoholTestResultRequest entMsgGetAlcoholTestResultRequest)
        {
            throw new NotImplementedException();
        }
    }
}
