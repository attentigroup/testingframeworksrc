﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;

namespace TestingFramework.Proxies.API.Events
{
    public class EventsProxy_1 : BaseProxy, Events1.IEvents
    {
        #region Consts
        protected const string EventsSvcPathToken = "EventsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "EventsSvc-1_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Events1.EventsClient EventsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private EventsProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, EventsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            EventsClient = new Events1.EventsClient(SvcEndpointConfigurationName, endpointAddress);

            EventsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            EventsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                var eventsMetadata = EventsClient.GetEventsMetadata();
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion Proxy init

        #region Singleton
        private static EventsProxy_1 eventsProxy_1 = null;
        public static EventsProxy_1 Instance
        {
            get
            {
                if (eventsProxy_1 == null)
                {
                    eventsProxy_1 = new EventsProxy_1();
                }

                return eventsProxy_1;
            }
        }
        #endregion Singleton

        public void AddHandlingAction(Events1.EntMsgAddHandlingActionRequest entMsgAddHandlingActionRequest)
        {
            try
            {
                EventsClient.AddHandlingAction(entMsgAddHandlingActionRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddHandlingActionAsync(Events1.EntMsgAddHandlingActionRequest entMsgAddHandlingActionRequest)
        {
            throw new NotImplementedException();
        }

        public Events1.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResult(Events1.EntMsgGetAlcoholTestResultRequest entMsgGetAlcoholTestResultRequest)
        {
            try
            {
                return EventsClient.GetAlcoholTestResult(entMsgGetAlcoholTestResultRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events1.EntMsgGetAlcoholTestResultResponse> GetAlcoholTestResultAsync(Events1.EntMsgGetAlcoholTestResultRequest entMsgGetAlcoholTestResultRequest)
        {
            throw new NotImplementedException();
        }

        public Events1.EntMsgGetEventsResponse GetEvents(Events1.EntMsgGetEventsRequest entMsgGetEventsRequest)
        {
            try
            {
                return EventsClient.GetEvents(entMsgGetEventsRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events1.EntMsgGetEventsResponse> GetEventsAsync(Events1.EntMsgGetEventsRequest entMsgGetEventsRequest)
        {
            throw new NotImplementedException();
        }

        public Events1.EntMsgGetEventsMetadataResponse GetEventsMetadata()
        {
            try
            {
                return EventsClient.GetEventsMetadata();
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events1.EntMsgGetEventsMetadataResponse> GetEventsMetadataAsync()
        {
            throw new NotImplementedException();
        }

        public Events1.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQuery(Events1.EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest)
        {
            try
            {
                return EventsClient.GetHandlingActionsByQuery(entMsgGetHandlingActionsRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events1.EntMsgGetHandlingActionsByQueryResponse> GetHandlingActionsByQueryAsync(Events1.EntMsgGetHandlingActionsByQueryRequest entMsgGetHandlingActionsRequest)
        {
            throw new NotImplementedException();
        }

        public Events1.EntMsgGetOffenderEventStatusResponse GetOffenderEventStatus(Events1.EntMsgGetOffenderEventStatusRequest entMsgGetOffenderEventStatusRequest)
        {
            try
            {
                return EventsClient.GetOffenderEventStatus(entMsgGetOffenderEventStatusRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events1.EntMsgGetOffenderEventStatusResponse> GetOffenderEventStatusAsync(Events1.EntMsgGetOffenderEventStatusRequest entMsgGetOffenderEventStatusRequest)
        {
            throw new NotImplementedException();
        }

        public Events1.EntMsgGetOpenPanicCallsResponse GetOpenPanicCalls(Events1.EntMsgGetOpenPanicCallsRequest entMsgGetOpenPanicCallsRequest)
        {
            try
            {
                return EventsClient.GetOpenPanicCalls(entMsgGetOpenPanicCallsRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Events1.EntMsgGetOpenPanicCallsResponse> GetOpenPanicCallsAsync(Events1.EntMsgGetOpenPanicCallsRequest entMsgGetOpenPanicCallsRequest)
        {
            throw new NotImplementedException();
        }

        public void HandleAllEvents(Events1.EntMsgHandleAllEventsRequest entMsgHandleAllEventsRequest)
        {
            try
            {
                EventsClient.HandleAllEvents(entMsgHandleAllEventsRequest);
            }
            catch (FaultException<Events1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task HandleAllEventsAsync(Events1.EntMsgHandleAllEventsRequest entMsgHandleAllEventsRequest)
        {
            throw new NotImplementedException();
        }
    }
}
