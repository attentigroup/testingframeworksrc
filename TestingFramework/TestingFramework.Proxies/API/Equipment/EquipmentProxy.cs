﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace TestingFramework.Proxies.API.Equipment
{

    public class EquipmentProxy : BaseProxy, EM.Interfaces.Equipment.IEquipment
    {
        
        private EM.Interfaces.Equipment.EquipmentClient EquipmentClient { get; set; }

        private EquipmentProxy()
        {
            string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
            string svcCurrentVerEndpoint = ConfigurationManager.AppSettings[SvcEndpointToken];
            string EquipmentSvcPath = ConfigurationManager.AppSettings[EquipmentSvcPathToken];
            string EquipmentSvcEndpointConfigurationName = ConfigurationManager.AppSettings[EquipmentSvcEndpointConfigurationNameToken];

            string fullUri = svcUri + svcCurrentVerEndpoint + EquipmentSvcPath;

            Log.InfoFormat("EquipmentProxy going to connect to URI:{0} EndpointConfigurationName:{1}", fullUri, EquipmentSvcEndpointConfigurationName);
            EndpointAddress fullSvcEndpoint = new EndpointAddress(fullUri);

            EquipmentClient = new EM.Interfaces.Equipment.EquipmentClient(EquipmentSvcEndpointConfigurationName, fullSvcEndpoint);

            var username = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            var password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            Log.InfoFormat("EquipmentProxy ClientCredentials UserName:{0} Password:{1}", username, password);
            EquipmentClient.ClientCredentials.UserName.UserName = username;
            EquipmentClient.ClientCredentials.UserName.Password = password;

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };

            CheckConectivity();
        }

        private static EquipmentProxy equipmentProxy;

        public static EquipmentProxy Instance
        {
            get
            {
                if (equipmentProxy == null)
                {
                    equipmentProxy = new EquipmentProxy();
                }

                return equipmentProxy;
            }
        }

        public void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = GetEquipmentList(request);
        }

        #region Private methods

        private string ParseValidationFault(FaultException<ValidationFault> fe)
        {
            var errorText = string.Join(". ", fe.Detail.Errors.Select(x => string.Join(",",
                    new[] { x.Rule.ToString(), x.Message.ToString(), string.Join("-", x.Field) })));
            return errorText;
        }
        #endregion Private methods

        public EM.Interfaces.Equipment.EntMsgAddEquipmentResponse AddEquipment(EM.Interfaces.Equipment.EntMsgAddEquipmentRequest entMsgAddEquipmentRequest)
        {
            try
            {
                return EquipmentClient.AddEquipment(entMsgAddEquipmentRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                var errorText = ParseValidationFault(fe);
                throw new Exception(errorText, fe);
            }
        }

        public Task<EM.Interfaces.Equipment.EntMsgAddEquipmentResponse> AddEquipmentAsync(EM.Interfaces.Equipment.EntMsgAddEquipmentRequest entMsgAddEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteCellularData(EM.Interfaces.Equipment.EntMsgDeleteCellularDataRequest entMsgDeleteCellularDataRequest)
        {
            try
            {
                EquipmentClient.DeleteCellularData(entMsgDeleteCellularDataRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteCellularDataAsync(EM.Interfaces.Equipment.EntMsgDeleteCellularDataRequest entMsgDeleteCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteEquipment(EM.Interfaces.Equipment.EntMsgDeleteEquipmentRequest entMsgDeleteEquipmentRequest)
        {
            try
            {
                EquipmentClient.DeleteEquipment(entMsgDeleteEquipmentRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteEquipmentAsync(EM.Interfaces.Equipment.EntMsgDeleteEquipmentRequest entMsgDeleteEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public EM.Interfaces.Equipment.EntMsgGetCellularDataResponse GetCellularData(EM.Interfaces.Equipment.EntMsgGetCellularDataRequest entMsgGetCellularDataRequest)
        {
            return EquipmentClient.GetCellularData(entMsgGetCellularDataRequest);
        }

        public Task<EM.Interfaces.Equipment.EntMsgGetCellularDataResponse> GetCellularDataAsync(EM.Interfaces.Equipment.EntMsgGetCellularDataRequest entMsgGetCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public EM.Interfaces.Equipment.EntMsgGetEquipmentListResponse GetEquipmentList(EM.Interfaces.Equipment.EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest)
        {
            try
            {
                return EquipmentClient.GetEquipmentList(entMsgGetEquipmentListRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<EM.Interfaces.Equipment.EntMsgGetEquipmentListResponse> GetEquipmentListAsync(EM.Interfaces.Equipment.EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateCellularData(EM.Interfaces.Equipment.EntMsgUpdateCellularDataRequest entMsgUpdateCellularDataRequest)
        {
            try
            {
                EquipmentClient.UpdateCellularData(entMsgUpdateCellularDataRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateCellularDataAsync(EM.Interfaces.Equipment.EntMsgUpdateCellularDataRequest entMsgUpdateCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateEquipment(EM.Interfaces.Equipment.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest)
        {
            try
            {
                EquipmentClient.UpdateEquipment(entMsgUpdateEquipmentRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateEquipmentAsync(EM.Interfaces.Equipment.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public EntMsgAddCellularDataResponse AddCellularData(EntMsgAddCellularDataRequest entMsgAddCellularDataRequest)
        {
            try
            {
                return EquipmentClient.AddCellularData(entMsgAddCellularDataRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (FaultException faultException)
            {
                throw new Exception(faultException.Message, faultException);
            }
            catch (CommunicationException ce)
            {
                throw new Exception(ce.Message, ce);
            }
            catch (Exception exception)
            {
                throw;

            }
        }

        public Task<EntMsgAddCellularDataResponse> AddCellularDataAsync(EntMsgAddCellularDataRequest entMsgAddCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileList(EntMsgGetAuthorizedMobileListRequest entMsgGetAuthorizedMobileListRequest)
        {
            try
            {
                return EquipmentClient.GetAuthorizedMobileList(entMsgGetAuthorizedMobileListRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<EntMsgGetAuthorizedMobileListResponse> GetAuthorizedMobileListAsync(EntMsgGetAuthorizedMobileListRequest entMsgGetAuthorizedMobileListRequest)
        {
            throw new NotImplementedException();
        }

        public void AddAuthorizedMobile(EntMsgAddAuthorizedMobileRequest entMsgAddAuthorizedMobileRequest)
        {
            try
            {
                EquipmentClient.AddAuthorizedMobile(entMsgAddAuthorizedMobileRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddAuthorizedMobileAsync(EntMsgAddAuthorizedMobileRequest entMsgAddAuthorizedMobileRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateAuthorizedMobile(EntMsgUpdateAuthorizedMobileRequest entMsgUpdateAuthorizedMobileRequest)
        {
            try
            {
                EquipmentClient.UpdateAuthorizedMobile(entMsgUpdateAuthorizedMobileRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateAuthorizedMobileAsync(EntMsgUpdateAuthorizedMobileRequest entMsgUpdateAuthorizedMobileRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteAuthorizedMobile(EntMsgDeleteAuthorizedMobileRequest entMsgDeleteAuthorizedMobileRequest)
        {
            try
            {
                EquipmentClient.DeleteAuthorizedMobile(entMsgDeleteAuthorizedMobileRequest);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteAuthorizedMobileAsync(EntMsgDeleteAuthorizedMobileRequest entMsgDeleteAuthorizedMobileRequest)
        {
            throw new NotImplementedException();
        }

        public EntMsgAddEquipmentListResponse AddEquipmentList(EntMsgAddEquipmentListRequest equipmentRequestList)
        {
            throw new NotImplementedException();
        }

        public Task<EntMsgAddEquipmentListResponse> AddEquipmentListAsync(EntMsgAddEquipmentListRequest equipmentRequestList)
        {
            throw new NotImplementedException();
        }
    }
}
