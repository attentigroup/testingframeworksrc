﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;

namespace TestingFramework.Proxies.API.Equipment
{
    public class EquipmentProxy_1 : BaseProxy, Equipment_1.IEquipment
    {
        protected const string EquipmentSvcEndpointConfigurationNameToken = "EquipmentSvc-1_EndpointConfigurationName";
        

        public Equipment_1.EquipmentClient EquipmentClient { get; set; }

        private static EquipmentProxy_1 equipmentProxy;

        public static EquipmentProxy_1 Instance
        {
            get
            {
                if (equipmentProxy == null)
                {
                    equipmentProxy = new EquipmentProxy_1();
                }

                return equipmentProxy;
            }
        }


        private EquipmentProxy_1()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, EquipmentSvcPathToken);
            string EquipmentSvcEndpointConfigurationName = ConfigurationManager.AppSettings[EquipmentSvcEndpointConfigurationNameToken];
            EquipmentClient = new Equipment_1.EquipmentClient(EquipmentSvcEndpointConfigurationName, endpointAddress);

            EquipmentClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            EquipmentClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        public void CheckConectivity()
        {
            var request = new Equipment_1.EntMsgGetEquipmentListRequest();
            request.Models = new Equipment_1.EnmEquipmentModel[] { Equipment_1.EnmEquipmentModel.One_Piece_GPS_RF };
            var respons = GetEquipmentList(request);
        }


        public void AddEquipment(Equipment_1.EntMsgAddEquipmentRequest entMsgAddEquipmentRequest)
        {
            try
            {
                EquipmentClient.AddEquipment(entMsgAddEquipmentRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddEquipmentAsync(Equipment_1.EntMsgAddEquipmentRequest entMsgAddEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteCellularData(Equipment_1.EntMsgDeleteCellularDataRequest entMsgDeleteCellularDataRequest)
        {
            try
            {
                EquipmentClient.DeleteCellularData(entMsgDeleteCellularDataRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteCellularDataAsync(Equipment_1.EntMsgDeleteCellularDataRequest entMsgDeleteCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteEquipment(Equipment_1.EntMsgDeleteEquipmentRequest entMsgDeleteEquipmentRequest)
        {
            try
            {
                EquipmentClient.DeleteEquipment(entMsgDeleteEquipmentRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteEquipmentAsync(Equipment_1.EntMsgDeleteEquipmentRequest entMsgDeleteEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public Equipment_1.EntMsgGetCellularDataResponse GetCellularData(Equipment_1.EntMsgGetCellularDataRequest entMsgGetCellularDataRequest)
        {
            try
            {
                return EquipmentClient.GetCellularData(entMsgGetCellularDataRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Equipment_1.EntMsgGetCellularDataResponse> GetCellularDataAsync(Equipment_1.EntMsgGetCellularDataRequest entMsgGetCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public Equipment_1.EntMsgGetEquipmentListResponse GetEquipmentList(Equipment_1.EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest)
        {
            try
            {
                return EquipmentClient.GetEquipmentList(entMsgGetEquipmentListRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Equipment_1.EntMsgGetEquipmentListResponse> GetEquipmentListAsync(Equipment_1.EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateCellularData(Equipment_1.EntMsgUpdateCellularDataRequest entMsgUpdateCellularDataRequest)
        {
            try
            {
                EquipmentClient.UpdateCellularData(entMsgUpdateCellularDataRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateCellularDataAsync(Equipment_1.EntMsgUpdateCellularDataRequest entMsgUpdateCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateEquipment(Equipment_1.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest)
        {
            try
            {
                EquipmentClient.UpdateEquipment(entMsgUpdateEquipmentRequest);
            }
            catch (FaultException<Equipment_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateEquipmentAsync(Equipment_1.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest)
        {
            throw new NotImplementedException();
        }
    }
}
