﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

namespace TestingFramework.Proxies.API.Equipment
{
    public class EquipmentProxy_2 : BaseProxy, Equipment_2.IEquipment
    {
        protected const string EquipmentSvcEndpointConfigurationNameToken_2 = "EquipmentSvc-2_EndpointConfigurationName";

        public EM.Interfaces.Equipment3_9.EquipmentClient EquipmentClient { get; set; }

        private static EquipmentProxy_2 equipmentProxy;

        public static EquipmentProxy_2 Instance
        {
            get
            {
                if (equipmentProxy == null)
                {
                    equipmentProxy = new EquipmentProxy_2();
                }

                return equipmentProxy;
            }
        }

        private EquipmentProxy_2()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, EquipmentSvcPathToken);
            string EquipmentSvcEndpointConfigurationName = ConfigurationManager.AppSettings[EquipmentSvcEndpointConfigurationNameToken_2];
            EquipmentClient = new Equipment_2.EquipmentClient(EquipmentSvcEndpointConfigurationName, endpointAddress);

            EquipmentClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            EquipmentClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var entMsgGetEquipmentListRequest = new Equipment_2.EntMsgGetEquipmentListRequest();
            entMsgGetEquipmentListRequest.Models = new Equipment_2.EnmEquipmentModel[] { Equipment_2.EnmEquipmentModel.One_Piece_GPS_RF };
            GetEquipmentList(entMsgGetEquipmentListRequest);
        }

        public void AddEquipment(Equipment_2.EntMsgAddEquipmentRequest entMsgAddEquipmentRequest)
        {
            try
            {
                EquipmentClient.AddEquipment(entMsgAddEquipmentRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddEquipmentAsync(Equipment_2.EntMsgAddEquipmentRequest entMsgAddEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteCellularData(Equipment_2.EntMsgDeleteCellularDataRequest entMsgDeleteCellularDataRequest)
        {
            try
            {
                EquipmentClient.DeleteCellularData(entMsgDeleteCellularDataRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteCellularDataAsync(Equipment_2.EntMsgDeleteCellularDataRequest entMsgDeleteCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteEquipment(Equipment_2.EntMsgDeleteEquipmentRequest entMsgDeleteEquipmentRequest)
        {
            try
            {
                EquipmentClient.DeleteEquipment(entMsgDeleteEquipmentRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteEquipmentAsync(Equipment_2.EntMsgDeleteEquipmentRequest entMsgDeleteEquipmentRequest)
        {
            throw new NotImplementedException();
        }

        public Equipment_2.EntMsgGetCellularDataResponse GetCellularData(Equipment_2.EntMsgGetCellularDataRequest entMsgGetCellularDataRequest)
        {
            try
            {
                return EquipmentClient.GetCellularData(entMsgGetCellularDataRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Equipment_2.EntMsgGetCellularDataResponse> GetCellularDataAsync(Equipment_2.EntMsgGetCellularDataRequest entMsgGetCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public Equipment_2.EntMsgGetEquipmentListResponse GetEquipmentList(Equipment_2.EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest)
        {
            try
            {
                return EquipmentClient.GetEquipmentList(entMsgGetEquipmentListRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Equipment_2.EntMsgGetEquipmentListResponse> GetEquipmentListAsync(Equipment_2.EntMsgGetEquipmentListRequest entMsgGetEquipmentListRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateCellularData(Equipment_2.EntMsgUpdateCellularDataRequest entMsgUpdateCellularDataRequest)
        {
            try
            {
                EquipmentClient.UpdateCellularData(entMsgUpdateCellularDataRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateCellularDataAsync(Equipment_2.EntMsgUpdateCellularDataRequest entMsgUpdateCellularDataRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateEquipment(Equipment_2.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest)
        {
            try
            {
                EquipmentClient.UpdateEquipment(entMsgUpdateEquipmentRequest);
            }
            catch (FaultException<Equipment_2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateEquipmentAsync(Equipment_2.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest)
        {
            throw new NotImplementedException();
        }
    }
}
