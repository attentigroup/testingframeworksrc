﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Log0 = TestingFramework.Proxies.EM.Interfaces.Log12_0;

namespace TestingFramework.Proxies.API
{
    public class LogProxy : BaseProxy, Log0.ILog
    {
        #region Consts
        protected const string LogSvcPathToken = "LogSvcPath";
        protected const string LogSvcEndpointConfigurationNameToken = "LogSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Log0.LogClient LogClient { get; set; }

        #endregion Properties

        #region Singleton
        private static LogProxy logProxy = null;
        public static LogProxy Instance
        {
            get
            {
                if (logProxy == null)
                {
                    logProxy = new LogProxy();
                }

                return logProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private LogProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, LogSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[LogSvcEndpointConfigurationNameToken];
            LogClient = new Log0.LogClient(SvcEndpointConfigurationName, endpointAddress);

            LogClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            LogClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };

            CheckConectivity();
        }


        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init
        public int[] GetCountersForLog(int[] filterIDs, Log0.EnmResultCode[] resultCodes)
        {
            throw new NotImplementedException();
        }

        public Task<int[]> GetCountersForLogAsync(int[] filterIDs, Log0.EnmResultCode[] resultCodes)
        {
            throw new NotImplementedException();
        }

        public Log0.GetDataResultOfEntLogBaseypU6tq2N GetLog(int filterID, Log0.EnmResultCode resultCode, int startRowIndex, int maximumRows)
        {
            try
            {
                return LogClient.GetLog(filterID, resultCode, startRowIndex, maximumRows);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Log0.GetDataResultOfEntLogBaseypU6tq2N> GetLogAsync(int filterID, Log0.EnmResultCode resultCode, int startRowIndex, int maximumRows)
        {
            throw new NotImplementedException();
        }
    }
}
