﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace TestingFramework.Proxies.API.ProgramActions
{
    public class ProgramActionsProxy : BaseProxy, ProgramActions0.IProgramActions
    {
        #region Consts
        protected const string ProgramActionsSvcPathToken = "ProgramActionsSvcPath";
        protected const string ProgramActionsSvcEndpointConfigurationNameToken = "ProgramActionsSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private ProgramActions0.ProgramActionsClient ProgramActionsClient { get; set; }

        #endregion Properties

        #region Singleton
        private static ProgramActionsProxy programActionsProxy = null;
        public static ProgramActionsProxy Instance
        {
            get
            {
                if (programActionsProxy == null)
                {
                    programActionsProxy = new ProgramActionsProxy();
                }

                return programActionsProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private ProgramActionsProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, ProgramActionsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[ProgramActionsSvcEndpointConfigurationNameToken];
            ProgramActionsClient = new ProgramActions0.ProgramActionsClient(SvcEndpointConfigurationName, endpointAddress);

            ProgramActionsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ProgramActionsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            GetActionsList();
        }

        public ProgramActions0.EntMsgGetActionsListResponse GetActionsList()
        {
            try
            {
                return ProgramActionsClient.GetActionsList();
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public ProgramActions0.EntMsgGetEndOfServiceDataResponse GetEndOfServiceData(ProgramActions0.EntMsgGetEndOfServiceDataRequest entMsgGetEndOfServiceDataRequest)
        {
            try
            {
                return ProgramActionsClient.GetEndOfServiceData(entMsgGetEndOfServiceDataRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public ProgramActions0.EntMsgGetEndOfServiceOptionsResponse GetEndOfServiceOptions()
        {
            try
            {
                return ProgramActionsClient.GetEndOfServiceOptions();
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message + fe.Detail.Errors[0].Rule, fe);
            }
        }

        public void SendEndOfServiceRequest(ProgramActions0.EntMsgSendEndOfServiceRequest entMsgSendEndOfServiceRequest)
        {
            try
            {
                ProgramActionsClient.SendEndOfServiceRequest(entMsgSendEndOfServiceRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendDownloadRequest(ProgramActions0.EntMsgSendDownloadRequest entMsgSendDownloadRequest)
        {
            try
            {
                ProgramActionsClient.SendDownloadRequest(entMsgSendDownloadRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendUploadRequest(ProgramActions0.EntMsgSendUploadRequest entMsgSendUploadRequest)
        {
            try
            {
                ProgramActionsClient.SendUploadRequest(entMsgSendUploadRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendStartRangeTestRequest(ProgramActions0.EntMsgSendStartRangeTestRequest entMsgSendStartRangeTestRequest)
        {
            try
            {
                ProgramActionsClient.SendStartRangeTestRequest(entMsgSendStartRangeTestRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendEndRangeTestRequest(ProgramActions0.EntMsgSendEndRangeTestRequest entMsgSendEndRangeTestRequest)
        {
            try
            {
                ProgramActionsClient.SendEndRangeTestRequest(entMsgSendEndRangeTestRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendMessageRequest(ProgramActions0.EntMsgSendMessageRequest entMsgSendMessageRequest)
        {
            try
            {
                ProgramActionsClient.SendMessageRequest(entMsgSendMessageRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendVibrationRequest(ProgramActions0.EntMsgSendVibrationRequest entMsgSendVibrationRequest)
        {
            try
            {
                ProgramActionsClient.SendVibrationRequest(entMsgSendVibrationRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendAlcoholTestRequest(ProgramActions0.EntMsgSendAlcoholTestRequest entMsgSendAlcoholTestRequest)
        {
            try
            {
                ProgramActionsClient.SendAlcoholTestRequest(entMsgSendAlcoholTestRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendVoiceTestRequest(ProgramActions0.EntMsgSendVoiceTestRequest entMsgSendVoiceTestRequest)
        {
            try
            {
                ProgramActionsClient.SendVoiceTestRequest(entMsgSendVoiceTestRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void StartVoiceEnrollment(ProgramActions0.EntMsgStartVoiceEnrollmentRequest entMsgStartVoiceEnrollmentRequest)
        {
            try
            {
                ProgramActionsClient.StartVoiceEnrollment(entMsgStartVoiceEnrollmentRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void ReactivateVoiceProgram(ProgramActions0.EntMsgActivateVoiceProgramRequest entMsgActivateVoiceProgramRequest)
        {
            try
            {
                ProgramActionsClient.ReactivateVoiceProgram(entMsgActivateVoiceProgramRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SuspendVoiceProgram(ProgramActions0.EntMsgSuspendVoiceProgramRequest entMsgSuspendVoiceProgramRequest)
        {
            try
            {
                ProgramActionsClient.SuspendVoiceProgram(entMsgSuspendVoiceProgramRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void ClearCurfewUnitTamper(ProgramActions0.EntMsgClearCurfewUnitTamperRequest entMsgClearTamperRequest)
        {
            try
            {
                ProgramActionsClient.ClearCurfewUnitTamper(entMsgClearTamperRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public ProgramActions0.EntMsgGetManualEventOptionsResponse GetManualEventOptions()
        {
            try
            {
                return ProgramActionsClient.GetManualEventOptions();
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void InsertManualEvent(ProgramActions0.EntMsgInsertManualEventRequest entMsgManualEventRequest)
        {
            try
            {
                ProgramActionsClient.InsertManualEvent(entMsgManualEventRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public ProgramActions0.EntMsgGetProgramSuspensionsDataResponse GetProgramSuspensionsData(ProgramActions0.EntMsgGetProgramSuspensionsDataRequest entMsgGetProgramSuspensionsDataRequest)
        {
            try
            {
                return ProgramActionsClient.GetProgramSuspensionsData(entMsgGetProgramSuspensionsDataRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public ProgramActions0.EntMsgGetSuspendProgramOptionsResponse GetSuspendProgramOptions()
        {
            try
            {
                return ProgramActionsClient.GetSuspendProgramOptions();
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public string SuspendProgram(ProgramActions0.EntMsgSuspendProgramRequest entMsgSuspendProgramRequest)
        {
            try
            {
                return ProgramActionsClient.SuspendProgram(entMsgSuspendProgramRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void ReactivateProgram(ProgramActions0.EntMsgReactivateProgramRequest entMsgReactivateProgramRequest)
        {
            try
            {
                ProgramActionsClient.ReactivateProgram(entMsgReactivateProgramRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void StartOnlineMode(ProgramActions0.EntMsgStartOnlineModeRequest entMsgStartOnlineModeRequest)
        {
            try
            {
                ProgramActionsClient.StartOnlineMode(entMsgStartOnlineModeRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void StopOnlineMode(ProgramActions0.EntMsgStopOnlineModeRequest entMsgStopOnlineModeRequest)
        {
            try
            {
                ProgramActionsClient.StopOnlineMode(entMsgStopOnlineModeRequest);
            }
            catch (FaultException<ProgramActions0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        #endregion Proxy init
    }
}
