﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;


#region API refs
using OffendersAction_1 = TestingFramework.Proxies.EM.Interfaces.OffenderActions3_10;
#endregion API refs

namespace TestingFramework.Proxies.API.OffenderActions
{
    /// <summary>
    /// implementation of the offenders action proxy
    /// at 3.10 the intrface call IOffenderActions.
    /// at 12 it changed to IProgramActions
    /// </summary>
    public class OffenderActionsProxy_1 : BaseProxy, OffendersAction_1.IOffenderActions
    {
        #region Consts
        /// <summary>
        /// path to the service
        /// </summary>
        protected const string OffenderActionsSvcPathToken = "OffenderActionsSvcPath";
        /// <summary>
        /// path to the service configuration
        /// </summary>
        protected const string SvcEndpointConfigurationNameToken = "OffenderActions-1_SvcSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private OffendersAction_1.OffenderActionsClient OffenderActionsClient { get; set; }

        #endregion Properties

        #region Proxy init

        private OffenderActionsProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, OffenderActionsSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            OffenderActionsClient = new OffendersAction_1.OffenderActionsClient(SvcEndpointConfigurationName, endpointAddress);

            OffenderActionsClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            OffenderActionsClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            try
            {
                var OffenderActionsList = OffenderActionsClient.GetOffenderActionsList(new OffendersAction_1.EntMsgGetOffenderActionsListRequest());
            }
            catch (FaultException<OffendersAction_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }
        #endregion Proxy init

        #region Singleton
        private static OffenderActionsProxy_1 _offenderActionsProxy_1 = null;
        public static OffenderActionsProxy_1 Instance
        {
            get
            {
                if (_offenderActionsProxy_1 == null)
                {
                    _offenderActionsProxy_1 = new OffenderActionsProxy_1();
                }

                return _offenderActionsProxy_1;
            }
        }
        #endregion Singleton


        #region Interface implementation

        public OffendersAction_1.EntMsgGetOffenderActionsListResponse GetOffenderActionsList(OffendersAction_1.EntMsgGetOffenderActionsListRequest entMsgGetOffenderActionsListRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetOffenderActionsList(entMsgGetOffenderActionsListRequest);
        }

        public OffendersAction_1.EntMsgGetOffenderEOSDataResponse GetOffenderEOSData(OffendersAction_1.EntMsgGetOffenderEOSDataRequest entMsgGetOffenderEOSDataRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetOffenderEOSData(entMsgGetOffenderEOSDataRequest);
        }

        public void SendEndOfServiceRequest(OffendersAction_1.EntMsgSendEndOfServiceRequest entMsgSendEndOfServiceRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendEndOfServiceRequest(entMsgSendEndOfServiceRequest);
        }

        public void SendDownloadRequest(OffendersAction_1.EntMsgSendDownloadRequest entMsgSendDownloadRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendDownloadRequest(entMsgSendDownloadRequest);
        }

        public void SendUploadRequest(OffendersAction_1.EntMsgSendUploadRequest entMsgSendUploadRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendUploadRequest(entMsgSendUploadRequest);
        }

        public void SendStartRangeTestRequest(OffendersAction_1.EntMsgSendStartRangeTestRequest entMsgSendStartRangeTestRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendStartRangeTestRequest(entMsgSendStartRangeTestRequest);
        }

        public void SendEndRangeTestRequest(OffendersAction_1.EntMsgSendEndRangeTestRequest entMsgSendEndRangeTestRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendEndRangeTestRequest(entMsgSendEndRangeTestRequest);
        }

        public void SendMessageRequest(OffendersAction_1.EntMsgSendMessageRequest entMsgSendMessageRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendMessageRequest(entMsgSendMessageRequest);
        }

        public void SendVibrationRequest(OffendersAction_1.EntMsgSendVibrationRequest entMsgSendVibrationRequest)
        {
            try
            {
                ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendVibrationRequest(entMsgSendVibrationRequest);
            }
            catch (FaultException<OffendersAction_1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void SendAlcoholTestRequest(OffendersAction_1.EntMsgSendAlcoholTestRequest entMsgSendAlcoholTestRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendAlcoholTestRequest(entMsgSendAlcoholTestRequest);
        }

        public void SendVoiceEnrollRequest(OffendersAction_1.EntMsgSendVoiceEnrollRequest entMsgSendVoiceEnrollRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendVoiceEnrollRequest(entMsgSendVoiceEnrollRequest);
        }

        public void SendVoiceActivateRequest(OffendersAction_1.EntMsgSendVoiceActivateRequest entMsgSendVoiceActivateRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendVoiceActivateRequest(entMsgSendVoiceActivateRequest);
        }

        public void SendVoiceSuspendRequest(OffendersAction_1.EntMsgSendVoiceSuspendRequest entMsgSendVoiceSuspendRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendVoiceSuspendRequest(entMsgSendVoiceSuspendRequest);
        }

        public void SendVoiceTestRequest(OffendersAction_1.EntMsgSendVoiceTestRequest entMsgSendVoiceTestRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendVoiceTestRequest(entMsgSendVoiceTestRequest);
        }

        public void SendClearTamperRequest(OffendersAction_1.EntMsgClearTamperRequest entMsgClearTamperRequest)
        {
            try
            {
                ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendClearTamperRequest(entMsgClearTamperRequest);
            }
            catch (FaultException<OffendersAction_1.ValidationFault> fe)
            {
                var errorText = string.Join(". ", fe.Detail.Errors.Select(x => string.Join(",",
                    new[] { x.Rule.ToString(), x.Message.ToString(), string.Join("-", x.Field) })));
                throw new Exception(errorText, fe);
                throw;
            }


        }

        public void SendManualEventRequest(OffendersAction_1.EntMsgManualEventRequest entMsgManualEventRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendManualEventRequest(entMsgManualEventRequest);
        }

        public void SendUnsuspendProgramRequest(OffendersAction_1.EntMsgSendUnsuspendProgramRequest entMsgSendUnsuspendProgramRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendUnsuspendProgramRequest(entMsgSendUnsuspendProgramRequest);
        }

        public string SendSuspendProgramRequest(OffendersAction_1.EntMsgSendSuspendProgramRequest entMsgSendSuspendProgramRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).SendSuspendProgramRequest(entMsgSendSuspendProgramRequest);
        }

        public OffendersAction_1.EntMsgGetOffenderWarningsResponse GetOffenderWarnings(OffendersAction_1.EntMsgGetOffenderWarningsRequest entMsgGetOffenderWarningsRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetOffenderWarnings(entMsgGetOffenderWarningsRequest);
        }

        public void AddOffenderWarning(OffendersAction_1.EntMsgAddOffenderWarningRequest entMsgAddOffenderWarningRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).AddOffenderWarning(entMsgAddOffenderWarningRequest);
        }

        public void DeleteOffenderWarning(OffendersAction_1.EntMsgDeleteOffenderWarningRequest entMsgDeleteOffenderWarningRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).DeleteOffenderWarning(entMsgDeleteOffenderWarningRequest);
        }

        public OffendersAction_1.EntMsgGetAuthorizedAbsenceRequestsListResponse GetAuthorizedAbsenceRequestsList(OffendersAction_1.EntMsgGetAuthorizedAbsenceRequestsListRequest entMsgGetAbsenceRequestsListRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetAuthorizedAbsenceRequestsList(entMsgGetAbsenceRequestsListRequest);
        }

        public void AddAuthorizedAbsenceData(OffendersAction_1.EntMsgAddAuthorizedAbsenceDataRequest entMsgAddAuthorizedAbsenceDataRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).AddAuthorizedAbsenceData(entMsgAddAuthorizedAbsenceDataRequest);
        }

        public void UpdateAuthorizedAbsenceData(OffendersAction_1.EntMsgUpdateAuthorizedAbsenceDataRequest entMsgUpdateAuthorizedAbsenceDataRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).UpdateAuthorizedAbsenceData(entMsgUpdateAuthorizedAbsenceDataRequest);
        }

        public void DeleteAuthorizedAbsenceData(OffendersAction_1.EntMsgDeleteAuthorizedAbsenceDataRequest entMsgDeleteAuthorizedAbsenceDataRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).DeleteAuthorizedAbsenceData(entMsgDeleteAuthorizedAbsenceDataRequest);
        }

        public OffendersAction_1.EntMsgGetScheduleChangesResponse GetScheduleChanges(OffendersAction_1.EntMsgGetScheduleChangesRequest entMsgGetScheduleChangesRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetScheduleChanges(entMsgGetScheduleChangesRequest);
        }

        public void AddScheduleChange(OffendersAction_1.EntMsgAddScheduleChangeRequest entMsgAddScheduleChangeRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).AddScheduleChange(entMsgAddScheduleChangeRequest);
        }

        public void UpdateScheduleChange(OffendersAction_1.EntMsgUpdateScheduleChangeRequest entMsgUpdateScheduleChangeRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).UpdateScheduleChange(entMsgUpdateScheduleChangeRequest);
        }

        public void DeleteScheduleChange(OffendersAction_1.EntMsgDeleteScheduleChangeRequest entMsgDeleteScheduleChangeRequest)
        {
            ((OffendersAction_1.IOffenderActions)OffenderActionsClient).DeleteScheduleChange(entMsgDeleteScheduleChangeRequest);
        }

        public OffendersAction_1.EntMsgGetSuspendProgramDataListResponse GetSuspendProgrmaDataList(OffendersAction_1.EntMsgGetSuspendProgramDataListRequest entMsgGetSuspendProgramDataListRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetSuspendProgrmaDataList(entMsgGetSuspendProgramDataListRequest);
        }

        public OffendersAction_1.EntMsgGetReallocateOffenderLogResponse GetReallocateOffenderLog(OffendersAction_1.EntMsgGetReallocateOffenderLogRequest entMsgGetReallocateOffenderLogRequest)
        {
            return ((OffendersAction_1.IOffenderActions)OffenderActionsClient).GetReallocateOffenderLog(entMsgGetReallocateOffenderLogRequest);
        }

        #endregion Interface implementation
    }
}
