﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;

namespace TestingFramework.Proxies.API.Zones
{
    public class ZonesProxy_2 : BaseProxy, Zones2.IZones
    {

        #region Consts
        protected const string ZonesSvcPathToken = "ZonesSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ZonesSvc-2_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Zones2.ZonesClient ZonesClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ZonesProxy_2()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, ZonesSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ZonesClient = new Zones2.ZonesClient(SvcEndpointConfigurationName, endpointAddress);

            ZonesClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ZonesClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        // TODO: change to get zones
        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        #region Singleton
        private static ZonesProxy_2 zonesProxy_2 = null;
        public static ZonesProxy_2 Instance
        {
            get
            {
                if (zonesProxy_2 == null)
                {
                    zonesProxy_2 = new ZonesProxy_2();
                }

                return zonesProxy_2;
            }
        }
        #endregion Singleton



        public Zones2.MsgAddZoneResponse AddCircularZone(Zones2.MsgAddCircularZoneRequest msgAddCircularZoneRequest)
        {
            try
            {
                return ZonesClient.AddCircularZone(msgAddCircularZoneRequest);
            }
            catch (FaultException<Zones2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones2.MsgAddZoneResponse> AddCircularZoneAsync(Zones2.MsgAddCircularZoneRequest msgAddCircularZoneRequest)
        {
            throw new NotImplementedException();
        }

        public Zones2.MsgAddZoneResponse AddPolygonZone(Zones2.MsgAddPolygonZoneRequest msgAddPolygonZoneRequest)
        {
            try
            {
                return ZonesClient.AddPolygonZone(msgAddPolygonZoneRequest);
            }
            catch (FaultException<Zones2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones2.MsgAddZoneResponse> AddPolygonZoneAsync(Zones2.MsgAddPolygonZoneRequest msgAddPolygonZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteZone(Zones2.MsgDeleteZoneRequest msgDeleteZoneRequest)
        {
            try
            {
                ZonesClient.DeleteZone(msgDeleteZoneRequest);
            }
            catch (FaultException<Zones2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteZoneAsync(Zones2.MsgDeleteZoneRequest msgDeleteZoneRequest)
        {
            throw new NotImplementedException();
        }

        public Zones2.MsgGetZonesByEntityIDResponse GetZonesByEntityID(Zones2.MsgGetZonesByEntityIDRequest msgGetZonesByEntityIDRequest)
        {
            try
            {
                return ZonesClient.GetZonesByEntityID(msgGetZonesByEntityIDRequest);
            }
            catch (FaultException<Zones2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones2.MsgGetZonesByEntityIDResponse> GetZonesByEntityIDAsync(Zones2.MsgGetZonesByEntityIDRequest msgGetZonesByEntityIDRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateCircularZone(Zones2.MsgUpdateCircularZoneRequest msgUpdateCircularZoneRequest)
        {
            try
            {
                ZonesClient.UpdateCircularZone(msgUpdateCircularZoneRequest);
            }
            catch (FaultException<Zones2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateCircularZoneAsync(Zones2.MsgUpdateCircularZoneRequest msgUpdateCircularZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdatePolygonZone(Zones2.MsgUpdatePolygonZoneRequest msgUpdatePolygonZoneRequest)
        {
            try
            {
                ZonesClient.UpdatePolygonZone(msgUpdatePolygonZoneRequest);
            }
            catch (FaultException<Zones2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdatePolygonZoneAsync(Zones2.MsgUpdatePolygonZoneRequest msgUpdatePolygonZoneRequest)
        {
            throw new NotImplementedException();
        }
    }
}
