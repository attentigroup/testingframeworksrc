﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;

namespace TestingFramework.Proxies.API.Zones
{
    public class ZonesProxy_1 : BaseProxy, Zones1.IZones
    {

        #region Consts
        protected const string ZonesSvcPathToken = "ZonesSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ZonesSvc-1_EndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Zones1.ZonesClient ZonesClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ZonesProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, ZonesSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ZonesClient = new Zones1.ZonesClient(SvcEndpointConfigurationName, endpointAddress);

            ZonesClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ZonesClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        // TODO: change to get zones
        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        #region Singleton
        private static ZonesProxy_1 zonesProxy_1 = null;
        public static ZonesProxy_1 Instance
        {
            get
            {
                if (zonesProxy_1 == null)
                {
                    zonesProxy_1 = new ZonesProxy_1();
                }

                return zonesProxy_1;
            }
        }
        #endregion Singleton

        public Zones1.MsgAddZoneResponse AddCircularZone(Zones1.MsgAddCircularZoneRequest msgAddCircularZoneRequest)
        {
            try
            {
                return ZonesClient.AddCircularZone(msgAddCircularZoneRequest);
            }
            catch (FaultException<Zones1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones1.MsgAddZoneResponse> AddCircularZoneAsync(Zones1.MsgAddCircularZoneRequest msgAddCircularZoneRequest)
        {
            throw new NotImplementedException();
        }

        public Zones1.MsgAddZoneResponse AddPolygonZone(Zones1.MsgAddPolygonZoneRequest msgAddPolygonZoneRequest)
        {
            try
            {
                return ZonesClient.AddPolygonZone(msgAddPolygonZoneRequest);
            }
            catch (FaultException<Zones1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones1.MsgAddZoneResponse> AddPolygonZoneAsync(Zones1.MsgAddPolygonZoneRequest msgAddPolygonZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteZone(Zones1.MsgDeleteZoneRequest msgDeleteZoneRequest)
        {
            try
            {
                ZonesClient.DeleteZone(msgDeleteZoneRequest);
            }
            catch (FaultException<Zones1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteZoneAsync(Zones1.MsgDeleteZoneRequest msgDeleteZoneRequest)
        {
            throw new NotImplementedException();
        }

        public Zones1.MsgGetZonesByEntityIDResponse GetZonesByEntityID(Zones1.MsgGetZonesByEntityIDRequest msgGetZonesByEntityIDRequest)
        {
            try
            {
                return ZonesClient.GetZonesByEntityID(msgGetZonesByEntityIDRequest);
            }
            catch (FaultException<Zones1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones1.MsgGetZonesByEntityIDResponse> GetZonesByEntityIDAsync(Zones1.MsgGetZonesByEntityIDRequest msgGetZonesByEntityIDRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateCircularZone(Zones1.MsgUpdateCircularZoneRequest msgUpdateCircularZoneRequest)
        {
            try
            {
                ZonesClient.UpdateCircularZone(msgUpdateCircularZoneRequest);
            }
            catch (FaultException<Zones1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateCircularZoneAsync(Zones1.MsgUpdateCircularZoneRequest msgUpdateCircularZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdatePolygonZone(Zones1.MsgUpdatePolygonZoneRequest msgUpdatePolygonZoneRequest)
        {
            try
            {
                ZonesClient.UpdatePolygonZone(msgUpdatePolygonZoneRequest);
            }
            catch (FaultException<Zones1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdatePolygonZoneAsync(Zones1.MsgUpdatePolygonZoneRequest msgUpdatePolygonZoneRequest)
        {
            throw new NotImplementedException();
        }
    }
}
