﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
 using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;


namespace TestingFramework.Proxies.API.Zones
{
    public class ZonesProxy : BaseProxy, Zones0.IZones
    {

        #region Consts
        protected const string ZonesSvcPathToken = "ZonesSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ZonesSvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Zones0.ZonesClient ZonesClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ZonesProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, ZonesSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ZonesClient = new Zones0.ZonesClient(SvcEndpointConfigurationName, endpointAddress);

            ZonesClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ZonesClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        // TODO: change to get zones
        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        #region Singleton
        private static ZonesProxy zonesProxy = null;
        public static ZonesProxy Instance
        {
            get
            {
                if (zonesProxy == null)
                {
                    zonesProxy = new ZonesProxy();
                }

                return zonesProxy;
            }
        }
        #endregion Singleton


        public Zones0.MsgAddZoneResponse AddCircularZone(Zones0.MsgAddCircularZoneRequest msgAddCircularZoneRequest)
        {
            try
            {
                return ZonesClient.AddCircularZone(msgAddCircularZoneRequest);
            }
            //catch (FaultException<Zones0.ValidationFault> fe)
            catch (FaultException<CommBox.Data.CommunicationAPI.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones0.MsgAddZoneResponse> AddCircularZoneAsync(Zones0.MsgAddCircularZoneRequest msgAddCircularZoneRequest)
        {
            throw new NotImplementedException();
        }

        public Zones0.MsgAddZoneResponse AddPolygonZone(Zones0.MsgAddPolygonZoneRequest msgAddPolygonZoneRequest)
        {
            try
            {
                return ZonesClient.AddPolygonZone(msgAddPolygonZoneRequest);
            }
            //catch (FaultException<Zones0.ValidationFault> fe)
            catch (FaultException<CommBox.Data.CommunicationAPI.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones0.MsgAddZoneResponse> AddPolygonZoneAsync(Zones0.MsgAddPolygonZoneRequest msgAddPolygonZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteZone(Zones0.MsgDeleteZoneRequest msgDeleteZoneRequest)
        {
            try
            {
                ZonesClient.DeleteZone(msgDeleteZoneRequest);
            }
            //catch (FaultException<Zones0.ValidationFault> fe)
            catch (FaultException<CommBox.Data.CommunicationAPI.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteZoneAsync(Zones0.MsgDeleteZoneRequest msgDeleteZoneRequest)
        {
            throw new NotImplementedException();
        }

        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityID(Zones0.MsgGetZonesByEntityIDRequest msgGetZonesByEntityIDRequest)
        {
            try
            {
                return ZonesClient.GetZonesByEntityID(msgGetZonesByEntityIDRequest);
            }
            //catch (FaultException<Zones0.ValidationFault> fe)
            catch (FaultException<CommBox.Data.CommunicationAPI.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Zones0.MsgGetZonesByEntityIDResponse> GetZonesByEntityIDAsync(Zones0.MsgGetZonesByEntityIDRequest msgGetZonesByEntityIDRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateCircularZone(Zones0.MsgUpdateCircularZoneRequest msgUpdateCircularZoneRequest)
        {
            try
            {
                ZonesClient.UpdateCircularZone(msgUpdateCircularZoneRequest);
            }
            //catch (FaultException<Zones0.ValidationFault> fe)
            catch(FaultException < CommBox.Data.CommunicationAPI.ValidationFault > fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateCircularZoneAsync(Zones0.MsgUpdateCircularZoneRequest msgUpdateCircularZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdatePolygonZone(Zones0.MsgUpdatePolygonZoneRequest msgUpdatePolygonZoneRequest)
        {
            try
            {
                ZonesClient.UpdatePolygonZone(msgUpdatePolygonZoneRequest);
            }
            //catch (FaultException<Zones0.ValidationFault> fe)
            catch(FaultException < CommBox.Data.CommunicationAPI.ValidationFault > fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdatePolygonZoneAsync(Zones0.MsgUpdatePolygonZoneRequest msgUpdatePolygonZoneRequest)
        {
            throw new NotImplementedException();
        }

        public bool ZoneTemplateNameIsExist(string templateName)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ZoneTemplateNameIsExistAsync(string templateName)
        {
            throw new NotImplementedException();
        }

        public Zones0.EntZoneTemplate[] GetZoneTemplates(int zoneTemplateID)
        {
            throw new NotImplementedException();
        }

        public Task<Zones0.EntZoneTemplate[]> GetZoneTemplatesAsync(int zoneTemplateID)
        {
            throw new NotImplementedException();
        }

        // Dani Peer - 6-Oct-19 - remove
        /*
        public Zones0.MsgTemplateZoneResponse AddZoneTemplate(Zones0.MsgAddTemplateZoneRequest zoneTemplate)
        {
            throw new NotImplementedException();
        }

        public Task<Zones0.MsgTemplateZoneResponse> AddZoneTemplateAsync(Zones0.MsgAddTemplateZoneRequest zoneTemplate)
        {
            throw new NotImplementedException();
        }
        */

        public void UpdateZoneTemplate(Zones0.MsgUpdateTemplateZoneRequest zoneTemplate)
        {
            throw new NotImplementedException();
        }

        public Task UpdateZoneTemplateAsync(Zones0.MsgUpdateTemplateZoneRequest zoneTemplate)
        {
            throw new NotImplementedException();
        }

        public void DeleteZoneTemplate(int[] templateIDList)
        {
            throw new NotImplementedException();
        }

        public Task DeleteZoneTemplateAsync(int[] templateIDList)
        {
            throw new NotImplementedException();
        }

        // Dani Peer - 6-Oct-19
        /*
        public Zones0.EntZoneTemplateName[] GetZoneTemplateNames(string filter)
        {
            throw new NotImplementedException();
        }

        public Task<Zones0.EntZoneTemplateName[]> GetZoneTemplateNamesAsync(string filter)
        {
            throw new NotImplementedException();
        }
        */

        public Zones0.MsgGetZoneTemplatesResponse GetZoneTemplates(Zones0.MsgGetZoneTemplatesRequest msgGetZoneTemplatesRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Zones0.MsgGetZoneTemplatesResponse> GetZoneTemplatesAsync(Zones0.MsgGetZoneTemplatesRequest msgGetZoneTemplatesRequest)
        {
            throw new NotImplementedException();
        }

        Zones0.MsgAddTemplateZoneResponse Zones0.IZones.AddZoneTemplate(Zones0.MsgAddTemplateZoneRequest msgAddTemplateZoneRequest)
        {
            throw new NotImplementedException();
        }

        Task<Zones0.MsgAddTemplateZoneResponse> Zones0.IZones.AddZoneTemplateAsync(Zones0.MsgAddTemplateZoneRequest msgAddTemplateZoneRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteZoneTemplate(Zones0.MsgDeleteZoneTemplateRequest msgDeleteZoneTemplateRequest)
        {
            throw new NotImplementedException();
        }

        public Task DeleteZoneTemplateAsync(Zones0.MsgDeleteZoneTemplateRequest msgDeleteZoneTemplateRequest)
        {
            throw new NotImplementedException();
        }
    }
}
