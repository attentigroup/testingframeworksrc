﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Agency0 = TestingFramework.Proxies.EM.Interfaces.Agency;

namespace TestingFramework.Proxies.API.Agency
{
    public class AgencyProxy : BaseProxy, Agency0.IAgency
    {
        #region Consts
        protected const string AgencySvcPathToken = "AgencySvcPath";
        protected const string AgencySvcEndpointConfigurationNameToken = "AgencySvcEndpointConfigurationName";
        #endregion Consts

        #region Properties
        private Agency0.AgencyClient AgencyClient { get; set; }

        #endregion Properties

        #region Singleton
        private static AgencyProxy agencyProxy = null;
        public static AgencyProxy Instance
        {
            get
            {
                if (agencyProxy == null)
                {
                    agencyProxy = new AgencyProxy();
                }

                return agencyProxy;
            }
        }
        #endregion Singleton

        #region Proxy init

        private AgencyProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, AgencySvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[AgencySvcEndpointConfigurationNameToken];
            AgencyClient = new Agency0.AgencyClient(SvcEndpointConfigurationName, endpointAddress);

            AgencyClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            AgencyClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }


        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init
        public Agency0.EntMsgGetAgenciesResponse GetAgencies(Agency0.EntMsgGetAgenciesRequest entMsgGetAgenciesRequest)
        {
            try
            {
                return AgencyClient.GetAgencies(entMsgGetAgenciesRequest);
            }
            catch (FaultException<Agency0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Agency0.EntMsgGetAgenciesResponse> GetAgenciesAsync(Agency0.EntMsgGetAgenciesRequest entMsgGetAgenciesRequest)
        {
            throw new NotImplementedException();
        }

        public Agency0.EntMsgGetEquipmentRelatedAgencyListResponse GetEquipmentRelatedAgencyList()
        {
            throw new NotImplementedException();
        }

        public Task<Agency0.EntMsgGetEquipmentRelatedAgencyListResponse> GetEquipmentRelatedAgencyListAsync()
        {
            throw new NotImplementedException();
        }
    }
}
