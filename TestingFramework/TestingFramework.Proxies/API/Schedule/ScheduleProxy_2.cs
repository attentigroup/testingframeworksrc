﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;

namespace TestingFramework.Proxies.API.Schedule
{
    public class ScheduleProxy_2 : BaseProxy, Schedule2.ISchedule
    {

        #region Consts
        protected const string ScheduleSvcPathToken = "ScheduleSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ScheduleSvc-2_EndpointConfigurationName";
        #endregion Consts

        public const int CurrentVersion = -1;
        public const int PlannedVersion = -2;

        #region Properties
        private Schedule2.ScheduleClient ScheduleClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ScheduleProxy_2()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_2_EndpointToken, ScheduleSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ScheduleClient = new Schedule2.ScheduleClient(SvcEndpointConfigurationName, endpointAddress);

            ScheduleClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ScheduleClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        #region Singleton
        private static ScheduleProxy_2 scheduleProxy_2 = null;
        public static ScheduleProxy_2 Instance
        {
            get
            {
                if (scheduleProxy_2 == null)
                {
                    scheduleProxy_2 = new ScheduleProxy_2();
                }

                return scheduleProxy_2;
            }
        }
        #endregion Singleton



        public void AddScheduleChangedReason(Schedule2.EntMsgAddScheduleChangedReasonRequest entMsgAddScheduleChangedReasonRequest)
        {
            try
            {
                ScheduleClient.AddScheduleChangedReason(entMsgAddScheduleChangedReasonRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddScheduleChangedReasonAsync(Schedule2.EntMsgAddScheduleChangedReasonRequest entMsgAddScheduleChangedReasonRequest)
        {
            throw new NotImplementedException();
        }

        public void AddTimeFrame(Schedule2.EntMsgAddTimeFrameRequest entMsgAddTimeFrameRequest)
        {
            try
            {
                ScheduleClient.AddTimeFrame(entMsgAddTimeFrameRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddTimeFrameAsync(Schedule2.EntMsgAddTimeFrameRequest entMsgAddTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecurringTimeFrameInstance(Schedule2.EntMsgDeleteRecurringTimeFrameInstance entMsgDeleteRecurringTimeFrameInstance)
        {
            try
            {
                ScheduleClient.DeleteRecurringTimeFrameInstance(entMsgDeleteRecurringTimeFrameInstance);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteRecurringTimeFrameInstanceAsync(Schedule2.EntMsgDeleteRecurringTimeFrameInstance entMsgDeleteRecurringTimeFrameInstance)
        {
            throw new NotImplementedException();
        }

        public void DeleteScheduleChangeReason(Schedule2.EntMsgDeleteScheduleChangedReasonRequest entMsgDeleteScheduleChangedReasonRequest)
        {
            try
            {
                ScheduleClient.DeleteScheduleChangeReason(entMsgDeleteScheduleChangedReasonRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteScheduleChangeReasonAsync(Schedule2.EntMsgDeleteScheduleChangedReasonRequest entMsgDeleteScheduleChangedReasonRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteTimeFrame(Schedule2.EntMsgDeleteTimeFrameRequest entMsgDeleteTimeFrameRequest)
        {
            try
            {
                ScheduleClient.DeleteTimeFrame(entMsgDeleteTimeFrameRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteTimeFrameAsync(Schedule2.EntMsgDeleteTimeFrameRequest entMsgDeleteTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule2.EntMsgGetAbsenceRequestsListResponse GetAbsenceRequestsList(Schedule2.EntMsgGetAbsenceRequestsListRequest entMsgGetAbsenceRequestsListRequest)
        {
            try
            {
                return ScheduleClient.GetAbsenceRequestsList(entMsgGetAbsenceRequestsListRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule2.EntMsgGetAbsenceRequestsListResponse> GetAbsenceRequestsListAsync(Schedule2.EntMsgGetAbsenceRequestsListRequest entMsgGetAbsenceRequestsListRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule2.EntMsgGetScheduleResponse GetSchedule(Schedule2.EntMsgGetScheduleRequest entMsgGetScheduleRequest)
        {
            try
            {
                return ScheduleClient.GetSchedule(entMsgGetScheduleRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule2.EntMsgGetScheduleResponse> GetScheduleAsync(Schedule2.EntMsgGetScheduleRequest entMsgGetScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule2.EntMsgGetScheduleChangeReasonResponse GetScheduleChangeReasons(Schedule2.EntMsgGetScheduleChangeReasonsRequest entMsgGetScheduleChangeReasonsRequest)
        {
            try
            {
                return ScheduleClient.GetScheduleChangeReasons(entMsgGetScheduleChangeReasonsRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule2.EntMsgGetScheduleChangeReasonResponse> GetScheduleChangeReasonsAsync(Schedule2.EntMsgGetScheduleChangeReasonsRequest entMsgGetScheduleChangeReasonsRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule2.EntMsgGetScheduleParametersResponse GetScheduleParameters(Schedule2.EntMsgGetScheduleParametersRequest entMsgGetScheduleParametersRequest)
        {
            try
            {
                return ScheduleClient.GetScheduleParameters(entMsgGetScheduleParametersRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule2.EntMsgGetScheduleParametersResponse> GetScheduleParametersAsync(Schedule2.EntMsgGetScheduleParametersRequest entMsgGetScheduleParametersRequest)
        {
            throw new NotImplementedException();
        }

        public void RepeatSchedule(Schedule2.EntMsgRepeatScheduleRequest entMsgRepeatScheduleRequest)
        {
            try
            {
                ScheduleClient.RepeatSchedule(entMsgRepeatScheduleRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task RepeatScheduleAsync(Schedule2.EntMsgRepeatScheduleRequest entMsgRepeatScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateSchedule(Schedule2.EntMsgUpdateScheduleRequest entMsgUpdateScheduleRequest)
        {
            try
            {
                ScheduleClient.UpdateSchedule(entMsgUpdateScheduleRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateScheduleAsync(Schedule2.EntMsgUpdateScheduleRequest entMsgUpdateScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateScheduleChangeReason(Schedule2.EntMsgUpdateScheduleChangedReasonRequest entMsgUpdateScheduleChangedReasonRequest)
        {
            try
            {
                ScheduleClient.UpdateScheduleChangeReason(entMsgUpdateScheduleChangedReasonRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateScheduleChangeReasonAsync(Schedule2.EntMsgUpdateScheduleChangedReasonRequest entMsgUpdateScheduleChangedReasonRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateTimeFrame(Schedule2.EntMsgUpdateTimeFrameRequest entMsgUpdateTimeFrameRequest)
        {
            try
            {
                ScheduleClient.UpdateTimeFrame(entMsgUpdateTimeFrameRequest);
            }
            catch (FaultException<Schedule2.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateTimeFrameAsync(Schedule2.EntMsgUpdateTimeFrameRequest entMsgUpdateTimeFrameRequest)
        {
            throw new NotImplementedException();
        }
    }
}
