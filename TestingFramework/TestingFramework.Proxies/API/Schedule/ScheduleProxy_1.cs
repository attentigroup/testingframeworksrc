﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;

using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;

namespace TestingFramework.Proxies.API.Schedule
{
    public class ScheduleProxy_1 : BaseProxy, Schedule1.ISchedule
    {

        #region Consts
        protected const string ScheduleSvcPathToken = "ScheduleSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ScheduleSvc-1_EndpointConfigurationName";
        #endregion Consts

        public const int CurrentVersion = -1;
        public const int PlannedVersion = -2;

        #region Properties
        private Schedule1.ScheduleClient ScheduleClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ScheduleProxy_1()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(Svc_1_EndpointToken, ScheduleSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ScheduleClient = new Schedule1.ScheduleClient(SvcEndpointConfigurationName, endpointAddress);

            ScheduleClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ScheduleClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        #region Singleton
        private static ScheduleProxy_1 scheduleProxy_1 = null;
        public static ScheduleProxy_1 Instance
        {
            get
            {
                if (scheduleProxy_1 == null)
                {
                    scheduleProxy_1 = new ScheduleProxy_1();
                }

                return scheduleProxy_1;
            }
        }
        #endregion Singleton



        public void AddTimeFrame(Schedule1.EntMsgAddTimeFrameRequest entMsgAddTimeFrameRequest)
        {
            try
            {
                ScheduleClient.AddTimeFrame(entMsgAddTimeFrameRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task AddTimeFrameAsync(Schedule1.EntMsgAddTimeFrameRequest entMsgAddTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecurringTimeFrameInstance(Schedule1.EntMsgDeleteRecurringTimeFrameInstance entMsgDeleteRecurringTimeFrameInstance)
        {
            try
            {
                ScheduleClient.DeleteRecurringTimeFrameInstance(entMsgDeleteRecurringTimeFrameInstance);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteRecurringTimeFrameInstanceAsync(Schedule1.EntMsgDeleteRecurringTimeFrameInstance entMsgDeleteRecurringTimeFrameInstance)
        {
            throw new NotImplementedException();
        }

        public void DeleteTimeFrame(Schedule1.EntMsgDeleteTimeFrameRequest entMsgDeleteTimeFrameRequest)
        {
            try
            {
                ScheduleClient.DeleteTimeFrame(entMsgDeleteTimeFrameRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteTimeFrameAsync(Schedule1.EntMsgDeleteTimeFrameRequest entMsgDeleteTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule1.EntMsgGetScheduleResponse GetSchedule(Schedule1.EntMsgGetScheduleRequest entMsgGetScheduleRequest)
        {
            try
            {
                return ScheduleClient.GetSchedule(entMsgGetScheduleRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule1.EntMsgGetScheduleResponse> GetScheduleAsync(Schedule1.EntMsgGetScheduleRequest entMsgGetScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule1.EntMsgGetScheduleParametersResponse GetScheduleParameters(Schedule1.EntMsgGetScheduleParametersRequest entMsgGetScheduleParametersRequest)
        {
            try
            {
                return ScheduleClient.GetScheduleParameters(entMsgGetScheduleParametersRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule1.EntMsgGetScheduleParametersResponse> GetScheduleParametersAsync(Schedule1.EntMsgGetScheduleParametersRequest entMsgGetScheduleParametersRequest)
        {
            throw new NotImplementedException();
        }

        public void RepeatSchedule(Schedule1.EntMsgRepeatScheduleRequest entMsgRepeatScheduleRequest)
        {
            try
            {
                ScheduleClient.RepeatSchedule(entMsgRepeatScheduleRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task RepeatScheduleAsync(Schedule1.EntMsgRepeatScheduleRequest entMsgRepeatScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateSchedule(Schedule1.EntMsgUpdateScheduleRequest entMsgUpdateScheduleRequest)
        {
            try
            {
                ScheduleClient.UpdateSchedule(entMsgUpdateScheduleRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateScheduleAsync(Schedule1.EntMsgUpdateScheduleRequest entMsgUpdateScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateTimeFrame(Schedule1.EntMsgUpdateTimeFrameRequest entMsgUpdateTimeFrameRequest)
        {
            try
            {
                ScheduleClient.UpdateTimeFrame(entMsgUpdateTimeFrameRequest);
            }
            catch (FaultException<Schedule1.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateTimeFrameAsync(Schedule1.EntMsgUpdateTimeFrameRequest entMsgUpdateTimeFrameRequest)
        {
            throw new NotImplementedException();
        }
    }
}
