﻿using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace TestingFramework.Proxies.API.Schedule
{
    public class ScheduleProxy : BaseProxy, Schedule0.ISchedule
    {

        #region Consts
        protected const string ScheduleSvcPathToken = "ScheduleSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "ScheduleSvcEndpointConfigurationName";
        #endregion Consts

        public const int CurrentVersion = -1;
        public const int PlannedVersion = -2;

        #region Properties
        private Schedule0.ScheduleClient ScheduleClient { get; set; }

        #endregion Properties

        #region Proxy init

        private ScheduleProxy()
        {
            Init();
        }

        private void Init()
        {
            var endpointAddress = GetEndpointAddressByConfig(SvcEndpointToken, ScheduleSvcPathToken);
            string SvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];
            ScheduleClient = new Schedule0.ScheduleClient(SvcEndpointConfigurationName, endpointAddress);

            ScheduleClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            ScheduleClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };


            CheckConectivity();
        }

        private void CheckConectivity()
        {
            var request = new EntMsgGetEquipmentListRequest();
            EntMsgGetEquipmentListResponse respons = Equipment.EquipmentProxy.Instance.GetEquipmentList(request);
        }

        #endregion Proxy init

        #region Singleton
        private static ScheduleProxy scheduleProxy = null;
        public static ScheduleProxy Instance
        {
            get
            {
                if (scheduleProxy == null)
                {
                    scheduleProxy = new ScheduleProxy();
                }

                return scheduleProxy;
            }
        }
        #endregion Singleton










        public Schedule0.EntMsgAddTimeFrameResponse AddTimeFrame(Schedule0.EntMsgAddTimeFrameRequest entMsgAddTimeFrameRequest)
        {
            try
            {
                return ScheduleClient.AddTimeFrame(entMsgAddTimeFrameRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(string.Format("Message:{0} Rule:{1} ", fe.Detail.Errors[0].Message , fe.Detail.Errors[0].Rule), fe);
            }
        }

        public Task<Schedule0.EntMsgAddTimeFrameResponse> AddTimeFrameAsync(Schedule0.EntMsgAddTimeFrameRequest entMsgAddTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecurringTimeFrameInstance(Schedule0.EntMsgDeleteRecurringTimeFrameInstance entMsgDeleteRecurringTimeFrameInstance)
        {
            try
            {
                ScheduleClient.DeleteRecurringTimeFrameInstance(entMsgDeleteRecurringTimeFrameInstance);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteRecurringTimeFrameInstanceAsync(Schedule0.EntMsgDeleteRecurringTimeFrameInstance entMsgDeleteRecurringTimeFrameInstance)
        {
            throw new NotImplementedException();
        }

        public void DeleteTimeFrame(Schedule0.EntMsgDeleteTimeFrameRequest entMsgDeleteTimeFrameRequest)
        {
            try
            {
                ScheduleClient.DeleteTimeFrame(entMsgDeleteTimeFrameRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task DeleteTimeFrameAsync(Schedule0.EntMsgDeleteTimeFrameRequest entMsgDeleteTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule0.EntMsgGetScheduleResponse GetSchedule(Schedule0.EntMsgGetScheduleRequest entMsgGetScheduleRequest)
        {
            try
            {
                return ScheduleClient.GetSchedule(entMsgGetScheduleRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule0.EntMsgGetScheduleResponse> GetScheduleAsync(Schedule0.EntMsgGetScheduleRequest entMsgGetScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public Schedule0.EntMsgGetScheduleParametersResponse GetScheduleParameters(Schedule0.EntMsgGetScheduleParametersRequest entMsgGetScheduleParametersRequest)
        {
            try
            {
                return ScheduleClient.GetScheduleParameters(entMsgGetScheduleParametersRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task<Schedule0.EntMsgGetScheduleParametersResponse> GetScheduleParametersAsync(Schedule0.EntMsgGetScheduleParametersRequest entMsgGetScheduleParametersRequest)
        {
            throw new NotImplementedException();
        }

        public void RepeatSchedule(Schedule0.EntMsgRepeatScheduleRequest entMsgRepeatScheduleRequest)
        {
            try
            {
                ScheduleClient.RepeatSchedule(entMsgRepeatScheduleRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task RepeatScheduleAsync(Schedule0.EntMsgRepeatScheduleRequest entMsgRepeatScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateSchedule(Schedule0.EntMsgUpdateScheduleRequest entMsgUpdateScheduleRequest)
        {
            try
            {
                ScheduleClient.UpdateSchedule(entMsgUpdateScheduleRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateScheduleAsync(Schedule0.EntMsgUpdateScheduleRequest entMsgUpdateScheduleRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateTimeFrame(Schedule0.EntMsgUpdateTimeFrameRequest entMsgUpdateTimeFrameRequest)
        {
            try
            {
                ScheduleClient.UpdateTimeFrame(entMsgUpdateTimeFrameRequest);
            }
            catch (FaultException<Schedule0.ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Task UpdateTimeFrameAsync(Schedule0.EntMsgUpdateTimeFrameRequest entMsgUpdateTimeFrameRequest)
        {
            throw new NotImplementedException();
        }

    }
}
