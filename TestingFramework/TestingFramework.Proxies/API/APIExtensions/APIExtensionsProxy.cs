﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;

namespace TestingFramework.Proxies.API.APIExtensions
{
    public class APIExtensionsProxy : BaseProxy, IAPIExtensions
    {
        //https://localhost:8087 /APIExtensionsServices/svc/APIExtensions.svc?wsdl
        #region Consts

        protected const string APIExtensionsServicesToken = "APIExtensionsServices";        
        protected const string APIExtensionsSvcPathToken = "APIExtensionsSvcPath";
        protected const string SvcEndpointConfigurationNameToken = "APIExtensionsSvcEndpointConfigurationName";

        protected const string WebUserIPAddressHeaderName = "WebUserIPAddress";
        #endregion Consts

        private APIExtensionsClient APIExtensionsClient { get; set; }

        #region singleton
        private static APIExtensionsProxy _apiExtensionsProxy;

        public static APIExtensionsProxy Instance
        {
            get
            {
                if (_apiExtensionsProxy == null)
                {
                    _apiExtensionsProxy = new APIExtensionsProxy();
                }

                return _apiExtensionsProxy;
            }
        }

        private APIExtensionsProxy()
        {
            Init();
        }

        #endregion singleton

        public void Init()
        {
            string svcUri = ConfigurationManager.AppSettings[SvcUriToken];
            string APIExtensionsServices = ConfigurationManager.AppSettings[APIExtensionsServicesToken];
            string apiExtensionsSvcPath = ConfigurationManager.AppSettings[APIExtensionsSvcPathToken];
            string apiExtensionsSvcEndpointConfigurationName = ConfigurationManager.AppSettings[SvcEndpointConfigurationNameToken];

            string fullUri = svcUri + APIExtensionsServices + apiExtensionsSvcPath;

            Log.InfoFormat("APIExtensionsProxy going to connect to URI:{0} EndpointConfigurationName:{1}", fullUri, apiExtensionsSvcEndpointConfigurationName);
            EndpointAddress fullSvcEndpoint = new EndpointAddress(fullUri);

            APIExtensionsClient = new APIExtensionsClient(apiExtensionsSvcEndpointConfigurationName, fullSvcEndpoint);

            var username = ConfigurationManager.AppSettings[SvcApiUserNameToken];
            var password = ConfigurationManager.AppSettings[SvcApiPasswordToken];

            Log.InfoFormat("EquipmentProxy ClientCredentials UserName:{0} Password:{1}", username, password);
            APIExtensionsClient.ClientCredentials.UserName.UserName = username;
            APIExtensionsClient.ClientCredentials.UserName.Password = password;

            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };

            #region add headers to every call

            Headers.Add(WebUserIPAddressHeaderName, LocalIpAddress);//add local ip address

            #endregion add headers to every call

            CheckConectivity();
        }

        private void CheckConectivity()
        {

            try
            {
                GetCommunicationServicesStatus();
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException != null)
                {
                    if (mse.InnerException is FaultException)
                    {
                        FaultException fe = mse.InnerException as FaultException;
                        throw new Exception(fe.Message, fe);
                    }
                }
            }
        }

        public void AlcoholTestPictureApproval(int eventID, bool match)
        {
            throw new NotImplementedException();
        }

        public ulong ApplyNewReceiverAddress(int offenderID, string receiverSN, string newStateID, string newCityID, string newAddress, string newTimezone, EntLandlineData newLandline)
        {
            throw new NotImplementedException();
        }

        public void AutoFaceRecognitionEnrollment(int offenderID, EntEnrollSelectedPictures[] picturesList)
        {
            throw new NotImplementedException();
        }

        public void ClearAllCache()
        {
            try
            {
                OperationContextScopeWithHeaders(APIExtensionsClient.InnerChannel, Headers);
                APIExtensionsClient.ClearAllCache();
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public void ClearSpecificCache(string cacheName)
        {
            throw new NotImplementedException();
        }

        public void DeleteAllTimeFrames(int entityID, EnmEntityType entityType, EnmProgramType program, EnmTimeFrameGroupType criteria)
        {
            throw new NotImplementedException();
        }

        public void DeleteColumnData(string screenName, string enmResultCode, string resourceID)
        {
            throw new NotImplementedException();
        }

        public bool DisplayAutoFaceRecognitionSetupMenuItem(int agencyID)
        {
            throw new NotImplementedException();
        }

        public EntOffenderPicture GetAlcoholTestPicture(int eventID)
        {
            throw new NotImplementedException();
        }

        public EntEventPicture[] GetAlcoholTestsEventIDList(int offenderID)
        {
            throw new NotImplementedException();
        }
        public EntColumnData[] GetColumnsData(string screenName, EnmResultCode? resultCode)
        {
            throw new NotImplementedException();
        }

        public EntCommunicationStatus GetCommunicationServicesStatus()
        {
            EntCommunicationStatus result = null;
            try
            {
                OperationContextScopeWithHeaders(APIExtensionsClient.InnerChannel, Headers);
                result = APIExtensionsClient.GetCommunicationServicesStatus();
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
            return result;
        }

        public List<EntUser> GetContactList(EnmContactType userType, int? userID, int? offenderID)
        {
            try
            {
                return APIExtensionsClient.GetContactList(userType, userID, offenderID);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public EntDailySchedule GetDailySchedule(int offenderID, DateTime requestedDate, int? version)
        {
            throw new NotImplementedException();
        }

        public string GetDeviceVersion(string serialNumber, EnmEquipmentType type)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, EntSecurity> GetDynamicFieldsSiteSecurity(string screen)
        {
            throw new NotImplementedException();
        }

        public EntEquipmentBase[] GetEquipmentAutoCompleteOptions(EnmEquipmentType type, string search, int max, int agencyID, int offenderID, EnmProgramType programType, string receiverSN, string transmitterSN, string deuSN, bool isDV, int? relatedOffenderID)
        {
            throw new NotImplementedException();
        }

        public EntEvent GetEventByID(int eventID)
        {
            throw new NotImplementedException();
        }

        public EntEventGroup[] GetEventGroupList()
        {
            throw new NotImplementedException();
        }

        public KeyValuePair<string, string>[] GetEventMessageOptionsForFilter(bool filterByProgramn, EnmProgramType program)
        {
            throw new NotImplementedException();
        }

        public EntEventDataResult GetEventsByFilterID(int startRowIndex, int maximumRows, int filterID, string screenName, byte[] previousTimeStamp, int pageUpperLimitEventId, int? offenderID, bool includeOffenderDetails, EntSortExpression sortExpression, string searchString, DateTime? fromTime, DateTime? toTime, bool forceRefresh, EnmProgramType? programType)
        {
            try
            {
                OperationContextScopeWithHeaders(APIExtensionsClient.InnerChannel, Headers);
                return APIExtensionsClient.GetEventsByFilterID(startRowIndex, maximumRows, filterID, screenName, previousTimeStamp, pageUpperLimitEventId, offenderID, includeOffenderDetails, sortExpression, searchString, fromTime, toTime, forceRefresh, programType);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public EntEquipmentBase[] GetExistingEquipmentList(EntEquipmentBase[] equipmentList)
        {
            throw new NotImplementedException();
        }
        public int[] GetFilterCountersForEvents(int[] filterIDs, bool includeHistory, int? offenderID)
        {
            throw new NotImplementedException();
        }

        public List<EntEventExtended> GetLatestOffenderEvents(int eventLogID, int? rowCount)
        {
            try
            {
                return APIExtensionsClient.GetLatestOffenderEvents(eventLogID, rowCount);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public Dictionary<string, KeyValuePair<string, string>[]> GetManualEventSubTypes()
        {
            throw new NotImplementedException();
        }

        public KeyValuePair<int, string>[] GetNonAllocatedVictims(int? offenderID, int? agencyID)
        {
            throw new NotImplementedException();
        }

        public EntOffenderPicture[] GetOffenderAutoFaceRecognitionPictures(int offenderID)
        {
            throw new NotImplementedException();
        }

        public EntOffender GetOffenderByID(int offenderID)
        {
            try
            {
                return APIExtensionsClient.GetOffenderByID(offenderID);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public EntProgramVersion GetOffenderDataVersionTimeRange(int offenderID, int versionNumber)
        {
            throw new NotImplementedException();
        }

        public EntEventAdditionalData GetOffenderEventAdditionalData(int offenderID, int eventID, string eventCode, DateTime eventTime, EnmEventSeverity severity, EnmHandlingStatusType? status, bool isHistory, int? eventVersion, bool isAlcoholEvent, EnmMapProvider mapProvider)
        {
            try
            {
                return APIExtensionsClient.GetOffenderEventAdditionalData(offenderID, eventID, eventCode, eventTime, severity, status, isHistory, eventVersion, isAlcoholEvent, mapProvider);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public int? GetOffenderForProgramCombination(string refID)
        {
            throw new NotImplementedException();
        }

        public EntOffenderPicture[] GetOffenderPicturesIDList(int offenderID)
        {
            throw new NotImplementedException();
        }

        public EntProgramVersion[] GetOffenderProgramVersions(int offenderID, int? relatedOffenderID, string where)
        {
            throw new NotImplementedException();
        }

        public int[] GetOffendersIDs(int filterID, EntSortExpression sortBy, string search)
        {
            throw new NotImplementedException();
        }

        public EntOffenderSummaryData GetOffenderSummary(int offenderID, EnmMapProvider mapProvider)
        {
            try
            {
                return APIExtensionsClient.GetOffenderSummary(offenderID, mapProvider);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public EntContactInfo GetReceiverAddressForSync(string receiverSN)
        {
            throw new NotImplementedException();
        }

        public List<EntEventExtended> GetRelatedEvents(int eventLogID, int? rowCount)
        {

            return APIExtensionsClient.GetRelatedEvents(eventLogID, rowCount);
        }

        public EntSysTableItem[] GetRFRanges()
        {
            throw new NotImplementedException();
        }

        public EntSysTableItem[] GetRingVolumes()
        {
            throw new NotImplementedException();
        }

        public int GetSilentLoginID()
        {
            throw new NotImplementedException();
        }

        public List<EntGroupOffender> GetUnallocatedOffenderList(EnmProgramGroup programGroup)
        {
            try
            {
                return APIExtensionsClient.GetUnallocatedOffenderList(programGroup);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }
        }

        public EntAgency[] GetUnfilteredAgencies()
        {
            throw new NotImplementedException();
        }
        public EntUser[] GetUnfilteredUsers()
        {
            throw new NotImplementedException();
        }

        public string[] GetUnregisteredUsers()
        {
            throw new NotImplementedException();
        }

        public EntVoiceRetry[] GetVoiceRetryList(int eventID, string eventCode, int? offenderID)
        {
            throw new NotImplementedException();
        }
        public MsgGetZonesByEntityIDResponse GetZonesByVersionDates(MsgGetZonesByVersionDates msg)
        {
            throw new NotImplementedException();
        }

        public bool IsE4DataChanged(int offenderID, EnmEquipmentFieldName fieldName)
        {
            throw new NotImplementedException();
        }

        public bool IsEventPanicCleared(int eventId)
        {
            throw new NotImplementedException();
        }

        public bool IsOffenderMemberInGroup(int offenderID)
        {
            throw new NotImplementedException();
        }

        public bool IsTimeFrameAttachedToZone(int entityID, EnmEntityType entityType, int zoneID)
        {
            throw new NotImplementedException();
        }

        public int MarkAlcoholTestPictureAsReference(int eventID)
        {
            throw new NotImplementedException();
        }

        public void ReplaceAlcoholTestReferencePicture(int eventID, int pictureID)
        {
            throw new NotImplementedException();
        }

        public void SwitchPhoneForDualSIM(EntCellularData newLeft, EntCellularData newRight, EntCellularData oldLeft, EntCellularData oldRight)
        {
            throw new NotImplementedException();
        }

        public void SyncUsersWithActiveDirectory()
        {
            throw new NotImplementedException();
        }

        public void UpdateColumnData(EntColumnData[] entColumnData)
        {
            throw new NotImplementedException();
        }

        public EntEquipmentDataResponse[] UpdateEquipmentList(EntEquipmentUpdateData[] EquipmentList)
        {
            throw new NotImplementedException();
        }
        public ulong UpdateOffenderProfilePhoto(int offenderID, int? photoID)
        {
            throw new NotImplementedException();
        }

        public void ValidateDenmarkOID(string offenderRefID)
        {
            throw new NotImplementedException();
        }

        public KeyValuePair<EntEquipmentBase, string>[] ValidateImportEquipmentList(EntEquipmentBase[] equipmentList, bool returnFailList)
        {
            throw new NotImplementedException();
        }

        public void ValidateOffenderAddress(EntOffenderAddress address)
        {
            throw new NotImplementedException();
        }

        public void ValidateSchedule(int entityID, EnmEntityType entityType, EntTimeFrame[] timeframes, EntIgnoreTimeFrame[] ignoreTimeframes, EntMEMSTimeFrame[] alcoholTimeframs, EntVoiceTimeFrame[] voiceTimeframs, DateTime from, DateTime to)
        {
            throw new NotImplementedException();
        }

        public GetDataResultOfEntDVRelationDataypU6tq2N GetVictimsAggressorsPairs(int startRowIndex, int maximumRows, bool showOnlyActive)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetVictimsAggressorsPairs(startRowIndex, maximumRows, showOnlyActive);
        }

        public void ValidateSchedule(int entityID, EnmEntityType entityType, List<EntTimeFrame> timeframes, List<EntIgnoreTimeFrame> ignoreTimeframes, List<EntMEMSTimeFrame> alcoholTimeframs, List<EntVoiceTimeFrame> voiceTimeframs, DateTime from, DateTime to)
        {
            ((IAPIExtensions)APIExtensionsClient).ValidateSchedule(entityID, entityType, timeframes, ignoreTimeframes, alcoholTimeframs, voiceTimeframs, from, to);
        }

        List<EntUser> IAPIExtensions.GetContactList(EnmContactType userType, int? userID, int? offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetContactList(userType, userID, offenderID);
        }

        public List<int> GetFilterCountersForEvents(List<int> filterIDs, bool includeHistory, int? offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetFilterCountersForEvents(filterIDs, includeHistory, offenderID);
        }

        List<EntEventExtended> IAPIExtensions.GetLatestOffenderEvents(int eventLogID, int? rowCount)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetLatestOffenderEvents(eventLogID, rowCount);
        }

        List<EntEventExtended> IAPIExtensions.GetRelatedEvents(int eventLogID, int? rowCount)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetRelatedEvents(eventLogID, rowCount);
        }

        List<EntVoiceRetry> IAPIExtensions.GetVoiceRetryList(int eventID, string eventCode, int? offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetVoiceRetryList(eventID, eventCode, offenderID);
        }

        List<EntEventGroup> IAPIExtensions.GetEventGroupList()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetEventGroupList();
        }

        List<EntEventPicture> IAPIExtensions.GetAlcoholTestsEventIDList(int offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetAlcoholTestsEventIDList(offenderID);
        }

        List<EntOffenderPicture> IAPIExtensions.GetOffenderAutoFaceRecognitionPictures(int offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOffenderAutoFaceRecognitionPictures(offenderID);
        }

        public void AutoFaceRecognitionEnrollment(int offenderID, List<EntEnrollSelectedPictures> picturesList)
        {
            ((IAPIExtensions)APIExtensionsClient).AutoFaceRecognitionEnrollment(offenderID, picturesList);
        }

        List<EntOffenderPicture> IAPIExtensions.GetOffenderPicturesIDList(int offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOffenderPicturesIDList(offenderID);
        }

        List<EntGroupOffender> IAPIExtensions.GetUnallocatedOffenderList(EnmProgramGroup programGroup)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetUnallocatedOffenderList(programGroup);
        }

        public List<EntRuleFieldsSetup> GetMetadataForGPSRules()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetMetadataForGPSRules();
        }

        List<KeyValuePair<string, string>> IAPIExtensions.GetEventMessageOptionsForFilter(bool filterByProgramn, EnmProgramType program)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetEventMessageOptionsForFilter(filterByProgramn, program);
        }

        List<EntProgramVersion> IAPIExtensions.GetOffenderProgramVersions(int offenderID, int? relatedOffenderID, string where)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOffenderProgramVersions(offenderID, relatedOffenderID, where);
        }

        List<KeyValuePair<int, string>> IAPIExtensions.GetNonAllocatedVictims(int? offenderID, int? agencyID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetNonAllocatedVictims(offenderID, agencyID);
        }

        public void UpdateColumnData(List<EntColumnData> entColumnData)
        {
            ((IAPIExtensions)APIExtensionsClient).UpdateColumnData(entColumnData);
        }

        List<EntEquipmentBase> IAPIExtensions.GetEquipmentAutoCompleteOptions(EnmEquipmentType type, string search, int max, int agencyID, int offenderID, EnmProgramType programType, string receiverSN, string transmitterSN, string deuSN, bool isDV, int? relatedOffenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetEquipmentAutoCompleteOptions(type, search, max, agencyID, offenderID, programType, receiverSN, transmitterSN, deuSN, isDV, relatedOffenderID);
        }

        List<KeyValuePair<string, string>> IAPIExtensions.GetChargeList()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetChargeList();
        }

        public List<EntEquipmentBase> GetExistingEquipmentList(List<EntEquipmentBase> equipmentList)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetExistingEquipmentList(equipmentList);
        }

        public List<KeyValuePair<EntEquipmentBase, string>> ValidateImportEquipmentList(List<EntEquipmentBase> equipmentList, bool returnFailList)
        {
            return ((IAPIExtensions)APIExtensionsClient).ValidateImportEquipmentList(equipmentList, returnFailList);
        }

        List<EntSysTableItem> IAPIExtensions.GetRFRanges()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetRFRanges();
        }

        List<EntSysTableItem> IAPIExtensions.GetRingVolumes()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetRingVolumes();
        }

        public List<EntEquipmentDataResponse> UpdateEquipmentList(List<EntEquipmentUpdateData> EquipmentList)
        {
            return ((IAPIExtensions)APIExtensionsClient).UpdateEquipmentList(EquipmentList);
        }

        List<string> IAPIExtensions.GetUnregisteredUsers()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetUnregisteredUsers();
        }

        List<EntAgency> IAPIExtensions.GetUnfilteredAgencies()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetUnfilteredAgencies();
        }

        List<EntUser> IAPIExtensions.GetUnfilteredUsers()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetUnfilteredUsers();
        }

        public string GetApplicationVersion()
        {
            return ((IAPIExtensions)APIExtensionsClient).GetApplicationVersion();
        }

        public List<EntEvent> GetLatestOpenEvents(int count, string search, List<int> agencies, List<int> officers, List<int> offenders)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetLatestOpenEvents(count, search, agencies, officers, offenders);
        }

        public List<List<KeyValuePair<string, int>>> GetEventsStatistics(string search, List<int> agencies, List<int> officers, List<int> offenders)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetEventsStatistics(search, agencies, officers, offenders);
        }

        public Dictionary<string, int> GetOffendersCounters(List<int> agencies, List<int> officers, List<int> offenders)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOffendersCounters(agencies, officers, offenders);
        }

        public Dictionary<int, string> GetOffendersNamesList(List<int> offenderIDs, bool addRefID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOffendersNamesList(offenderIDs, addRefID);
        }

        public GetDataResultOfEntArchivedOffenderypU6tq2N GetArchivedOffenders(string search, int start, int max)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetArchivedOffenders(search, start, max);
        }

        public List<int> GetOffendersIDs(int filterID, EntSortExpression sortBy, string search, bool archivedOffender)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOffendersIDs(filterID, sortBy, search, archivedOffender);
        }

        public EntOnlineTrailData GetOnlineTrail(DateTime fromTime, EntOffenderForFullPointRequest offenderParams, EntOffenderForFullPointRequest relatedOffenderParams, EnmCoordinatesFormat coordinatesFormat)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOnlineTrail(fromTime, offenderParams, relatedOffenderParams, coordinatesFormat);
        }

        public EntOnlineModeData GetOnlineOffenderData(int offenderID)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetOnlineOffenderData(offenderID);
        }

        public List<EntColumnData> GetColumnsData(string screenName, EnmResultCode? resultCode, int? userId)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetColumnsData(screenName, resultCode, userId);
        }

        public EntEquipmentListData GetEquipmentListFofEquipmentScreen(EntMsgGetEquipmentListRequestForEquipmentScreen request)
        {
            return ((IAPIExtensions)APIExtensionsClient).GetEquipmentListFofEquipmentScreen(request);
        }

        public void UpdateEquipmentAgency(int? agencyID, List<int> equipmentIDList)
        {
            ((IAPIExtensions)APIExtensionsClient).UpdateEquipmentAgency(agencyID, equipmentIDList);
        }

        public GetDataResultOfEntOffenderShortypU6tq2N GetOffendersByFilterID(int filter, string search, EntSortExpression sort, int start, int max)
        {
            try
            {
                return APIExtensionsClient.GetOffendersByFilterID(filter, search, sort, start, max);
            }
            catch (FaultException<ValidationFault> fe)
            {
                throw new Exception(fe.Detail.Errors[0].Message, fe);
            }

        }

        public void UpdateZoneTemplateDefinitions(Dictionary<EnmDBAction, List<EntZoneTemplateName>> zoneTemplateDefinitions)
        {
            throw new NotImplementedException();
        }
    }
}
