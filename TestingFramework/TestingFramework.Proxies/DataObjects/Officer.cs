﻿namespace TestingFramework.Proxies.DataObjects
{
    public class Officer : User
    {
        public string MRDSerialNo { get; set; }

        public Officer()
        {

        }

        public Officer(TestingFramework.Proxies.EM.Interfaces.Users.EntOfficer entOfficer):
            base(entOfficer)
        {
            MRDSerialNo = entOfficer.MRDSerialNo;
        }
    }
}
