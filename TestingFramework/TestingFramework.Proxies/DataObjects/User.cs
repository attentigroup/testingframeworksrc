﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.DataObjects
{
    public enum UserStatus
    {
        Active,
        Locked,
        NotActive,
        Deleted,
    }
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public UserStatus UserStatus { get; set; }

        public string Password { get; set; }


        public User()
        {

        }

        public User(TestingFramework.Proxies.EM.Interfaces.Users.EntUser entUser)
        {
            Id = entUser.ID;
            FirstName = entUser.FirstName;
            UserStatus = (TestingFramework.Proxies.DataObjects.UserStatus)entUser.UserStatus;
        }
    }
}
