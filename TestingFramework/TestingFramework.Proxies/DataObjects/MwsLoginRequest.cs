﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.DataObjects.Mws
{
    public class MwsLoginRequest
    {
        public MwsLoginRequest()
        {

        }
        public MwsLoginRequest(string un, string ps)
        {
            UserName = un;
            Password = ps;
        }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class MwsCredentialsFull
    {
        public MwsLoginRequest credentials { get; set; }

        public MwsCredentialsFull()
        {
            credentials = new MwsLoginRequest();
        }
    }
}
