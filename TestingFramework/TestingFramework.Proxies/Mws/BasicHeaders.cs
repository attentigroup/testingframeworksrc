﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Mws
{
    public class BasicHeaders
    {
        public string MobileID { get; set; }
        public string AppId { get; set; }
    }
}
