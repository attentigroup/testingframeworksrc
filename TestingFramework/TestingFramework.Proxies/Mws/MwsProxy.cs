﻿using System;
using System.Collections.Generic;
using log4net;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.Proxies.DataObjects.Mws;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using System.Reflection;

namespace TestingFramework.Proxies.Mws
{

    public class MwsClient : WebClient
    {

        public MwsClient()
        {
            Headers.Add(HttpRequestHeader.ContentType, HeadersConsts.Header_Content_Type_Application_Json);
            Headers.Add(HttpRequestHeader.UserAgent, "Mws User Agent"); //Mws User Agent //Mozilla/5.0
            Headers.Add(HttpRequestHeader.Accept, "*/*");
            Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
        }


        public MwsClient(BaseRequest baseRequest) : this()
        {
            SetBasicHeaders(baseRequest);
        }


        public void SetBasicHeaders(BaseRequest baseRequest)
        {
            Headers.Add(RequestHeader.RequestHeader_MobileID, baseRequest.MobileID);
            Headers.Add(RequestHeader.RequestHeader_AppID, baseRequest.ApplicationID);
            Headers.Add(RequestHeader.RequestHeader_AppVersion, baseRequest.AppVersion);

            if (baseRequest.SessionToken != null)
            {
                Headers.Add(RequestHeader.RequestHeader_SessionToken, baseRequest.SessionToken);
            }
        }
    }


    public class MwsProxy
    {
        #region consts

        private const string MwsAddressToken = "MobileWebServiceAddress";

        //private const string MwsLoginServiceToken = "LoginService";
        //private const string MwsGlobalServiceToken = "GlobalService";
        //private const string MwsMonitorServiceToken = "MonitorService";


        private const string MwsGetMobileStatusMethodToken = "GetMobileStatusMethod";
        private const string MwsValidateLoginMethodToken = "ValidateLoginMethod";
        private const string MwsLogoutMethodToken = "LogoutMethod";
        private const string MwsRegisterMobileMethodToken = "RegisterMobileMethod";
        private const string MwsGetTranslationsMethodToken = "GetTranslationsMethod";
        private const string MwsGetMetaDataMethodToken = "GetMetaDataMethod";
        private const string MwsGetOffendersListMethodToken = "GetOffendersListMethod";

        #endregion consts

        private static readonly object Lockobject = new object();

        private static volatile MwsProxy _mwsProxy = null;

        /// <summary>
        /// Log object for all errors from that proxy.
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /* Services Uri's */
        //public string LoginServiceUri { get; set; }

        //public string GlobalServiceUri { get; set; }

        //public string MonitorServiceUri { get; set; }


        /* Methods Uri's */
        public string GetMobileStatusFullUri { get; set; }

        public string ValidateLoginFullUri { get; set; }

        public string LogoutFullUri { get; set; }

        public string RegisterMobileFullUri { get; set; }

        public string GetTranslationsFullUri { get; set; }

        public string GetMetaDataFullUri { get; set; }

        public string GetOffendersListFullUri { get; set; }

        public static MwsProxy Instance
        {
            get
            {
                if (_mwsProxy == null)
                {
                    lock (Lockobject)
                    {
                        if (_mwsProxy == null)
                        {
                            return _mwsProxy = new MwsProxy();
                        }
                    }
                }

                return _mwsProxy;
            }
        }

        private MwsProxy()
        {
            InitClient();
        }

        private void InitClient()
        {
            var mwsUri = ConfigurationManager.AppSettings[MwsAddressToken];

            //var loginServiceEndPoint = ConfigurationManager.AppSettings[MwsLoginServiceToken];
            //var globalServiceEndPoint = ConfigurationManager.AppSettings[MwsGlobalServiceToken];
            //var monitorServiceEndPoint = ConfigurationManager.AppSettings[MwsMonitorServiceToken];

            var GetMobileStatusMethod = ConfigurationManager.AppSettings[MwsGetMobileStatusMethodToken];
            var ValidateLoginMethod = ConfigurationManager.AppSettings[MwsValidateLoginMethodToken];
            var LogoutMethod = ConfigurationManager.AppSettings[MwsLogoutMethodToken];
            var RegisterMobileMethod = ConfigurationManager.AppSettings[MwsRegisterMobileMethodToken];
            var GetTranslationsMethod = ConfigurationManager.AppSettings[MwsGetTranslationsMethodToken];
            var GetMetaDataMethod = ConfigurationManager.AppSettings[MwsGetMetaDataMethodToken];
            var GetOffendersListMethod = ConfigurationManager.AppSettings[MwsGetOffendersListMethodToken];

            // TODO:  Build string in methods
            // Uri for service endpoints
            //LoginServiceUri = mwsUri + loginServiceEndPoint;
            //GlobalServiceUri = mwsUri + globalServiceEndPoint;
            //MonitorServiceUri = mwsUri + monitorServiceEndPoint;

            // Uri for methods
            GetMobileStatusFullUri = mwsUri + GetMobileStatusMethod;
            ValidateLoginFullUri = mwsUri + ValidateLoginMethod;
            LogoutFullUri = mwsUri + LogoutMethod;
            RegisterMobileFullUri = mwsUri + RegisterMobileMethod;
            GetTranslationsFullUri = mwsUri + GetTranslationsMethod;
            GetMetaDataFullUri = mwsUri + GetMetaDataMethod;
            GetOffendersListFullUri = mwsUri + GetOffendersListMethod;


            //avoid certification error
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
            {
                return true;
            };
        }

        /// <summary>
        /// Login Service - GetMobileStatus Method
        /// </summary>
        /// <param name="GetMobileStatusRequest"></param>
        /// <returns></returns>
        public string GetMobileStatus(EntMsgMwsGetMobileStatusRequest GetMobileStatusRequest)
        {

            var mwsClient = new MwsClient(GetMobileStatusRequest);
            string jsonResponse = null;

            try
            {
                byte[] responseArray = mwsClient.DownloadData(GetMobileStatusFullUri);
                jsonResponse = Encoding.ASCII.GetString(responseArray);

                if (jsonResponse == null)
                    throw new Exception(jsonResponse);
            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "GetMobileStatus");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return jsonResponse;
        }


        /// <summary>
        /// Login Service - Login Method
        /// </summary>
        /// <param name="MwsValidateLoginRequest"></param>
        /// <returns></returns>
        public EntMsgMwsValidateLoginResponse Login(EntMsgMwsValidateLoginRequest MwsValidateLoginRequest)
        {
            EntMsgMwsValidateLoginResponse loginResponse = null;

            var mwsCredentialsFull = new MwsCredentialsFull();
            mwsCredentialsFull.credentials.UserName = MwsValidateLoginRequest.Username;
            mwsCredentialsFull.credentials.Password = MwsValidateLoginRequest.Password;

            var jss = new JavaScriptSerializer();
            string json = jss.Serialize(mwsCredentialsFull);
            byte[] data = Encoding.Default.GetBytes(json);

            var mwsClient = new MwsClient(MwsValidateLoginRequest);

            try
            {
                byte[] responseArray = mwsClient.UploadData(ValidateLoginFullUri, "POST", data);
                string jsonResponse = Encoding.ASCII.GetString(responseArray);

                loginResponse = jss.Deserialize<EntMsgMwsValidateLoginResponse>(jsonResponse);

                if (!loginResponse.Message.Contains("Success"))
                    throw new Exception(loginResponse.Message);

                loginResponse.mwsSecurityData.SessionToken = mwsClient.ResponseHeaders["SESSION_TOKEN"];
                //loginResponse.mwsSecurityData.STT = mwsClient.ResponseHeaders["STT"];

            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "Login");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return loginResponse;
        }


        /// <summary>
        /// Login Service - Logout Method
        /// </summary>
        /// <param name="MwsLogoutRequest"></param>
        public void Logout(EntMsgMwsLogoutRequest MwsLogoutRequest)
        {
            var mwsClient = new MwsClient(MwsLogoutRequest);
            try
            {
                byte[] responseArray = mwsClient.DownloadData(LogoutFullUri);
                string jsonResponse = Encoding.ASCII.GetString(responseArray);

                if (!jsonResponse.Contains("Success"))
                    throw new Exception(jsonResponse);
            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "Logout");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Global Service - Get Translations Method
        /// </summary>
        /// <param name="MwsLogoutRequest"></param>
        public string GetTranslations(EntMsgMwsGetTranslationsRequest GetTranslationsRequest)
        {
            var mwsClient = new MwsClient(GetTranslationsRequest);
            string jsonResponse = null;

            try
            {
                byte[] responseArray = mwsClient.DownloadData(GetTranslationsFullUri);
                jsonResponse = Encoding.ASCII.GetString(responseArray);

                if (jsonResponse == null)
                    throw new Exception(jsonResponse);
            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "GetTranslations");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return jsonResponse;
        }


        /// <summary>
        /// Global Service - Get MetaData Method
        /// </summary>
        /// <param name="MwsLogoutRequest"></param>
        public string GetMetaData(EntMsgMwsGetMetaDataRequest GetMetaDataRequest)
        {
            var mwsClient = new MwsClient(GetMetaDataRequest);
            string jsonResponse = null;
            try
            {
                byte[] responseArray = mwsClient.DownloadData(GetMetaDataFullUri);
                jsonResponse = Encoding.ASCII.GetString(responseArray);

                if (jsonResponse == null)
                    throw new Exception(jsonResponse);
            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "GetMetaData");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return jsonResponse;
        }


        /// <summary>
        /// Login Service - Register Mobile Method
        /// </summary>
        /// <param name="MwsRegisterRequest"></param>
        public void RegisterMobile(EntMsgMwsRegisterRequest MwsRegisterRequest)
        {
            var jss = new JavaScriptSerializer();
            string json = jss.Serialize(MwsRegisterRequest);
            byte[] data = Encoding.Default.GetBytes(json);

            var mwsClient = new MwsClient(MwsRegisterRequest);
            string jsonResponse = null;

            try
            {
                byte[] responseArray = mwsClient.UploadData(RegisterMobileFullUri, "POST", data);
                jsonResponse = Encoding.ASCII.GetString(responseArray);
                if (!jsonResponse.ToLower().Contains("true"))
                {
                    var errorMsg = string.Format("Register Mobile Failed with response: {0}", jsonResponse);
                    throw new Exception(errorMsg);
                }

            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "RegisterMobile");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Login Service - Login Method
        /// </summary>
        /// <param name="MwsValidateLoginRequest"></param>
        /// <returns></returns>
        public string GetOffendersList(EntMsgMwsGetOffendersListRequest GetOffendersListRequest)
        {
            string jsonResponse = null;
            GetOffendersListData getOffendersListData = new GetOffendersListData();
            getOffendersListData.isOnlyActive = GetOffendersListRequest.isOnlyActive;
            getOffendersListData.isOnlyViolation = GetOffendersListRequest.isOnlyViolation;

            var jss = new JavaScriptSerializer();
            string json = jss.Serialize(getOffendersListData);
            byte[] data = Encoding.Default.GetBytes(json);

            var mwsClient = new MwsClient(GetOffendersListRequest);

            try
            {
                byte[] responseArray = mwsClient.UploadData(GetOffendersListFullUri, "POST", data);
                jsonResponse = Encoding.ASCII.GetString(responseArray);

                if (jsonResponse == null)
                    throw new Exception(jsonResponse);

            }
            catch (FaultException fe)
            {
                throw new Exception(fe.Message, fe);
            }
            catch (WebException ex)
            {
                HandleWebException(ex, "Get Offenders List");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return jsonResponse;
        }


        private static void HandleWebException(WebException ex, string exceptionOrigin)
        {
            if (ex.Status != WebExceptionStatus.ProtocolError || ex.Response == null) return;

            Stream receiveStream = ex.Response.GetResponseStream();
            Encoding encode = Encoding.GetEncoding("utf-8");

            if (receiveStream == null)
            {
                string error = string.Format("No response stream at {0}.", exceptionOrigin);
                Log.Error(error);
                throw new Exception(error, ex);
            }

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            using (var readStream = new StreamReader(receiveStream, encode))
            {
                string error = "Error at " + exceptionOrigin + ": " + readStream.ReadToEnd();
                Log.Error(error);

                //TODO: need to add special case for each error based on text from the body of the response that saved to string error
                if (error.ToLower().Contains("user"))
                {
                    throw new Exception("Failed to login because user name invalid.see inner for parameters.", ex);
                    //throw new LoginException("Failed to login because user name invalid.see inner for parameters.", ex);
                }
                //else if(error.Contains something)
                //{
                //}
                //...
                else//in case of unknown error
                {
                    string unknownError = string.Format("Response stream at {0} containd unknown error:{1}", exceptionOrigin, error);
                    Log.Error(unknownError);
                    throw new Exception(unknownError, ex);
                }
                #region special cases examples
                //if (error.Contains("has the same priority"))
                //{
                //    throw new SamePriorityException(ex);
                //}
                //if (error.Contains("No rule exists by that GUID"))
                //{
                //    throw new NoRuleExistsException(ex);
                //}
                //if (error.Contains("No Rule Match the critirias"))
                //{
                //    throw new NoMatchedRuleFoundException(ex);
                //}
                //if (error.Contains("already exists"))
                //{
                //    throw new RuleAlreadyExistsException(ex);
                //} 
                #endregion
            }
        }
    }


    public class BaseRequest
    {
        public string AppVersion { get; set; }

        public string ApplicationID { get; set; }

        public string MobileID { get; set; }

        public string SessionToken { get; set; }

        //public string STT { get; set; }
    }


    public class EntMsgMwsGetMobileStatusRequest : BaseRequest
    {

    }


    public class EntMsgMwsValidateLoginRequest : BaseRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }


    public class EntMsgMwsValidateLoginResponse
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }

        public MwsSecurityData mwsSecurityData { get; set; }

        public EntMsgMwsValidateLoginResponse()
        {
            mwsSecurityData = new MwsSecurityData();
        }
    }


    public class EntMsgMwsLogoutRequest : BaseRequest
    {

    }


    public class EntMsgMwsRegisterRequest : BaseRequest
    {
        public EntMsgMwsRegisterRequest()
        {
            Credentials credentials = new Credentials();
            MobileData mobileData = new MobileData();
        }

        public Credentials credentials { get; set; }
        public MobileData mobileData { get; set; }
    }


    public class EntMsgMwsGetTranslationsRequest : BaseRequest
    {

    }


    public class EntMsgMwsGetMetaDataRequest : BaseRequest
    {

    }


    public class EntMsgMwsGetOffendersListRequest : BaseRequest
    {
        public bool isOnlyActive { get; set; }

        public bool isOnlyViolation { get; set; }
    }


    public class MwsSecurityData
    {
        public string SessionToken { get; set; }
        //public string STT { get; set; }
    }


    public class Credentials
    {

        public string UserName { get; set; }

        public string Password { get; set; }
    }


    public class MobileData
    {
        public string Model { get; set; }
        public string OperatingSystem { get; set; }
        public string CellularNumber { get; set; }
    }


    public class GetOffendersListData
    {
        public bool isOnlyActive { get; set; }
        public bool isOnlyViolation { get; set; }
    }


    //public class RegisterPayload
    //{
    //    public RegisterPayload()
    //    {
    //        LoginData LoginData = new LoginData();
    //        MobileData MobileData = new MobileData();
    //    }

    //    public LoginData LoginData { get; set; }
    //    public MobileData MobileData { get; set; }
    //}



}






