﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Runtime.InteropServices;

namespace TestingFramework.Proxies.Tools
{
    public enum EnmAuthenticateResult
    {
        Success, Failed, Disabled, Locked, MustChangePassword
    }
    public enum EnmUserStatus
    {
        Active,
        Locked,
        NotActive,
        Deleted
    }

    public class ActiveDirectiryHelper
    {
        #region Const

        public string ActiveDirectoryDomainKey = "Active_Directory_Domain";
        public string ActiveDirectoryContainerKey = "Active_Directory_Container";
        public string ActiveDirectoryAdministratorUsernameKey = "Active_Directory_Administrator_Username";
        public string ActiveDirectoryAdministratorPasswordKey = "Active_Directory_Administrator_Password";

        #endregion Const


        #region Fields

        private string activeDirectoryContainer = null;

        public string ActiveDirectoryContainer
        {
            get
            {
                if (string.IsNullOrEmpty(activeDirectoryContainer))
                {
                    activeDirectoryContainer = ConfigurationManager.AppSettings[ActiveDirectoryContainerKey];
                }
                return activeDirectoryContainer;
            }
            set { }
        }


        private string activeDirectoryDomain = null;

        public string ActiveDirectoryDomain
        {
            get
            {
                if(string.IsNullOrEmpty(activeDirectoryDomain))
                {
                    activeDirectoryDomain = ConfigurationManager.AppSettings[ActiveDirectoryDomainKey];
                }
                return activeDirectoryDomain;
            }
            set { }
        }


        private string administratorUserName = null;
        public string AdministratorUserName
        {
            get
            {
                if(string.IsNullOrEmpty(administratorUserName))
                {
                    administratorUserName = StringCipher.Decrypt(ConfigurationManager.AppSettings[ActiveDirectoryAdministratorUsernameKey]);
                }
                return administratorUserName;                
            }
            set { }
        }

        string key = "Authenticated|Usernames";

        private PrincipalContext administrator;
        private PrincipalContext Administrator
        {
            get
            {
                if (administrator == null)
                {
                    try
                    {
                        
                        var password = StringCipher.Decrypt(ConfigurationManager.AppSettings[ActiveDirectoryAdministratorPasswordKey]);

                        administrator = new PrincipalContext(ContextType.Domain,
                                                             ActiveDirectoryDomain,
                                                             ActiveDirectoryContainer,
                                                             ContextOptions.Negotiate,
                                                             AdministratorUserName,
                                                             password);
                    }
                    catch (Exception ex)
                    {
                        // in this case 'ex' means that AD service is not available, or one of the config parameters is wrong 
                        var adEx = GetActiveDirectoryError(ex);
                        throw adEx;
                    }
                }
                return administrator;
            }
            set { }
        }

        #endregion Fields

        #region Constractors
        public ActiveDirectiryHelper()
        {

        }

        #endregion Constractors

        #region Public

        public EnmAuthenticateResult Authenticate(string username, string password)
        {
            // check that account exists in the domain
            var user = UserPrincipal.FindByIdentity(Administrator, IdentityType.SamAccountName, username);
            if (user == null)
                return EnmAuthenticateResult.Failed; // incorrect username   

            if (Administrator.ValidateCredentials(username, password))
                return EnmAuthenticateResult.Success;

            // in case authenticate failed, find out the reason                      

            // account is not active or account expired
            if (user.Enabled == false || (user.AccountExpirationDate != null && user.AccountExpirationDate <= DateTime.Now))
                return EnmAuthenticateResult.Disabled;

            // account is locked 
            if (user.IsAccountLockedOut())
                return EnmAuthenticateResult.Locked;

            // in case password must change or password expired, make sure the credentials are correct
            if (user.PasswordNeverExpires == false)
            {
                // find whether password must change or password expired
                var passwordMustChange = user.LastPasswordSet == null;
                var passwordExpired = false;
                if (passwordMustChange == false)
                {
                    DirectoryEntry entry = (System.DirectoryServices.DirectoryEntry)user.GetUnderlyingObject();
                    ActiveDs.IADsUser native = (ActiveDs.IADsUser)entry.NativeObject;
                    passwordExpired = native.PasswordExpirationDate <= DateTime.Now;
                }

                if (passwordMustChange || passwordExpired)
                {
                    // 1: temporary remove 'Password Must Change' flag
                    user.ExpirePasswordNow(); // this must be done in case passwordExpired before RefreshExpiredPassword()
                    user.RefreshExpiredPassword();

                    // 2: try to validate credentials 
                    var isPasswordCorrect = Administrator.ValidateCredentials(username, password);

                    // 3: return the 'Password Must Change' flag
                    user.ExpirePasswordNow();
                    user.Save();

                    // 4: in case the second attempt to validate credentials (step 2) changed the account to be locked, unlock the account
                    user.UnlockAccount();

                    if (isPasswordCorrect)
                    {
                        return EnmAuthenticateResult.MustChangePassword;
                    }
                }
            }

            // in this case the error must be incorrect password
            return EnmAuthenticateResult.Failed;
        }

        public List<KeyValuePair<string, EnmUserStatus>> GetAllUsers()
        {
            var list = new List<KeyValuePair<string, EnmUserStatus>>();

            PrincipalSearcher search = new PrincipalSearcher(new UserPrincipal(Administrator));

            foreach (UserPrincipal user in search.FindAll())
            {
                // AD administrator is not one of the login users
                if (user.SamAccountName == AdministratorUserName)
                    continue;

                EnmUserStatus status = EnmUserStatus.Active;

                // user is not active or account expired
                if (user.Enabled == false || (user.AccountExpirationDate != null && user.AccountExpirationDate <= DateTime.Now))
                    status = EnmUserStatus.NotActive;
                else
                {
                    // user is locked 
                    if (user.IsAccountLockedOut())
                        status = EnmUserStatus.Locked;
                }

                list.Add(new KeyValuePair<string, EnmUserStatus>(user.SamAccountName, status));
            }

            return list;
        }


        public UserPrincipal GetUserByUserName(string userName)
        {
            return UserPrincipal.FindByIdentity(Administrator, IdentityType.SamAccountName, userName);
        }

        public bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            // find whether password must be changed or password expired, if so, we need to use admin credentials 
            // to temporary remove the flag to make 'Change Password' action more smoothly

            var temp_user = UserPrincipal.FindByIdentity(Administrator, IdentityType.SamAccountName, username);
            var passwordCurrentlyExpired = temp_user.LastPasswordSet == null; // password must change is set to 'true'
            if (!passwordCurrentlyExpired)
            {
                DirectoryEntry entry = (System.DirectoryServices.DirectoryEntry)temp_user.GetUnderlyingObject();
                ActiveDs.IADsUser native = (ActiveDs.IADsUser)entry.NativeObject;
                passwordCurrentlyExpired = native.PasswordExpirationDate <= DateTime.Now; // password expired
            }

            if (passwordCurrentlyExpired)
            {
                // temporary remove 'Password Must Change' flag
                temp_user.ExpirePasswordNow(); // this must be done in case passwordExpired before RefreshExpiredPassword()
                temp_user.RefreshExpiredPassword();
                temp_user.Save();
            }

            try
            {
                // 'Change Password' action must be done using user credentials to force password policy

                // create context using domain\username format (full logon name), this is done so to fix known issue caused by Windows update 3178465
                // details can be found in: https://support.microsoft.com/en-us/kb/3178465

                var domain = ActiveDirectoryDomain;
                var fullLogonName = (domain.EndsWith(".com") ? domain.Replace(".com", "") : domain) + "\\" + username;
                var context = new PrincipalContext(ContextType.Domain,
                                                   domain,
                                                   ActiveDirectoryContainer,
                                                   fullLogonName,
                                                   oldPassword);

                var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, username);

                user.ChangePassword(oldPassword, newPassword);
                user.Save();

                return true;
            }
            catch (Exception x)
            {
                if (passwordCurrentlyExpired)
                {
                    // return 'Password Must Change' flag to the original state
                    temp_user.ExpirePasswordNow();
                    temp_user.Save();
                }
                throw;
            }
        }

        public bool ResetPassword(string username, string newPassword)
        {
            try
            {
                // use Administrator context to reset the password 
                var user = UserPrincipal.FindByIdentity(Administrator, IdentityType.SamAccountName, username);
                user.SetPassword(newPassword);
                user.ExpirePasswordNow();
                user.Save();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void UnlockUser(string username)
        {
            // unlock account
            var user = UserPrincipal.FindByIdentity(Administrator, IdentityType.SamAccountName, username);
            user.UnlockAccount();
            user.Save();
        }

        public void EnableDisableUser(string username, bool enable)
        {
            // enable/disable account
            var user = UserPrincipal.FindByIdentity(Administrator, IdentityType.SamAccountName, username);

            // if request is to enable the user acount, and acount is expired, fix the acount expiration date
            if (enable && (user.AccountExpirationDate != null && user.AccountExpirationDate <= DateTime.Now))
            {
                // enable user who's disabled and also who's date is expired.
                user.AccountExpirationDate = DateTime.Now.AddYears(1);
            }
            user.Enabled = enable;
            user.Save();
        }

        public void AddUser(string username, string password, bool isEnabled)
        {
            UserPrincipal oUserPrincipal = new UserPrincipal(Administrator, username, password, isEnabled);
            oUserPrincipal.UserPrincipalName = username;
            oUserPrincipal.Save();
        }

        #endregion Public

        #region Private

        private Exception GetActiveDirectoryError(Exception ex)
        {
            Exception exception = null;

            if (ex is ActiveDirectoryOperationException)
            {
                exception = new Exception("Active directory operation failure", ex);
            }
            if (ex is ActiveDirectoryServerDownException)
            {
                exception = new Exception("Active directory server down", ex);
            }
            if (ex is DirectoryServicesCOMException)
            {
                exception = new Exception("Directory services COM failure", ex);
            }
            if (ex is COMException)
            {
                exception = new Exception("Active directory Server is not operational", ex);
            }

            var msg = ex.Message;
            if (ex.InnerException != null && string.IsNullOrEmpty(ex.InnerException.Message) == false)
                msg += " - " + ex.InnerException.Message;

            exception = new Exception(string.Format("Active directory unknown error: {0}", msg));

            return exception;
        }

        #endregion Private

    }
}
