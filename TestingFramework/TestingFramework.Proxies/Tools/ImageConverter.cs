﻿using log4net;
using System;
using System.IO;
using System.Reflection;

namespace TestingFramework.Proxies.Common
{
    public static class ImageConverter 
    {
        /// <summary>
        /// Converts image file to byte[]
        /// </summary>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        /// 
        /// <summary>
        /// Log interface for all tests
        /// </summary>
        static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static byte[] GetBytesFromImage(String imageFile)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                Log.Debug($"Try to open image file {imageFile}");
                System.Drawing.Image img = System.Drawing.Image.FromFile(imageFile);
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (System.Exception exception)
            {
               var error = ($"try to open image file {imageFile}, but not found. message:{exception.Message}");
                throw new Exception(error, exception);
            }
            return ms.ToArray();
            
        }
    }
}
