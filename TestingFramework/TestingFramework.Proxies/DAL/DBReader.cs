﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;

namespace TestingFramework.Proxies.DAL
{
    public class DBReader
    {
        /// <summary>
        /// connect to DB
        /// </summary>
        /// <param name="strUserName"></param>
        /// <param name="strPassword"></param>
        /// <param name="strPort"></param>
        /// <param name="strHostName"></param>
        /// <param name="dbName"></param>
        /// <returns>return ODBC connection object to use in the execute method</returns>
        public OdbcConnection Connect(string driver, string strUserName, string strPassword, string strPort, string strHostName, string dbName)
        {
            OdbcConnection con = null;
            string conString = "Driver=" + driver + "; server=" + strHostName + ";" + "port=" + strPort + ";db=" + dbName + ";uid=" + strUserName + ";pwd=" + strPassword + ";";
            con = new OdbcConnection(conString);
            con.Open();
            return con;
        }

        /// <summary>
        /// execute qury
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="query"></param>
        /// <returns>execute the query </returns>
        public OdbcDataReader Execute(OdbcConnection connection, string query)
        {
            Console.WriteLine("In database {0}", connection.Database);
            OdbcCommand command = connection.CreateCommand();
            command.CommandText = query;
            OdbcDataReader reader = command.ExecuteReader();

            return reader;
        }

        /// <summary>
        /// read the data from the query and return list of the requested type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="typeOf"></param>
        /// <returns>List of type T with the values assigned as the property name or the "Odbc ColumnName Attribute" data</returns>
        public List<T> Read<T>(OdbcDataReader reader, Type typeOf)
        {
            var results = new List<T>();

            PropertyInfo[] propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            int fCount = reader.FieldCount;

            while (reader.Read())
            {
                var item = (T)Activator.CreateInstance(typeOf);
                foreach (var pi in propertyInfos)
                {
                    var columnNameAttribute = Attribute.GetCustomAttributes(pi, typeof(OdbcColumnNameAttribute), true);

                    try
                    {
                        var columnName = pi.Name;

                        if (columnNameAttribute.Length > 0)
                        {
                            OdbcColumnNameAttribute propertyOdbcAttribute = columnNameAttribute[0] as OdbcColumnNameAttribute;
                            columnName = propertyOdbcAttribute.ColumnName;
                        }
                        bool bIgnore = false;
                        //get the ignore attribute in case it is exist
                        var ignoreNameAttributes = Attribute.GetCustomAttributes(pi, typeof(OdbcColumnIgnoreAttribute), true);
                        if (ignoreNameAttributes.Length > 0)
                        {
                            OdbcColumnIgnoreAttribute ignoreNameAttribute = ignoreNameAttributes[0] as OdbcColumnIgnoreAttribute;
                            bIgnore = ignoreNameAttribute.Ignore;
                        }
                        if (bIgnore == false)
                        {
                            var columnType = reader[columnName].GetType();
                            if (columnType == typeof(DBNull))
                            {
                                //column return null.ignore it.
                                continue;
                            }
                            else if (columnType == typeof(string) || columnType == typeof(String))
                            {
                                var stringValue = reader[columnName] as string;
                                pi.SetValue(item, stringValue.TrimEnd());
                            }
                            else
                            {
                                pi.SetValue(item, reader[columnName]);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write("while try to set value to {0} get ex: {1}", pi.Name, ex.Message);
                    }
                }
                results.Add(item);
            }
            reader.Close();
            return results;
        }

        public void ExecuteStoredProcedure(OdbcConnection connection, ref IStoredProcedureFormat storedProcedure)
        {
            using (OdbcCommand cmd = new OdbcCommand(storedProcedure.StoredProcedure, connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedure.StoredProcedure;

                SetParameters(cmd, storedProcedure);
                cmd.ExecuteNonQuery();
            }
        }
        private void SetParameters(OdbcCommand cmd, IStoredProcedureFormat storedProcedure)
        {
            PropertyInfo[] propertyInfos = storedProcedure.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in propertyInfos)
            {
                var odbcSpParamAttributes = Attribute.GetCustomAttributes(property, typeof(OdbcSpParamAttribute), true);

                if(odbcSpParamAttributes.Length > 0 )
                {
                    OdbcSpParamAttribute odbcSpParamAttribute = odbcSpParamAttributes[0] as OdbcSpParamAttribute;
                    var odbcParameter = new OdbcParameter(string.Format("@{0}", property.Name), odbcSpParamAttribute.OdbcType, odbcSpParamAttribute.Size);
                    odbcParameter.Direction = odbcSpParamAttribute.ParameterDirection;
                    var parameter = cmd.CreateParameter();


                    cmd.Parameters.Add(string.Format("@{0}", property.Name), odbcSpParamAttribute.OdbcType, odbcSpParamAttribute.Size).Value = property.GetValue(storedProcedure);
                    //cmd.Parameters.AddWithValue(string.Format("@{0}", property.Name), property.GetValue(storedProcedure));
                    //cmd.Parameters.Add(odbcParameter).Value = property.GetValue(storedProcedure);
                }
            }
        }
    }
}
