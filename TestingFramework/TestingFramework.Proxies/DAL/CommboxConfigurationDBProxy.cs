﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Odbc;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;

namespace TestingFramework.Proxies.DAL
{
    public class CommboxConfigurationDBProxy
    {
        #region properties
        public DBReader DBReader { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Port { get; set; }

        public string HostName { get; set; }

        public string DbName { get; set; }

        private Dictionary<string, OdbcConnection> _connections = new Dictionary<string, OdbcConnection>();

        private string UserNameConfigToken = "DB_UserName";
        private string PasswordConfigToken = "DB_Password";
        private string PortConfigToken = "DB_Port";
        private string HostNameConfigToken = "DB_HostName";
        private string DbNameConfigToken = "DB_DbName";


        private Dictionary<Type, string> _queriesDictionary;
        /// <summary>
        /// dictionary of all property types and the data generator that implemented for it.
        /// </summary>
        public Dictionary<Type, string> QueriesDictionary
        {
            get { return _queriesDictionary; }
        }
        #endregion properties

        #region singleton

        private static CommboxConfigurationDBProxy _commboxConfigurationDBProxy;

        public static CommboxConfigurationDBProxy Instance
        {
            get
            {
                if (_commboxConfigurationDBProxy == null)
                {
                    _commboxConfigurationDBProxy = new CommboxConfigurationDBProxy();
                }

                return _commboxConfigurationDBProxy;
            }
        }

        private CommboxConfigurationDBProxy()
        {
            DBReader = new DBReader();
            ReadConfig();
            InitQueriesFactory();
        }

        private void InitQueriesFactory()
        {
            _queriesDictionary = new Dictionary<Type, string>();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var assmebly = assemblies.First(x => x.FullName.StartsWith("TestingFramework.Proxies"));
            var entitiesClassesTypes = assmebly.GetTypes().Where(
                type => type.IsClass && 
                type.Namespace != null 
                && type.Namespace.Contains("TestingFramework.Proxies.DAL.Entities.CommboxConfiguration"));

            foreach (var classType in entitiesClassesTypes)
            {
                var CompilerGenAttr = classType.GetCustomAttributes(typeof(CompilerGeneratedAttribute), true);
                if (CompilerGenAttr.Length > 0)
                {
                    //Log.DebugFormat("classType {0} will not load because because this is not real class", classType.ToString());
                    continue;
                }

                IQueryFormat iQueryFormat = null;
                try
                {
                    var defaultConstructor = classType.GetConstructor(Type.EmptyTypes);
                    if(defaultConstructor != null)
                    {
                        iQueryFormat = assmebly.CreateInstance(classType.FullName) as IQueryFormat;
                    }
                    else
                    {
                        //TODO: log because this class implemented without default constractor.
                    }
                    
                }
                catch (Exception ex)
                {
                    //TODO add logs
                    continue;
                }
                if (iQueryFormat == null)
                {
                    var errMsg = string.Format("ClassType:{0} couldnt created as a IQueryFormat", classType.FullName);
                    //m_Log.Wa(errMsg);
                    continue;
                }
                var type = iQueryFormat.GetType();
                if (QueriesDictionary.ContainsKey(type))
                {
                    //TODO : m_Log.Error(String.Format("A data generator for the type:{0} already been registered.", propertyType));
                }
                else
                {
                    QueriesDictionary.Add(type, iQueryFormat.QueryFormat);
                }
            }
        }

        private void ReadConfig()
        {
            UserName = ConfigurationManager.AppSettings[UserNameConfigToken];
            Password = ConfigurationManager.AppSettings[PasswordConfigToken];
            Port = ConfigurationManager.AppSettings[PortConfigToken];
            HostName = ConfigurationManager.AppSettings[HostNameConfigToken];
            DbName = ConfigurationManager.AppSettings[DbNameConfigToken];
    }

        #endregion singleton



        public OdbcConnection GetConnection(string strUserName, string strPassword, string strPort, string strHostName, string dbName)
        {
            OdbcConnection connection = null;

            var key = strUserName + strPassword + strPort + strHostName + dbName;
            if( _connections.ContainsKey(key) == false)
            {
                connection = DBReader.Connect(OdbcDriversParams.SYBASE_DRIVER, strUserName, strPassword, strPort, strHostName, dbName);
                _connections.Add(key, connection);
            }

            connection = _connections[key];

            return connection;
        }



        public EntAssignedDemoID GetAssignedDemoID(int MTDID)
        {
            var connection = GetConnection(UserName, Password, Port, HostName, DbName);
            var query = string.Format(EntAssignedDemoID.QueryFormat, MTDID);
            OdbcDataReader reader = DBReader.Execute(connection, query);
            List<EntAssignedDemoID> results = DBReader.Read<EntAssignedDemoID>(reader, typeof(EntAssignedDemoID));
            return results[0];
        }

        public EntProtechEDemographicsParams GetDemographicsParams(params object[] args)
        {
            var parameters = GetEntityByArgs<EntProtechEDemographicsParams>(typeof(EntProtechEDemographicsParams), args);
            return parameters;
        }

        public EntProtechEOffenderParams GetProtechEOffenderParams(params object[] args)
        {
            var parameters = GetEntityByArgs<EntProtechEOffenderParams>(typeof(EntProtechEOffenderParams), args);
            return parameters;
        }

        public EntProtechEMTDParameters GetProtechEMTDParameters(params object[] args)
        {
            var parameters = GetEntityByArgs<EntProtechEMTDParameters>(typeof(EntProtechEMTDParameters), args);
            return parameters;
        }

        public EntProtechEOffenderMtdParams GetProtechEOffenderMtdParams(params object[] args)
        {
            var parameters = GetEntityByArgs<EntProtechEOffenderMtdParams>(typeof(EntProtechEOffenderMtdParams), args);
            return parameters;
        }

        

        public T GetEntityByArgs<T>(Type queryFormatType, params object[] args)
        {
            var queryTemplate = QueriesDictionary[queryFormatType];

            var connection = GetConnection(UserName, Password, Port, HostName, DbName);
            var query = string.Format(queryTemplate, args);
            OdbcDataReader reader = DBReader.Execute(connection, query);
            var results = DBReader.Read<T>(reader, typeof(T));
            return results[0];
        }

        public void RunQueryByArgs<T>(Type queryFormatType, params object[] args)
        {
            var queryTemplate = QueriesDictionary[queryFormatType];

            var connection = GetConnection(UserName, Password, Port, HostName, DbName);
            var query = string.Format(queryTemplate, args);
            OdbcDataReader reader = DBReader.Execute(connection, query);
            DBReader.Read<T>(reader, typeof(T));            
        }



        public void RunSP(ref IStoredProcedureFormat storedProcedure)
        {
            var connection = GetConnection(UserName, Password, Port, HostName, DbName);
            DBReader.ExecuteStoredProcedure(connection, ref storedProcedure);
        }
    }
}
