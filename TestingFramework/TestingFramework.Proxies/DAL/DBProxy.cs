﻿using Common.Entities.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.DAL
{
    public class OdbcDriversParams
    {
        public const string SYBASE_DRIVER = "{Adaptive Server Enterprise}";
        public const string ORACLE_DRIVER = "{Microsoft ODBC for Oracle}";
    }

    public class DBProxy
    {
        public SUM_COL QuerySybase(string strUserName, string strPassword, string strPort, string strHostName, string dbName, string query)
        {
            var dbReader = new DBReader();
            var conn = dbReader.Connect(OdbcDriversParams.SYBASE_DRIVER, strUserName, strPassword, strPort, strHostName, dbName);
            var reader = dbReader.Execute(conn, query);

            var result = dbReader.Read<SUM_COL>(reader, typeof(SUM_COL));
            if (result.Count != 1)
            {
                throw new Exception(string.Format("QuerySybase expect 1 item in list and got {0}.", result.Count));
            }

            return result[0];
        }
    }
}
