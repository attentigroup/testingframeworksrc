﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.DAL.Entities.CommboxConfiguration
{
    public abstract class IQueryFormat
    {
        [OdbcColumnIgnore(true)]
        public string QueryFormat { get; set; }
    }

    public class EntProtechEDemographicsParams : IQueryFormat
    {
        public int StandardTimeOffset { get; set; }
        public int DaylightTimeOffset { get; set; }

        public short DemographicType { get; set; }

        public EntProtechEDemographicsParams()
        {
            QueryFormat = "SELECT StandardTimeOffset, DaylightTimeOffset, DemographicType FROM ProtechE.dbo.Demographics WHERE DemographicID = {0}";

        }
    }

    public class EntChargeLow : IQueryFormat
    {
        public int ChargeLow { get; set; }

        public EntChargeLow()
        {
            QueryFormat = "select ChargeLow from ProtechE.dbo.MTDParameters where MTDID = {0}";
        }        
    }

    public class EntAssignedDemoID
    {
        public int AssignedDemoID { get; set; }
        public static string QueryFormat
        {
            get { return "select AssignedDemoID from ProtechE.dbo.AssemblyMTDs where MTDID = {0}"; }
        }
    }

    public class EntProtechEOffenderParams : IQueryFormat
    {
        public int BraceletID { get; set; }
        public byte BraceletType { get; set; }

        public DateTime MonitorStartTimestamp { get; set; }//local time

        public DateTime MonitorEndTimestamp { get; set; }//local time

        public EntProtechEOffenderParams()
        {
            QueryFormat = "select BraceletID, BraceletType, MonitorStartTimestamp, MonitorEndTimestamp " +
                  "from ProtechE.dbo.Offenders where DemographicID = {0}";
        }
    }

    public class EntProtechEOffenderMtdParams : IQueryFormat
    {
        public short BraceletGonePreGrace { get; set; }

        public short CurfewPreGrace { get; set; }

        public byte SystemType { get; set; }

        public int InChargerCallBackIntervalSecs { get; set; }

        public int NoMotionMinutes { get; set; }

        public int MtdControlBits { get; set; }

        public byte ReportedSystemType { get; set; }

        public byte LaccidStorageControl { get; set; }

        public int DSTStartDay { get; set; }
        public int DSTEndDay { get; set; }

        public String MessageDateFormat { get; set; }

        public String DisplayDateFormat { get; set; }

        public short NoLBSTimer { get; set; }

        public short LbsMode { get; set; }

        public String LbsPrimaryIP { get; set; }

        public String LbsSecondaryIP { get; set; }

        public short LbsDecisionTime { get; set; }

        public short LbsPLGSamplingRate { get; set; }

        public short LbsPLGRequestTimeOut { get; set; }

        public String LbsUnitSimNumber { get; set; }

        public String MtdDialoutDescription_1 { get; set; }

        public String MtdDialoutPhoneNumber_1 { get; set; }

        public String MtdDialoutDescription_2 { get; set; }

        public String MtdDialoutPhoneNumber_2 { get; set; }

        public String MtdDialoutDescription_3 { get; set; }

        public String MtdDialoutPhoneNumber_3 { get; set; }

        public String MtdDialoutDescription_4 { get; set; }

        public String MtdDialoutPhoneNumber_4 { get; set; }

        public String MtdDialoutDescription_5 { get; set; }

        public String MtdDialoutPhoneNumber_5 { get; set; }

        public String MtdDialoutDescription_6 { get; set; }

        public String MtdDialoutPhoneNumber_6 { get; set; }

        public EntProtechEOffenderMtdParams()
        {
            QueryFormat = "select " +
                "BraceletGonePreGrace, " +
                "CurfewPreGrace, " +
                "SystemType," +
                "InChargerCallBackIntervalSecs," +
                "NoMotionMinutes," +
                "MtdControlBits," +
                "ReportedSystemType," +
                "LaccidStorageControl," +
                "DSTStartDay, DSTEndDay," +
                "MessageDateFormat," +
                "DisplayDateFormat," +
                "NoLBSTimer," +
                "LbsMode," +
                "LbsPrimaryIP, LbsSecondaryIP," +
                "LbsDecisionTime," +
                "LbsPLGSamplingRate," +
                "LbsPLGRequestTimeOut," +
                "LbsUnitSimNumber," +
                "MtdDialoutDescription_1,MtdDialoutPhoneNumber_1,"+
                "MtdDialoutDescription_2,MtdDialoutPhoneNumber_2,"+
                "MtdDialoutDescription_3,MtdDialoutPhoneNumber_3,"+
                "MtdDialoutDescription_4,MtdDialoutPhoneNumber_4,"+
                "MtdDialoutDescription_5,MtdDialoutPhoneNumber_5,"+
                "MtdDialoutDescription_6,MtdDialoutPhoneNumber_6 "+
                "from ProtechE.dbo.OffenderMtdParams where DemographicID = {0}";
        }        
    }


    public class EntCurfewPreGrace : IQueryFormat
    {
        public short CurfewPreGrace { get; set; }

        public EntCurfewPreGrace()
        {
            QueryFormat = "select CurfewPreGrace from ProtechE.dbo.OffenderMtdParams where DemographicID = {0}";
        }        
    }

    public class EntSystemType : IQueryFormat
    {
        public byte SystemType { get; set; }
        public EntSystemType()
        {
            QueryFormat = "select SystemType from ProtechE.dbo.OffenderMtdParams where DemographicID = {0}";
        }
    }


    //public class EntNavigationElevationMask : IQueryFormat
    //{
    //    public short NavigationElevationMask { get; set; }
    //    public EntNavigationElevationMask()
    //    {
    //        QueryFormat = "select NavigationElevationMask from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}

    //public class EntNavigationPowerMask : IQueryFormat
    //{
    //    public short NavigationPowerMask { get; set; }
    //    public EntNavigationPowerMask()
    //    {
    //        QueryFormat = "select NavigationPowerMask from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}

    //public class EntSoftwareMotionCheckDuration : IQueryFormat
    //{
    //    public short SoftwareMotionCheckDuration { get; set; }
    //    public EntSoftwareMotionCheckDuration()
    //    {
    //        QueryFormat = "select SoftwareMotionCheckDuration from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}   

    //public class EntSoftwareMotionCheckInterval : IQueryFormat
    //{
    //    public short SoftwareMotionCheckInterval { get; set; }
    //    public EntSoftwareMotionCheckInterval()
    //    {
    //        QueryFormat = "select SoftwareMotionCheckInterval from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}   
    //public class EntMinimumBraceletBatteryVoltage : IQueryFormat
    //{
    //    public short BraceletBatteryMinVoltage { get; set; }
    //    public EntMinimumBraceletBatteryVoltage()
    //    {
    //        QueryFormat = "select BraceletBatteryMinVoltage from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}   
    //public class EntActivationBraceletBatteryVoltage : IQueryFormat
    //{
    //    public short BraceletBatteryMinActVoltage { get; set; }
    //    public EntActivationBraceletBatteryVoltage()
    //    {
    //        QueryFormat = "select BraceletBatteryMinActVoltage from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}   

    //public class EntCallBackIntervalSeconds : IQueryFormat
    //{
    //    public int CallBackIntervalSeconds { get; set; }

    //    public EntCallBackIntervalSeconds()
    //    {
    //        QueryFormat = "select CallBackIntervalSeconds from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}


    //public class EntNormalGPSSaveInterval
    //{
    //    public int NormalGPSSaveInterval { get; set; }
    //    public static string QueryFormat
    //    {
    //        get { return "select NormalGPSSaveInterval from ProtechE.dbo.MTDParameters where MTDID = {0}"; }
    //    }
    //}

    //public class EntAlarmGPSSaveInterval
    //{
    //    public int AlarmGPSSaveInterval { get; set; }
    //    public static string QueryFormat
    //    {
    //        get { return "select AlarmGPSSaveInterval from ProtechE.dbo.MTDParameters where MTDID = {0}"; }
    //    }
    //}

    //public class EntBacklightOnTime
    //{
    //    public byte BacklightOnTime { get; set; }
    //    public static string QueryFormat
    //    {
    //        get { return "select BacklightOnTime from ProtechE.dbo.MTDParameters where MTDID = {0}"; }
    //    }
    //}

    //public class EntPiezoType
    //{
    //    public String PiezoType { get; set; }
    //    public static string QueryFormat
    //    {
    //        get { return "select PiezoType from ProtechE.dbo.MTDParameters where MTDID = {0}"; }
    //    }
    //}

    //public class EntBraceletNoRxGone
    //{
    //    public short BraceletNoRxGone { get; set; }
    //    public static string QueryFormat
    //    {
    //        get { return "select BraceletNoRxGone from ProtechE.dbo.MTDParameters where MTDID = {0}"; }
    //    }
    //}

    //public class EntMinValidBraceletRSSI : IQueryFormat
    //{
    //    public short MinValidBraceletRSSI { get; set; }

    //    public EntMinValidBraceletRSSI()
    //    {
    //        QueryFormat = "select MinValidBraceletRSSI from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}

    //public class EntIgnoreBraceletErrors : IQueryFormat
    //{
    //    public string IgnoreBraceletErrors { get; set; }
    //    public EntIgnoreBraceletErrors()
    //    {
    //        QueryFormat = "select IgnoreBraceletErrors from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}

    //public class EntMotionThreshold : IQueryFormat
    //{
    //    public short MotionThreshold { get; set; }
    //    public EntMotionThreshold()
    //    {
    //        QueryFormat = "select MotionThreshold from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}

    //public class EntChargeKamikaze : IQueryFormat
    //{
    //    public short ChargeKamikaze { get; set; }
    //    public EntChargeKamikaze()
    //    {
    //        QueryFormat = "select ChargeKamikaze from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}

    //public class EntInChargerBumpLeeway : IQueryFormat
    //{
    //    public short InChargerBumpLeeway { get; set; }
    //    public EntInChargerBumpLeeway()
    //    {
    //        QueryFormat = "select InChargerBumpLeeway from ProtechE.dbo.MTDParameters where MTDID = {0}";
    //    }
    //}




    public class EntProtechEMTDParameters : IQueryFormat
    {
        public int CallBackIntervalSeconds { get; set; }

        public short BraceletBatteryMinActVoltage { get; set; }

        public short BraceletBatteryMinVoltage { get; set; }

        public short SoftwareMotionCheckInterval { get; set; }

        public short SoftwareMotionCheckDuration { get; set; }

        public short NavigationPowerMask { get; set; }

        public short NavigationElevationMask { get; set; }

        public short InChargerBumpLeeway { get; set; }

        public short ChargeKamikaze { get; set; }

        public short MotionThreshold { get; set; }

        public string IgnoreBraceletErrors { get; set; }

        public short MinValidBraceletRSSI { get; set; }

        public short BraceletNoRxGone { get; set; }

        public String PiezoType { get; set; }

        public byte BacklightOnTime { get; set; }

        public int AlarmGPSSaveInterval { get; set; }

        public int NormalGPSSaveInterval { get; set; }

        public String PrimarySDCIPAddress { get; set; }

        public String SecondarySDCIPAddress { get; set; }

        public int FactoryMenuPassword { get; set; }

        public int OfficerMenuPassword { get; set; }

        public int MAXInChargerTHPE { get; set; }

        public String GPRSAPN { get; set; }
        public String PrimarySDCPhoneNumber { get; set; }

        public int TamperThreshold { get; set; }

        public String NetworkUserId { get; set; }
        public String NetworkPassword { get; set; }

        public String GPRSAPN1 { get; set; }

        public byte PrimaryNetworkID { get; set; }

        public int DevicePrimaryNetworkToggleMins { get; set; }


        public EntProtechEMTDParameters()
        {
            QueryFormat = "select " +
                "CallBackIntervalSeconds," +
                "BraceletBatteryMinActVoltage," +
                "BraceletBatteryMinVoltage,"+
                "SoftwareMotionCheckInterval," +
                "SoftwareMotionCheckDuration," +
                "NavigationPowerMask," +
                "NavigationElevationMask," +
                "InChargerBumpLeeway," +
                "ChargeKamikaze," +
                "MotionThreshold," +
                "IgnoreBraceletErrors," +
                "MinValidBraceletRSSI," +
                "BraceletNoRxGone," +
                "PiezoType," +
                "BacklightOnTime," +
                "AlarmGPSSaveInterval," +
                "NormalGPSSaveInterval," +
                "PrimarySDCIPAddress," +
                "SecondarySDCIPAddress," +
                "FactoryMenuPassword," +
                "OfficerMenuPassword," +
                "MAXInChargerTHPE," +
                "GPRSAPN," +
                "PrimarySDCPhoneNumber," +
                "TamperThreshold," +
                "NetworkPassword," +
                "NetworkUserId," +
                "GPRSAPN1," +
                "PrimaryNetworkID," +
                "DevicePrimaryNetworkToggleMins " +
                "from ProtechE.dbo.MTDParameters where MTDID = {0}";
        }
    }


    public class InsertMtdEventAction : IQueryFormat
    {
        public InsertMtdEventAction()
        {
            QueryFormat = "INSERT INTO MTDActions (DeviceID,Action,DemographicID,CommandNumber,CommandParam,SoftwareType) " +
                "Values({0},{1},{2},{3},{4},{5})";
            //"Values(@DeviceID,@Action,@DemograhpicID,@CommandNumber,@CommandParam,@SoftwareType)";
        }

        //public long DeviceID { get; set; }

        //public string Action { get; set; }

        //public long DemograhpicID { get; set; }

        //public int CommandNumber { get; set; }

        //public string CommandParam { get; set; }

        //public int SoftwareType { get; set; }

    }


    public abstract class IStoredProcedureFormat
    {
        [OdbcColumnIgnore(true)]
        public string StoredProcedure { get; set; }
    }

    public class sndcds_MtdAct_InsMTDAct_001_SP : IStoredProcedureFormat
    {
        public sndcds_MtdAct_InsMTDAct_001_SP()
        {
            StoredProcedure = "sndcds_MtdAct_InsMTDAct_001";
        }
        [OdbcSpParam(System.Data.ParameterDirection.Input, System.Data.Odbc.OdbcType.Int)]
        public long DeviceID { get; set; }

        [OdbcSpParam(System.Data.ParameterDirection.Input, System.Data.Odbc.OdbcType.VarChar, 20)]
        public string Action { get; set; }

        [OdbcSpParam(System.Data.ParameterDirection.Input, System.Data.Odbc.OdbcType.Int)]
        public long DemograhpicID { get; set; }

        [OdbcSpParam(System.Data.ParameterDirection.Input, System.Data.Odbc.OdbcType.Int)]
        public int CommandNumber { get; set; }

        [OdbcSpParam(System.Data.ParameterDirection.Input, System.Data.Odbc.OdbcType.VarChar, 160)]
        public string CommandParam { get; set; }

        [OdbcSpParam(System.Data.ParameterDirection.Input, System.Data.Odbc.OdbcType.TinyInt)]
        public int SoftwareType { get; set; }

        [OdbcSpParam(System.Data.ParameterDirection.Output, System.Data.Odbc.OdbcType.Int)]
        public int MTDActionID { get; set; }


        // action = MTD_APP
        // iCommandNumber = 14
        // sCommandParam = 5_12_10_33
        // sw type = 2
    }
}
