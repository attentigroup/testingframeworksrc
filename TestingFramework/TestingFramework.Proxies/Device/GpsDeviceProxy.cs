﻿using CommBox.Infrastructure;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Threading;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.Proxies.Device.CommandsHandler.Exceptions;

namespace TestingFramework.Proxies.Device
{
    public class GpsDeviceProxy : IExecuteCommands
    {
        #region singleton
        private static GpsDeviceProxy gpsDeviceProxy;

        public static GpsDeviceProxy Instance
        {
            get
            {
                if (gpsDeviceProxy == null)
                {
                    gpsDeviceProxy = new GpsDeviceProxy();
                }

                return gpsDeviceProxy;
            }
        }

        public GpsDeviceProxy()
        {
            Init();
        }

        #endregion singleton

        #region Properties


        public static string GpsDeviceServerIPToken = "GpsDeviceServerIP";
        public static string GpsDeviceServerPortToken = "GpsDeviceServerPort";

        private string _gpsDeviceServerIP = null;

        public string GpsDeviceServerIP
        {
            get
            {
                if( string.IsNullOrEmpty(_gpsDeviceServerIP))
                {//read from config
                    _gpsDeviceServerIP = ConfigurationManager.AppSettings[GpsDeviceServerIPToken];
                }
                return _gpsDeviceServerIP;
            }
            set { _gpsDeviceServerIP = value; }
        }


        private int _gpsDeviceServerPort = 0;

        public int GpsDeviceServerPort
        {
            get
            {
                if (_gpsDeviceServerPort == 0)
                {//read from config
                    int.TryParse(ConfigurationManager.AppSettings[GpsDeviceServerPortToken], out _gpsDeviceServerPort);
                }
                return _gpsDeviceServerPort;
            }
            set { _gpsDeviceServerPort = value; }
        }


        public DeviceNetwork DeviceNetworkObject { get; set; }

        public const int DEFAULT_POINTS_COUNT = 2;

        public List<EnumRules> HwRules2Send { get; set; }
        /// <summary>
        /// log object
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion Properties

        #region Methods


        private void Init()
        {
            CancellationToken ct = new CancellationToken();
            CommBox.Infrastructure.Settings sns = new CommBox.Infrastructure.Settings();
            DeviceNetworkObject = new DeviceNetwork(ct, sns);
        }

        public void MakeConversation(int deviceId, uint offenderId, Encoding encoding, string deviceApplication = "V5.12.9.2", 
            int pointsCount = DEFAULT_POINTS_COUNT, bool sendHwEvents = false, bool AGPSSupport = false, uint AGPSFileAgeMinutes = 0)
        {
            int                 braceletID = 42;
            EnumEncryptionType  enumEncryptionType = EnumEncryptionType.TypeOne;

            if(HwRules2Send == null || HwRules2Send.Count == 0)
            {
                HwRules2Send = new List<EnumRules>() { EnumRules.MotionNoGPS };
            }

            AddCommand(new CommandRequestAllIDs() { DeviceId = deviceId });
            AddCommand(new CommandCurrentSettingsBaseSetOffenderDemographicID() { OffenderId = offenderId });
            AddCommand(new CommandRequestSWVersion() { DeviceApplication = deviceApplication });
            AddCommand(new CommandRequestID() { DeviceId = deviceId });
            AddCommand(new CommandCurrentSettingsSetBraceletSerialNumber() { BraceletID = braceletID });
            AddCommand(new CommandEncryptionMethodSupported() { EnumEncryptionType = enumEncryptionType });
            AddCommand(new CommandRequestHWRevandBatteryCapacity());
            AddCommand(new CommandCurrentSettingsCommandLanguageSet());
            AddCommand(new CommandCurrentSettingsCommandSupportedCharset());
            AddCommand(new CommandCommunicationMode());
            AddCommand(new CommandConfigCRC());
            AddCommand(new CommandRequestRulesCRC());
            AddCommand(new CommandRequestOneTimeScheduleCRC());
            AddCommand(new CommandRequestCurrentSettingsSetTime());
            AddCommand(new CommandRequestCodeCRC());
            AddCommand(new CommandSetOffenderDemographicID());
            AddCommand(new CommandEnableFeatures());
            AddCommand(new CommandUserName() { Encoding = encoding });
            AddCommand(new CommandSetUTCOffsets());
            AddCommand(new CommandNormalGPSSaveInterval());
            AddCommand(new CommandGPSAlarmSaveInterval());
            AddCommand(new CommandBacklightOnTime());
            AddCommand(new CommandPiezoType());
            AddCommand(new CommandSetBraceletSerialNumber());
            AddCommand(new CommandBraceletGoneTime());
            AddCommand(new CommandBraceletRange());
            AddCommand(new CommandIgnoreBraceletErrs());
            AddCommand(new CommandMotionThreshold());
            AddCommand(new CommandContactMethod());
            AddCommand(new Command3VchgLow());
            AddCommand(new Command3VchgKamikaze());
            AddCommand(new CommandInChargerBumpLeeway());
            AddCommand(new CommandBraceletGonePreGrace());
            AddCommand(new CommandCurfewPreGrace());
            AddCommand(new CommandNAVElevationMask());
            AddCommand(new CommandNAVPowerMask());
            AddCommand(new CommandSoftwareMotionCheckDuration());
            AddCommand(new CommandSoftwareMotionCheckInterval());
            AddCommand(new CommandMinimumBraceletBatteryVoltage());
            AddCommand(new CommandActivationBraceletBatteryVoltage());
            AddCommand(new CommandSystemType());
            AddCommand(new CommandCallBackInterval());
            AddCommand(new CommandPrimaryGPRSIPAddress());
            AddCommand(new CommandSecondaryGPRSIPAddress());
            AddCommand(new CommandNoMotionMinutes());
            AddCommand(new CommandFactoryMenuPassword());
            AddCommand(new CommandOfficerMenuPassword());
            AddCommand(new CommandBitOptions());
            AddCommand(new CommandMaxInChargerTHPE());
            AddCommand(new CommandReportedSystemType());
            AddCommand(new CommandLACCIDStorageControl());
            AddCommand(new CommandDaylightSavingTime());
            AddCommand(new CommandMonthDayDateFormat());
            AddCommand(new CommandMonthDayYearFormat());
            AddCommand(new CommandMonitorStartEndDateTime());
            AddCommand(new CommandBraceletType());
            AddCommand(new CommandGSMData1APN());
            AddCommand(new CommandGSMData1APNUser());
            AddCommand(new CommandGSMData1APNPassword());
            AddCommand(new CommandGSMDataPrimaryAPN());            
            AddCommand(new CommandGSMData0APN());
            AddCommand(new CommandGSMData0User());
            AddCommand(new CommandGSMData0Password());
            AddCommand(new CommandPriPhoneNumber());
            AddCommand(new CommandTamperThreshold());
            AddCommand(new CommandOperationMode());
            AddCommand(new CommandLBSMode());
            AddCommand(new CommandNoLBSTimer());
            AddCommand(new CommandPrimaryPLGIPAddress());
            AddCommand(new CommandSecondayPLGIPAddress());
            AddCommand(new CommandLBSDecisionTime());
            AddCommand(new CommandPLGSamplingRate());
            AddCommand(new CommandPLGRequestTimeout());
            AddCommand(new CommandUnitSIMNumber());
            AddCommand(new CommandPhoneNumbers() { Encoding = encoding });
            AddCommand(new CommandCurrentSettingsRequestSTimersandCRCs() { AGPSSupport = AGPSSupport, DeviceAGPSFileAgeOffsetMinutes = AGPSFileAgeMinutes });
            AddCommand(new CommandSetTime());
            AddCommand(new CommandSendAllPoints() { PointsCount = pointsCount , HwRules2Send = this.HwRules2Send });
            AddCommand(new CommandRule() { Encoding = encoding });
            AddCommand(new CommandACK());
            AddCommand(new CommandStopSendingPoints());
            AddCommand(new CommandHangup());
            AddCommand(new CommandGSMDataToggleTime());
            AddCommand(new CommandRequestCurrentSettingsBulkLoadCRCandAddress());
            AddCommand(new CommandDisplayMessage() { Encoding = encoding });

            //Partial Code Load - AGPS
            AddCommand(new CommandPartialCodeLoadInit());
            AddCommand(new CommandPartialCodeLoadCode());
            AddCommand(new CommandPartialCodeLoadCrc());
            AddCommand(new CommandPartialCodeLoadLoad());
            //Partial Code Load - SW Download
            AddCommand(new CommandPartialCodeLoadInit() { SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.MTDAppInit });
            AddCommand(new CommandPartialCodeLoadCode() { SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.MTDAppCode });
            AddCommand(new CommandPartialCodeLoadCrc() { SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.MTDAppCrc });
            AddCommand(new CommandPartialCodeLoadLoad() { SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.MTDAppLoad });

            DeviceNetworkObject.ConnectClient(GpsDeviceServerIP, GpsDeviceServerPort, deviceId, offenderId, this);

        }


        private Dictionary<string, ICommand> _dicCommands = new Dictionary<string, ICommand>();

        public Dictionary<string, ICommand> Commands
        {
            get { return _dicCommands; }
        }
        
        public void AddCommand(ICommand command)
        {
            string commandKey = command.CommandKey;
            if( _dicCommands.ContainsKey(commandKey) == true)
            {
                throw new CommandAlreadyExistException(command);
            }
            command.ExecuteCommandsObject = this;

            _dicCommands.Add(commandKey, command);
        }

        public byte[] ExecuteCommand(EnumProtocolCommands command, UInt16 Size, EnumProtocolCommands subCommand, byte[] input, ref int pos)
        {
            string commandKey = CommandBase.GenerateKeyFromCommands(command, subCommand);
            if (_dicCommands.ContainsKey(commandKey) == false)
            {
                throw new CommandDoesNotExistException(command, subCommand);
            }

            ICommand cmd = _dicCommands[commandKey];

            byte[] result = null;
            try
            {
                result = cmd.Execute(input, Size, ref pos);
                cmd.CommandExecuted = EnumCommandExecuted.CommandExecutedPass;
                Log.Debug($"Command {commandKey} pass. set method executed status to Pass");
            }
            catch (Exception exception)
            {
                Log.Debug($"Command {commandKey} fail with error message: {exception.Message}. set method executed status to Fail");
                cmd.CommandExecuted = EnumCommandExecuted.CommandExecutedFail;
                throw;
            }
            return result;
        }

        public ICommand GetCommand(EnumProtocolCommands command, EnumProtocolCommands subCommand)
        {
            string commandKey = CommandBase.GenerateKeyFromCommands(command, subCommand);
            if (_dicCommands.ContainsKey(commandKey) == false)
            {
                throw new CommandDoesNotExistException(command, subCommand);
            }

            ICommand cmd = _dicCommands[commandKey];
            return cmd;

        }

        public void PacketReset()
        {
            foreach (var command in _dicCommands.Values)
            {
                try
                {
                    command.PacketReset();
                }
                catch (Exception exception)
                {
                    Log.ErrorFormat("command {0} reset packet method throw exception {1}", command.CommandKey, exception.Message);
                }
            }
        }
        #endregion Methods
    }
}
