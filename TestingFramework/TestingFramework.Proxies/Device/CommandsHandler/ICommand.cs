﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler
{
    /// <summary>
    /// ICommand interface for implementation command pattern for the communication iwth the CommBox.
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// executer command interface to implement connection between commands in case needed.
        /// </summary>
        IExecuteCommands ExecuteCommandsObject { get; set; }
        /// <summary>
        /// the command from the protocol.
        /// </summary>
        EnumProtocolCommands    Command { get; set; }
        /// <summary>
        /// sub command in case the protocol asks.
        /// In case the protocol not need sub-command it must be set to CommandBase.EMPTY_SUB_COMMAND
        /// </summary>
        EnumProtocolCommands SubCommand { get; set; }
        /// <summary>
        /// the key of the command
        /// </summary>
        string CommandKey { get; }
        /// <summary>
        /// method will be implement by commands in order to excecute the handling of the command
        /// </summary>
        /// <param name="input">the input buffer</param>
        /// <param name="Size">size of the command</param>
        /// <param name="pos">the position of the command on the overall buffer</param>
        /// <returns>byte[] of the answer.In case no answer an empty buffer should returen.</returns>
        byte[] Execute(byte[] input, UInt16 Size, ref int pos);
        /// <summary>
        /// indication for the command that the handling of the packet was finish 
        /// and if the command need to do some reset, this is the time to do it.
        /// </summary>
        void PacketReset();
        /// <summary>
        /// indication of the command if it was executed
        /// </summary>
        EnumCommandExecuted CommandExecuted { get; set; }
    }

    public enum EnumCommandExecuted
    {
        /// <summary>
        /// indicate that the command didn't wexecuted
        /// </summary>
        CommandExecutedNotExecuted,
        /// <summary>
        /// indicate that the command executed and pass
        /// </summary>
        CommandExecutedPass,
        /// <summary>
        /// indicate that the command executed and fail
        /// </summary>
        CommandExecutedFail,
    }
}
