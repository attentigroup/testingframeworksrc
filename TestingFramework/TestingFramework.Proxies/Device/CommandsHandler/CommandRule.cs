﻿
using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace TestingFramework.Proxies.Device.CommandsHandler
{
    public interface IRule
    {
        void ReadBuffer(EnumProductFamily productType, byte[] input, ushort Size, ref int pos);
    }

    public abstract class RuleBase : IRule
    {
        public int RuleID { get; set; }

        public UInt32 Actions { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public int GracePeriod { get; set; }

        public byte[] TimeGrid { get; set; }

        public string ViolationMessage { get; set; }


        public RuleBase(){}

        public RuleBase(RuleBase rb)
        {
            RuleID = rb.RuleID;
            Actions = rb.Actions;
            StartDateTime = rb.StartDateTime;
            EndDateTime = rb.EndDateTime;
            GracePeriod = rb.GracePeriod;
            TimeGrid = new byte[rb.TimeGrid.Length];
            Buffer.BlockCopy(rb.TimeGrid, 0, TimeGrid, 0, rb.TimeGrid.Length);
            ViolationMessage = rb.ViolationMessage;
        }

        virtual public void ReadBuffer(EnumProductFamily productType, byte[] input, ushort Size, ref int pos) { }
    }
        
    public class HwRule : RuleBase
    {
        public int HWRuleNumber { get; set; }

        public Encoding Encoding { get; set; }

        /// <summary>
        /// the rule status to send
        /// </summary>
        public UInt16 RuleStatus { get; set; }

        public static int HardwareRuleTimeGridSize = 42;//CommBox\Software\CommBox.Data\APIExtensions\EntDeviceRules.cs line 193

        public HwRule(){}

        public HwRule(HwRule hwRule) : base(hwRule)
        {
            HWRuleNumber = hwRule.HWRuleNumber;
            Encoding = hwRule.Encoding;
        }

        override public void ReadBuffer(EnumProductFamily productType, byte[] input, ushort Size, ref int pos)
        {
            HWRuleNumber = PacketBuilder.GetByte(input, ref pos);
            RuleID = (int)PacketBuilder.GetUInt32(input, ref pos);
            Actions = PacketBuilder.GetUInt32(input, ref pos);
            var startDateTime = PacketBuilder.GetInt32(input, ref pos);
            StartDateTime = Utilities.GetDeviceLocalTime(startDateTime);
            var endDateTime = PacketBuilder.GetInt32(input, ref pos);
            EndDateTime = Utilities.GetDeviceLocalTime(startDateTime);
            GracePeriod = PacketBuilder.GetInt32(input, ref pos);
            TimeGrid = PacketBuilder.GetBuffer(input, HardwareRuleTimeGridSize, ref pos);

            if(Size >= pos)//it might be without violation message.
            {
                var violationMessageBuferSize = Size - pos;
                var tempViolationMessageBuffer = PacketBuilder.GetBuffer(input, violationMessageBuferSize, ref pos);
                ViolationMessage = Encoding.Unicode.GetString(tempViolationMessageBuffer);
            }            
        }
    }
    
    public class CircleRule : RuleBase
    {
        public Double LatitudeToSend { get; set; }
        public Double LongitudeToSend { get; set; }

        public Double BufferDistanceToSend { get; set; }

        public int BufferGrace { get; set; }

        public static int CircleRuleTimeGridSize = 42;

        public override void ReadBuffer(EnumProductFamily productType, byte[] input, ushort Size, ref int pos)
        {
            base.ReadBuffer(productType, input, Size, ref pos);
            RuleID = (int)PacketBuilder.GetUInt32(input, ref pos);
            if(productType == EnumProductFamily.OnePiece)
            {
                int PreviousRuleID = (int)PacketBuilder.GetUInt32(input, ref pos);
            }

            LongitudeToSend = PacketBuilder.GetUInt32(input, ref pos);
            LatitudeToSend = PacketBuilder.GetUInt32(input, ref pos);

            Actions = PacketBuilder.GetUInt32(input, ref pos);

            var startDateTime = PacketBuilder.GetInt32(input, ref pos);
            StartDateTime = Utilities.GetDeviceLocalTime(startDateTime);
            var endDateTime = PacketBuilder.GetInt32(input, ref pos);
            EndDateTime = Utilities.GetDeviceLocalTime(startDateTime);

            GracePeriod = PacketBuilder.GetInt32(input, ref pos);

            BufferDistanceToSend = PacketBuilder.GetInt32(input, ref pos);

            BufferGrace = PacketBuilder.GetInt32(input, ref pos);

            TimeGrid = PacketBuilder.GetBuffer(input, CircleRuleTimeGridSize, ref pos);

            if (Size >= pos)//it might be without violation message.
            {
                var violationMessageBuferSize = Size - pos;
                var tempViolationMessageBuffer = PacketBuilder.GetBuffer(input, violationMessageBuferSize, ref pos);
                ViolationMessage = Encoding.Unicode.GetString(tempViolationMessageBuffer);
            }

        }


    }

    public class CurfewRule : RuleBase
    {
        public Double LatitudeToSend { get; set; }
        public Double LongitudeToSend { get; set; }

        public Double DistanceToSend { get; set; }

        public int CurfewDeviceID { get; set; }

        public static int CurfewRuleTimeGridSize = 42;

        public override void ReadBuffer(EnumProductFamily productType, byte[] input, ushort Size, ref int pos)
        {
            base.ReadBuffer(productType, input, Size, ref pos);
            RuleID = (int)PacketBuilder.GetUInt32(input, ref pos);
            if (productType == EnumProductFamily.OnePiece)
            {
                int PreviousRuleID = (int)PacketBuilder.GetUInt32(input, ref pos);
            }

            LongitudeToSend = PacketBuilder.GetUInt32(input, ref pos);
            LatitudeToSend = PacketBuilder.GetUInt32(input, ref pos);

            DistanceToSend = PacketBuilder.GetUInt32(input, ref pos);

            Actions = PacketBuilder.GetUInt32(input, ref pos);

            var startDateTime = PacketBuilder.GetInt32(input, ref pos);
            StartDateTime = Utilities.GetDeviceLocalTime(startDateTime);
            var endDateTime = PacketBuilder.GetInt32(input, ref pos);
            EndDateTime = Utilities.GetDeviceLocalTime(startDateTime);

            GracePeriod = PacketBuilder.GetInt32(input, ref pos);

            CurfewDeviceID = PacketBuilder.GetInt32(input, ref pos);

            TimeGrid = PacketBuilder.GetBuffer(input, CurfewRuleTimeGridSize, ref pos);

            if (Size >= pos)//it might be without violation message.
            {
                var violationMessageBuferSize = Size - pos;
                var tempViolationMessageBuffer = PacketBuilder.GetBuffer(input, violationMessageBuferSize, ref pos);
                ViolationMessage = Encoding.Unicode.GetString(tempViolationMessageBuffer);
            }
        }
    }

    public class OneTimeScheduleData : RuleBase
    {
        public int OneTimeSchedID { get; set; }

        public void ReadBuffer(EnumProductFamily productType, byte[] input, ushort Size, ref int pos)
        {
            OneTimeSchedID = PacketBuilder.GetInt32(input, ref pos);
            RuleID = PacketBuilder.GetInt32(input, ref pos);
            var startDate = PacketBuilder.GetInt32(input, ref pos);
            StartDateTime = Utilities.GetDeviceLocalTime(startDate);
            var timeGridSize = Size - pos;
            TimeGrid = PacketBuilder.GetBuffer(input, timeGridSize, ref pos);
        }
    }

    public class CommandRule : CommandBase
    {
        public Encoding Encoding { get; set; }

        public EnumProductFamily ProductType { get; set; }

        private bool _ackAlreadySent = false;
        public bool AckAlreadySent
        {
            get         { return _ackAlreadySent; }
            private set { _ackAlreadySent = value; }
        }

        public bool DeleteAllOneTimeSchedulesReceived { get; set; }

        public bool DeleteAllRulesReceived { get; set; }

        public List<HwRule> HwRules { get; set; }

        public List<CircleRule> CircleRules { get; set; }

        public List<CurfewRule> CurfewRules { get; set; }

        public List<int> DeletedOneTimeSchedIDs { get; set; }

        public List<int> DeletedRuleIDs { get; set; }
        
        public List<OneTimeScheduleData> AddedOneTimeSchedules { get; set; }

        public CommandRule() : base()
        {
            Command = EnumProtocolCommands.CommandRule;
            Encoding = Encoding.ASCII;
            ProductType = EnumProductFamily.TwoPiece;

            DeleteAllOneTimeSchedulesReceived = false;
            DeleteAllRulesReceived = false;

            HwRules = new List<HwRule>();
            CircleRules = new List<CircleRule>();
            CurfewRules = new List<CurfewRule>();
            DeletedOneTimeSchedIDs = new List<int>();
            DeletedRuleIDs = new List<int>();
            AddedOneTimeSchedules = new List<OneTimeScheduleData>();
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            int                 timeGridSize = 0;
            EnumRuleSubCommand  rsc = (EnumRuleSubCommand)PacketBuilder.GetByte(input, ref pos);
            Log.DebugFormat("{0} sub command {1} was recived", Command, rsc);

            switch (rsc)
            {
                case EnumRuleSubCommand.AddSquareGeoRule://no reference at commBox
                    break;
                case EnumRuleSubCommand.DeleteRule:
                    {
                        int ruleId = PacketBuilder.GetInt32(input, ref pos);
                        Log.DebugFormat("DeleteRule - RuleId {0}.", ruleId);
                        DeletedRuleIDs.Add(ruleId);
                    }
                    break;
                case EnumRuleSubCommand.AddCircleRule:
                    {
                        CircleRule circleRule = new CircleRule();
                        circleRule.ReadBuffer(ProductType, input, Size, ref pos);
                        CircleRules.Add(circleRule);

                    }
                    break;
                case EnumRuleSubCommand.AddCurfewRule:
                    {
                        CurfewRule curfewRule = new CurfewRule();
                        curfewRule.ReadBuffer(ProductType, input, Size, ref pos);
                        CurfewRules.Add(curfewRule);
                    }
                    break;
                case EnumRuleSubCommand.AddPolygonRule:
                    timeGridSize = 42;
                    break;
                case EnumRuleSubCommand.AddOneTimeSchedule:
                    {
                        var ots = new OneTimeScheduleData();
                        ots.ReadBuffer(ProductType, input, Size, ref pos);
                        AddedOneTimeSchedules.Add(ots);
                    }
                    break;
                case EnumRuleSubCommand.DeleteOneTimeSchedule:
                    {
                        int oneTimeSchedID = PacketBuilder.GetInt32(input, ref pos);
                        Log.DebugFormat("DeleteOneTimeSchedule - OneTimeSchedID {0}.", oneTimeSchedID);
                        DeletedOneTimeSchedIDs.Add(oneTimeSchedID);
                    }
                    break;
                case EnumRuleSubCommand.DeleteAllOneTimeSchedules:
                    DeleteAllOneTimeSchedulesReceived = true;
                    break;
                case EnumRuleSubCommand.AddMTDHardwareRule:
                    {
                        HwRule hr = new HwRule() { Encoding = Encoding };
                        hr.ReadBuffer(ProductType, input, Size, ref pos);
                        HwRules.Add(hr);
                    }
                    break;
                case EnumRuleSubCommand.DeleteAllRules:
                    DeleteAllRulesReceived = true;
                    break;
                default:
                    break;
            }
            byte[] answer = null;
            if( !AckAlreadySent)
            {
                answer = new byte[] { 0x10, 0x00, 0x00 };//ACK
                AckAlreadySent = true;
                Log.DebugFormat("{0} add ack to the buffer", Command, rsc);
            }            
            return answer;
        }
        
        public override void PacketReset()
        {
            base.PacketReset();
            AckAlreadySent = false;
        }
    }
}
