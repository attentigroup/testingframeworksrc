﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandEnableFeatures : CommandBase
    {
        public Int32 FeaturesMap { get; set; }
        public CommandEnableFeatures() : base()
        {
            Command = EnumProtocolCommands.CommandEnableFeatures;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            FeaturesMap = PacketBuilder.GetInt32(input, ref pos);

            byte[] answer = null;
            return answer;
        }
    }
}
