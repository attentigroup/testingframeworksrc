﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandBraceletGonePreGrace : CommandBase
    {
        public short BraceletGonePreGrace { get; set; }
        public CommandBraceletGonePreGrace() : base()
        {
            Command = EnumProtocolCommands.CommandBraceletGonePreGrace;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            BraceletGonePreGrace = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
