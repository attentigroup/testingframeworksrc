﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class ChunkData
    {
        public byte[] ChunksBytes { get; set; }

        public int ChunkAddress { get; set; }

    }
    public class CommandPartialCodeLoadCode : CommandPartialCodeLoadBase
    {
        public List<ChunkData> ChunksList { get; set; }
        public CommandPartialCodeLoadCode() : base()
        {
            SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisCode;
            ChunksList = new List<ChunkData>();
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            Int32 packetAddress = PacketBuilder.GetInt32(input, ref pos);
            int chunkSize = Size - Marshal.SizeOf(packetAddress)-1;//packet ddress

            var chunk = PacketBuilder.GetBuffer(input, chunkSize, ref pos);

            var chunkData = new ChunkData() { ChunksBytes = chunk, ChunkAddress = packetAddress };
            Log.DebugFormat("{0}, chunk received. size = {1} address {2}",
                (EnumCodeLoadCommands)SubCommand, chunkSize, packetAddress);
            ChunksList.Add(chunkData);

            var answer = new byte[] { 0x10, 0x00, 0x00 };//ACK
            return answer;
        }
    }
}
