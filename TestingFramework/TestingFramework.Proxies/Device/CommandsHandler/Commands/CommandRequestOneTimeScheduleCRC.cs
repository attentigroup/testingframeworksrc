﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestOneTimeScheduleCRC : CommandRequestCurrentSettingsBase
    {
        public CommandRequestOneTimeScheduleCRC() : base()
        {
            SubCommand = EnumProtocolCommands.CommandRequestOneTimeScheduleCRC;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            var answer = Utilities.Combine(
                 new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                 BitConverter.GetBytes((ushort)5),//Size
                 new byte[] { (byte)EnumProtocolCommands.CommandRequestOneTimeScheduleCRC },
                 BitConverter.GetBytes(0));//??Config CRC
            return answer;
        }
    }
}
