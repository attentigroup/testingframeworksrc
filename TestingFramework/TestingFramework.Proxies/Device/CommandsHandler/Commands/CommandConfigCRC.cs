﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandConfigCRC : CommandRequestCurrentSettingsBase
    {
        private bool _alreadySent = false;
        public bool AlreadySent
        {
            get { return _alreadySent; }
            private set { _alreadySent = value; }
        }

        public CommandConfigCRC()
        {
            SubCommand = EnumProtocolCommands.CommandConfigCRC;
        }
        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            byte[] answer = null;

            if (!AlreadySent)
            {
                answer = Utilities.Combine(
                         new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                         BitConverter.GetBytes((ushort)5),//Size
                         new byte[] { (byte)EnumProtocolCommands.CommandConfigCRC },
                         BitConverter.GetBytes(0));//??Config CRC 
                AlreadySent = true;
            }
            return answer;
        }
    }
}
