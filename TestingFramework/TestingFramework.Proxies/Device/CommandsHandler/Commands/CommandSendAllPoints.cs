﻿using CommBox.Infrastructure;
using Common.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using TestingFramework.TestsInfraStructure.Factories;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    /// <summary>
    /// implementation of send all points command
    /// </summary>
    public class CommandSendAllPoints : CommandBase
    {
        #region Properties

        private static byte[] _ack = null;
        /// <summary>
        /// ACK message buffer
        /// </summary>
        public byte[] ACK
        {
            get
            {
                if (_ack == null)
                {
                    _ack = Utilities.Combine(
                        new byte[] { (byte)EnumProtocolCommands.CommandACK },
                        BitConverter.GetBytes((short)0));
                }
                return _ack;
            }
            private set { }
        }

        private static byte[] _commandCaughtUp = null;
        /// <summary>
        /// Caughtup message buffer
        /// </summary>
        public byte[] CommandCaughtUp
        {
            get
            {
                if (_commandCaughtUp == null)
                {
                    _commandCaughtUp = Utilities.Combine(
                        new[] { (byte)EnumProtocolCommands.CommandCaughtUp },
                        BitConverter.GetBytes((short)0));
                }
                return _commandCaughtUp;
            }
            private set { }
        }

        /// <summary>
        /// the amount of points need to send
        /// </summary>
        public int PointsCount { get; set; }

        /// <summary>
        /// indicate if the gps device proxy need to send HW events
        /// </summary>
        public bool SendHWEvents { get; set; }

        public List<EnumRules> HwRules2Send { get; set; }

        public static int SINGLE_POINT_MESSAGE_SIZE = 170;

        public static Coordinate UpperLeft = new Coordinate() { Longitude = 32.106927, Latitude = 34.833193 };
        public static Coordinate UpperRight = new Coordinate() { Longitude = 32.110462, Latitude = 34.837849 };
        public static Coordinate LowerLeft = new Coordinate() { Longitude = 32.105864, Latitude = 34.834030 };
        public static Coordinate LowerRight = new Coordinate() { Longitude = 32.108872, Latitude = 34.839630 };
        
        public List<Point> PointsSent { get; set; }
        #endregion Properties
        
        public CommandSendAllPoints()
        {
            Command = EnumProtocolCommands.CommandSendAllPoints;
            SubCommand = EMPTY_SUB_COMMAND;

            PointsCount = 2;
        }
        
        /// <summary>
        /// implementation of execute method from the interface
        /// </summary>
        /// <param name="input"></param>
        /// <param name="Size"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public override byte[] Execute(byte[] input, System.UInt16 Size, ref int pos)
        {
            var pointsBuffer = GeneratePointsBuffer();
            var answer = Utilities.Combine(pointsBuffer, CommandCaughtUp);
            return answer;
        }
        /// <summary>
        /// this method will generate buffer ready to send to the communication server.
        /// </summary>
        /// <returns></returns>
        private byte[] GeneratePointsBuffer()
        {
            PointsSent = GenerateRandomPoints();
            //calc the size of the message
            int     maxMessageSize = SINGLE_POINT_MESSAGE_SIZE * PointsSent.Count;

            byte[]  bufferTemp = new byte[maxMessageSize];
            int     position = 0;
            
            foreach (var point in PointsSent)
            {
                PacketBuilder.AddData(ref bufferTemp, ref position, point.GetBuffer()); 
            }
            //copy just the buffer with the data.
            byte[] result = new byte[position];
            Buffer.BlockCopy(bufferTemp, 0,result, 0, position);
            return result;
        }

        /// <summary>
        /// get points inside area as define in the coordinates
        /// update for each point the time
        /// </summary>
        /// <returns>list of points within specific area and time updates with sepcific offset between points.</returns>
        private List<Point> GenerateRandomPoints()
        {
            var             points = new List<Point>();
            CommandRule     cr = ExecuteCommandsObject.GetCommand(EnumProtocolCommands.CommandRule, EMPTY_SUB_COMMAND) as CommandRule;
            var             hwRules4Call = GenerateHwRules(cr.HwRules, HwRules2Send);
            int             pointsCount = hwRules4Call.Count;//every rule will have point
            Coordinate[]    pointsCoordinates = Calculate(pointsCount, UpperLeft, UpperRight, LowerLeft, LowerRight);

            uint startTime = TimeUtility.GetEpoch();
            for (int i = 0,secondsOffset = 0; i < pointsCount; i++, secondsOffset += (i*10))
            {
                var point = new Point();
                point.PointCoordinate.Longitude = pointsCoordinates[i].Longitude;
                point.PointCoordinate.Latitude = pointsCoordinates[i].Latitude;
                point.CurrentTimeEPoc = startTime + (uint)secondsOffset;
                point.HwRules.Add(hwRules4Call[i]);
                points.Add(point);
            }
            return points;
        }

        /// <summary>
        /// generate list of hw rules and status out of the option supplied
        /// </summary>
        /// <param name="hwRulesOrigin"></param>
        /// <param name="hwRules2Send"></param>
        /// <returns></returns>
        private List<HwRule> GenerateHwRules(List<HwRule> hwRulesOrigin, List<EnumRules> hwRules2Send)
        {
            Int32 ruleStatus = 0;
            var result = new List<HwRule>();


            foreach (var hwRule2Send in hwRules2Send)
            {
                HwRule theSelectedRule = null;

                //find specific
                theSelectedRule = hwRulesOrigin.Find(x => x.HWRuleNumber == (int)hwRule2Send);
                if (theSelectedRule == null)//didn't find - continue with random
                {
                    var rulesIndex = DataGeneratorFactory.Instance.RNG.Next(0, hwRulesOrigin.Count);
                    theSelectedRule = hwRulesOrigin[rulesIndex];
                }
                //FirstViolation
                var hrWithFirstViolationStatus = new HwRule(theSelectedRule);
                ruleStatus = 0;
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.HardwareRule);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.RuleIDForHardwareRule);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.FirstViolation);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.RuleInAlarm);
                hrWithFirstViolationStatus.RuleStatus = (UInt16)ruleStatus;
                result.Add(hrWithFirstViolationStatus);


                //RuleInAlarm
                var hrWithRuleInAlarmStatus = new HwRule(theSelectedRule); ;
                ruleStatus = 0;
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.HardwareRule);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.RuleIDForHardwareRule);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.RuleInAlarm);
                hrWithRuleInAlarmStatus.RuleStatus = (UInt16)ruleStatus;
                result.Add(hrWithRuleInAlarmStatus);

                //RuleInAlarm
                var hrWithViolationClearedStatus = new HwRule(theSelectedRule);
                ruleStatus = 0;
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.HardwareRule);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.RuleIDForHardwareRule);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.ViolationCleared);
                PacketBuilder.SetBit(ref ruleStatus, (EnumBits)EnumPointRuleStatus.RuleInAlarm);
                hrWithViolationClearedStatus.RuleStatus = (UInt16)ruleStatus;
                result.Add(hrWithViolationClearedStatus);
            }
            return result;
        }

        /// <summary>
        /// generate points with in specific area
        /// </summary>
        /// <param name="pointsCount"></param>
        /// <param name="location1"></param>
        /// <param name="location2"></param>
        /// <param name="location3"></param>
        /// <param name="location4"></param>
        /// <returns></returns>
        private Coordinate[] Calculate(int pointsCount, Coordinate location1, Coordinate location2, Coordinate location3,Coordinate location4)
        {
            Coordinate[] allCoords = { location1, location2, location3, location4 };
            double minLat = allCoords.Min(x => x.Latitude);
            double minLon = allCoords.Min(x => x.Longitude);
            double maxLat = allCoords.Max(x => x.Latitude);
            double maxLon = allCoords.Max(x => x.Longitude);

            var random = DataGeneratorFactory.Instance.RNG;

            Coordinate[] result = new Coordinate[pointsCount];
            for (int i = 0; i < result.Length; i++)
            {
                Coordinate point = new Coordinate();
                do
                {
                    point.Latitude = random.NextDouble() * (maxLat - minLat) + minLat;
                    point.Longitude = random.NextDouble() * (maxLon - minLon) + minLon;
                } while (!IsPointInPolygon(point, allCoords));
                result[i] = point;
            }
            return result;
        }

        //took it from http://codereview.stackexchange.com/a/108903
        //you can use your own one
        private bool IsPointInPolygon(Coordinate point, Coordinate[] polygon)
        {
            int polygonLength = polygon.Length, i = 0;
            bool inside = false;
            // x, y for tested point.
            double pointX = point.Longitude, pointY = point.Latitude;
            // start / end point for the current polygon segment.
            double startX, startY, endX, endY;
            Coordinate endPoint = polygon[polygonLength - 1];
            endX = endPoint.Longitude;
            endY = endPoint.Latitude;
            while (i < polygonLength)
            {
                startX = endX;
                startY = endY;
                endPoint = polygon[i++];
                endX = endPoint.Longitude;
                endY = endPoint.Latitude;
                //
                inside ^= ((endY > pointY) ^ (startY > pointY)) /* ? pointY inside [startY;endY] segment ? */
                          && /* if so, test if it is under the segment */
                          (pointX - endX < (pointY - endY) * (startX - endX) / (startY - endY));
            }
            return inside;
        }
        
    }

    public class Coordinate
    {
        public double Latitude { set; get; }
        public double Longitude { set; get; }
    }

    public class Point
    {
        public static Int32 LAT_LONG_FACTOR = 60000;
        public byte[] LongPointArray
        {
            get { return BitConverter.GetBytes(Convert.ToInt32(PointCoordinate.Longitude * LAT_LONG_FACTOR)); }
            private set { }
        }
        public byte[] LatPointArray
        {
            get { return BitConverter.GetBytes(Convert.ToInt32(PointCoordinate.Latitude * LAT_LONG_FACTOR)); }
            private set { }
        }

        public Coordinate PointCoordinate { get; set; }

        public uint CurrentTimeEPoc { get; set; }

        public List<HwRule> HwRules { get; set; }

        /// <summary>
        ///     An array of 12 bytes (one for each channel of the GPS Receiver), 
        ///     each of which contains the Satellite Vehicle Number (SVN) visible by the corresponding channel of the receiver followed by an array of 12 bytes 
        ///     (one for each SVN entry), each of which contains the Signal to Noise ratio for the corresponding channel and satellite.
        /// </summary>
        public const int SatelliteCount = 12;

        public Point()
        {
            PointCoordinate = new Coordinate();
            HwRules = new List<HwRule>();
        }


        public byte[] GetBuffer()
        {
            ushort deviceStatus = 42;
            byte speed = 42;
            byte heading = 42;
            short elevation = 42;
            byte PDOP = 42;
            byte HDOP = 42;
            byte VDOP = 42;
            byte GPSMode = 42;
            byte SatsInView = SatelliteCount;
            byte[] SatelliteVehicleNumber = new byte[SatelliteCount];//SVN
            byte[] SVR = new byte[SatelliteCount];
            byte temperature = 42;
            byte batVoltage = 42;

            byte ruleCount = (byte)HwRules.Count;
            var rulesBuffer = GenerateRulesBuffer(HwRules);

            var dataBuffer = Utilities.Combine(
                LongPointArray,
                LatPointArray,                
                BitConverter.GetBytes(  CurrentTimeEPoc),
                new byte[] {            speed },
                new byte[] {            heading },
                BitConverter.GetBytes(  elevation),
                new byte[] { PDOP },
                new byte[] { HDOP },
                new byte[] { VDOP },
                new byte[] { GPSMode },
                new byte[] { SatsInView },
                SatelliteVehicleNumber,
                SVR,
                new byte[] {            temperature },
                new byte[] {            batVoltage },
                BitConverter.GetBytes(  deviceStatus),
                new byte[] {            ruleCount },
                rulesBuffer
                );

            UInt16 messageSize = (UInt16)dataBuffer.Length;

            var buffer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandBaseXP },
                BitConverter.GetBytes(messageSize),
                dataBuffer);

            return buffer;
        }

        private static int SIZE_OF_RULE = sizeof(Int32) + sizeof(Int16);//RuleID + RuleStatus

        private byte[] GenerateRulesBuffer(List<HwRule> hwRules)
        {
            int rulesBufferSize = hwRules.Count * SIZE_OF_RULE;
            byte[] buffer = new byte[rulesBufferSize];

            int position = 0;
            foreach (var rule in hwRules)
            {
                PacketBuilder.AddData(ref buffer, ref position, rule.RuleID);
                PacketBuilder.AddData(ref buffer, ref position, rule.RuleStatus);

            }
            return buffer;
        }
    }

    /// <summary>
    /// implementation of Stop Sending Points command
    /// </summary>
    public class CommandStopSendingPoints : CommandBase
    {
        public CommandStopSendingPoints()
        {
            Command = EnumProtocolCommands.CommandStopSendingPoints;
            SubCommand = EMPTY_SUB_COMMAND;
        }

        public override byte[] Execute(byte[] input, System.UInt16 Size, ref int pos)
        {
            //                      |ACK    |size=0   |
            var answer = new byte[] { 0x10, 0x00, 0x00};
            return answer;
        }
    }
}
