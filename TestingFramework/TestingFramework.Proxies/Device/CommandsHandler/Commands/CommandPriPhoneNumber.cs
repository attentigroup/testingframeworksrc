﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPriPhoneNumber : CommandBase
    {
        public string PrimarySDCPhoneNumber { get; set; }
        public Encoding Encoding { get; set; }
        public CommandPriPhoneNumber() : base()
        {
            Command = EnumProtocolCommands.CommandPriPhoneNumber;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            PrimarySDCPhoneNumber = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
