﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandCurfewPreGrace : CommandBase
    {
        public short CurfewPreGrace { get; set; }
        public CommandCurfewPreGrace() : base()
        {
            Command = EnumProtocolCommands.CommandCurfewPreGrace;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            CurfewPreGrace = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
