﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandSecondaryGPRSIPAddress : CommandBase
    {
        public string SecondarySDCIPAddress { get; set; }
        public Encoding Encoding { get; set; }
        public CommandSecondaryGPRSIPAddress() : base()
        {
            Command = EnumProtocolCommands.CommandSecondaryGPRSIPAddress;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            SecondarySDCIPAddress = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
