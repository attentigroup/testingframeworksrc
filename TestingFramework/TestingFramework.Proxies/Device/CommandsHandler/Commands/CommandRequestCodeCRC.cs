﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestCodeCRC : CommandRequestCurrentSettingsBase
    {
        public CommandRequestCodeCRC() : base()
        {
            SubCommand = EnumProtocolCommands.CommandRequestCodeCRC;
        }

        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes((ushort)5),//Size
                new byte[] { (byte)EnumProtocolCommands.CommandRequestCodeCRC },
                BitConverter.GetBytes(0));//??Config CRC
            return answer;
        }
    }
}
