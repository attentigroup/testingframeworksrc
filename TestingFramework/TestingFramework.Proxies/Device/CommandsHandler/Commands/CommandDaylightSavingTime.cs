﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandDaylightSavingTime : CommandBase
    {
        public int DSTStartDay { get; set; }
        public int DSTEndDay { get; set; }
        
        public CommandDaylightSavingTime() : base()
        {
            Command = EnumProtocolCommands.CommandDaylightSavingTime;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            DSTStartDay = PacketBuilder.GetInt16(input, ref pos);
            DSTEndDay = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
