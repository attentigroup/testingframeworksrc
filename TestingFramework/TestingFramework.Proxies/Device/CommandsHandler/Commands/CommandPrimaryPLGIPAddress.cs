﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPrimaryPLGIPAddress : CommandBase
    {
        public string PrimaryPLGIPAddress { get; set; }
        public Encoding Encoding { get; set; }
        public CommandPrimaryPLGIPAddress() : base()
        {
            Command = EnumProtocolCommands.CommandPrimaryPLGIPAddress;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            PrimaryPLGIPAddress = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
