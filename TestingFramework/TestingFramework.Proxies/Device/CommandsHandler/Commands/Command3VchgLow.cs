﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class Command3VchgLow : CommandBase
    {
        public short ChargeLow { get; private set; }

        public Command3VchgLow() : base()
        {
            Command = EnumProtocolCommands.Command3VchgLow;
        }        

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            ChargeLow = PacketBuilder.GetInt16(input, ref pos);
            byte[] answer = null;
            return answer;
        }
    }
}
