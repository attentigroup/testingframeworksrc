﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandCurrentSettingsBaseSetOffenderDemographicID : CommandRequestCurrentSettingsBase
    {
        public uint OffenderId { get; set; }

        public CommandCurrentSettingsBaseSetOffenderDemographicID()
        {
            SubCommand = EnumProtocolCommands.CommandSetOffenderDemographicID;
        }
        public override byte[] Execute(byte[] input, System.UInt16 Size, ref int pos)
        {
            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes((ushort)5),              //Size
                new byte[] { (byte)EnumProtocolCommands.CommandSetOffenderDemographicID },
                BitConverter.GetBytes(OffenderId));
            return answer;
        }
    }

    public class CommandSetOffenderDemographicID : CommandBase
    {
        public int AssignedDemoID { get; set; }
        public CommandSetOffenderDemographicID()
        {
            Command = EnumProtocolCommands.CommandSetOffenderDemographicID;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            AssignedDemoID = PacketBuilder.GetInt32(input, ref pos);
            byte[] answer = null;
            return answer;
        }
    }
}
