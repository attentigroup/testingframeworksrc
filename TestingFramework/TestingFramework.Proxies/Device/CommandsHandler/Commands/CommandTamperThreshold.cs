﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandTamperThreshold : CommandBase
    {
        public short TamperThreshold { get; set; }
        public CommandTamperThreshold() : base()
        {
            Command = EnumProtocolCommands.CommandTamperThreshold;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            TamperThreshold = PacketBuilder.GetInt16(input, ref pos);

            byte[] answer = null;
            return answer;
        }
    }
}
