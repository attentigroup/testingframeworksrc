﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandBitOptions : CommandBase
    {
        public int MtdControlBits { get; set; }
        public CommandBitOptions() : base()
        {
            Command = EnumProtocolCommands.CommandBitOptions;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            MtdControlBits = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
