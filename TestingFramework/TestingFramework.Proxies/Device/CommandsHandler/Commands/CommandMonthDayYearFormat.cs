﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandMonthDayYearFormat : CommandBase
    {
        public string MonthDayYearFormat { get; set; }

        public Encoding Encoding { get; set; }
        public CommandMonthDayYearFormat() : base()
        {
            Command = EnumProtocolCommands.CommandMonthDayYearFormat;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            //TODO: understand the way commbox send date time values
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            MonthDayYearFormat = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
