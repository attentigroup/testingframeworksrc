﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestHWRevandBatteryCapacity : CommandRequestCurrentSettingsBase
    {
        public byte BatteryCapacity { get; set; }
        public byte HardwareVersion { get; set; }

        public CommandRequestHWRevandBatteryCapacity() : base()
        {
            SubCommand = EnumProtocolCommands.CommandRequestHWRevandBatteryCapacity;
            BatteryCapacity = 42;
            HardwareVersion = 42;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            var answer = Utilities.Combine(
                 new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                 BitConverter.GetBytes((ushort)3),//Size
                 new byte[] { (byte)EnumProtocolCommands.CommandRequestHWRevandBatteryCapacity },
                 new byte[] { BatteryCapacity, HardwareVersion });
            return answer;
        }
    }
}
