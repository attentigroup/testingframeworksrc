﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandMaxInChargerTHPE : CommandBase
    {
        public int MaxInChargerTHPE { get; set; }
        public CommandMaxInChargerTHPE() : base()
        {
            Command = EnumProtocolCommands.CommandMaxInChargerTHPE;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            MaxInChargerTHPE = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
