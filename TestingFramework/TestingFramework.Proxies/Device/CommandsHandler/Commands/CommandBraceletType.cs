﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandBraceletType : CommandBase
    {

        public byte BraceletType { get; set; }
        public CommandBraceletType() : base()
        {
            Command = EnumProtocolCommands.CommandBraceletType;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            BraceletType = PacketBuilder.GetByte(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
