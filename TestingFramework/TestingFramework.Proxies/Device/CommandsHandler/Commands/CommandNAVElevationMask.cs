﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandNAVElevationMask : CommandBase
    {
        public short NavigationElevationMask { get; set; }
        public CommandNAVElevationMask() : base()
        {
            Command = EnumProtocolCommands.CommandNAVElevationMask;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            NavigationElevationMask = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
