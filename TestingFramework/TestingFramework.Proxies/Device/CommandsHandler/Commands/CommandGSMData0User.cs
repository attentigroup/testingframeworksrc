﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandGSMData0User : CommandBase
    {
        public string NetworkUserId { get; set; }
        public Encoding Encoding { get; set; }

        public CommandGSMData0User() : base()
        {
            Command = EnumProtocolCommands.CommandGSMData0User;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            NetworkUserId = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
