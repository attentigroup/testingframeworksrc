﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandRequestCurrentSettingsBulkLoadCRCandAddress : CommandRequestCurrentSettingsBase
    {
        public UInt32 DeviceFileCRC { get; set; }

        public Int32 DeviceFileOffset { get; set; }


        public CommandRequestCurrentSettingsBulkLoadCRCandAddress() : base()
        {
            SubCommand = EnumProtocolCommands.CommandBulkLoadCRCandAddress;
            
            DeviceFileCRC = 0;
            DeviceFileOffset = 0;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            EnumCodeLoadCommands loadCommand = (EnumCodeLoadCommands)PacketBuilder.GetByte(input, ref pos);
            EnumCodeLoadCommands FileType = loadCommand;
            switch (loadCommand)
            {
                case EnumCodeLoadCommands.MTDAppInit:
                case EnumCodeLoadCommands.SIRFEphemerisInit:
                    break;//we are good
                default:
                    {
                        throw new Exception(string.Format("cmd:{0} sub-comd:{1} received unsupported load command with value {2}",
                            Command, SubCommand, loadCommand));
                    }
            }

            byte[] data = Utilities.Combine(
                BitConverter.GetBytes((byte)FileType),
                BitConverter.GetBytes(DeviceFileCRC),
                BitConverter.GetBytes(DeviceFileOffset));
            
            UInt16 size = (UInt16)(sizeof(EnumProtocolCommands) + data.Length);

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(size),
                new byte[] { (byte)EnumProtocolCommands.CommandBulkLoadCRCandAddress },
                data);

            return answer;
        }
    }
}
