﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPLGSamplingRate : CommandBase
    {
        public short PLGSamplingRate { get; set; }
        public CommandPLGSamplingRate() : base()
        {
            Command = EnumProtocolCommands.CommandPLGSamplingRate;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            PLGSamplingRate = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
