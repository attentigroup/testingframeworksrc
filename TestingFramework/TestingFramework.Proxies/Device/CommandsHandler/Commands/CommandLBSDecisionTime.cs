﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandLBSDecisionTime : CommandBase
    {
        public short LBSDecisionTime { get; set; }
        public CommandLBSDecisionTime() : base()
        {
            Command = EnumProtocolCommands.CommandLBSDecisionTime;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            LBSDecisionTime = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
