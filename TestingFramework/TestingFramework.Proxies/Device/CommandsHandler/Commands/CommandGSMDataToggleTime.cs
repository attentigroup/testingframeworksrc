﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandGSMDataToggleTime : CommandBase
    {
        public int DevicePrimaryNetworkToggleMins { get; set; }
        public CommandGSMDataToggleTime() : base()
        {
            Command = EnumProtocolCommands.CommandGSMDataToggleTime;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            DevicePrimaryNetworkToggleMins = PacketBuilder.GetInt32(input, ref pos);

            byte[] answer = null;
            return answer;
        }
    }
}
