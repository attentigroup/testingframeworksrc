﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandNoMotionMinutes : CommandBase
    {
        public int NoMotionMinutes { get; set; }

        public CommandNoMotionMinutes() : base()
        {
            Command = EnumProtocolCommands.CommandNoMotionMinutes;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            NoMotionMinutes = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
