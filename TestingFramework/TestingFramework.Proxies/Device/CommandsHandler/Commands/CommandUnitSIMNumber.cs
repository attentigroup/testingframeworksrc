﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandUnitSIMNumber : CommandBase
    {
        public string UnitSIMNumber { get; set; }
        public Encoding Encoding { get; set; }
        public CommandUnitSIMNumber() : base()
        {
            Command = EnumProtocolCommands.CommandUnitSIMNumber;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            UnitSIMNumber = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
