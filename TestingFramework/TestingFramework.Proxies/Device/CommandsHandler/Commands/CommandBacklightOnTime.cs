﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandBacklightOnTime : CommandBase
    {
        public byte BacklightOnTime { get; set; }
        public CommandBacklightOnTime() : base()
        {
            Command = EnumProtocolCommands.CommandBacklightOnTime;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            BacklightOnTime = PacketBuilder.GetByte(input, ref pos);
            byte[] answer = null;
            return answer;
        }
    }
}
