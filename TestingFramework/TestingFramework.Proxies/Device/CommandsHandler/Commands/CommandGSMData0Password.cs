﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandGSMData0Password : CommandBase
    {
        public string NetworkPassword { get; set; }
        public Encoding Encoding { get; set; }


        public CommandGSMData0Password() : base()
        {
            Command = EnumProtocolCommands.CommandGSMData0Password;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            NetworkPassword = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
