﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandSystemType : CommandBase
    {
        public byte SystemType { get; set; }
        public CommandSystemType() : base()
        {
            Command = EnumProtocolCommands.CommandSystemType;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            SystemType = PacketBuilder.GetByte(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
