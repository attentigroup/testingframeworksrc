﻿using CommBox.Infrastructure;
using System;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandDisplayMessage : CommandBase
    {
        public Int32 DateInt32 { get; set; }

        public Encoding Encoding { get; set; }

        public string DisplayMessage { get; set; }

        public CommandDisplayMessage()
        {
            Command = EnumProtocolCommands.CommandDisplayMessage;
        }
        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            DateInt32 = PacketBuilder.GetInt32(input, ref pos);
            int bufferSize = Size - sizeof(Int32);
            byte[] buffer = PacketBuilder.GetBuffer(input, bufferSize, ref pos);

            DisplayMessage = Encoding.GetString(buffer);
            
            return ACK;
        }
    }
}
