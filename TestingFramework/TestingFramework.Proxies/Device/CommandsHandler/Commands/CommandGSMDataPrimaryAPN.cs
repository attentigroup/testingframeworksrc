﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandGSMDataPrimaryAPN : CommandBase
    {
        public byte PrimaryNetworkID { get; set; }
        public CommandGSMDataPrimaryAPN() : base()
        {
            Command = EnumProtocolCommands.CommandGSMDataPrimaryAPN;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            PrimaryNetworkID = PacketBuilder.GetByte(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
