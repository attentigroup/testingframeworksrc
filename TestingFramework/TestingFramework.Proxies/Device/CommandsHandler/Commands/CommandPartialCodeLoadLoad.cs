﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPartialCodeLoadLoad : CommandPartialCodeLoadBase
    {
        public CommandPartialCodeLoadLoad()
        {
            SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisLoad;
        }
        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            var answer = new byte[] { 0x10, 0x00, 0x00 };//ACK
            return answer;
        }
    }
}
