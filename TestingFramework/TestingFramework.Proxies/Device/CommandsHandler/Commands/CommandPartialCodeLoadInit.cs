﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPartialCodeLoadInit : CommandPartialCodeLoadBase
    {
        public UInt32 FileCrc { get; set; }
        public CommandPartialCodeLoadInit() : base()
        {
            SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisInit;
        }
        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            FileCrc = PacketBuilder.GetUInt32(input, ref pos);

            var answer = new byte[] { 0x10, 0x00, 0x00 };//ACK
            return answer;
        }
    }
}
