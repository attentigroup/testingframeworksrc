﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPhoneNumbers : CommandBase
    {
        public static int SIZE_OF_PHONE_COUNT_UNICODE_STRING = 2;

        public static byte UnicodePhoneSeperator = 0xE0;

        public byte PhoneCount { get; set; }

        public Encoding Encoding { get; set; }

        public List<String> MtdDialoutDescriptionList { get; set; }

        public List<String> MtdDialoutPhoneNumberList { get; set; }

        public CommandPhoneNumbers() : base()
        {
            Command = EnumProtocolCommands.CommandPhoneNumbers;
            Encoding = Encoding.Unicode;
            MtdDialoutDescriptionList = new List<String>();
            MtdDialoutPhoneNumberList = new List<String>();
        }

        /// <summary>
        ///
        /// Phone Numbers command structure for ASCII
        /// 
        ///  -----------------------------------------------------------------------------------------------------------------------------
        /// |Phone Count|Description 1| ','  | Phone 1  | 0xFF |Description 2| ','  | Phone 2  | 0xFF |...|Description n| ','  | Phone n  |
        /// |1 byte     |1-16 bytes   |1 byte|1-20 bytes|1 byte|1-16 bytes   |1 byte|1-20 bytes|1 byte|...|1-16 bytes   |1 byte|1-20 bytes|
        ///  -----------------------------------------------------------------------------------------------------------------------------
        ///
        /// </summary>
        /// <param name="input"></param>
        /// <param name="Size"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            if( Encoding == Encoding.Unicode)//TODO: need to get this from outside the command
            {
                byte[]      buffer = PacketBuilder.GetBuffer(input, SIZE_OF_PHONE_COUNT_UNICODE_STRING, ref pos);
                var         phoneCountUnicodeString = Encoding.Unicode.GetString(buffer);
                int         sizeOfRestOfMessage = Size - SIZE_OF_PHONE_COUNT_UNICODE_STRING;
                byte[]      unicodePhoneSeperatorByteArray = { 0, UnicodePhoneSeperator };

                PhoneCount = byte.Parse(phoneCountUnicodeString);

                buffer = PacketBuilder.GetBuffer(input, sizeOfRestOfMessage, ref pos);
                var overAllUnicodeString = Encoding.Unicode.GetString(buffer);

                for (int i = 0,indexOfUnicodePhoneSeperator = 0, indexToStartFrom = 0; 
                    indexOfUnicodePhoneSeperator < sizeOfRestOfMessage; 
                    i++, indexToStartFrom = indexOfUnicodePhoneSeperator+1)
                {
                    indexOfUnicodePhoneSeperator = Array.IndexOf(buffer, UnicodePhoneSeperator, indexToStartFrom);
                    if (indexOfUnicodePhoneSeperator == -1)
                        break;
                    int count = indexOfUnicodePhoneSeperator - indexToStartFrom;
                    String descriptionAndPhone = Encoding.GetString(buffer, indexToStartFrom, count);
                    //remove the unicode seperator                    
                    String unicodePhoneSeperatorString = Encoding.GetString(unicodePhoneSeperatorByteArray);
                    descriptionAndPhone.Replace(unicodePhoneSeperatorString, "");
                    //split the string to description and phone number.
                    var phoneAndDescriptionSplited = descriptionAndPhone.Split(',');
                    MtdDialoutDescriptionList.Add(phoneAndDescriptionSplited[0]);
                    MtdDialoutPhoneNumberList.Add(phoneAndDescriptionSplited[1]);
                }

            }
            else
            {
                PhoneCount = PacketBuilder.GetByte(input, ref pos);
            }
            var answer = ACK;
            return answer;
        }
    }
}
