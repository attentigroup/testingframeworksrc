﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandIgnoreBraceletErrs : CommandBase
    {
        public char IgnoreBraceletErrors { get; set; }
        public CommandIgnoreBraceletErrs() : base()
        {
            Command = EnumProtocolCommands.CommandIgnoreBraceletErrors;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            short ignoreBraceletErrors = PacketBuilder.GetByte(input, ref pos);

            IgnoreBraceletErrors = 'N';

            if ( ignoreBraceletErrors == 1)
            {
                IgnoreBraceletErrors = 'Y';
            }

            byte[] answer = null;
            return answer;
        }
    }
}
