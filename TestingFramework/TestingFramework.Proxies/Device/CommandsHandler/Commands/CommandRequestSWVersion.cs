﻿using CommBox.Infrastructure;
using System;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestSWVersion : CommandBase
    {
        public string DeviceApplication { get; set; }

        public CommandRequestSWVersion()
        {
            Command = EnumProtocolCommands.CommandRequestCurrentSettings;
            SubCommand = EnumProtocolCommands.CommandRequestSWVersion;

            DeviceApplication = "V5.12.9.2";
        }

        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            string version = "";
            EnumFirmwareVersionTypes RequestedSwVersion = (EnumFirmwareVersionTypes)PacketBuilder.GetByte(input, ref pos);
            switch (RequestedSwVersion)
            {
                case EnumFirmwareVersionTypes.DeviceApplication: version = DeviceApplication; break;
                case EnumFirmwareVersionTypes.DeviceRom: version = "N/A"; break;
                case EnumFirmwareVersionTypes.DevicePIC: version = "N/1"; break;
                case EnumFirmwareVersionTypes.DeviceSirf: version = "322.000.000-O400"; break;
                case EnumFirmwareVersionTypes.DeviceSirfPTMSpecific: version = "N/1"; break;
                case EnumFirmwareVersionTypes.DeviceModem: version = "N/1"; break;
                case EnumFirmwareVersionTypes.DeviceBrx: version = "N/1"; break;
                case EnumFirmwareVersionTypes.ChargerApplication: version = "N/1"; break;
                case EnumFirmwareVersionTypes.ChargerRom: version = "N/1"; break;
                case EnumFirmwareVersionTypes.ChargerModem: version = "N/1"; break;
                case EnumFirmwareVersionTypes.ChargerBrx: version = "N/1"; break;
                case EnumFirmwareVersionTypes.DeviceCPLD: version = "V1.1.0"; break;
                default:
                    break;
            }
            //prepare answer
            //|Command|Length |SubCommand|VersionType|Data        |
            //|1 byte |2 bytes|1 byte    |1 byte     |Length bytes|

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },//Command 223
                BitConverter.GetBytes((ushort)(version.Length + 2)),  //Size
                new byte[] { (byte)EnumProtocolCommands.CommandRequestSWVersion, (byte)RequestedSwVersion },            //sub-command and version type
                Encoding.ASCII.GetBytes(version));          //Version
            return answer;
        }
    }
}
