﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestRulesCRC : CommandRequestCurrentSettingsBase
    {
        public CommandRequestRulesCRC() : base()
        {
            SubCommand = EnumProtocolCommands.CommandRequestRulesCRC;
        }

        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            UInt32 CommandRequestRulesCRC = 42;
            byte[] CommandRequestRulesCRCBytes = BitConverter.GetBytes(CommandRequestRulesCRC);
            int size32 = sizeof(EnumProtocolCommands) + CommandRequestRulesCRCBytes.Length;
            UInt16 size = (UInt16)size32;

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(size),
                new byte[] { (byte)EnumProtocolCommands.CommandRequestRulesCRC },
                CommandRequestRulesCRCBytes);
            return answer;
        }
    }
}
