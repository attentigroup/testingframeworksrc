﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandBraceletGoneTime : CommandBase
    {
        public short BraceletNoRxGone { get; set; }

        public CommandBraceletGoneTime() : base()
        {
            Command = EnumProtocolCommands.CommandBraceletGoneTime;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            BraceletNoRxGone = PacketBuilder.GetInt16(input, ref pos);

            byte[] answer = null;
            return answer;
        }
    }
}
