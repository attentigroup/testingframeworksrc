﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandUserName : CommandBase
    {
        public Encoding Encoding { get; set; }

        public string UserName { get; set; }
        public CommandUserName() : base()
        {
            Command = EnumProtocolCommands.CommandUserName;
            Encoding = Encoding.Unicode;
        }

        public override byte[] Execute(byte[] input, System.UInt16 Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            UserName = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
