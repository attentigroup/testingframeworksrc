﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPLGRequestTimeout : CommandBase
    {
        public short PLGRequestTimeout { get; set; }
        public CommandPLGRequestTimeout() : base()
        {
            Command = EnumProtocolCommands.CommandPLGRequestTimeout;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            PLGRequestTimeout = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
