﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandMonitorStartEndDateTime : CommandBase
    {
        public DateTime MonitorStartTimestamp { get; set; }
        public DateTime MonitorEndTimestamp { get; set; }


        public CommandMonitorStartEndDateTime() : base()
        {
            Command = EnumProtocolCommands.CommandMonitorStartEndDateTime;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            var _monitorStartTimestamp = PacketBuilder.GetInt32(input, ref pos);
            var _monitorEndTimestamp = PacketBuilder.GetInt32(input, ref pos);

            MonitorStartTimestamp = Utilities.GetDeviceLocalTime(_monitorStartTimestamp);
            MonitorEndTimestamp = Utilities.GetDeviceLocalTime(_monitorEndTimestamp);

            var answer = new byte[0];
            return answer;
        }
    }
}
