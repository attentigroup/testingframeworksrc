﻿using CommBox.Infrastructure;
using Common.Time;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandCurrentSettingsRequestSTimersandCRCs : CommandRequestCurrentSettingsBase
    {
        public bool AGPSInformationWasAsked { get; private set; }

        public bool AGPSSupport { get; set; }

        public uint DeviceAGPSFileAgeOffsetMinutes { get; set; }

        public CommandCurrentSettingsRequestSTimersandCRCs()
        {
            SubCommand = EnumProtocolCommands.CommandRequestSTimersandCRCs;
            AGPSSupport = true;
            DeviceAGPSFileAgeOffsetMinutes = 125 * 60; //125 Hours

        }
        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            //verify if AGPS data was asked
            UInt16 maskBits = PacketBuilder.GetUInt16(input, ref pos);
            AGPSInformationWasAsked = PacketBuilder.GetBit(maskBits, (EnumBits)EnumCRCandSTimerRequest.EphemerisTimeStamp);


            int maskBits2Send = 0;
            if(AGPSInformationWasAsked && AGPSSupport)
            {
                PacketBuilder.SetBit(ref maskBits2Send, (EnumBits)EnumCRCandSTimerRequest.EphemerisTimeStamp);
            }
            
            UInt32 DeviceAGPSFileAge = TimeUtility.GetEpoch() - DeviceAGPSFileAgeOffsetMinutes * 60;

            byte[] data = Utilities.Combine(BitConverter.GetBytes((short)maskBits2Send), BitConverter.GetBytes(DeviceAGPSFileAge));
            int size32 = sizeof(EnumProtocolCommands) + data.Length;
            UInt16 size = (UInt16)size32;

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(size),
                new byte[] { (byte)EnumProtocolCommands.CommandRequestSTimersandCRCs },
                data);

            return answer;
        }
    }
}
