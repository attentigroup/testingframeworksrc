﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandCallBackInterval : CommandBase
    {
        public short CallBackIntervalSeconds { get; set; }
        public short InChargerCallBackIntervalSecs { get; set; }

        public CommandCallBackInterval() : base()
        {
            Command = EnumProtocolCommands.CommandCallBackInterval;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            CallBackIntervalSeconds = PacketBuilder.GetInt16(input, ref pos);
            InChargerCallBackIntervalSecs = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
