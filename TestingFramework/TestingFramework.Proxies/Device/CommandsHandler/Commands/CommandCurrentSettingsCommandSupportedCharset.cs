﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    class CommandCurrentSettingsCommandSupportedCharset : CommandRequestCurrentSettingsBase
    {
        public EnumSupportedCharset CharSetVersion { get; set; }
        public CommandCurrentSettingsCommandSupportedCharset()
        {
            SubCommand = EnumProtocolCommands.CommandSupportedCharset;
            CharSetVersion = EnumSupportedCharset.Unknonw;
        }
        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            UInt16 size = 2;// sizeof(EnumProtocolCommands) + sizeof(EnumSupportedCharset);

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(size),
                new byte[] { (byte)EnumProtocolCommands.CommandSupportedCharset },
                new byte[] { (byte)CharSetVersion });

            return answer;
        }
    }
}
