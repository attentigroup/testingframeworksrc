﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandLBSMode : CommandBase
    {
        public short LBSMode { get; set; }
        public CommandLBSMode() : base()
        {
            Command = EnumProtocolCommands.CommandLBSMode;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            LBSMode = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
