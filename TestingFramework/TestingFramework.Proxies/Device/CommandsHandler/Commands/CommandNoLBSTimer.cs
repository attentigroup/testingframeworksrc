﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandNoLBSTimer : CommandBase
    {
        public short NoLBSTimer { get; set; }
        public CommandNoLBSTimer() : base()
        {
            Command = EnumProtocolCommands.CommandNoLBSTimer;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            NoLBSTimer = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
