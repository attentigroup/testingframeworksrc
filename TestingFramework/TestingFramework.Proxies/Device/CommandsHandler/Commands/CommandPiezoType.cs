﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPiezoType : CommandBase
    {
        public char PiezoType { get; set; }
        public CommandPiezoType() : base()
        {
            Command = EnumProtocolCommands.CommandPiezoType;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte piezoTypeByte = PacketBuilder.GetByte(input, ref pos);
            PiezoType = Convert.ToChar(piezoTypeByte);

            byte[] answer = null;
            return answer;

        }
    }

}
