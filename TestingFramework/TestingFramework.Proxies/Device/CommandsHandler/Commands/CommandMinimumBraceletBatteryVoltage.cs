﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandMinimumBraceletBatteryVoltage : CommandBase
    {
        public short MinimumBraceletBatteryVoltage { get; set; }
        public CommandMinimumBraceletBatteryVoltage() : base()
        {
            Command = EnumProtocolCommands.CommandMinimumBraceletBatteryVoltage;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            MinimumBraceletBatteryVoltage = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
