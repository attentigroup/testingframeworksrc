﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandOperationMode : CommandBase
    {
        public byte OperationMode { get; set; }
        public CommandOperationMode() : base()
        {
            Command = EnumProtocolCommands.CommandOperationMode;
        }
            
        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            OperationMode = PacketBuilder.GetByte(input, ref pos);
            
            byte[] answer = null;
            return answer;
        }
    }

}
