﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPartialCodeLoadCrc : CommandPartialCodeLoadBase
    {
        public UInt32 FileCrc { get; set; }

        public CommandPartialCodeLoadCrc() : base()
        {
            SubCommand = (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisCrc;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            FileCrc = PacketBuilder.GetUInt32(input, ref pos);

            var answer = new byte[] { 0x10, 0x00, 0x00 };//ACK
            return answer;
        }
    }
}
