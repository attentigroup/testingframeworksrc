﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    /// <summary>
    /// used for all CommandPartialCodeLoad commands
    /// </summary>
    public abstract class CommandPartialCodeLoadBase : CommandBase
    {
        public CommandPartialCodeLoadBase()
        {
            Command = EnumProtocolCommands.CommandPartialCodeLoad;
        }
    }
}
