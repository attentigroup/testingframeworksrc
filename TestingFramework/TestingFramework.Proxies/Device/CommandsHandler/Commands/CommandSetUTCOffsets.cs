﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    /// <summary>
    /// This frame type is used to configure the device with offsets used to calculate local time from GMT time based on whether Daylight Saving Time is in effect or not.
    /// There are two command frame formats as shown in the figures below.The Northern Frame is used in the northern hemisphere, or where the start of daylight saving time occurs earlier in the year than the end.The Southern Frame is used in the southern hemisphere, or where the start of daylight saving time occurs later in the year than the end.
    /// Set UTC Offsets Northern Frame:
    /// |StandardTimeOffset |DaylightTimeOffset |
    /// -----------------------------------------
    /// |2 bytes            |2 bytes            |
    ///
    /// Set UTC Offsets Southern Frame
    /// |DaylightTimeOffset |StandardTimeOffset|
    /// -----------------------------------------
    /// |2 bytes            |2 bytes            |
    /// 
    /// Both STD Offset and DST Offset are provided in minutes and represent the offset between GMT and local time for Standard Time and Daylight Saving Time respectively.
    ///  For example, the values provided for STD Offset and DST Offset would be -300 and -240 respectively for the US Eastern time zone.This means that during Standard Time, we are 300 minutes(5 hours) behind GMT and during Daylight Saving Time, we are 240 minutes(4 hours) behind GMT.
    ///If the device is in an area where Daylight Saving Time is not honored, then the STD Offset would be provided for each parameter so that the time would not change when Daylight Saving Time goes into effect.
    ///This command must be used in conjunction with the command to set the actual start and end dates for Daylight Saving Time described in section 5.15, Command #63: Daylight Saving Time.
    /// </summary>
    public class CommandSetUTCOffsets : CommandBase
    {
        public int StandardTimeOffset { get; set; }
        public int DaylightTimeOffset { get; set; }
        public CommandSetUTCOffsets() : base()
        {
            Command = EnumProtocolCommands.CommandSetUTCOffsets;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {//assume Northern
            StandardTimeOffset = PacketBuilder.GetInt16(input, ref pos);
            DaylightTimeOffset = PacketBuilder.GetInt16(input, ref pos);
            byte[] answer = null;
            return answer;
        }
    }
}
