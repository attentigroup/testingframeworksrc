﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandSecondayPLGIPAddress : CommandBase
    {
        public string SecondayPLGIPAddress{ get; set; }
        public Encoding Encoding { get; set; }
        public CommandSecondayPLGIPAddress() : base()
        {
            Command = EnumProtocolCommands.CommandSecondayPLGIPAddress;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            SecondayPLGIPAddress = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
