﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandBraceletRange : CommandBase
    {
        public short MinValidBraceletRssi { get; set; }
        public CommandBraceletRange() : base()
        {
            Command = EnumProtocolCommands.CommandBraceletRange;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            MinValidBraceletRssi = PacketBuilder.GetInt16(input, ref pos);

            byte[] answer = null;
            return answer;
        }
    }
}
