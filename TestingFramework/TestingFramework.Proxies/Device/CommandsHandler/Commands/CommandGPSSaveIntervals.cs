﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandNormalGPSSaveInterval : CommandBase
    {
        public int NormalGPSSaveInterval { get; set; }
        public CommandNormalGPSSaveInterval() : base()
        {
            Command = EnumProtocolCommands.CommandGPSNormalSave;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            NormalGPSSaveInterval = PacketBuilder.GetInt16(input, ref pos);
            byte[] answer = null;
            return answer;
        }
    }

    public class CommandGPSAlarmSaveInterval : CommandBase
    {
        public int AlarmGPSSaveInterval { get; set; }
        public CommandGPSAlarmSaveInterval() : base()
        {
            Command = EnumProtocolCommands.CommandGPSAlarmSave;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            AlarmGPSSaveInterval = PacketBuilder.GetInt16(input, ref pos);
            byte[] answer = null;
            return answer;
        }
    }


}
