﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandMotionThreshold : CommandBase
    {
        public short MotionThreshold { get; set; }
        public CommandMotionThreshold() : base()
        {
            Command = EnumProtocolCommands.CommandMotionThreshold;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            MotionThreshold = PacketBuilder.GetInt16(input, ref pos);
            var answer = new byte[0];
            return answer;
        }
    }
}
