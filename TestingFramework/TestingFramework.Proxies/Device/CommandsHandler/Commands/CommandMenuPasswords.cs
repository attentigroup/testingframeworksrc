﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandFactoryMenuPassword : CommandBase
    {
        public int FactoryMenuPassword { get; set; }
        public CommandFactoryMenuPassword() : base()
        {
            Command = EnumProtocolCommands.CommandFactoryMenuPassword;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            FactoryMenuPassword = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
    public class CommandOfficerMenuPassword : CommandBase
    {
        public int OfficerMenuPassword { get; set; }
        public CommandOfficerMenuPassword() : base()
        {
            Command = EnumProtocolCommands.CommandOfficerMenuPassword;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            OfficerMenuPassword = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
