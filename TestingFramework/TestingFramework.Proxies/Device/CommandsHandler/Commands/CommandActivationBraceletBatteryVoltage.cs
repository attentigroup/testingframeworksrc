﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandActivationBraceletBatteryVoltage : CommandBase
    {
        public short ActivationBraceletBatteryVoltage { get; set; }
        public CommandActivationBraceletBatteryVoltage() : base()
        {
            Command = EnumProtocolCommands.CommandActivationBraceletBatteryVoltage;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            ActivationBraceletBatteryVoltage = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
