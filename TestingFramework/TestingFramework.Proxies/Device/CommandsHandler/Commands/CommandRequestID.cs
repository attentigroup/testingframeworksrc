﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestID : CommandBase
    {
        #region Properties
        public int DeviceId { get; set; }

        #endregion Properties

        public CommandRequestID()
        {
            Command = EnumProtocolCommands.CommandRequestCurrentSettings;
            SubCommand = EnumProtocolCommands.CommandRequestID;
        }

        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            var answer = Utilities.Combine(
                                        new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                                        BitConverter.GetBytes((ushort)6),
                                        new byte[] { (byte)EnumProtocolCommands.CommandRequestID },
                                        new byte[] { (byte)EnumDeviceType.MTD },
                                        BitConverter.GetBytes(DeviceId)
                                        );
            return answer;
        }
    }
}
