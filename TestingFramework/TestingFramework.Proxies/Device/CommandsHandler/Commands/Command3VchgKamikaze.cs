﻿using CommBox.Infrastructure;


namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class Command3VchgKamikaze : CommandBase
    {
        public short ChargeKamikaze { get; set; }
        public Command3VchgKamikaze() : base()
        {
            Command = EnumProtocolCommands.Command3VchgKamikaze;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            ChargeKamikaze = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
