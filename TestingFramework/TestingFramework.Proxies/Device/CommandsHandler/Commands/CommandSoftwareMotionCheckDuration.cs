﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandSoftwareMotionCheckDuration : CommandBase
    {
        public short SoftwareMotionCheckDuration { get; set; }
        public CommandSoftwareMotionCheckDuration() : base()
        {
            Command = EnumProtocolCommands.CommandSoftwareMotionCheckDuration;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            SoftwareMotionCheckDuration = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
