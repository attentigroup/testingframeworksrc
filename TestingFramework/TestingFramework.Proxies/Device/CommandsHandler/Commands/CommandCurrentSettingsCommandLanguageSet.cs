﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    class CommandCurrentSettingsCommandLanguageSet : CommandRequestCurrentSettingsBase
    {
        public UInt16 LanguageSet{ get; set; }

        public CommandCurrentSettingsCommandLanguageSet()
        {
            SubCommand = EnumProtocolCommands.CommandLanguageSet;
            LanguageSet = 3;//why? because...
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] data = BitConverter.GetBytes(LanguageSet);
            int size32 = sizeof(EnumProtocolCommands) + data.Length;
            UInt16 size = (UInt16)size32;

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(size),
                new byte[] { (byte)EnumProtocolCommands.CommandLanguageSet },
                data);

            return answer;
        }
    }
}
