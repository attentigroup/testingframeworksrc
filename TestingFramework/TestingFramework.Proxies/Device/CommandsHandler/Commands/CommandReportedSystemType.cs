﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandReportedSystemType : CommandBase
    {
        public byte ReportedSystemType { get; set; }
        public CommandReportedSystemType() : base()
        {
            Command = EnumProtocolCommands.CommandReportedSystemType;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            ReportedSystemType = PacketBuilder.GetByte(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
