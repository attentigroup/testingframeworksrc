﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandMonthDayDateFormat : CommandBase
    {
        public string MonthDayDateFormat { get; set; }

        public Encoding Encoding { get; set; }
        public CommandMonthDayDateFormat() : base()
        {
            Command = EnumProtocolCommands.CommandMonthDayDateFormat;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            //TODO: understand the way commbox send date time values
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            MonthDayDateFormat = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;

        }
    }
}
