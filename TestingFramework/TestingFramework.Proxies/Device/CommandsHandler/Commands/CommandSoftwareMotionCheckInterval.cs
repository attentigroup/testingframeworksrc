﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandSoftwareMotionCheckInterval : CommandBase
    {
        public short SoftwareMotionCheckInterval { get; set; }
        public CommandSoftwareMotionCheckInterval() : base()
        {
            Command = EnumProtocolCommands.CommandSoftwareMotionCheckInterval;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            SoftwareMotionCheckInterval = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
