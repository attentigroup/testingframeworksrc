﻿using CommBox.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandCurrentSettingsSetBraceletSerialNumber : CommandRequestCurrentSettingsBase
    {
        #region Properties
        public int BraceletID { get; set; }

        #endregion Properties

        public CommandCurrentSettingsSetBraceletSerialNumber()
        {
            SubCommand = EnumProtocolCommands.CommandSetBraceletSerialNumber;
        }
        public override byte[] Execute(byte[] input, System.UInt16 Size, ref int pos)
        {
            ushort messageSize = sizeof(byte) + sizeof(int);

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(messageSize),
                new byte[] { (byte)EnumProtocolCommands.CommandSetBraceletSerialNumber },
                BitConverter.GetBytes(BraceletID)
                );
            return answer;
        }
    }

    public class CommandSetBraceletSerialNumber : CommandBase
    {
        public int BraceletID { get; set; }
        public CommandSetBraceletSerialNumber() : base()
        {
            Command = EnumProtocolCommands.CommandSetBraceletSerialNumber;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            BraceletID = PacketBuilder.GetInt32(input, ref pos);

            byte[] answer = null;
            return answer;
        }
    }
}
