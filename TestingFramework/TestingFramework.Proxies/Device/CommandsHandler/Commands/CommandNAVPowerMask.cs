﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandNAVPowerMask : CommandBase
    {
        public byte NavigationPowerMask { get; set; }
        public CommandNAVPowerMask() : base()
        {
            Command = EnumProtocolCommands.CommandNAVPowerMask;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            NavigationPowerMask = PacketBuilder.GetByte(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
