﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandEncryptionMethodSupported : CommandBase
    {
        #region Properties
        public EnumEncryptionType EnumEncryptionType { get; set; }

        #endregion Properties

        public CommandEncryptionMethodSupported()
        {
            Command = EnumProtocolCommands.CommandRequestCurrentSettings;
            SubCommand = EnumProtocolCommands.CommandEncryptionMethodSupported;
        }

        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            ushort messageSize = sizeof(byte) + sizeof(byte);

            var answer = Utilities.Combine(
                new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                BitConverter.GetBytes(messageSize),
                new byte[] { (byte)EnumProtocolCommands.CommandEncryptionMethodSupported },
                new byte[] { ((byte)EnumEncryptionType) }
                );
            return answer;
        }
    }
}
