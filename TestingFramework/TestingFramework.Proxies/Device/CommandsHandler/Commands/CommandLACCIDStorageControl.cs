﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{

    public class CommandLACCIDStorageControl : CommandBase
    {
        public byte LaccidStorageControl { get; set; }
        public CommandLACCIDStorageControl() : base()
        {
            Command = EnumProtocolCommands.CommandLACCIDStorageControl;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            LaccidStorageControl = PacketBuilder.GetByte(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
