﻿using CommBox.Infrastructure;
using log4net;
using System;
using System.Reflection;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    /// <summary>
    /// base class for all commands
    /// </summary>
    abstract public class CommandBase : ICommand
    {
        #region Consts

        public static EnumProtocolCommands EMPTY_SUB_COMMAND = (EnumProtocolCommands)0;

        #endregion Consts
        
        #region Properties

        private EnumProtocolCommands _commnad;

        public EnumProtocolCommands Command
        {
            get { return _commnad; }
            set { _commnad = value; }
        }

        private EnumProtocolCommands _subCommand;

        public EnumProtocolCommands SubCommand
        {
            get { return _subCommand; }
            set { _subCommand = value; }
        }

        private EnumCommandExecuted _commandExecuted = EnumCommandExecuted.CommandExecutedNotExecuted;
        public EnumCommandExecuted CommandExecuted
        {
            get { return _commandExecuted; }
            set { _commandExecuted = value; }
        }

        public string CommandKey => GenerateKeyFromCommands();

        IExecuteCommands _executeCommandsObject;
        public IExecuteCommands ExecuteCommandsObject
        {
            get { return _executeCommandsObject; }
            set { _executeCommandsObject = value; }
        }

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static byte[] _ack = null;
        /// <summary>
        /// ACK message buffer
        /// </summary>
        public byte[] ACK
        {
            get
            {
                if (_ack == null)
                {
                    _ack = Utilities.Combine(
                        new byte[] { (byte)EnumProtocolCommands.CommandACK },
                        BitConverter.GetBytes((short)0));
                }
                return _ack;
            }
            private set { }
        }

        #endregion Properties

        public abstract byte[] Execute(byte[] input, UInt16 Size, ref int pos);

        /// <summary>
        /// method can be override by implementation
        /// </summary>
        public virtual void PacketReset(){}


        protected string GenerateKeyFromCommands()
        {
            var result = GenerateKeyFromCommands(Command, SubCommand);
            return result;
        }

        public static string GenerateKeyFromCommands(EnumProtocolCommands _commnad, EnumProtocolCommands _subCommnad)
        {
            return string.Format("{0}_{1}", _commnad.ToString(), _subCommnad.ToString());
        }

        
    }

    public abstract class CommandRequestCurrentSettingsBase : CommandBase
    {
        public CommandRequestCurrentSettingsBase()
        {
            Command = EnumProtocolCommands.CommandRequestCurrentSettings;
        }
    }

    public abstract class CommandReturnEmptyBufferBase : CommandRequestCurrentSettingsBase
    {
        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            var answer = new byte[0];
            return answer;
        }
    }
}
