﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandInChargerBumpLeeway : CommandBase
    {
        public short InChargerBumpLeeway { get; set; }
        public CommandInChargerBumpLeeway() : base()
        {
            Command = EnumProtocolCommands.CommandInChargerBumpLeeway;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            InChargerBumpLeeway = PacketBuilder.GetInt16(input, ref pos);

            var answer = new byte[0];
            return answer;
        }
    }
}
