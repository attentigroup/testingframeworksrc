﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandPrimaryGPRSIPAddress : CommandBase
    {
        public string PrimarySDCIPAddress { get; set; }
        public Encoding Encoding { get; set; }
        public CommandPrimaryGPRSIPAddress() : base()
        {
            Command = EnumProtocolCommands.CommandPrimaryGPRSIPAddress;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            PrimarySDCIPAddress = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
