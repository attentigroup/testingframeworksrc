﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandContactMethod : CommandBase
    {
        public byte ContactMethod { get; set; }
        public CommandContactMethod() : base()
        {
            Command = EnumProtocolCommands.CommandContactMethod;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            ContactMethod = PacketBuilder.GetByte(input, ref pos);
            var answer = new byte[0];
            return answer;
        }
    }
}
