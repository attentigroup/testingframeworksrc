﻿using CommBox.Infrastructure;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestCurrentSettingsSetTime : CommandReturnEmptyBufferBase
    {
        public CommandRequestCurrentSettingsSetTime() : base()
        {
            SubCommand = EnumProtocolCommands.CommandSetTime;
        }
    }


 

    public class CommandSetTime : CommandReturnEmptyBufferBase
    {
        public CommandSetTime() : base()
        {
            Command = EnumProtocolCommands.CommandSetTime;
        }
    }
    
    public class CommandACK : CommandReturnEmptyBufferBase
    {
        public CommandACK() : base()
        {
            Command = EnumProtocolCommands.CommandACK;
        }
    }
    
    public class CommandGSMData1APNUser : CommandReturnEmptyBufferBase
    {
        public CommandGSMData1APNUser() : base()
        {
            Command = EnumProtocolCommands.CommandGSMData1APNUser;
        }
    }
    public class CommandGSMData1APNPassword : CommandReturnEmptyBufferBase
    {
        public CommandGSMData1APNPassword() : base()
        {
            Command = EnumProtocolCommands.CommandGSMData1APNPassword;
        }
    }
}
