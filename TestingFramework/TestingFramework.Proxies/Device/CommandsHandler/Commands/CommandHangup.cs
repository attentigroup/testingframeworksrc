﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandHangup : CommandBase
    {
        public CommandHangup()
        {
            Command = EnumProtocolCommands.CommandHangup;
        }
        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            var answer = new byte[] { 0x10, 0x00, 0x00 };//ACK
            return answer;
        }
    }
}
