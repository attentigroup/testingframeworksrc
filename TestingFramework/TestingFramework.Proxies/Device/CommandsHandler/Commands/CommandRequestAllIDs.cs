﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandRequestAllIDs : CommandBase
    {
        #region Properties
        public int DeviceId { get; set; }
        public ushort DeviceStatus { get; set; }

        #endregion Properties


        public CommandRequestAllIDs()
        {
            Command = EnumProtocolCommands.CommandRequestCurrentSettings;
            SubCommand = EnumProtocolCommands.CommandRequestAllIDs;

            DeviceStatus = 0x40;
        }

        public override byte[] Execute(byte[] input, UInt16 Size, ref int pos)
        {
            // |Commands|Size   |SubCommand|Pri device ID|Pri device Status|Sec device ID|Sec device Status|
            // |1 byte  |2 bytes|1 byte    |4 bytes      |2 bytes          |4 bytes      |2 bytes          |

            var answer = Utilities.Combine(
                                         new byte[] { (byte)EnumProtocolCommands.CommandResponseCurrentSettings },
                                         BitConverter.GetBytes((ushort)8),              //Size
                                         new byte[] { (byte)EnumProtocolCommands.CommandRequestAllIDs },
                                         new byte[] { 1 },                              //count of devices ids
                                         BitConverter.GetBytes(DeviceId), BitConverter.GetBytes(DeviceStatus));
            return answer;

        }
    }
}
