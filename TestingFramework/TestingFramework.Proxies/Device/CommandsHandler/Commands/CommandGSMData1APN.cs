﻿using CommBox.Infrastructure;
using System.Text;

namespace TestingFramework.Proxies.Device.CommandsHandler.Commands
{
    public class CommandGSMData1APN : CommandBase
    {
        public string GPRSAPN1 { get; set; }
        public Encoding Encoding { get; set; }
        public CommandGSMData1APN() : base()
        {
            Command = EnumProtocolCommands.CommandGSMData1APN;
            Encoding = Encoding.ASCII;
        }

        public override byte[] Execute(byte[] input, ushort Size, ref int pos)
        {
            byte[] buffer = PacketBuilder.GetBuffer(input, Size, ref pos);
            GPRSAPN1 = Encoding.GetString(buffer);

            var answer = new byte[0];
            return answer;
        }
    }
}
