﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler.Exceptions
{
    public class CommandAlreadyExistException : Exception
    {
        private ICommand command;

        public ICommand Command
        {
            get { return command; }
            set { command = value; }
        }

        public CommandAlreadyExistException(ICommand command)
        {
            Command = command;
        }

        public override string Message => 
            string.Format("Command {0} subcommand{1} already exist in the dictionary.", Command.Command, Command.SubCommand);
    }
}
