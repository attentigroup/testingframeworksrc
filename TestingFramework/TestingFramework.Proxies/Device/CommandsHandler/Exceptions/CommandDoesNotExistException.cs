﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler.Exceptions
{
    public class CommandDoesNotExistException : Exception
    {
        public EnumProtocolCommands Command { get; set; }
        public EnumProtocolCommands SubCommand { get; set; }

        public CommandDoesNotExistException(EnumProtocolCommands command, EnumProtocolCommands subCommand)
        {
            Command = command;
            SubCommand = subCommand;
        }

        public override string Message => 
            string.Format("command for {0} subcommand {1} not exist in the dictionary", Command, SubCommand);
    }
}
