﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device.CommandsHandler
{
    public interface ICommandMultiKey
    {

        byte[] Execute(byte[] input, ref int pos);
    }
}
