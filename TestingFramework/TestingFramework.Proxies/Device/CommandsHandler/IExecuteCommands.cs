﻿using CommBox.Infrastructure;
using System;

namespace TestingFramework.Proxies.Device.CommandsHandler
{
    /// <summary>
    /// Execute-Commands interface for executing-commands object
    /// </summary>
    public interface IExecuteCommands
    {
        //adding command to the executing object
        void AddCommand(ICommand command);
        /// <summary>
        /// executing commands
        /// </summary>
        /// <param name="command"></param>
        /// <param name="Size"></param>
        /// <param name="subCommand"></param>
        /// <param name="input"></param>
        /// <param name="pos"></param>
        /// <returns>byte[] that represent the answer of the command in case we </returns>
        byte[] ExecuteCommand(EnumProtocolCommands command, UInt16 Size, EnumProtocolCommands subCommand, byte[] input, ref int pos);
        /// <summary>
        /// get specific command by command and sub-command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="subCommand"></param>
        /// <returns>ICommand interface to the actual command</returns>
        ICommand GetCommand(EnumProtocolCommands command, EnumProtocolCommands subCommand);
        /// <summary>
        /// indicate to reset data in all commands that it is importent for.
        /// </summary>
        void PacketReset();
    }
}
