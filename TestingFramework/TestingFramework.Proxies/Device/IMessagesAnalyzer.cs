﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device
{
    interface IMessagesAnalyzer
    {
        void AnalyzeFrames();
    }
}
