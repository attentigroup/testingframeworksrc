﻿using CommBox.Infrastructure;
using Common.Enum;
using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Factories;

namespace TestingFramework.Proxies.Device
{
    public class DeviceClientDetais : ClientDetails
    {
        public int DeviceId { get; set; }
        public uint OffenderId { get; set; }
        public TcpClient TcpClientConnection { get; set; }
        public byte[] PacketData;
        public Task ReceiverDeviceMessagesTask { get; set; }
        public IExecuteCommands ExecuterObject { get; set; }

        public uint RulesCrc { get; set; }



    }

    public class DeviceNetwork
    {
        #region Consts

        public const int SocketBufferSize = 2048;
        public const int CRC_SIZE = 4;

        #endregion Consts

        #region Properties

        //Public
        /// <summary>
        /// description of the network issues need to be generate in this client
        /// </summary>
        public NetworkIssuesDescription NetworkIssuesDescription { get; set; }
        /// <summary>
        /// dictionary of the connection that this device network holds
        /// </summary>
        public ConcurrentDictionary<int, DeviceClientDetais> DeviceClients { get; set; }

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //Private
        private Boolean RunReceiverDeviceMessages = false;

        #endregion Properties



        public DeviceNetwork(CancellationToken cancellationToken, Settings settings)
        {
            Init();
        }

        private void Init()
        {
            DeviceClients = new ConcurrentDictionary<int, DeviceClientDetais>();
        }
        public void ConnectClient(string hostname, int port, int deviceId, uint offenderId, IExecuteCommands executerObject)
        {
            //create TCP Client
            var tcpClientConnection = new TcpClient(hostname, port);
            SendHello(tcpClientConnection.GetStream(), deviceId);
            //Create the detaild object for the client
            var deviceClientDetais = new DeviceClientDetais
            {
                DeviceId = deviceId,
                OffenderId = offenderId,
                TcpClientConnection = tcpClientConnection,
                ExecuterObject = executerObject,
            };

            //create task to recive the messages
            Task receiverDeviceMessagesTask = new Task(() => ReceiverDeviceMessages(deviceClientDetais.DeviceId), TaskCreationOptions.AttachedToParent);
            if (receiverDeviceMessagesTask.Status == TaskStatus.Created)
            {
                //update the task for later use
                deviceClientDetais.ReceiverDeviceMessagesTask = receiverDeviceMessagesTask;
                //verify the device client in the list
                if (DeviceClients.TryAdd(deviceClientDetais.DeviceId, deviceClientDetais) == false)
                {
                    Log.ErrorFormat("Fail to get device client ussing DeviceClients.TryAdd for index[{1}]", deviceClientDetais.DeviceId);
                }

                ReceiverDeviceMessages(deviceId);
            }
        }

        private void ReceiverDeviceMessages(Object taskContext)
        {
            DeviceClientDetais  deviceClientDetais;

            int deviceId = (int)taskContext;

            if (DeviceClients.TryGetValue(deviceId, out deviceClientDetais) == false)
            {
                Log.ErrorFormat("Fail to Clients.TryGetValue for index[{1}]", deviceId);
            }

            if (deviceClientDetais != null)
            {
                NetworkStream Stream = new NetworkStream(deviceClientDetais.TcpClientConnection.Client);
                int BytesRead = 0;
                deviceClientDetais.TimeStamp = DateTime.Now;

                bool ContinueCommunication = true;
                
                Log.DebugFormat("New connection from {0}", deviceClientDetais.TcpClientConnection.Client.RemoteEndPoint);
                try
                {
                    Byte[] ReceivedBytes = new Byte[SocketBufferSize];
                    while (ContinueCommunication && (BytesRead = Stream.Read(ReceivedBytes, 0, ReceivedBytes.Length)) != 0)
                    {
                        int Length = BytesRead;
                        deviceClientDetais.TimeStamp = DateTime.Now;

                        Collect(ref deviceClientDetais, ReceivedBytes, BytesRead);
                        if (PacketBuilder.CheckSMARTMessage(deviceClientDetais.PacketData, deviceClientDetais.PacketData.Length) == true )
                        {
                            byte packetID = (byte)deviceClientDetais.Index;
                            EnumEncryptionType encryption = EnumEncryptionType.None;
                            var converstationData = new ConversationData() { DeviceID = deviceId, PacketID = packetID, EncryptionLevel = encryption };
                            var message = PacketBuilder.DismantlePacket(
                                deviceClientDetais.PacketData, 
                                ref converstationData,
                                EnumEndPoint.Device);

                            int             receivedBytesPosition = 0;
                            byte[]          command_buff = new byte[message.Length];//temp - to hold each command at a time - //??it might be that COMMAND_SIZE is good to add
                            List<byte[]>    answers = new List<byte[]>();
                            byte[]          send_buff = null;
                            

                            while (receivedBytesPosition < (message.Length))
                            {
                                //copy the command and the rest of the buffer to command buffer - because the size of commands is variant
                                Buffer.BlockCopy(message, receivedBytesPosition, command_buff, 0, message.Length - receivedBytesPosition);

                                FullCommandDescription currentfullCommandDescription = new FullCommandDescription();
                                byte[] answer = null;
                                int commandOffset = ParseCommand(deviceClientDetais, command_buff, ref currentfullCommandDescription, ref answer);
                                //support network issues - in case we need to stop sending from that command.
                                bool continueSending = AnalyzeCommand4NetworkIssues(currentfullCommandDescription);
                                if (continueSending == false)
                                {
                                    break;
                                }
                                AnalyzeCommand4DataIntegrityNetworkIssues(currentfullCommandDescription, ref answer);//in case that data integrety needed the method will update the buffer.
                                answers.Add(answer);
                                receivedBytesPosition += commandOffset;//add the command offset
                                if (currentfullCommandDescription.Command == EnumProtocolCommands.CommandHangup)
                                {
                                    ContinueCommunication = false;//close the connection after sending the last answer.
                                }
                            }
                            deviceClientDetais.PacketData = null;

                            if(answers.Count > 0 )//in case of hangup the list is empty
                            {
                                Log.DebugFormat("finish handling buffer. total of {0} answers(not all answers will be sent).", answers.Count);
                                byte[] sendBuffer = null;
                                foreach (var answer in answers)//answers.ForEach(a => sendBuffer = Utilities.Combine(sendBuffer, a));
                                {
                                    if( sendBuffer == null && answer == null)
                                    {//first buff and the answer is null - skip it
                                        continue;
                                    }
                                    if(sendBuffer == null && answer != null)//first buffer need to use it and not combine
                                    {
                                        sendBuffer = answer;
                                    }
                                    else if(answer != null)//not first buffer - and answer not null
                                    {
                                        sendBuffer = Utilities.Combine(sendBuffer, answer);
                                    }                                    
                                }
                                
                                if(sendBuffer != null && sendBuffer.Length>0)
                                {
                                    Log.DebugFormat("sending {0} answers buffer size {1}", answers.Count, sendBuffer.Length);
                                    Write(deviceClientDetais, sendBuffer, EnumEncryptionType.TypeOne);
                                }
                                else
                                {
                                    Log.Warn($"Nothing will be sent (all answers null)");
                                }
                            }
                            //finish handling packet.need to reset the commands
                            deviceClientDetais.ExecuterObject.PacketReset();
                        }
                        else
                        {
                            Assemble(deviceClientDetais, ReceivedBytes, BytesRead, ref Length);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string errMessage = string.Format("ReceiverDeviceMessages for device {0} error:", deviceId);
                    Log.ErrorFormat(errMessage, ex);
                    throw new Exception(errMessage, ex);
                }
                finally
                {
                    Thread.Sleep(200);//wait for the server to get the last ack
                    if (DeviceClients.TryRemove(deviceId, out deviceClientDetais) == false)
                    {
                        Log.ErrorFormat("Fail to Remove Device client device-client listfor device id ", deviceId);
                    }
                }
            }            
        }

        /// <summary>
        /// analyze the buffer in case we need to generate network issue for that command
        /// </summary>
        /// <param name="fullCommandDescription">Full description of the command</param>
        /// <returns>If the client need to continue handling the command(send it to server) or not.</returns>
        private bool AnalyzeCommand4NetworkIssues(FullCommandDescription fullCommandDescription)
        {
            bool continueWithTheCommand = true;

            if(NetworkIssuesDescription != null && NetworkIssuesDescription is NetworkIssuesDescription)
            {
                var currentCommandKey = CommandBase.GenerateKeyFromCommands(fullCommandDescription.Command, fullCommandDescription.SubCommand);
                var networkIssuesCommandKey = CommandBase.GenerateKeyFromCommands(NetworkIssuesDescription.Command, NetworkIssuesDescription.SubCommand);

                if( String.Compare(currentCommandKey, networkIssuesCommandKey, true) == 0)//we are in the command need to be stopped
                {
                    Log.DebugFormat($"Command {currentCommandKey} will be interrupted due to network issues feature.");
                    continueWithTheCommand = false;
                    
                }                
            }
            return continueWithTheCommand;            
        }

        /// <summary>
        /// In case that data Integrity needed the method will update the buffer.
        /// </summary>
        /// <param name="currentfullCommandDescription"></param>
        /// <param name="answer"></param>
        private void AnalyzeCommand4DataIntegrityNetworkIssues(FullCommandDescription currentfullCommandDescription, ref byte[] answer)
        {
            if (NetworkIssuesDescription != null && NetworkIssuesDescription is NetworkIssuesDescriptionForDataIntegrity)
            {
                var currentCommandKey = CommandBase.GenerateKeyFromCommands(currentfullCommandDescription.Command, currentfullCommandDescription.SubCommand);
                var networkIssuesCommandKey = CommandBase.GenerateKeyFromCommands(NetworkIssuesDescription.Command, NetworkIssuesDescription.SubCommand);

                if (String.Compare(currentCommandKey, networkIssuesCommandKey, true) == 0 )                    
                {
                    var nidfdi = NetworkIssuesDescription as NetworkIssuesDescriptionForDataIntegrity;
                    var bufferOffset = (int)nidfdi.NetworkIssuesInPacket;
                    Log.Info($"Command {currentCommandKey} answer will changed due to network issues feature." +
                        $"(change from value {answer[bufferOffset]} to {nidfdi.DataIntegrityValue}.");

                    answer[bufferOffset] = nidfdi.DataIntegrityValue;
                }
            }
        }

        private int ParseCommand(DeviceClientDetais dcd, byte[] command_buff, ref FullCommandDescription fullCommandDescription, ref byte[] answer)
        {
            int     messagePosition = 0;

            fullCommandDescription.Command = (EnumProtocolCommands)PacketBuilder.GetByte(command_buff, ref messagePosition);
            fullCommandDescription.Size = (UInt16)PacketBuilder.GetInt16(command_buff, ref messagePosition);

            fullCommandDescription.SubCommand = CommandBase.EMPTY_SUB_COMMAND;
            if (IsSubCommandRequired(fullCommandDescription.Command))
            {
                fullCommandDescription.SubCommand = (EnumProtocolCommands)PacketBuilder.GetByte(command_buff, ref messagePosition);
            }

            try
            {
                Log.Debug($"cmd= {fullCommandDescription.Command} sub cmd = {fullCommandDescription.SubCommand} size {fullCommandDescription.Size}");
                bool continueSending = AnalyzeCommand4NetworkIssues(fullCommandDescription);
                if( continueSending == true)
                {
                    answer = dcd.ExecuterObject.ExecuteCommand(fullCommandDescription.Command, fullCommandDescription.Size, fullCommandDescription.SubCommand, command_buff, ref messagePosition);
                }
                
            }
            catch (Exception ex)
            {
                Log.Error("while try to answer command:", ex);
                throw;
            }
            //return messagePosition;
            int messageOffset = sizeof(byte) + sizeof(UInt16) + fullCommandDescription.Size;//command, sub command and the actual size
            return messageOffset;
        }

        /// <summary>
        /// Is Sub command Required
        /// </summary>
        /// <param name="command"></param>
        /// <returns>true if Is Sub command Required, else false</returns>
        private bool IsSubCommandRequired(EnumProtocolCommands command)
        {
            bool result = false;
            switch(command)
            {
                case EnumProtocolCommands.CommandRequestCurrentSettings:
                case EnumProtocolCommands.CommandPartialCodeLoad:
                    result = true;
                    break;
            }
            return result;
        }

        private void SendHello(NetworkStream stream, int deviceId)
        {
            byte[] buffer = new byte[Defines.SocketBufferSize];
            int position = 0;

            PacketBuilder.AddData(ref buffer, ref position, new byte[] { 0x05 });
            PacketBuilder.AddData(ref buffer, ref position, PacketBuilder.Hello);
            PacketBuilder.AddData(ref buffer, ref position, BitConverter.GetBytes(deviceId));

            stream.Write(buffer, 0, position);

        }

        protected void Collect(ref DeviceClientDetais Client, Byte[] ReceivedBytes, Int32 BytesRead)
        {
            // Info("Part of SMART message received");
            if (Client.PacketData == null)
            {
                Client.PacketData = new byte[BytesRead];
            }
            Buffer.BlockCopy(ReceivedBytes, 0, Client.PacketData, 0, BytesRead);
            Array.Resize(ref Client.PacketData, BytesRead);

            //Client.PacketData = new byte[BytesRead];
            //Buffer.BlockCopy(ReceivedBytes, 0, Client.PacketData, 0, BytesRead);
        }

        private void Assemble(DeviceClientDetais Client, Byte[] ReceivedBytes, Int32 BytesRead, ref Int32 Length)
        {
            if (Client.PacketData != null && Client.PacketData.Length > 0)
            {
                Int32 Pos = Client.PacketData.Length;
                Array.Resize(ref Client.PacketData, Client.PacketData.Length + BytesRead);

                // First copy the data to PacketData - so it will be kept in order
                Buffer.BlockCopy(ReceivedBytes, 0, Client.PacketData, Pos, BytesRead);

                // Than copy the data back to receivedBytes
                Buffer.BlockCopy(Client.PacketData, 0, ReceivedBytes, 0, Client.PacketData.Length);
                Length = Client.PacketData.Length;
            }
        }
        private int AddFrame(DeviceClientDetais deviceClientDetais, ref byte[] buffer, ref int pos, 
            EnumProtocolCommands command, int size, EnumProtocolCommands subCommand, byte[] data)
        {
            PacketBuilder.AddData(ref buffer, ref pos, (byte)command);
            PacketBuilder.AddData(ref buffer, ref pos, (ushort)size);
            PacketBuilder.AddData(ref buffer, ref pos, subCommand);
            PacketBuilder.AddData(ref buffer, ref pos, data);
            return pos;
        }

        private Stream _stream;
        
        /// <summary>
        /// init stream private object with the correct instance
        /// </summary>
        /// <param name="dcd"></param>
        /// <returns></returns>
        private Stream GetStreamForSending(DeviceClientDetais dcd)
        {
            if(_stream == null)
            {
                if (NetworkIssuesDescription != null && NetworkIssuesDescription.BPS > 0 )
                {
                    Log.Debug($"BPS = {NetworkIssuesDescription.BPS} . going to use Throttled Stream for network issues.");
                    var originalDestinationStream = dcd.TcpClientConnection.GetStream();
                    _stream = new LowBandwidthStream(originalDestinationStream, NetworkIssuesDescription.BPS);
                }
                else
                {
                    Log.Debug("going to use normal stream from TCP Connection.");
                    _stream = dcd.TcpClientConnection.GetStream();
                }
            }
            return _stream;
        }


        void Write(DeviceClientDetais dcd, byte[] data, EnumEncryptionType encryption)
        {
            if (dcd != null)
            {
                var conversationData = new ConversationData() { DeviceID = dcd.DeviceId, PacketID = (byte)dcd.Index, EncryptionLevel = encryption };
                byte[] buff = PacketBuilder.BuildPacket(conversationData, data, EnumEndPoint.Device, encryption);

                AnalyzeHeader4NetworkIssues(ref buff);

                var stream = GetStreamForSending(dcd);
                stream.Write(buff, 0, buff.Length);
                dcd.Index++;
            }
        }

        /// <summary>
        /// in case of network issue requierd to be generated the method will change the buffer as needed.
        /// </summary>
        /// <param name="buff"></param>
        /// <returns></returns>
        private void AnalyzeHeader4NetworkIssues(ref byte[] buff)
        {
            if( NetworkIssuesDescription!= null && 
                NetworkIssuesDescription.NetworkIssuesInPacket != EnumNetworkIssuesInPacket.None)
            {
                switch (NetworkIssuesDescription.NetworkIssuesInPacket)
                {
                    case EnumNetworkIssuesInPacket.Sync:
                    case EnumNetworkIssuesInPacket.Source:
                    case EnumNetworkIssuesInPacket.Destination:
                    case EnumNetworkIssuesInPacket.Port:
                    case EnumNetworkIssuesInPacket.Length:
                    case EnumNetworkIssuesInPacket.PacketID:
                    case EnumNetworkIssuesInPacket.Encryption:
                        GenerateNetworkIssuesBasedOnFixedOffset(NetworkIssuesDescription.NetworkIssuesInPacket, ref buff);
                        break;
                    case EnumNetworkIssuesInPacket.Data:
                        break;
                    case EnumNetworkIssuesInPacket.CRC32:
                        GenerateNetworkIssues4Crc(ref buff);
                        break;
                    default:
                        break;
                }

                
            }
        }

        private byte[] GenerateRandomByteBuffer(int size)
        {
            var randomBuffer = new byte[size];
            //can't use DataGenerator dll because circular referece.
            DataGeneratorFactory.Instance.RNG.NextBytes(randomBuffer);
            return randomBuffer;
        }

        /// <summary>
        /// this method will use the enum value as the offset in the buffer and change it to something else causing network issue.
        /// </summary>
        /// <param name="networkIssuesInPacket">the type of network issue need to be generate</param>
        /// <param name="buff">the buffer of the message</param>
        private void GenerateNetworkIssuesBasedOnFixedOffset(EnumNetworkIssuesInPacket networkIssuesInPacket, ref byte[] buff)
        {
            int networkIssueByteOffset = (int)NetworkIssuesDescription.NetworkIssuesInPacket;
            var randomBuffer = GenerateRandomByteBuffer(1);
            Log.Debug($"Replacing byte index {networkIssueByteOffset} from {buff[networkIssueByteOffset]} to {randomBuffer[0]}.");
            buff[networkIssueByteOffset] = randomBuffer[0];
        }

        /// <summary>
        /// use the last bytes in the buffer as the CRC and change it
        /// </summary>
        /// <param name="buff"></param>
        private void GenerateNetworkIssues4Crc(ref byte[] buff)
        {
            var randomBuffer = GenerateRandomByteBuffer(1);
            var crcOffset = buff.Length - 3;
            Log.Debug($"Replacing byte index {crcOffset} (as the crc offset) from {buff[crcOffset]} to {randomBuffer[0]}.");
            buff[crcOffset] = randomBuffer[0];
        }
    }

    public class FullCommandDescription
    {
        public EnumProtocolCommands Command { get; set; }
        public UInt16 Size { get; set; }
        public EnumProtocolCommands SubCommand { get; set; }
        public EnumFirmwareVersionTypes RequestedSwVersion { get; set; }
    }

    /// <summary>
    /// enumeration of all 
    /// </summary>
    public enum EnumNetworkIssuesInPacket
    {
        /// <summary>
        /// no network issue need to be generated
        /// </summary>
        None = -1,
        /// <summary>
        /// sync part of the header(bytes 0-7)
        /// </summary>
        Sync = 0,
        /// <summary>
        /// source part of the header (bytes 8-11)
        /// </summary>
        Source = 8,
        /// <summary>
        /// destinationpart of the header (bytes 12-15)
        /// </summary>
        Destination = 12,
        /// <summary>
        /// port part of the header (bytes 16-17)
        /// </summary>
        Port = 16,
        /// <summary>
        /// Length part of the header (bytes 18-19)
        /// </summary>
        Length = 18,
        /// <summary>
        /// Length part of the header (bytes 20)
        /// </summary>
        PacketID = 20,
        /// <summary>
        /// Encryption part of the header (bytes 21)
        /// </summary>
        Encryption = 21,
        /// <summary>
        /// Data part of the header (from byte 22 and up to 1024 bytes)
        /// </summary>
        Data,
        /// <summary>
        /// CRC32 part of the header (4 last bytes in the buffer)
        /// </summary>
        CRC32
    };

    /// <summary>
    /// 
    /// </summary>
    public enum NetworkIssueInFrame
    {
        /// <summary>
        /// change the byte that represent the command value
        /// </summary>
        CommandOffset = 0,
        /// <summary>
        /// change the 2 bytes that represent the size value
        /// </summary>
        SizeOffset = 1
    }

    public class NetworkIssuesDescription
    {
        public EnumProtocolCommands Command { get; set; }
        public EnumProtocolCommands SubCommand { get; set; }

        /// <summary>
        /// Byte per second for support low bandwidth
        /// switch( user.Membership )
        ///   {
        ///       case MembershipLevel.Bronze:
        ///           bps = 51200;
        ///           break;
        ///
        ///       case MembershipLevel.Silver:
        ///           bps = 102400;
        ///           break;
        ///
        ///       case MembershipLevel.Gold:
        ///           bps = 153600;
        ///           break;
        ///
        ///       case MembershipLevel.Platina:
        ///           bps = ThrottledStream.Infinite;
        ///           break;
        ///   }
        /// </summary>
        public long BPS { get; set; }

        public EnumNetworkIssuesInPacket NetworkIssuesInPacket { get; set; }

        public NetworkIssuesDescription()
        {
            NetworkIssuesInPacket = EnumNetworkIssuesInPacket.None;
        }
    }

    /// <summary>
    /// specific implementation for data integrity issues.
    /// </summary>
    public class NetworkIssuesDescriptionForDataIntegrity : NetworkIssuesDescription
    {
        /// <summary>
        /// in case of data Integrity in frame level is needed this is the value that will be set in the command insted of the real value
        /// </summary>
        public byte DataIntegrityValue { get; set; }
        /// <summary>
        /// the data in the frame that need to be changed
        /// </summary>
        public NetworkIssueInFrame NetworkIssueInFrameData { get; set; }
    }

    interface ISerializeObject
    {
        byte[] SerializeObject();
    }

    public class DeviceIdStatus : ISerializeObject
    {
        public int MTD_ID { get; set; }
        public ushort Status { get; set; }

        public byte[] SerializeObject()
        {
            byte[] ret_buff = Utilities.Combine(BitConverter.GetBytes(MTD_ID), BitConverter.GetBytes(Status));
            return ret_buff;
        }
    }

    public class RequestALLIDsResponse : ISerializeObject
    {
        public RequestALLIDsResponse()
        {
            DevicesIdStatus = new List<DeviceIdStatus>();
        }

        public List<DeviceIdStatus> DevicesIdStatus { get; set; }
        public byte[] SerializeObject()
        {
            byte byteCount = (byte)DevicesIdStatus.Count;
            //byte[] ret_buff = BitConverter.GetBytes(byteCount);
            byte[] ret_buff = new byte[] { byteCount };

            foreach (var device in DevicesIdStatus)
            {
                ret_buff = Utilities.Combine(ret_buff, device.SerializeObject());
            }
            return ret_buff;
        }
    }
}
