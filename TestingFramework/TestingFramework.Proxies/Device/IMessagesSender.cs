﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device
{
    interface IMessagesSender
    {
        void Init();

        void SendHello();

        void SendAnalyzedFrames();
    }
}
