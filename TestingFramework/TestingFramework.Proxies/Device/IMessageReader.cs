﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies.Device
{
    interface IMessageReader
    {
        void Read();

        bool CallEnd();
    }
}
