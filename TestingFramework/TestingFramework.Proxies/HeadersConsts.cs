﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.Proxies
{
    public class HeadersConsts
    {
        public static readonly string Header_Content_Type_Application_Json = "application/json; charset=utf-8";
    }

    public class RequestHeader
    {
        public static readonly string RequestHeader_MobileID = "MobileID";
        public static readonly string RequestHeader_AppID = "AppID";
        public static readonly string RequestHeader_AppVersion = "AppVersion";
        public static readonly string RequestHeader_STT = "STT";
        public static readonly string RequestHeader_SessionToken = "SESSION_TOKEN";
    }
}
