﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestingFramework.IntegrationTests.Devices
{
    public class GpsDeviceTestsAssemblyActions : BaseUnitTest
    {
        #region Assembly methods
        /// <summary>
        /// The Assembly level methods must be implemented in the same class of the tests.
        /// </summary>
        /// <param name="testContext"></param>
        [AssemblyInitialize]
        public static void TestingFrameworkAssemblyInit(TestContext testContext)
        {
            BaseUnitTest.AssemblyInit(testContext);
        }
        /// <summary>
        /// cleanup method for the testing framework assembly
        /// </summary>
        [AssemblyCleanup]
        public static void TestingFrameworkAssemblyCleanup()
        {
            BaseUnitTest.AssemblyCleanup();
        }
        #endregion Assembly methods
    }
}
