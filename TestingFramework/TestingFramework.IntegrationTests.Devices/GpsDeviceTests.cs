﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Categories;
using TestBlocksLib.DevicesBlocks;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using AssertBlocksLib.DevicesAsserts;
using System.Text;
using TestingFramework.UnitTests.ConfigurationSvcTests;
using LogicBlocksLib.ConfigurationBlocks;
using TestBlocksLib.ConfigurationBlocks;
using LogicBlocksLib.Events;
using System.ComponentModel;
using TestBlocksLib.EventsBlocks;
using TestBlocksLib.APIExtensions;
using TestBlocksLib;
using LogicBlocksLib.ScheduleBlocks;
using TestBlocksLib.ScheduleBlocks;
using System;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
#endregion

namespace TestingFramework.IntegrationTests.Devices
{
    [TestClass]
    [DeploymentItem("ConfigurationFiles\\TestSettings.testsettings", "ConfigurationFiles")]
    [DeploymentItem("ConfigurationFiles\\log4net.config.xml", "ConfigurationFiles")]
    [DeploymentItem("ConfigurationFiles", "ConfigurationFiles")]
    [DeploymentItem("EncryptionP8.dll")]
    public class GpsDeviceTests : BaseUnitTest
    {
        public int EquipmentID4Testing { get; set; }

        public int OffenderID4Testing { get; set; }

        public string OldDeviceVersion { get; set; }


        public const string EquipmentID4Testing_Token = "EquipmentID4Testing";

        public GpsDeviceTests()
        {
            //read Reciver SN from config in case of working with specific device
            var ReceiverSN = ConfigurationManager.AppSettings[EquipmentID4Testing_Token];            

            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            var errorMessage = string.Empty;

            if(string.IsNullOrEmpty(ReceiverSN))
            {//get first active 2Piece offender
                GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active };
                GetOffendersListRequest.ProgramType = new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece };
                GetOffendersListRequest.Limit = 1;
                errorMessage = "no Two_Piece active offenders in the system";
            }
            else
            {//get the offender that related to that receiver
                GetOffendersListRequest.Receiver = new Offenders0.EntStringParameter()
                    { Operator = Offenders0.EnmStringOperator.Equal, Value = ReceiverSN };
                errorMessage = $"no active offender for receiver Device ID {ReceiverSN}";
            }

            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);
            if(GetOffendersResponse.OffendersList.Length <= 0)
            {
                throw new Exception(errorMessage);
            }

            OffenderID4Testing = GetOffendersResponse.OffendersList[0].ID;
            EquipmentID4Testing = int.Parse(GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN);
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_FirstMiddleLastName()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                
                 AddBlock<CommandUserNameAssertBlock>().
                
                 ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_FirstMiddleLastNameEncodingASCII()
        {
            int oldVersionOffenderId = 1346;
            int oldVersionEquipmentId = 35620342;

            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(oldVersionOffenderId).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(oldVersionEquipmentId).
                    SetProperty(x => x.Encoding).WithValue(Encoding.ASCII).
                    SetProperty(x => x.DeviceApplication).WithValue("V5.12.9.0").
                AddBlock<CommandUserNameAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_UTCOffsets()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSetUTCOffsetsAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_ChargeLow()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<Command3VchgLowAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_OffenderDemographicID()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSetOffenderDemographicIDAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_NormalGPSSaveInterval()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGPSNormalSaveAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_AlarmGPSSaveInterval()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGPSAlarmSaveAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BacklightOnTime()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandBacklightOnTimeAssertBlock>().
                ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PiezoType()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPiezoTypeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BraceletID()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSetBraceletSerialNumberAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BraceletNoRxGone()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandBraceletGoneTimeAssertBlock>().
                ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BraceletRange()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandBraceletRangeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_IgnoreBraceletErrors()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandIgnoreBraceletErrsAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MotionThreshold()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMotionThresholdAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_ContactMethod()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandContactMethodAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_ChargeKamikaze()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandChargeKamikazeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_InChargerBumpLeeway()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandInChargerBumpLeewayAssertBlock>().
                ExecuteFlow();
        }




        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BraceletGonePreGrace()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandBraceletGonePreGraceAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_NoMotionMinutes()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandNoMotionMinutesAssertBlock>().
                ExecuteFlow();
        }

        

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_CurfewPreGrace()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandCurfewPreGraceAssertBlock>().
                ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_NAVElevationMask()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandNAVElevationMaskAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_CommandNAVPowerMask()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandNAVPowerMaskAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_SoftwareMotionCheckDuration()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSoftwareMotionCheckDurationAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_SoftwareMotionCheckInterval()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSoftwareMotionCheckIntervalAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MinimumBraceletBatteryVoltage()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMinimumBraceletBatteryVoltageAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_ActivationBraceletBatteryVoltage()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandActivationBraceletBatteryVoltageAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_SystemType()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSystemTypeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_CallBackInterval()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandCallBackIntervalAssertBlock>().
                ExecuteFlow();
        }
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PrimaryGPRSIPAddress()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPrimaryGPRSIPAddressAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_SecondaryGPRSIPAddress()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSecondaryGPRSIPAddressAssertBlock>().
                ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MenuPasswords()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMenuPasswordsAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BitOptions()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandBitOptionsAssertBlock>().
                ExecuteFlow();
        }
  
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MaxInChargerTHPE()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMaxInChargerTHPEAssertBlock>().
                ExecuteFlow();
        }      
  
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_ReportedSystemType()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandReportedSystemTypeAssertBlock>().
                ExecuteFlow();
        }  
    
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_LACCIDStorageControl()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandLACCIDStorageControlAssertBlock>().
                ExecuteFlow();
        }       
    
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_DaylightSavingTime()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandDaylightSavingTimeAssertBlock>().
                ExecuteFlow();
        }       
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MonthDayDateFormat()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMonthDayDateFormatAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MonthDayYearFormat()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMonthDayYearFormatAssertBlock>().
                ExecuteFlow();
        }  

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_BraceletType()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandBraceletTypeAssertBlock>().
                ExecuteFlow();
        }
        
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMData0APN()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGSMData0APNAssertBlock>().
                ExecuteFlow();
        }  
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PriPhoneNumber()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPriPhoneNumberAssertBlock>().
                ExecuteFlow();
        }  
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_TamperThreshold()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandTamperThresholdAssertBlock>().
                ExecuteFlow();
        } 
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_OperationMode()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandOperationModeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_NoLBSTimer()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandNoLBSTimerAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_EnableFeatures()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandEnableFeaturesAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_MonitorStartEndDateTime()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandMonitorStartEndDateTimeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMData0User()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGSMData0UserAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMData0UserOldVersion()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.DeviceApplication).WithValue("V5.12.9.0").
                    SetProperty(x => x.Encoding).WithValue(Encoding.ASCII).
                AddBlock<CommandGSMData0UserAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMData0Password()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGSMData0PasswordAssertBlock>().
                ExecuteFlow();
        }     

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMData1APN()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGSMData1APNAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMDataPrimaryAPN()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGSMDataPrimaryAPNAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_GSMDataToggleTime()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandGSMDataToggleTimeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_LBSMode()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandLBSModeAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PrimaryPLGIPAddress()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPrimaryPLGIPAddressAssertBlock>().
                ExecuteFlow();
        }
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_SecondayPLGIPAddress()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandSecondayPLGIPAddressAssertBlock>().
                ExecuteFlow();
        }
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_LBSDecisionTime()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandLBSDecisionTimeAssertBlock>().
                ExecuteFlow();
        }
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PLGSamplingRate()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPLGSamplingRateAssertBlock>().
                ExecuteFlow();
        }
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PLGRequestTimeout()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPLGRequestTimeoutAssertBlock>().
                ExecuteFlow();
        }
        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_UnitSIMNumber()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandUnitSIMNumberAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_ConfigurationChanges_PhoneNumbers()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CommandPhoneNumbersAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_EventsGeneration_SendHWEvent()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.SendHwEvents).WithValue(true).
                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get events request").
                    SetProperty(x => x.OffenderID).WithValue((int)OffenderID4Testing).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.SortDirection).WithValue(ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.Severity).WithValue(new Events0.EnmEventSeverity?(Events0.EnmEventSeverity.Alarm) ).
                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").
                AddBlock<SendHWEventAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_EventsGeneration_VerifyRules()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    BeginTransaction("bla").EndTransaction("bla").
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.SendHwEvents).WithValue(true).

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).WithValue((int)OffenderID4Testing).
                    SetProperty(x => x.VersionNumber).WithValue(ConfigurationSvcTests.CURRENT_VERSION_NUMBER).
                
                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<GetMetadataForGPSRulesTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Meta data For GPS Rules").
                
                AddBlock<VerifyRulesAssertBlock>().
                
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_EventsGeneration_VerifyAGPSFile()
        {
            uint deviceAGPSFileAgeOffsetMinutes = 125 * 60;//125 Hours

            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    BeginTransaction("Get Offenders Transaction").EndTransaction("Get Offenders Transaction").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.SendHwEvents).WithValue(true).
                    SetProperty(x => x.AGPSSupport).WithValue(true).
                    SetProperty(x => x.DeviceAGPSFileAgeOffsetMinutes).WithValue(deviceAGPSFileAgeOffsetMinutes).
                AddBlock<VerifyAGPSFileAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_SwDownload_VerifySwDownload()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderID4Testing).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<InsertMtdActionTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.DeviceID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.Action).WithValue("\'MTD_APP\'").
                    SetProperty(x => x.CommandNumber).WithValue(14).
                    SetProperty(x => x.CommandParam).WithValue("\'V5.12.13.32\'").
                    SetProperty(x => x.SoftwareType).WithValue(2).
                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(60).
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.SendHwEvents).WithValue(true).
                AddBlock<VerifySwDownloadAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_Schedule_VerifySchedualList()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetScheduleRequestBlock>().
                    SetDescription("Get schedule request for delete current").
                    SetProperty(x => x.EntityID).WithValue((int)OffenderID4Testing).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddYears(1)).
                    SetProperty(x => x.Version).WithValue(-1).//Current
                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetName("GetScheduleTestBlock_Current").
                    SetDescription("Get schedule test block").
                AddBlock<DeleteTimeFrameByListTestBlock>().UsePreviousBlockData().
                    SetDescription("delete all time frames for the offender.").
                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.Version).WithValue(-1).//current
                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").
                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Parse("May 31 2019 9:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Parse("May 31 2019 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("GetZonesByEntityIDTestBlock").
                            FromDeepProperty<GetZonesByEntityIDTestBlock>(x => x.GetZonesByEntityIDResponse.ZoneList[0].ID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).
                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).WithValue((int)OffenderID4Testing).
                
                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                AddBlock<CreateEntMsgGetScheduleRequestBlock>().
                    SetDescription("Get schedule request for verification").
                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                AddBlock<VerifyScheduleAssertBlock>().
                ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Tests)]
        public void CommBox_Actions_SendMessage()
        {
            TestingFlow.
                AddBlock<CreateEntMsgSendMessageRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).WithValue((int)OffenderID4Testing).
                AddBlock<SendMessageRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").
                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(60).
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EquipmentID).WithValue(EquipmentID4Testing).
                    SetProperty(x => x.SendHwEvents).WithValue(true).
                AddBlock<VerifyActionsSendMessageAssertBlock>().
                ExecuteFlow();
        }
    }
}

