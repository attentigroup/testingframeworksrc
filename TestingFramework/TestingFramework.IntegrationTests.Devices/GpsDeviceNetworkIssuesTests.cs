﻿using System;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using AssertBlocksLib.DevicesAsserts;
using CommBox.Infrastructure;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.OffendersBlocks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
#endregion



namespace TestingFramework.IntegrationTests.Devices
{
    [TestClass]
    [DeploymentItem("ConfigurationFiles\\TestSettings.testsettings", "ConfigurationFiles")]
    [DeploymentItem("ConfigurationFiles\\log4net.config.xml", "ConfigurationFiles")]
    [DeploymentItem("ConfigurationFiles", "ConfigurationFiles")]
    [DeploymentItem("EncryptionP8.dll")]
    public class GpsDeviceNetworkIssuesTests : BaseUnitTest
    {
        public const string GpsDeviceNetworkIssues_Commands_CSV_File = @"GpsDeviceNetworkIssues_Commands.csv";
        public const string GpsDeviceNetworkIssues_DataIntegrity_CSV_File = @"GpsDeviceNetworkIssues_DataIntegrity.csv";

        public const string GpsDeviceNetworkIssues_LineCounter_COL_NAME = "LineCounter";
        public const string GpsDeviceNetworkIssues_Command_COL_NAME = "Command";
        public const string GpsDeviceNetworkIssues_SubCommand_Key_COL_NAME = "SubCommand";
        public const string GpsDeviceNetworkIssues_OffenderId_COL_NAME = "OffenderId";
        public const string GpsDeviceNetworkIssues_ReceiverSN_COL_NAME = "ReceiverSN";

        public const string GpsDeviceNetworkIssues_DataIntegrityValue_COL_NAME = "DataIntegrityValue";
        public const string GpsDeviceNetworkIssues_NetworkIssueInFrame_COL_NAME = "NetworkIssueInFrame";
        


        [ClassInitialize]
        public static void GpsDeviceNetworkIssuesTests_InitForLoadTest(TestContext testContext)
        {
            GenerateCommandsCsvFile(GpsDeviceNetworkIssues_Commands_CSV_File, GpsDeviceNetworkIssues_DataIntegrity_CSV_File);
        }

        /// <summary>
        /// generate file with the names of the commands from the classes that inherite from ICommand interface.
        /// We use ICommand interface because this interface holds the EnumProtocolCommands Command property.
        /// </summary>
        /// <param name="filePath">the file path to write</param>
        private static void GenerateCommandsCsvFile(string filePath, string dataIntegrityFile)
        {
            bool generateFileForDataIntegrity = false;
            try
            {                
                Log.Info($"going to retrive offenders for load test and save to file {filePath}.");
                if( !string.IsNullOrEmpty(dataIntegrityFile))
                {
                    generateFileForDataIntegrity = true;
                    Log.Info($"going to retrive offenders for data integrity test and save to file {dataIntegrityFile}.");
                }
                var GetOffendersListRequest =
                        new Offenders0.EntMsgGetOffendersListRequest()
                            {
                                ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active },
                                ProgramType = new Offenders0.EnmProgramType[] {Offenders0.EnmProgramType.Two_Piece}
                            };

                var GetOffendersResponse = OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);
                var offendersCount = GetOffendersResponse.OffendersList == null ? 0 : GetOffendersResponse.OffendersList.Length;

                Log.Info($"Finish load {GetOffendersResponse.OffendersList.Length} offenders to file {filePath}");

                Log.Info("start load all data generators from DLL");

                var     sbData               = new System.Text.StringBuilder();
                var     sbData4dataIntegrity = new System.Text.StringBuilder();
                
                var     assemblyName            = "TestingFramework.Proxies";// ConfigurationManager.AppSettings[DataGeneratorsAssemblyNameConfigkey];
                var     nameSpace               = "TestingFramework.Proxies.Device.CommandsHandler.Commands";// ConfigurationManager.AppSettings[DataGeneratorsNamespaceConfigKey];
                int     classLoaded             = 0;
                int     currentOffenderIndex    = 0;

                //header
                sbData.AppendLine(
                    $"{GpsDeviceNetworkIssues_LineCounter_COL_NAME}, { GpsDeviceNetworkIssues_Command_COL_NAME}, " +
                    $"{GpsDeviceNetworkIssues_SubCommand_Key_COL_NAME},{GpsDeviceNetworkIssues_OffenderId_COL_NAME}, " +
                    $"{GpsDeviceNetworkIssues_ReceiverSN_COL_NAME}");

                if( generateFileForDataIntegrity)//header for data integrity
                {
                    sbData4dataIntegrity.AppendLine(
                        $"{GpsDeviceNetworkIssues_LineCounter_COL_NAME}, { GpsDeviceNetworkIssues_Command_COL_NAME}, {GpsDeviceNetworkIssues_SubCommand_Key_COL_NAME}, " +
                        $"{GpsDeviceNetworkIssues_OffenderId_COL_NAME}, {GpsDeviceNetworkIssues_ReceiverSN_COL_NAME}, {GpsDeviceNetworkIssues_DataIntegrityValue_COL_NAME}," +
                        $"{GpsDeviceNetworkIssues_NetworkIssueInFrame_COL_NAME}");
                }

                Log.DebugFormat("Load commands data from dll {0} using name space {1}", assemblyName, nameSpace);
                AppDomain.CurrentDomain.Load(assemblyName);
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                var assmebly = assemblies.First(x => x.FullName.StartsWith(assemblyName));
                //get the classes that implement ICommand interface
                var classesTypes = assmebly.GetTypes().Where(type => 
                    type.IsClass && type.Namespace != null && 
                    type.Namespace.Contains(nameSpace) &&
                    type.GetInterfaces().Contains(typeof(Proxies.Device.CommandsHandler.ICommand)));
                int lineCounter = 0;
                foreach (var classType in classesTypes)
                {
                    try
                    {
                        lineCounter++;
                        var CompilerGenAttr = classType.GetCustomAttributes(typeof(CompilerGeneratedAttribute), true);
                        if (CompilerGenAttr.Length > 0)
                        {
                            Log.DebugFormat("classType {0} will not load because because this is not real class", classType.ToString());
                            continue;
                        }

                        System.Reflection.PropertyInfo[] properties = classType.GetProperties();

                        foreach (var property in properties)
                        {
                            Log.DebugFormat($"class type {classType.Name} property {property.Name}");
                        }

                        var iCommand = assmebly.CreateInstance(classType.FullName) as Proxies.Device.CommandsHandler.ICommand;

                        if (iCommand.Command == EnumProtocolCommands.Command3VchgKamikaze ||
                            iCommand .Command == EnumProtocolCommands.Command3VchgLow)
                            continue;

                        var offender = GetOffendersResponse.OffendersList[currentOffenderIndex++];
                        var offenderId = offender.ID;
                        var receiverSN = offender.EquipmentInfo.ReceiverSN;
                        //move to next offender or start again.
                        if (currentOffenderIndex >= GetOffendersResponse.OffendersList.Length)
                        {
                            currentOffenderIndex = 0;
                        }
                        var line = $"{lineCounter}, {iCommand.Command}, {iCommand.SubCommand}, {offenderId}, {receiverSN}";
                        sbData.AppendLine(line);
                        Log.Debug($"Add command {line} to the file");
                        if( generateFileForDataIntegrity)
                        {
                            for (byte div = 0; div < byte.MaxValue; div++)
                            {
                                NetworkIssueInFrame[] networkIssueInFrameList = (NetworkIssueInFrame[])Enum.GetValues(typeof(NetworkIssueInFrame));
                                foreach (var niif in networkIssueInFrameList)
                                {
                                    var dataIntegrityline = $"{line},{div},{niif}";
                                    sbData4dataIntegrity.AppendLine(dataIntegrityline);
                                    Log.Debug($"Add data integrity line {dataIntegrityline} to the file");
                                }                                
                            }
                        }
                        classLoaded++;
                    }
                    catch (Exception exception)
                    {
                        Log.Error($"While processing class {classType.Name}, error message:{exception.Message}");
                    }

                }
                Log.Info("Finish load all commands from DLL");

                //write to file
                Log.Info("start writing buffer to file");
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
                System.IO.File.WriteAllText(filePath, sbData.ToString());
                Log.Info($"Finish write {classLoaded} commands to file {filePath}");

                if ( generateFileForDataIntegrity)
                {
                    Log.Info($"start writing buffer to file {dataIntegrityFile}");
                    if (System.IO.File.Exists(dataIntegrityFile))
                        System.IO.File.Delete(dataIntegrityFile);
                    System.IO.File.WriteAllText(dataIntegrityFile, sbData4dataIntegrity.ToString());
                    Log.Info($"Finish write {classLoaded} commands to file {dataIntegrityFile}");
                }
            }
            catch (System.Exception exception)
            {
                Log.ErrorFormat($"GenerateOffendersIdCsvFile for path {filePath} end with error:{exception.Message}");
                Log.DebugFormat($"GenerateOffendersIdCsvFile for path {filePath} end with error:{exception}");
                if( generateFileForDataIntegrity)
                {
                    Log.ErrorFormat($"GenerateOffendersIdCsvFile for path {dataIntegrityFile} end with error:{exception.Message}");
                    Log.DebugFormat($"GenerateOffendersIdCsvFile for path {dataIntegrityFile} end with error:{exception}");

                }
            }
        }

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + GpsDeviceNetworkIssues_Commands_CSV_File,
            "GpsDeviceNetworkIssues_Commands#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_DisconnectInAllCallStages()
        {
            DataRow                     currentDataRow = TestContext.DataRow;
            EnumProtocolCommands        command = CommandBase.EMPTY_SUB_COMMAND;
            EnumProtocolCommands        subCommand = CommandBase.EMPTY_SUB_COMMAND;
            long                        OffenderID = long.MaxValue;

            Enum.TryParse(currentDataRow[GpsDeviceNetworkIssues_Command_COL_NAME].ToString(), out command);
            Enum.TryParse(currentDataRow[GpsDeviceNetworkIssues_SubCommand_Key_COL_NAME].ToString(), out subCommand);

            string EquipmentSerialNumber = currentDataRow[GpsDeviceNetworkIssues_ReceiverSN_COL_NAME].ToString();
            long.TryParse(currentDataRow[GpsDeviceNetworkIssues_OffenderId_COL_NAME].ToString(), out OffenderID);


            TestingFlow.
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().
                    SetProperty(x => x.OffenderID).WithValue(OffenderID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(EquipmentSerialNumber).
                    SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { Command = command, SubCommand = subCommand }).
                ExecuteFlow();
        }

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + GpsDeviceNetworkIssues_DataIntegrity_CSV_File,
            "GpsDeviceNetworkIssues_DataIntegrity#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_DataIntegrityInFrameLevel()
        {
            DataRow                 currentDataRow = TestContext.DataRow;
            EnumProtocolCommands    command = CommandBase.EMPTY_SUB_COMMAND;
            EnumProtocolCommands    subCommand = CommandBase.EMPTY_SUB_COMMAND;
            NetworkIssueInFrame     networkIssueInFrameData;
            int                     lineCounter = 0;
            long                    OffenderID = long.MaxValue;
            byte                    div = byte.MinValue;

            Enum.TryParse(currentDataRow[GpsDeviceNetworkIssues_Command_COL_NAME].ToString(), out command);
            Enum.TryParse(currentDataRow[GpsDeviceNetworkIssues_SubCommand_Key_COL_NAME].ToString(), out subCommand);
            Enum.TryParse(currentDataRow[GpsDeviceNetworkIssues_NetworkIssueInFrame_COL_NAME].ToString(), out networkIssueInFrameData);

            string EquipmentSerialNumber = currentDataRow[GpsDeviceNetworkIssues_ReceiverSN_COL_NAME].ToString();
            long.TryParse(currentDataRow[GpsDeviceNetworkIssues_OffenderId_COL_NAME].ToString(), out OffenderID);

            byte.TryParse(currentDataRow[GpsDeviceNetworkIssues_DataIntegrityValue_COL_NAME].ToString(), out div);
            int.TryParse(currentDataRow[GpsDeviceNetworkIssues_LineCounter_COL_NAME].ToString(), out lineCounter);
            
            Log.Info($"DataIntegrityInFrameLevel line {lineCounter} cmd = {command}, subCmd = {subCommand}, DIV {div}, Network Issue In Frame Data = {networkIssueInFrameData}");

            TestingFlow.
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().
                    SetProperty(x => x.OffenderID).WithValue(OffenderID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(EquipmentSerialNumber).
                    SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescriptionForDataIntegrity()
                        { Command = command, SubCommand = subCommand , DataIntegrityValue = div, NetworkIssueInFrameData = networkIssueInFrameData}).
                ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_LowBandwidth()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { BPS = 100 }).
                AddBlock<CommandUserNameAssertBlock>().
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderSync()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.Sync }).
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderSource()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.Source }).
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderDestination()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.Destination }).
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderPort()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.Port }).
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderLength()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.Length }).
            ExecuteFlow();
        }


        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderPacketID()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.PacketID }).
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderEncryption()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.Encryption }).
            ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Devices_Network_Issues_Tests)]
        public void CommBox_NetworkIssues_PacketHeaderCRC32()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by program status and type").
                    SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.NetworkIssuesDescription).WithValue(new NetworkIssuesDescription() { NetworkIssuesInPacket = EnumNetworkIssuesInPacket.CRC32 }).
            ExecuteFlow();
        }
    }
}
