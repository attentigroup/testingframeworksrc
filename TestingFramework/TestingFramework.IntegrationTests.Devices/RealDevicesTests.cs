﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using OffendersAction_1 = TestingFramework.Proxies.EM.Interfaces.OffenderActions3_10;

using TestingFramework.Proxies.API.Configuration;
using TestingFramework.Proxies.API.OffenderActions;
using Common.Categories;
using Racelogic.LabSatAPI;
using System.IO.Ports;
using System.Linq;
using TestingFramework.Proxies.Device;
using CommBox.Infrastructure;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using static TestingFramework.IntegrationTests.Devices.SerialPortEx;
using System.IO;
using log4net;
using System.Reflection;
#endregion


namespace TestingFramework.IntegrationTests.Devices
{
    /// <summary>
    /// implementation of tests for real devices to simulate stress states.
    /// </summary>
    [TestClass]
    [DeploymentItem("ConfigurationFiles\\TestSettings.testsettings", "ConfigurationFiles")]
    [DeploymentItem("ConfigurationFiles\\log4net.config.xml", "ConfigurationFiles")]
    [DeploymentItem("ConfigurationFiles", "ConfigurationFiles")]
    public class RealDevicesTests : BaseUnitTest
    {
        //tokens
        public static string PortName_Token = "PortName";
        public static string BaudRate_Token = "BaudRate";
        public static string Parity_Token = "Parity";
        public static string DataBits_Token = "DataBits";
        public static string StopBits_Token = "StopBits";

        public static string SMSTrigger_Token = "SMSTrigger";
        public static string SmsGatewayAppFolder_Token = "SmsGatewayAppFolder";
        public static string SmsGatewayAppExe_Token = "SmsGatewayAppExe";
        public static string SmsGatewayCommandPattern_Token = "SmsGatewayCommandPattern";
        public static string SMSContent_Token = "SMSContent";
        public static string EquipmentPhoneNumber_Token = "EquipmentPhoneNumber";

        public static string ArduinoClientAddress_Token = "ArduinoClientAddress";
        public static string SetGsmONPath_Token = "Set GSM ON";
        public static string SetGsmOFFPath_Token = "Set GSM OFF";

        public static string SetBeaconONPath_Token = "Set Beacon ON";
        public static string SetBeaconOFFPath_Token = "Set Beacon OFF";

        public static string labSatIpAddress_IP_ADDRESS_Token = "labSatIpAddress_IP_ADDRESS";
        public static string labSatIpFileName_Token = "labSatIpFileName";

        private string[] SmsTriggers { get; set; }

        private static HttpClient _client;

        /// <summary>
        /// HTTP Client for arduino
        /// </summary>
        public static HttpClient ArduinoClient
        {
            get
            {
                if (_client == null)
                {
                    _client = new HttpClient();
                    string uriString = ConfigurationManager.AppSettings[ArduinoClientAddress_Token]; 
                    if( string.IsNullOrEmpty(uriString) == true)
                    {
                        throw new Exception($"Arduino Client Address empty. please value to key named {ArduinoClientAddress_Token} ( for example http://10.101.10.145:8098/ )");
                    }
                    _client.BaseAddress = new Uri(uriString);
                }
                return _client;
            }
            set { _client = value; }
        }

        private static SerialPortEx SerialPortReaderObj = null;

        /// <summary>
        /// turn on and off the GSM based on the last update time of the call at the offender.
        /// </summary>
        [TestMethod]
        [TestCategory(CategoriesNames.Real_Devices_Tests_Load)]
        public void MissedCallByTurnRfOff()
        {
            //read SMS related data
            SmsTriggers = ConfigurationManager.AppSettings[SMSTrigger_Token].Split(',');            

            //set up serial port
            string portName = ConfigurationManager.AppSettings[PortName_Token];
            int baudRate = int.Parse(ConfigurationManager.AppSettings[BaudRate_Token]);//< add key = "BaudRate" value = "57600" />
            Parity parity = (Parity)Enum.Parse(typeof(Parity), ConfigurationManager.AppSettings[Parity_Token].ToString());//   < add key = "Parity" value = "None" />
            int dataBits = int.Parse(ConfigurationManager.AppSettings[DataBits_Token]);//      < add key = "DataBits" value = "8" />
            StopBits stopBits = (StopBits)Enum.Parse(typeof(StopBits), ConfigurationManager.AppSettings[StopBits_Token]);//         < add key = "StopBits" value = "One" />

            SerialPortReaderObj = new SerialPortEx(portName, baudRate, parity, dataBits, stopBits);
            SerialPortReaderObj.LinesReceived += new SerialPortEx.SerialPortReaderDataReceivedEventHandler(LinesReceived);

            try
            {
                SerialPortReaderObj.Start();
                //send log print f commands
                uint currentMask = 0xffffffff;
                SendLogPrintfCommand(ushort.MaxValue, (byte)LogPrintFDebugLevel.Maximum, currentMask, SerialPortReaderObj);
            }
            catch (Exception e)
            {
                Log.Error($"Unable to open port. error: {e.Message}");
                throw;
            }

            ArduinoClient.DefaultRequestHeaders.Accept.Clear();
            ArduinoClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            Log.Info($"Base address for arduino {ArduinoClient.BaseAddress}");

            //read Reciver SN from config in case of working with specific device
            var ReceiverSN = ConfigurationManager.AppSettings[GpsDeviceTests.EquipmentID4Testing_Token];
            Log.Info($"ReceiverSN {ReceiverSN }");

            //read Path for commands
            var SetGsmOnPath = ConfigurationManager.AppSettings[SetGsmONPath_Token];
            var SetGsmOffPath = ConfigurationManager.AppSettings[SetGsmOFFPath_Token];

            var SetBeaconOnPath = ConfigurationManager.AppSettings[SetBeaconONPath_Token];
            var SetBeaconOffPath = ConfigurationManager.AppSettings[SetBeaconOFFPath_Token];

            var LastUploadDate = DateTime.Now;
            Offenders0.EntMsgGetOffendersResponse GetOffendersResponse = null;

            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            var errorMessage = string.Empty;
            //get the offender that related to that receiver
            GetOffendersListRequest.Receiver = new Offenders0.EntStringQueryParameter() { Operator = Offenders0.EnmSqlOperatorStr.Equal, Value = ReceiverSN };
            Proxies.API.Offenders.OffendersProxy_1 OffendersProxy = null;
            try
            {
                OffendersProxy = Proxies.API.Offenders.OffendersProxy_1.Instance;
                
            }
            catch (Exception exception)
            {
                Log.Error($"Unable to connect to web service.error: { exception.Message}");
                throw;
            }

            try
            {
                GetOffendersResponse = OffendersProxy.GetOffenders(GetOffendersListRequest);
                if( GetOffendersResponse.OffendersList.Length == 0)
                {
                    throw new Exception("Offender list length = 0");
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Error while try to get offender for SN {ReceiverSN}. Error: {exception.Message}");
                throw;
            }
                        
            LastUploadDate = GetOffendersResponse.OffendersList[0].LastUploadDate.Value;
            Log.Info($"Last update time {LastUploadDate}");
            
            int currentHour = DateTime.Now.Hour;
            PlayGpsFile();

            var GetOffenderConfigurationDataRequest = new Configuration0.EntMsgGetOffenderConfigurationDataRequest();
            GetOffenderConfigurationDataRequest.OffenderID = GetOffendersResponse.OffendersList[0].ID;

            var GetOffenderConfigurationDataResponse = ConfigurationProxy_1.Instance.GetOffenderConfigurationData(GetOffenderConfigurationDataRequest);
            var EntConfigurationGPS = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration0.EntConfigurationGPS;

            int gsmDownTimeBetweenIntervalsSec = 20;
            int MIN_GSM_UP_TIME_SEC = 70;
            int MAX_GSM_UP_TIME_SEC = 80;
            int GPS_UP_TIME_STEP_SEC = 5;
            int ONE_SECOND_MILI = 1000;
            int sleepTimeMili = 0;

            //send vibrate to get the unit out of rest mode.
            //var entMsgSendVibrationRequest = new OffendersAction_1.EntMsgSendVibrationRequest(){ OffenderID = GetOffendersResponse.OffendersList[0].ID };
            //Log.Info($"Send vibrate to offender {entMsgSendVibrationRequest.OffenderID}");

            var entMsgSendUploadRequest = new OffendersAction_1.EntMsgSendUploadRequest() { OffenderID = GetOffendersResponse.OffendersList[0].ID };
            Log.Info($"Send upload request to offender {entMsgSendUploadRequest.OffenderID}");
            
            try
            {
                //OffenderActionsProxy_1.Instance.SendVibrationRequest(entMsgSendVibrationRequest);
                OffenderActionsProxy_1.Instance.SendUploadRequest(entMsgSendUploadRequest);
            }
            catch (Exception exception)
            {
                Log.Warn($"Send upload request faild.Continue with the test.(error:{exception.Message}.");
            }

            //Log.Info("set beacon to OFF");//by default set beacon to off
            //SetRF("?Out23DE9AC=0").GetAwaiter().GetResult();


            Log.Info($"Test will run until hour will change.CurrentHour = {currentHour}");
            while (currentHour == DateTime.Now.Hour)
            {
                for (int i = MIN_GSM_UP_TIME_SEC; i <= MAX_GSM_UP_TIME_SEC; i += GPS_UP_TIME_STEP_SEC)
                {
                    sleepTimeMili = i * ONE_SECOND_MILI;
                    Log.Info($"Set GSM ON for {i} seconds");
                    SetRF(SetGsmOnPath).GetAwaiter().GetResult();
                    SleepWithPrint(sleepTimeMili, "GSM ON", currentHour);
                    Log.Info($"Set GSM OFF for {gsmDownTimeBetweenIntervalsSec} seconds");
                    SetRF(SetGsmOffPath).GetAwaiter().GetResult();
                    sleepTimeMili = gsmDownTimeBetweenIntervalsSec * ONE_SECOND_MILI;
                    SleepWithPrint(sleepTimeMili, "GSM OFF", currentHour);
                    //set beacon every to between minute 18 and minute 33 minutes (15 minutes)
                    var currentMinute = DateTime.Now.Minute;
                    if( currentMinute >= 18 && currentMinute <= 33)
                    {
                        Log.Info("set beacon to ON");
                        SetRF(SetBeaconOnPath).GetAwaiter().GetResult();
                        try
                        {
                            var entMsgClearTamperRequest = new OffendersAction_1.EntMsgClearTamperRequest()
                                { OffenderID = GetOffendersResponse.OffendersList[0].ID };
                            OffenderActionsProxy_1.Instance.SendClearTamperRequest(entMsgClearTamperRequest);
                        }
                        catch (Exception exception)
                        {
                            Log.Warn($"Send ClearTamper request faild.Continue with the test.(error:{exception.Message}.");
                        }
                    }
                    else
                    {
                        Log.Info("set beacon to OFF");
                        SetRF(SetBeaconOffPath).GetAwaiter().GetResult();
                    }
                }
            }
            SerialPortReaderObj.Stop();
        }

        private static string labSatIpAddress_IP_ADDRESS_Default = "10.10.12.10";
        private static string labSatFileName_Default = "GPS2BIT_UK_Dyn";


        private void PlayGpsFile()
        {
            //check the status and if not playing and the minutes is between 0 and 10 start play.
            try
            {
                //Log.Info($"In case hour devided by 3 and first 20 munites we start GPS(Hour = {DateTime.Now.Hour}, minute = {DateTime.Now.Minute}.");
                //if (DateTime.Now.Hour % 3 == 0 && DateTime.Now.Minute < 20)
                Log.Info($"In case first 20 munites we start GPS(Hour = {DateTime.Now.Hour}, minute = {DateTime.Now.Minute}.");
                if (DateTime.Now.Minute < 20)
                {
                    var labSatIpAddress = ConfigurationManager.AppSettings[labSatIpAddress_IP_ADDRESS_Token];
                    if( string.IsNullOrEmpty(labSatIpAddress) == true)
                    {
                        labSatIpAddress = labSatIpAddress_IP_ADDRESS_Default;
                    }
                    var labSatFileName = ConfigurationManager.AppSettings[labSatIpFileName_Token];
                    if (string.IsNullOrEmpty(labSatFileName) == true)
                    {
                        labSatFileName = labSatFileName_Default;
                    }
                    Log.Info($"Going to play GPS file {labSatFileName} with GPS Player at ip {labSatIpAddress}.");
                    var cmd = new PlayCommand(labSatIpAddress);
                    cmd.Execute(new PlayCommandParams() { FileName = labSatFileName });
                }
                else
                {
                    Log.Info($"NOT play GPS file because minutes value = {DateTime.Now.Minute}");
                }

            }
            catch (Exception ex)
            {
                Log.Error($"Error while try to play : {ex.Message}");
            }
        }

        private void SleepWithPrint(int sleepTimeMili, string message, int currentHour)
        {
            if (currentHour != DateTime.Now.Hour)
            {
                Log.Info($"no sleep because current hour changed( current hour = {currentHour}. now = {DateTime.Now.Hour}");
                return;
            }
            Log.Info($"Going to sleep totaly {sleepTimeMili} mili.\t{message}");
            if (sleepTimeMili < 10000)
            {
                Thread.Sleep(sleepTimeMili);
            }
            else
            {
                int sleepIntervalForPrint = 5000;
                for (int i = sleepIntervalForPrint; i < sleepTimeMili; i += sleepIntervalForPrint)
                {
                    Log.Info($"Going to sleep {sleepIntervalForPrint} mili.\t{message}");
                    Thread.Sleep(sleepIntervalForPrint);
                    if (currentHour != DateTime.Now.Hour)
                    {
                        Log.Info($"Stop test because current hour changed( current hour = {currentHour}. now = {DateTime.Now.Hour}");
                        break;
                    }

                }
            }
        }

        private Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest CreateUpdateRequestFromGet(Offenders0.EntOffender offender, Configuration0.EntConfigurationGPS entConfigurationGPS)
        {
            var entMsgUpdateGPSOffenderConfigurationDataRequest = new Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest();

            entMsgUpdateGPSOffenderConfigurationDataRequest.OffenderID = offender.ID;
            entMsgUpdateGPSOffenderConfigurationDataRequest.TransmitterDisappearTime = entConfigurationGPS.TransmitterDisappearTime;
            entMsgUpdateGPSOffenderConfigurationDataRequest.SupportGPRS = entConfigurationGPS.SupportGPRS;
            entMsgUpdateGPSOffenderConfigurationDataRequest.SupportCSD = entConfigurationGPS.SupportCSD;
            entMsgUpdateGPSOffenderConfigurationDataRequest.ReceiverRange = entConfigurationGPS.ReceiverRange;
            entMsgUpdateGPSOffenderConfigurationDataRequest.DetectOtherTransmitters = entConfigurationGPS.DetectOtherTransmitters;
            entMsgUpdateGPSOffenderConfigurationDataRequest.ScheduleActive = entConfigurationGPS.ScheduleActive;
            entMsgUpdateGPSOffenderConfigurationDataRequest.ReceptionIconsActive = entConfigurationGPS.ReceptionIconsActive;
            entMsgUpdateGPSOffenderConfigurationDataRequest.DisplayOffenderName = entConfigurationGPS.DisplayOffenderName;
            entMsgUpdateGPSOffenderConfigurationDataRequest.DefaultMessage = entConfigurationGPS.DefaultMessage;
            entMsgUpdateGPSOffenderConfigurationDataRequest.GPSProximity = entConfigurationGPS.GPSProximity;
            entMsgUpdateGPSOffenderConfigurationDataRequest.GPSProximityBuffer = entConfigurationGPS.GPSProximityBuffer;
            entMsgUpdateGPSOffenderConfigurationDataRequest.PassiveMode = entConfigurationGPS.PassiveMode;
            entMsgUpdateGPSOffenderConfigurationDataRequest.NormalLoggingRate = entConfigurationGPS.NormalLoggingRate;
            entMsgUpdateGPSOffenderConfigurationDataRequest.ViolationLoggingRate = entConfigurationGPS.ViolationLoggingRate;
            entMsgUpdateGPSOffenderConfigurationDataRequest.TimeBetweenUploads = entConfigurationGPS.TimeBetweenUploads;
            entMsgUpdateGPSOffenderConfigurationDataRequest.LBSActive = entConfigurationGPS.LBSActive;
            entMsgUpdateGPSOffenderConfigurationDataRequest.GPSDisappearTime = entConfigurationGPS.GPSDisappearTime;
            entMsgUpdateGPSOffenderConfigurationDataRequest.LBSSamplingRate = entConfigurationGPS.LBSSamplingRate;
            entMsgUpdateGPSOffenderConfigurationDataRequest.LBSRequestTimeout = entConfigurationGPS.LBSRequestTimeout;
            entMsgUpdateGPSOffenderConfigurationDataRequest.LBSNoPositionTimeout = entConfigurationGPS.LBSNoPositionTimeout;
            entMsgUpdateGPSOffenderConfigurationDataRequest.BaseUnitActive = entConfigurationGPS.BaseUnitActive;
            entMsgUpdateGPSOffenderConfigurationDataRequest.BaseUnitRange = entConfigurationGPS.BaseUnitRange;
            entMsgUpdateGPSOffenderConfigurationDataRequest.TimeBetweenBaseUnitUploads = entConfigurationGPS.TimeBetweenUploads;
            entMsgUpdateGPSOffenderConfigurationDataRequest.LandlineActive = entConfigurationGPS.LandlineActive;
            entMsgUpdateGPSOffenderConfigurationDataRequest.LandlineCallerIDRequired = entConfigurationGPS.LandlineCallerIDRequired;

            return entMsgUpdateGPSOffenderConfigurationDataRequest;
        }

        static async Task SetRF(string path)
        {
            Log.Info($"Set RF to path {path}");
            try
            {
                HttpResponseMessage response = await ArduinoClient.GetAsync(path);
                if (response.IsSuccessStatusCode)
                {
                    string resultString = await response.Content.ReadAsStringAsync();
                    Log.Info($"Set RF to path {path},result = {resultString}");
                }
                else
                {
                    Log.Info($"Set RF to path {path},result = {response.StatusCode}");
                }
            }
            catch (Exception exception)
            {
                Log.Error($"Set RF to path { path} faild with error {exception.Message}");
            }
        }

        private void LinesReceived(object sender, SerialPortReaderDataReceivedEventArgs e)
        {
            var lines = e.Lines;

            foreach (var line in lines)
            {
                Log.Info($"Data from device:{line}.");
                foreach (var trigger in SmsTriggers)
                {
                    if (line.IndexOf(trigger) != -1)
                    {
                        Log.Info($"going to send sms based on line {line}");
                        SendSms();
                    }
                }
                
            }
        }

        private void SendSms()
        {
            var SmsGatewayAppFolder = ConfigurationManager.AppSettings[SmsGatewayAppFolder_Token];
            var SmsGatewayAppExe = ConfigurationManager.AppSettings[SmsGatewayAppExe_Token];
            var SmsGatewayCommandPattern = ConfigurationManager.AppSettings[SmsGatewayCommandPattern_Token];
            var SMSContent = ConfigurationManager.AppSettings[SMSContent_Token];
            var EquipmentPhoneNumber = ConfigurationManager.AppSettings[EquipmentPhoneNumber_Token];

            string args = string.Format(SmsGatewayCommandPattern, EquipmentPhoneNumber, SMSContent, "MissedCallTest");
            var fileName = Path.Combine(SmsGatewayAppFolder, SmsGatewayAppExe);

            try
            {
                Log.Info($"Going to send SMS. file name {fileName}, args = {args}");
                Process smsGatewayAppProcess = new Process
                {
                    StartInfo = new ProcessStartInfo(fileName)
                    {
                        Arguments = args,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true
                    }
                };
                                
                smsGatewayAppProcess.Start();
                while (!smsGatewayAppProcess.StandardOutput.EndOfStream)
                {
                    string line = smsGatewayAppProcess.StandardOutput.ReadLine();
                    Log.Info($"SMS GateWay output: {line}");
                }
                //smsGatewayAppProcess.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
                //smsGatewayAppProcess.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
                //smsGatewayAppProcess.BeginOutputReadLine();
                //smsGatewayAppProcess.BeginErrorReadLine();
                //smsGatewayAppProcess.WaitForExit();

                Log.Info("Finish send SMS without errors.");
            }
            catch (Exception exception)
            {
                Log.Error($"While try to send sms . error: {exception.Message}");
            }
        }

        //static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        //{
        //    Log.Info($"SMS GateWay output: {outLine.Data}");
        //}

        /// <summary>
        /// Log printF Sub commands
        /// </summary>
        public enum LogPrintfSubCommands : byte
        {
            LOG_PRINTF_TIME_DURATION,
            LOG_PRINTF_MASK,
            LOG_PRINTF_DEBUG_LEVEL,
            LOG_PRINTF_START_MESSAGE_TIME,
            LOG_PRINTF_SEND_NEXT_MESSAGE,
        }

        public enum LogPrintFDebugLevel
        {
            None,
            Minimum,
            Medium,
            Maximum
        }

        private void SendLogPrintfCommand(ushort timeout, byte debugLevel, uint mask, SerialPortEx SerialPortObj)
        {
            List<CommandEx> commands = new List<CommandEx>
            {   // add the debug level sub command
                new CommandEx(
                    EnumProtocolCommands.CommandLogPrintfSetup, //                (byte)SmartCommands.CommandIds.PROTOCOL_CMD_LOGPRINTF_CONTROL,
                    (byte)LogPrintfSubCommands.LOG_PRINTF_DEBUG_LEVEL,
                    new byte[] { debugLevel }),

                // add the mask sub command
                new CommandEx(
                    EnumProtocolCommands.CommandLogPrintfSetup,
                    (byte)LogPrintfSubCommands.LOG_PRINTF_MASK,
                    BitConverter.GetBytes(mask)),

                // add the timeout duration sub command
                new CommandEx(
                    EnumProtocolCommands.CommandLogPrintfSetup,
                    (byte)LogPrintfSubCommands.LOG_PRINTF_TIME_DURATION,
                    BitConverter.GetBytes(timeout))
            };
            bool useRetransmissions = true;
            bool abortIfBusy = true;
            SmartProtocolHelper.SendCommands(commands, useRetransmissions, abortIfBusy, SerialPortObj);
        }
    }

    /// <summary>
    /// Provides data for the SerialPortReaderDataReceivedEventHandler event.
    /// </summary>
    public class SerialPortReaderDataReceivedEventArgs : EventArgs
    {
        /// <summary>
        ///Gets or sets the event type.
        ///Returns:Lines received.
        /// </summary>
        public String[] Lines { get; }
        /// <summary>
        /// default c'tor
        /// </summary>
        public SerialPortReaderDataReceivedEventArgs()
        {
        }
        /// <summary>
        /// c'tor that get the data
        /// </summary>
        public SerialPortReaderDataReceivedEventArgs(String[] lines)
        {
            Lines = lines;
        }
    }

    public class SerialPortExDataSendEventArgs : EventArgs
    {
        public SerialPort SerialPort { get; set; }
        public byte[] Buffer { get; set; }
    }



    /// <summary>
    /// 
    /// </summary>
    public class SerialPortEx
    {
        private string buffer { get; set; }

        private static byte[] rxBuffer = new byte[0];
        private SerialPort _port { get; set; }

        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public Parity Parity { get; set; }
        public int DataBits { get; set; }
        public StopBits StopBits { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void SerialPortReaderDataReceivedEventHandler(object sender, SerialPortReaderDataReceivedEventArgs e);

        /// <summary>
        /// Indicates that data has been received through a port at least one line.
        /// </summary>
        public event SerialPortReaderDataReceivedEventHandler LinesReceived;
        /// <summary>
        /// default c'tor for serial port reader
        /// </summary>
        public SerialPortEx(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
        {
            PortName = portName;
            BaudRate = baudRate;
            Parity = Parity;
            DataBits = dataBits;
            StopBits = stopBits;
        }
        
        public void Start()
        {
            _port = new SerialPort(PortName, BaudRate, Parity, DataBits, StopBits);
            _port.DataReceived += new SerialDataReceivedEventHandler(dataReceived);
            buffer = string.Empty;
            try
            {
                _port.Open();
            }
            catch (UnauthorizedAccessException uaae)
            {
                var message = $"Access is denied to the port.-or - The current process, " +
                    $"or another process on the system, already has the specified COM port open either " +
                    $"by a System.IO.Ports.SerialPort instance or in unmanaged code.";
                throw new Exception(message, uaae);
            }
            catch(ArgumentOutOfRangeException aoore)
            {
                var message = $"One or more of the properties for this instance are invalid.For example, the " +
                    "System.IO.Ports.SerialPort.Parity, System.IO.Ports.SerialPort.DataBits, or System.IO.Ports.SerialPort.Handshake " +
                    "properties are not valid values; the System.IO.Ports.SerialPort.BaudRate is less " +
                    "than or equal to zero; the System.IO.Ports.SerialPort.ReadTimeout or System.IO.Ports.SerialPort.WriteTimeout " +
                    "property is less than zero and is not System.IO.Ports.SerialPort.InfiniteTimeout.";
                throw new Exception(message, aoore);
            }
            catch(ArgumentException ae)
            {
                var message = $"The port name does not begin with \"COM\". - or -The file type of the port is not supported.";
                throw new Exception(message, ae);
            }

            catch (IOException ioe)
            {
                var message = $"The port is in an invalid state. - or - An attempt to set the state of the underlying " +
                    "port failed. For example, the parameters passed from this System.IO.Ports.SerialPort object were invalid.";
                throw new Exception(message, ioe);
            }

            catch (InvalidOperationException ioe)
            {
                var message = $"The specified port on the current instance of the System.IO.Ports.SerialPort is already open.";
                throw new Exception(message, ioe);
            }
        }

        public void Stop()
        {
            _port.DataReceived -= new SerialDataReceivedEventHandler(dataReceived);
            _port.Close();
            buffer += Environment.NewLine;
        }

        private void dataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort port = sender as SerialPort;
            if (port.IsOpen == false)
                return;

            byte[] buffer = new byte[port.ReadBufferSize];
            int amountOfBytesRead = port.Read(buffer, 0, port.ReadBufferSize);

            // add whatever we got to the rx buffer
            rxBuffer = rxBuffer.Concat(buffer.Take(amountOfBytesRead)).ToArray();
            if (SmartProtocolHelper.LookForPreamble(ref rxBuffer))
            {
                byte[] packet;
                if (SmartProtocolHelper.ContainsAWholePacket(ref rxBuffer, out packet))
                {
                    SmartProtocolHelper.ParsePacket(packet, LinesReceived);
                }
            }
        }

        public void SendData(object sender, SerialPortExDataSendEventArgs e)
        {
            if (e.SerialPort == null || !e.SerialPort.IsOpen)
                return;

            try
            {
                _port.Write(e.Buffer, 0, e.Buffer.Length);
            }
            catch (ArgumentNullException innerEx)
            {
                var message = "The buffer passed is null.";
                throw new Exception(message, innerEx);
            }

            catch (InvalidOperationException innerEx)
            {
                var message = "The specified port is not open.";
                throw new Exception(message, innerEx);
            }

            catch (ArgumentOutOfRangeException innerEx)
            {
                var message = "The offset or count parameters are outside a valid region of the buffer being passed. Either offset or count is less than zero.";
                throw new Exception(message, innerEx);
            }

            catch (ArgumentException innerEx)
            {
                var message = "offset plus count is greater than the length of the buffer.";
                throw new Exception(message, innerEx);
            }

            catch (System.ServiceProcess.TimeoutException innerEx)
            {
                var message = "The operation did not complete before the time-out period ended.";
                throw new Exception(message, innerEx);
            }
        }

        public void SendData(byte[] buffer)
        {
            if (_port == null || !_port.IsOpen)
                return;

            try
            {
                _port.Write(buffer, 0, buffer.Length);
            }
            catch (ArgumentNullException innerEx)
            {
                var message = "The buffer passed is null.";
                throw new Exception(message, innerEx);
            }

            catch (InvalidOperationException innerEx)
            {
                var message = "The specified port is not open.";
                throw new Exception(message, innerEx);
            }

            catch (ArgumentOutOfRangeException innerEx)
            {
                var message = "The offset or count parameters are outside a valid region of the buffer being passed. Either offset or count is less than zero.";
                throw new Exception(message, innerEx);
            }

            catch (ArgumentException innerEx)
            {
                var message = "offset plus count is greater than the length of the buffer.";
                throw new Exception(message, innerEx);
            }

            catch (System.ServiceProcess.TimeoutException innerEx)
            {
                var message = "The operation did not complete before the time-out period ended.";
                throw new Exception(message, innerEx);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class SmartProtocolHelper
    {
        /// <summary>
        /// log implementation 
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #region variables

        private static byte packetNumber = 1;
        private const uint MTDTERM_SOURCE_ADDRESS = 0x10020304;
        public const uint BROADCAST_MTD_DEST_ADDRESS = 0x02FFFFFF;
        public const uint BROADCAST_CHG_DEST_ADDRESS = 0x03FFFFFF;
        public const ulong IR_SYNC_STR = 0x96dfaaacff005502;
        private const short PORT = 0x0000;
        private const int IR_SYNC_BYTE_LENGTH = 8;
        private const int CRC_32_FIELD_LENGTH = 4;
        private static uint maxRetryCount = 3;
        private static uint retryTimeout = 2000;

        public static uint destinationAddress = 0;

        private static Timer retransmitTimer;
        private static int consequtiveRetransmissions = 0;
        private static int amountOfResponsesToWaitFor;
        private static byte[] lastPacketSent;
        //private static SmartCommands.DeviceType deviceMode = SmartCommands.DeviceType.MTD;

        public const int SOURCE_ADDRESS_OFFSET = 0;
        public const int SOURCE_ADDRESS_LENGTH = 4;
        public const int DESTINATION_ADDRESS_OFFSET = SOURCE_ADDRESS_LENGTH;
        public const int DESTINATION_ADDRESS_LENGTH = 4;
        public const int PORT_FIELD_OFFSET = DESTINATION_ADDRESS_OFFSET + DESTINATION_ADDRESS_LENGTH;
        public const int PORT_FIELD_LENGTH = 2;
        public const int LENGTH_FIELD_OFFSET = PORT_FIELD_OFFSET + PORT_FIELD_LENGTH;
        public const int LENGTH_FIELD_LENGTH = 2;
        public const int PACKET_NUMBER_FIELD_OFFSET = LENGTH_FIELD_OFFSET + LENGTH_FIELD_LENGTH;
        public const int PACKET_NUMBER_FIELD_LENGTH = 1;
        public const int ENCRYPTION_TYPE_FIELD_OFFSET = PACKET_NUMBER_FIELD_OFFSET + PACKET_NUMBER_FIELD_LENGTH;
        public const int ENCRYPTION_TYPE_FIELD_LENGTH = 1;
        public const int DATA_FIELD_OFFSET = ENCRYPTION_TYPE_FIELD_OFFSET + ENCRYPTION_TYPE_FIELD_LENGTH;
        public const int PACKET_HEADER_LENGTH = SOURCE_ADDRESS_LENGTH + DESTINATION_ADDRESS_LENGTH + PORT_FIELD_LENGTH + LENGTH_FIELD_LENGTH + PACKET_NUMBER_FIELD_LENGTH + ENCRYPTION_TYPE_FIELD_LENGTH;

        #endregion

        public static bool LookForPreamble(ref byte[] buffer)
        {
            bool result = false;
            int preambleStartIndex = buffer.IndexOf(IR_SYNC_STR.ToByteArray());
            if (preambleStartIndex != -1)
            {
                // there is a chance that we got garbaged transmission, or missed part of it - need to remove the junk from the FIFO
                if (preambleStartIndex > 0)
                {
                    // delete evrything that is before the preamble
                    buffer = buffer.Skip(preambleStartIndex).ToArray();
                }
                result = true;
            }
            return result;
        }

        public static bool ContainsAWholePacket(ref byte[] buffer, out byte[] packetBuffer)
        {
            bool result = false;
            packetBuffer = null;

            // make sure the buffer has enough bytes for a packet preamble+header length
            if (buffer.Length >= IR_SYNC_BYTE_LENGTH + PACKET_HEADER_LENGTH)
            {
                ushort dataLength = BitConverter.ToUInt16(buffer, IR_SYNC_BYTE_LENGTH + LENGTH_FIELD_OFFSET);
                int totalPacketSize = IR_SYNC_BYTE_LENGTH + PACKET_HEADER_LENGTH + dataLength + CRC_32_FIELD_LENGTH;

                // if we have a whole packet in the buffer
                if (buffer.Length >= totalPacketSize)
                {
                    // copy the packet to a new buffer
                    packetBuffer = new byte[totalPacketSize];
                    Array.Copy(buffer, packetBuffer, totalPacketSize);

                    // remove the packet from the buffer
                    buffer = buffer.Skip(totalPacketSize).ToArray();
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        public static void ParsePacket(byte[] packet, SerialPortReaderDataReceivedEventHandler LinesReceivedEvent)
        {
            // remove the preamble from the packet buffer
            packet = packet.Skip(IR_SYNC_BYTE_LENGTH).ToArray();

            byte[] crcBuffer = new byte[packet.Length - CRC_32_FIELD_LENGTH];
            Array.Copy(packet, 0, crcBuffer, 0, crcBuffer.Length);
            uint calculatedCrc = Proxies.Device.Crc32.InitCrc32();
            calculatedCrc = Proxies.Device.Crc32.GetCrc32(Proxies.Device.Crc32.AddCrc32(crcBuffer, calculatedCrc));

            // take the crc field from the packet
            byte[] receivedCrc = new byte[CRC_32_FIELD_LENGTH];
            Array.Copy(packet, packet.Length - 4, receivedCrc, 0, CRC_32_FIELD_LENGTH);

            // do we have a CRC match?
            if (calculatedCrc == BitConverter.ToUInt32(receivedCrc, 0))
            {
                // remove the CRC from the packet
                packet = packet.Take(packet.Length - CRC_32_FIELD_LENGTH).ToArray();

                // get the source address
                byte[] sourceAddress = packet.ExtractBytes(SOURCE_ADDRESS_OFFSET, SOURCE_ADDRESS_LENGTH);

                // get the destination address
                byte[] destinationAddress = packet.ExtractBytes(DESTINATION_ADDRESS_OFFSET, DESTINATION_ADDRESS_LENGTH);

                // take the port field
                byte[] port = packet.ExtractBytes(PORT_FIELD_OFFSET, PORT_FIELD_LENGTH);

                // take the length field
                byte[] length = packet.ExtractBytes(LENGTH_FIELD_OFFSET, LENGTH_FIELD_LENGTH);

                // take the packet number field
                byte[] packetNumber = packet.ExtractBytes(PACKET_NUMBER_FIELD_OFFSET, PACKET_NUMBER_FIELD_LENGTH);

                // take the encryption type
                byte[] encryptionType = packet.ExtractBytes(ENCRYPTION_TYPE_FIELD_OFFSET, ENCRYPTION_TYPE_FIELD_LENGTH);

                // take the data field
                ushort iLength = BitConverter.ToUInt16(length, 0);
                byte[] data = packet.ExtractBytes(DATA_FIELD_OFFSET, iLength);

                ParseDataPacket(data, LinesReceivedEvent);
            }
        }

        private static void ParseDataPacket(byte[] buffer, SerialPortReaderDataReceivedEventHandler LinesReceivedEvent)
        {
            var lines = new List<string>();

            int counter = 0;
            while (buffer.Length > 0)
            {
                byte[] command = buffer.ExtractBytes(0, 1);
                EnumProtocolCommands commandId = (EnumProtocolCommands)command[0];
                byte[] length = buffer.ExtractBytes(1, 2);
                ushort iLength = BitConverter.ToUInt16(length, 0);
                byte[] data = buffer.ExtractBytes(3, iLength);
                //
                if (commandId == EnumProtocolCommands.CommandLogPrintf)
                {
                    string line = Encoding.ASCII.GetString(data);
                    lines.Add(line);
                }
                //OnPacketReceived(commandId, data);
                counter++;
                buffer = buffer.Skip(3 + iLength).ToArray();
            }

            if( LinesReceivedEvent != null && lines.Count > 0 )
            {
                var eventArgs = new SerialPortReaderDataReceivedEventArgs(lines.ToArray());
                LinesReceivedEvent(null, eventArgs);

            }
        }

        private static void OnPacketReceived(EnumProtocolCommands command, byte[] data)
        {
            if (command == EnumProtocolCommands.CommandLogPrintf)
            {
                string line = Encoding.ASCII.GetString(data);
                Trace.WriteLine($"[LogPrintf] {line}");

                //DebugView.WriteLine(line, DebugView.Sources.LogPrintf);
                //if (checkBox2.Checked && line.Contains("GPS DUMP|"))
                //{
                //    string[] tokens = line.Split(new char[] { '|' });
                //    int msgId = int.Parse(tokens[1]);
                //    int msgLength = int.Parse(tokens[2]);
                //    string msgData = tokens[3];
                //    byte[] dump_data = msgData.FromHexByteStringToByteArray(false);
                //    string[] parsedMessage = GPS.Extensions.SiRF_protocol.ParseMessageDump(msgId, dump_data);
                //    foreach (var l in parsedMessage)
                //    {
                //        DebugView.WriteLine(l, DebugView.Sources.GPSDump);
                //    }
                //}
            }
        }

        public static byte[] ExtractBytes(this byte[] buffer, int offset, int count)
        {
            byte[] result = new byte[count];
            Array.Copy(buffer, offset, result, 0, count);
            return result;
        }

        public static byte[] ToByteArray(this ulong number)
        {
            return BitConverter.GetBytes(number);
        }

        public static byte[] ToByteArray(this int number)
        {
            return BitConverter.GetBytes(number);
        }

        public static byte[] ToByteArray(this uint number)
        {
            return BitConverter.GetBytes(number);
        }

        public static byte[] ToByteArray(this short number)
        {
            return BitConverter.GetBytes(number);
        }

        public static byte[] ToByteArray(this ushort number)
        {
            return BitConverter.GetBytes(number);
        }

        public static byte[] ToByteArray(this byte number)
        {
            byte[] result = { number };
            return result;
        }

        public static int IndexOf(this byte[] bufferToLookIn, byte[] bufferToLookFor)
        {
            int j = 0;

            for (int i = 0; i < bufferToLookIn.Length; i++)
            {
                if (bufferToLookIn[i] == bufferToLookFor[j])
                {
                    if (j == bufferToLookFor.Length - 1)
                    {
                        return i - j;
                    }
                    j++;
                }
                else
                    j = 0;
            }

            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        public delegate void SerialDataSendEventHandler(byte[] buffer);

        public static void SendCommands(
            IEnumerable<CommandEx> commands, 
            bool useRetransmissions = true, 
            bool abortIfBusy = true,
            SerialPortEx SerialPortObj = null)
        {
            if (commands.Count() == 0)
                return;

            if (amountOfResponsesToWaitFor > 0)
            {
                if (abortIfBusy)
                    return;
                else
                    packetNumber++;
            }

            lastPacketSent = new byte[0];
            foreach (CommandEx command in commands)
            {
                amountOfResponsesToWaitFor++;
                if (command.ContainsSubCommand)
                    lastPacketSent = lastPacketSent.Concat(_buildCommandBuffer(command.CommandId, command.SubCommandId, command.Data)).ToArray();
                else
                    lastPacketSent = lastPacketSent.Concat(_buildCommandBuffer(command.CommandId, command.Data)).ToArray();
            }
            SendIRPacket(lastPacketSent, useRetransmissions, SerialPortObj);
        }

        private static byte[] _buildCommandBuffer(EnumProtocolCommands command, byte[] data)
        {
            Log.DebugFormat("{0}: Building message for command #{1} {2}", DateTime.Now.ToLongTimeString(), (int)command, command.ToString());
            byte commandId = (byte)command;
            short length = (short)(data.Length);
            byte[] result = 
                new byte[] { commandId }.
                    Concat(BitConverter.GetBytes(length).
                        Concat(data)).ToArray();
            return result;
        }

        private static byte[] _buildCommandBuffer(EnumProtocolCommands command, byte subCommandId, byte[] data)
        {
            Log.DebugFormat("{0}: Building message for command #{1} {2}, sub-command: #{3}", DateTime.Now.ToLongTimeString(), (int)command, command.ToString(), subCommandId);
            byte commandId = (byte)command;
            short length = (short)(data.Length + 1);
            byte[] result =
                new byte[] { commandId }.
                        Concat(BitConverter.GetBytes(length)).
                            Concat(new byte[] { subCommandId }).
                                Concat(data).ToArray();
            return result;
        }
        public static string ToHexByteString(this byte[] buffer)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in buffer)
            {
                sb.AppendFormat("{0:X2}", b);
                sb.Append(" ");
            }

            return sb.ToString().TrimEnd(new char[] { ' ' });
        }


        private static byte[] SendIRPacket(byte[] packetData, bool useRetransmissions, SerialPortEx SerialPortObj)
        {
            
            Log.DebugFormat("Sending packet #{0} with {1} bytes", packetNumber, packetData.Length);

            byte[] buffer = BuildIrPacket(packetData);
            var hexBuffer = buffer.ToHexByteString();
            Log.Debug($"Buffer with headers and CRC {buffer.Length} bytes, HEX:{hexBuffer}.");

            // send the packet using the IR
            SerialPortObj.SendData(buffer);

            //if (useRetransmissions)
            //    startRetransmitTimer();
            return buffer;
        }

        /// <summary>
        /// device type
        /// </summary>
        public enum DeviceType
        {
            /// <summary>
            /// MTD device type
            /// </summary>
            MTD = 2,
            /// <summary>
            /// CHG device type
            /// </summary>
            CHG = 3
        }

        /// <summary>
        /// build packet for sending the IR
        /// </summary>
        /// <param name="packetData"></param>
        /// <returns></returns>
        public static byte[] BuildIrPacket(byte[] packetData)
        {
            //if (destinationAddress == 0)
            //    destinationAddress = SmartProtocol.DeviceMode == SmartCommands.DeviceType.MTD ?
            //    BROADCAST_MTD_DEST_ADDRESS : BROADCAST_CHG_DEST_ADDRESS;
            short dataLength = (short)packetData.Length;
            byte encryptionType = 0;

            // build the packet header + data field
            byte[] buffer2crc =
                BitConverter.GetBytes(MTDTERM_SOURCE_ADDRESS).               // source address
                Concat(BitConverter.GetBytes(BROADCAST_MTD_DEST_ADDRESS)).   // destination address
                Concat(BitConverter.GetBytes(PORT)).                         // port
                Concat(BitConverter.GetBytes(dataLength)).                   // data field length
                Concat(new byte[] { packetNumber }).                         // packet number
                Concat(new byte[] { encryptionType }).                       // encryption type
                Concat(packetData).                                          // the data itself
                ToArray();

            Log.Debug($"packet header + data : {buffer2crc.ToHexByteString()}");
            // run CRC32 on the buffer in hand
            uint crcValue = Proxies.Device.Crc32.InitCrc32();
            crcValue = Proxies.Device.Crc32.GetCrc32(Proxies.Device.Crc32.AddCrc32(buffer2crc, crcValue));

            // build the whole packet
            byte[] buffer = 
                IR_SYNC_STR.ToByteArray().
                Concat(buffer2crc).                         // packet header + data
                Concat(BitConverter.GetBytes(crcValue)).     // CRC32
                ToArray();

            Log.Debug($"full buffer: {buffer.ToHexByteString()}");

            return buffer;
        }
    }

    /// <summary>
    /// command extention for the commands 
    /// </summary>
    public class CommandEx
    {
        /// <summary>
        /// command id
        /// </summary>
        public EnumProtocolCommands CommandId { get; }
        /// <summary>
        /// sub-command id - it is a byte (and not EnumProtocolCommands because it need to support bytre values like LogPrintfSubCommands).
        /// </summary>
        public byte SubCommandId { get; }

        private byte[] data;
        /// <summary>
        /// data of the command
        /// </summary>
        public byte[] Data
        {
            get
            {
                if (data == null)
                    return new byte[0];
                else
                    return data;
            }
        }
        /// <summary>
        /// indication if the command contains sub-command
        /// </summary>
        public bool ContainsSubCommand { get; } = false;
        /// <summary>
        /// c'tor for commands class
        /// </summary>
        /// <param name="_commandId"></param>
        /// <param name="_subCommandId"></param>
        /// <param name="_data"></param>
        public CommandEx(EnumProtocolCommands _commandId, byte _subCommandId, byte[] _data)
        {
            CommandId = _commandId;
            SubCommandId = _subCommandId;
            data = _data;
            ContainsSubCommand = true;
        }
        /// <summary>
        /// c'tor for commands class
        /// </summary>
        /// <param name="_commandId"></param>
        /// <param name="_data"></param>
        public CommandEx(EnumProtocolCommands _commandId, byte[] _data)
        {
            CommandId = _commandId;
            data = _data;
            ContainsSubCommand = false;
        }
        /// <summary>
        /// to string implementation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("ID: {0}, SubCommand ID: {1}", CommandId, SubCommandId);
        }
    }
}
