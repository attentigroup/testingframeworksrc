﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Log0 = TestingFramework.Proxies.EM.Interfaces.Log12_0;

namespace TestBlocksLib.LogBlocks
{
    public class GetLogProgramTestBlock : GetLogTestBlock
    {
        [PropertyTest(EnumPropertyName.QueueProgramTracker, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<Log0.EntLogProgram> LogProgram { get; set; }


        protected override void ExecuteBlock()
        {

            base.ExecuteBlock();
            LogProgram = new List<Log0.EntLogProgram>();
            foreach (var request in GetLogResponse.Entities)
            {
                LogProgram.Add(request as Log0.EntLogProgram);
            }

        }
    }
}
