﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.SeleniumWrapper;
using Log0 = TestingFramework.Proxies.EM.Interfaces.Log12_0;

namespace TestBlocksLib.LogBlocks
{
    public class WaitForDoneRequestStatusAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int MaximumRows { get; set; }


        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int StartRowIndex { get; set; }


        [PropertyTest(EnumPropertyName.filterID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int filterID { get; set; }

        [PropertyTest(EnumPropertyName.resultCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Log0.EnmResultCode resultCode { get; set; }

        [PropertyTest(EnumPropertyName.RequestID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RequestID { get; set; }

        [PropertyTest(EnumPropertyName.GetLogResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Log0.GetDataResultOfEntLogBaseypU6tq2N GetLogResponse { get; set; }

        [PropertyTest(EnumPropertyName.Timeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int SecondsTimeOut { get; set; }

        Log0.GetDataResultOfEntLogBaseypU6tq2N CurrentLogResponse => LogProxy.Instance.GetLog(filterID, resultCode, StartRowIndex, MaximumRows);
        protected override void ExecuteBlock()
        {
            Wait.Until(() => CurrentLogResponse.Entities.Any(x => x.RequestID == RequestID && x.RequestStatus.Equals("Done")), $"Event not created after timeout of {SecondsTimeOut} seconds", SecondsTimeOut);
        }
    }
}
