﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace TestBlocksLib.ScheduleBlocks
{
    public class GetScheduleParametersTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetScheduleParametersRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleParametersRequest GetScheduleParametersRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleParametersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgGetScheduleParametersResponse GetScheduleParametersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleParametersResponse = ScheduleProxy.Instance.GetScheduleParameters(GetScheduleParametersRequest);
        }
    }
}
