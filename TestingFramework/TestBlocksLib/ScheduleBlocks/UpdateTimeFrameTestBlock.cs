﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class UpdateTimeFrameTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            ScheduleProxy.Instance.UpdateTimeFrame(UpdateTimeFrameRequest);
        }
    }

    public class UpdateTimeFrameTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            ScheduleProxy_1.Instance.UpdateTimeFrame(UpdateTimeFrameRequest);
        }
    }


    public class UpdateTimeFrameTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            ScheduleProxy_2.Instance.UpdateTimeFrame(UpdateTimeFrameRequest);
        }
    }
}
