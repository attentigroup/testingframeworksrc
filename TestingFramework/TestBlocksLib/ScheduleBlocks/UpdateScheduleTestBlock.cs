﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{

    public class UpdateScheduleTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }
        

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }
        

        
        protected override void ExecuteBlock()
        {
            ScheduleProxy.Instance.UpdateSchedule(UpdateScheduleRequest);
        }
    }
    public class UpdateScheduleTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }



        protected override void ExecuteBlock()
        {
            ScheduleProxy_1.Instance.UpdateSchedule(UpdateScheduleRequest);
        }
    }

    public class UpdateScheduleTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }



        protected override void ExecuteBlock()
        {
            ScheduleProxy_2.Instance.UpdateSchedule(UpdateScheduleRequest);
        }
    }
}
