﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;
using System.Linq;

#region API refs
using offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class GetScheduleTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeIDsList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<int> TimeFrameIdList { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleResponse = ScheduleProxy.Instance.GetSchedule(GetScheduleRequest);

            TimeFrameIdList = GetScheduleResponse.Schedule.WeeklySchedule.Select(x => x.ID).ToList();
            TimeFrameIdList.AddRange(GetScheduleResponse.Schedule.CalendarSchedule.Select(x => x.ID));        
        }
    }

    public class GetScheduleTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }
        
        protected override void ExecuteBlock()
        {
            GetScheduleResponse = ScheduleProxy_1.Instance.GetSchedule(GetScheduleRequest);
        }
    }

    public class GetScheduleTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleResponse = ScheduleProxy_2.Instance.GetSchedule(GetScheduleRequest);
        }
    }
}
