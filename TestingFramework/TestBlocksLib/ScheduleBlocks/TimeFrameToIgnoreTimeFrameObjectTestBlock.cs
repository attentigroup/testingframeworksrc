﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.ScheduleBlocks
{
    public class TimeFrameToIgnoreTimeFrameObjectTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.Date, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime SkipDate { get; set; }

        [PropertyTest(EnumPropertyName.SkipDates, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntIgnoreTimeFrame IgnoreTimeFrame { get; set; }
        protected override void ExecuteBlock()
        {
            IgnoreTimeFrame = new Schedule0.EntIgnoreTimeFrame() { TimeFrameID = TimeframeID.Value, SkipDate = SkipDate };
        }
    }
}
