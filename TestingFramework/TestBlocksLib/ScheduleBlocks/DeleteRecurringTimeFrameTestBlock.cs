﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class DeleteRecurringTimeFrameTestBlock :TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteRecurringTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgDeleteRecurringTimeFrameInstance DeleteRequrringTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ScheduleProxy.Instance.DeleteRecurringTimeFrameInstance(DeleteRequrringTimeFrameRequest);

        }
    }

    public class DeleteRecurringTimeFrameTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteRecurringTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgDeleteRecurringTimeFrameInstance DeleteRequrringTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ScheduleProxy_1.Instance.DeleteRecurringTimeFrameInstance(DeleteRequrringTimeFrameRequest);

        }
    }

    public class DeleteRecurringTimeFrameTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteRecurringTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgDeleteRecurringTimeFrameInstance DeleteRequrringTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ScheduleProxy_2.Instance.DeleteRecurringTimeFrameInstance(DeleteRequrringTimeFrameRequest);

        }
    }
}
