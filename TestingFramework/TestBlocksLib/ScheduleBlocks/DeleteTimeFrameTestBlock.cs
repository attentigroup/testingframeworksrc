﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class DeleteTimeFrameTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgDeleteTimeFrameRequest DeleteTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ScheduleProxy.Instance.DeleteTimeFrame(DeleteTimeFrameRequest);
        }
    }

    public class DeleteTimeFrameTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgDeleteTimeFrameRequest DeleteTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ScheduleProxy_1.Instance.DeleteTimeFrame(DeleteTimeFrameRequest);
        }
    }

    public class DeleteTimeFrameTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgDeleteTimeFrameRequest DeleteTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ScheduleProxy_2.Instance.DeleteTimeFrame(DeleteTimeFrameRequest);
        }
    }
}
