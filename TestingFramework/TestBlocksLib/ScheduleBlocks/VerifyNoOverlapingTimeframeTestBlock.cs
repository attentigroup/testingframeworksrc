﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Schedule;

namespace TestBlocksLib.ScheduleBlocks
{
    public class VerifyNoOverlapingTimeframeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }
        protected override void ExecuteBlock()
        {
            if(GetScheduleResponse.Schedule.CalendarSchedule != null && GetScheduleResponse.Schedule.CalendarSchedule.Length > 0)
            {
                foreach(var timeframe in GetScheduleResponse.Schedule.CalendarSchedule)
                {
                    if(((timeframe.FromTime > StartTime) &&  (timeframe.FromTime < EndTime)) ||
                        ((timeframe.ToTime > StartTime) && (timeframe.ToTime < EndTime)))
                    {
                        ScheduleProxy.Instance.DeleteTimeFrame(
                            new Schedule0.EntMsgDeleteTimeFrameRequest()
                            {
                                TimeframeID = timeframe.ID,
                                EntityID = GetScheduleResponse.Schedule.EntityID
                            });
                    }
                }
            }
            if (GetScheduleResponse.Schedule.WeeklySchedule != null && GetScheduleResponse.Schedule.WeeklySchedule.Length > 0)
            {
                foreach (var timeframe in GetScheduleResponse.Schedule.WeeklySchedule)
                {
                    if (((StartTime > timeframe.FromTime) && (StartTime < timeframe.ToTime)) ||
                        ((EndTime > timeframe.FromTime) && (EndTime < timeframe.ToTime)))
                    {
                        ScheduleProxy.Instance.DeleteTimeFrame(
                            new Schedule0.EntMsgDeleteTimeFrameRequest()
                            {
                                TimeframeID = timeframe.ID,
                                EntityID = GetScheduleResponse.Schedule.EntityID
                            });
                    }
                }
            }
        }
    }
}
