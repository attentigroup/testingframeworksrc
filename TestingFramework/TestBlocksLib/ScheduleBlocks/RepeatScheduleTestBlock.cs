﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class RepeatScheduleTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public offenders0.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }
        protected override void ExecuteBlock()
        {
            ScheduleProxy.Instance.RepeatSchedule(RepeatScheduleRequest);
        }

        public override void CleanUp()
        {
            //Requests and Responses needed for the cleanup 
            Schedule0.EntMsgGetScheduleRequest getOffenderScheduleRequest = new Schedule0.EntMsgGetScheduleRequest();
            Schedule0.EntMsgGetScheduleResponse getOffenderScheduleResponse = new Schedule0.EntMsgGetScheduleResponse(); 
            Schedule0.EntMsgUpdateScheduleRequest updateSchedule = new Schedule0.EntMsgUpdateScheduleRequest();

            //Create Get Offender Schedule Request 
            getOffenderScheduleRequest.EntityID = AddOffenderResponse.NewOffenderID;
            getOffenderScheduleRequest.EntityType = Schedule0.EnmEntityType.Offender;
            getOffenderScheduleRequest.StartDate = DateTime.Now.Date;
            getOffenderScheduleRequest.EndDate = DateTime.Now.Date.AddDays(10);

            //Send Get Offender Schedule Request 
            getOffenderScheduleResponse = ScheduleProxy.Instance.GetSchedule(getOffenderScheduleRequest);

            //Create update schedule Request 
            updateSchedule.RFTimeStamp = getOffenderScheduleResponse.Schedule.RFScheduleTimeStamp;
            updateSchedule.EntityID = AddOffenderResponse.NewOffenderID;
            updateSchedule.EntityType = Schedule0.EnmEntityType.Offender;
            updateSchedule.Timeframes = null;

            //Send update schedule Request 
            ScheduleProxy.Instance.UpdateSchedule(updateSchedule);

        }
    }

    public class RepeatScheduleTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public offenders1.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }
        protected override void ExecuteBlock()
        {
            ScheduleProxy_1.Instance.RepeatSchedule(RepeatScheduleRequest);
        }

        public override void CleanUp()
        {
            //Requests and Responses needed for the cleanup 
            Schedule1.EntMsgGetScheduleRequest getOffenderScheduleRequest = new Schedule1.EntMsgGetScheduleRequest();
            Schedule1.EntMsgGetScheduleResponse getOffenderScheduleResponse = new Schedule1.EntMsgGetScheduleResponse();
            Schedule1.EntMsgUpdateScheduleRequest updateSchedule = new Schedule1.EntMsgUpdateScheduleRequest();

            //Create Get Offender Schedule Request 
            getOffenderScheduleRequest.EntityID = AddOffenderResponse.NewOffenderID;
            getOffenderScheduleRequest.EntityType = Schedule1.EnmEntityType.Offender;
            getOffenderScheduleRequest.StartDate = DateTime.Now.Date;
            getOffenderScheduleRequest.EndDate = DateTime.Now.Date.AddDays(10);

            //Send Get Offender Schedule Request 
            getOffenderScheduleResponse = ScheduleProxy_1.Instance.GetSchedule(getOffenderScheduleRequest);

            //Create update schedule Request 
            updateSchedule.RFTimeStamp = getOffenderScheduleResponse.Schedule.RFScheduleTimeStamp;
            updateSchedule.EntityID = AddOffenderResponse.NewOffenderID;
            updateSchedule.EntityType = Schedule1.EnmEntityType.Offender;
            updateSchedule.Timeframes = null;

            //Send update schedule Request 
            ScheduleProxy_1.Instance.UpdateSchedule(updateSchedule);

        }
    }

    public class RepeatScheduleTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public offenders2.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }
        protected override void ExecuteBlock()
        {
            ScheduleProxy_2.Instance.RepeatSchedule(RepeatScheduleRequest);
        }

        public override void CleanUp()
        {
            //Requests and Responses needed for the cleanup 
            Schedule2.EntMsgGetScheduleRequest getOffenderScheduleRequest = new Schedule2.EntMsgGetScheduleRequest();
            Schedule2.EntMsgGetScheduleResponse getOffenderScheduleResponse = new Schedule2.EntMsgGetScheduleResponse();
            Schedule2.EntMsgUpdateScheduleRequest updateSchedule = new Schedule2.EntMsgUpdateScheduleRequest();

            //Create Get Offender Schedule Request 
            getOffenderScheduleRequest.EntityID = AddOffenderResponse.NewOffenderID;
            getOffenderScheduleRequest.EntityType = Schedule2.EnmEntityType.Offender;
            getOffenderScheduleRequest.StartDate = DateTime.Now.Date;
            getOffenderScheduleRequest.EndDate = DateTime.Now.Date.AddDays(10);

            //Send Get Offender Schedule Request 
            getOffenderScheduleResponse = ScheduleProxy_2.Instance.GetSchedule(getOffenderScheduleRequest);

            //Create update schedule Request 
            updateSchedule.RFTimeStamp = getOffenderScheduleResponse.Schedule.RFScheduleTimeStamp;
            updateSchedule.EntityID = AddOffenderResponse.NewOffenderID;
            updateSchedule.EntityType = Schedule2.EnmEntityType.Offender;
            updateSchedule.Timeframes = null;

            //Send update schedule Request 
            ScheduleProxy_2.Instance.UpdateSchedule(updateSchedule);

        }
    }
}
