﻿using Common.CustomAttributes;
using Common.Enum;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;



#region API refs

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class DeleteTimeFrameByListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeIDsList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<int> TimeFrameIdList { get; set; }

        public DeleteTimeFrameByListTestBlock()
        {
            TimeFrameIdList = new List<int>();
        }
        protected override void ExecuteBlock()
        {
            var exceptions = new Dictionary<string, System.Exception>();

            var DeleteTimeFrameRequest = new Schedule0.EntMsgDeleteTimeFrameRequest();
            foreach (var tfi in TimeFrameIdList)
            {
                DeleteTimeFrameRequest.EntityID = EntityID;
                DeleteTimeFrameRequest.EntityType = EntityType;
                DeleteTimeFrameRequest.TimeframeID = tfi;

                try
                {
                    ScheduleProxy.Instance.DeleteTimeFrame(DeleteTimeFrameRequest);
                }
                catch (System.Exception exception)
                {
                    exceptions.Add(string.Format("Error while try to delete time-frame id {0} for entity id {1}", tfi, EntityID), exception);                    
                }
            }            
        }
    }
}
