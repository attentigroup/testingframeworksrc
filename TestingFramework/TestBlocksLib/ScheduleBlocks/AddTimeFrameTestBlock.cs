﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace TestBlocksLib.ScheduleBlocks
{
    public class AddTimeFrameTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgAddTimeFrameResponse AddTimeFrameResponse { get; set; }

        protected override void ExecuteBlock()
        {
            AddTimeFrameResponse = ScheduleProxy.Instance.AddTimeFrame(AddTimeFrameRequest);
        }

        public override void CleanUp()
        {
            Schedule0.EntMsgDeleteTimeFrameRequest deleteTimeFrameRequest = new Schedule0.EntMsgDeleteTimeFrameRequest();

            deleteTimeFrameRequest.EntityID = AddTimeFrameRequest.EntityID;
            deleteTimeFrameRequest.EntityType = Schedule0.EnmEntityType.Offender;
            deleteTimeFrameRequest.TimeframeID = AddTimeFrameResponse.NewTimeFrameID;

            /* Send Delete Time Frame Request */
            ScheduleProxy.Instance.DeleteTimeFrame(deleteTimeFrameRequest);
        }
    }


    public class AddTimeFrameTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }


        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public offenders1.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }



        protected override void ExecuteBlock()
        {
            ScheduleProxy_1.Instance.AddTimeFrame(AddTimeFrameRequest);
        }

        public override void CleanUp()
        {
            /* Requests and Responses needed for the cleanup */
            Schedule1.EntMsgGetScheduleRequest getOffenderScheduleRequest = new Schedule1.EntMsgGetScheduleRequest();
            Schedule1.EntMsgGetScheduleResponse getOffenderScheduleResponse = new Schedule1.EntMsgGetScheduleResponse();
            Schedule1.EntMsgDeleteTimeFrameRequest deleteTimeFrameRequest = new Schedule1.EntMsgDeleteTimeFrameRequest();

            /* Create Get Offender Schedule Request */
            getOffenderScheduleRequest.EntityID = AddOffenderResponse.NewOffenderID;
            getOffenderScheduleRequest.EntityType = Schedule1.EnmEntityType.Offender;
            getOffenderScheduleRequest.StartDate = DateTime.Now.Date;
            getOffenderScheduleRequest.EndDate = DateTime.Now.Date.AddYears(3);

            /* Send Get Offender Schedule Request */
            getOffenderScheduleResponse = ScheduleProxy_1.Instance.GetSchedule(getOffenderScheduleRequest);

            /* Create Delete Time Frame Request */
            Schedule1.EntTimeFrame[] timeframseArray = getOffenderScheduleResponse.Schedule.CalendarSchedule;
            int TimeframeID = timeframseArray[0].ID;

            deleteTimeFrameRequest.EntityID = AddOffenderResponse.NewOffenderID;
            deleteTimeFrameRequest.EntityType = Schedule1.EnmEntityType.Offender;
            deleteTimeFrameRequest.TimeframeID = TimeframeID;

            /* Send Delete Time Frame Request */
            ScheduleProxy_1.Instance.DeleteTimeFrame(deleteTimeFrameRequest);
        }
    }


    public class AddTimeFrameTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }


        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public offenders2.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }



        protected override void ExecuteBlock()
        {
            ScheduleProxy_2.Instance.AddTimeFrame(AddTimeFrameRequest);
        }

        public override void CleanUp()
        {
            /* Requests and Responses needed for the cleanup */
            Schedule2.EntMsgGetScheduleRequest getOffenderScheduleRequest = new Schedule2.EntMsgGetScheduleRequest();
            Schedule2.EntMsgGetScheduleResponse getOffenderScheduleResponse = new Schedule2.EntMsgGetScheduleResponse();
            Schedule2.EntMsgDeleteTimeFrameRequest deleteTimeFrameRequest = new Schedule2.EntMsgDeleteTimeFrameRequest();

            /* Create Get Offender Schedule Request */
            getOffenderScheduleRequest.EntityID = AddOffenderResponse.NewOffenderID;
            getOffenderScheduleRequest.EntityType = Schedule2.EnmEntityType.Offender;
            getOffenderScheduleRequest.StartDate = DateTime.Now.Date;
            getOffenderScheduleRequest.EndDate = DateTime.Now.Date.AddYears(3);

            /* Send Get Offender Schedule Request */
            getOffenderScheduleResponse = ScheduleProxy_2.Instance.GetSchedule(getOffenderScheduleRequest);

            /* Create Delete Time Frame Request */
            Schedule2.EntTimeFrame[] timeframseArray = getOffenderScheduleResponse.Schedule.CalendarSchedule;
            int TimeframeID = timeframseArray[0].ID;

            deleteTimeFrameRequest.EntityID = AddOffenderResponse.NewOffenderID;
            deleteTimeFrameRequest.EntityType = Schedule2.EnmEntityType.Offender;
            deleteTimeFrameRequest.TimeframeID = TimeframeID;

            /* Send Delete Time Frame Request */
            ScheduleProxy_2.Instance.DeleteTimeFrame(deleteTimeFrameRequest);
        }
    }
}
