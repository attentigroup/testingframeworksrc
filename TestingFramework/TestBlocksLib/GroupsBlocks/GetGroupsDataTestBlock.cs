﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Groups;

namespace TestBlocksLib.GroupsBlocks
{
    public class GetGroupsDataTestBlock : TestingBlockBase
    {


        [PropertyTest(EnumPropertyName.GetGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntMsgGetGroupDetailsRequest GetGroupDetailsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGroupDetailsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Groups0.EntMsgGetGroupDetailsResponse GetGroupDetailsResponse { get; set; }


        protected override void ExecuteBlock()
        {
            TestingFramework.Proxies.API.APIExtensions.APIExtensionsProxy.Instance.ClearAllCache();
            
            GetGroupDetailsResponse = GroupsProxy.Instance.GetGroupData(GetGroupDetailsRequest);
        }
    }
}
