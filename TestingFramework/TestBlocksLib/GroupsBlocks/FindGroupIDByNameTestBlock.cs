﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace TestBlocksLib.GroupsBlocks
{
    public class FindGroupIDByNameTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetGroupDetailsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntMsgGetGroupDetailsResponse GetGroupDetailsResponse { get; set; }

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int GroupID { get; set; }

        [PropertyTest(EnumPropertyName.OffendersList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Groups0.EntGroupOffender[] OffendersList { get; set; }

        [PropertyTest(EnumPropertyName.OffendersListIDs, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int[] OffendersListIDs { get; set; }

        protected override void ExecuteBlock()
        {
            

            if (GetGroupDetailsResponse == null || GetGroupDetailsResponse.GroupDetails.Length < 1)
                throw new Exception("GetGroupDetailsResponse is empty!");

            var groupsList = GetGroupDetailsResponse.GroupDetails.ToList();

            //Finds the first group in groupsList that has the GroupName and takes his ID and his offenders list
            var groupDetailes = groupsList.Find(x => x.Name.Equals(GroupName));
            //var groupDetailes = groupsList.First(x => x.Name.Equals(GroupName));
            GroupID = groupDetailes.ID;
            OffendersList = groupDetailes.OffendersList;

            OffendersListIDs = OffendersList.Select(x => x.ID).ToArray();

        }
    }
}
