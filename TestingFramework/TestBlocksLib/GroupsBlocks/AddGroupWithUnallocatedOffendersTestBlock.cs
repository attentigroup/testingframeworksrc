﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Groups;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace TestBlocksLib.GroupsBlocks
{
    public class AddGroupWithUnallocatedOffendersTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetUnallocatedOffenderListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntGroupOffender> GetUnallocatedOffenderListResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Groups0.EntMsgAddGroupDetailsRequest AddGroupDetailsRequest { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.ProgramGroup, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmProgramGroup ProgramGroup { get; set; }
        
        [PropertyTest(EnumPropertyName.OffendersListIDs, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int[] OffendersListIDs { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int GroupID { get; set; }

        protected override void ExecuteBlock()
        {
            if(GetUnallocatedOffenderListResponse.Count < Limit)
            {
                throw new Exception($"Got {GetUnallocatedOffenderListResponse.Count} Offenders, Limit was set to {Limit}");
            }

            AddGroupDetailsRequest = new Groups0.EntMsgAddGroupDetailsRequest();
            AddGroupDetailsRequest.Description = Description;
            AddGroupDetailsRequest.Name = GroupName;
            AddGroupDetailsRequest.OffendersList = OffendersListIDs;
            AddGroupDetailsRequest.ProgramGroup = (Groups0.EnmProgramGroup) ProgramGroup;

            GroupID = GroupsProxy.Instance.AddGroup(AddGroupDetailsRequest);
        }

        public override void CleanUp()
        {
            var deleteGroupRequest = new Groups0.EntMsgDeleteGroupDetailsRequest();
            deleteGroupRequest.GroupID = GroupID;
            GroupsProxy.Instance.DeleteGroup(deleteGroupRequest);
        }
    }
}
