﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Groups;

namespace TestBlocksLib.GroupsBlocks
{
    public class GetGroupWithOffendersTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? GroupID { get; set; }

        [PropertyTest(EnumPropertyName.GroupsWithOffenders, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<Groups0.EntGroup> GroupsWithOffendersLst { get; set; }

        [PropertyTest(EnumPropertyName.GetGroupDetailsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntMsgGetGroupDetailsResponse GetGroupDetailsResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GroupsWithOffendersLst = new List<Groups0.EntGroup>();
            foreach (var group in GetGroupDetailsResponse.GroupDetails)
            {
                if (group.OffendersList.Any())
                    GroupsWithOffendersLst.Add(group);
            }
        }
    }
}
