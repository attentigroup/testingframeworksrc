﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Groups;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace TestBlocksLib.GroupsBlocks
{
    public class DeleteGroupTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntMsgDeleteGroupDetailsRequest DeleteGroupDetailsRequest { get; set; }


        protected override void ExecuteBlock()
        {
            GroupsProxy.Instance.DeleteGroup(DeleteGroupDetailsRequest);
        }
    }
}
