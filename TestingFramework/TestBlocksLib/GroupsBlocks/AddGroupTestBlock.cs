﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Groups;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace TestBlocksLib.GroupsBlocks
{
    public class AddGroupTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntMsgAddGroupDetailsRequest AddGroupDetailsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int GroupID { get; set; }

        protected override void ExecuteBlock()
        {
            GroupID = GroupsProxy.Instance.AddGroup(AddGroupDetailsRequest);
        }

        public override void CleanUp()
        {
            var deleteGroupRequest = new Groups0.EntMsgDeleteGroupDetailsRequest();
            deleteGroupRequest.GroupID = GroupID;
            GroupsProxy.Instance.DeleteGroup(deleteGroupRequest);
        }
    }
}
