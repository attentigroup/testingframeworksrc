﻿using Common.CustomAttributes;
using Common.Enum;
using System.Threading;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib
{
    public class SleepTestingBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.SleepDurationSeconds, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int SleepDurationSeconds { get; set; }
        protected override void ExecuteBlock()
        {
            Thread.Sleep(SleepDurationSeconds * 1000);
        }
    }
}
