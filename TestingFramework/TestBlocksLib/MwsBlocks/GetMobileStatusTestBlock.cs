﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class GetMobileStatusTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgMwsGetMobileStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsGetMobileStatusRequest GetMobileStatusRequest { get; set; }

        protected override void ExecuteBlock()
        {
            MwsProxy.Instance.GetMobileStatus(GetMobileStatusRequest);
        }
    }
}
