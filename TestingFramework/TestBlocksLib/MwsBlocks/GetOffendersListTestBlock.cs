﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class GetOffendersListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgMwsGetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsGetOffendersListRequest GetOffendersListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            string getOffendersListResponse = MwsProxy.Instance.GetOffendersList(GetOffendersListRequest);

            if (getOffendersListResponse == null)
            {
                throw new Exception("Get Meta Data Response is empty!");
            }
        }
    }
}
