﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class ValidateLoginTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgValidateLoginRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsValidateLoginRequest ValidateLoginRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgMwsValidateLoginResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsValidateLoginResponse ValidateLoginResponse { get; set; }

        protected override void ExecuteBlock()
        {
            ValidateLoginResponse = MwsProxy.Instance.Login(ValidateLoginRequest);
        }
    }
}
