﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class RegisterTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgMwsRegisterRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsRegisterRequest RegisterRequest { get; set; }


        protected override void ExecuteBlock()
        {
            MwsProxy.Instance.RegisterMobile(RegisterRequest);
        }
    }
}
