﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class GetMetaDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgMwsGetMetaDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsGetMetaDataRequest GetMetaDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            string getMetaDataResponse = MwsProxy.Instance.GetMetaData(GetMetaDataRequest);

            if(getMetaDataResponse == null)
            {
                throw new Exception("Get Meta Data Response is empty!");
            }
        }
    }
}
