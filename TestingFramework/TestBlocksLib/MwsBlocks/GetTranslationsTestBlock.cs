﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class GetTranslationsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgMwsGetTranslationsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsGetTranslationsRequest GetTranslationsRequest { get; set; }

        protected override void ExecuteBlock()
        {
            MwsProxy.Instance.GetTranslations(GetTranslationsRequest);
        }
    }
}
