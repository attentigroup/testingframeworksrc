﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.MwsBlocks
{
    public class LogoutTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgMwsLogoutRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgMwsLogoutRequest LogoutRequest { get; set; }

        protected override void ExecuteBlock()
        {
            MwsProxy.Instance.Logout(LogoutRequest);
        }
    }
}
