﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class SendStartRangeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgSendStartRangeTestRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendStartRangeTestRequest EntMsgSendStartRangeTestRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ProgramActionsProxy.Instance.SendStartRangeTestRequest(EntMsgSendStartRangeTestRequest);
        }
    }
}
