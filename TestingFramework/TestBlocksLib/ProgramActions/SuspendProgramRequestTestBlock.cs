﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using System.Diagnostics;

namespace TestBlocksLib.ProgramActions
{
    public class SuspendProgramRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgSuspendProgramRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSuspendProgramRequest EntMsgSuspendProgramRequest { get; set; }

        [PropertyTest(EnumPropertyName.SuspendID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string SuspendID { get; set; }


        protected override void ExecuteBlock()
        {
            SuspendID = ProgramActionsProxy.Instance.SuspendProgram(EntMsgSuspendProgramRequest);
        }

    }
}
