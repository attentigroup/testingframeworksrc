﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class ReactivateProgramRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgReactivateProgramRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgReactivateProgramRequest EntMsgReactivateProgramRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ProgramActionsProxy.Instance.ReactivateProgram(EntMsgReactivateProgramRequest);
        }
    }
}
