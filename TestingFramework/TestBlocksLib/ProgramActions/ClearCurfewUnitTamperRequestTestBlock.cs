﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.ProgramActions
{
    public class ClearCurfewUnitTamperRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgClearCurfewUnitTamperRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgClearCurfewUnitTamperRequest EntMsgClearCurfewUnitTamperRequest { get; set; }

        protected override void ExecuteBlock()
        {
            //var a = OffendersProxy.Instance.GetOffenderCurrentStatus(new Offenders0.EntMsgGetOffenderCurrentStatusRequest() { OffenderID = 6652 });
            ProgramActionsProxy.Instance.ClearCurfewUnitTamper(EntMsgClearCurfewUnitTamperRequest);
        }
    }
}
