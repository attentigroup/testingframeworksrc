﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class GetEndOfServiceDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEndOfServiceDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgGetEndOfServiceDataRequest EntMsgGetEndOfServiceDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEndOfServiceDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetEndOfServiceDataResponse EntMsgGetEndOfServiceDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetEndOfServiceDataResponse = ProgramActionsProxy.Instance.GetEndOfServiceData(EntMsgGetEndOfServiceDataRequest);
        }
    }
}
