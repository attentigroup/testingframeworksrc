﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class GetManualEventOptionsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetManualEventOptionsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetManualEventOptionsResponse EntMsgGetManualEventOptionsResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetManualEventOptionsResponse = ProgramActionsProxy.Instance.GetManualEventOptions();
        }
    }
}
