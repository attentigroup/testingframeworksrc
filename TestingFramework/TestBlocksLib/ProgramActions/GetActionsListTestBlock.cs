﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class GetActionsListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetActionsListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetActionsListResponse EntMsgGetActionsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
             EntMsgGetActionsListResponse = ProgramActionsProxy.Instance.GetActionsList();
        }
    }
}
