﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace TestBlocksLib.ProgramActions
{
    public class StopOnlineModeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.StopOnlineModeRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgStopOnlineModeRequest StopOnlineModeRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ProgramActionsProxy.Instance.StopOnlineMode(StopOnlineModeRequest);
        }
    }
}
