﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class GetProgramSuspensionsDataRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetProgramSuspensionsDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgGetProgramSuspensionsDataRequest EntMsgGetProgramSuspensionsDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetProgramSuspensionsDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetProgramSuspensionsDataResponse EntMsgGetProgramSuspensionsDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetProgramSuspensionsDataResponse = ProgramActionsProxy.Instance.GetProgramSuspensionsData(EntMsgGetProgramSuspensionsDataRequest);
        }
    }
}
