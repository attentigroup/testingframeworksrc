﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using System.Threading;

namespace TestBlocksLib.ProgramActions
{
    public class SendDownloadRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgSendDownloadRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendDownloadRequest EntMsgSendDownloadRequest { get; set; }

        protected override void ExecuteBlock()
        {
            TestingFramework.Proxies.API.APIExtensions.APIExtensionsProxy.Instance.ClearAllCache();
            ProgramActionsProxy.Instance.SendDownloadRequest(EntMsgSendDownloadRequest);
        }
    }
}
