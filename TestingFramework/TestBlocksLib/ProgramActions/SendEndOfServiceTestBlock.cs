﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using System.Threading;

namespace TestBlocksLib.ProgramActions
{
    
    public class SendEndOfServiceTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgSendEndOfServiceRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendEndOfServiceRequest EntMsgSendEndOfServiceRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ProgramActionsProxy.Instance.SendEndOfServiceRequest(EntMsgSendEndOfServiceRequest);
            Log.Info($"EOS code: {EntMsgSendEndOfServiceRequest.EndOfServiceCode}");
        }
    }
}
