﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class SendUploadRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgSendUploadRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendUploadRequest EntMsgSendUploadRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ProgramActionsProxy.Instance.SendUploadRequest(EntMsgSendUploadRequest);
        }
    }
}
