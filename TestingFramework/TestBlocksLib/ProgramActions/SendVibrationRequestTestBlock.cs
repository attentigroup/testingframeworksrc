﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;


namespace TestBlocksLib.ProgramActions
{
    public class SendVibrationRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgSendVibrationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendVibrationRequest EntMsgSendVibrationRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ProgramActionsProxy.Instance.SendVibrationRequest(EntMsgSendVibrationRequest);
        }
    }
}
