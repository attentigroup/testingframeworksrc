﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using System.Collections.Generic;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestBlocksLib.ProgramActions
{
    public class GetSuspendProgramOptionsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetSuspendProgramOptionsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetSuspendProgramOptionsResponse EntMsgGetSuspendProgramOptionsResponse { get; set; }

        [PropertyTest(EnumPropertyName.ReactivateOptions, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Dictionary<string, string> ReactivateOptions { get; set; }

        [PropertyTest(EnumPropertyName.SuspendOptions, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Dictionary<string, string> SuspendOptions { get; set; }

        [PropertyTest(EnumPropertyName.ReactivateReason, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string ReactivateReason { get; set; }

        [PropertyTest(EnumPropertyName.SuspendReason, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string SuspendReason { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetSuspendProgramOptionsResponse = ProgramActionsProxy.Instance.GetSuspendProgramOptions();

            Assert.AreNotEqual(EntMsgGetSuspendProgramOptionsResponse.ReactivateOptions.Keys.Count, 0, "ReactivateOptions can't be empty" );
            Assert.AreNotEqual(EntMsgGetSuspendProgramOptionsResponse.SuspendOptions.Keys.Count, 0, "SuspendOptions can't be empty");

            foreach (var kvp in EntMsgGetSuspendProgramOptionsResponse.ReactivateOptions)
            {
                ReactivateReason = kvp.Key;
                break;
            }

            foreach (var kvp in EntMsgGetSuspendProgramOptionsResponse.SuspendOptions)
            {
                SuspendReason = kvp.Key;
                break;
            }
        }
    }
}
