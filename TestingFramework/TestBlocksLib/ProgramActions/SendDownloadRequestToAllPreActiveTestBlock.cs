﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using System.Threading;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;


namespace TestBlocksLib.ProgramActions
{
    public class SendDownloadRequestToAllPreActiveTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Immediate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool Immediate { get; set; }

        [PropertyTest(EnumPropertyName.IsMultipleDownload, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsMultipleDownload { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendDownloadRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgSendDownloadRequest EntMsgSendDownloadRequest { get; set; }

        protected override void ExecuteBlock()
        {
            TestingFramework.Proxies.API.APIExtensions.APIExtensionsProxy.Instance.ClearAllCache();

            for (int i = 0; i < GetOffendersResponse.OffendersList.Length; i++)
            {
                if (GetOffendersResponse.OffendersList[i].EquipmentInfo.ReceiverSN != null)
                {
                    EntMsgSendDownloadRequest = new ProgramActions0.EntMsgSendDownloadRequest();
                    EntMsgSendDownloadRequest.Immediate = Immediate;
                    EntMsgSendDownloadRequest.IsMultipleDownload = IsMultipleDownload;
                    EntMsgSendDownloadRequest.OffenderID = GetOffendersResponse.OffendersList[i].ID;
                    ProgramActionsProxy.Instance.SendDownloadRequest(EntMsgSendDownloadRequest);
                }
            }
        }
    }
}
