﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.ProgramActions
{
    public class GetEndOfServiceOptionsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEndOfServiceOptionsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetEndOfServiceOptionsResponse EntMsgGetEndOfServiceOptionsResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetEndOfServiceOptionsResponse = ProgramActionsProxy.Instance.GetEndOfServiceOptions();
        }
    }
}
