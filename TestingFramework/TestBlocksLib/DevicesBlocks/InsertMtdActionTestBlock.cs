﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.DevicesBlocks
{
    public class InsertMtdActionTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public long DeviceID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public long DemograhpicID { get; set; }

        [PropertyTest(EnumPropertyName.MtdAction, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Action { get; set; }

        [PropertyTest(EnumPropertyName.MtdActionCommandNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int CommandNumber { get; set; }

        [PropertyTest(EnumPropertyName.MtdActionCommandParam, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CommandParam { get; set; }

        [PropertyTest(EnumPropertyName.MtdActionSoftwareType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int SoftwareType { get; set; }

        
        protected override void ExecuteBlock()
        {
            //IStoredProcedureFormat spParams = new sndcds_MtdAct_InsMTDAct_001_SP()
            //{
            //    DeviceID = DeviceID,
            //    DemograhpicID = DemograhpicID,
            //    Action = Action,
            //    CommandNumber = CommandNumber,
            //    CommandParam = CommandParam,
            //    SoftwareType = SoftwareType
            //};
            //CommboxConfigurationDBProxy.Instance.RunSP(ref spParams);

            InsertMtdEventAction        insData = new InsertMtdEventAction();
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;

            dbProxy.RunQueryByArgs<InsertMtdEventAction>(typeof(InsertMtdEventAction), 
                DeviceID , Action , DemograhpicID ,CommandNumber, CommandParam , SoftwareType );
        }
    }
}
