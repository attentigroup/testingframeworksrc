﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.Device;
using TestingFramework.TestsInfraStructure.Implementation;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.DevicesBlocks
{
    public class RunXTSimulatorForGPSDeviceTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EquipmentSN { get; set; }

        [PropertyTest(EnumPropertyName.ExitCode, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int ExitCode { get; set; }

        [PropertyTest(EnumPropertyName.DB_IP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DB_IP { get; set; }

        [PropertyTest(EnumPropertyName.DB_Port, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DB_Port { get; set; }

        [PropertyTest(EnumPropertyName.Vcomm_IP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Vcomm_IP { get; set; }

        [PropertyTest(EnumPropertyName.Vcomm_Port, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Vcomm_Port { get; set; }

        [PropertyTest(EnumPropertyName.SHUTDOWNTIMER, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SHUTDOWNTIMER { get; set; }

        [PropertyTest(EnumPropertyName.Rule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Rule { get; set; }

        [PropertyTest(EnumPropertyName.LBS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LBS { get; set; }

        [PropertyTest(EnumPropertyName.NUMPOINTS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string NUMPOINTS { get; set; }

        [PropertyTest(EnumPropertyName.ACKCOUNT, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ACKCOUNT { get; set; }

        public static int timeout = 30000;

        public RunXTSimulatorForGPSDeviceTestBlock()
        {
            DB_IP = ConfigurationManager.AppSettings["DB_HostName"];
            DB_Port = ConfigurationManager.AppSettings["DB_Port"];
            Vcomm_IP = ConfigurationManager.AppSettings["GpsDeviceServerIP"];
            Vcomm_Port = ConfigurationManager.AppSettings["GpsDeviceServerPort"];
            SHUTDOWNTIMER = "30000";
            Rule = "Device_Tamper-first_violation;Device_Tamper-restore";
            LBS = "false";
            NUMPOINTS = "2";
            ACKCOUNT = "3";
        }

        public void Execute()
        {
            ExecuteBlock();
        }
        protected override void ExecuteBlock()
        {
            TimerCallback callback = new TimerCallback(new CheckStatus().KillSimulator);

            string dirBase = System.AppDomain.CurrentDomain.BaseDirectory;
            var simulator = "XT_Simulator.exe";

            var dir = Path.Combine(dirBase, @"xt_simulator");
            if (Directory.Exists(dir) == false)
            {
                var pathNotExist = dir;
                Log.Warn($"Can't find simulator at {dir} directory. try to combine different path.");
                dir = Path.Combine(dirBase, @"DevicesBlocks", @"xt_simulator");
                if (Directory.Exists(dir) == false)
                {
                    throw new Exception($"Cant find simulators and path {dir}");
                }
                else
                {
                    var fileFullPath = Path.Combine(dir, simulator);
                    if (File.Exists(fileFullPath) == false)
                    {
                        throw new Exception($"Cant find simulator at path {fileFullPath}");
                    }
                }
            }
            else
            {
                var fileFullPath = Path.Combine(dir, simulator);
                if (File.Exists(fileFullPath) == false)
                {
                    throw new Exception($"Cant find simulator at path {fileFullPath}");
                }
            }
            Log.Info($"XT Simulator directory: {dir}\\{simulator}");

            var args = string.Format("-DBSERVERIP {0} -DBPORT {1} -VCOMM {2} -PORT {3} -UNIT {4} -SHUTDOWNTIMER {5} -RULE {6} -LBS {7} -NUMPOINTS {8} -ACKCOUNT {9}", DB_IP, DB_Port, Vcomm_IP, Vcomm_Port, EquipmentSN, SHUTDOWNTIMER, Rule, LBS, NUMPOINTS, ACKCOUNT);

            Process process = new Process();
            process.StartInfo.WorkingDirectory = dir;
            process.StartInfo.FileName = simulator;
            process.StartInfo.Arguments = args;
            Timer timer = new Timer(callback, process, timeout, Timeout.Infinite);

            process.Start();
            process.WaitForExit();
            ExitCode = process.ExitCode;
            timer.Dispose();
            WebTests.SeleniumWrapper.Logger.WriteLine($"Running XTSimulator with args: {args} ; Exid code is {ExitCode}; Timeout = {timeout} milliseconds");
        }

        public class CheckStatus
        {
            public void KillSimulator(object stateInfo)
            {
                string message = "";

                var proc = stateInfo as Process;
                try
                {
                    proc.Kill();
                    message = $"XT simulator exited after {RunXTSimulatorTestBlock.timeout / 1000} seconds";
                }
                catch
                {
                    message = "Simulator kill failed";
                }

                Logger.WriteLine(message);
                Log.Info(message);
            }
        }
    }
}
