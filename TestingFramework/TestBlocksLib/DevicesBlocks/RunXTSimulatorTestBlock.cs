﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.Device;
using TestingFramework.TestsInfraStructure.Implementation;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.DevicesBlocks
{
    public class RunXTSimulatorTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EquipmentSN { get; set; }

        [PropertyTest(EnumPropertyName.ExitCode, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int ExitCode { get; set; }

        [PropertyTest(EnumPropertyName.DB_IP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DB_IP { get; set; }

        [PropertyTest(EnumPropertyName.DB_Port, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DB_Port { get; set; }

        [PropertyTest(EnumPropertyName.Vcomm_IP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Vcomm_IP { get; set; }

        [PropertyTest(EnumPropertyName.Vcomm_Port, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Vcomm_Port { get; set; }

        [PropertyTest(EnumPropertyName.SHUTDOWNTIMER, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SHUTDOWNTIMER { get; set; }

        [PropertyTest(EnumPropertyName.Rule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Rule { get; set; }

        [PropertyTest(EnumPropertyName.RuleState, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string RuleState { get; set; }
        
        [PropertyTest(EnumPropertyName.LBS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LBS { get; set; }

        [PropertyTest(EnumPropertyName.NUMPOINTS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string NUMPOINTS { get; set; }

        [PropertyTest(EnumPropertyName.ACKCOUNT, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ACKCOUNT { get; set; }

        [PropertyTest(EnumPropertyName.DevicesCount, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DevicesCount { get; set; }

        [PropertyTest(EnumPropertyName.CallInterval, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CallInterval { get; set; }

        [PropertyTest(EnumPropertyName.LoopSleep, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LoopSleep { get; set; }

        [PropertyTest(EnumPropertyName.DV, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DV { get; set; }

        [PropertyTest(EnumPropertyName.isDebug, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string isDebug { get; set; }

        [PropertyTest(EnumPropertyName.ConsoleDebug, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ConsoleDebug { get; set; }
        

        public static int timeout = 120000;

        public RunXTSimulatorTestBlock()
        {
            DB_IP = ConfigurationManager.AppSettings["DB_HostName"];
            DB_Port = ConfigurationManager.AppSettings["DB_Port"];
            Vcomm_IP = ConfigurationManager.AppSettings["GpsDeviceServerIP"];
            Vcomm_Port = ConfigurationManager.AppSettings["GpsDeviceServerPort"];
            SHUTDOWNTIMER = "3";
            Rule = "HW_NO_GPS;HW_DEVICE_TAMPER"/*;"HW_BTX_STRAP"*/;
            LBS = "false";
            NUMPOINTS = "4";
            ACKCOUNT = "3";
            DevicesCount = "1";
            CallInterval = "30";
            LoopSleep = "500";
            DV = "false";
            isDebug = "true";
            ConsoleDebug = "true";
            RuleState = "on";
        }

        public void Execute()
        {
            ExecuteBlock();
        }
        protected override void ExecuteBlock()
        {
            Thread.Sleep(3000);

            TimerCallback callback = new TimerCallback(new CheckStatus().KillSimulator);

            string dirBase = System.AppDomain.CurrentDomain.BaseDirectory;
            var simulator = "XT_Simulator.exe";

            var dir = Path.Combine(dirBase, @"xt_simulator");
            if (Directory.Exists(dir) == false)
            {
                var pathNotExist = dir;
                Log.Warn($"Can't find simulator at {dir} directory. try to combine different path.");
                dir = Path.Combine(dirBase, @"DevicesBlocks", @"xt_simulator");
                if (Directory.Exists(dir) == false)
                {
                    throw new Exception($"Cant find simulators and path {dir}");
                }
                else
                {
                    var fileFullPath = Path.Combine(dir, simulator);
                    if (File.Exists(fileFullPath) == false)
                    {
                        throw new Exception($"Cant find simulator at path {fileFullPath}");
                    }
                }
            }
            else
            {
                var fileFullPath = Path.Combine(dir, simulator);
                if (File.Exists(fileFullPath) == false)
                {
                    throw new Exception($"Cant find simulator at path {fileFullPath}");
                }
            }
            Log.Info($"XT Simulator directory: {dir}\\{simulator}");

            var args = $"-DBServerIP {DB_IP} " +
                $"-DBServerPort {DB_Port} " +
                $"-CommBoxIP {Vcomm_IP} " +
                $"-CommBoxPort {Vcomm_Port} " +
                $"-UnitSN {EquipmentSN}" +
                $" -FirstUnitSN {0} " +
                $"-ShutDownTimer {SHUTDOWNTIMER} " +
                $"-Rule {Rule}={RuleState} " +
                $"-LBS {LBS} " +
                $"-PointsNumber {NUMPOINTS} " +
                $"-ACKCount {ACKCOUNT} " +
                $"-DevicesCount {DevicesCount}" +
                $" -CallInterval {CallInterval} " +
                $"-LoopSleep {LoopSleep} " +
                $"-DV {DV} " +
                $"-Debug {isDebug} " +
                $"-ConsoleDebug {ConsoleDebug} ";

            RenameXtSimulatorLogsFileDirectory(dir);

            Log.Info($"Going to run simulator. file name {simulator}, args = {args}");

            
            Process process = new Process
            {
                StartInfo = new ProcessStartInfo(simulator)
                {
                    WorkingDirectory = dir,
                    Arguments = args,
                    Verb = "runas",
                    //UseShellExecute = false,
                    //CreateNoWindow = true,
                    //RedirectStandardOutput = true,
                    //RedirectStandardError = true,
                    //RedirectStandardInput = true
                }
            };

            Timer timer = new Timer(callback, process, timeout, Timeout.Infinite);

            //process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.UseShellExecute = false;
            process.Start();

            //while (!process.StandardOutput.EndOfStream)
            //{
            //    string line = process.StandardOutput.ReadLine();
            //    Log.Info($"Simulator output: {line}");
            //}

            process.WaitForExit();
            ExitCode = process.ExitCode;
            timer.Dispose();

            Log.Info($"XT Simulator Exit Code: {ExitCode}");
            //WebTests.SeleniumWrapper.Logger.WriteLine($"Running XTSimulator with args: {args} ; Exid code is {ExitCode}; Timeout = {timeout} milliseconds");
            ReadXtSimulatorLogs(dir);
        }

        private void RenameXtSimulatorLogsFileDirectory(string directory)
        {
            Log.Info($"Start rename xt_simulator log files {directory}");
            var logFileDir = Path.Combine(directory, @"Log Files");
            if (Directory.Exists(logFileDir))
            {
                var newLogFileDirName = logFileDir + "_" + DateTime.Now.ToBinary();
                try
                {
                    Log.Info($"rename xt_simulator log files from { logFileDir} to { newLogFileDirName}");
                    Directory.Move(logFileDir, newLogFileDirName);
                }
                catch (Exception ex)
                {
                    Log.Error($"Error while rename xt_simulator log files from {logFileDir} to {newLogFileDirName}. Error:{ex.Message}");
                }
            }
            Log.Info($"Finish rename xt_simulator log files {directory}");
        }

        private void ReadXtSimulatorLogs(string dir)
        {
            var logFileDir = Path.Combine(dir, @"Log Files");
            if( Directory.Exists(logFileDir))
            {
                foreach (var file in Directory.EnumerateFiles(logFileDir, "*"))
                {
                    var lines = File.ReadAllLines(file);
                    foreach (var line in lines)
                    {
                        Log.Debug($"Line from XT Simulator Log:{line}");
                    }
                }
            }
        }

        public class CheckStatus
        {
            public void KillSimulator(object stateInfo)
            {
                string message = "";
                var proc = stateInfo as Process;

                try
                {
                    proc.Kill();
                    message = $"XT simulator exited successfully after {RunXTSimulatorTestBlock.timeout / 1000} seconds";
                }
                catch
                {
                    message = "Simulator kill failed";
                }
               
                Logger.WriteLine(message);
                Log.Info(message);
            }
        }
    }
}
