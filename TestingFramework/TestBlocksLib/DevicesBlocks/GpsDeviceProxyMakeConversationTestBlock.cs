﻿using Common.CustomAttributes;
using Common.Enum;
using System.Text;
using TestingFramework.Proxies.Device;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

#endregion

namespace TestBlocksLib.DevicesBlocks
{
    public class GpsDeviceProxyMakeConversationTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Encoding, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Encoding Encoding { get; set; }

        [PropertyTest(EnumPropertyName.DeviceApplication, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DeviceApplication { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.None)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxyPointsCount, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int PointsCount { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxySendHwEvents, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SendHwEvents { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxyAGPSSupport, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AGPSSupport { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxyDeviceAGPSFileAgeOffsetMinutes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public uint DeviceAGPSFileAgeOffsetMinutes { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxyNetworkIssuesDescription, EnumPropertyType.None, EnumPropertyModifier.None)]
        public NetworkIssuesDescription NetworkIssuesDescription { get; set; }
        

        public GpsDeviceProxyMakeConversationTestBlock()
        {
            Encoding = Encoding.Unicode;
            DeviceApplication = "V5.12.10.49";//because i can... "V5.12.10.31"
            PointsCount = GpsDeviceProxy.DEFAULT_POINTS_COUNT;
            SendHwEvents = false;
            AGPSSupport = false;
            DeviceAGPSFileAgeOffsetMinutes = 0;
            EquipmentID = -1;
        }
        protected override void ExecuteBlock()
        {
            if( EquipmentID == -1)
            {
                EquipmentID = int.Parse(EquipmentSerialNumber);
            }
            if (GpsDeviceProxy == null)
                GpsDeviceProxy = new GpsDeviceProxy();
            GpsDeviceProxy.DeviceNetworkObject.NetworkIssuesDescription = NetworkIssuesDescription;
            GpsDeviceProxy.MakeConversation(EquipmentID, (uint)OffenderID, Encoding, DeviceApplication, PointsCount, SendHwEvents, AGPSSupport, DeviceAGPSFileAgeOffsetMinutes);
        }
    }
}
