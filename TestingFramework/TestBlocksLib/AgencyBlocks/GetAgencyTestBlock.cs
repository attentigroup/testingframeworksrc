﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Agency;
using TestingFramework.TestsInfraStructure.Implementation;
using Agency = TestingFramework.Proxies.EM.Interfaces.Agency;

namespace TestBlocksLib.AgencyBlocks
{
    public class GetAgencyTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.GetAgenciesRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Agency.EntMsgGetAgenciesRequest GetAgenciesRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetAgenciesResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Agency.EntMsgGetAgenciesResponse GetAgenciesResponse { get; set; }


        protected override void ExecuteBlock()
        {

           GetAgenciesResponse = AgencyProxy.Instance.GetAgencies(new Agency.EntMsgGetAgenciesRequest()
           {
               AgencyID = AgencyID
           });
        }
    }
}
