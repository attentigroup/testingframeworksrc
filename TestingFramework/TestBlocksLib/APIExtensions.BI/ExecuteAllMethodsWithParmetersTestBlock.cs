﻿using TestingFramework.Proxies.API.APIExtensions.BI;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions.BI
{
    /// <summary>
    /// execute all methods with parameters in the BI API
    /// </summary>
    public class ExecuteAllMethodsWithParmetersTestBlock : TestingBlockBase
    {
        /// <summary>
        /// calling the method that run all methods parameters.
        /// </summary>
        protected override void ExecuteBlock()
        {
            APIExtensionsBiProxy.Instance.ExecuteAllMethodsWithParameters();
        }
    }
}
