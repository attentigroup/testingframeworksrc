﻿using TestingFramework.Proxies.API.APIExtensions.BI;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions.BI
{
    /// <summary>
    /// execute all methods without parameters in the BI API
    /// </summary>
    public class ExecuteAllNonParmetersMethodsTestBlock : TestingBlockBase
    {
        /// <summary>
        /// calling the method that run all methods without parameters.
        /// </summary>
        protected override void ExecuteBlock()
        {
            APIExtensionsBiProxy.Instance.ExecuteAllNonParmetersMethods();
        }
    }
}
