﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.TestingFrameworkBlocks
{
    /// <summary>
    /// this block will count the number of time that the block executed
    /// </summary>
    public class ExecutionCounterTestBlock : TestingBlockBase
    {
        public static int Counter { get; set; }

        public ExecutionCounterTestBlock()
        {
            Counter = 0;
        }
        protected override void ExecuteBlock()
        {
            Counter++;
        }
    }
}
