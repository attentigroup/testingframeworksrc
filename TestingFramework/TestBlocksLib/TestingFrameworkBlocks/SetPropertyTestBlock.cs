﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.TestingFrameworkBlocks
{
    public class SetPropertyTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.PropertyValue, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string PropertyValue { get; set; }

        [PropertyTest(EnumPropertyName.OutputPropertyValue, EnumPropertyType.String, EnumPropertyModifier.Output)]
        public string OutputPropertyValue { get; set; }


        protected override void ExecuteBlock()
        {
            //TODO Log the property value
            _propertyValue = PropertyValue;

            OutputPropertyValue = _propertyValue;//just to set some value
        }

        private static string _propertyValue;
        public static string GetPropertyValue()
        {
            return _propertyValue;
        }
    }
}
