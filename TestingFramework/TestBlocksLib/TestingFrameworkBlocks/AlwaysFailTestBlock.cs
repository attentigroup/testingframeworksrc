﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.TestingFrameworkBlocks
{
    public class AlwaysFailTestBlock : TestingBlockBase
    {
        protected override void ExecuteBlock()
        {
            throw new NotImplementedException();
        }
    }
}
