﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.TestingFrameworkBlocks
{
    public class CleanupTestBlock : TestingBlockBase
    {
        public const string CLEANUP_MSG_FORMAT = "cleanup called for block name {0} description {1}";
        public string CleanupResult { get; set; }
        public DateTime CleanupDateTime { get; set; }
        protected override void ExecuteBlock()
        {
            Log.DebugFormat("execute cleanup test block for block name {0} desc {1}", BlockName, BlockDescription);
            Thread.Sleep(1000);
        }
        public override void CleanUp()
        {   //in order to able to identify the execution time correct we need 
            //to set the cleanup proccess to take some time and not be short
            Thread.Sleep(1000);
            CleanupResult = string.Format(CLEANUP_MSG_FORMAT, BlockName, BlockDescription);
            CleanupDateTime = DateTime.Now;
        }
    }
}
