﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.TestingFrameworkBlocks
{
    public class SetPropertyCollectionTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Index2Set, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Index2Set { get; set; }

        [PropertyTest(EnumPropertyName.Value2Set, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Value2Set { get; set; }

        [PropertyTest(EnumPropertyName.StringsArray, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string[] StringsArray { get; set; }

        [PropertyTest(EnumPropertyName.ListLength, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ListLength { get; set; }

        [PropertyTest(EnumPropertyName.StringsList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<string> StringsList { get; set; }

        protected override void ExecuteBlock()
        {
            //Array
            Assert.IsTrue(StringsArray.Length > 0, "string array length expected to be more then 0(actual {0})", StringsArray.Length);
            Assert.IsTrue(StringsArray.Length > Index2Set, "string array length expected to be at least {0} (actual {0})", Index2Set, StringsArray.Length);
            Assert.AreEqual(Value2Set, StringsArray[Index2Set], "string at position {0} ({1}) not as expected({2}", Index2Set, StringsArray[Index2Set], Value2Set);

            //List
            Assert.AreEqual(StringsList.Count, ListLength, "string list length expected to be {0} (actual {0})", ListLength, StringsList.Count);
            Assert.AreEqual(Value2Set, StringsList[Index2Set], "string at position {0} ({1}) not as expected({2}", Index2Set, StringsList[Index2Set], Value2Set);
        }
    }
}
