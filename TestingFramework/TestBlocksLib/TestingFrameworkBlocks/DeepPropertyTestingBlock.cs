﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.TestingFrameworkBlocks
{
    public class DeepPropertyClass
    {
        public int DeeperProperty { get; set; }
    }

    public class DeepPropertyTestingBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ListLength, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ListLength { get; set; }

        [PropertyTest(EnumPropertyName.StringsList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<string> StringsList { get; set; }

        [PropertyTest(EnumPropertyName.DeepProperty, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DeepPropertyClass DeepProperty { get; set; }

        public DeepPropertyTestingBlock()
        {
            DeepProperty = new DeepPropertyClass();
        }

        protected override void ExecuteBlock()
        {
            for (int i = 0; i < ListLength; i++)
            {
                Log.DebugFormat("list index:{0} value:{1}", i, StringsList[i]);
            }
        }
    }
}
