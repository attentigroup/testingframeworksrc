﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
    public class UpdateGPSOffenderConfigurationDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ConfigurationProxy.Instance.UpdateGPSOffenderConfigurationData(UpdateGPSOffenderConfigurationDataRequest);
        }
    }

    public class UpdateGPSOffenderConfigurationDataTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ConfigurationProxy_1.Instance.UpdateGPSOffenderConfigurationData(UpdateGPSOffenderConfigurationDataRequest);
        }
    }

    public class UpdateGPSOffenderConfigurationDataTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ConfigurationProxy_2.Instance.UpdateGPSOffenderConfigurationData(UpdateGPSOffenderConfigurationDataRequest);
        }
    }
}
