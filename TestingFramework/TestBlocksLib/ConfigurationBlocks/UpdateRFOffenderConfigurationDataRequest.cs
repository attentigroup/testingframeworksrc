﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
  public class UpdateRFOffenderConfigurationDataRequestTestsBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }


        protected override void ExecuteBlock()
        {

            ConfigurationProxy.Instance.UpdateRFOffenderConfigurationData(UpdateRFOffenderConfigurationDataRequest);
        }
    }

    public class UpdateRFOffenderConfigurationDataRequestTestsBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }


        protected override void ExecuteBlock()
        {

            ConfigurationProxy_1.Instance.UpdateRFOffenderConfigurationData(UpdateRFOffenderConfigurationDataRequest);
        }
    }

    public class UpdateRFOffenderConfigurationDataRequestTestsBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }


        protected override void ExecuteBlock()
        {

            ConfigurationProxy_2.Instance.UpdateRFOffenderConfigurationData(UpdateRFOffenderConfigurationDataRequest);
        }
    }
}
