﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion
namespace TestBlocksLib.ConfigurationBlocks
{
   public class ResetGPSRuleConfigurationRequestTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgResetGPSRuleConfigurationRequest ResetGPSRuleConfigurationRequest { get; set; }
      
        protected override void ExecuteBlock()
        {   
                ConfigurationProxy.Instance.ResetGPSRuleConfiguration(ResetGPSRuleConfigurationRequest);
        }
    }

    public class ResetGPSRuleConfigurationRequestTestBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgResetGPSRuleConfigurationnRequest ResetGPSRuleConfigurationRequest { get; set; }

        protected override void ExecuteBlock()
        {
                ConfigurationProxy_1.Instance.ResetGPSRuleConfiguration(ResetGPSRuleConfigurationRequest);
        }
    }

    public class ResetGPSRuleConfigurationRequestTestBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgResetGPSRuleConfigurationnRequest ResetGPSRuleConfigurationRequest { get; set; }

        protected override void ExecuteBlock()
        {
                ConfigurationProxy_2.Instance.ResetGPSRuleConfiguration(ResetGPSRuleConfigurationRequest);  
        }
    }
}
