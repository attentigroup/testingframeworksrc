﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
    public class GetHandlingProcedureTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetHandlingProcedureRequest GetHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingProcedureResponse = ConfigurationProxy.Instance.GetHandlingProcedure(GetHandlingProcedureRequest);
        }
    }

    public class GetHandlingProcedureTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetHandlingProcedureRequest GetHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingProcedureResponse = ConfigurationProxy_1.Instance.GetHandlingProcedure(GetHandlingProcedureRequest);
        }
    }

    public class GetHandlingProcedureTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetHandlingProcedureRequest GetHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingProcedureResponse = ConfigurationProxy_2.Instance.GetHandlingProcedure(GetHandlingProcedureRequest);
        }
    }
}
