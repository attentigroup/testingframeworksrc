﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
    public class UpdateGPSRuleConfigurationRequestTestsBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }


        protected override void ExecuteBlock()
        {
            try
            {
                ConfigurationProxy.Instance.UpdateGPSRuleConfiguration(UpdateGPSRuleConfigurationRequest);
            }
            catch
            {
                ConfigurationProxy.Instance.UpdateGPSRuleConfiguration(UpdateGPSRuleConfigurationRequest);                
            }
            
        }
        public override void CleanUp()
        {
            var resetToPrevioesParams = new Configuration0.EntMsgResetGPSRuleConfigurationRequest();
            resetToPrevioesParams.OffenderID = UpdateGPSRuleConfigurationRequest.OffenderID;
            resetToPrevioesParams.RuleID = UpdateGPSRuleConfigurationRequest.RuleID;
            ConfigurationProxy.Instance.ResetGPSRuleConfiguration(resetToPrevioesParams);

        }

    }

    public class UpdateGPSRuleConfigurationRequestTestsBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ConfigurationProxy_1.Instance.UpdateGPSRuleConfiguration(UpdateGPSRuleConfigurationRequest);
        }

        public override void CleanUp()
        {
            var resetToPrevioesParams = new Configuration1.EntMsgResetGPSRuleConfigurationnRequest();
            resetToPrevioesParams.OffenderID = UpdateGPSRuleConfigurationRequest.OffenderID;
            resetToPrevioesParams.RuleID = UpdateGPSRuleConfigurationRequest.RuleID;
            ConfigurationProxy_1.Instance.ResetGPSRuleConfiguration(resetToPrevioesParams);
   
        }

    }

    public class UpdateGPSRuleConfigurationRequestTestsBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ConfigurationProxy_2.Instance.UpdateGPSRuleConfiguration(UpdateGPSRuleConfigurationRequest);
        }

        public override void CleanUp()
        {
            var resetToPrevioesParams = new Configuration2.EntMsgResetGPSRuleConfigurationnRequest();
            resetToPrevioesParams.OffenderID = UpdateGPSRuleConfigurationRequest.OffenderID;
            resetToPrevioesParams.RuleID = UpdateGPSRuleConfigurationRequest.RuleID;
            ConfigurationProxy_2.Instance.ResetGPSRuleConfiguration(resetToPrevioesParams);


        }

    }
}
