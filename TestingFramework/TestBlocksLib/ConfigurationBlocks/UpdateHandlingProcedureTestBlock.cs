﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
    public class UpdateHandlingProcedureTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            ConfigurationProxy.Instance.UpdateHandlingProcedure(UpdateHandlingProcedureRequest);
        }
    }

    public class UpdateHandlingProcedureTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            ConfigurationProxy_1.Instance.UpdateHandlingProcedure(UpdateHandlingProcedureRequest);
        }
    }

    public class UpdateHandlingProcedureTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            ConfigurationProxy_2.Instance.UpdateHandlingProcedure(UpdateHandlingProcedureRequest);
        }
    }
}
