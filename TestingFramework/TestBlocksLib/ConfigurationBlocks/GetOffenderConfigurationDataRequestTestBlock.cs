﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
    public class GetOffenderConfigurationDataRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderConfigurationDataResponse = ConfigurationProxy.Instance.GetOffenderConfigurationData(GetOffenderConfigurationDataRequest);
           
        }
    }


   public class GetOffenderConfigurationDataRequestTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderConfigurationDataResponse = ConfigurationProxy_1.Instance.GetOffenderConfigurationData(GetOffenderConfigurationDataRequest);

        }
    }

   public class GetOffenderConfigurationDataRequestTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderConfigurationDataResponse = ConfigurationProxy_2.Instance.GetOffenderConfigurationData(GetOffenderConfigurationDataRequest);

        }
    }

}
