﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.ConfigurationBlocks
{
   public class GetGPSRuleConfigurationListRequestTestBlocks : TestingBlockBase
    {
        //TODO Oren: to add a new block with enam with a new name GetGPSRuleConfigurationListResponse for system that will give a 2 para, that can be equalanted
        //and then fixed the assert that should be have the new params.
        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListRequest GetGPSRuleConfigurationListRequest { get; set; }
        
        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListDefaultResponse { get; set; }
        

        protected override void ExecuteBlock()
        {
            GetGPSRuleConfigurationListResponse = ConfigurationProxy.Instance.GetGPSRuleConfigurationList(GetGPSRuleConfigurationListRequest);
            GetGPSRuleConfigurationListDefaultResponse = ConfigurationProxy.Instance.GetGPSRuleConfigurationList(new Configuration0.EntMsgGetGPSRuleConfigurationListRequest() { OffenderID = 0 });
            //ConfigurationProxy.Instance.GetGPSRuleConfigurationList(GetGPSRuleConfigurationListRequest);
        }
    }

    public class GetGPSRuleConfigurationListRequestTestBlocks_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListRequest GetGPSRuleConfigurationListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListDefaultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            

            GetGPSRuleConfigurationListResponse = ConfigurationProxy_1.Instance.GetGPSRuleConfigurationList(GetGPSRuleConfigurationListRequest);
            GetGPSRuleConfigurationListDefaultResponse = ConfigurationProxy_1.Instance.GetGPSRuleConfigurationList(new Configuration1.EntMsgGetGPSRuleConfigurationListRequest() { OffenderID = 0 });
        }
    }

    public class GetGPSRuleConfigurationListRequestTestBlocks_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListRequest GetGPSRuleConfigurationListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListDefaultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetGPSRuleConfigurationListResponse = ConfigurationProxy_2.Instance.GetGPSRuleConfigurationList(GetGPSRuleConfigurationListRequest);
            GetGPSRuleConfigurationListDefaultResponse = ConfigurationProxy_2.Instance.GetGPSRuleConfigurationList(new Configuration2.EntMsgGetGPSRuleConfigurationListRequest() { OffenderID = 0 });
        }
    }
}

