﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Offenders;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace TestBlocksLib.OffendersBlocks
{
    public class StringToEntStringParameterConverter : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ElementToConvert, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ElementToConvert { get; set; }

        [PropertyTest(EnumPropertyName.ConvertedValue, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntStringParameter ConvertedValue { get; set; }

        [PropertyTest(EnumPropertyName.Operator, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmStringOperator Operator { get; set; }

        public StringToEntStringParameterConverter()
        {
            Operator = Offenders0.EnmStringOperator.Equal;
        }

        protected override void ExecuteBlock()
        {
            ConvertedValue = new Offenders0.EntStringParameter() { Operator = Operator, Value = ElementToConvert };
        }

    }
}
