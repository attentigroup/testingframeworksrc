﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class UpdateOffendersProgramTimeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            try
            {
                for (int i = 0; i < GetOffendersResponse.OffendersList.Length; i++)
                {

                    Offenders0.EntMsgUpdateOffenderRequest UpdateOffenderRequest = new Offenders0.EntMsgUpdateOffenderRequest();
                    UpdateOffenderRequest.ProgramStart = (DateTime)GetOffendersResponse.OffendersList[i].ProgramStart;
                    UpdateOffenderRequest.ProgramEnd = DateTime.Now.AddYears(1); //Extand the program end date by 3 years
                    UpdateOffenderRequest.OffenderID = GetOffendersResponse.OffendersList[i].ID;
                    
                    UpdateOffenderRequest.AgencyID = GetOffendersResponse.OffendersList[i].AgencyID;
                    UpdateOffenderRequest.AllowVoiceTests = (bool) GetOffendersResponse.OffendersList[i].AllowVoiceTests;
                    UpdateOffenderRequest.CityID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].CityID;
                    UpdateOffenderRequest.ContactPhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePhoneNumber;
                    UpdateOffenderRequest.ContactPrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePrefixPhone;
                    UpdateOffenderRequest.CourtOrderNumber = GetOffendersResponse.OffendersList[i].CourtOrderNumber;
                    UpdateOffenderRequest.DateOfBirth = GetOffendersResponse.OffendersList[i].DateOfBirth;
                    UpdateOffenderRequest.DepartmentOfCorrection = GetOffendersResponse.OffendersList[i].DepartmentOfCorrection;
                    UpdateOffenderRequest.FirstName = GetOffendersResponse.OffendersList[i].FirstName;
                    UpdateOffenderRequest.Gender = GetOffendersResponse.OffendersList[i].Gender;
                    UpdateOffenderRequest.HomeUnit = GetOffendersResponse.OffendersList[i].EquipmentInfo.HomeUnitSN;
                    if (GetOffendersResponse.OffendersList[i].ContactInfo.LandLine != null)
                    {
                        UpdateOffenderRequest.LandlineDialOutPrefix = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.DialOutPrefix;
                        UpdateOffenderRequest.LandlinePhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.Phone;
                        UpdateOffenderRequest.LandlinePrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.PrefixPhone;
                    }
                    UpdateOffenderRequest.Language = GetOffendersResponse.OffendersList[i].Language;
                    UpdateOffenderRequest.LastName = GetOffendersResponse.OffendersList[i].LastName;
                    UpdateOffenderRequest.MiddleName = GetOffendersResponse.OffendersList[i].MiddleName;

                    UpdateOffenderRequest.OfficerID = GetOffendersResponse.OffendersList[i].OfficerID;
                    UpdateOffenderRequest.PhotoID = GetOffendersResponse.OffendersList[i].PhotoID;
                    UpdateOffenderRequest.ProgramType = (Offenders0.EnmProgramType) GetOffendersResponse.OffendersList[i].ProgramType;
                    UpdateOffenderRequest.Receiver = GetOffendersResponse.OffendersList[i].EquipmentInfo.ReceiverSN;
                    UpdateOffenderRequest.RelatedOffenderID = GetOffendersResponse.OffendersList[i].RelatedOffenderID;
                    UpdateOffenderRequest.SocialSecurity = GetOffendersResponse.OffendersList[i].SocialSecurity;
                    UpdateOffenderRequest.StateID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].StateID;
                    UpdateOffenderRequest.Street = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].Street;
                    UpdateOffenderRequest.Timestamp = GetOffendersResponse.OffendersList[i].Timestamp;
                    UpdateOffenderRequest.Title = GetOffendersResponse.OffendersList[i].Title;
                    UpdateOffenderRequest.Transmitter = GetOffendersResponse.OffendersList[i].EquipmentInfo.TransmitterSN;
                    UpdateOffenderRequest.ZipCode = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].ZipCode;
                    
                    OffendersProxy.Instance.UpdateOffender(UpdateOffenderRequest);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e + "GetOffendersResponse.Offenders List is empty!");
            }
        }
    }


    public class UpdateOffendersProgramTimeTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            try
            {
                for (int i = 0; i < GetOffendersResponse.OffendersList.Length; i++)
                {

                    Offenders1.EntMsgUpdateOffenderRequest UpdateOffenderRequest = new Offenders1.EntMsgUpdateOffenderRequest();
                    UpdateOffenderRequest.ProgramStart = (DateTime)GetOffendersResponse.OffendersList[i].ProgramStart;
                    UpdateOffenderRequest.ProgramEnd = DateTime.Now.AddYears(2); //Extand the program end date by 2 years
                    UpdateOffenderRequest.OffenderID = GetOffendersResponse.OffendersList[i].ID;
                    
                    UpdateOffenderRequest.AgencyID = GetOffendersResponse.OffendersList[i].AgencyID;
                    UpdateOffenderRequest.AllowVoiceTests = (bool)GetOffendersResponse.OffendersList[i].AllowVoiceTests;
                    UpdateOffenderRequest.CityID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].CityID;
                    UpdateOffenderRequest.ContactPhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePhoneNumber;
                    UpdateOffenderRequest.ContactPrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePrefixPhone;
                    UpdateOffenderRequest.CourtOrderNumber = GetOffendersResponse.OffendersList[i].CourtOrderNumber;
                    UpdateOffenderRequest.DateOfBirth = GetOffendersResponse.OffendersList[i].DateOfBirth;
                    UpdateOffenderRequest.DepartmentOfCorrection = GetOffendersResponse.OffendersList[i].DepartmentOfCorrection;
                    UpdateOffenderRequest.FirstName = GetOffendersResponse.OffendersList[i].FirstName;
                    UpdateOffenderRequest.Gender = GetOffendersResponse.OffendersList[i].Gender;
                    UpdateOffenderRequest.HomeUnit = GetOffendersResponse.OffendersList[i].EquipmentInfo.HomeUnitSN;
                    if (GetOffendersResponse.OffendersList[i].ContactInfo.LandLine != null)
                    {
                        UpdateOffenderRequest.LandlineDialOutPrefix = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.DialOutPrefix;
                        UpdateOffenderRequest.LandlinePhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.Phone;
                        UpdateOffenderRequest.LandlinePrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.PrefixPhone;
                    }
                    UpdateOffenderRequest.Language = GetOffendersResponse.OffendersList[i].Language;
                    UpdateOffenderRequest.LastName = GetOffendersResponse.OffendersList[i].LastName;
                    UpdateOffenderRequest.MiddleName = GetOffendersResponse.OffendersList[i].MiddleName;

                    UpdateOffenderRequest.OfficerID = GetOffendersResponse.OffendersList[i].OfficerID;
                    UpdateOffenderRequest.PhotoID = GetOffendersResponse.OffendersList[i].PhotoID;
                    UpdateOffenderRequest.ProgramType = (Offenders1.EnmProgramType)GetOffendersResponse.OffendersList[i].ProgramType;
                    UpdateOffenderRequest.Receiver = GetOffendersResponse.OffendersList[i].EquipmentInfo.ReceiverSN;
                    UpdateOffenderRequest.RelatedOffenderID = GetOffendersResponse.OffendersList[i].RelatedOffenderID;
                    UpdateOffenderRequest.SocialSecurity = GetOffendersResponse.OffendersList[i].SocialSecurity;
                    UpdateOffenderRequest.StateID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].StateID;
                    UpdateOffenderRequest.Street = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].Street;
                    UpdateOffenderRequest.Timestamp = GetOffendersResponse.OffendersList[i].Timestamp1;
                    UpdateOffenderRequest.Title = GetOffendersResponse.OffendersList[i].Title;
                    UpdateOffenderRequest.Transmitter = GetOffendersResponse.OffendersList[i].EquipmentInfo.TransmitterSN;
                    UpdateOffenderRequest.ZipCode = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].ZipCode;


                    OffendersProxy_1.Instance.UpdateOffender(UpdateOffenderRequest);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e + "GetOffendersResponse.Offenders List is empty!");
            }
        }
    }
}
