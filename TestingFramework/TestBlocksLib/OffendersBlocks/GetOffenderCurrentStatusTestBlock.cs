﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderCurrentStatusTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderCurrentStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffenderCurrentStatusRequest GetOffenderCurrentStatusRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderCurrentStatusResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffenderCurrentStatusResponse GetOffenderCurrentStatusResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderCurrentStatusResponse = OffendersProxy.Instance.GetOffenderCurrentStatus(GetOffenderCurrentStatusRequest);
        }
    }
}
