﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class DeleteOffenderContactRequestTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgDeleteContactRequest DeleteOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.DeleteOffenderContact(DeleteOffenderContactRequest);
        }
    }


    public class DeleteOffenderContactRequestTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgDeleteOffenderContactRequest DeleteOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.DeleteOffenderContact(DeleteOffenderContactRequest);
        }
    }


    public class DeleteOffenderContactRequestTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgDeleteOffenderContactRequest DeleteOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.DeleteOffenderContact(DeleteOffenderContactRequest);
        }
    }

}
