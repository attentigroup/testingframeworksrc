﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class DeleteOffenderAddressTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgDeleteOffenderAddressRequest DeleteOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.DeleteOffenderAddress(DeleteOffenderAddressRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class DeleteOffenderAddressTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgDeleteOffenderAddressRequest DeleteOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.DeleteOffenderAddress(DeleteOffenderAddressRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class DeleteOffenderAddressTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgDeleteOffenderAddressRequest DeleteOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.DeleteOffenderAddress(DeleteOffenderAddressRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
