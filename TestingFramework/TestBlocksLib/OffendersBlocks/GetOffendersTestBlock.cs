﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Linq;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffendersTestBlock : OffenderProxyTestBlockBase
    {
        [PropertyTest(EnumPropertyName.GetAgenciesListForMCResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] GetAgenciesListForMCResponse{ get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ulong LastTimestamp { get; set; }

        [PropertyTest(EnumPropertyName.amountOfOffenders, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int amountOfOffenders { get; set; }

        [PropertyTest(EnumPropertyName.RefIdStringMinLength, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? RefIdStringMinLength { get; set; }

        public GetOffendersTestBlock()
        {
            GetAgenciesListForMCResponse = null;
        }

        protected override void ExecuteBlock()
        {
            amountOfOffenders = 0;
            if (GetAgenciesListForMCResponse != null && GetAgenciesListForMCResponse.Length > 0)
            {
                for (int i = 0; i < GetAgenciesListForMCResponse.Length; i++)
                {
                    Offenders0.EntNumericParameter AgencyIDQueryParameter = new Offenders0.EntNumericParameter();
                    AgencyIDQueryParameter.Value = (long)GetAgenciesListForMCResponse[i];
                    AgencyIDQueryParameter.Operator = Offenders0.EnmNumericOperator.Equal;
                    GetOffendersListRequest.AgencyID = AgencyIDQueryParameter;

                    GetOffendersResponse = OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);
                    amountOfOffenders += GetOffendersResponse.OffendersList.Length;
                }
            }
            else
            {
                if (OffendersProxyObject != null)
                {
                    GetOffendersResponse = OffendersProxyObject.GetOffenders(GetOffendersListRequest);
                }
                else
                {
                    GetOffendersResponse = OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);
                }               

                if(RefIdStringMinLength != null)
                {
                    GetOffendersResponse.OffendersList = GetOffendersResponse.OffendersList.Where(x => x.RefID.Length >= RefIdStringMinLength).ToArray();
                }
                
                if (GetOffendersResponse.OffendersList?.Length > 0)
                {
                    LastTimestamp = GetOffendersResponse.OffendersList[0].Timestamp;
                    amountOfOffenders = GetOffendersResponse.TotalCount;
                }
                else
                {
                    throw new Exception("GetOffendersResponse.Offenders List is empty!");
                }
            }
        }
    }


    public class GetOffendersTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public long LastTimestamp { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffendersResponse = OffendersProxy_1.Instance.GetOffenders(GetOffendersListRequest);
            LastTimestamp = (long) GetOffendersResponse.OffendersList[0].TimeStamp;
        }
    }


    public class GetOffendersTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public long LastTimestamp { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffendersResponse = OffendersProxy_2.Instance.GetOffenders(GetOffendersListRequest);
            LastTimestamp = (long)GetOffendersResponse.OffendersList[0].TimeStamp;
        }
    }
}
