﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Linq;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.Queue;
using TestingFramework.TestsInfraStructure.Implementation;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetActiveOffenderWithoutRequestInQueueTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.filterID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int FilterID { get; set; }

        [PropertyTest(EnumPropertyName.resultCode, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EnmResultCode ResultCode { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var offender in GetOffendersResponse.OffendersList)
            {
                var GetQueueResponse = QueueProxy.Instance.GetQueue(new Queue0.EntMessageGetQueueRequest()
                {
                    StartRowIndex = 0,
                    MaximumRows = 100,
                    filterID = FilterID,
                    resultCode = ResultCode
                });


                var request = GetQueueResponse.RequestList.FirstOrDefault(x => (x as Queue0.EntQueueProgramTracker).OffenderId == offender.ID);
                if(request == null)
                {
                    Offender = offender;
                    break;
                }

                //bool isRequestExist = false;
                //foreach(var request in GetQueueResponse.RequestList)
                //{
                //    var EntQueueProgramTracker = (Queue0.EntQueueProgramTracker)request;
                //    if (EntQueueProgramTracker.OffenderId == offender.ID)
                //    {
                //        isRequestExist = true;
                //        break;
                //    }                        
                //}
                //if (!isRequestExist)
                //{
                //    Offender = offender;
                //    break;
                //}

            }
            if (Offender == null)
                throw new Exception("No active offender without request in queue was found!");
        }
    }
}
