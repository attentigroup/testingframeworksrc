﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class UpdateOffenderTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgUpdateOffenderRequest UpdateOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            try
            {
                OffendersProxy.Instance.UpdateOffender(UpdateOffenderRequest);
            }
            catch (Exception ex)
            {
                throw new Exception($"while Update offender {UpdateOffenderRequest.OffenderID} Error: {ex.Message}", ex);
            }
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateOffenderTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgUpdateOffenderRequest UpdateOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.UpdateOffender(UpdateOffenderRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateOffenderTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgUpdateOffenderRequest UpdateOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.UpdateOffender(UpdateOffenderRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
