﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using TestingFramework.Proxies.API.Events;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Events12_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffendersIdsByEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.OffendersIdsList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<int> OffendersIdsList { get; set; }

        [PropertyTest(EnumPropertyName.OfficersIdsList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<int> OfficerIdsList { get; set; }


        [PropertyTest(EnumPropertyName.AgenciesIdList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<int> AgenciesIdList { get; set; }
        
        private Events12_0.EntMsgGetEventsRequest getEventsRequest;

        public GetOffendersIdsByEventsTestBlock()
        {
            getEventsRequest = new Events12_0.EntMsgGetEventsRequest();
            getEventsRequest.IncludeHistory = false;
            getEventsRequest.SortField = Events12_0.EnmEventsSortOptions.EventTime;
            getEventsRequest.SortDirection = System.ComponentModel.ListSortDirection.Descending;
        }
        protected override void ExecuteBlock()
        {
            int totalEventRetrived = 0;
            var dicOffenders = new Dictionary<int, int>();
            var dicOfficers = new Dictionary<int, int>();
            var dicAgenciesIdNameDictionary = new Dictionary<int, string>();
            var retrivaleEndTime = DateTime.Now.AddDays(-7);
            
            //set limit
            getEventsRequest.Limit = Limit;
            //set initialize time
            getEventsRequest.EndTime = DateTime.Now;           

            while (dicOffenders.Count < Limit && getEventsRequest.EndTime > retrivaleEndTime)
            {
                //set time range
                getEventsRequest.StartTime = getEventsRequest.EndTime.Value.AddHours(-1);
                Events12_0.EntMsgGetEventsResponse response = EventsProxy.Instance.GetEvents(getEventsRequest);
                Log.DebugFormat("request events from {0} to {1} return {2} events.", 
                    getEventsRequest.StartTime.Value, getEventsRequest.EndTime.Value, response.EventsList.Length);
                totalEventRetrived += response.EventsList.Length;

                foreach (var eventItem in response.EventsList)
                {
                    if(eventItem.OffenderID.HasValue)
                    {
                        int offenderId = eventItem.OffenderID.Value;
                        if( offenderId <= 0)
                        {
                            Log.DebugFormat("Event id {0} arrived with offender id {1} - skipping it", eventItem.ID, offenderId);
                            continue;
                        }
                        if (dicOffenders.ContainsKey(offenderId) == false)
                        {//new offender - add it to the dictionary
                            dicOffenders.Add(offenderId, offenderId);
                            if (dicOffenders.Count >= Limit)
                                break;//no need more offenders
                        }
                        else
                        {
                            Log.DebugFormat("Event id {0} arrived with offender id {1} that allready added to the dicionary - skipping it", eventItem.ID, offenderId);
                        }
                    }                    
                    else
                    {
                        Log.DebugFormat("Event id {0} arrived without offender id - skipping it", eventItem.ID);
                    }
                }
                getEventsRequest.EndTime = getEventsRequest.StartTime;
            }

            Log.DebugFormat("After retrieve {0} events arrived to {1} offenders.", totalEventRetrived, dicOffenders.Count);
            OffendersIdsList = dicOffenders.Keys.ToList();
            //log offendersref id
            Offenders12_0.EntMsgGetOffendersListRequest entMsgGetOffendersRequest = new Offenders12_0.EntMsgGetOffendersListRequest();
            foreach (var offenderId in OffendersIdsList)
            {
                entMsgGetOffendersRequest.OffenderID = new Offenders12_0.EntNumericParameter() { Value = offenderId, Operator = Offenders12_0.EnmNumericOperator.Equal } ;

                var EntMsgGetOffendersResponse = OffendersProxy.Instance.GetOffenders(entMsgGetOffendersRequest);
                var offender = EntMsgGetOffendersResponse.OffendersList[0];
                Log.InfoFormat("offender id = {0} ref id = {1}", offenderId, offender.RefID);
                if(dicOfficers.ContainsKey(offender.OfficerID) == false )
                {
                    dicOfficers.Add(offender.OfficerID, offender.OfficerID);
                }
                if(dicAgenciesIdNameDictionary.ContainsKey(offender.AgencyID) == false)
                {
                    dicAgenciesIdNameDictionary.Add(offender.AgencyID, offender.AgencyName);
                }
            }
            OfficerIdsList = dicOfficers.Keys.ToList();
            AgenciesIdList = dicAgenciesIdNameDictionary.Keys.ToList();
            Log.DebugFormat("After retrieve {0} offenders we have {1} officers.", OffendersIdsList.Count, OfficerIdsList.Count);
        }
    }
}
