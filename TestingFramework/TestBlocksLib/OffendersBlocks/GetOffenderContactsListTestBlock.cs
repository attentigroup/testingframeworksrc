﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderContactsListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderContactsListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetContactsListRequest GetOffenderContactsListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsListResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderContactsListResponse = OffendersProxy.Instance.GetOffenderContactsList(GetOffenderContactsListRequest);
        }
    }


    public class GetOffenderContactsListTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderContactsListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffenderContactsListRequest GetOffenderContactsListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffenderContactsListResponse GetOffenderContactsListResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderContactsListResponse = OffendersProxy_1.Instance.GetOffenderContactsList(GetOffenderContactsListRequest);
        }
    }


    public class GetOffenderContactsListTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderContactsListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffenderContactsListRequest GetOffenderContactsListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffenderContactsListResponse GetOffenderContactsListResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderContactsListResponse = OffendersProxy_2.Instance.GetOffenderContactsList(GetOffenderContactsListRequest);
        }
    }
}
