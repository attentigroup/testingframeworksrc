﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffendersCountersTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffendersCountersResponse GetOffendersCountersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffendersCountersResponse = OffendersProxy.Instance.GetOffendersCounters();
        }
    }

    public class GetOffendersCountersTestBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffendersCountersResponse GetOffendersCountersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffendersCountersResponse = OffendersProxy_1.Instance.GetOffendersCounters();
        }
    }

    public class GetOffendersCountersTestBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffendersCountersResponse GetOffendersCountersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffendersCountersResponse = OffendersProxy_2.Instance.GetOffendersCounters();
        }
    }
}