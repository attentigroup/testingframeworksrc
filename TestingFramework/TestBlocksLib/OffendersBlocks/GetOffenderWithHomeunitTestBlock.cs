﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithHomeunitTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var offenderCurefew = GetOffendersResponse.OffendersList.FirstOrDefault(x => x.EquipmentInfo.ReceiverModel != null);
            offenderCurefew.RefID = offenderCurefew.EquipmentInfo.ReceiverSN;
            GetOffendersResponse.OffendersList[0] = offenderCurefew;
        }
    }
}
