﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using TestingFramework.Proxies.API.Offenders;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderFromEvent : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int OffenderID { get; set; }

        protected override void ExecuteBlock()
        {

            foreach(var oEvent in GetEventsResponse.EventsList)
            {
                if (oEvent.OffenderID > 0)
                {
                    GetOffendersResponse = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new Offenders0.EntNumericParameter() { Operator = Offenders0.EnmNumericOperator.Equal, Value = (long)oEvent.OffenderID.Value }                        
                    });
                    if (GetOffendersResponse.OffendersList[0].IsActive == true)
                    {
                        OffenderID = GetOffendersResponse.OffendersList[0].ID;
                        break;
                    }
                }

            }
            
        }
    }
}
