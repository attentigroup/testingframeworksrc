﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetActiveDVPairOffendersTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Aggressor, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Aggressor { get; set; }

        [PropertyTest(EnumPropertyName.Victim, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Victim { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var offender in GetOffendersResponse.OffendersList)
            {
                if(offender.RelatedOffenderID != null)
                {
                    var getVictim = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new Offenders0.EntNumericParameter() { Operator = Offenders0.EnmNumericOperator.Equal, Value = offender.RelatedOffenderID.Value }
                    });
                    if (getVictim.OffendersList[0].IsActive)
                    {
                        Aggressor = offender;
                        Victim = getVictim.OffendersList[0];
                        break;
                    }
                }

            }
            if (Victim == null)
                throw new Exception("No active DV pair");
        }
    }
}
