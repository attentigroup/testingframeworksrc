﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class DeleteOffenderPictureTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderPicture, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgDeletePictureRequest DeleteOffenderPicture { get; set; }


        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.DeleteOffenderPicture(DeleteOffenderPicture);
        }

        public override void CleanUp()
        {  }
    }


    public class DeleteOffenderPictureTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderPicture, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgDeleteOffenderPicture DeleteOffenderPicture { get; set; }


        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.DeleteOffenderPicture(DeleteOffenderPicture);
        }

        public override void CleanUp()
        { }
    }


    public class DeleteOffenderPictureTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderPicture, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgDeleteOffenderPicture DeleteOffenderPicture { get; set; }


        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.DeleteOffenderPicture(DeleteOffenderPicture);
        }

        public override void CleanUp()
        { }
    }
}
