﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class DeleteOffenderTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgDeleteOffenderRequest DeleteOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.DeleteOffender(DeleteOffenderRequest);
        }
    }


    public class DeleteOffenderTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgDeleteOffenderRequest DeleteOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.DeleteOffender(DeleteOffenderRequest);
        }
    }


    public class DeleteOffenderTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgDeleteOffenderRequest DeleteOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.DeleteOffender(DeleteOffenderRequest);
        }
    }
}
