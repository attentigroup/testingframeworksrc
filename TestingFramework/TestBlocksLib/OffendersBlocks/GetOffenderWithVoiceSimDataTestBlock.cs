﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithVoiceSimDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }

        public GetOffenderWithVoiceSimDataTestBlock()
        {
            Offender = new Offenders0.EntOffender();
        }
        protected override void ExecuteBlock()
        {
            var getOffenders = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
            {
                ProgramType = new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF },
                ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }                

            });

            Offender = getOffenders.OffendersList.FirstOrDefault(x => !string.IsNullOrEmpty(x.ContactInfo.CellData.VoicePhoneNumber));

            if (Offender == null)
                throw new Exception("No offenders with VoicePhoneNumber found!");
        }
    }
}
