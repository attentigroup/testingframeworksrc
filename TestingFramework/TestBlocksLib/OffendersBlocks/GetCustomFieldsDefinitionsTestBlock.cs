﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Offenders;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetCustomFieldsDefinitionsTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.CustomFieldDefinition, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntCustomFieldDefinition[] CustomFieldDefinition { get; set; }


        protected override void ExecuteBlock()
        {
            CustomFieldDefinition = OffendersProxy.Instance.GetCustomFieldsDefinitions();
        }
    }
}
