﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestBlocksLib.DevicesBlocks;
using System.Threading;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public abstract class OffenderProxyTestBlockBase : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OffendersProxy, EnumPropertyType.None, EnumPropertyModifier.None)]
        public OffendersProxy OffendersProxyObject { get; set; }
    }


    public class AddOffenderTestBlock : OffenderProxyTestBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgAddZoneResponse AddZoneResponse { get; set; }

        protected override void ExecuteBlock()
        {

            if(OffendersProxyObject != null)
            {
                AddOffenderResponse = OffendersProxyObject.AddOffender(AddOffenderRequest);
            }
            else
            {
                AddOffenderResponse = OffendersProxy.Instance.AddOffender(AddOffenderRequest);
            }
        }

        public override void CleanUp()
        {
            // build get offender request
            var offenderId = AddOffenderResponse.NewOffenderID;
            var getOffenderListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            getOffenderListRequest.OffenderID = new Offenders0.EntNumericParameter() { Operator = Offenders0.EnmNumericOperator.Equal, Value = offenderId };

            // take the response and get the timestamp for the delete request
            var entGetOffendersResponse = OffendersProxy.Instance.GetOffenders(getOffenderListRequest);
            var timestamp = entGetOffendersResponse.OffendersList[0].Timestamp;
            if (entGetOffendersResponse.OffendersList[0].IsActive)
            {
                ProgramActionsProxy.Instance.SendEndOfServiceRequest(new ProgramActions0.EntMsgSendEndOfServiceRequest()
                {
                    OffenderID = offenderId,
                    EndOfServiceType = ProgramActions0.EnmEOSType.Default,
                    EndOfServiceCode = "MEOS",
                    Comment = "Send EOS Request Test",
                });

                RunXTSimulatorTestBlock xtSimulator = new RunXTSimulatorTestBlock();
                xtSimulator.EquipmentSN = entGetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN;
                //xtSimulator.NUMPOINTS = "1";
                xtSimulator.Execute();

                Thread.Sleep(10000);

                entGetOffendersResponse = OffendersProxy.Instance.GetOffenders(getOffenderListRequest);
                timestamp = entGetOffendersResponse.OffendersList[0].Timestamp;
            }

            // build the delete offender request
            var deleteOffenderRequest = new Offenders0.EntMsgDeleteOffenderRequest();
            deleteOffenderRequest.Timestamp = timestamp;
            deleteOffenderRequest.OffenderID = offenderId;

            Thread.Sleep(1000);
            // send the delete offender request
            OffendersProxy.Instance.DeleteOffender(deleteOffenderRequest);
        }
    }

        public class AddOffenderTestBlock_1 : TestingBlockBase
        {
            [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
            public Offenders1.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

            [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
            public Offenders1.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

            protected override void ExecuteBlock()
            {
                AddOffenderResponse = OffendersProxy_1.Instance.AddOffender(AddOffenderRequest);
            }

            public override void CleanUp()
            {
                // build get offender request
                var offenderId = AddOffenderResponse.NewOffenderID;
                var getOffenderListRequest = new Offenders1.EntMsgGetOffendersListRequest();
                getOffenderListRequest.OffenderID = new Offenders1.EntNumericQueryParameter() { Operator = Offenders1.EnmSqlOperatorNum.Equal, Value = offenderId };

                // take the response and get the timestamp for the delete request
                var entGetOffendersResponse = OffendersProxy_1.Instance.GetOffenders(getOffenderListRequest);
                var timestamp = entGetOffendersResponse.OffendersList[0].TimeStamp;

                if (entGetOffendersResponse.OffendersList[0].IsActive)
                {
                    ProgramActionsProxy.Instance.SendEndOfServiceRequest(new ProgramActions0.EntMsgSendEndOfServiceRequest()
                    {
                        OffenderID = offenderId,
                        EndOfServiceType = ProgramActions0.EnmEOSType.Default,
                        EndOfServiceCode = "MEOS",
                        Comment = "Send EOS Request Test",
                    });

                    RunXTSimulatorTestBlock xtSimulator = new RunXTSimulatorTestBlock();
                    xtSimulator.EquipmentSN = entGetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN;
                    xtSimulator.Execute();

                }

                // build the delete offender request
                var deleteOffenderRequest = new Offenders1.EntMsgDeleteOffenderRequest();
                if (timestamp != null)
                    deleteOffenderRequest.Timestamp = (ulong)timestamp.GetValueOrDefault();
                deleteOffenderRequest.OffenderID = offenderId;

                // send the delete offender request
                OffendersProxy_1.Instance.DeleteOffender(deleteOffenderRequest);
            }
        }


        public class AddOffenderTestBlock_2 : TestingBlockBase
        {
            [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
            public Offenders2.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

            [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
            public Offenders2.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

            protected override void ExecuteBlock()
            {
                AddOffenderResponse = OffendersProxy_2.Instance.AddOffender(AddOffenderRequest);
            }

            public override void CleanUp()
            {
                // build get offender request
                var offenderId = AddOffenderResponse.NewOffenderID;
                var getOffenderListRequest = new Offenders2.EntMsgGetOffendersListRequest();
                getOffenderListRequest.OffenderID = new Offenders2.EntNumericQueryParameter() { Operator = Offenders2.EnmSqlOperatorNum.Equal, Value = offenderId };

                // take the response and get the timestamp for the delete request
                var entGetOffendersResponse = OffendersProxy_2.Instance.GetOffenders(getOffenderListRequest);
                var timestamp = entGetOffendersResponse.OffendersList[0].TimeStamp;

                if (entGetOffendersResponse.OffendersList[0].IsActive)
                {
                    ProgramActionsProxy.Instance.SendEndOfServiceRequest(new ProgramActions0.EntMsgSendEndOfServiceRequest()
                    {
                        OffenderID = offenderId,
                        EndOfServiceType = ProgramActions0.EnmEOSType.Default,
                        EndOfServiceCode = "MEOS",
                        Comment = "Send EOS Request Test",
                    });

                    RunXTSimulatorTestBlock xtSimulator = new RunXTSimulatorTestBlock();
                    xtSimulator.EquipmentSN = entGetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN;
                    xtSimulator.Execute();

                }

                // build the delete offender request
                var deleteOffenderRequest = new Offenders2.EntMsgDeleteOffenderRequest();
                if (timestamp != null)
                    deleteOffenderRequest.Timestamp = (long)timestamp.GetValueOrDefault();
                deleteOffenderRequest.OffenderID = offenderId;

                // send the delete offender request
                OffendersProxy_2.Instance.DeleteOffender(deleteOffenderRequest);
            }
        }
    }
