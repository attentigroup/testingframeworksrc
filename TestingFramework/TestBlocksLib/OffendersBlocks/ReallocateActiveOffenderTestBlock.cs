﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.OffendersBlocks
{
    public class ReallocateActiveOffenderTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ReallocateActiveOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders12_0.EntMsgReallocateActiveOffenderRequest ReallocateActiveOffenderRequest { get; set; }


        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.ReallocateActiveOffender(ReallocateActiveOffenderRequest);
        }

        public override void CleanUp()
        {      }
    }
}
