﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class AddOffenderContactTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderContactResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddContactResponse AddOffenderContactResponse { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderContactResponse = OffendersProxy.Instance.AddOffenderContact(AddOffenderContactRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderContact = new Offenders0.EntMsgDeleteContactRequest();
            deleteOffenderContact.ContactID = AddOffenderContactResponse.NewContactID;
            OffendersProxy.Instance.DeleteOffenderContact(deleteOffenderContact);
        }
    }


    public class AddOffenderContactTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.AddOffenderContact(AddOffenderContactRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderContact = new Offenders1.EntMsgDeleteOffenderContactRequest();
            var getOffenderContactList = new Offenders1.EntMsgGetOffenderContactsListRequest();

            getOffenderContactList.OffenderID = AddOffenderResponse.NewOffenderID;
            var getOffenderListResponse = OffendersProxy_1.Instance.GetOffenderContactsList(getOffenderContactList);

            deleteOffenderContact.ContactID = getOffenderListResponse.ContactsList[0].ContactID;
            OffendersProxy_1.Instance.DeleteOffenderContact(deleteOffenderContact);
        }
    }


    public class AddOffenderContactTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }


        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.AddOffenderContact(AddOffenderContactRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderContact = new Offenders2.EntMsgDeleteOffenderContactRequest();
            var getOffenderContactList = new Offenders2.EntMsgGetOffenderContactsListRequest();

            getOffenderContactList.OffenderID = AddOffenderResponse.NewOffenderID;
            var getOffenderListResponse = OffendersProxy_2.Instance.GetOffenderContactsList(getOffenderContactList);

            deleteOffenderContact.ContactID = getOffenderListResponse.ContactsList[0].ContactID;
            OffendersProxy_2.Instance.DeleteOffenderContact(deleteOffenderContact);
        }
    }
}
