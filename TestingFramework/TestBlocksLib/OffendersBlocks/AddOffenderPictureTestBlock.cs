﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class AddOffenderPictureTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddPictureRequest AddOffenderPictureRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderPictureResponse = OffendersProxy.Instance.AddOffenderPicture(AddOffenderPictureRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderPicture = new Offenders0.EntMsgDeletePictureRequest();
            deleteOffenderPicture.PhotoID = AddOffenderPictureResponse.NewPhotoID;
            if (AddOffenderResponse != null)
                deleteOffenderPicture.OffenderID = AddOffenderResponse.NewOffenderID;
            else if (GetOffendersResponse != null)
                deleteOffenderPicture.OffenderID = GetOffendersResponse.OffendersList[0].ID;
            OffendersProxy.Instance.DeleteOffenderPicture(deleteOffenderPicture);
        }
    }


    public class AddOffenderPictureTestBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderPictureRequest AddOffenderPictureRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgAddOffenderPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderPictureResponse = OffendersProxy_1.Instance.AddOffenderPicture(AddOffenderPictureRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderPicture = new Offenders1.EntMsgDeleteOffenderPicture();
            deleteOffenderPicture.PictureID = AddOffenderPictureResponse.NewPhotoID;
            deleteOffenderPicture.OffenderID = AddOffenderResponse.NewOffenderID;
            OffendersProxy_1.Instance.DeleteOffenderPicture(deleteOffenderPicture);
        }
    }


    public class AddOffenderPictureTestBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.AddOffenderResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderResponse AddOffenderResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderPictureRequest AddOffenderPictureRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgAddOffenderPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderPictureResponse = OffendersProxy_2.Instance.AddOffenderPicture(AddOffenderPictureRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderPicture = new Offenders2.EntMsgDeleteOffenderPicture();
            deleteOffenderPicture.PictureID = AddOffenderPictureResponse.NewPhotoID;
            deleteOffenderPicture.OffenderID = AddOffenderResponse.NewOffenderID;
            OffendersProxy_2.Instance.DeleteOffenderPicture(deleteOffenderPicture);
        }
    }
}
