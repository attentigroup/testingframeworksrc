﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Offender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithTrailByProgramTypeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramConcept ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }

        public GetOffenderWithTrailByProgramTypeTestBlock()
        {
            ProgramConcept = Offenders0.EnmProgramConcept.Regular;
        }
        protected override void ExecuteBlock()
        {
            foreach(var offenderID in OffenderWithTrailIDGenerator.OffenderWithTrailIdList)
            {
                var getOffender = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
                {
                    OffenderID = new Offenders0.EntNumericParameter() { Operator = Offenders0.EnmNumericOperator.Equal, Value = offenderID }
                });
                if(getOffender.OffendersList[0].ProgramType == ProgramType && getOffender.OffendersList[0].ProgramConcept == ProgramConcept)
                {
                    Offender = getOffender.OffendersList[0];
                    break;
                }
            }
        }
    }
}
