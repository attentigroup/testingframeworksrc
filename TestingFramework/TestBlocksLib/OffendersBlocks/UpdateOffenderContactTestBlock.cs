﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class UpdateOffenderContactTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgUpdateContactRequest UpdateOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.UpdateOffenderContact(UpdateOffenderContactRequest);
        }
    }


    public class UpdateOffenderContactTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgUpdateOffenderContactRequest UpdateOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.UpdateOffenderContact(UpdateOffenderContactRequest);
        }
    }


    public class UpdateOffenderContactTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgUpdateOffenderContactRequest UpdateOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.UpdateOffenderContact(UpdateOffenderContactRequest);
        }
    }
}
