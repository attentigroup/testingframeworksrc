﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithReceiverTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntOffender Offender { get; set; }
        protected override void ExecuteBlock()
        {
            var _offender = GetOffendersResponse.OffendersList.FirstOrDefault(x => !string.IsNullOrEmpty(x.EquipmentInfo.ReceiverSN));
            Offender = _offender;
        }
    }
}
