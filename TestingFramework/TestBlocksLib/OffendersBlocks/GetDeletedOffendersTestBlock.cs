﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class GetDeletedOffendersTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetDeletedOffendersRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetDeletedOffendersRequest GetDeletedOffendersRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetDeletedOffendersResponse GetDeletedOffendersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetDeletedOffendersResponse = OffendersProxy.Instance.GetDeletedOffenders(GetDeletedOffendersRequest);
        }
    }


    public class GetDeletedOffendersTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetDeletedOffendersRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetDeletedOffendersRequest GetDeletedOffendersRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetDeletedOffendersResponse GetDeletedOffendersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetDeletedOffendersResponse = OffendersProxy_1.Instance.GetDeletedOffenders(GetDeletedOffendersRequest);
        }
    }


    public class GetDeletedOffendersTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetDeletedOffendersRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetDeletedOffendersRequest GetDeletedOffendersRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetDeletedOffendersResponse GetDeletedOffendersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetDeletedOffendersResponse = OffendersProxy_2.Instance.GetDeletedOffenders(GetDeletedOffendersRequest);
        }
    }
}