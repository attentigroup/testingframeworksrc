﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.Trails;
using TestingFramework.TestsInfraStructure.Implementation;

using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithTrailByEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.OffendersIdWithTrail, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int OffenderIdWithTrail { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffendersResponse GetOffenderWithTrail { get; set; }

        private Events0.EntMsgGetEventsRequest getEventsRequest;

        //TODO: Need to move this to more central location.
        public static int EVENTS_LIMIT_FORTRAILS_TESTS = 20;//because i can... 

        public GetOffenderWithTrailByEventsTestBlock()
        {
            Limit = EVENTS_LIMIT_FORTRAILS_TESTS;
            getEventsRequest = new Events0.EntMsgGetEventsRequest();
            getEventsRequest.SortField = Events0.EnmEventsSortOptions.EventTime;
            getEventsRequest.SortDirection = System.ComponentModel.ListSortDirection.Descending;
            getEventsRequest.Limit = Limit;
            getEventsRequest.Severity = Events0.EnmEventSeverity.Action;
        }
        protected override void ExecuteBlock()
        {            
            var retrivalEndTime = DateTime.Now.AddDays(-7);
            getEventsRequest.Limit = Limit;
            getEventsRequest.EndTime = DateTime.Now;

            while (OffenderIdWithTrail == 0 && getEventsRequest.EndTime > retrivalEndTime)
            {

                getEventsRequest.StartTime = getEventsRequest.EndTime.Value.AddDays(-1);
                Events0.EntMsgGetEventsResponse response = EventsProxy.Instance.GetEvents(getEventsRequest);

                foreach (var eventItem in response.EventsList)
                {
                    
                    int offenderId = eventItem.OffenderID.Value;
                    
                    var getTrail = TrailsProxy.Instance.GetTrail(new TestingFramework.Proxies.EM.Interfaces.Trails12_0.EntMsgGetTrailRequest()
                    {
                        OffenderID = offenderId,
                        StartTime = DateTime.Now.AddMonths(-3),
                        EndTime = DateTime.Now
                    });
                    if (getTrail.Points.Length > 0)
                    {
                        OffenderIdWithTrail = offenderId;
                        GetOffenderWithTrail = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
                        {
                            OffenderID = new Offenders0.EntNumericParameter() { Value = OffenderIdWithTrail, Operator = Offenders0.EnmNumericOperator.Equal },
                        });
                        break;
                    }
                    
                }
                getEventsRequest.EndTime = getEventsRequest.StartTime;
            }

        }
    }

    public class GetOffenderWithTrailByEventsTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.OffendersIdWithTrail, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int OffenderIdWithTrail { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffendersResponse GetOffenderWithTrail { get; set; }

        private Events1.EntMsgGetEventsRequest getEventsRequest;

        public GetOffenderWithTrailByEventsTestBlock_1()
        {
            getEventsRequest = new Events1.EntMsgGetEventsRequest();
            getEventsRequest.SortField = Events1.EnmEventsSortOptions.EventTime;
            getEventsRequest.SortDirection = Events1.ListSortDirection.Descending;
            getEventsRequest.Limit = Limit;
            getEventsRequest.Severity = Events1.EnmEventSeverity.Action;
        }
        protected override void ExecuteBlock()
        {
            var retrivalEndTime = DateTime.Now.AddDays(-7);
            getEventsRequest.Limit = Limit;
            getEventsRequest.EndTime = DateTime.Now;

            while (OffenderIdWithTrail == 0 && getEventsRequest.EndTime > retrivalEndTime)
            {

                getEventsRequest.StartTime = getEventsRequest.EndTime.Value.AddDays(-1);
                Events1.EntMsgGetEventsResponse response = EventsProxy_1.Instance.GetEvents(getEventsRequest);

                foreach (var eventItem in response.EventsList)
                {

                    int offenderId = eventItem.OffenderID.Value;

                    var getTrail = TrailsProxy.Instance.GetTrail(new TestingFramework.Proxies.EM.Interfaces.Trails12_0.EntMsgGetTrailRequest()
                    {
                        OffenderID = offenderId,
                        StartTime = DateTime.Now.AddMonths(-3),
                        EndTime = DateTime.Now
                    });
                    if (getTrail.Points.Length > 0)
                    {
                        OffenderIdWithTrail = offenderId;
                        GetOffenderWithTrail = OffendersProxy_1.Instance.GetOffenders(new Offenders1.EntMsgGetOffendersListRequest()
                        {
                            OffenderID = new Offenders1.EntNumericQueryParameter() { Value = OffenderIdWithTrail, Operator = Offenders1.EnmSqlOperatorNum.Equal },
                        });
                        break;
                    }

                }
                getEventsRequest.EndTime = getEventsRequest.StartTime;
            }

        }
    }

    public class GetOffenderWithTrailByEventsTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.OffendersIdWithTrail, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int OffenderIdWithTrail { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffendersResponse GetOffenderWithTrail { get; set; }

        private Events2.EntMsgGetEventsRequest getEventsRequest;

        public GetOffenderWithTrailByEventsTestBlock_2()
        {
            getEventsRequest = new Events2.EntMsgGetEventsRequest();
            getEventsRequest.SortField = Events2.EnmEventsSortOptions.EventTime;
            getEventsRequest.SortDirection = Events2.ListSortDirection.Descending;
            getEventsRequest.MaximumRows = Limit;
            getEventsRequest.Severity = Events2.EnmEventSeverity.Action;
        }
        protected override void ExecuteBlock()
        {
            var retrivalEndTime = DateTime.Now.AddDays(-7);
            getEventsRequest.MaximumRows = Limit;
            getEventsRequest.ToTime = DateTime.Now;

            while (OffenderIdWithTrail == 0 && getEventsRequest.ToTime > retrivalEndTime)
            {

                getEventsRequest.FromTime = getEventsRequest.ToTime.Value.AddDays(-1);
                Events2.EntMsgGetEventsResponse response = EventsProxy_2.Instance.GetEvents(getEventsRequest);

                foreach (var eventItem in response.EventsList)
                {
                    Events2.EntOffenderEvent offenderEvent = new Events2.EntOffenderEvent();
                    offenderEvent = (Events2.EntOffenderEvent)eventItem;

                    int offenderId = offenderEvent.OffenderID;

                    var getTrail = TrailsProxy.Instance.GetTrail(new TestingFramework.Proxies.EM.Interfaces.Trails12_0.EntMsgGetTrailRequest()
                    {
                        OffenderID = offenderId,
                        StartTime = DateTime.Now.AddMonths(-3),
                        EndTime = DateTime.Now
                    });
                    if (getTrail.Points.Length > 0)
                    {
                        OffenderIdWithTrail = offenderId;
                        GetOffenderWithTrail = OffendersProxy_2.Instance.GetOffenders(new Offenders2.EntMsgGetOffendersListRequest()
                        {
                            OffenderID = new Offenders2.EntNumericQueryParameter() { Value = OffenderIdWithTrail, Operator = Offenders2.EnmSqlOperatorNum.Equal },
                        });
                        break;
                    }

                }
                getEventsRequest.ToTime = getEventsRequest.FromTime;
            }

        }
    }


}







