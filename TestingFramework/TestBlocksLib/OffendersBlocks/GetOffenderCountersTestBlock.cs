﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderCountersTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetOffendersCountersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffendersCountersResponse GetOffendersCountersResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffendersCountersResponse = OffendersProxy.Instance.GetOffendersCounters();
        }
    }
}
