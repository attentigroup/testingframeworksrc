﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithPhoneNumberTestBlock : TestingBlockBase//TODO:Need to be LogicBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }

        protected override void ExecuteBlock()
        {
            Offender = GetOffendersResponse.OffendersList.FirstOrDefault(x => x.ContactInfo.CellData.VoicePhoneNumber != null);
            if (Offender == null)
                throw new Exception("There is no offender with phone number!");
        }
    }
}
