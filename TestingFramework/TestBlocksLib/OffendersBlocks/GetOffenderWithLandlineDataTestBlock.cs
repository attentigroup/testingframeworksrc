﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderWithLandlineDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        public GetOffenderWithLandlineDataTestBlock()
        {
            Offender = new Offenders0.EntOffender();
        }
        protected override void ExecuteBlock()
        {

            foreach (var offender in GetOffendersResponse.OffendersList)
            {

                if (offender.ContactInfo.LandLine.Phone != null)
                {
                    Offender = offender;
                    break;
                }

            }
        }
    }
}
