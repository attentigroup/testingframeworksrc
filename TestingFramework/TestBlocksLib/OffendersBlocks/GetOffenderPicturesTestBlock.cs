﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Offenders;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderPicturesTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetPicturesRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetPicturesRequest GetPicturesRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetPicturesResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetPicturesResponse GetPicturesResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetPicturesResponse = OffendersProxy.Instance.GetOffenderPictures(GetPicturesRequest);
        }
    }
}
