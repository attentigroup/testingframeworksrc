﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOffenderPictureIDListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetPictureIDListRequest GetOffenderPictureIDListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetPictureIDListResponse GetOffenderPictureIDListResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderPictureIDListResponse = OffendersProxy.Instance.GetOffenderPictureIDList(GetOffenderPictureIDListRequest);
        }
    }


    public class GetOffenderPictureIDListTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffenderPictureIDListRequest GetOffenderPictureIDListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffenderPictureIDListResponse GetOffenderPictureIDListResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderPictureIDListResponse = OffendersProxy_1.Instance.GetOffenderPictureIDList(GetOffenderPictureIDListRequest);
        }
    }


    public class GetOffenderPictureIDListTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffenderPictureIDListRequest GetOffenderPictureIDListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffenderPictureIDListResponse GetOffenderPictureIDListResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffenderPictureIDListResponse = OffendersProxy_2.Instance.GetOffenderPictureIDList(GetOffenderPictureIDListRequest);
        }
    }
}
