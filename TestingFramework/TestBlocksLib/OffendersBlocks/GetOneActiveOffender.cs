﻿using System;
using System.Collections.Generic;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class GetOneActiveOffender : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetQueueResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueResponse GetQueueResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }


        public GetOneActiveOffender()
        {
            Offender = new Offenders0.EntOffender();
        }

        protected override void ExecuteBlock()
        {

            // Find an active offender who doesn't have any request in the queue //
            try
            {
                HashSet<int> hashset = new HashSet<int>();
                Queue0.EntQueueProgramTracker EntQueueProgramTracker;
                for (int i=0; i< GetQueueResponse.RequestList.Length; i++)
                {
                    EntQueueProgramTracker = (Queue0.EntQueueProgramTracker)GetQueueResponse.RequestList[i];
                    hashset.Add(EntQueueProgramTracker.OffenderId);
                }
                // Check that the offender is for automation //
                for (int i = 0; i < GetOffendersResponse.OffendersList.Length; i++)
                {
                    if (/*GetOffendersResponse.OffendersList[i].FirstName.Contains("Automation") &&*/ !hashset.Contains(GetOffendersResponse.OffendersList[i].ID))
                  //  if (!GetOffendersResponse.OffendersList[i].FirstName.Contains("******") && !hashset.Contains(GetOffendersResponse.OffendersList[i].ID))

                        {
                            Offender = GetOffendersResponse.OffendersList[i];
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                throw new Exception(e + " - there's no active offenders!");
            }

            if (Offender.ID == 0)
                throw new Exception("there's no active offenders!");

        }
    }
}
