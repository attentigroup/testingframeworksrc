﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Offenders;
using WebTests.SeleniumWrapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestBlocksLib.OffendersBlocks
{
    public class WaitForSpecificOffenderStatusTestBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Timeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int SecondsTimeOut { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramStatus ExpectedStatus { get; set; }

        Offenders0.EntMsgGetOffendersResponse CurrentOffendersResponse => OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);
        protected override void ExecuteBlock()
        {
            Wait.Until(() => CurrentOffendersResponse.OffendersList[0].ProgramStatus == ExpectedStatus, $"Offender status: {CurrentOffendersResponse.OffendersList[0].ProgramStatus}, not as expected after timeout of {SecondsTimeOut} seconds", SecondsTimeOut);
        }
    }
}
