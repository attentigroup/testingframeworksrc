﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class UpdateOffenderAddressTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgUpdateOffenderAddressRequest UpdateOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy.Instance.UpdateOffenderAddress(UpdateOffenderAddressRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateOffenderAddressTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgUpdateOffenderAddressRequest UpdateOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_1.Instance.UpdateOffenderAddress(UpdateOffenderAddressRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateOffenderAddressTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgUpdateOffenderAddressRequest UpdateOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            OffendersProxy_2.Instance.UpdateOffenderAddress(UpdateOffenderAddressRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
