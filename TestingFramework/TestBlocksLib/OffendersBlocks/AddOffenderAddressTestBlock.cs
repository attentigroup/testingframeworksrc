﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class AddOffenderAddressTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddOffenderAddressResponse AddOffenderAddressResponse { get; set; }


        protected override void ExecuteBlock()
        {
            AddOffenderAddressResponse = OffendersProxy.Instance.AddOffenderAddress(AddOffenderAddressRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderAddress = new Offenders0.EntMsgDeleteOffenderAddressRequest();
            deleteOffenderAddress.OffenderID = AddOffenderAddressRequest.OffenderID;
            deleteOffenderAddress.AddressID = AddOffenderAddressResponse.NewAddressID;
            OffendersProxy.Instance.DeleteOffenderAddress(deleteOffenderAddress);
        }
    }


    public class AddOffenderAddressTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgAddOffenderAddressResponse AddOffenderAddressResponse { get; set; }


        protected override void ExecuteBlock()
        {
            AddOffenderAddressResponse = OffendersProxy_1.Instance.AddOffenderAddress(AddOffenderAddressRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderAddress = new Offenders1.EntMsgDeleteOffenderAddressRequest();
            deleteOffenderAddress.OffenderID = AddOffenderAddressRequest.OffenderID;
            deleteOffenderAddress.AddressID = AddOffenderAddressResponse.NewAddressID;
            OffendersProxy_1.Instance.DeleteOffenderAddress(deleteOffenderAddress);
        }
    }


    public class AddOffenderAddressTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgAddOffenderAddressResponse AddOffenderAddressResponse { get; set; }


        protected override void ExecuteBlock()
        {
            AddOffenderAddressResponse = OffendersProxy_2.Instance.AddOffenderAddress(AddOffenderAddressRequest);
        }

        public override void CleanUp()
        {
            var deleteOffenderAddress = new Offenders2.EntMsgDeleteOffenderAddressRequest();
            deleteOffenderAddress.OffenderID = AddOffenderAddressRequest.OffenderID;
            deleteOffenderAddress.AddressID = AddOffenderAddressResponse.NewAddressID;
            OffendersProxy_2.Instance.DeleteOffenderAddress(deleteOffenderAddress);
        }
    }
}
