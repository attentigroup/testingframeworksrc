﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.OffendersBlocks
{
    public class GetAggressorWithVictimTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender Offender { get; set; }

        public GetAggressorWithVictimTestBlock()
        {
            Offender = new Offenders0.EntOffender();
        }
        protected override void ExecuteBlock()
        {
            var getOffenders = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
            {
                ProgramType = new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece },
                ProgramConcept = Offenders0.EnmProgramConcept.Aggressor,
                ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }

            });

            foreach(var offender in getOffenders.OffendersList) 
            {
                if (offender.RelatedOffenderID != null)
                {
                    Offender = offender;
                    break;
                }
                    
            }
        }
    }
}
