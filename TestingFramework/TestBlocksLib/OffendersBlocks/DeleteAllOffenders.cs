﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using TestingFramework.Proxies.API.Offenders;
using TestBlocksLib.DevicesBlocks;
using TestingFramework.Proxies.API.OffenderActions;

#endregion

namespace TestBlocksLib.OffendersBlocks
{
    public class DeleteAllOffenders : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public long Timestamp { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }



        protected override void ExecuteBlock()
        {
            foreach(var offender in GetOffendersResponse.OffendersList)
            {
                if(offender.ProgramStatus == Offenders1.EnmProgramStatus.Active)
                {
                    OffenderActionsProxy_1.Instance.SendEndOfServiceRequest(new TestingFramework.Proxies.EM.Interfaces.OffenderActions3_10.EntMsgSendEndOfServiceRequest()
                    {
                        OffenderID = offender.ID,
                        EndOfServiceType = TestingFramework.Proxies.EM.Interfaces.OffenderActions3_10.EnmEOSType.Default,
                        EndOfServiceCode = "AEOS",
                        DeactivateReceiver = false,
                    });

                    RunXTSimulatorTestBlock xtSimulator = new RunXTSimulatorTestBlock();
                    xtSimulator.EquipmentSN = offender.EquipmentInfo.ReceiverSN;
                    xtSimulator.Execute();
                }
                OffendersProxy_1.Instance.DeleteOffender(new Offenders1.EntMsgDeleteOffenderRequest()
                {
                    Timestamp = (ulong)offender.TimeStamp/*Timestamp*/,
                    OffenderID = offender.ID
                });
            }

            
        }
    }
}
