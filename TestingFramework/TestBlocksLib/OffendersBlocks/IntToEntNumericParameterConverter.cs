﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.OffendersBlocks
{
    public class IntToEntNumericParameterConverter : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ElementToConvert, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ElementToConvert { get; set; }

        [PropertyTest(EnumPropertyName.ConvertedValue, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntNumericParameter ConvertedValue { get; set; }

        protected override void ExecuteBlock()
        {
            ConvertedValue = new Offenders0.EntNumericParameter() { Operator = Offenders0.EnmNumericOperator.Equal, Value = ElementToConvert };
        }

    }
}
