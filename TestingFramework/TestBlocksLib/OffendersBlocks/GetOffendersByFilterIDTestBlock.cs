﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Linq;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestBlocksLib.OffendersBlocks
{
    /// <summary>
    /// using as request for privat API method GetOffendersByFilterID
    /// </summary>
    public class GetOffendersByFilterIDRequest
    {
        public GetDataResultOfEntOffenderShortypU6tq2N response;

        public int filterID { get; set; }
        public string search { get; set; }
        public EntSortExpression sort { get; set; }
        public int start { get; set; }
        public int max { get; set; }

        public GetOffendersByFilterIDRequest(int filter_id)
        {
            this.filterID = filter_id;
            response =APIExtensionsProxy.Instance.GetOffendersByFilterID(filterID, search, sort, start, max);
        }
    }
    /// <summary>
    /// using as response for private API method GetOffendersByFilterID
    /// </summary>
    public class GetOffendersByFilterIDResponse
    {
        public GetDataResultOfEntOffenderShortypU6tq2N GetOffendersByFilterIDRes { get; set; }
        public int TotalRowCount;
        public GetOffendersByFilterIDResponse(GetDataResultOfEntOffenderShortypU6tq2N response)
        {
            TotalRowCount = response.TotalRowCount;
            GetOffendersByFilterIDRes = response;
        }
    }

    public class GetOffendersByFilterIDTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersByFilterIDRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public GetOffendersByFilterIDRequest GetOffendersByFilterIDRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersByFilterIDResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public GetOffendersByFilterIDResponse GetOffendersByFilterIDResponse { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int AmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.filterID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int FilterID { get; set; }


        protected override void ExecuteBlock()
        {
            GetOffendersByFilterIDRequest= new GetOffendersByFilterIDRequest (FilterID );
            if (GetOffendersByFilterIDRequest.response != null)
                AmountOfRows = GetOffendersByFilterIDRequest.response.TotalRowCount;
            else
                AmountOfRows = 0;
        }
    }
}
