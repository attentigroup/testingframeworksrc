﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions.Security;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetGuiSecurityDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UserName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string UserName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.UserType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmUserType UserType { get; set; }

        [PropertyTest(EnumPropertyName.GuiSecurityResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntSecurity> GuiSecurityResponse { get; set; }

        protected override void ExecuteBlock()
        {
            if (UserName == null)
                UserName = ConfigurationManager.AppSettings["GuiSecurityUserName"];

            if (Password == null)
                Password = ConfigurationManager.AppSettings["GuiSecurityPassword"];
                      
            var userType = ConfigurationManager.AppSettings["UserType"];
            Enum.TryParse(userType, out EnmUserType UserType);
            

            GuiSecurityResponse = APIExtensionsSecurityProxy.Instance.GetGUISecurityData(UserName, UserType);
        }
    }
}


