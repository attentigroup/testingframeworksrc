﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;


namespace TestBlocksLib.APIExtensions
{
    public class GetUnallocatedOffenderListBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.ProgramGroup, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmProgramGroup ProgramGroup { get; set; }

        [PropertyTest(EnumPropertyName.GetUnallocatedOffenderListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntGroupOffender> GetUnallocatedOffenderListResponse { get; set; }

        [PropertyTest(EnumPropertyName.OffendersListIDs, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int[] OffendersListIDs { get; set; }


        protected override void ExecuteBlock()
        {
            GetUnallocatedOffenderListResponse = APIExtensionsProxy.Instance.GetUnallocatedOffenderList(ProgramGroup);
            int[] AllOffendersListIDs = GetUnallocatedOffenderListResponse.Select(x => x.ID).ToArray();
            OffendersListIDs = AllOffendersListIDs.Take(Limit).ToArray();
        }
    }
}
