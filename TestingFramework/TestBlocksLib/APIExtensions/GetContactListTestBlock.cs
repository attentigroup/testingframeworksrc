﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetContactListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmContactType ContactType { get; set; }

        [PropertyTest(EnumPropertyName.UserID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? UserID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ContactList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntUser> ContactList { get; set; }
        protected override void ExecuteBlock()
        {
            ContactList = APIExtensionsProxy.Instance.GetContactList(ContactType, UserID, OffenderID);
        }
    }
}
