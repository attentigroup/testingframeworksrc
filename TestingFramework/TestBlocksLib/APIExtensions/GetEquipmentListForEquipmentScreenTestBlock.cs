﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetEquipmentListForEquipmentScreenTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEquipmentListRequestForEquipmentScreen, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntMsgGetEquipmentListRequestForEquipmentScreen GetEquipmentListRequestForEquipmentScreen { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentListData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntEquipmentListData EquipmentListData { get; set; }
        protected override void ExecuteBlock()
        {
            EquipmentListData = APIExtensionsProxy.Instance.GetEquipmentListFofEquipmentScreen(GetEquipmentListRequestForEquipmentScreen);
        }
    }
}
