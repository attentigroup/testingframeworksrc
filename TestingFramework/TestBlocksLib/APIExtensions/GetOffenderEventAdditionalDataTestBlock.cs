﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetOffenderEventAdditionalDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.EventCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EventCode { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EventTime { get; set; }

        [PropertyTest(EnumPropertyName.EventSeverity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmEventSeverity EventSeverity { get; set; }

        [PropertyTest(EnumPropertyName.HandlingStatusType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmHandlingStatusType? HandlingStatusType { get; set; }

        [PropertyTest(EnumPropertyName.IsHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsHistory { get; set; }

        [PropertyTest(EnumPropertyName.EventVersion, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? EventVersion { get; set; }

        [PropertyTest(EnumPropertyName.IsAlcoholEvent, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsAlcoholEvent { get; set; }

        [PropertyTest(EnumPropertyName.MapProvider, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmMapProvider MapProvider { get; set; }

        [PropertyTest(EnumPropertyName.EventAdditionalData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntEventAdditionalData EventAdditionalData { get; set; }
        protected override void ExecuteBlock()
        {
            EventAdditionalData = APIExtensionsProxy.Instance.GetOffenderEventAdditionalData(OffenderID, EventID, EventCode, EventTime, EventSeverity, HandlingStatusType, IsHistory, EventVersion, IsAlcoholEvent, MapProvider);
        }
    }
}
