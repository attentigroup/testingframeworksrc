﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetOffenderSummaryTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.MapProvider, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmMapProvider MapProvider { get; set; }

        [PropertyTest(EnumPropertyName.OffenderSummaryData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntOffenderSummaryData OffenderSummaryData { get; set; }
        protected override void ExecuteBlock()
        {
            OffenderSummaryData = APIExtensionsProxy.Instance.GetOffenderSummary(OffenderID, MapProvider);
        }
    }
}
