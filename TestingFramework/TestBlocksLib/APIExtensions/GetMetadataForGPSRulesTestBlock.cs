﻿using Common.CustomAttributes;
using Common.Enum;
using System.Collections.Generic;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetMetadataForGPSRulesTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.MetadataForGPSRules, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntRuleFieldsSetup> MetadataForGPSRules { get; set; }

        protected override void ExecuteBlock()
        {
            MetadataForGPSRules = APIExtensionsProxy.Instance.GetMetadataForGPSRules();
        }
    }
}
