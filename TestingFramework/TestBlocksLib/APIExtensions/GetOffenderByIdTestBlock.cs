﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetOffenderByIdTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntOffender Offender { get; set; }
        protected override void ExecuteBlock()
        {
            Offender = APIExtensionsProxy.Instance.GetOffenderByID(OffenderID);
        }
    }
}
