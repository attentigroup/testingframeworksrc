﻿using Common.CustomAttributes;
using Common.Enum;
using System.Collections.Generic;
using System.Linq;
using TestingFramework.Proxies.API.APIExtensions.Security;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class SetGUISecurityDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string UserName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.ModifyOption, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModifyOption ModifyOption { get; set; }

        [PropertyTest(EnumPropertyName.ModifyStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModify ModifyStatus { get; set; }

        [PropertyTest(EnumPropertyName.GuiSecurityResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntSecurity> GuiSecurityResponse { get; set; }

        [PropertyTest(EnumPropertyName.ResourceID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string[] ResourceID { get; set; }

        protected override void ExecuteBlock()
        {
            var entSecurityItem = new List<EntSecurity>();

            var response = GuiSecurityResponse.FindAll(x => ResourceID.Any(y => x.ResourceID.Contains(y)));

            foreach (var item in response)
            {
                item.ModifyOption = ModifyOption;

                item.ModifyStatus = ModifyStatus;

                entSecurityItem.Add(item);
            }

            APIExtensionsSecurityProxy.Instance.SetGUISecurityData(UserName, entSecurityItem);
        }

        public override void CleanUp()
        {
            //if (UserName != "SA")//Remove this condition after fixing bug
                APIExtensionsSecurityProxy.Instance.SetSecurityToDefault(UserName);
        }
    }
}