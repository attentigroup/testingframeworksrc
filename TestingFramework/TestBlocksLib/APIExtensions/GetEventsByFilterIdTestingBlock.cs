﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetEventsByFilterIdTestingBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsByFilterID_StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_MaximumRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_FilterID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int FilterID { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_ScreenName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScreenName { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_PreviousTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public byte[] PreviousTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_PageUpperLimitEventId, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int PageUpperLimitEventId { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_SortExpression, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntSortExpression SortExpression{ get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_SearchString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchString { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_FromTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? FromTime { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_ToTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ToTime { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_ForceRefresh, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ForceRefresh { get; set; }//in case of changes in ui for example first request with out searchString and in the second time with searchString so  need to be true.

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OffenderId { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        protected EnmProgramType? programType { get; set; }

        [PropertyTest(EnumPropertyName.EntEventDataResult, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntEventDataResult EventDataResult { get; set; }


        private bool includeOffenderDetails = true;//alwways true - It's old parameter not in use anymore

        public GetEventsByFilterIdTestingBlock()
        {
            includeOffenderDetails = true;//alwways true

            OffenderId = null;//NR - in case of currenyt status screen
            programType = null;//NR - in case of currenyt status screen

            SearchString = string.Empty;
            SortExpression = null;
        }
        protected override void ExecuteBlock()
        {
            EventDataResult = APIExtensionsProxy.Instance.GetEventsByFilterID(StartRowIndex, MaximumRows, FilterID, ScreenName, PreviousTimeStamp,
                    PageUpperLimitEventId, OffenderId, includeOffenderDetails, SortExpression, SearchString, FromTime, ToTime, ForceRefresh, programType);
        }
    }
}
