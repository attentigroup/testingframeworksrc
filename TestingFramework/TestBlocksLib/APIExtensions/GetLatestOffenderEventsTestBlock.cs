﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetLatestOffenderEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.MetadataForGPSRules, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventLogID { get; set; }

        [PropertyTest(EnumPropertyName.MetadataForGPSRules, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? RowCount { get; set; }

        [PropertyTest(EnumPropertyName.MetadataForGPSRules, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntEventExtended> LastOffenderEvents { get; set; }
        protected override void ExecuteBlock()
        {
            LastOffenderEvents = APIExtensionsProxy.Instance.GetLatestOffenderEvents(EventLogID, RowCount);
        }
    }
}
