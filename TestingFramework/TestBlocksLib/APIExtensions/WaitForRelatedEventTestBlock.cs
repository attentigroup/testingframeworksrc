﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.APIExtensions
{
    public class WaitForRelatedEventTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.APIAmountOfRowsBefore, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int APIAmountOfRowsBefore { get; set; }

        [PropertyTest(EnumPropertyName.APIAmountOfRowsAfter, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int APIAmountOfRowsAfter { get; set; }

        [PropertyTest(EnumPropertyName.GetRelatedEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntEventExtended> GetRelatedEventsResponse { get; set; }
        /// <summary>
        /// output of the test - return the evnt_id of the schedule task event that linked with the EventID
        /// </summary>
        [PropertyTest(EnumPropertyName.LinkedEventID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int LinkedEventID { get; set; }

        [PropertyTest(EnumPropertyName.DelayTimeForLinkedEvent, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int DelayTimeForLinkedEvent { get; set; }

        public WaitForRelatedEventTestBlock()
        {
            DelayTimeForLinkedEvent = 120;
        }


        protected override void ExecuteBlock()
        {
            
            Wait.Until(() => ((APIExtensionsProxy.Instance.GetRelatedEvents(EventID, 2000).Count) > APIAmountOfRowsBefore), "Linked event not exist after timeout of seconds", 120+DelayTimeForLinkedEvent);

            List<EntEventExtended> linkedEventsList = APIExtensionsProxy.Instance.GetRelatedEvents(EventID, 2000);
            APIAmountOfRowsAfter = APIExtensionsProxy.Instance.GetRelatedEvents(EventID, 2000).Count;
            var lastDateEventTime = DateTime.MinValue;
            LinkedEventID = -1;

            foreach (var linkedEvent in linkedEventsList)
            {
                if (linkedEvent.EventTime.HasValue == true && 
                    linkedEvent.EventTime > lastDateEventTime)
                {
                    lastDateEventTime = linkedEvent.EventTime.Value;
                    LinkedEventID = linkedEvent.ID;
                }
            }
            
            if ( LinkedEventID == -1)
            {
                throw new Exception("No linked event found");
            }

        }
    }
}