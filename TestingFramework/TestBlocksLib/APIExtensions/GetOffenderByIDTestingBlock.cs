﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{/// <summary>
/// block get offender id and retrive all 
/// the offender data using GetOffendeID Method in APIExtention service
/// </summary>
    public class GetOffenderByIDTestingBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        protected int OffenderId { get; set; }


        [PropertyTest(EnumPropertyName.EntEventDataResult, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntOffender EntOffenderResult { get; set; }

        protected override void ExecuteBlock()
        {
            EntOffenderResult = APIExtensionsProxy.Instance.GetOffenderByID(OffenderId);
        }
    }
}
