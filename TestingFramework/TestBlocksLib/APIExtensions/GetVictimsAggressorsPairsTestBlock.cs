﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{
    public class GetVictimsAggressorsPairsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.ShowOnlyActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowOnlyActive { get; set; }

        [PropertyTest(EnumPropertyName.GetVictimAggressorPairsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public GetDataResultOfEntDVRelationDataypU6tq2N GetVictimAggressorPairsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetVictimAggressorPairsResponse = APIExtensionsProxy.Instance.GetVictimsAggressorsPairs(StartRowIndex, MaximumRows, ShowOnlyActive);
        }
    }
}
