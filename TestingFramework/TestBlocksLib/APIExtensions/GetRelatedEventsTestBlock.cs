﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.APIExtensions
{/// <summary>
/// Class create the get message of GetRelatedEvents request from APIExtentions
/// </summary>
    public class GetRelatedEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EventLogID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventLogID { get; set; }

        [PropertyTest(EnumPropertyName.RowCount, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RowCount { get; set; }


        [PropertyTest(EnumPropertyName.APIAmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int APIAmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.GetRelatedEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List <EntEventExtended> GetRelatedEventsResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetRelatedEventsResponse = APIExtensionsProxy.Instance.GetRelatedEvents(EventLogID, RowCount);
            APIAmountOfRows = GetRelatedEventsResponse.Count;
        }
    }
}
