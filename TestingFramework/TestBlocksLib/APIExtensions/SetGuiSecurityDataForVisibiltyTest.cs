﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TestingFramework.Proxies.API.APIExtensions.Security;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.APIExtensions
{
    public class SetGuiSecurityDataForVisibilityTest : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UserName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string UserName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.ModifyOption, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModifyOption ModifyOption { get; set; }

        [PropertyTest(EnumPropertyName.ModifyStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModify ModifyStatus { get; set; }

        [PropertyTest(EnumPropertyName.GuiSecurityResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntSecurity> GuiSecurityResponse { get; set; }

        [PropertyTest(EnumPropertyName.ResourceID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] ResourceID { get; set; }

        [PropertyTest(EnumPropertyName.Refine, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string[] Refine { get; set; }

        [PropertyTest(EnumPropertyName.ExcludeItem, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] ExcludeItems { get; set; }

        [PropertyTest(EnumPropertyName.RefineNewRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntSecurity> RefineNewRequest { get; set; }

        [PropertyTest(EnumPropertyName.IsExact, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsExactEqual { get; set; }

        public SetGuiSecurityDataForVisibilityTest()
        {
        }
        protected override void ExecuteBlock()
        {
            CleanUp();

            List<EntSecurity> response;
            Logger.WriteLine($"Modify Options: {ModifyOption}; Modify Status: {ModifyStatus}");
            var Response = new List<EntSecurity>();
            var GuiSecurityResponseCopy = new List<EntSecurity>(GuiSecurityResponse);

            if (ExcludeItems != null)
            {
                var excludeItemsList = ExcludeItems.ToList<string>();
                foreach (string excludeItem in excludeItemsList)
                {
                    //var findExcludeItems = GuiSecurityResponse.FindAll(x => x.ResourceID == excludeItem);

                    var findExcludeItems = GuiSecurityResponse.FindAll(x => x.ResourceID.StartsWith(excludeItem));

                    for (int i = 0; i < findExcludeItems.Count; i++)
                    {
                        GuiSecurityResponseCopy.Remove(findExcludeItems[i]);
                    }
                }
            }

            if (IsExactEqual == false)
                response = GuiSecurityResponseCopy.FindAll(x => Refine.Any(y => x.ResourceID.StartsWith(y)));
            else
                response = GuiSecurityResponseCopy.FindAll(x => Refine.Any(y => x.ResourceID.Equals(y)));

            if (GuiSecurityResponse.Any() && !response.Any())
                Assert.Fail("No element(s) fit the refine criteria");
            else if (!GuiSecurityResponse.Any())
                Assert.Fail("Security response is empty!");

            RefineNewRequest = response;
           
            foreach (var item in RefineNewRequest)
            {
                item.ModifyOption = ModifyOption;
                item.ModifyStatus = ModifyStatus;
                Response.Add(item);
            }


            APIExtensionsSecurityProxy.Instance.SetGUISecurityData(UserName, Response);
        }
        public override void CleanUp()
        {
                APIExtensionsSecurityProxy.Instance.SetSecurityToDefault(UserName);
        }
    }
}
