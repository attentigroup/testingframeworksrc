﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Agency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Agency;
using TestingFramework.Proxies.API.Users;
using TestingFramework.TestsInfraStructure.Implementation;
using Users = TestingFramework.Proxies.EM.Interfaces.Users;

namespace TestBlocksLib.OfficerBlocks
{
    public class GetOfficerTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.GetAgenciesRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Users.EntMsgGetOfficerRequest GetAgenciesRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOfficerResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Users.EntMsgGetOfficerResponse GetOfficerResponse { get; set; }

        [PropertyTest(EnumPropertyName.OfficerName, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string OfficerName { get; set; }


        protected override void ExecuteBlock()
        {
            var OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID);
            GetOfficerResponse = UsersProxy.Instance.GetOfficer(new Users.EntMsgGetOfficerRequest()
            {
                OfficerID = OfficerID
            });

            OfficerName = GetOfficerResponse.Officer.LastName + " " + GetOfficerResponse.Officer.FirstName;

        }
    }
}
