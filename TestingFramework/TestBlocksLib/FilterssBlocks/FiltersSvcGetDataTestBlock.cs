﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.API.Filters;
using TestingFramework.Proxies.EM.Interfaces.Filters12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.FilterssBlocks
{
    public class FiltersSvcGetDataTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.GetDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GetDataRequest GetDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public GetDataResponse GetDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetDataResponse = FiltersProxy.Instance.GetData(GetDataRequest);
        }
    }
}
