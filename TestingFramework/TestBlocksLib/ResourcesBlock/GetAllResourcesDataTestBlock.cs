﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.ResourcesBlock
{
    public class GetAllResourcesDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ResourcePlatform, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmResourcePlatform ResourcePlatform { get; set; }

        [PropertyTest(EnumPropertyName.GetAllResourcesData, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntResource> GetAllResourcesData { get; set; }

        public GetAllResourcesDataTestBlock()
        {
            ResourcePlatform = EnmResourcePlatform.WEB;
        }
        protected override void ExecuteBlock()
        {
            GetAllResourcesData = APIResourcesProxy.Instance.GetAllResourceData(ResourcePlatform);
        }

        public override void CleanUp()
        {
            
        }

    }
}
