﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.ResourcesBlock
{
    public class UpdateEmsSystableTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EmsSystableDictionary, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Dictionary<EnmDBAction, List<EntSysTableItem>> CustomFieldsListValue { get; set; }

        [PropertyTest(EnumPropertyName.EmsSystableDictionary, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Dictionary<EnmDBAction, List<EntSysTableItem>> CleanUp_CustomFieldDictionary { get; set; }
        protected override void ExecuteBlock()
        {
            APIResourcesProxy.Instance.UpdateEmsSystable(CustomFieldsListValue);
        }

        public override void CleanUp()
        {           
            APIResourcesProxy.Instance.UpdateEmsSystable(CleanUp_CustomFieldDictionary);
        }

        
    }
}
