﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class GetPredefinedListsFromSysTableTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.TableID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableID { get; set; }

        [PropertyTest(EnumPropertyName.CustomFields, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntCustomFiledListItem> CustomFields { get; set; }
        protected override void ExecuteBlock()
        {
            CustomFields = InformationProxy.Instance.GetPredefinedListsFromSysTable(new List<string>() { TableID});
        }
    }
}
