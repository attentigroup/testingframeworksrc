﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class UpdateCustomFieldsNanesTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        private List<string> CustomFieldsNames = new List<string>();

        protected override void ExecuteBlock()
        {
            foreach (var customField in CustomFields)
            {
                CustomFieldsNames.Add(customField.Name);
                customField.Name = customField.WebFieldType.ToString();
            }
            InformationProxy.Instance.UpdateCustomFieldsSystemDefinitions(CustomFields);
        }

        public override void CleanUp()
        {
            int i = 0;
            foreach (var customField in CustomFields)
            {
                customField.Name = CustomFieldsNames[i];
                i++;
            }
            InformationProxy.Instance.UpdateCustomFieldsSystemDefinitions(CustomFields);
        }


    }
}
