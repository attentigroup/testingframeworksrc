﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion



namespace TestBlocksLib.InformationBlock
{
    /// <summary>
    /// get the visible custom fields defnition
    /// </summary>
    public class GetCustomFieldsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.VisibleCustomFieldsDefinitions, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntCustomFieldDefinition[] VisibleCustomFieldsDefinitions { get; set; }

        
        protected override void ExecuteBlock()
        {
            VisibleCustomFieldsDefinitions = OffendersProxy.Instance.GetCustomFieldsDefinitions();
        }
    }
}
