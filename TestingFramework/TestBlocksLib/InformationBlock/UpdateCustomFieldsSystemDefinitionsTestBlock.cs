﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    //TODO NOa find what we do insted of this
    public class UpdateCustomFieldsSystemDefinitionsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsSystemDefinitions { get; set; }

        protected override void ExecuteBlock()
        {
            InformationProxy.Instance.UpdateCustomFieldsSystemDefinitions(CustomFieldsSystemDefinitions);
        }
    }
}
