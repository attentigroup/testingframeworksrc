﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class GetCustomFieldsSystemDefinitionsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsSystemDefinitions { get; set; }


        protected override void ExecuteBlock()
        {
            CustomFieldsSystemDefinitions = InformationProxy.Instance.GetCustomFieldsSystemDefinitions().DefinitionsList;
        }
    }
}
