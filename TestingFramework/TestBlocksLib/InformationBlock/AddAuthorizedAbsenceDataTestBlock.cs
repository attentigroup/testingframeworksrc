﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.ProgramActions
{
    public class AddAuthorizedAbsenceDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddAuthorizedAbsenceDataRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Information0.EntMsgAddAuthorizedAbsenceDataRequest AddAuthorizedAbsenceDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            InformationProxy.Instance.AddAuthorizedAbsenceData(AddAuthorizedAbsenceDataRequest);
        }

        public override void CleanUp()
        {
            var GetAuthorizedAbsenceData = InformationProxy.Instance.GetAuthorizedAbsenceRequestsList(new Information0.EntMsgGetAuthorizedAbsenceRequestsListRequest()
            {
                OffenderID = AddAuthorizedAbsenceDataRequest.OffenderID
            });
            if(GetAuthorizedAbsenceData.AbsenceRequestsList == null || GetAuthorizedAbsenceData.AbsenceRequestsList.Count == 0)
            {
                throw new Exception(string.Format("Offender with ID: {0} doesn't have absence requests", AddAuthorizedAbsenceDataRequest.OffenderID));
            }

            InformationProxy.Instance.DeleteAuthorizedAbsenceData(new Information0.EntMsgDeleteAuthorizedAbsenceDataRequest()
            {
                OffenderID = AddAuthorizedAbsenceDataRequest.OffenderID,
                RequestID = GetAuthorizedAbsenceData.AbsenceRequestsList[0].ID 
            });
        }
    }
}
