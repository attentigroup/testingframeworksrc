﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.InformationBlock
{
    public class AddScheduleChangeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddScheduleChangeRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Information0.EntMsgAddScheduleChangeRequest AddScheduleChangeRequest { get; set; }

        protected override void ExecuteBlock()
        {
            InformationProxy.Instance.AddScheduleChange(AddScheduleChangeRequest);
        }

        public override void CleanUp()
        {
            var GetScheduleChange = InformationProxy.Instance.GetScheduleChanges(new Information0.EntMsgGetScheduleChangesRequest()
            {
                OffenderID = AddScheduleChangeRequest.OffenderID
            });
            if (GetScheduleChange.ChangesList == null || GetScheduleChange.ChangesList.Count == 0)
            {
                throw new Exception(string.Format("Offender with ID: {0} doesn't have schedule changes", AddScheduleChangeRequest.OffenderID));
            }
            InformationProxy.Instance.DeleteScheduleChange (new Information0.EntMsgDeleteScheduleChangeRequest()
            {
                OffenderID = AddScheduleChangeRequest.OffenderID,
                ChangeID = GetScheduleChange.ChangesList[0].ID
            });

            
        }
    }
}
