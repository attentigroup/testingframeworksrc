﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.InformationBlock
{
    public class AddOffenderWarningTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddOffenderWarningRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Information0.EntMsgAddOffenderWarningRequest AddOffenderWarningRequest { get; set; }

        protected override void ExecuteBlock()
        {
            InformationProxy.Instance.AddOffenderWarning(AddOffenderWarningRequest);
        }
        public override void CleanUp()
        {
            var GetOffenderWarning = InformationProxy.Instance.GetOffenderWarnings(new Information0.EntMsgGetOffenderWarningsRequest()
            {
                OffenderID = AddOffenderWarningRequest.OffenderID
            });
            if (GetOffenderWarning.OffenderWarningsData == null || GetOffenderWarning.OffenderWarningsData.Count == 0)
            {
                throw new Exception(string.Format("Offender with ID: {0} doesn't have warnings", AddOffenderWarningRequest.OffenderID));
            }
            InformationProxy.Instance.DeleteOffenderWarning(new Information0.EntMsgDeleteOffenderWarningRequest()
            {
                OffenderID = AddOffenderWarningRequest.OffenderID,
                WarningNumber = GetOffenderWarning.OffenderWarningsData[0].WarningNumber
            });
        }
    }
}
