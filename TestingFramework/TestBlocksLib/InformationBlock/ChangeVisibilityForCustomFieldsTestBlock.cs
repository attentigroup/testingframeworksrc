﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class ChangeVisibilityForCustomFieldsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        protected override void ExecuteBlock()
        {           
            InformationProxy.Instance.UpdateCustomFieldsSystemDefinitions(CustomFields);

        }

        public override void CleanUp()
        {
            foreach (var customField in CustomFields)
            {
                customField.ShowInOffenderDetails = true;
                customField.ShowInOffendersList = true;
            }
            InformationProxy.Instance.UpdateCustomFieldsSystemDefinitions(CustomFields);
        }
    }
}
