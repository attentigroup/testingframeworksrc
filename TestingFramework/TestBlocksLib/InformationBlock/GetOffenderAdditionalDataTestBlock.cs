﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class GetOffenderAdditionalDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderAdditionalData, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntOffenderAdditionalData OffenderAdditionalData { get; set; }
        protected override void ExecuteBlock()
        {
            OffenderAdditionalData = InformationProxy.Instance.GetOffenderAdditionalData(OffenderID);
        }
    }
}
