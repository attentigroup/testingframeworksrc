﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class GetCustomFieldsByFieldTypeTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsDefinitions { get; set; }

        [PropertyTest(EnumPropertyName.FieldType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmWebFieldType[] FieldTypes { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTypeLst { get; set; }

        public GetCustomFieldsByFieldTypeTestBlock()
        {
            CustomFieldsByTypeLst = new List<EntCustomFieldSystemDefinition>();
        }
        protected override void ExecuteBlock()
        {
            for(int i = 0; i < FieldTypes.Length; i++)
            {
                var customField = CustomFieldsDefinitions.FirstOrDefault(x => x.WebFieldType == FieldTypes[i]);
                customField.ShowInOffenderDetails = false;//generate value
                CustomFieldsByTypeLst.Add(customField);
            }
        }
    }
}
