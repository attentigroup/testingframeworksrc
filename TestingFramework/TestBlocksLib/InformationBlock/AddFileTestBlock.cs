﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;
namespace TestBlocksLib.InformationBlock
{
    public class AddFileTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.UploadFileRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Information0.EntMsgUploadFileRequest UploadFileRequest { get; set; }

        [PropertyTest(EnumPropertyName.request, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Information0.EntMsgGetFilesMetadataRequest request { get; set; }

        [PropertyTest(EnumPropertyName.OwnerEntity, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmOwnerEntity OwnerEntity { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.FileDisplayName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FileDisplayName { get; set; }

        protected override void ExecuteBlock()
        {
            
             InformationProxy.Instance.AddFile(UploadFileRequest);
        }

        public override void CleanUp()
        {
         request = new Information0.EntMsgGetFilesMetadataRequest();
            request.FileID = null;
            request.OwnerEntity = OwnerEntity;
            request.OwnerEntityID = OffenderID;
            request.FileDisplayName = FileDisplayName;

            var GetFileList = InformationProxy.Instance.GetFileList(request);

            var fileid = GetFileList.Entities[0].FileId;

            InformationProxy.Instance.DeleteFile(fileid, OwnerEntity, OffenderID);
        }
    }
}
