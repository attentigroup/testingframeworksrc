﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events12_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Events;
using System.Collections.Generic;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestBlocksLib.EventsBlocks
{
    public class GetEventsByGetOffendersResponseTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders12_0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.HandlingStatusType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events12_0.EnmHandlingStatusType HandlingStatusType { get; set; }

        [PropertyTest(EnumPropertyName.EventsList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<Events12_0.EntEvent> EventsList { get; set; }

        [PropertyTest(EnumPropertyName.OffenderId2EventsDictionary, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public DicInt2EventList Offenders2Events { get; set; }

        private Events12_0.EntMsgGetEventsRequest getEventsRequest;
        public GetEventsByGetOffendersResponseTestBlock()
        {
            Offenders2Events = new DicInt2EventList();

            EventsList = new List<Events12_0.EntEvent>();
            getEventsRequest = new Events12_0.EntMsgGetEventsRequest();
            getEventsRequest.IncludeHistory = false;


        }
        protected override void ExecuteBlock()
        {
            getEventsRequest.Status = HandlingStatusType;
            foreach (var offender in GetOffendersResponse.OffendersList)
            {
                getEventsRequest.OffenderID = offender.ID;
                Log.InfoFormat("Start Get events for offender id {0} ref id {1}.(getEventsRequest={2})", offender.ID, offender.RefID, getEventsRequest.ToLog());
                try
                {                    
                    Events12_0.EntMsgGetEventsResponse response = EventsProxy.Instance.GetEvents(getEventsRequest);
                    Log.InfoFormat("Get {0} events for offender id {1}.", response.EventsList.Length, offender.ID);
                    Offenders2Events.Add(offender.ID, new List<Events12_0.EntEvent>(response.EventsList));
                    EventsList.AddRange(response.EventsList);
                }
                catch (Exception exception)
                {
                    Log.ErrorFormat("GetEvents for offender {0} failed but block will continue to the next offender. Error: {1}", offender.ID, exception.Message);
                }                
            }
            Log.InfoFormat("Total {0} events for {1} offenders.", EventsList.Count, GetOffendersResponse.OffendersList.Length);
        }
    }
}
