﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class GetHandlingActionByQueryTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetHandlingActionsByQueryRequest GetHandlingActionsByQueryRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetHandlingActionsByQueryResponse = EventsProxy.Instance.GetHandlingActionsByQuery(GetHandlingActionsByQueryRequest);
        }
    }

    public class GetHandlingActionByQueryTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetHandlingActionsByQueryRequest GetHandlingActionsByQueryRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetHandlingActionsByQueryResponse = EventsProxy_1.Instance.GetHandlingActionsByQuery(GetHandlingActionsByQueryRequest);
        }
    }

    public class GetHandlingActionByQueryTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetHandlingActionsByQueryRequest GetHandlingActionsByQueryRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetHandlingActionsByQueryResponse = EventsProxy_2.Instance.GetHandlingActionsByQuery(GetHandlingActionsByQueryRequest);
        }
    }
}
