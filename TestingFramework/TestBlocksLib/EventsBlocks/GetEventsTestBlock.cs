﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Events;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class GetEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfEvents, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AmountOfEvents { get; set; }


        protected override void ExecuteBlock()
        {
            GetEventsResponse = EventsProxy.Instance.GetEvents(GetEventsRequest);
            AmountOfEvents = GetEventsResponse.EventsList.Count();
        }
    }

    public class GetEventsTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetEventsResponse = EventsProxy_1.Instance.GetEvents(GetEventsRequest);
        }
    }

    public class GetEventsTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        public List<Events2.EntOffenderEvent> OffenderEvents { get; set; }

        public GetEventsTestBlock_2()
        {
            OffenderEvents = new List<Events2.EntOffenderEvent>();
        }

        protected override void ExecuteBlock()
        {
            GetEventsResponse = EventsProxy_2.Instance.GetEvents(GetEventsRequest);

            foreach (Events2.EntEvent baseEvent in GetEventsResponse.EventsList)
            {
                if( baseEvent.Type == Events2.EnmEventType.Offender)
                {
                    OffenderEvents.Add((Events2.EntOffenderEvent)baseEvent);
                }
            }



        }
    }
}
