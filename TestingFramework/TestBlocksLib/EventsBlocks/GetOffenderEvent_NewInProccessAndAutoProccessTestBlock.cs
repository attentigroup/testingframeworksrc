﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Events;

namespace LogicBlocksLib.Events
{
    public class GetOffenderEvent_NewInProccessAndAutoProccessTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Event, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntEvent OffenderEvent { get; set; }

        protected override void ExecuteBlock()
        {
            var getEvents = EventsProxy.Instance.GetEvents(new Events0.EntMsgGetEventsRequest()
            {
                Severity = Events0.EnmEventSeverity.Violation,
                StartTime = DateTime.Now.AddHours(-66),
                EndTime = DateTime.Now
            });
            foreach (var oEvent in getEvents.EventsList)
            {
                if (oEvent.Status == Events0.EnmHandlingStatusType.New || oEvent.Status == Events0.EnmHandlingStatusType.InProcess ||
                    oEvent.Status == Events0.EnmHandlingStatusType.AutoProcess)
                {
                    OffenderEvent = oEvent;
                    break;
                }

            }

        }
    }
}
