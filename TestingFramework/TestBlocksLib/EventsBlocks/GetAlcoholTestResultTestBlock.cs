﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class GetAlcoholTestResultTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetAlcoholTestResultRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetAlcoholTestResultRequest GetAlcoholTestResultRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetAlcoholTestResultResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetAlcoholTestResultResponse = EventsProxy.Instance.GetAlcoholTestResult(GetAlcoholTestResultRequest);
        }
    }

    public class GetAlcoholTestResultTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetAlcoholTestResultRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetAlcoholTestResultRequest GetAlcoholTestResultRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetAlcoholTestResultResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetAlcoholTestResultResponse = EventsProxy_1.Instance.GetAlcoholTestResult(GetAlcoholTestResultRequest);
        }
    }
}
