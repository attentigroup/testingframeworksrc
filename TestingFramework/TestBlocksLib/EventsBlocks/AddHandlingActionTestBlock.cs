﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class AddHandlingActionTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EventsProxy.Instance.AddHandlingAction(AddHandlingActionRequest);
        }

        public override void CleanUp()
        {

        }
    }

    public class AddHandlingActionTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EventsProxy_1.Instance.AddHandlingAction(AddHandlingActionRequest);
        }

        public override void CleanUp()
        {

        }
    }

    public class AddHandlingActionTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EventsProxy_2.Instance.AddHandlingAction(AddHandlingActionRequest);
        }

        public override void CleanUp()
        {

        }
    }
}
