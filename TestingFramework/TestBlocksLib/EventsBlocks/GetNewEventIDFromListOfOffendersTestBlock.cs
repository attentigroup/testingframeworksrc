﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events12_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Events;
using System.Collections.Generic;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestBlocksLib.EventsBlocks
{
    /// <summary>
    /// Test Block that get list of offenders and list of events 
    /// and return the first event id that belong to any offender id from the offender list.  
    /// </summary>
    public class GetNewEventIDFromListOfOffendersTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders12_0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events12_0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.Event, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int EventID { get; set; }

        public string EventIdString { get { return EventID.ToString(); } }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int OffenderID { get; set; }

        protected override void ExecuteBlock()
        {
            foreach (var offender in GetOffendersResponse.OffendersList)
            {
                foreach (var event1 in GetEventsResponse.EventsList)
                    if (event1.OffenderID == offender.ID)
                    {
                        EventID = event1.ID;
                        OffenderID = offender.ID;
                        break;
                    }
            }
            if ((EventID == 0) || (OffenderID == 0))
                throw new Exception("Not found New event for active offender in the system");
        }
    }
}
