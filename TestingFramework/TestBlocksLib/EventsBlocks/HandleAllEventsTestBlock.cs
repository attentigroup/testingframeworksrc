﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class HandleAllEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.HandleAllEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgHandleAllEventsRequest HandleAllEventsRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EventsProxy.Instance.HandleAllEvents(HandleAllEventsRequest);
        }

        public override void CleanUp()
        {

                
        }
    }

    public class HandleAllEventsTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.HandleAllEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgHandleAllEventsRequest HandleAllEventsRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EventsProxy_1.Instance.HandleAllEvents(HandleAllEventsRequest);
        }
    }

    public class HandleAllEventsTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.HandleAllEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgHandleAllEventsRequest HandleAllEventsRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EventsProxy_2.Instance.HandleAllEvents(HandleAllEventsRequest);
        }
    }
}
