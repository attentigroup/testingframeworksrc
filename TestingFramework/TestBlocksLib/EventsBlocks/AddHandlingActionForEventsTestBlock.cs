﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Events;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Events12_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace TestBlocksLib.EventsBlocks
{
    public class AddHandlingActionForEventsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EventsPercentageToHandle, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventsPercentageToHandle { get; set; }

        [PropertyTest(EnumPropertyName.OffenderId2EventsDictionary, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DicInt2EventList Offenders2Events { get; set; }

        [PropertyTest(EnumPropertyName.HandlingActionType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events12_0.EnmHandlingActionType HandlingActionType { get; set; }

        [PropertyTest(EnumPropertyName.OfficersIdsList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<int> OfficerIdsList { get; set; }


        private Events12_0.EntMsgAddHandlingActionRequest AddHandlingActionRequest;

        private int _officerIndex = 0;

        public AddHandlingActionForEventsTestBlock()
        {
            AddHandlingActionRequest = new Events12_0.EntMsgAddHandlingActionRequest();

            
        }
        protected override void ExecuteBlock()
        {
            bool bErrors = false;

            AddHandlingActionRequest.Type = HandlingActionType;

            foreach (var kvp in Offenders2Events)
            {
                var offenderId = kvp.Key;
                var events = kvp.Value;

                int actualEvents2Handle = (int)Math.Round((EventsPercentageToHandle / 100.0)*events.Count);
                
                Log.InfoFormat("Going to handle {0} events for offender {1} - Action - {2}. ({3} % from total {4} events)", 
                    actualEvents2Handle, offenderId, AddHandlingActionRequest.Type, EventsPercentageToHandle, events.Count);

                for (int i = 0; i < actualEvents2Handle; i++)
                {
                    var eventEntity = events[i];
                    AddHandlingActionRequest.EventID = eventEntity.ID;
                    Log.DebugFormat("Going to handle event: {0} ", eventEntity.ToLog());
                    AddHandlingActionRequest.Comment = string.Format("{0} - AddHandlingActionForEventsTestBlock handle event id {1} for offender {2}",
                        DateTime.Now.ToLongTimeString(), eventEntity.ID, eventEntity.OffenderID);

                    switch (AddHandlingActionRequest.Type)
                    {
                        case Events12_0.EnmHandlingActionType.QuickHandle:
                            break;
                        case Events12_0.EnmHandlingActionType.Handle:
                            break;
                        case Events12_0.EnmHandlingActionType.Unhandled:
                            break;
                        case Events12_0.EnmHandlingActionType.PhoneCall:
                            break;
                        case Events12_0.EnmHandlingActionType.SendFax:
                            break;
                        case Events12_0.EnmHandlingActionType.SendTextMessage:
                            break;
                        case Events12_0.EnmHandlingActionType.SendEmail:
                            AddHandlingActionRequest.ContactType = Events12_0.EnmContactType.Officer;
                            AddHandlingActionRequest.ContactID = GetNextOfficer();
                            AddHandlingActionRequest.EmailAddress = GenerateEmailAddress();
                            break;
                        case Events12_0.EnmHandlingActionType.CreateRemark:
                            break;
                        case Events12_0.EnmHandlingActionType.CreateWarning:
                            break;
                        case Events12_0.EnmHandlingActionType.BatchHandling:
                            break;
                        case Events12_0.EnmHandlingActionType.ClearPanicAlert:
                            break;
                        case Events12_0.EnmHandlingActionType.ScheduleTask:
                            break;
                        case Events12_0.EnmHandlingActionType.CustomAction:
                            break;
                        case Events12_0.EnmHandlingActionType.SendToThirdParty:
                            break;
                        case Events12_0.EnmHandlingActionType.ReturnFromThirdParty:
                            break;
                        case Events12_0.EnmHandlingActionType.ClearTamper:
                            break;
                        default:
                            break;
                    }
                    try
                    {
                        EventsProxy.Instance.AddHandlingAction(AddHandlingActionRequest);
                    }
                    catch (Exception exception)
                    {
                        Log.ErrorFormat("Error while try to handle event {0} for offender id {1}, error message:{2}", eventEntity, eventEntity.OffenderID, exception.Message);
                        bErrors = true;
                    }
                }
                Log.InfoFormat("Finish handle {0} events {1} errors.", actualEvents2Handle, bErrors ? "With" : "Without");
            }
        }

        private string GenerateEmailAddress()
        {
            var EmailAddress = string.Format("{0}_{1}@{2}.com", Environment.MachineName, DateTime.Now.Ticks, Environment.UserDomainName);
            return EmailAddress;
        }

        private int? GetNextOfficer()
        {
            if (_officerIndex >= OfficerIdsList.Count)
                _officerIndex = 0;
            var officerId = OfficerIdsList[_officerIndex++];
            return officerId;
        }
    }
}
