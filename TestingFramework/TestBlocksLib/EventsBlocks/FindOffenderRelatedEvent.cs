﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;


namespace TestBlocksLib.EventsBlocks
{
    public class FindOffenderRelatedEvent : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int? OffenderID { get; set; }

        protected override void ExecuteBlock()
        {
            OffenderID = GetEventsResponse.EventsList.FirstOrDefault(x => x.OffenderID != null).OffenderID;
        }
    }
}
