﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class GetOffenderEventStatusTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderEventStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetOffenderEventStatusRequest GetOffenderEventStatusRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderEventStatusResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetOffenderEventStatusResponse GetOffenderEventStatusResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetOffenderEventStatusResponse = EventsProxy.Instance.GetOffenderEventStatus(GetOffenderEventStatusRequest);
        }
    }

    public class GetOffenderEventStatusTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderEventStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetOffenderEventStatusRequest GetOffenderEventStatusRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderEventStatusResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetOffenderEventStatusResponse GetOffenderEventStatusResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetOffenderEventStatusResponse = EventsProxy_1.Instance.GetOffenderEventStatus(GetOffenderEventStatusRequest);
        }
    }

    public class GetOffenderEventStatusTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOffenderEventStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetOffenderEventStatusRequest GetOffenderEventStatusRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderEventStatusResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetOffenderEventStatusResponse GetOffenderEventStatusResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetOffenderEventStatusResponse = EventsProxy_2.Instance.GetOffenderEventStatus(GetOffenderEventStatusRequest);
        }
    }
}
