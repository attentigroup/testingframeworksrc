﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using System.Linq;

namespace TestBlocksLib.EventsBlocks
{
    public class GetEvents_NewInProcessAutoProcess_TestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponseCopy, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetEventsResponse GetEventsResponseCopy { get; set; }

        protected override void ExecuteBlock()
        {
            GetEventsResponseCopy = new Events0.EntMsgGetEventsResponse();
            GetEventsResponseCopy = GetEventsResponse;
            foreach (var eventItem in GetEventsResponse.EventsList)
            {
                if (!(eventItem.Status == Events0.EnmHandlingStatusType.New) && !(eventItem.Status == Events0.EnmHandlingStatusType.InProcess) && !(eventItem.Status == Events0.EnmHandlingStatusType.AutoProcess))
                    GetEventsResponseCopy.EventsList.ToList().Remove(eventItem);
            }
        }
    }
}
