﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using TestingFramework.Proxies.API.Offenders;
using Config0 = TestingFramework.Proxies.API.Configuration;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;

namespace TestBlocksLib.EventsBlocks
{
    public class GetEventWithOffenderTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.Event, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntEvent EventWithOffender { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int? OffenderId { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int EventId { get; set; }

        Configuration0.EntMsgGetHandlingProcedureRequest handlingProcedureRequest = new Configuration0.EntMsgGetHandlingProcedureRequest();

        protected override void ExecuteBlock()
        {
            if (GetEventsResponse.EventsList.Length < 1)
                throw new Exception("No Events in system!");
            
            EventWithOffender = GetEventsResponse.EventsList.FirstOrDefault(x => x.OffenderID > 0 && GetHandlingProcedure((int)x.OffenderID) != null);
            if(EventWithOffender == null)
                throw new Exception("No Events with offender were found!");

            OffenderId = EventWithOffender.OffenderID;

            EventId = EventWithOffender.ID;
        }
        /// <summary>
        /// This method is used to filter out entries with HandlingProcedure = null
        /// </summary>
        /// <param name="offenderId"></param>
        /// <returns></returns>
        private EntHandlingProcedure GetHandlingProcedure(int offenderId)
        {
            handlingProcedureRequest.OffenderID = offenderId;
            var response = Config0.ConfigurationProxy.Instance.GetHandlingProcedure(handlingProcedureRequest);
            return response.HandlingProcedure;
        }
    }
}
