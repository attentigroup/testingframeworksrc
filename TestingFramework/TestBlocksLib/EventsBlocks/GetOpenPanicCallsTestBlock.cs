﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class GetOpenPanicCallsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOpenPanicCallsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetOpenPanicCallsRequest GetOpenPanicCallsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOpenPanicCallsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetOpenPanicCallsResponse GetOpenPanicCallsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetOpenPanicCallsResponse = EventsProxy.Instance.GetOpenPanicCalls(GetOpenPanicCallsRequest);            
        }
    }

    public class GetOpenPanicCallsTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOpenPanicCallsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetOpenPanicCallsRequest GetOpenPanicCallsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOpenPanicCallsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetOpenPanicCallsResponse GetOpenPanicCallsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetOpenPanicCallsResponse = EventsProxy_1.Instance.GetOpenPanicCalls(GetOpenPanicCallsRequest);
        }
    }

    public class GetOpenPanicCallsTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetOpenPanicCallsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetOpenPanicCallsRequest GetOpenPanicCallsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOpenPanicCallsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetOpenPanicCallsResponse GetOpenPanicCallsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetOpenPanicCallsResponse = EventsProxy_2.Instance.GetOpenPanicCalls(GetOpenPanicCallsRequest);
        }
    }
}
