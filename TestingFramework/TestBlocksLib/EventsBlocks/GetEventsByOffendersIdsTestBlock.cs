﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using TestingFramework.Proxies.API.Events;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Events12_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace TestBlocksLib.EventsBlocks
{
    public class GetEventsByOffendersIdsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.OffendersIdsList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<int> OffendersIdsList { get; set; }

        [PropertyTest(EnumPropertyName.HandlingStatusType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events12_0.EnmHandlingStatusType HandlingStatusType { get; set; }

        [PropertyTest(EnumPropertyName.OffenderId2EventsDictionary, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public DicInt2EventList Offenders2Events { get; set; }

        private Events12_0.EntMsgGetEventsRequest getEventsRequest;

        public GetEventsByOffendersIdsTestBlock()
        {
            Offenders2Events = new DicInt2EventList();
            getEventsRequest = new Events12_0.EntMsgGetEventsRequest();
            getEventsRequest.IncludeHistory = false;
            getEventsRequest.SortField = Events12_0.EnmEventsSortOptions.EventID;
            getEventsRequest.SortDirection = ListSortDirection.Descending;
        }
        protected override void ExecuteBlock()
        {
            int totalEvents = 0;
            getEventsRequest.Status = HandlingStatusType;
            foreach (var offenderId in OffendersIdsList)
            {
                getEventsRequest.OffenderID = offenderId;
                Log.InfoFormat("Start Get events for offender id {0} .(getEventsRequest={1})", offenderId, getEventsRequest.ToLog());
                try
                {
                    Events12_0.EntMsgGetEventsResponse response = EventsProxy.Instance.GetEvents(getEventsRequest);
                    Log.InfoFormat("Get {0} events for offender id {1}.", response.EventsList.Length, offenderId);
                    totalEvents += response.EventsList.Length;
                    Offenders2Events.Add(offenderId, new List<Events12_0.EntEvent>(response.EventsList));
                }
                catch (Exception exception)
                {
                    Log.ErrorFormat("GetEvents for offender {0} failed but block will continue to the next offender. Error: {1}", offenderId, exception.Message);
                }
            }
            Log.InfoFormat("retrieve total {0} events for {1} offenders.", totalEvents, OffendersIdsList.Count);
        }
    }
}
