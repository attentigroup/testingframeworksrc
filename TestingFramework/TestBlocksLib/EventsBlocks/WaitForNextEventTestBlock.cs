﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using WebTests.SeleniumWrapper;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace TestBlocksLib.EventsBlocks
{
    public class WaitForNextEventTestBlock : GetEventsTestBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.Timeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int SecondsTimeOut { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EquipmentSN { get; set; }

        Events0.EntMsgGetEventsResponse CurrentEventsResponse => EventsProxy.Instance.GetEvents(GetEventsRequest);
        protected override void ExecuteBlock()
        {
            //Remark: make sure that GetEventsReques is not limited to 1 entry
            Wait.Until(() => CurrentEventsResponse.EventsList.Any(x => x.ID > GetEventsResponse.EventsList.First().ID && x.DeviceSN == EquipmentSN), $"Event not created after timeout of {SecondsTimeOut} seconds", SecondsTimeOut);
        }
    }
}
