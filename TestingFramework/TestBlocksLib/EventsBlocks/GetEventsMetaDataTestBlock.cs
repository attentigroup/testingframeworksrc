﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace TestBlocksLib.EventsBlocks
{
    public class GetEventsMetaDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetEventsMetadataResponse GetEventsMetadataResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetEventsMetadataResponse = EventsProxy.Instance.GetEventsMetadata();
        }
    }

    public class GetEventsMetaDataTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetEventsMetadataResponse GetEventsMetadataResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetEventsMetadataResponse = EventsProxy_1.Instance.GetEventsMetadata();
        }
    }

    public class GetEventsMetaDataTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetEventsMetadataResponse GetEventsMetadataResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetEventsMetadataResponse = EventsProxy_2.Instance.GetEventsMetadata();
        }
    }
}
