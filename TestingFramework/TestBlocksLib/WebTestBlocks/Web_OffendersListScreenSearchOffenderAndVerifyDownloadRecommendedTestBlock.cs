﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenSearchOffenderAndVerifyDownloadRecommendedTestBlock : Web_TestBlockBase
    {
        // bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.OffenderLabel, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string OffenderLabel { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(FirstName, sectionNumber, Title, EquipmentSerialNumber);
                OffenderLabel = addNewOffenderTestLogic_NoCLick.GetOffenderDetailsLabel();                
            });
        }
    }
}