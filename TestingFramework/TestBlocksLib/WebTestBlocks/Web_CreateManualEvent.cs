﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateManualEvent : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Message { get; set; }

        [PropertyTest(EnumPropertyName.SubType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SubType { get; set; }

        [PropertyTest(EnumPropertyName.HandlingStatusType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HandlingStatus { get; set; }

        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1, "All");
                addNewOffenderTestLogic_NoCLick.CreateManualEvent();

            });
        }
    }
}
