﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndEditPolygonZoneTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsFullSchedule { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.Zone.UpdatePolygonZone(SearchTerm, Name, GraceTime, IsFullSchedule);
            });
        }
    }
}
