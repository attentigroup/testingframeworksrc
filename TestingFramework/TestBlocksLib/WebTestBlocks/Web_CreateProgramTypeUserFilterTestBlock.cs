﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateProgramTypeUserFilterTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.ProgramTypes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string []ProgramTypes { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitle, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string FilterTitleNameThatDisplayInWeb { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitleNameToSet, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FilterTitleNameToSet { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.CreateProgramTypeUserFilter(ProgramTypes, FilterTitleNameToSet);
                FilterTitleNameThatDisplayInWeb =WebTestLogic.MonitorScreenTestLogic.GetFilterTitle();

            });
        }

        public override void CleanUp()
        {
            WebTestLogic.MonitorScreenTestLogic.DeleteUserFilter(FilterTitleNameToSet);
        }
    }
}
