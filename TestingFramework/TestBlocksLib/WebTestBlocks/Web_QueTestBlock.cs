﻿using Common.CustomAttributes;
using Common.E4XtSimulator;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_QueueTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int FilterSectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitle, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FilterTitle { get; set; }

        [PropertyTest(EnumPropertyName.FilterCounter, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<QueueRow> QueueList { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected virtual void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                QueueList = WebTestLogic.SystemQueAndLog.GetQueueRows(FilterSectionNumber, FilterTitle);
            });
        }
    }
}
