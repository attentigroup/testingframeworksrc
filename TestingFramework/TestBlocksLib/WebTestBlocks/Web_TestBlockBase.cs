﻿using Common.CustomAttributes;
using Common.Versioning;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.TestLogicLayer;

#region API refs

#endregion

namespace TestBlocksLib.WebTestBlocks
{
    //*****************************************************************************************************
    //*                 Use the format below in the classes derived from Web_TestBlockBase                *
    //*===================================================================================================*
    //*                                                                                                   *
    //* [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]   *
    //* protected override void ExecuteBlock_Basic()                                                               *
    //* {                                                                                                 *
    //*  Run(() =>                                                                                        *
    //*  {                                                                                                *
    //*   ...//Enter your code here                                                                       *
    //*   WebTestLogic.MyPageObjectLogicLayer // Call the Page Objects via the 'Web Test Logic Layer'     *
    //*   ...                                                                                             *
    //*  });                                                                                              *
    //* }                                                                                                 *
    //                                                                                                    *
    //*****************************************************************************************************

    public abstract class Web_TestBlockBase : TestingBlockBase
    {
        protected WebTestLogicBase WebTestLogic { get; set; }

        private string _webVersion = null;
        public string WebVersion
        {
            get
            {
                if (string.IsNullOrEmpty(_webVersion))
                {
                    AppVersions appVersions;
                    try
                    {
                        _webVersion = APIExtensionsProxy.Instance.GetApplicationVersion();
                    }
                    catch (Exception exception)
                    {//TODO omer: throw exception
                        //use file version
                        Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                        var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                        _webVersion = fvi.FileVersion;
                    }
                }
                return _webVersion;
            }
            private set { }
        }

        public Web_TestBlockBase()
        {
            Init();
        }

        public void Init()
        {
            WebTestLogic = new WebTestLogicBase();
            MapMethodsPerVersionForBlock();
        }

        protected override void ExecuteBlock()
        {
            //get the method of the class order by version
            var rangeFinder = MethodsPerVersionMapping[GetType()];
            //convert to uint
            var uintVersion = MethodVersionAttribute.GetIntVersion(WebVersion);
            var rangeGroup = rangeFinder.Find(uintVersion) as WebRangeGroup;

            if (rangeGroup != null)
            {
                rangeGroup.Method4Version.Invoke(this, null);
            }
            else
            {
                string message = string.Format("Unable to find method for version {0}", WebVersion);
                Log.Error(message);
                throw new Exception(message);
            }
        }

        protected void Run(Action action)
        {
            try
            {
                action();
                WebTests.SeleniumWrapper.Logger.WriteLine($"Block '{GetType().Name}' * Pass *");
            }
            catch (Exception exfail)
            {
                WebTests.SeleniumWrapper.Logger.TakeScreenshot();
                WebTests.SeleniumWrapper.Logger.Fail($"Block '{GetType().Name}' failed: {exfail}");
                throw new WebTestingException($"Web testing exception: See Web log for details; Exception Message: {exfail}");
            }
        }

        #region Versioning support

        private static readonly ConcurrentDictionary<Type, RangeGroupFinder> MethodsPerVersionMapping = new ConcurrentDictionary<Type, RangeGroupFinder>();
        private void MapMethodsPerVersionForBlock()
        {
            //in case we use the same class the properties can't changed in run time so the map need to be done only once
            var currentBlockType = GetType();

            if (MethodsPerVersionMapping.ContainsKey(currentBlockType) == false)
            {
                var eFlags = BindingFlags.Instance | BindingFlags.NonPublic;
                var blockMethods = currentBlockType.GetMethods(eFlags);
                var dictionary = new SortedDictionary<int, PropertyInfo>();
                RangeGroupFinder rgf = new RangeGroupFinder();

                foreach (var method in blockMethods)
                {
                    var testBlockMethodAttrs = Attribute.GetCustomAttributes(method, typeof(MethodVersionAttribute), true);

                    foreach (var methodAttr in testBlockMethodAttrs)
                    {
                        if (methodAttr != null)
                        {
                            if (methodAttr is MethodVersionAttribute)
                            {
                                MethodVersionAttribute mva = methodAttr as MethodVersionAttribute;
                                var group = new WebRangeGroup
                                {
                                    Low = mva.MinVersionInt,
                                    High = mva.MaxVersionInt,
                                    Method4Version = method
                                };

                                rgf.AddGroup(group);
                            }
                        }

                    }
                }

                MethodsPerVersionMapping.TryAdd(currentBlockType, rgf);
            }
        }
        #endregion Versioning support
    }
}
