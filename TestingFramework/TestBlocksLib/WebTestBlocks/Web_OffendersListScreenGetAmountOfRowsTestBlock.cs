﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_OffendersListScreenGetAmountOfRowsTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int AmountOfRows { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                AmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRows());
                AmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRows());
            });
        }
    }
}
