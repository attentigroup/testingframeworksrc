﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_ForgotPasswordTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Email, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Email { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var user = new WebTests.InfraStructure.Entities.User() { UserName = FirstName, Email = Email };
                WebTestLogic.Login.LoginForgotPassword(user);
            });
        }
    }
}
