﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateEventTimeUserFilterTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Time, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EventTime { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.SecondarySearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondarySearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AmountOfRows { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1, "Active Offenders", SecondarySearchTerm);
                addNewOffenderTestLogic_NoCLick.CreateEventTimeUserFilter(EventTime);
            });
        }

        public override void CleanUp()
        {
            //TODO: replace with API cleanup
            WebTestLogic.AddNewOffenderTestLogic_NoClick.DeleteUserFilter("Event Time Filter");
        }
    }
}
