﻿using Common.CustomAttributes;
using Common.Enum;
using TestBlocksLib.WebTestBlocks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.TestingFrameworkBlocks.TestBlocksLib.WebTestBlocks
{
    public class GetVersionTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.String, EnumPropertyModifier.None)]
        public AppVersions ActualVersions { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedVersions, EnumPropertyType.None, EnumPropertyModifier.None)]
        public AppVersions ExpectedVersions { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                ActualVersions = WebTestLogic.GetAppVersionsTestLogic.GetVersions();
            });
        }
    }
}
