﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CurrentStatusSearchEventTestBlock : Web_TestBlockBase
    {
   
        [PropertyTest(EnumPropertyName.SearchField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int SearchField { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.DoNavigateToCurrentStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool DoNavigateToCurrentStatus { get; set; }

        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.None)]
        public CurrentStatusTableRow CurrentStatusTableRow { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]

        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                SearchField = GetEventsResponse.EventsList[0].ID; //Search by event ID from GetEventsResponse
                string OffenderID = GetOffendersResponse.OffendersList[0].RefID;
                 CurrentStatusTableRow = WebTestLogic.AddNewOffenderTestLogic.AddNewOffenderTestLogic.SearchEventByEventId(DoNavigateToCurrentStatus, SearchField, OffenderID, EquipmentSerialNumber);
                
            });
        }
    }
}
