﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenSearchOffenderAndGetTxFromDVPairTestBlock : Web_TestBlockBase
    {
        // bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.SecondarySearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondarySearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.isDVLinkWorks, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool isDVLinkWorks { get; set; }

        [PropertyTest(EnumPropertyName.AggTX, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string AggTX { get; set; }

        [PropertyTest(EnumPropertyName.VicTX, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string VicTX { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, sectionNumber, Title, SecondarySearchTerm);
                AggTX = addNewOffenderTestLogic_NoCLick.GetOffenderTransmitter();
                addNewOffenderTestLogic_NoCLick.VerifyDVLink();
                VicTX = addNewOffenderTestLogic_NoCLick.GetOffenderTransmitter();
            });
        }
    }
}