﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectGroupAndDeleteRecurringTimeframesTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var groupPage = WebTestLogic.GroupPageTestLogic.SelectGroupByName(GroupName);

                var DeleteRecurringTimeframesPopup = groupPage.DeleteRecurringTimeframes();

                DeleteRecurringTimeframesPopup.OK();
            });
        }
    }
}
