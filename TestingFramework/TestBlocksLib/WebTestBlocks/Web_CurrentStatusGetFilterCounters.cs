﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CurrentStatusGetFilterCounters : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FilterName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FilterTitle { get; set; }

        [PropertyTest(EnumPropertyName.FilterCounter, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int FilterCounter { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]

        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                FilterCounter = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetCurrentStatusFilterCounter(FilterTitle);
            });
        }
    }
}
