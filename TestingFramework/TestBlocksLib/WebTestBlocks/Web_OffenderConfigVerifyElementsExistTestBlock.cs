﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_OffenderConfigVerifyElementsExistTestBlock : Web_TestBlockBase
    {
        private const int RESET_RULE_ID = 0;

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.ElementsList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<WebTests.InfraStructure.Entities.ConfigurationElement> ElementsList { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1, "Active Offenders");
                WebTestLogic.AddNewOffenderTestLogic_NoClick.MoveToConfigurationTab();
                WebTestLogic.AddNewOffenderTestLogic_NoClick.UpdateProgramParametersValues(ElementsList);
            });
        }

        public override void CleanUp()
        {
            var resetToPrevioesParams = new Configuration0.EntMsgResetGPSRuleConfigurationRequest();
            resetToPrevioesParams.OffenderID = GetOffenderConfigurationDataRequest.OffenderID;
            resetToPrevioesParams.RuleID = RESET_RULE_ID;
            ConfigurationProxy.Instance.ResetGPSRuleConfiguration(resetToPrevioesParams);
        }
    }
}