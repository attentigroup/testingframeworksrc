﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using WebTests.InfraStructure.Entities;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddNewOffenderExtendedTestBlock : Web_AddNewOffenderTestBlockBase
    {
        [PropertyTest(EnumPropertyName.OfficerName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OfficerName { get; set; }

        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.None)]
        public OffenderDetails RelatedOffenderDetails { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                OffenderDetails.Agency = AgencyName; 
                OffenderDetails.ProgramType = ProgramType;
                if (Receiver != null)
                    OffenderDetails.Receiver = Receiver;
                if (Transmitter != null)
                    OffenderDetails.Tx = Transmitter;
                if (SubType != null)
                    OffenderDetails.SubType = SubType;
                if (RelatedOffenderDetails != null)
                   OffenderDetails.RelatedOffenderRefId = RelatedOffenderDetails.LastName + " " + RelatedOffenderDetails.FirstName + " (" + RelatedOffenderDetails.OffenderRefId + ")";//RelatedOffenderRefId;
                if (OfficerName != null)
                    OffenderDetails.Officer = OfficerName;

                OffenderDetails =  WebTestLogic.AddNewOffenderTestLogic.AddNewOffenderExtended(OffenderDetails);
            });
        }
    }
}
