﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{/// <summary>
/// Test get event id and return linked event list during WEB
/// </summary>
    public class Web_GetLinkedEventTestBlock : Web_TestBlockBase
    {/// <summary>
    /// Id of the event the we need its linked events
    /// </summary>
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int eventID { get; set; }

        /// <summary>
        /// Array that collect all the linked events that belong to the eventID
        /// </summary>
        [PropertyTest(EnumPropertyName.LinkedEventsTableRow, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public MonitorTableRow[] LinkedEventsArr { get; set; }
        [PropertyTest(EnumPropertyName.ArrayLength, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ArrayLength { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.SelectEventAndPressOnLinkedEvents(eventID.ToString());
                LinkedEventsArr = WebTestLogic.MonitorScreenTestLogic.GetLinkedEventsDetailsArr(eventID.ToString(), ArrayLength);

            });
        }
    }
}
