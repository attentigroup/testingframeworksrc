﻿using Common.CustomAttributes;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_LoginDefaultTestBlock : Web_TestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.Login.Login(null);
            });
        }
    }
}
