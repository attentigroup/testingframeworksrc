﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_DeleteGroupTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.GroupPageTestLogic.DeleteGroupByName(GroupName);

            });
        }
    }
}
