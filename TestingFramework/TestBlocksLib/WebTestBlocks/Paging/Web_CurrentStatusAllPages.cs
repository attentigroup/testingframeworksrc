﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks.Paging
{
    public class Web_CurrentStatusAllPages : Web_PagingBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.IsPagingWorks, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsPagingWorks { get; set; }

        protected override void ExecuteBlock_Basic()
        {
            var offenderPageLogic =  WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1);

            IsPagingWorks = offenderPageLogic.ClickOnPagerButtons(); 
        }
    }
}
