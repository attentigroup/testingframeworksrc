﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks.Paging
{
    public class Web_CurrentStatusPaging : Web_PagingBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<ValueTuple<int, int>> PagerAmountOfDisplayedRowInPageList { get; set; }

        protected override void ExecuteBlock_Basic()
        {
            var offenderPageLogic =  WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1);

            PagerAmountOfDisplayedRowInPageList = offenderPageLogic.GetShownResultsPerPage(ShowResultsNumber);
        }
    }
}
