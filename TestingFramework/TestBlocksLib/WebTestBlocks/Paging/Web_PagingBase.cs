﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks.Paging
{
    public abstract class Web_PagingBase : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.ShowResults, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ShowResultsNumber { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<ValueTuple<int, int>> PagerAmountOfDisplayedRowInPageList { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected abstract void ExecuteBlock_Basic();                                                          
         
    }
}
