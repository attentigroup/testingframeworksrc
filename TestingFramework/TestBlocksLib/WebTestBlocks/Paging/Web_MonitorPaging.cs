﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks.Paging
{
    public class Web_MonitorPaging : Web_PagingBase
    {
        [PropertyTest(EnumPropertyName.IsPagingWorks, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool isPagingWorks { get; set; }

        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                isPagingWorks = WebTestLogic.MonitorScreenTestLogic.PagingAllPages();
            });
        }
    }
}
