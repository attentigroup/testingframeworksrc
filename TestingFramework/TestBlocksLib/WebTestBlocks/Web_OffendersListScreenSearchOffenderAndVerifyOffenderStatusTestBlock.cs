﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenSearchOffenderAndVerifyOffenderStatusTestBlock : Web_TestBlockBase
    {
        // bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.SecondarySearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondarySearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.OffenderStatus, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string OffenderStatus { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, sectionNumber, Title, SecondarySearchTerm);
                OffenderStatus = addNewOffenderTestLogic_NoCLick.GetOffenderStatus();           
            });
        }
    }
}