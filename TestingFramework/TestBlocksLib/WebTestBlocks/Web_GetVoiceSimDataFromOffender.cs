﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetVoiceSimDataFromOffender : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string offenderRefId { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(offenderRefId, sectionNumber, Title);
                addNewOffenderTestLogic_NoCLick.GetVoiceSimData();
            });
        }
    }
}
