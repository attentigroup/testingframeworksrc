﻿using Common.CustomAttributes;
using TestBlocksLib.WebTestBlocks;

namespace TestBlocksLib.TestingFrameworkBlocks.TestBlocksLib.WebTestBlocks
{
    public class Web_GetOffendersList : Web_TestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.ShowList();
            });
        }
    }
}