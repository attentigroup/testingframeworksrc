﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.WebAmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int AmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var eventID = EventID.ToString();
                var addNewOffender = WebTestLogic.MonitorScreenTestLogic.NavigateToOffenderThroughEventRow(sectionNumber, Title, eventID);
                AmountOfRows = int.Parse(WebTestLogic.AddNewOffenderTestLogic.GetAmountOfRowsForSpecificSystemFilter(sectionNumber, Title));
            });
        }
    }
}
