﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.ActualTitle, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string ActualTitle { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.ClickOnFilter(sectionNumber, Title);
                ActualTitle = WebTestLogic.OffendersListTestLogic.GetFilterTitle();
            });
        }
    }
}