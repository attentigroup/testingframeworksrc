﻿using Common.CustomAttributes;
using Common.Enum;
using System.Configuration;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_LoginTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.UserName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string UserName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.NewPassword, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string NewPassword { get; set; }
        
        public Web_LoginTestBlock()
        {
            if (UserName == null)
                UserName = ConfigurationManager.AppSettings["WebUserName"];

            if (Password == null)
                Password = ConfigurationManager.AppSettings["WebPassword"];
        }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var user = new WebTests.InfraStructure.Entities.User() { UserName = UserName, Password = Password };
                WebTestLogic.Login.Login(user);
            });
        }
    }
}
