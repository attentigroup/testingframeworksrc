﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_RefreshPageBlock : Web_TestBlockBase
    {
        protected override void ExecuteBlock()
        {
            Run(() =>
            {
                WebTestLogic.GeneralTestLogic.RefreshPage();
             });
        }
    }
}
