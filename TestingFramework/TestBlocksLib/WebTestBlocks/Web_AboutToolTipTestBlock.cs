﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AboutToolTipTestBlock : Web_TestBlockBase
    {

        //TODO : Oren To change the name of the test block 

        [PropertyTest(EnumPropertyName.IsElementVisible, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsEelementVisible { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsEelementVisible = WebTestLogic.UserMenuTestLogic.IsChangePasswordVisible();
            });
        }
    }
}
