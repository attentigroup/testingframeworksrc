﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetReadOnlyHideDataSecurityResultsForGroupDetailsTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.securityList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntSecurity> securityList { get; set; }

        [PropertyTest(EnumPropertyName.RefineNewRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntSecurity> RefineNewRequest { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.isElementShown, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<SecurityConfigObject> isElementShown { get; set; }

        //
        [PropertyTest(EnumPropertyName.ModifyOption, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public WebTests.Common.EnmModifyOption ModifyOption { get; set; }

        //

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                isElementShown = new List<SecurityConfigObject>();
                WebTestLogic.GroupsListTestLogic.SelectGroup();
                for (int i = 0; i < RefineNewRequest.Count; i++)
                {
                    string[] sys_confige = RefineNewRequest[i].ResourceID.Split('_');
                    string Type = sys_confige[2];
                    string Title = sys_confige[3];

                    var elementResult = WebTestLogic.GroupPageTestLogic.GetReadOnlyHideDataSecurityResult(Type, Title, ModifyOption);
                    elementResult.ResourceId = RefineNewRequest[i].ResourceID;
                    isElementShown.Add(elementResult);
                }

            });
        }
    }
}
