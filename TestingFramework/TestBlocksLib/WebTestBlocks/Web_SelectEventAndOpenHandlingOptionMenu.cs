﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectEventAndOpenHandlingOptionMenu : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int eventID { get; set; }

        [PropertyTest(EnumPropertyName.isElementShown, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<SecurityConfigObject> isElementShown { get; set; }

        [PropertyTest(EnumPropertyName.RefineNewRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntSecurity> RefineNewRequest { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                isElementShown = new List<SecurityConfigObject>();

                WebTestLogic.MonitorScreenTestLogic.SelectEventAndExpandTableRow(eventID.ToString());

                for (int i = 0; i < RefineNewRequest.Count; i++)
                {
                    var elementResult = WebTestLogic.MonitorScreenTestLogic.GetSecurityResultFromEventExpandConteiner(RefineNewRequest[i].ResourceID);
                    elementResult.ResourceId = RefineNewRequest[i].ResourceID;
                    isElementShown.Add(elementResult);
                }


            });
        }
    }
}
