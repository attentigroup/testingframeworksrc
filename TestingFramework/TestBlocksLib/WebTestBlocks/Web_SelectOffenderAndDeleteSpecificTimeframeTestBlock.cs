﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TimeFrameID { get; set; }

        [PropertyTest(EnumPropertyName.IsE4WeeklyTimeframe, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsE4WeeklyTimeframe { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {

                WebTestLogic.Schedule.DeleteTimeFrame(SearchTerm, TimeFrameID.ToString(), IsE4WeeklyTimeframe);
            });
        }
    }
}
