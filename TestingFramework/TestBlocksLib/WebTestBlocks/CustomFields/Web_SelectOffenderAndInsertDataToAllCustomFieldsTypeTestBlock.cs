﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Factories;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndInsertDataToAllCustomFieldsTypeTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(SearchTerm);
                foreach (var customField in CustomFields)
                {
                    var resourceID = customField.ResourceID;

                    switch (customField.Type)
                    {
                        case EnmFieldType.Numeric:
                            var numericGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
                            var number = numericGenerator.GenerateData(0, 100);
                            WebTestLogic.AddNewOffenderTestLogic_NoClick.InsertDataToNumericCustomField(resourceID, number.ToString());
                            break;
                        case EnmFieldType.String:
                            var stringGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.String) as StringGenerator;
                            var textFieldStr = stringGenerator.GenerateRandomStringWithLength(10);
                            WebTestLogic.AddNewOffenderTestLogic_NoClick.InsertDataToTextCustomField(resourceID, textFieldStr);
                            break;
                        case EnmFieldType.Date:
                            WebTestLogic.AddNewOffenderTestLogic_NoClick.InsertDataToDateTimeCustomField(resourceID, "");
                            break;
                        case EnmFieldType.Predefiend:
                            var customFieldListItemOfTableID = InformationProxy.Instance.GetPredefinedListsFromSysTable(new List<string>() { customField.TableID });
                            var listLength = customFieldListItemOfTableID.Count();
                            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
                            var index = intGenerator.GenerateData(1, listLength - 1);
                            WebTestLogic.AddNewOffenderTestLogic_NoClick.InsertDataToListCustomField(resourceID, index.ToString());
                            break;
                        default:
                            throw new Exception($"No matching type found for ResourceID: {resourceID}!");
                    }

                    WebTestLogic.AddNewOffenderTestLogic_NoClick.SaveForUpdateOffender();
                }

                

            });
        }
    }
}
