﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Factories;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddCustomFieldsListValuesTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.TableIdName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableIdName { get; set; }

        [PropertyTest(EnumPropertyName.ListLength, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Length { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<string> CustomFieldsListValues { get; set; }

        [PropertyTest(EnumPropertyName.TableID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableID { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                CustomFieldsListValues = new List<string>();

                for (int i = 0; i< Length; i++)
                {
                    var stringGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.String) as StringGenerator;
                    var value = stringGenerator.GenerateRandomStringWithLength(10);
                    CustomFieldsListValues.Add(value);
                }

                WebTestLogic.SystemTablesTestLogic.AddCustomFieldsListValues(TableIdName, CustomFieldsListValues);
            });
        }

        public override void CleanUp()
        {
            var CustomFields = InformationProxy.Instance.GetPredefinedListsFromSysTable(new List<string>() { TableID });

            var InputCustomFieldsDictionary = new Dictionary<EnmDBAction, List<EntSysTableItem>>();

            InputCustomFieldsDictionary.Add(EnmDBAction.I, null);
            InputCustomFieldsDictionary.Add(EnmDBAction.U, null);
            InputCustomFieldsDictionary.Add(EnmDBAction.D, null);

            var CustomFieldsNew = new List<EntSysTableItem>();

            foreach (var value in CustomFieldsListValues)
            {
                var customFieldValue = CustomFields.First(x => x.ValueDescription == value);
                var sysTableItem = new EntSysTableItem() { TableID = customFieldValue.TableID, ValueCode = customFieldValue.ValueCode, LanguageCode = customFieldValue.Language, Description = customFieldValue.ValueDescription };
                CustomFieldsNew.Add(sysTableItem);
            }

            InputCustomFieldsDictionary[EnmDBAction.D] = CustomFieldsNew;

            APIResourcesProxy.Instance.UpdateEmsSystable(InputCustomFieldsDictionary);


        }
    }
}
