﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;

namespace TestBlocksLib.WebTestBlocks.CustomFields
{
    public class Web_SelectOffenderAndVerifyCustomFieldDataDisplayedInOffendersListTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsVisibility, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Dictionary<int, bool> CustomFieldsVisibility { get; set; }

        [PropertyTest(EnumPropertyName.GetAllResourcesData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntResource> GetAllResourcesData { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                CustomFieldsVisibility = new Dictionary<int, bool>();
                WebTestLogic.OffendersListTestLogic.SearchOffender(SearchTerm);
                foreach (var customField in CustomFields)
                {
                    var resource = GetAllResourcesData.FindAll(x => x.DynamicFieldID == customField.ID);
                    var offendersListResource = resource.First(x => x.ResourceID.StartsWith("OffendersList_"));
                    var isVisible = WebTestLogic.OffendersListTestLogic.IsCustomFieldDataDisplayed(offendersListResource.ResourceID, SearchTerm);
                    CustomFieldsVisibility.Add(customField.ID, isVisible);
                }

            });
        }
    }
}
