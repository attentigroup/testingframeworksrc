﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks.CustomFields
{
    public class Web_CheckCustomFieldListValueIsNotDeletableTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ValueDescription { get; set; }

        [PropertyTest(EnumPropertyName.IsNotDeletable, EnumPropertyType.String, EnumPropertyModifier.None)]
        public bool IsNotDeletable { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsNotDeletable = WebTestLogic.SystemTablesTestLogic.CheckIfItsPossibleToDeleteCustomFieldListValueUsedByOffender(ValueDescription);
            });
        }
    }
}
