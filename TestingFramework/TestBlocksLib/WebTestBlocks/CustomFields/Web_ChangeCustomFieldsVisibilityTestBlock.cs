﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_ChangeCustomFieldsVisibilityTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTypeLst { get; set; }

        [PropertyTest(EnumPropertyName.ShowInOffenderDetails, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowInOffenderDetails { get; set; }

        [PropertyTest(EnumPropertyName.ShowInOffendersList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowInOffendersList { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var customFieldsNamesList = new List<string>();

                foreach(var field in CustomFieldsByTypeLst)
                {
                    customFieldsNamesList.Add(field.Name);
                }

                WebTestLogic.SystemTablesTestLogic.ChangeCustomFieldsVisibility(customFieldsNamesList, ShowInOffenderDetails, ShowInOffendersList);
            });
        }

        public override void CleanUp()
        {
            
        }
    }
}
