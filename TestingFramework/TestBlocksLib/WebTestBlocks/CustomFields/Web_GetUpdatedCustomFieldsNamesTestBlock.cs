﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.WebTestBlocks.CustomFields
{
    public class Web_GetUpdatedCustomFieldsNamesTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTypeLst { get; set; }

        [PropertyTest(EnumPropertyName.IsUpdatedCustomFieldNameExist, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Dictionary<string, bool> IsUpdatedCustomFieldNameExistDictionary { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var customFieldsNamesList = new List<string>();

                foreach (var field in CustomFieldsByTypeLst)
                {
                    customFieldsNamesList.Add(field.Name);
                }

                IsUpdatedCustomFieldNameExistDictionary = WebTestLogic.SystemTablesTestLogic.GetUpdatedCustomFieldsNames(customFieldsNamesList);
            });
        }

        public override void CleanUp()
        {

        }
    }
}
