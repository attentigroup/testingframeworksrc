﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Information;
using TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_UpdateCustomFieldsNamesTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTypeLst { get; set; }

        private Dictionary<string, string> CustomFieldsNames = new Dictionary<string, string>();

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                CustomFieldsNames = new Dictionary<string, string>();

                foreach (var field in CustomFieldsByTypeLst)
                {
                    CustomFieldsNames.Add(field.Name, field.FieldType);
                }

                WebTestLogic.SystemTablesTestLogic.UpdateCustomFieldsNames(CustomFieldsNames);
            });
        }

        public override void CleanUp()
        {
            int i = 0;
            foreach (var customField in CustomFieldsByTypeLst)
            {
                customField.Name = CustomFieldsNames.ElementAt(i).Key;
                i++;
            }
            InformationProxy.Instance.UpdateCustomFieldsSystemDefinitions(CustomFieldsByTypeLst);
        }
    }
}




