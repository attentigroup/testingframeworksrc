﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;

namespace TestBlocksLib.WebTestBlocks.CustomFields
{
    public class Web_SelectOffenderAndGetCustomFieldsListValuesVisibilityTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.IsElementVisible, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsVisible { get; set; }

        [PropertyTest(EnumPropertyName.EmsSystableDictionary, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Dictionary<EnmDBAction, List<EntSysTableItem>> InputCustomFieldsDictionary { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsListValuesVisibility, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Dictionary<string, bool> CustomFieldsListValuesVisibility { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<string> CustomFieldsListValues { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                //var valuesLst = new List<string>();
                //var sysTableLst = InputCustomFieldsDictionary[EnmDBAction.I];
                //foreach(var sysTableItem in sysTableLst)
                //{
                //    valuesLst.Add(sysTableItem.Description);
                //}
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(SearchTerm);
                EntCustomFieldSystemDefinition customField = null;
                if (CustomFields.Any())
                {
                    customField = CustomFields.First();
                }
                else
                    throw new Exception("Custom fields list is null!");
                IsVisible = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetCustomFieldsVisibilityFromOffenderDetails(customField.ResourceID);
                CustomFieldsListValuesVisibility = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetCustomFieldsListValuesVisibilityFromOffenderDetails(customField.ResourceID, CustomFieldsListValues);

            });
        }
    }
}
