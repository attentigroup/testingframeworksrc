﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_UpdateCustomFieldListValueTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.TableIdName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableIdName { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ValueDescription { get; set; }

        [PropertyTest(EnumPropertyName.UpdatedDescription, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string UpdatedValueDescription { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {

                WebTestLogic.SystemTablesTestLogic.UpdateCustomFieldsListValues(TableIdName, ValueDescription, UpdatedValueDescription);
            });
        }
    }
}
