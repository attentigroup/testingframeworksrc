﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndGetCustomFieldsVisibilityFromOffenderDetailsTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsVisibility, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Dictionary<int, bool> CustomFieldsVisibility { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                CustomFieldsVisibility = new Dictionary<int, bool>();
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(SearchTerm);
                foreach(var customField in CustomFields)
                {
                    var isVisible = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetCustomFieldsVisibilityFromOffenderDetails(customField.ResourceID);
                    CustomFieldsVisibility.Add(customField.ID, isVisible);
                }
                
            });
        }
    }
}
