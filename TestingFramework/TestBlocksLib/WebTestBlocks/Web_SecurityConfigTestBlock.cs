﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SecurityConfigResults, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<List<SecurityConfigObject>> SecurityConfigResults { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                SecurityConfigResults = WebTestLogic.SecurityConfigLogic_Test.SecurityConfigTest();
            });
        }
    }
}
