﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectGroupAndAddScheduleTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrame, EnumPropertyType.None, EnumPropertyModifier.None)]
        public TimeFrame TimeFrame { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderId { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.StartDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string StartDateText { get; set; }

        [PropertyTest(EnumPropertyName.EndDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EndDateText { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string StartTimeText { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EndTimeText { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.Location, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Location { get; set; }

        [PropertyTest(EnumPropertyName.RecurseEveryWeek, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool RecurseEveryWeek { get; set; }

        [PropertyTest(EnumPropertyName.RecurseEveryWeek, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] CopyWithinWeek { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        public Web_SelectGroupAndAddScheduleTestBlock()
        {
            TimeFrame = new TimeFrame();
        }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var groupPage = WebTestLogic.GroupPageTestLogic.SelectGroupByName(GroupName);


                bool isStartDate = DateTime.TryParse(StartDateText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime Sresult);
                if (isStartDate)
                    StartDate = Sresult;

                bool isEndDate = DateTime.TryParse(EndDateText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime Eresult);
                if (isStartDate)
                    EndDate = Eresult;

                TimeFrame.StartDate = StartDateText;
                TimeFrame.StartTime = StartTimeText;
                TimeFrame.EndDate = EndDateText;
                TimeFrame.EndTime = EndTimeText;
                TimeFrame.Type = Type;
                TimeFrame.Location = Location;
                TimeFrame.RecurseEveryWeek = RecurseEveryWeek.ToString();
                TimeFrame.Limitation = Limitation;

                groupPage.AddSchedule(TimeFrame);
            });
        }
    }
}
