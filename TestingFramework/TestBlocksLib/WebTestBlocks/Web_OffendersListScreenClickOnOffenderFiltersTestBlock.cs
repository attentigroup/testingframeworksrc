﻿using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenClickOnOffenderFiltersTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int AmountOfRows { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                AmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRowsByFilter(sectionNumber, Title));
            });
        }
    }
}
