﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderByCreateDateTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstSearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FirstSearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.SecondSearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondSearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.OffenderDateCreated, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? DateCreated { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitle, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string filterTitle { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilterAndDateCreated(FirstSearchTerm, 1, DateCreated, filterTitle);
            });
        }
    }
}
