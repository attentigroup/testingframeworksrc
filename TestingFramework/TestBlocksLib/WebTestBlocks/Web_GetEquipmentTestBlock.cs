﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetEquipmentTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EquipmentSN { get; set; }

        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment Equipment { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                Equipment = WebTestLogic.EquipmentManagementTestLogic.GetEquipmentDetailsFromTable(EquipmentSN);
            });
        }
    }
}
