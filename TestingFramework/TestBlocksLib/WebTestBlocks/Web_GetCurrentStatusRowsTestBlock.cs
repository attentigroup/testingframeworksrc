﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.WebTestBlocks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib
{
    public class Web_GetCurrentStatusRowsTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public CurrentStatusTableRow[] EventsArr { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                EventsArr = WebTestLogic.OffendersListTestLogic.GetCurrentStatusRowsArray();
            });
        }
    }
}
