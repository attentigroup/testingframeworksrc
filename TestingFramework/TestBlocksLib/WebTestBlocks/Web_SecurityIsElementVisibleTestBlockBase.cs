﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public abstract class Web_SecurityIsElementVisibleTestBlockBase : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.IsElementVisible, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsElementVisible { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Password { get; set; }
    }
}
