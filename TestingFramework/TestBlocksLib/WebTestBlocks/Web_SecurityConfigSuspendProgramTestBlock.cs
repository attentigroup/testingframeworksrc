﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigSuspendProgramTestBlock : Web_SecurityConfigFastTestBlock
    {
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsElementShown = new List<SecurityConfigObject>();
                var result = WebTestLogic.AddNewOffenderTestLogic.GetSecurityResultSuspendProgram();
                IsElementShown.Add(result);
            });
        }
}
}
