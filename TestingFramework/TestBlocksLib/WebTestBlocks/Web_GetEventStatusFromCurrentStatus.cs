﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetEventStatusFromCurrentStatus : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.EventStatus, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string EventStatus { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string eventID { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                EventStatus = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetEventStatus(eventID);
            });
        }
    }
}
