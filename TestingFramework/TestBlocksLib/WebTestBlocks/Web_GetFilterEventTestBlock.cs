﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetFilterEventTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Date, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string[] DatesArr { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
               DatesArr =  WebTestLogic.MonitorScreenTestLogic.GetFilterEventsArray();
            });
        }
    }
}
