﻿using Common.CustomAttributes;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_InfoToolTipTestBlock : Web_SecurityIsElementVisibleTestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsElementVisible = WebTestLogic.AboutTooltipTestLogic.IsAboutVisible();
            });
        }
    }
}
