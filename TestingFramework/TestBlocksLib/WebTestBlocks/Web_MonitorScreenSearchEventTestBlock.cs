﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_MonitorScreenSearchEventTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.SearchField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int SearchField { get; set; }

        [PropertyTest(EnumPropertyName.EventsList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public MonitorTableRow MonitorTableRow { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                SearchField = GetEventsResponse.EventsList[0].ID;
                MonitorTableRow = WebTestLogic.MonitorScreenTestLogic.SearchEventByEventId(SearchField);
            });
        }
    }
}
