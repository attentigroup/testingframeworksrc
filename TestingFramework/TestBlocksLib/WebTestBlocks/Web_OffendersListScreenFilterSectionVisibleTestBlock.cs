﻿using Common.CustomAttributes;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenFilterSectionVisibleTestBlock : Web_SecurityIsElementVisibleTestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var user = new WebTests.InfraStructure.Entities.User() { UserName = FirstName, Password = Password };
                IsElementVisible = WebTestLogic.OffendersListTestLogic.IsFilterSectionVisible(user);
            });
        }
    }
}
