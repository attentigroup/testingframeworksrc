﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using WebTests.InfraStructure.Entities;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public abstract class Web_AddNewOffenderTestBlockBase : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Id { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.AgencyName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AgencyName { get; set; }

        [PropertyTest(EnumPropertyName.SubType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SubType { get; set; }


        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.None)]
        public OffenderDetails RelatedOffenderDetails { get; set; }


        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public OffenderDetails OffenderDetails { get; set; }

        public Web_AddNewOffenderTestBlockBase()
        {
            OffenderDetails = new OffenderDetails();

        }
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected abstract void ExecuteBlock_Basic();
        

        public override void CleanUp()
        {
            var getOffenderListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            getOffenderListRequest.OffenderRefID = new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = OffenderDetails.OffenderRefId };

            var entGetOffender = OffendersProxy.Instance.GetOffenders(getOffenderListRequest);

            //build the delete offender request
            var deleteOffenderRequest = new Offenders0.EntMsgDeleteOffenderRequest();

            deleteOffenderRequest.OffenderID = entGetOffender.OffendersList[0].ID;
            deleteOffenderRequest.Timestamp = entGetOffender.OffendersList[0].Timestamp;
            // send the delete offender request
            OffendersProxy.Instance.DeleteOffender(deleteOffenderRequest);

        }
    }
}
