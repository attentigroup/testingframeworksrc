﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using WebTests.InfraStructure.Entities;

using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddSeveralEquipmentTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FileName, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string FileName { get; set; }

        [PropertyTest(EnumPropertyName.Receivers, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string [] receiver { get; set; }

        [PropertyTest(EnumPropertyName.Transmitters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string [] Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.AgencyName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AgencyName { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment.EntMsgAddEquipmentResponse AddEquipmentResponse { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.EquipmentManagementTestLogic.AddEquipments(FileName, receiver, Transmitter);
            });
        }

        public override void CleanUp()
        {
            foreach (var rec in receiver)
            {
                DeleteEquipment(rec);
            }
            foreach(var tran in Transmitter)
            {
                DeleteEquipment(tran);
            }
            
        }

        private void DeleteEquipment(string equipmentSN)
        {
            var getEquipment = EquipmentProxy.Instance.GetEquipmentList(new EntMsgGetEquipmentListRequest()
            {
                SerialNumber = equipmentSN,
            });
            if (getEquipment.EquipmentList[0].AgencyID != -1)
            {
                EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest = new EntMsgUpdateEquipmentRequest()
                { EquipmentID = getEquipment.EquipmentList[0].ID, AgencyID = null };
                EquipmentProxy.Instance.UpdateEquipment(entMsgUpdateEquipmentRequest);
            }

            var getCellularData = EquipmentProxy.Instance.GetCellularData(new EntMsgGetCellularDataRequest()
            {
                SerialNumber = equipmentSN,
            });
            if (getCellularData.CellularData.Count() > 0)
            {
                var deleteCellularDataRequest = new EntMsgDeleteCellularDataRequest();
                deleteCellularDataRequest.EquipmentID = getEquipment.EquipmentList[0].ID;
                EquipmentProxy.Instance.DeleteCellularData(deleteCellularDataRequest);
            }

            //create equipment delete request
            Thread.Sleep(1500);
           
            var deleteRequest = new EntMsgDeleteEquipmentRequest();
            deleteRequest.EquipmentID = getEquipment.EquipmentList[0].ID;
            try
            {
                EquipmentProxy.Instance.DeleteEquipment(deleteRequest);
            }
            catch (Exception e)
            {
                if (e.Message.Contains("#SY"))
                {
                    Thread.Sleep(30000);
                    EquipmentProxy.Instance.DeleteEquipment(deleteRequest);
                }
                else
                {
                    throw new Exception(e + " the CleanUp for AddEquipment has failed!");
                }
            }
        }  
    }
}
