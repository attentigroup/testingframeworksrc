﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffenderConfigTabSelectorAnyOffenderTestBlock : Web_TestBlockBase
    {

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectFirstOffender();
                WebTestLogic.AddNewOffenderTestLogic_NoClick.MoveToConfigurationTab();
            });
        }
    }
}