﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_NavigateToGroupsListAddNewGroupTestBlock : Web_TestBlockBase
    {
        /// <summary>
        /// GroupType options are: Tracker or RF Curfew(E4) Family
        /// </summary>
        [PropertyTest(EnumPropertyName.GroupType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GroupTypeEnum GroupType { get; set; }

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.GroupDescription, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string GroupDescription { get; set; }

        [PropertyTest(EnumPropertyName.NumOfOffenders, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int NumOfOffenders { get; set; }

        public Web_NavigateToGroupsListAddNewGroupTestBlock()
        {
            NumOfOffenders = 1;
        }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.GroupPageTestLogic.AddNewGroup(GroupType, GroupName, GroupDescription, NumOfOffenders);
            });
        }
    }
}
