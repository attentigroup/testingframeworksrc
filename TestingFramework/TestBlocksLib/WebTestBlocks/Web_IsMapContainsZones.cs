﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_IsMapContainsZones : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.IsElementExist, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsElementExist { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsElementExist = WebTestLogic.Zone.IsMapContainsZones();
            });
        }
    }
}
