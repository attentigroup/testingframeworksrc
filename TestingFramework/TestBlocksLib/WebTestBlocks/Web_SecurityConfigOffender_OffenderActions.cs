﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigOffender_OffenderActionsTestBlock : Web_SecurityConfigOffender_ActionsTestBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(GetOffendersResponse.OffendersList.First().RefID, 1, filterTitle: "Active Offenders");

                WebTestLogic.AddNewOffenderTestLogic.SecurityConfigOffender_OffenderActions();
            });
        }
    }
}
