﻿using Common.CustomAttributes;
using System;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_DVPairsVisibleTestBlock : Web_SecurityIsElementVisibleTestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsElementVisible = WebTestLogic.UserMenuTestLogic.IsDVPairsVisible();
            });
        }
    }
}
