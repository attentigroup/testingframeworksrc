﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_FilterOffenderEventsByRangeOfDates : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime startTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime endTime { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.SecondarySearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondarySearchTerm { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1, "All", SecondarySearchTerm);
                addNewOffenderTestLogic_NoCLick.FilterCurrentStatusEventsByRangeOfDates(startTime, endTime);

            });
        }
    }
}
