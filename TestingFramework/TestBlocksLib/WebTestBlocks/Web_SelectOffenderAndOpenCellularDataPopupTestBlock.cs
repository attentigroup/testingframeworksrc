﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndOpenCellularDataPopupTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.PrimaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PrimaryTabLocatorString { get; set; }

        [PropertyTest(EnumPropertyName.SecondaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondaryTabLocatorString { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1);
                addNewOffenderTestLogic_NoCLick.OpenCellularDataPopup(PrimaryTabLocatorString, SecondaryTabLocatorString);
            });
        }
    }
}
