﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_FilterByTimeAndSelectEvent : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int eventID { get; set; }

        [PropertyTest(EnumPropertyName.Recent, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Recent { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.FilterByTimeSelectEventAndExpand(eventID.ToString(), Recent);
            });
        }
    }
}
