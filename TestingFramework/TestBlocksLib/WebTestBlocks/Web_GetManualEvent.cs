﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetManualEvent : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.None)]
        public CurrentStatusTableRow CurrentStatusTableRow { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                CurrentStatusTableRow = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetManualEvent();

            });
        }
    }
}
