﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetSystemFiltersMonitorScreenTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.SystemFilters, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string[] SystemFilters { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                SystemFilters = WebTestLogic.MonitorScreenTestLogic.GetSystemFilters();
            });
        }
    }
}
