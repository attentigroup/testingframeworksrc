﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TestBlocksLib.WebTestBlocks 
{
   public class Web_SecurityConfigUser_Disable : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.AccountStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string AccountStatus { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {

                WebTestLogic.UsersListTestLogic.SelectUserByTerm(AccountStatus);

            });
        }

    }
}