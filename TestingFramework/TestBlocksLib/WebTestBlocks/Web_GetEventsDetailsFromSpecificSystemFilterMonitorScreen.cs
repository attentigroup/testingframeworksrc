﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.ListLength, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventsDetailsArrLength { get; set; }

        [PropertyTest(EnumPropertyName.MonitorTableRowArray, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public MonitorTableRow[] MonitorTableRowArr { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                MonitorTableRowArr = WebTestLogic.MonitorScreenTestLogic.GetEventsDetailsArr(sectionNumber, Title, EventsDetailsArrLength);

            });
        }
    }
}
