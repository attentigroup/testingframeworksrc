﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string message { get; set; }

        [PropertyTest(EnumPropertyName.Prepration, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool Prepration { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.SelectEventByMessageAndOpenHandlingOptionMenu(message, Prepration);
            });
        }
    }
}
