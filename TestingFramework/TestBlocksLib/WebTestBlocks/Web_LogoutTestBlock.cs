﻿using Common.CustomAttributes;
using Common.Enum;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_LogoutTestBlock : Web_TestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.UserMenuTestLogic.Logout();
            });
        }
    }
}
