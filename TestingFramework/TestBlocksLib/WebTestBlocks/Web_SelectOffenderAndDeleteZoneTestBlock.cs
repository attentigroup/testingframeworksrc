﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndDeleteZoneTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(SearchTerm);
                WebTestLogic.AddNewOffenderTestLogic_NoClick.DeleteZone(ZoneID.ToString());
            });
        }
    }
}
