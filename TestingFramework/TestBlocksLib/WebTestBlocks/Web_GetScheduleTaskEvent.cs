﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_GetScheduleTaskEvent : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.MonitorTableRow, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public MonitorTableRow MonitorTableRow { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime time { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSN { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                MonitorTableRow = WebTestLogic.MonitorScreenTestLogic.GetScheduleTaskEvent(time, ReceiverSN, false);
            });
        }
    }
}
