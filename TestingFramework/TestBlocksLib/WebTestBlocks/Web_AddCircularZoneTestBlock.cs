﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.Zones;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddCircularZoneTestBlock : Web_TestBlockBase
    {


        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefId { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.ZoneName, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsFullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RealRadious { get; set; }

        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int BufferRadious { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            WebTestLogic.Zone.AddCircularZone(SearchTerm, Name, Limitation, GraceTime, IsFullSchedule, RealRadious, BufferRadious);
        }

        public override void CleanUp()
        {
            var getOffender = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
            {
                OffenderRefID = new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = OffenderRefId }
            });
            var getZone = ZonesProxy.Instance.GetZonesByEntityID(new Zones0.MsgGetZonesByEntityIDRequest()
            {
                EntityID = getOffender.OffendersList[0].ID
            });
            var zone = getZone.ZoneList.FirstOrDefault(x => x.Name == Name);
            var deleteZone = new Zones0.MsgDeleteZoneRequest();
            deleteZone.EntityID = zone.EntityID;
            deleteZone.EntityType = zone.EntityType;
            deleteZone.ZoneID = zone.ID;
            ZonesProxy.Instance.DeleteZone(deleteZone);
        }
    }
}
