﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateOfficerNameUserFilterTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string OfficerName { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitle, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string FilterTitleNameThatDisplayInWeb { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitleNameToSet, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FilterTitleNameToSet { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.CreateOfficerNameUserFilter(OfficerName, FilterTitleNameToSet);
                FilterTitleNameThatDisplayInWeb = WebTestLogic.MonitorScreenTestLogic.GetFilterTitle();
 
            });
        }

        public override void CleanUp()
        {
            WebTestLogic.MonitorScreenTestLogic.DeleteUserFilter(FilterTitleNameToSet);

        }
    }
}
