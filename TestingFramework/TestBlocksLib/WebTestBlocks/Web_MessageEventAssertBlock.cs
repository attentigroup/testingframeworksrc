﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_MessageEventAssertBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.None)]
        public CurrentStatusTableRow CurrentStatusTableRow { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Expected { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {                
                string msg = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetEventMessage(Expected);

                Assert.AreEqual(msg, Expected);
            });
        }
    }
}
