﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.TestLogicLayer;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_NavigateToTabTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.PrimaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PrimaryTabLocatorString { get; set; }

        [PropertyTest(EnumPropertyName.SecondaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondaryTabLocatorString { get; set; }
        

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                new AddNewOffenderTestLogic(false).NavigateToTab(PrimaryTabLocatorString, SecondaryTabLocatorString);
            });
        }
    }
}
