﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.Schedule;
using WebTests.InfraStructure.Entities;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddTimeframeTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.TimeFrame, EnumPropertyType.None, EnumPropertyModifier.None)]
        public TimeFrame TimeFrame { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderId { get; set; }

        [PropertyTest(EnumPropertyName.StartDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string StartDateText { get; set; }

        [PropertyTest(EnumPropertyName.EndDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EndDateText { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string StartTimeText { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EndTimeText { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.Location, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Location { get; set; }

        [PropertyTest(EnumPropertyName.RecurseEveryWeek, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool RecurseEveryWeek { get; set; }

        [PropertyTest(EnumPropertyName.RecurseEveryWeek, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] CopyWithinWeek { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        public Web_AddTimeframeTestBlock()
        {
            TimeFrame = new TimeFrame();
        }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {

                bool isStartDate = DateTime.TryParse(StartDateText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime Sresult);
                if (isStartDate)
                    StartDate = Sresult;

                bool isEndDate = DateTime.TryParse(EndDateText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime Eresult);
                if (isStartDate)
                    EndDate = Eresult;

                TimeFrame.StartDate = StartDateText;
                TimeFrame.StartTime = StartTimeText;
                TimeFrame.EndDate = EndDateText;
                TimeFrame.EndTime = EndTimeText;
                TimeFrame.Type = Type;
                TimeFrame.Location = Location;
                TimeFrame.RecurseEveryWeek = RecurseEveryWeek.ToString();
                TimeFrame.Limitation = Limitation;

                WebTestLogic.Schedule.AddTimeframe(TimeFrame);
            });
        }

        public override void CleanUp()
        {
            var startTime = (Convert.ToDateTime(StartDateText).AddDays(-1));
            var endTime = (Convert.ToDateTime(EndDateText).AddDays(1));
            var getSchedule = ScheduleProxy.Instance.GetSchedule(new Schedule0.EntMsgGetScheduleRequest()
            {
                EntityID = OffenderId,
                EntityType = Schedule0.EnmEntityType.Offender,
                Version = -2,
                StartDate = StartDate.AddDays(-1),
                EndDate = EndDate.AddDays(1),
            });
            Schedule0.EntTimeFrame timeFrame;
            if (RecurseEveryWeek)
            {
                timeFrame = getSchedule.Schedule.WeeklySchedule.FirstOrDefault(x => x.FromTime == Convert.ToDateTime(StartDateText) && x.ToTime == Convert.ToDateTime(EndDateText));
            }
            else
            {
                timeFrame = getSchedule.Schedule.CalendarSchedule.FirstOrDefault(x => x.FromTime == Convert.ToDateTime(StartDateText) && x.ToTime == Convert.ToDateTime(EndDateText));
            }

            Schedule0.EntMsgDeleteTimeFrameRequest deleteTimeFrameRequest = new Schedule0.EntMsgDeleteTimeFrameRequest();

            deleteTimeFrameRequest.EntityID = OffenderId;
            deleteTimeFrameRequest.EntityType = Schedule0.EnmEntityType.Offender;
            deleteTimeFrameRequest.TimeframeID = timeFrame.ID;

            /* Send Delete Time Frame Request */
            ScheduleProxy.Instance.DeleteTimeFrame(deleteTimeFrameRequest);
        }
    }
}
