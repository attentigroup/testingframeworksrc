﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using Common.Enum;

namespace TestBlocksLib.WebTestBlocks.AddNewOffender_Location
{
    public class Web_TrailReportsTestBlock : AddNewOffenderLocationBase
    {
        [PropertyTest(EnumPropertyName.TrailReport, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public TrailReport TrailReport { get; set; }

        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectOffenderByRefId(RefID);
                OffenderLocation = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetOffenderLocationTrailData();
                TrailReport = WebTestLogic.AddNewOffenderTestLogic_NoClick.GetLocationTrailReport();
            });
        }
    }
}
