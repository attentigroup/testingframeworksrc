﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks.AddNewOffender_Location
{
    public abstract class AddNewOffenderLocationBase : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.OffenderRefID, EnumPropertyModifier.None)]
        public string RefID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderLocation, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public OffenderLocation OffenderLocation { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected abstract void ExecuteBlock_Basic();
    }
}
