﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks.AddNewOffender_Location
{
    public class Web_GetDVLastPoint : AddNewOffenderLocationBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderLogic = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(RefID, 1);
                //OffenderLocation = addNewOffenderLogic.Get;
                Logger.WriteLine($"Label = {OffenderLocation.NonDataLabel}, Last point = {OffenderLocation.LastTrailPointDateTime}");
            });
        }
    }
}
