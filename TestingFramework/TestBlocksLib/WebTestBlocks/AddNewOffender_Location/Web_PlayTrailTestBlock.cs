﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks.AddNewOffender_Location
{
    public class Web_PlayTrailTestBlock : AddNewOffenderLocationBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderLogic = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1);
                addNewOffenderLogic.LocationPlayTrail();
            });
        }
    }
}
