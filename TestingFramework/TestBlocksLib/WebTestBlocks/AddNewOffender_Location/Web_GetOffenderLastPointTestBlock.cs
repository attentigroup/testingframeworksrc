﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.Trails;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks.AddNewOffender_Location
{
    public class Web_OffenderLocationTestBlock : AddNewOffenderLocationBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.FilterTitle, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FilterTitle { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderLogic = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1, filterTitle: FilterTitle);
                OffenderLocation = addNewOffenderLogic.GetOffenderLocationTrailData();
                Logger.WriteLine($"Label = {OffenderLocation.NonDataLabel}, Last point = {OffenderLocation.LastTrailPointDateTime}");
            });
        }
    }
}
