﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndVerifyTimeframeIsDisplayedInScheduleTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeFrameID { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.IsElementExist, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsElementExist { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsElementExist = WebTestLogic.Schedule.IsTimeframeDisplayedInSchedule(SearchTerm, TimeFrameID.ToString());
            });
        }
    }
}
