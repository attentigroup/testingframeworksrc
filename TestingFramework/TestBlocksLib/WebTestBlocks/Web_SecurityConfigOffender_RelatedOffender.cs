﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigOffender_RelatedOffender : Web_SecurityConfigOffender_ActionsTestBlock
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(GetOffendersResponse.OffendersList.FirstOrDefault(x => x.RelatedOffenderID != null).EquipmentInfo.ReceiverSN);
            });
        }
    }
}
