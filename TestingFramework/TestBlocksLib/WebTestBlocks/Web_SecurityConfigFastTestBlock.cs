﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;
using WebTests.InfraStructure.Pages.WOMPages;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigFastTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.securityList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntSecurity> securityList { get; set; }

        [PropertyTest(EnumPropertyName.RefineNewRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntSecurity> RefineNewRequest { get; set; }
        
        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.Prepration, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool Prepration { get; set; }

        [PropertyTest(EnumPropertyName.isElementShown, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<SecurityConfigObject> IsElementShown { get; set; }

        //
        [PropertyTest(EnumPropertyName.ModifyOption, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public WebTests.Common.EnmModifyOption ModifyOption { get; set; }

        //

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected virtual void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                IsElementShown = new List<SecurityConfigObject>();

                for (int i = 0; i < RefineNewRequest.Count; i++)
                {
                    string[] sys_confige = RefineNewRequest[i].ResourceID.Split('_');
                    string ScreenName = sys_confige.First();
                    string Section = sys_confige[1];
                    string Type = sys_confige[2];
                    string Title = sys_confige[3];
                    
                    var elementResult = WebTestLogic.SecurityConfigLogic_Test.GetSecurityElementResult(ScreenName, Section, RefineNewRequest[i].ResourceID, Title, Prepration, ModifyOption);
                    elementResult.ResourceId = RefineNewRequest[i].ResourceID;
                    IsElementShown.Add(elementResult);
                }              

            });
        }
    }
}
