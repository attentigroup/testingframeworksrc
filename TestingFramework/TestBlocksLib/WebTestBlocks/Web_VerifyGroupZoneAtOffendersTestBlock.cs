﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_VerifyGroupZoneAtOffendersTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.NumOfOffenders, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int NumOfOffenders { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool Expected { get; set; }

        [PropertyTest(EnumPropertyName.isZoneExist, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool isZoneExist { get; set; }
        

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                isZoneExist = false;
                for (int i=1; i <= NumOfOffenders; i++)
                {
                    var groupPage = WebTestLogic.GroupPageTestLogic.SelectGroupByName(GroupName);
                    var offenderPage = groupPage.SelectOffenderByIndex(i);
                    isZoneExist = offenderPage.CheckGroupZoneExist();
                    if (isZoneExist != Expected)
                        break;
                }

                if (isZoneExist != Expected)
                    throw new Exception("Group Zone Wasn't Found At Offenders Or Zone Wasn't Deleted");
            });
        }
    }
}
