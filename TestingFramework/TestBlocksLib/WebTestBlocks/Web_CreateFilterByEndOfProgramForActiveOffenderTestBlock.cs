﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateFilterByEndOfProgramForActiveOffenderTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Time, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime Time { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.CreateFilterByEndOfProgramForActiveOffender(Time);
            });
        }
        //TODO: add filter
    }
}
