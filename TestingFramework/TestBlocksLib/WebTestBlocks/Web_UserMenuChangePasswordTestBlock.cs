﻿using Common.CustomAttributes;
using Common.Enum;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_UserMenuChangePasswordTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.NewPassword, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string NewPassword { get; set; }

        User user;


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
             user = new User()
            {
                UserName = FirstName,
                Password = Password,
                NewPassword = NewPassword
            };

            WebTestLogic.UserMenuTestLogic.ChangePassword(user);
        }
        public override void CleanUp()
        {
            //WebTestLogic.UserMenuTestLogic.ChangerPassword(new User() { UserName = User.UserName, Password = "W2e3r4t5", NewPassword= "E3r4t5y6" });
            //WebTestLogic.UserMenuTestLogic.ChangerPassword(new User() { UserName = User.UserName, Password = "E3r4t5y6", NewPassword = "R4t5y6u7" });
            //WebTestLogic.UserMenuTestLogic.ChangerPassword(new User() { UserName = User.UserName, Password = "R4t5y6u7", NewPassword = "T5y6u7i8" });
            WebTestLogic.UserMenuTestLogic.ChangePassword(new User() { UserName = user.UserName, Password = "W2e3r4t5", NewPassword = "Q1w2e3r4" });
        }
    }
}