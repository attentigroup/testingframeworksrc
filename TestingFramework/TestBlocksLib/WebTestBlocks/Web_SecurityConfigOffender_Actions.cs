﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigOffender_ActionsTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse
        {
            get
            {
                if (getOffendersResponse == null)
                    throw new Exception("GetOffenders Response is null");
                return getOffendersResponse;
            }
            set { getOffendersResponse = value; }
        }
        private Offenders0.EntMsgGetOffendersResponse getOffendersResponse;

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected virtual void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(GetOffendersResponse.OffendersList.First().RefID, 1, filterTitle: "Active Offenders");
                WebTestLogic.AddNewOffenderTestLogic.SecurityConfigOffender_Actions();
            });
        }
    }
}
