﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_VerifyGroupScheduleAtOffendersTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string TimeFrameName { get; set; }

        [PropertyTest(EnumPropertyName.NumOfOffenders, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int NumOfOffenders { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool Expected { get; set; }

        [PropertyTest(EnumPropertyName.isTimeFrameExist, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool isTimeFrameExist { get; set; }
        

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                isTimeFrameExist = false;
                for (int i=1; i <= NumOfOffenders; i++)
                {
                    var groupPage = WebTestLogic.GroupPageTestLogic.SelectGroupByName(GroupName);
                    var offenderPage = groupPage.SelectOffenderByIndex(i);
                    isTimeFrameExist = offenderPage.IsGroupTimeframeExist(TimeFrameName);
                    if (isTimeFrameExist != Expected)
                        break;
                }

                if (isTimeFrameExist != Expected)
                    throw new Exception("Group TimeFrame Wasn't Found At Offenders Or TimeFrame Wasn't Deleted");
            });
        }
    }
}
