﻿using Common.CustomAttributes;
using System.Threading;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_ClearCacheTestBlock : Web_TestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.UserMenuTestLogic.ClearCache();
            });
        }
    }
}
