﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddEquipmentTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.FileName, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string FileName { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EquipmentSN { get; set; }

        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment Equipment { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
               Equipment = WebTestLogic.EquipmentManagementTestLogic.AddEquipment(FileName, EquipmentSN);
            });
        }

        public override void CleanUp()
        {
            var getEquipment = EquipmentProxy.Instance.GetEquipmentList(new EntMsgGetEquipmentListRequest()
            {
                SerialNumber = EquipmentSN,
            });
            if (getEquipment.EquipmentList[0].AgencyID != -1)
            {
                EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest = new EntMsgUpdateEquipmentRequest()
                { EquipmentID = getEquipment.EquipmentList[0].ID, AgencyID = null };
                EquipmentProxy.Instance.UpdateEquipment(entMsgUpdateEquipmentRequest);
            }

            var getCellularData = EquipmentProxy.Instance.GetCellularData(new EntMsgGetCellularDataRequest()
            {
                SerialNumber = EquipmentSN,
            });
            if(getCellularData.CellularData.Count() > 0)
            {
                var deleteCellularDataRequest = new EntMsgDeleteCellularDataRequest();
                deleteCellularDataRequest.EquipmentID = getEquipment.EquipmentList[0].ID;
                EquipmentProxy.Instance.DeleteCellularData(deleteCellularDataRequest);
            }
            //TODO: verify is working
            Thread.Sleep(1000);
            //create equipment delete request
            var deleteRequest = new EntMsgDeleteEquipmentRequest();
            deleteRequest.EquipmentID = getEquipment.EquipmentList[0].ID;
            EquipmentProxy.Instance.DeleteEquipment(deleteRequest);
            //Wait.Until(() => DeleteRequest(getEquipment.EquipmentList[0].ID) == true);
        }
        private bool DeleteRequest(int equipmentId)
        {

            var deleteRequest = new EntMsgDeleteEquipmentRequest();
            deleteRequest.EquipmentID = equipmentId;
            try
            {
                EquipmentProxy.Instance.DeleteEquipment(deleteRequest);
                return true;
            }
            catch
            {
                return false;
            }
            
        }
    }
}
