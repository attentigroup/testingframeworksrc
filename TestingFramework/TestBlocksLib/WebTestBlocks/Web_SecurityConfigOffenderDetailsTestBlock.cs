﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigOffenderDetailsTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.securityList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntSecurity> securityList { get; set; }

        [PropertyTest(EnumPropertyName.RefineNewRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntSecurity> RefineNewRequest { get; set; }

        [PropertyTest(EnumPropertyName.ScreenName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScreenName { get; set; }

        [PropertyTest(EnumPropertyName.section, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string section { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string RefID { get; set; }

        [PropertyTest(EnumPropertyName.isElementShown, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Dictionary<string, bool> isElementShown { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter("automationTP", 1);

                isElementShown = new Dictionary<string, bool>();

                for (int i = 0; i < RefineNewRequest.Count; i++)
                {
                    string[] sys_confige = RefineNewRequest[i].ResourceID.Split('_');
                    string ScreenName = sys_confige.First();
                    string Section = sys_confige[1];
                    string Type = sys_confige[2];
                    string Title = sys_confige[3];

                    
                    //var elementResult = addNewOffenderTestLogic.GetSecurityElementResult(Section, RefineNewRequest[i].ResourceID);
                    //isElementShown.Add(RefineNewRequest[i].ResourceID, elementResult);

                }

            });
        }
    }
}
