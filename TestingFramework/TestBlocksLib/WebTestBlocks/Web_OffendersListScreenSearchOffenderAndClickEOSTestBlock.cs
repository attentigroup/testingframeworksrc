﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock : Web_TestBlockBase
    {
        // bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Title { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.noReceiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool noReceiver { get; set; }

        [PropertyTest(EnumPropertyName.isAutoEOS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool isAutoEOS { get; set; }

        [PropertyTest(EnumPropertyName.isSuccessful, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool isSuccessful { get; set; }

        [PropertyTest(EnumPropertyName.isCurfewViolations, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool isCurfewViolations { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(FirstName, sectionNumber, Title);
                addNewOffenderTestLogic_NoCLick.SendEOS(noReceiver, isAutoEOS, isSuccessful, isCurfewViolations);
            });
        }
    }
}