﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_NavigateToOperationalReportsTestBlock : Web_TestBlockBase
    {   
        /// <summary>
        /// Navigate to Operational Report Page
        /// </summary>

        [PropertyTest(EnumPropertyName.ReportName, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string ReportName { get; set; }

        [PropertyTest(EnumPropertyName.GenerateReportDateTime, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public DateTime GenerateReportDateTime { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
               GenerateReportDateTime = DateTime.Now;
                WebTestLogic.OperationalReportsTestLogic.SelectOperationalReports(ReportName);
            });

        }
    }
}
