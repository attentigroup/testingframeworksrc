﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using WebTests.InfraStructure.Entities;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndMoveCursorToContactInformationRowTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefId { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.PrimaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PrimaryTabLocatorString { get; set; }

        [PropertyTest(EnumPropertyName.SecondaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondaryTabLocatorString { get; set; }

        [PropertyTest(EnumPropertyName.isElementShown, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<SecurityConfigObject> isElementShown { get; set; }

        [PropertyTest(EnumPropertyName.RefineNewRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntSecurity> RefineNewRequest { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                isElementShown = new List<SecurityConfigObject>();
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1);
                for (int i = 0; i < RefineNewRequest.Count; i++)
                {                    
                    var elementResult = addNewOffenderTestLogic_NoCLick.GetSecurityResultFromOffenderDetailsRow(PrimaryTabLocatorString, SecondaryTabLocatorString, RefineNewRequest[i].ResourceID);
                    elementResult.ResourceId = RefineNewRequest[i].ResourceID;
                    isElementShown.Add(elementResult);
                }
                                
            });
        }
    }
}

            