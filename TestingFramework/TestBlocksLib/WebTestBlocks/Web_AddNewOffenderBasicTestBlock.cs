﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_AddNewOffenderBasicTestBlock : Web_AddNewOffenderTestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                OffenderDetails.Agency = AgencyName;
                OffenderDetails.ProgramType = ProgramType;
                OffenderDetails.Receiver = Receiver;
                if (Transmitter != null)
                    OffenderDetails.Tx = Transmitter;
                if (SubType != null)
                    OffenderDetails.SubType = SubType;
                if (RelatedOffenderDetails != null)
                    OffenderDetails.RelatedOffenderRefId = RelatedOffenderDetails.LastName + " " + RelatedOffenderDetails.FirstName + " (" + RelatedOffenderDetails.OffenderRefId + ")";//RelatedOffenderRefId;

                OffenderDetails = WebTestLogic.AddNewOffenderTestLogic.AddNewOffenderBasic(OffenderDetails);
            });
        }
    }
}
