﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_FilterEventsByRangeOfDates : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime startTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime endTime { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.FilterEventsByRangeOfDates(startTime, endTime);
            });
        }
    }
}
