﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndUpdateTimeFrameTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TimeFrameID { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrame, EnumPropertyType.None, EnumPropertyModifier.None)]
        public TimeFrame TimeFrame { get; set; }

        //[PropertyTest(EnumPropertyName.StartDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        //public string StartDateText { get; set; }

        //[PropertyTest(EnumPropertyName.EndDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        //public string EndDateText { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.Location, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Location { get; set; }

        [PropertyTest(EnumPropertyName.RecurseEveryWeek, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool RecurseEveryWeek { get; set; }


        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        public Web_SelectOffenderAndUpdateTimeFrameTestBlock()
        {
            TimeFrame = new TimeFrame();
        }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                //bool isStartDate = DateTime.TryParse(StartDateText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime Sresult);
                //if (isStartDate)
                //    StartDate = Sresult;

                //bool isEndDate = DateTime.TryParse(EndDateText, new CultureInfo("en-US"), DateTimeStyles.None, out DateTime Eresult);
                //if (isStartDate)
                //    EndDate = Eresult;

                TimeFrame.StartDate = StartDate.ToString();
                TimeFrame.EndDate = EndDate.ToString();
                TimeFrame.Type = Type;
                TimeFrame.Location = Location;
                TimeFrame.RecurseEveryWeek = RecurseEveryWeek.ToString();
                TimeFrame.Limitation = Limitation;

                WebTestLogic.Schedule.UpdateTimeFrame(SearchTerm, TimeFrame, TimeFrameID.ToString(), ProgramType.ToString());
            });
        }
    }
}
