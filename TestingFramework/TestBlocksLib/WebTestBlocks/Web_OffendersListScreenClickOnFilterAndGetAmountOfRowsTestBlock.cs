﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using WebTests.SeleniumWrapper;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string filterTitle { get; set; }

        [PropertyTest(EnumPropertyName.sectionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int sectionNumber { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int AmountOfRows { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.ClickOnFilter(sectionNumber, filterTitle);

                List<BrowserElement> rows = WebTestLogic.OffendersListTestLogic.GetAllRows();
                int counterOrig = WebTestLogic.OffendersListTestLogic.GetAllRows().Count;
                int counterNow = 0;

                while (true)
                {
                    Thread.Sleep(100);
                    counterNow = WebTestLogic.OffendersListTestLogic.GetAllRows().Count;
                    if (counterNow == counterOrig)
                        break;

                    counterOrig = counterNow;
                    rows = WebTestLogic.OffendersListTestLogic.GetAllRows();
                }

                if (counterNow != 0)
                {
                    rows.Last().MoveCursorToElement();
                    Wait.Until(() => rows.Last().Displayed);
                }
                    

                AmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRows());

                //WebTestLogic.OffendersListTestLogic.ClickOnFilter(sectionNumber, filterTitle);
                //int prevAmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRows());
                //AmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRows());

                //if (prevAmountOfRows != AmountOfRows)
                //{
                //    WebTestLogic.OffendersListTestLogic.ClickOnFilter(sectionNumber, filterTitle);
                //    AmountOfRows = int.Parse(WebTestLogic.OffendersListTestLogic.GetAmountOfRows());
                //}

            });
        }
    }
}
