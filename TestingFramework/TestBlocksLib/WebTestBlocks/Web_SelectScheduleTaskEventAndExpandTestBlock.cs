﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectScheduleTaskEventAndExpandTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.Recent, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Recent { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.MonitorScreenTestLogic.FilterByTimeSelectScheduleTaskEventAndExpand(Recent);
            });
        }
    }
}
