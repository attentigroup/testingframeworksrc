﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectOffenderAndEditCircularZoneTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefId { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsFullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RealRadious { get; set; }

        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int BufferRadious { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.Zone.UpdateCircularZone(SearchTerm, Name, GraceTime, IsFullSchedule, RealRadious, BufferRadious);
            });
        }
    }
}
