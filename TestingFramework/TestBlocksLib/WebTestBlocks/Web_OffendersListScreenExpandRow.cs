﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_OffendersListScreenExpandRow : Web_TestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]   
         protected void ExecuteBlock_Basic()                                                               
         {                                                                                                 
          Run(() =>                                                                                        
          {
              WebTestLogic.OffendersListTestLogic.ExpandRow();
          });                                                                                              
         } 
    }
}
