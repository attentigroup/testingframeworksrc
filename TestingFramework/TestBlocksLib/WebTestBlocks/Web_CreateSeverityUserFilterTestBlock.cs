﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateSeverityUserFilterTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.Severity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Severity { get; set; }

        [PropertyTest(EnumPropertyName.SearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchTerm { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSN { get; set; }

        [PropertyTest(EnumPropertyName.SecondarySearchTerm, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SecondarySearchTerm { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var addNewOffenderTestLogic_NoCLick = WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter(SearchTerm, 1, "Active Offenders", SecondarySearchTerm);
                addNewOffenderTestLogic_NoCLick.CreateSeverityUserFilter(Severity);
            });
        }

        public override void CleanUp()
        {
            WebTestLogic.AddNewOffenderTestLogic_NoClick.DeleteUserFilter("Severity Filter");
        }
    }
}
