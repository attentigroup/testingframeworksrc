﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfig_Offender_CurrentPlannedList : Web_TestBlockBase
    {

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected virtual void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectFirstOffenderUsingFilter(1, "Active Offenders");

                WebTestLogic.AddNewOffenderTestLogic.AddNewOffenderTestLogic_NoClick.MoveToConfigurationTab();
            });
        }
    }
}
