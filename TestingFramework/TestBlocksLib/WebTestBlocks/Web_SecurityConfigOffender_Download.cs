﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigOffender_Download : Web_SecurityConfigOffender_ActionsTestBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var selectedOffender = GetOffendersResponse.OffendersList.FirstOrDefault(x => x.ProgramEnd >= DateTime.Now);
                if (selectedOffender == null)
                    throw new Exception("No offender with relevant 'Program End' found");
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(selectedOffender.EquipmentInfo.ReceiverSN);
                WebTestLogic.AddNewOffenderTestLogic.SecurityConfigOffender_Download();
            });
        }
    }
}
