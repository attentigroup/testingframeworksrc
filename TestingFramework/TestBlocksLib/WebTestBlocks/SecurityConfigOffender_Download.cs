﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class SecurityConfigOffender_Download : Web_SecurityConfigOffender_ActionsTestBlock
    {
        protected virtual void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SelectOffenderByTerm(ProgramType);
                WebTestLogic.AddNewOffenderTestLogic.SecurityConfigOffender_Download();
            });
        }

    }
}
