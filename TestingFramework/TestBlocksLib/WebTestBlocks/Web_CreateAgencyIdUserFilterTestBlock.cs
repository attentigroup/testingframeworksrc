﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_CreateAgencyIdUserFilterTestBlock : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyId { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AmountOfRows { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                AmountOfRows = int.Parse(WebTestLogic.MonitorScreenTestLogic.CreateAgencyIdUserFilter(AgencyId.ToString()));

            });
        }

        public override void CleanUp()
        {
            WebTestLogic.MonitorScreenTestLogic.DeleteUserFilter("Agency ID Filter");
        }
    }
}
