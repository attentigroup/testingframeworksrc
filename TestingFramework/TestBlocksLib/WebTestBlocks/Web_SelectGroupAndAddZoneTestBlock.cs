﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectGroupAndAddZoneTestBlock : Web_TestBlockBase
    {

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.ZoneName, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsFullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RealRadious { get; set; }

        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int BufferRadious { get; set; }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                var groupPage = WebTestLogic.GroupPageTestLogic.SelectGroupByName(GroupName);
                groupPage.AddCircularZone(Name, Limitation, GraceTime, IsFullSchedule, RealRadious, BufferRadious);
            });
        }
    }
}
