﻿using Common.CustomAttributes;
using Common.Enum;
using System;

namespace TestBlocksLib.WebTestBlocks
{

    public class Web_OffenderConfigTrackingRulesTabSelectorTestBlock : Web_TestBlockBase
    {        
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.OffendersListTestLogic.SerachOffenderUsingFilter("1 Piece", 1, "Active Offenders");
                WebTestLogic.AddNewOffenderTestLogic_NoClick.MoveToTrackerRulesTab();
            });
        }
    }
}