﻿using Common.CustomAttributes;

namespace TestBlocksLib.WebTestBlocks
{
    //TODO: delete after use
    public class Web_FlowDebugger : Web_TestBlockBase
    {
        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.UsersListTestLogic.SearchUser("All");
            });
        }
    }
}
