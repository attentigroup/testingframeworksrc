﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.API.Offenders;
using WebTests.InfraStructure.Entities;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_AddNewOffenderFullTestBlock : Web_AddNewOffenderTestBlockBase
    {
        

        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment Equipment { get; set; }

        

        public Web_AddNewOffenderFullTestBlock()
        {
            OffenderDetails = new OffenderDetails();
        }

        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected override void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                OffenderDetails.Agency = Equipment.Agency;
                OffenderDetails.ProgramType = ProgramType;
                OffenderDetails.Receiver = Equipment.SerialNumber;

                WebTestLogic.AddNewOffenderTestLogic.AddNewOffenderFull(OffenderDetails);
            });
        }
    }
}
