﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestBlocksLib.WebTestBlocks
{
    public class Web_SelectEquipmentAndOpenVoiceSimDataPopup : Web_TestBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string equipmentSN { get; set; }


        [MethodVersion(MethodVersionAttribute.EARLIEST_VERSION, MethodVersionAttribute.LATEST_VERSION)]
        protected void ExecuteBlock_Basic()
        {
            Run(() =>
            {
                WebTestLogic.EquipmentManagementTestLogic.SelectEquipmentAndOpenVoiceSimDataPopup(equipmentSN);
            });
        }
    }
}
