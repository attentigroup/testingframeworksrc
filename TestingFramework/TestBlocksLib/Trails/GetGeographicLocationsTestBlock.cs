﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Trails;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace TestBlocksLib.Trails
{
    public class GetGeographicLocationsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetGeographicLocationsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetGeographicLocationsRequest GetGeographicLocationsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGeographicLocationsResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails0.EntMsgGetGeographicLocationsResponse GetGeographicLocationsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetGeographicLocationsResponse = TrailsProxy.Instance.GetGeographicLocations(GetGeographicLocationsRequest);
        }
    }
}
