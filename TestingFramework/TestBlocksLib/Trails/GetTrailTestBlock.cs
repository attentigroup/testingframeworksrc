﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Trails;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace TestBlocksLib.Trails
{
    public class GetTrailTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails0.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetTrailResponse = TrailsProxy.Instance.GetTrail(GetTrailRequest);
        }
    }

    public class GetTrailTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails1.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails1.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetTrailResponse = TrailsProxy_1.Instance.GetTrail(GetTrailRequest);
        }
    }

    public class GetTrailTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails2.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetTrailResponse = TrailsProxy_2.Instance.GetTrail(GetTrailRequest);
        }
    }
}
