﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Offender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.Trails
{
    public class GetOffenderWithTrailTestBlock : TestingBlockBase
    {
        //[PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        //public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntOffender Offender { get; set; }
        protected override void ExecuteBlock()
        {
            var getOffender = OffendersProxy.Instance.GetOffenders(new EntMsgGetOffendersListRequest()
            {
                OffenderID = new EntNumericParameter() { Operator = EnmNumericOperator.Equal, Value = OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }
            });

            Offender = getOffender.OffendersList[0];
        }
    }
}
