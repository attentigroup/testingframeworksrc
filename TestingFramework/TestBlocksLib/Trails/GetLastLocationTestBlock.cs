﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Trails;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace TestBlocksLib.Trails
{
    public class GetLastLocationTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails0.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetLastLocationResponse = TrailsProxy.Instance.GetLastLocation(GetLastLocationRequest);
        }
    }

    public class GetLastLocationTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails1.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails1.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetLastLocationResponse = TrailsProxy_1.Instance.GetLastLocation(GetLastLocationRequest);
        }
    }

    public class GetLastLocationTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails2.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }
        protected override void ExecuteBlock()
        {
            GetLastLocationResponse = TrailsProxy_2.Instance.GetLastLocation(GetLastLocationRequest);
        }
    }
}
