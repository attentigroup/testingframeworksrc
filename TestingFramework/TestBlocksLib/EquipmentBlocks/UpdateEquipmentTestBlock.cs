﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Equipment;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion


namespace TestBlocksLib.EquipmentBlocks
{
    public class UpdateEquipmentTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy.Instance.UpdateEquipment(UpdateEquipmentRequest);
        }

        public override void CleanUp()
        {

        }
    }


    public class UpdateEquipmentTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy_1.Instance.UpdateEquipment(UpdateEquipmentRequest);
        }

        public override void CleanUp()
        {

        }
    }


    public class UpdateEquipmentTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy_2.Instance.UpdateEquipment(UpdateEquipmentRequest);
        }

        public override void CleanUp()
        {

        }
    }
}
