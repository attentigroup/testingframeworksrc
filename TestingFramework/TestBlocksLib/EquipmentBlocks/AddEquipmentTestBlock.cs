﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;
using System;
using System.Threading;

#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    /// <summary>
    /// Add Equipment for the latest version
    /// </summary>
    public class AddEquipmentTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment.EntMsgAddEquipmentResponse AddEquipmentResponse { get; set; }

        [PropertyTest(EnumPropertyName.IsEquipmentExists, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsEquipmentExists { get; set; }

        public AddEquipmentTestBlock()
        {
            //IsEquipmentExists = false;
        }

        protected override void ExecuteBlock()
        {
            if (!IsEquipmentExists)
                AddEquipmentResponse = EquipmentProxy.Instance.AddEquipment(AddEquipmentRequest);
        }

        public override void CleanUp()
        {
            //in case equipment related to agency need to remove it from the agency
            if (AddEquipmentRequest.AgencyID != -1)
            {
                Equipment.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest = new Equipment.EntMsgUpdateEquipmentRequest()
                { EquipmentID = AddEquipmentResponse.NewEquipmentID, AgencyID = null };
                try
                {
                    EquipmentProxy.Instance.UpdateEquipment(entMsgUpdateEquipmentRequest);
                }
                catch (FaultException<ValidationFault> fe)
                {
                    Log.ErrorFormat("Error while try to update equipment {0} .{1}. Continue to delete the equipment.",
                        AddEquipmentResponse.NewEquipmentID, fe.Detail.Errors[0].Message);
                }
            }
            //create equipment delete request
            Thread.Sleep(1500);
            var deleteRequest = new Equipment.EntMsgDeleteEquipmentRequest();
            deleteRequest.EquipmentID = AddEquipmentResponse.NewEquipmentID;
            try
            {
                EquipmentProxy.Instance.DeleteEquipment(deleteRequest);
            }
            catch (Exception e)
            {
                if (e.Message.Contains("#SY"))
                {
                    Thread.Sleep(30000);
                    EquipmentProxy.Instance.DeleteEquipment(deleteRequest);
                }
                else
                {
                    throw new Exception(e + " the CleanUp for AddEquipment has failed!");
                }
            }
        }
    }


    /// <summary>
    /// Add Equipment for one version berfore lastest
    /// </summary>
    public class AddEquipmentTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy_1.Instance.AddEquipment(AddEquipmentRequest);
        }
        /// <summary>
        /// clean up for the block.
        /// need to check if the equipment related to agency,
        /// if yes need to disconnect it first and only then to delete the equipment
        /// </summary>
        public override void CleanUp()
        {
            //get the equipment id - assuming SerialNumber uniqe.
            var entMsgGetEquipmentListRequest = new Equipment_1.EntMsgGetEquipmentListRequest();
            entMsgGetEquipmentListRequest.SerialNumber = AddEquipmentRequest.SerialNumber;
            var entMsgGetEquipmentListResponse = EquipmentProxy_1.Instance.GetEquipmentList(entMsgGetEquipmentListRequest);

            if (entMsgGetEquipmentListResponse.EquipmentList == null ||
                entMsgGetEquipmentListResponse.EquipmentList.Length == 0)
            {
                Log.WarnFormat("while cleanup - equipment list return empty for SerialNumber {0}.", AddEquipmentRequest.SerialNumber);
            }
            else
            {//create equipment delete request
                var entEquipment = entMsgGetEquipmentListResponse.EquipmentList[0];
                if (entEquipment.AgencyID == AddEquipmentRequest.AgencyID)
                {//update the agency in case of agency assigned
                    Equipment_1.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest =
                        new Equipment_1.EntMsgUpdateEquipmentRequest() { EquipmentID = entEquipment.ID, AgencyID = null };
                    try
                    {
                        EquipmentProxy_1.Instance.UpdateEquipment(entMsgUpdateEquipmentRequest);
                    }
                    catch (FaultException<Equipment_1.ValidationFault> fe)
                    {
                        Log.WarnFormat("while cleanup - UpdateEquipment failed for SN {0} error: {1}", 
                            entEquipment.SerialNumber, fe.Detail.Errors[0].Message);
                    }
                    catch (Exception exception)
                    {
                        Log.WarnFormat("while cleanup - UpdateEquipment failed for SN {0} error: {1}",
                            entEquipment.SerialNumber, exception.Message);
                    }
                }
                //delete the equipment
                var deleteRequest = new Equipment_1.EntMsgDeleteEquipmentRequest();
                deleteRequest.EquipmentID = entEquipment.ID;

                try
                {
                    EquipmentProxy_1.Instance.DeleteEquipment(deleteRequest);
                }
                catch (FaultException<Equipment_1.ValidationFault> fe)
                {
                    Log.WarnFormat("while cleanup - DeleteEquipment failed for EquipmentID {0} error: {1}",
                        deleteRequest.EquipmentID, fe.Detail.Errors[0].Message);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("#SY"))
                    {
                        Thread.Sleep(30000);
                        EquipmentProxy_1.Instance.DeleteEquipment(deleteRequest);
                    }
                    else
                    {
                        throw new Exception(e + " the CleanUp for AddEquipment has failed!");
                    }
                }
            }
        }
    }


    /// <summary>
    /// Add Equipment for two versions berfore lastest
    /// </summary>
    public class AddEquipmentTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy_2.Instance.AddEquipment(AddEquipmentRequest);
        }

        public override void CleanUp()
        {
            //get the equipment id - assuming SerialNumber uniqe.
            var entMsgGetEquipmentListRequest = new Equipment_2.EntMsgGetEquipmentListRequest();
            entMsgGetEquipmentListRequest.SerialNumber = AddEquipmentRequest.SerialNumber;
            entMsgGetEquipmentListRequest.AgencyID = AddEquipmentRequest.AgencyID;
            var entMsgGetEquipmentListResponse = EquipmentProxy_2.Instance.GetEquipmentList(entMsgGetEquipmentListRequest);

            //create equipment delete request
            var entEquipment = entMsgGetEquipmentListResponse.EquipmentList[0];
            if (entEquipment.SerialNumber.CompareTo(AddEquipmentRequest.SerialNumber) == 0 &&
               entEquipment.AgencyID == AddEquipmentRequest.AgencyID)

            {//delete the equipment
                Equipment_2.EntMsgUpdateEquipmentRequest entMsgUpdateEquipmentRequest = new Equipment_2.EntMsgUpdateEquipmentRequest()
                { EquipmentID = entEquipment.ID, AgencyID = null };

                EquipmentProxy_2.Instance.UpdateEquipment(entMsgUpdateEquipmentRequest);

                var deleteRequest = new Equipment_2.EntMsgDeleteEquipmentRequest();
                deleteRequest.EquipmentID = entEquipment.ID;
                EquipmentProxy_2.Instance.DeleteEquipment(deleteRequest);
            }
            else
            {
                Log.WarnFormat(
                    "AddEquipmentTestBlock:Cleanup-need to delete equipment with SN {0} but Get equipment by SN return different equipment ID={1} SN={2}",
                    AddEquipmentRequest.SerialNumber, entEquipment.ID, entEquipment.SerialNumber);
            }
        }
    }
}
