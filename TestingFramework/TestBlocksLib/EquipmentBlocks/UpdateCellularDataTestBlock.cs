﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class UpdateCellularDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        //[PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        //public EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            //get the equipment id - assuming SerialNumber uniqe.
            //var entMsgGetEquipmentListRequest = new EntMsgGetEquipmentListRequest();
            //entMsgGetEquipmentListRequest.SerialNumber = AddEquipmentRequest.SerialNumber;
            //entMsgGetEquipmentListRequest.AgencyID = AddEquipmentRequest.AgencyID;
            //var entMsgGetEquipmentListResponse = EquipmentProxy.Instance.GetEquipmentList(entMsgGetEquipmentListRequest);

            //Create equipment

            //var entEquipment = entMsgGetEquipmentListResponse.EquipmentList[0];
            //if (entEquipment.SerialNumber.CompareTo(AddEquipmentRequest.SerialNumber) == 0 &&
            //    entEquipment.AgencyID == AddEquipmentRequest.AgencyID)
            //{
            //    UpdateCellularDataRequest.ID = entEquipment.ID;
            //}


            EquipmentProxy.Instance.UpdateCellularData(UpdateCellularDataRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateCellularDataTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        //[PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        //public EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            //get the equipment id - assuming SerialNumber uniqe.
            //var entMsgGetEquipmentListRequest = new EntMsgGetEquipmentListRequest();
            //entMsgGetEquipmentListRequest.SerialNumber = AddEquipmentRequest.SerialNumber;
            //entMsgGetEquipmentListRequest.AgencyID = AddEquipmentRequest.AgencyID;
            //var entMsgGetEquipmentListResponse = EquipmentProxy.Instance.GetEquipmentList(entMsgGetEquipmentListRequest);

            //Create equipment

            //var entEquipment = entMsgGetEquipmentListResponse.EquipmentList[0];
            //if (entEquipment.SerialNumber.CompareTo(AddEquipmentRequest.SerialNumber) == 0 &&
            //    entEquipment.AgencyID == AddEquipmentRequest.AgencyID)
            //{
            //    UpdateCellularDataRequest.ID = entEquipment.ID;
            //}


            EquipmentProxy_1.Instance.UpdateCellularData(UpdateCellularDataRequest);
        }

        public override void CleanUp()
        {
            var deleteCellularDataRequest = new Equipment_1.EntMsgDeleteCellularDataRequest();
            deleteCellularDataRequest.ReceiverSerialNumber = UpdateCellularDataRequest.ReceiverSerialNumber;
            EquipmentProxy_1.Instance.DeleteCellularData(deleteCellularDataRequest);
        }
    }


    public class UpdateCellularDataTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        //[PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        //public EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            //get the equipment id - assuming SerialNumber uniqe.
            //var entMsgGetEquipmentListRequest = new EntMsgGetEquipmentListRequest();
            //entMsgGetEquipmentListRequest.SerialNumber = AddEquipmentRequest.SerialNumber;
            //entMsgGetEquipmentListRequest.AgencyID = AddEquipmentRequest.AgencyID;
            //var entMsgGetEquipmentListResponse = EquipmentProxy.Instance.GetEquipmentList(entMsgGetEquipmentListRequest);

            //Create equipment

            //var entEquipment = entMsgGetEquipmentListResponse.EquipmentList[0];
            //if (entEquipment.SerialNumber.CompareTo(AddEquipmentRequest.SerialNumber) == 0 &&
            //    entEquipment.AgencyID == AddEquipmentRequest.AgencyID)
            //{
            //    UpdateCellularDataRequest.ID = entEquipment.ID;
            //}


            EquipmentProxy_2.Instance.UpdateCellularData(UpdateCellularDataRequest);
        }

        public override void CleanUp()
        {
            var deleteCellularDataRequest = new Equipment_2.EntMsgDeleteCellularDataRequest();
            deleteCellularDataRequest.ReceiverSerialNumber = UpdateCellularDataRequest.ReceiverSerialNumber;
            EquipmentProxy_2.Instance.DeleteCellularData(deleteCellularDataRequest);
        }
    }
}
