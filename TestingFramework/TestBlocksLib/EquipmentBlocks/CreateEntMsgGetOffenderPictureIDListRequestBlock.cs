﻿//using System;
//using TestingFramework.TestsInfraStructure.Implementation;
//using Common.Enum;
//using Common.CustomAttributes;
//using System.ComponentModel;
//using System.Collections.Generic;

//#region API refs
//using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
//using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
//using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
//#endregion

//namespace TestBlocksLib.EquipmentBlocks
//{
//    public class CreateEntMsgGetOffenderPictureIDListRequestBlock : LogicBlockBase
//    {
//        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
//        public int OffenderID { get; set; }

//        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
//        public Offenders0.EntMsgGetPictureIDListRequest GetOffenderPictureIDListRequest { get; set; }


//        protected override void ExecuteBlock()
//        {
//            GetOffenderPictureIDListRequest = new Offenders0.EntMsgGetPictureIDListRequest();
//            GetOffenderPictureIDListRequest.OffenderID = OffenderID;
//        }
//    }


//    public class CreateEntMsgGetOffenderPictureIDListRequestBlock_1 : LogicBlockBase
//    {
//        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
//        public int OffenderID { get; set; }

//        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
//        public Offenders1.EntMsgGetOffenderPictureIDListRequest GetOffenderPictureIDListRequest { get; set; }


//        protected override void ExecuteBlock()
//        {
//            GetOffenderPictureIDListRequest = new Offenders1.EntMsgGetOffenderPictureIDListRequest();
//            GetOffenderPictureIDListRequest.OffenderID = OffenderID;
//        }
//    }


//    public class CreateEntMsgGetOffenderPictureIDListRequestBlock_2 : LogicBlockBase
//    {
//        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
//        public int OffenderID { get; set; }

//        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
//        public Offenders2.EntMsgGetOffenderPictureIDListRequest GetOffenderPictureIDListRequest { get; set; }


//        protected override void ExecuteBlock()
//        {
//            GetOffenderPictureIDListRequest = new Offenders2.EntMsgGetOffenderPictureIDListRequest();
//            GetOffenderPictureIDListRequest.OffenderID = OffenderID;
//        }
//    }
//}
