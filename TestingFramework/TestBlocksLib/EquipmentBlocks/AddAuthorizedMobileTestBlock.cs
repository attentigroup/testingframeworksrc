﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.EquipmentBlocks
{
    public class AddAuthorizedMobileTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgAddAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgAddAuthorizedMobileRequest AddAuthorizedMobileRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy.Instance.AddAuthorizedMobile(AddAuthorizedMobileRequest);
        }

        public override void CleanUp()
        {
            var deleteRequest = new EntMsgDeleteAuthorizedMobileRequest()
            {
                MobileID = AddAuthorizedMobileRequest.ID,
                ApplicationID = AddAuthorizedMobileRequest.ApplicationID
            };

            EquipmentProxy.Instance.DeleteAuthorizedMobile(deleteRequest);
        }
    }
}
