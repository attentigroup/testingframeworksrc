﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.EquipmentBlocks
{
    public class AddCellularDataTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgAddCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgAddCellularDataResponse AddCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgAddCellularDataRequest AddCellularDataRequest { get; set; }

        public AddCellularDataTestBlock()
        {
            AddCellularDataResponse = new EntMsgAddCellularDataResponse();
        }

        protected override void ExecuteBlock()
        {
            AddCellularDataResponse = EquipmentProxy.Instance.AddCellularData(AddCellularDataRequest);
        }

        public override void CleanUp()
        {
            var deleteRequest = new EntMsgDeleteCellularDataRequest();
            deleteRequest.EquipmentID = AddCellularDataRequest.EquipmentID;
            EquipmentProxy.Instance.DeleteCellularData(deleteRequest);
        }
    }
}
