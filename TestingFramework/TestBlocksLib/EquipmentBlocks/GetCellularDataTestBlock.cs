﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class GetCellularDataTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetCellularDataRequest GetCellularDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetCellularDataResponse = EquipmentProxy.Instance.GetCellularData(GetCellularDataRequest);
            //int id = GetCellularDataResponse.CellularData[0].ID;
        }
        public override void CleanUp()
        {
        }
    }


    public class GetCellularDataTestBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgGetCellularDataRequest GetCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSerialNumber { get; set; }

        protected override void ExecuteBlock()
        {
            //if(UpdateCellularDataRequest != null)
                GetCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;

            GetCellularDataResponse = EquipmentProxy_1.Instance.GetCellularData(GetCellularDataRequest);
            //int id = GetCellularDataResponse.CellularData[0].ID;
        }
        public override void CleanUp()
        {
        }
    }


    public class GetCellularDataTestBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgGetCellularDataRequest GetCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSerialNumber { get; set; }

        protected override void ExecuteBlock()
        {
            GetCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;

            GetCellularDataResponse = EquipmentProxy_2.Instance.GetCellularData(GetCellularDataRequest);
        }
        public override void CleanUp()
        {
        }
    }
}
