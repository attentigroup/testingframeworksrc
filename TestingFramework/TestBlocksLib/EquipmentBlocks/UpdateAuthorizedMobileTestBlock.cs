﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.EquipmentBlocks
{
    public class UpdateAuthorizedMobileTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgUpdateAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateAuthorizedMobileRequest UpdateAuthorizedMobileRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy.Instance.UpdateAuthorizedMobile(UpdateAuthorizedMobileRequest);
        }

        public override void CleanUp()
        {
            var deleteRequest = new EntMsgDeleteAuthorizedMobileRequest()
            {
                MobileID = UpdateAuthorizedMobileRequest.ID,
                ApplicationID = UpdateAuthorizedMobileRequest.ApplicationID
            };

            EquipmentProxy.Instance.DeleteAuthorizedMobile(deleteRequest);
        }

    }
}
