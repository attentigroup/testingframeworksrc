﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Agency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;
#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class VerifyIfEquipmenExistsTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentSN, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EquipmentSN { get; set; }

        [PropertyTest(EnumPropertyName.IsEquipmentExists, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsEquipmentExists { get; set; }

        public VerifyIfEquipmenExistsTestBlock()
        {
            IsEquipmentExists = false;
        }
        protected override void ExecuteBlock()
        {
            var getEquipment = EquipmentProxy.Instance.GetEquipmentList(new Equipment.EntMsgGetEquipmentListRequest()
            {
                SerialNumber = EquipmentSN
            });
            if (getEquipment.EquipmentList.Length > 0)
            {
                IsEquipmentExists = true;
                //EquipmentProxy.Instance.UpdateEquipment(new Equipment.EntMsgUpdateEquipmentRequest()
                //{
                //    EquipmentID = getEquipment.EquipmentList[0].ID,
                //    AgencyID = AgencyIdGenerator.AgencyIdList[0],
                //});
            }
                

        }
    }

    public class VerifyIfEquipmenExistsTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.IsEquipmentExists, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public bool IsEquipmentExists { get; set; }

        public VerifyIfEquipmenExistsTestBlock_1()
        {
            IsEquipmentExists = false;
        }
        protected override void ExecuteBlock()
        {
            var getEquipment = EquipmentProxy_1.Instance.GetEquipmentList(new Equipment_1.EntMsgGetEquipmentListRequest()
            {
                SerialNumber = AddEquipmentRequest.SerialNumber,
            });
            if (getEquipment.EquipmentList.Length > 0)
                IsEquipmentExists = true;
        }
    }
}

