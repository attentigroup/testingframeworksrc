﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Agency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;
#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class GetEquipmentWithoutOffender : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment.EnmEquipmentModel Model { get; set; }

        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment.EntEquipmentBase Equipment { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSN, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public string EquipmentSN { get; set; }

        protected override void ExecuteBlock()
        {
            int index = 0;
            List<int> agencies = AgencyIdGenerator.AgencyIdList;
            while (Equipment == null && index< agencies.Count)
            {
                var GetEquipmentListRequest = new Equipment.EntMsgGetEquipmentListRequest();
                GetEquipmentListRequest.AgencyID = agencies.ElementAt(index);
                GetEquipmentListRequest.Programs = new Equipment.EnmProgramType[] { ProgramType };                
                GetEquipmentListRequest.Type = EnmEquipmentType.Receiver;
                GetEquipmentListRequest.IsDamaged = false;

                if (Model != 0)
                    GetEquipmentListRequest.Models = new Equipment.EnmEquipmentModel[] { Model };

                var getEqipment = EquipmentProxy.Instance.GetEquipmentList(GetEquipmentListRequest);

                if (getEqipment.EquipmentList.Length > 0)
                {

                    foreach(var equipment in getEqipment.EquipmentList)
                    {
                        if ((equipment.OffenderID == null) && (equipment.AgencyID != null))
                        {
                            var getCellularData = EquipmentProxy.Instance.GetCellularData(new Equipment.EntMsgGetCellularDataRequest()
                            {
                                DeviceIDList = new int[] { equipment.ID }
                            });
                            if (getCellularData.CellularData.Length > 0)
                            {
                                Equipment = equipment;
                                EquipmentSN = equipment.SerialNumber;
                                break;
                            }
       
                        }
                    }

                }
                index++;
            }

            if (EquipmentSN == null)
                throw new Exception("Equipment Without Offender Not Found");

            //else create equipment
            //if (Equipment == null)
            //{
            //    var AddEquipmentResponse = EquipmentProxy.Instance.AddEquipment(new Equipment.EntMsgAddEquipmentRequest()
            //    {
            //        AgencyID = agencies.ElementAt(0),
            //        EquipmentEncryptionGSM = EnmEncryptionType.NoEncryption,
            //        EquipmentEncryptionRF = EnmEncryptionType.NoEncryption,
            //        Model = EnmEquipmentModel.One_Piece_GPS_RF,
            //        ModemType = EnmModemType.TwoG,
            //        ProtocolType = EnmProtocolType.Protocol_5,
            //        SerialNumber = "34211587",
            //    });

            //    var AddCellularDataResponse = EquipmentProxy.Instance.AddCellularData(new EntMsgAddCellularDataRequest()
            //    {
            //        EquipmentID = AddEquipmentResponse.NewEquipmentID,
            //        ProviderID = 2,
            //        VoicePrefixPhone = "97252",
            //        VoicePhoneNumber = "1234567",
            //        DataPrefixPhone = "97252",
            //        DataPhoneNumber = "1234567",
            //    });

            //    var getEquipment = EquipmentProxy.Instance.GetEquipmentList(new EntMsgGetEquipmentListRequest()
            //    {
            //        SerialNumber = "34211587",
            //    });

            //    Equipment = getEquipment.EquipmentList[0];
            //}

        }
    }

    public class GetEquipmentWithoutOffender_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EnmEquipmentModel Model { get; set; }

        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntEquipmentBase Equipment { get; set; }
        protected override void ExecuteBlock()
        {
            int index = 0;
            List<int> agencies = AgencyIdGenerator.AgencyIdList;
            while (Equipment == null && index < agencies.Count)
            {
                var getEqipment = EquipmentProxy_1.Instance.GetEquipmentList(new Equipment_1.EntMsgGetEquipmentListRequest()
                {
                    AgencyID = agencies.ElementAt(index),
                    Programs = new Equipment_1.EnmProgramType[] { ProgramType },
                    Models = new Equipment_1.EnmEquipmentModel[] { Model },
                    Type = Equipment_1.EnmEquipmentType.Receiver,
                    IsDamaged = false

                });
                if (getEqipment.EquipmentList.Length > 0)
                {

                    foreach (var equipment in getEqipment.EquipmentList)
                    {
                        if ((equipment.OffenderID == null) && (equipment.AgencyID != null))
                        {
                            var getCellularData = EquipmentProxy_1.Instance.GetCellularData(new Equipment_1.EntMsgGetCellularDataRequest()
                            {
                                ReceiverIDList = new int[] { equipment.ID }
                            });
                            if (getCellularData.CellularData.Length > 0)
                            {
                                Equipment = equipment;
                                break;
                            }

                        }
                    }

                }
                index++;
            }

            //else create equipment
            if (Equipment == null)
            {
                EquipmentProxy_1.Instance.AddEquipment(new Equipment_1.EntMsgAddEquipmentRequest()
                {
                    AgencyID = agencies.ElementAt(0),
                    EquipmentEncryptionGSM = Equipment_1.EnmEncryptionType.NoEncryption,
                    EquipmentEncryptionRF = Equipment_1.EnmEncryptionType.NoEncryption,
                    Model = Equipment_1.EnmEquipmentModel.One_Piece_GPS_RF,
                    ModemType = Equipment_1.EnmModemType._2G,
                    ProtocolType = Equipment_1.EnmProtocolType.Protocol_5,
                    SerialNumber = "34211587",
                });

                EquipmentProxy_1.Instance.UpdateCellularData(new Equipment_1.EntMsgUpdateCellularDataRequest()
                {
                    ReceiverSerialNumber = "34211587",
                    ProviderID = 2,
                    VoicePrefixPhone = "97252",
                    VoicePhoneNumber = "1234567",
                    CSDPrefixPhone = "97252",
                    CSDPhoneNumber = "1234567",
                });

                var getEquipment = EquipmentProxy_1.Instance.GetEquipmentList(new Equipment_1.EntMsgGetEquipmentListRequest()
                {
                    SerialNumber = "34211587",
                });

                Equipment = getEquipment.EquipmentList[0];
            }

        }
    }

    public class GetEquipmentWithoutOffender_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EnmEquipmentModel Model { get; set; }

        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntEquipmentBase Equipment { get; set; }
        protected override void ExecuteBlock()
        {
            int index = 0;
            List<int> agencies = AgencyIdGenerator.AgencyIdList;
            while (Equipment == null && index < agencies.Count)
            {
                var getEqipment = EquipmentProxy_2.Instance.GetEquipmentList(new Equipment_2.EntMsgGetEquipmentListRequest()
                {
                    AgencyID = agencies.ElementAt(index),
                    Programs = new Equipment_2.EnmProgramType[] { ProgramType },
                    Models = new Equipment_2.EnmEquipmentModel[] { Model },
                    Type = Equipment_2.EnmEquipmentType.Receiver,
                    IsDamaged = false


                });
                if (getEqipment.EquipmentList.Length > 0)
                {

                    foreach (var equipment in getEqipment.EquipmentList)
                    {
                        if ((equipment.OffenderID == null) && (equipment.AgencyID != null))
                        {
                            var getCellularData = EquipmentProxy_2.Instance.GetCellularData(new Equipment_2.EntMsgGetCellularDataRequest()
                            {
                                ReceiverIDList = new int[] { equipment.ID }
                            });
                            if (getCellularData.CellularData.Length > 0)
                            {
                                Equipment = equipment;
                                break;
                            }

                        }
                    }

                }
                index++;
            }

            //else create equipment
            if (Equipment == null)
            {
                EquipmentProxy_2.Instance.AddEquipment(new Equipment_2.EntMsgAddEquipmentRequest()
                {
                    AgencyID = agencies.ElementAt(0),
                    EquipmentEncryptionGSM = Equipment_2.EnmEncryptionType.NoEncryption,
                    EquipmentEncryptionRF = Equipment_2.EnmEncryptionType.NoEncryption,
                    Model = Equipment_2.EnmEquipmentModel.One_Piece_GPS_RF,
                    ModemType = Equipment_2.EnmModemType._2G,
                    ProtocolType = Equipment_2.EnmProtocolType.Protocol_5,
                    SerialNumber = "34211587",
                });

                EquipmentProxy_2.Instance.UpdateCellularData(new Equipment_2.EntMsgUpdateCellularDataRequest()
                {
                    ReceiverSerialNumber = "34211587",
                    ProviderID = 2,
                    VoicePrefixPhone = "97252",
                    VoicePhoneNumber = "1234567",
                    CSDPrefixPhone = "97252",
                    CSDPhoneNumber = "1234567",
                });

                var getEquipment = EquipmentProxy_2.Instance.GetEquipmentList(new Equipment_2.EntMsgGetEquipmentListRequest()
                {
                    SerialNumber = "34211587",
                });

                Equipment = getEquipment.EquipmentList[0];
            }

        }
    }
}
