﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class DeleteCellularDataTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgDeleteCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgDeleteCellularDataRequest DeleteCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            EquipmentProxy.Instance.DeleteCellularData(DeleteCellularDataRequest);
        }
    }


    public class DeleteCellularDataTestBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgDeleteCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgDeleteCellularDataRequest DeleteCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            EquipmentProxy_1.Instance.DeleteCellularData(DeleteCellularDataRequest);
        }
    }


    public class DeleteCellularDataTestBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgDeleteCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgDeleteCellularDataRequest DeleteCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            EquipmentProxy_2.Instance.DeleteCellularData(DeleteCellularDataRequest);
        }
    }
}
