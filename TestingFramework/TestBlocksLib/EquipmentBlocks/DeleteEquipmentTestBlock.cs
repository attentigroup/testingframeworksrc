﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class DeleteEquipmentTestBlock : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgDeleteEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgDeleteEquipmentRequest DeleteEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy.Instance.DeleteEquipment(DeleteEquipmentRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class DeleteEquipmentTestBlock_1 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgDeleteEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgDeleteEquipmentRequest DeleteEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy_1.Instance.DeleteEquipment(DeleteEquipmentRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class DeleteEquipmentTestBlock_2 : TestingBlockBase
    {

        [PropertyTest(EnumPropertyName.EntMsgDeleteEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgDeleteEquipmentRequest DeleteEquipmentRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy_2.Instance.DeleteEquipment(DeleteEquipmentRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
