﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.EquipmentBlocks
{
    public class UpdateActiveStatus2AuthorizedMobilesTestBlock : TestingBlockBase
    {
        //[PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        //public EntMsgGetAuthorizedMobileListRequest GetAuthorizedMobileListRequest { get; set; }

        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmMobileApplication ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.IsActive, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public bool IsActive { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetAuthorizedMobileListRequest GetAuthorizedMobileListRequest = new EntMsgGetAuthorizedMobileListRequest();
            GetAuthorizedMobileListRequest.ApplicationID = ApplicationID;

            GetAuthorizedMobileListResponse = EquipmentProxy.Instance.GetAuthorizedMobileList(GetAuthorizedMobileListRequest);

            EntMsgUpdateAuthorizedMobileRequest entMsgUpdateAuthorizedMobileRequest = new EntMsgUpdateAuthorizedMobileRequest();
            var innerExceptions = new List<Exception>();

            foreach (var mobile in GetAuthorizedMobileListResponse.AuthorizedMobilesList)
            {
                if (mobile.IsActive == true)
                    continue;
                entMsgUpdateAuthorizedMobileRequest.ID = mobile.ID;
                entMsgUpdateAuthorizedMobileRequest.ApplicationID = mobile.ApplicationID;
                entMsgUpdateAuthorizedMobileRequest.IsActive = IsActive;
                entMsgUpdateAuthorizedMobileRequest.AppVersion = mobile.AppVersion;
                try
                {
                    EquipmentProxy.Instance.UpdateAuthorizedMobile(entMsgUpdateAuthorizedMobileRequest);
                }
                catch (Exception ex)
                {
                    var msg = string.Format("error while try to update mobile id {0}", mobile.ID);
                    innerExceptions.Add(new Exception(msg, ex));
                }
            }

            if( innerExceptions.Count > 0)
            {
                var aggregateException = new AggregateException("errors during Update Active Status 2 Authorized Mobiles Test Block", innerExceptions);
                throw aggregateException;
            }            
        }
    }
}
