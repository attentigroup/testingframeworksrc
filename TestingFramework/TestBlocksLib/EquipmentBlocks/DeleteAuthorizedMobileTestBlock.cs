﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.EquipmentBlocks
{
    public class DeleteAuthorizedMobileTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgDeleteAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgDeleteAuthorizedMobileRequest DeleteAuthorizedMobileRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EquipmentProxy.Instance.DeleteAuthorizedMobile(DeleteAuthorizedMobileRequest);
        }

        public override void CleanUp()
        {

        }
    }
}
