﻿using Common.CustomAttributes;
using Common.Enum;
using System.Collections.Generic;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace TestBlocksLib.EquipmentBlocks
{
    public class GetEquipmentListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListRequest, EnumPropertyType.EntMsgGetEquipmentListRequest, EnumPropertyModifier.None)]
        public EntMsgGetEquipmentListRequest GetEquipmentListRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, Common.Enum.EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntEquipmentBaseList, Common.Enum.EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntEquipmentBase> FreeEquipments { get; set; }

        public GetEquipmentListTestBlock()
        {
            FreeEquipments = new List<EntEquipmentBase>();
            GetEquipmentListRequest = new EntMsgGetEquipmentListRequest();
        }
        protected override void ExecuteBlock()
        {
            if(string.IsNullOrEmpty(EquipmentSerialNumber) == false) 
                GetEquipmentListRequest.SerialNumber = EquipmentSerialNumber;
            GetEquipmentListResponse = EquipmentProxy.Instance.GetEquipmentList(GetEquipmentListRequest);
            //find the unallocated rcvs
            foreach (var Equipment in GetEquipmentListResponse.EquipmentList)
            {
                if(Equipment.OffenderID.HasValue == false)
                {
                    FreeEquipments.Add(Equipment);
                }
            }
        }
    }


    public class GetEquipmentListTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.SerialNumber, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string SerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListRequest, EnumPropertyType.EntMsgGetEquipmentListRequest, EnumPropertyModifier.None)]
        public Equipment_1.EntMsgGetEquipmentListRequest GetEquipmentListRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, Common.Enum.EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        //public GetEquipmentListTestBlock_1()
        //{
        //    GetEquipmentListRequest = new Equipment_1.EntMsgGetEquipmentListRequest();
        //    GetEquipmentListResponse = new Equipment_1.EntMsgGetEquipmentListResponse();
        //}

        protected override void ExecuteBlock()
        {
            GetEquipmentListRequest = new Equipment_1.EntMsgGetEquipmentListRequest();
            GetEquipmentListRequest.SerialNumber = SerialNumber;
            GetEquipmentListResponse = EquipmentProxy_1.Instance.GetEquipmentList(GetEquipmentListRequest);
        }
    }


    public class GetEquipmentListTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.SerialNumber, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string SerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListRequest, EnumPropertyType.EntMsgGetEquipmentListRequest, EnumPropertyModifier.None)]
        public Equipment_2.EntMsgGetEquipmentListRequest GetEquipmentListRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, Common.Enum.EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        public GetEquipmentListTestBlock_2()
        {
            GetEquipmentListRequest = new Equipment_2.EntMsgGetEquipmentListRequest();
            GetEquipmentListResponse = new Equipment_2.EntMsgGetEquipmentListResponse();
        }
        protected override void ExecuteBlock()
        {
            GetEquipmentListRequest.SerialNumber = SerialNumber;
            GetEquipmentListResponse = EquipmentProxy_2.Instance.GetEquipmentList(GetEquipmentListRequest);
        }
    }
}
