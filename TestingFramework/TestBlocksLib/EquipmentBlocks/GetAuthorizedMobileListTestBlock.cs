﻿using Common.CustomAttributes;
using Common.Enum;
using System.ServiceModel;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.EquipmentBlocks
{
    public class GetAuthorizedMobileListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetAuthorizedMobileListRequest GetAuthorizedMobileListRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetAuthorizedMobileListResponse = EquipmentProxy.Instance.GetAuthorizedMobileList(GetAuthorizedMobileListRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
