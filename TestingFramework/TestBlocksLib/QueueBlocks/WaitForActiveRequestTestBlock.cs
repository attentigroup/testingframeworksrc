﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Queue;
using WebTests.SeleniumWrapper;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestBlocksLib.QueueBlocks
{
    public class WaitForActiveRequestTestBlock : AssertBaseBlock
    {
        public const int WAIT_TIME_FOR_REQUEST_TO_HANDLE_SECONDS = 2;

        [PropertyTest(EnumPropertyName.GetQueueRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueRequest GetQueueRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetQueueResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueResponse GetQueueResponse { get; set; }

        [PropertyTest(EnumPropertyName.Timeout, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int SecondsTimeOut { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntQueueProgramTracker, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Queue0.EntQueueProgramTracker EntQueueProgramTracker { get; set; }

        Queue0.EntMessageGetQueueResponse CurrentQueueResponse => QueueProxy.Instance.GetQueue(GetQueueRequest);

        public WaitForActiveRequestTestBlock()
        {
            SecondsTimeOut = 60;
        }
        protected override void ExecuteBlock()
        {
            Wait.Until(() => IsRequestStatusActive(), $"Request doesn't exist, or its status not active after timeout of {SecondsTimeOut} seconds", SecondsTimeOut);
        }

        private bool IsRequestStatusActive()
        {
            bool reqFound = false;
            foreach(var request in CurrentQueueResponse.RequestList)
            {
                EntQueueProgramTracker = (Queue0.EntQueueProgramTracker)request;
                Log.Debug($"tracker request params {PropertiesLogging.ToLog(EntQueueProgramTracker)}");
                if ((EntQueueProgramTracker.OffenderId == OffenderID) && 
                    (EntQueueProgramTracker.RequestStatus == Queue0.EnmRequestStatus.Active))
                {
                    reqFound = true;
                    break;
                }
            }
            return reqFound;
        }
    }
}
