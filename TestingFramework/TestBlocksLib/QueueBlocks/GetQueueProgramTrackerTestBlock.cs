﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Queue;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;

namespace TestBlocksLib.QueueBlocks
{
    public class GetQueueProgramTrackerTestBlock : GetQueueTestBlock
    {

        [PropertyTest(EnumPropertyName.QueueProgramTracker, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<Queue0.EntQueueProgramTracker> QueueProgramTracker { get; set; }


        protected override void ExecuteBlock()
        {

            base.ExecuteBlock();
            QueueProgramTracker = new List<Queue0.EntQueueProgramTracker>();
            foreach (var request in GetQueueResponse.RequestList)
            {
                QueueProgramTracker.Add(request as Queue0.EntQueueProgramTracker);
            }

        }
    }
}
