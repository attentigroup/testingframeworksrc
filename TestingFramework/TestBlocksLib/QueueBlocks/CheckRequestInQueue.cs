﻿using Common.CustomAttributes;
using Common.Enum;
using System.Threading;
using TestingFramework.Proxies.API.Queue;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
#endregion

namespace TestBlocksLib.QueueBlocks
{
    public class CheckRequestInQueue : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetQueueRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueRequest GetQueueRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetQueueResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Queue0.EntMessageGetQueueResponse GetQueueResponse { get; set; }


        protected override void ExecuteBlock()
        {

            GetQueueResponse = QueueProxy.Instance.GetQueue(GetQueueRequest);
        }
    }
}
