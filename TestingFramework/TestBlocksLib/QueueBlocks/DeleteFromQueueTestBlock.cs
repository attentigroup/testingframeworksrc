﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.API.Queue;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Queue;

#region API refs
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

#endregion

namespace TestBlocksLib.QueueBlocks
{
    /// <summary>
    /// Delete request from queue
    /// </summary>
    public class DeleteFromQueueTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetQueueRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueRequest GetQueueRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntQueueProgramTracker, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntQueueProgramTracker EntQueueProgramTracker { get; set; }

        protected override void ExecuteBlock()
        {
            bool res = QueueProxy.Instance.Delete(EntQueueProgramTracker.RequestID, GetQueueRequest.filterID, GetQueueRequest.resultCode);
        }
    }
}
