﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.Proxies.API.Users;
using Common.CustomAttributes;
using Common.Enum;

#region API refs
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion

namespace TestBlocksLib.UsersBlocks
{
    public class GetUsersListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetUsersListRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Users0.EntMsgGetUsersListRequest EntMsgGetUsersListRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetUsersListResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntMsgGetUsersListResponse EntMsgGetUsersListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetUsersListResponse = UsersProxy.Instance.GetUsersList(EntMsgGetUsersListRequest);

            if (EntMsgGetUsersListResponse == null || EntMsgGetUsersListResponse.UsersList.Length == 0)
            {
                throw new Exception("EntMsgGetUsersListResponse is empty!");
            }
        }
    }
}
