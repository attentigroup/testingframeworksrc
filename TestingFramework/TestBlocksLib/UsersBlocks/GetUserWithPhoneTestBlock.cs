﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;

namespace TestBlocksLib.UsersBlocks
{
    public class GetUserWithPhoneTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetUsersListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Users0.EntMsgGetUsersListResponse EntMsgGetUsersListResponse { get; set; }

        [PropertyTest(EnumPropertyName.UserID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntUser User { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var user in EntMsgGetUsersListResponse.UsersList)
            {
                if (user.Phone.PrefixPhone != null)
                    User = user; 
            }
            if (User == null)
                throw new Exception("No user with phone was found");
        }
    }
}
