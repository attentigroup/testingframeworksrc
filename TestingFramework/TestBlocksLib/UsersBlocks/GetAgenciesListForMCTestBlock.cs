﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.Proxies.API.Users;
using Common.CustomAttributes;
using Common.Enum;

#region API refs
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion

namespace TestBlocksLib.UsersBlocks
{
    public class GetAgenciesListForMCTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UserID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int UserID { get; set; }

        [PropertyTest(EnumPropertyName.GetAgenciesListForMCResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int[] GetAgenciesListForMCResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetAgenciesListForMCResponse = UsersProxy.Instance.GetAgenciesListForMC(UserID);
            if(GetAgenciesListForMCResponse == null || GetAgenciesListForMCResponse.Length == 0)
            {
                throw new Exception("GetAgenciesListForMCResponse is empty!");
            }
        }
    }
}
