﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using TestingFramework.Proxies.API.Users;
using Common.CustomAttributes;
using Common.Enum;

#region API refs
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion

namespace TestBlocksLib.UsersBlocks
{
    public class GetOfficerTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetOfficerRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Users0.EntMsgGetOfficerRequest EntMsgGetOfficerRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetOfficerResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntMsgGetOfficerResponse EntMsgGetOfficerResponse { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetOfficerResponse = UsersProxy.Instance.GetOfficer(EntMsgGetOfficerRequest);
        }
    }
}
