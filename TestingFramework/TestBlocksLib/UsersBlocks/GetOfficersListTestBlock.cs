﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Users;

namespace TestBlocksLib.UsersBlocks
{
    public class GetOfficersListTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.EntMsgGetOfficerRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Users0.EntMsgGetOfficersListRequest GetOfficersListRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetOfficerResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntMsgGetOfficersListResponse GetOfficersListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            GetOfficersListResponse = UsersProxy.Instance.GetOfficers(GetOfficersListRequest);
        }
    }
}
