﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace TestBlocksLib.ZonesBlocks
{
    public class GetZonesByEntityIDTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }

        protected override void ExecuteBlock()
        {            
            GetZonesByEntityIDResponse = ZonesProxy.Instance.GetZonesByEntityID(GetZonesByEntityIDRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class GetZonesByEntityIDTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetZonesByEntityIDResponse = ZonesProxy_1.Instance.GetZonesByEntityID(GetZonesByEntityIDRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class GetZonesByEntityIDTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }


        protected override void ExecuteBlock()
        {
            GetZonesByEntityIDResponse = ZonesProxy_2.Instance.GetZonesByEntityID(GetZonesByEntityIDRequest);
        }

        public override void CleanUp()
        {
        }
    }
}

