﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace TestBlocksLib.ZonesBlocks
{
    public class AddCircularZoneTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgAddZoneResponse AddZoneResponse { get; set; }


        protected override void ExecuteBlock()
        {
            AddZoneResponse = ZonesProxy.Instance.AddCircularZone(AddCircularZoneRequest);
        }

        public override void CleanUp()
        {
            var deleteZone = new Zones0.MsgDeleteZoneRequest();
            deleteZone.EntityID = AddCircularZoneRequest.EntityID;
            deleteZone.EntityType = AddCircularZoneRequest.EntityType;
            deleteZone.ZoneID = AddZoneResponse.ZoneID;
            ZonesProxy.Instance.DeleteZone(deleteZone);
        }
    }


    public class AddCircularZoneTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgAddZoneResponse AddZoneResponse { get; set; }


        protected override void ExecuteBlock()
        {
            AddZoneResponse = ZonesProxy_1.Instance.AddCircularZone(AddCircularZoneRequest);
        }

        public override void CleanUp()
        {
            var deleteZone = new Zones1.MsgDeleteZoneRequest();
            deleteZone.EntityID = AddCircularZoneRequest.EntityID;
            deleteZone.EntityType = AddCircularZoneRequest.EntityType;
            deleteZone.ZoneID = AddZoneResponse.ZoneID;
            ZonesProxy_1.Instance.DeleteZone(deleteZone);
        }
    }


    public class AddCircularZoneTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgAddZoneResponse AddZoneResponse { get; set; }


        protected override void ExecuteBlock()
        {
            AddZoneResponse = ZonesProxy_2.Instance.AddCircularZone(AddCircularZoneRequest);
        }

        public override void CleanUp()
        {
            var deleteZone = new Zones2.MsgDeleteZoneRequest();
            deleteZone.EntityID = AddCircularZoneRequest.EntityID;
            deleteZone.EntityType = AddCircularZoneRequest.EntityType;
            deleteZone.ZoneID = AddZoneResponse.ZoneID;
            ZonesProxy_2.Instance.DeleteZone(deleteZone);
        }
    }
}
