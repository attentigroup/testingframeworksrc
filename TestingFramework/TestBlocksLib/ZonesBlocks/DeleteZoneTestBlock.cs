﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace TestBlocksLib.ZonesBlocks
{
    public class DeleteZoneTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgDeleteZoneRequest DeleteZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy.Instance.DeleteZone(DeleteZoneRequest);
        }
    }


    public class DeleteZoneTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.MsgDeleteZoneRequest DeleteZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy_1.Instance.DeleteZone(DeleteZoneRequest);
        }
    }


    public class DeleteZoneTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.DeleteZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.MsgDeleteZoneRequest DeleteZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy_2.Instance.DeleteZone(DeleteZoneRequest);
        }
    }
}
