﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace TestBlocksLib.ZonesBlocks
{
    public class UpdatePolygonZoneTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy.Instance.UpdatePolygonZone(UpdatePolygonZoneRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdatePolygonZoneTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy_1.Instance.UpdatePolygonZone(UpdatePolygonZoneRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdatePolygonZoneTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy_2.Instance.UpdatePolygonZone(UpdatePolygonZoneRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
