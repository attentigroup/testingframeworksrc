﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace TestBlocksLib.ZonesBlocks
{
    public class UpdateCircularZoneTestBlock : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy.Instance.UpdateCircularZone(UpdateCircularZoneRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateCircularZoneTestBlock_1 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy_1.Instance.UpdateCircularZone(UpdateCircularZoneRequest);
        }

        public override void CleanUp()
        {
        }
    }


    public class UpdateCircularZoneTestBlock_2 : TestingBlockBase
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ZonesProxy_2.Instance.UpdateCircularZone(UpdateCircularZoneRequest);
        }

        public override void CleanUp()
        {
        }
    }
}
