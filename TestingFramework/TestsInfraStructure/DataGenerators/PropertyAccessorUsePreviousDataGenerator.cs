﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestingFramework.TestsInfraStructure.DataGenerators
{
    /// <summary>
    /// data generator that use property accessor to get the value
    /// </summary>
    public class PropertyAccessorUsePreviousDataGenerator : BaseUsePreviousDataGenerator, IDataGenerator
    {
        #region Properties
        /// <summary>
        /// will use to access the testing flow based on that property accessor
        /// </summary>
        public PropertyAccessor PropertyAccessor { get; set; }

        #endregion Properties

        #region Methods

        /// <summary>
        /// c'tor for numeric index of the previous block
        /// </summary>
        /// <param name="testingFlow">the testing flow</param>
        /// <param name="numericBlockIndex">the index</param>
        /// <param name="propertyAccessor">the property accessor to use</param>
        public PropertyAccessorUsePreviousDataGenerator(ITestingFlow testingFlow, int numericBlockIndex, PropertyAccessor propertyAccessor)
            : base(testingFlow, numericBlockIndex)
        {
            PropertyAccessor = propertyAccessor;
        }
        /// <summary>
        /// c'tor for string index of the previous block
        /// </summary>
        /// <param name="testingFlow">the testing flow</param>
        /// <param name="stringBlockIndex">the index</param>
        /// <param name="propertyAccessor">the property accessor to use</param>
        public PropertyAccessorUsePreviousDataGenerator(ITestingFlow testingFlow, string stringBlockIndex, PropertyAccessor propertyAccessor)
            : base(testingFlow, stringBlockIndex)
        {
            PropertyAccessor = propertyAccessor;
        }
        /// <summary>
        /// generate data for the current property based on the property accessor and the block index
        /// </summary>
        /// <param name="system">identification of the system for the data generator to generate data to the selected system</param>
        /// <returns>return the value of the property the <see cref="PropertyAccessor"/> indicate to</returns>
        public object GenerateData(string system)
        {
            int blockIndex = CalculateBlockIndex();
            var block = TestingFlow.Blocks[blockIndex] as IBlock;
            var result = PropertyAccessor.GetPropertyValue(block);
            return result;
        }
        #endregion Methods
    }
}
