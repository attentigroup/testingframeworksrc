﻿using Common.Enum;
using System;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.DataGenerators
{
    /// <summary>
    /// use previous data generator based on property name
    /// </summary>
    public class PropertyNameUsePreviousDataGenerator : BaseUsePreviousDataGenerator, IDataGenerator
    {
        #region Properties
        /// <summary>
        /// The data generator will return the value of the property as the property name
        /// </summary>
        public EnumPropertyName PropertyName { get; set; }

        #endregion Properties

        #region Methods
        /// <summary>
        /// C'tor for PropertyNameUsePreviousDataGenerator with int index for the block index
        /// </summary>
        /// <param name="testingFlow">the testing flow</param>
        /// <param name="numericBlockIndex">the index</param>
        /// <param name="propertyName">Property name of the property that the data generator will return the value of</param>
        public PropertyNameUsePreviousDataGenerator(ITestingFlow testingFlow, int numericBlockIndex, EnumPropertyName propertyName)
            : base(testingFlow, numericBlockIndex)
        {
            PropertyName = propertyName;
        }
        /// <summary>
        /// C'tor for PropertyNameUsePreviousDataGenerator with string index for the block index
        /// </summary>
        /// <param name="testingFlow">the testing flow</param>
        /// <param name="stringBlockIndex">the index</param>
        /// <param name="propertyName">Property name of the property that the data generator will return the value of</param>
        public PropertyNameUsePreviousDataGenerator(ITestingFlow testingFlow, string stringBlockIndex, EnumPropertyName propertyName)
            : base(testingFlow, stringBlockIndex)
        {
            PropertyName = propertyName;
        }
        /// <summary>
        /// generate data for the current property based on the property name and the block index
        /// </summary>
        /// <param name="system">identification of the system for the data generator to generate data to the selected system</param>
        /// <returns>return the value of the property the <see cref="PropertyName"/> indicate to in the block that was passed in the C'tors</returns>
        public object GenerateData(string system)
        {
            int blockIndex = CalculateBlockIndex();
            var result = TestingFlow.TestBlocksPropertiesData[PropertyName][blockIndex];
            return result;
        }

        #endregion Methods
    }
}
