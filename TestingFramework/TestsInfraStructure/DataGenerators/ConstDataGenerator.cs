﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.DataGenerators
{
    /// <summary>
    /// A constant data generator, 
    /// it doesn't really have a functionality but it act as a place holder for the constant value
    /// </summary>
    public class ConstDataGenerator : IDataGenerator
    {
        private readonly object ConstValue;
        /// <summary>
        /// c'tor for the const data generator. The C'tor get the const value and save it for later use
        /// </summary>
        /// <param name="constValue">the value that the data generator will return upon calling <see cref="GenerateData"/> method</param>
        public ConstDataGenerator(object constValue)
        {
            ConstValue = constValue;
        }
        /// <summary>
        /// the implementation of the IDataGenerator interface for const value.
        /// It return the const value that received at the C'tor
        /// </summary>
        /// <param name="system">identification of the system for the data generator to generate data to the selected system</param>
        /// <returns>the const value that received at the C'tor</returns>
        public object GenerateData(string system)
        {
            return ConstValue;
        }
    }
}
