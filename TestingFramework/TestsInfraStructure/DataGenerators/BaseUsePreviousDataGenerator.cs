﻿using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.DataGenerators
{
    /// <summary>
    /// base class for use previous data generators
    /// the base class will hold the index of the block(int or string) and the testing flow
    /// the inherited class will use this object as need to get the data
    /// </summary>
    public abstract class BaseUsePreviousDataGenerator
    {
        #region Properties

        /// <summary>
        /// used for the inherited class to access the data
        /// </summary>
        public ITestingFlow TestingFlow { get; set; }

        /// <summary>
        /// use to access the block by numeric index
        /// </summary>
        private int NumericBlockIndex { get; set; }
        /// <summary>
        /// use to access the block by string index
        /// </summary>
        private string StringBlockIndex { get; set; }

        #endregion Properties

        #region Methods
        /// <summary>
        /// basic c'tor for BaseUsePreviousDataGenerator.
        /// No inherited class need to use this c'tor because it get only the testing flow without any index
        /// </summary>
        /// <param name="testingFlow">the testing flow that the data generator will take the data from</param>
        private BaseUsePreviousDataGenerator(ITestingFlow testingFlow)
        {
            TestingFlow = testingFlow;
        }
        /// <summary>
        /// c'tor for numeric index of the previous block
        /// </summary>
        /// <param name="testingFlow">the testing flow that the data generator will take the data from</param>
        /// <param name="numericBlockIndex">int index of the block that the data generator will use to take the data</param>
        public BaseUsePreviousDataGenerator(ITestingFlow testingFlow, int numericBlockIndex)
            : this(testingFlow)
        {
            NumericBlockIndex = numericBlockIndex;
            StringBlockIndex = string.Empty;//not used in this instance
        }
        /// <summary>
        /// c'tor for string index of the previous block
        /// </summary>
        /// <param name="testingFlow">the testing flow that the data generator will take the data from</param>
        /// <param name="stringBlockIndex">string index of the block that the data generator will use to take the data</param>
        public BaseUsePreviousDataGenerator(ITestingFlow testingFlow, string stringBlockIndex)
            :this(testingFlow)
        {
            StringBlockIndex = stringBlockIndex;
            NumericBlockIndex = -1;//not used in this instance
        }
        /// <summary>
        /// return the block index in int format.
        /// In case string block index used the method convert the string index to int and return it
        /// </summary>
        /// <returns>int block index</returns>
        public int CalculateBlockIndex()
        {
            int result = NumericBlockIndex;
            if (StringBlockIndex != string.Empty)
            {
                result = TestingFlow.BlockNameToIndex(StringBlockIndex);
            }
            return result;
        }

        #endregion Methods

    }
}
