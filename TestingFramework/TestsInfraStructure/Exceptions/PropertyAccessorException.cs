﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// used to throw exceptions specific for property accessor 
    /// </summary>
    /// <example>
    /// 
    /// <code>
    /// public class PropertyAccessor
    /// {
    ///     ...
    ///     public void SetProperty(IBlock block, dynamic value)
    ///     {
    ///         ...
    ///         throw new PropertyAccessorException(this, string.Format("{0} type is not supported.", propertyType));
    ///     }
    /// }
    /// </code>
    /// </example>
    public class PropertyAccessorException : AssertFailedException
    {
        /// <summary>
        /// the property accessor that cause the exception
        /// </summary>
        public PropertyAccessor PropertyAccessor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyAccessor"></param>
        /// <param name="msg">message to display.This message will be override with specific <see cref="Message"/></param>
        /// <param name="innerEx">inner exception if exist</param>        
        public PropertyAccessorException(PropertyAccessor propertyAccessor, string msg, Exception innerEx = null) 
            : base(msg, innerEx)
        {
            PropertyAccessor = propertyAccessor;
        }
        /// <summary>
        /// override the default behavior of Message property in exception to reflect the Property Accessor Exception.
        /// </summary>
        public override string Message
        {
            get
            {
                var msg = string.Format("Property Accessor Exception: {0}. Message:{1}", PropertyAccessor.ToString(), base.Message);
                return msg;
            }
        }
    }
}
