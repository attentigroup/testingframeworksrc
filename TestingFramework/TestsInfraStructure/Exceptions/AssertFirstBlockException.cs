﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// Exception for special case of block from type assert that was add to the flow as the first block
    /// </summary>
    public class AssertFirstBlockException : BlockInitException
    {
        /// <summary>
        /// default c'tor for the AssertFirstBlockException that get the block as parameter
        /// </summary>
        /// <param name="block"></param>
        public AssertFirstBlockException(IBlock block)
            : base(block, "default message for AssertFirstBlockException")
        {

        }
        /// <summary>
        /// Gets a message that describes the error with the block that cause the exception.
        /// </summary>
        public override string Message
        {
            get
            {
                return string.Format("Can't add assert from type {0} because it is the first block in testing flow", Block.GetType().Name);
            }
        }

    }
}
