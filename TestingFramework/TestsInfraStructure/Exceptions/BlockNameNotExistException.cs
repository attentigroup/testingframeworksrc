﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// exception to use in case search block by name ant the block name not exist
    /// </summary>
    public class BlockNameNotExistException : BaseTestingFlowException
    {
        /// <summary>
        /// the block names that exist in the mapping
        /// </summary>
        public string[] BlockNames { get; set; }
        /// <summary>
        /// the name of the block that wasn't found
        /// </summary>
        public string NotExistBlockName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testingFlow"></param>
        /// <param name="msg"></param>
        /// <param name="innerEx"></param>
        public BlockNameNotExistException(ITestingFlow testingFlow, string msg, Exception innerEx = null) : base(testingFlow, msg, innerEx)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testingFlow"></param>
        /// <param name="blockNames"></param>
        /// <param name="notExistBlockName"></param>
        /// <param name="msg"></param>
        /// <param name="innerEx"></param>
        public BlockNameNotExistException(
            ITestingFlow testingFlow, 
            string[] blockNames, 
            string notExistBlockName, 
            string msg = "Block not exist default message", 
            Exception innerEx = null) :
            base(testingFlow, msg, innerEx)
        {
            BlockNames = blockNames;
            NotExistBlockName = notExistBlockName;
        }

        public override string Message
        {
            get
            {
                string blocksNameMapping = string.Join(Environment.NewLine, BlockNames);
                return $"Block name \'{NotExistBlockName}\' not exist in mapping({blocksNameMapping})";
            }
        }
    }
}
