﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// Exception will use to indicate errors while block run time.
    /// </summary>
    /// <example>
    /// 
    /// <code>
    /// protected override void ExecuteBlock()
    /// {
    ///     if( BlockException != null)
    ///     {
    ///         if( ExpectedType != null)
    ///         {
    ///             AssertType();
    ///             AssertMessage();
    ///         }
    ///         else
    ///         {
    ///             var errorMsg = "The expected type was not set!";
    ///             throw new <b>BlockRunTimeException</b>(this, errorMsg, BlockException);
    ///         }
    ///     }
    ///     else
    ///     {
    ///         throw new <b>BlockRunTimeException</b>(
    ///             this,
    ///             string.Format("Exception from previous block was expected, but no exception was received. ExpectedType={0} ExpectedMessage={1}",
    ///                 ExpectedType, ExpectedMessage));
    ///     }
    /// }
    /// </code>
    /// </example>
    public class BlockRunTimeException : BaseBlockException
    {
        /// <summary>
        /// C'tor to create instance of BlockRunTimeException
        /// </summary>
        /// <param name="msg">message to display</param>
        /// <param name="innerEx">inner exception if exist</param>
        public BlockRunTimeException(string msg, Exception innerEx = null) 
            : base( msg, innerEx)
        {
        }
        /// <summary>
        /// C'tor to create instance of BlockRunTimeException with block parameter
        /// </summary>
        /// <param name="block">the block that generate the exception</param>
        /// <param name="msg">message to display</param>
        /// <param name="innerEx">inner exception if exist</param>
        /// <example>
        /// in this example the <b>this parmeter</b> is the class that raise the exception and it is from type IBlock
        /// <code>
        /// public class BaseExceptionAssertBlock : AssertBaseBlock
        /// {
        ///     ...
        ///     protected override void ExecuteBlock()
        ///     {
        ///         ...
        ///             throw new BlockRunTimeException(<b>this</b>, errorMsg, BlockException);
        ///         ...
        ///     }
        /// }
        /// </code>
        /// </example>
        public BlockRunTimeException(IBlock block, string msg, Exception innerEx = null)
            : base(block, msg, innerEx)
        {
        }
    }
}
