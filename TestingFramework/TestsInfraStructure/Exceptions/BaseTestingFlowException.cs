﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// used for testing flow exceptions
    /// </summary>
    public class BaseTestingFlowException : AssertFailedException
    {
        /// <summary>
        /// the testing flow interface related to the excfeption
        /// </summary>
        public ITestingFlow TestingFlow { get; set; }

        /// <summary>
        /// cconstrator for clasic exception
        /// </summary>
        /// <param name="msg">the exception text message</param>
        /// <param name="innerEx">inner exception if exist</param>
        public BaseTestingFlowException(string msg, Exception innerEx = null)
            : base(msg, innerEx)
        {
        }

        public BaseTestingFlowException(ITestingFlow testingFlow, string msg, Exception innerEx = null)
            : base(msg, innerEx)
        {
            TestingFlow = testingFlow;
        }
    }
}
