﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    public class UsePreviousForFirstBlockException : BlockInitException
    {
        public UsePreviousForFirstBlockException(IBlock block)
            : base(block, "The block can't set \"Use Previous data\" on first block.")
        {
        }

        public override string Message
        {
            get
            {
                return string.Format("The block {0} can't set \"Use Previous data\" on first block.", Block.BlockName);
            }
        }
    }
}
