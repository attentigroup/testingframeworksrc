﻿using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// Special exception for case when mandatory fields wasn't set during the test for specific block
    /// </summary>
    public class MandatoryFieldNotSetException : AssertFailedException
    {
        /// <summary>
        /// List of the properties that mark as mandatory but wasn't set value
        /// </summary>
        public List<EnumPropertyName> PropertyNames { get; set; }
        /// <summary>
        /// the block that raise the exception
        /// </summary>
        public IBlock Block { get; set; }
        /// <summary>
        /// C'tor for MandatoryFieldNotSetException with block and list of property name as parameters
        /// </summary>
        /// <param name="block">the block that raise the exception</param>
        /// <param name="propertyNames">List of the properties that mark as mandatory but wasn't set value</param>
        /// <param name="msg">message to display.This message will be override with specific <see cref="Message"/></param>
        /// <param name="innerEx">inner exception if exist</param>
        public MandatoryFieldNotSetException(IBlock block, List<EnumPropertyName> propertyNames, string msg = null, Exception innerEx = null)
            : base(msg, innerEx)
        {
            Block = block;
            PropertyNames = propertyNames;
        }
        /// <summary>
        /// override the default behavior of Message property in exception to reflect 
        /// the block and all the fields that mandatory and wasn't set.
        /// </summary>
        public override string Message
        {
            get
            {
                string joinedMessages = string.Join(", ", PropertyNames.Select(x => x.ToString()));
                string msg = string.Format("Block {0} properties {1} are mandatory and wasn't set: ",
                    Block.BlockName, joinedMessages);

                return msg;
            }
        }
    }
}
