﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Factories;
using System.Linq;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// exception that indicate that the data generator not exist in the dictionary of the data generators.
    /// </summary>
    public class DataGeneratorNotExistException : AssertFailedException
    {
        public string NotExistDataGeneratorName { get; set; }

        public string DataGenerators { get; set; }
        public DataGeneratorNotExistException(string notExistDataGeneratorName, string msg, Exception innerEx = null)
               : base(msg, innerEx)
        {
            NotExistDataGeneratorName = notExistDataGeneratorName;
            //generate list of all data generators
            DataGenerators = string.Join(",", DataGeneratorFactory.Instance.DataGenerators.Keys.ToArray());
        }
        public override string Message
        {
            get
            {
                return $"data generator {NotExistDataGeneratorName} not exist in the list .Message = {base.Message}. " +
                    $"list of data generators:{DataGenerators}.";
            }
        }
    }
}
