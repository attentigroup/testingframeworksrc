﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// exception will raise in case of errors in property helper object, e.g. error while try to parse Lambda expression.
    /// </summary>
    public class PropertiesHelperException : AssertFailedException
    {
        /// <summary>
        /// LambdaExpression that cause the exception 
        /// </summary>
        public LambdaExpression PropertyNameExpression { get; set; }
        /// <summary>
        /// the property that cause the exception
        /// </summary>
        public PropertyInfo PropertyInfo { get; set; }

        /// <summary>
        /// C'tor for PropertiesHelperException that get the LambdaExpression as parameter
        /// </summary>
        /// <param name="propertyNameExpression">Lamdba expression that cause the error</param>
        /// <param name="msg">message to display.This message will be override with specific <see cref="Message"/></param>
        /// <param name="innerEx">inner exception if exist</param>        
        public PropertiesHelperException(LambdaExpression propertyNameExpression, string msg, Exception innerEx = null)
            : base(msg, innerEx)
        {
            PropertyNameExpression = propertyNameExpression;
        }

        /// <summary>
        /// C'tor for PropertiesHelperException that get the PropertyInfo as parameter
        /// </summary>
        /// <param name="propertyInfo">ProertyInfo that cause the error</param>
        /// <param name="msg">message to display.This message will be override with specific <see cref="Message"/></param>
        /// <param name="innerEx">inner exception if exist</param>        
        public PropertiesHelperException(PropertyInfo propertyInfo, string msg, Exception innerEx = null)
            : base(msg, innerEx)
        {
            PropertyInfo = propertyInfo;
        }

        /// <summary>
        /// override the default behavior of Message property in exception to reflect the Properties Helper Exception.
        /// </summary>
        public override string Message
        {
            get
            {
                string type = PropertyNameExpression != null ? "PropertyNameExpression" : "PropertyInfo";
                string value = PropertyNameExpression != null ? PropertyNameExpression.ToString() : PropertyInfo.ToString();
                string msg = string.Format("Properties Helper Exception {0} {1} ,Message:{2}",type, value, base.Message);
                return msg;
            }
        }
    }
}
