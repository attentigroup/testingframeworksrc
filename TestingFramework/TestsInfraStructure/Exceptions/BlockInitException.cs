﻿using System;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// Exception will use to indicate errors while block initialization.
    /// </summary>
    /// <example>
    /// This example shows how to use <see cref="BlockInitException"/> as base class for exceptions that raise during block initialize.
    /// <code>
    /// public class AssertFirstBlockException : <b>BlockInitException</b>
    /// {
    ///     public AssertFirstBlockException(IBlock block)
    ///         : <b>base</b>(block, "default message for AssertFirstBlockException")
    ///     {
    ///     }
    /// }
    /// </code>
    /// </example>
    public class BlockInitException : BaseBlockException
    {
        /// <summary>
        /// cconstrator for clasic exception
        /// </summary>
        /// <param name="msg">the exception text message</param>
        /// <param name="innerEx">inner exception if exist</param>
        public BlockInitException(string msg, Exception innerEx = null) 
            : base( msg, innerEx)
        {
        }
        /// <summary>
         /// constractor for the Block init exception that get the block asparameter
         /// </summary>
         /// <param name="block">the block that raise the error</param>
         /// <param name="msg">the exception text message</param>
         /// <param name="innerEx">inner exception if exist</param>
        public BlockInitException(IBlock block, string msg, Exception innerEx = null)
            : base(block, msg, innerEx)
        {
        }
    }
}
