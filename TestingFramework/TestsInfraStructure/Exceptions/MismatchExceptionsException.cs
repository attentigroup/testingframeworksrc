﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    public class MismatchExceptionsException : BlockRunTimeException
    {
        public Type Actual { get; set; }
        public Type Expected{ get; set; }
        public MismatchExceptionsException(IBlock block, Type actual, Type expected, Exception innerEx = null)
            :base(block, "Mismatch exceptions Exception Actual Type:{0} Expected Type: {1}", innerEx)
        {
            Actual = actual;
            Expected = expected;
        }


        public override string Message
        {
            get
            {
                var message = string.Format(base.Message, Actual.Name, Expected.Name);
                return message;
            }
        }
        
    }
}
