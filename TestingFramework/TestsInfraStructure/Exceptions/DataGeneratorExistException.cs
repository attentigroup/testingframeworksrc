﻿using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    public class DataGeneratorExistException : AssertFailedException
    {
        /// <summary>
        /// the property type of the data generator that exist in the dictionary of the data generators.
        /// </summary>
        public EnumPropertyType PropertyType { get; set; }


        public DataGeneratorExistException(EnumPropertyType propertyType, string msg = null, Exception innerEx = null)
               : base(msg, innerEx)
        {
            PropertyType = propertyType;
        }
        public override string Message
        {
            get
            {
                return $"data generator from type {PropertyType} already exist and can't load twice.";
            }
        }
    }
}
