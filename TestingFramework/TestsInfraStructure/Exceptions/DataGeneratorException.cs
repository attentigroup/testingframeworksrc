﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    public class DataGeneratorException : AssertFailedException
    {
        public IDataGenerator DataGenerator { get; set; }
        public DataGeneratorException(IDataGenerator dataGenerator, string msg, Exception innerEx = null)
               : base(msg, innerEx)
        {
            DataGenerator = dataGenerator;
        }
        public override string Message
        {
            get
            {
                return string.Format("data generator from type {0} failed : {1}",
                    DataGenerator.GetType(), base.Message);
            }
        }
    }
}
