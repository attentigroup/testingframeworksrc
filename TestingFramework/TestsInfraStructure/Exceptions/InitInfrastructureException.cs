﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// exception that will used in case of initialzation error in the framework
    /// </summary>
    /// <example>
    /// In this example DataGeneratorFactory initalize all data generators 
    /// and in case it failed to create instance of one of the data generators it throw <see cref="InitInfrastructureException"/>.
    /// <code>
    /// var dataGenerator = assmebly.CreateInstance(classType.FullName) as IDataGenerator;
    /// if (dataGenerator == null)
    /// {
    ///     var errMsg = string.Format("ClassType:{0} couldnt created as a dataGenerator", classType.FullName);
    ///     throw new <b>InitInfrastructureException</b>(errMsg);
    /// }
    /// </code>
    /// </example>
    public class InitInfrastructureException : AssertFailedException
    {
        /// <summary>
        /// C'tor for InitInfrastructureException that get the meesage and inner exception a parameters
        /// </summary>
        /// <param name="msg">message to display</param>
        /// <param name="innerEx">inner exception if exist</param>
        public InitInfrastructureException(string msg, Exception innerEx = null)
            : base(msg, innerEx)
        {
        }
    }
}
