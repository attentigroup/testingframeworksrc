﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// exception for special case when test try to set output property with value
    /// </summary>
    public class OutputFieldSetException : BlockInitException
    {
        /// <summary>
        /// the property that is output and was try to be setted
        /// </summary>
        public EnumPropertyType PropertyType { get; set; }
        /// <summary>
        /// the block that raise the exception
        /// </summary>
        public IBlock Block { get; set; }
        /// <summary>
        /// C'tor for OutputFieldSetException with the property type as parameters
        /// </summary>
        /// <param name="block">the block that raise the exception</param>
        /// <param name="propertyType">The property type enum that define as output and the test try to set a value to it</param>
        /// <param name="msg">message to display.This message will be override with specific <see cref="Message"/></param>
        /// <param name="innerEx">inner exception if exist</param>        
        public OutputFieldSetException(IBlock block, EnumPropertyType propertyType, string msg = null, Exception innerEx = null)
            : base(msg, innerEx)
        {
            Block = block;
            PropertyType = propertyType;
        }
        /// <summary>
        /// override the default behavior of Message property in exception to reflect the block and property.
        /// </summary>
        public override string Message
        {
            get
            {
                string msg = string.Format("Block {0} property {1} are output and set({2})",
                    Block.BlockName, PropertyType, base.Message);
                return msg;
            }
        }
    }
}
