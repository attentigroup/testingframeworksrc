﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Exceptions
{
    /// <summary>
    /// base exception class for all exceptions that related to block
    /// </summary>
    /// <example>
    /// This exmple shows how to use <see cref="BaseBlockException"/>
    /// <code>
    /// public class AssertFirstBlockException : BlockInitException
    /// {
    ///     public AssertFirstBlockException(IBlock block)
    ///         : base(block, "default message for AssertFirstBlockException")
    ///     {
    ///     }
    ///     public override string Message
    ///     {
    ///         get
    ///         {
    ///             return string.Format("Can't add assert from type {0} because it is the first block in testing flow", Block.GetType().Name);
    ///         }
    ///     }
    ///}
    /// </code>
    /// </example>
    public class BaseBlockException : AssertFailedException
    {
        /// <summary>
        /// the block that raise the error
        /// </summary>
        public IBlock Block { get; set; }

        /// <summary>
        /// cconstrator for clasic exception
        /// </summary>
        /// <param name="msg">the exception text message</param>
        /// <param name="innerEx">inner exception if exist</param>
        public BaseBlockException(string msg, Exception innerEx = null) 
            : base( msg, innerEx)
        {
        }
        /// <summary>
        /// constractor for the Block init exception that get the block asparameter
        /// </summary>
        /// <param name="block">the block that raise the error</param>
        /// <param name="msg">the exception text message</param>
        /// <param name="innerEx">inner exception if exist</param>
        public BaseBlockException(IBlock block, string msg, Exception innerEx = null)
            : base(msg, innerEx)
        {
            Block = block;
        }
    }
}
