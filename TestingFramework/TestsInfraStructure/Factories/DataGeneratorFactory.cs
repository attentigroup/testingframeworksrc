﻿using Common.Base;
using Common.CustomAttributes;
using Common.Enum;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Factories
{
    /// <summary>
    /// Responsible to the data grenerators creation and usage based on <see cref="EnumPropertyType"/>
    /// </summary>
    /// <example>
    /// This example shows how to get string data generator
    /// <code>
    /// var value2Set = DataGeneratorFactory.Instance.GenerateData(EnumPropertyType.String, string.Empty) as string;
    /// </code>
    /// This exmple shows how to get all properties that the DataGeneratorFactory holds <see cref="IDataGenerator"/> object for.
    /// <code>
    /// var dataGenerators = DataGeneratorFactory.Instance.DataGenerators;
    /// List&lt;EnumPropertyType&gt; properties = dataGenerators.Keys.ToList();
    /// 
    /// foreach (var property in PropertyTypes)
    /// {
    ///     try
    ///     {
    ///         var dataGenerator = DataGeneratorFactory.Instance.GetDataGenerator(property);
    ///         ...
    ///     }
    /// }
    /// </code>
    /// </example>
    public class DataGeneratorFactory
    {
        #region Properties
        //assembly and namespace for the environment presets
        private const string DataGeneratorsNamespaceConfigKey = "DataGeneratorsNamespace";
        private const string DataGeneratorsAssemblyNameConfigkey = "DataGeneratorsAssmeblyName";
        private static readonly Lazy<DataGeneratorFactory> m_Instance = new Lazy<DataGeneratorFactory>(() => new DataGeneratorFactory());
        /// <summary>
        /// Log interface for Data generators factory
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// The Singleton Design Pattern instance of DataGeneratorFactory
        /// </summary>
        public static DataGeneratorFactory Instance
        {
            get { return m_Instance.Value; }
        }

        private Dictionary<EnumPropertyType, IDataGenerator> dataGenerators;
        /// <summary>
        /// dictionary of all property types and the data generator that implemented for it.
        /// </summary>
        public Dictionary<EnumPropertyType, IDataGenerator> DataGenerators
        {
            get { return dataGenerators; }
        }
        /// <summary>
        /// RNG- Random Number Generator that used all data generators
        /// </summary>
        /// <example>
        /// This example shows how to get random value from topical <see cref="IDataGenerator.GenerateData(string)"/> Method
        /// <code>
        /// public object GenerateData(string system)
        /// {
        ///     return DataGeneratorFactory.Instance.RNG.NextDouble();
        /// }
        /// </code>
        /// </example>
        public Random RNG { get; private set; }

        #endregion Properties
        #region Methods
        /// <summary>
        /// constractor of the data generator factyory.
        /// after the c'tor finish to execute all data generators can be used by the tests.
        /// </summary>
        private DataGeneratorFactory()
        {
            InitDataGenerators();
        }
        /// <summary>
        /// initialize all data generators to be available for tests to use.
        /// </summary>
        private void InitDataGenerators()
        {
            dataGenerators = new Dictionary<EnumPropertyType, IDataGenerator>();
            SetDefaultSeed();
            UpdateDataGeneratorsListFromDll();
        }
        /// <summary>
        /// read all data generator from the dlls in the current directory with the name from configuration(app.config file, key:DataGeneratorsAssmeblyName)
        /// and classs from the namespace as in configuration(app.config file, key:DataGeneratorsNamespace).
        /// </summary>
        /// <example>
        /// app.config file:
        /// <code>
        /// <?xml version="1.0" encoding="utf-8"?>
        /// <configuration>
        ///     <appSettings>
        ///         ...
        ///         <add key="DataGeneratorsNamespace" value="DataGenerators" />
        ///         <add key = "DataGeneratorsAssmeblyName" value="DataGenerators" />
        ///         ...
        ///     </appSettings>
        /// </configuration>
        /// </code>
        /// </example>
        public void UpdateDataGeneratorsListFromDll()
        {
            Log.Info("start load all data generators from DLL");
            var dataGeneratrosAssemblyName = ConfigurationManager.AppSettings[DataGeneratorsAssemblyNameConfigkey];
            var dataGeneratorsNameSpace = ConfigurationManager.AppSettings[DataGeneratorsNamespaceConfigKey];

            Log.DebugFormat("Load data geberators from dll {0} using name space {1}", dataGeneratrosAssemblyName, dataGeneratorsNameSpace);
            AppDomain.CurrentDomain.Load(dataGeneratrosAssemblyName);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var assmebly = assemblies.First(x => x.FullName.StartsWith(dataGeneratrosAssemblyName));
            var dataGeneratrosClassesTypes = assmebly.GetTypes().Where(type => type.IsClass && type.Namespace != null && type.Namespace.Contains(dataGeneratorsNameSpace));
            foreach (var classType in dataGeneratrosClassesTypes)
            {
                var CompilerGenAttr = classType.GetCustomAttributes(typeof(CompilerGeneratedAttribute), true);
                if(CompilerGenAttr.Length > 0 )
                {
                    Log.DebugFormat("classType {0} will not load because because this is not real class", classType.ToString());
                    continue;
                }
                var dataGeneratorAttribute = classType.GetCustomAttributes(typeof(DataGeneratorAttribute), true);
                if (dataGeneratorAttribute.Length == 0)
                {
                    //TODO Log : continue because this is not data generator class
                    continue;
                }

                var dataGenerator = assmebly.CreateInstance(classType.FullName) as IDataGenerator;
                if (dataGenerator == null)
                {
                    var errMsg = string.Format("ClassType:{0} couldnt created as a dataGenerator", classType.FullName);
                    //TODO : m_Log.Error(errMsg);
                    throw new InitInfrastructureException(errMsg);
                }
                var propertyType = (dataGeneratorAttribute[0] as DataGeneratorAttribute).PropertyType;
                if (DataGenerators.ContainsKey(propertyType))
                {
                    throw new DataGeneratorExistException(propertyType);
                }
                else
                {
                    DataGenerators.Add(propertyType, dataGenerator);
                }
            }
            Log.Info("Finish load all data generators from DLL");
        }
        /// <summary>
        /// Set the default seed for the RNG to generate different value every run
        /// </summary>
        public void SetDefaultSeed()
        {
            //Int32 seed = (int)DateTime.Now.Ticks;
            Int32 seed = (int)DateTime.Now.Ticks & 0x0000FFFF;

            //string stringSeed = ConfigurationManager.AppSettings[DataGeneratorsSeedkey];

            //if (stringSeed != null)
            //{
            //    m_Log.Debug(String.Format("Seed:{0} was supplied from the app.config.", stringSeed));
            //    seed = Int32.Parse(stringSeed);
            //    seed += Int32.Parse(E2EMachineIP.Address.ToString().Substring(E2EMachineIP.Address.ToString().LastIndexOf('.') + 1));
            //    m_Log.Debug(string.Format("After adding ip seed was {0}", seed));
            //    UsingDefaultSeed = false;
            //}
            //else
            //{
            //    seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
            //    UsingDefaultSeed = true;
            //}

            SetSeed(seed);
        }
        /// <summary>
        /// this method give the option to set a specific seed 
        /// if the tests need to generate the same "random" values every run
        /// </summary>
        /// <param name="seed"></param>
        public void SetSeed(int seed)
        {
            RNG = new Random(seed);
        }
        /// <summary>
        /// generate data for the requested property
        /// </summary>
        /// <param name="propertyType">Property type to generate value for</param>
        /// <param name="system">identification of the system to generate value for</param>
        /// <returns>the generated value</returns>
        /// <example>
        /// <code>
        /// var value2Set = DataGeneratorFactory.Instance.<b>GenerateData</b>(EnumPropertyType.String, string.Empty) as string;
        /// </code>
        /// </example>
        public object GenerateData(EnumPropertyType propertyType, string system)
        {
            return DataGenerators[propertyType].GenerateData(system);
        }
        /// <summary>
        /// get the data generator based on property type
        /// </summary>
        /// <remarks>
        /// this method will use when the usage of the data generator need more parameters the system identification 
        /// so the blocks can use the data generator special interface by casting the IDataGenerator object to the 
        /// specific data generator that needed
        /// </remarks>
        /// <param name="propertyType"></param>
        /// <returns>reference from type IDataGenerator to the data generator</returns>
        /// <example>
        /// This example shows how to get specific data generator and use internal methods that not part of the interface.
        /// In this example the dat generator IntGenerator expose method <see cref="int GenerateData(int minValue, int maxValue)"/> 
        /// which is not part of the interface
        /// <code>
        /// var intGenerator = DataGeneratorFactory.Instance.<b>GetDataGenerator</b>(EnumPropertyType.Int) as <b><see cref="DataGenerators.IntGenerator"/></b>;
        /// var index2Set = intGenerator.GenerateData(24, 42);
        /// </code>
        /// </example>
        public IDataGenerator GetDataGenerator(EnumPropertyType propertyType)
        {
            return dataGenerators[propertyType];
        }
        #endregion Methods
    }
}
