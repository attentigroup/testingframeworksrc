﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Factories
{
    /// <summary>
    /// static class to create the testing flow instance from central factory.
    /// </summary>
    public static class TestingFlowFactory
    {
        /// <summary>
        /// getting the testing flow implementation based on testing flow type
        /// In case of adding new implementation of ITestingFlow it should be add to here with uniqe udentifier(string)
        /// </summary>
        /// <param name="testingFlowType">the type of the implemntation</param>
        /// <returns>Interface from type ITestingFlow to the newly created instance</returns>
        public static ITestingFlow GetTestingFlow(string testingFlowType = null)
        {
            ITestingFlow testingFlowInterface = null;

            switch (testingFlowType)
            {
                default:
                    testingFlowInterface = new TestsInfraStructure.Implementation.TestingFlowBase();
                    break;
            }

            return testingFlowInterface;
        }

        /// <summary>
        /// Get implementation of IBlocksExecuter based on block executer type.
        /// In case of adding new implementation of IBlocksExecuter it should be add to here with uniqe udentifier(string)
        /// </summary>
        /// <param name="blocksExecuterType">the type of the block-executer implementation</param>
        /// <returns>Interface from type IBlocksExecuter to the newly created instance</returns>
        public static IBlocksExecuter GetBlocksExecuter(string blocksExecuterType = null)
        {
            IBlocksExecuter testingFlowInterface = null;

            switch (blocksExecuterType)
            {
                default:
                    testingFlowInterface = new TestsInfraStructure.Implementation.BlocksExecuterBase();
                    break;
            }
            return testingFlowInterface;
        }

        public static ITransactionManager GetTransactionManager(string transactionManagerType, params object[] objects)
        {
            ITransactionManager transactionManager = null;

            switch (transactionManagerType)
            {
                default:
                    transactionManager = new TransactionManagerWithTestContext(objects);
                    break;
            }

            return transactionManager;
        }
    }
}
