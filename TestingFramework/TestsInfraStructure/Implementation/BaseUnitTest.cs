﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Repository.Hierarchy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;
using TP.AutomationTests.Infra.Common;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// base class for unit tests.
    /// This class holding the testing flow objectm, Log object, Assembly Initialize and cleanup and test initialize and cleanup.
    /// All tests must inherit from this class.
    /// </summary>
    /// <remarks>
    /// The inherit class must have <see cref="TestClassAttribute"/> attribute 
    /// and every test must be public void method without parameters with <see cref="TestMethodAttribute"/> atribute.
    /// for more information see <see href="!:https://docs.microsoft.com/en-us/visualstudio/test/walkthrough-creating-and-running-unit-tests-for-managed-code">walkthrough-creating-and-running-unit-tests-for-managed-code</see>
    /// </remarks>
    /// <example>
    /// This example shows how to use BaseUnitTest class
    /// <code>
    ///     [TestClass]
    ///     public class EquipmentSvcTests : BaseUnitTest
    ///     {
    ///         [TestMethod]
    ///         public void EquipmentSvcTests_GetEquipmentList_Success()
    ///         {
    ///             TestingFlow.
    ///                 AddBlock&lt;GetEquipmentListTestBlock&gt;().ShallGeneratePropertiesWithRandomValues().
    ///                     SetDescription("Get Equipment List Test Block").
    ///             ExecuteFlow();
    ///         }
    ///         ...
    /// }
    /// </code>
    /// </example>
    [TestClass]
    public class BaseUnitTest
    {
        #region Properties
        /// <summary>
        /// default implementation
        /// </summary>
        virtual public string TestingFlowType
        {
            get
            {
                return string.Empty;
            }
        }

        private ITestingFlow _testingFlow = null;
        /// <summary>
        /// the testing flow interface
        /// </summary>
        /// <example>
        /// This example shows how to change the default testing flow to different impleentation
        /// (assuming the different implementation allready ready and use <see cref="ITestingFlow"/> interface.
        /// <code>
        /// //the implementation of the 'different <see cref="BaseUnitTest"/>' class
        /// 
        /// public class DifferentBaseUntiTest : BaseUnitTest
        /// {
        ///     public override string TestingFlowType => "Different Implementation";
        ///     ...
        /// }
        /// 
        /// //the usage of 'different <see cref="BaseUnitTest"/>' class.
        /// //The TestingFlow property is from type "DifferentBaseUntiTest" and not from type <see cref="BaseUnitTest"/>.
        /// 
        /// [TestClass]
        /// public class DifferentBaseUnitTestClassTests : DifferentBaseUntiTest
        /// {
        ///     [TestMethod]
        ///     public void DifferentBaseUnitTest()
        ///     {
        ///        <b>TestingFlow</b>.ExecuteFlow();
        ///     }
        ///}
        /// </code>
        /// </example>
        public ITestingFlow TestingFlow { get { return _testingFlow; } }

        /// <summary>
        /// Used to store information that is provided to unit tests.
        /// see <see cref="TestInitialize()"/>.
        /// </summary>
        public TestContext TestContext { get; protected set; }

        /// <summary>
        /// Log interface for all tests
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// test start time stamp
        /// </summary>
        public DateTime TestStartTime { get; set; }

        #endregion Properties

        #region Class Method
        /// <summary>
        /// C'tor for base unit test.
        /// It intialize Testing flow as TestingFlowType parmeter.
        /// </summary>
        public BaseUnitTest()
        {
            _testingFlow = TestingFlowFactory.GetTestingFlow(TestingFlowType);
        }
        #endregion Class Methods

        #region Assembly methods
        /// <summary>
        /// The Assembly level methods must be implemented in the same class of the tests.
        /// see this link for more information: https://connect.microsoft.com/VisualStudio/feedback/details/1834204/test-executiion-will-not-run-assemblyinitialize-or-assemblycleanup-in-a-base-class
        /// This is the method that contains code to be used before all tests in the assembly
        /// have run and to allocate resources obtained by the assembly.
        /// </summary>
        /// <param name="testContext"></param>
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext testContext)
        {
            InitLog4Net();
            _assemblyStartTime = DateTime.Now;
        }

        private static DateTime _assemblyStartTime;
        

        private static void InitLog4Net()
        {
            //init log4net
            const string log4NetConfigFilePath = "log4netConfigFilePath";
            string log4NetConfigFile = ConfigurationManager.AppSettings[log4NetConfigFilePath];
            string logConfigPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, log4NetConfigFile);
            if (File.Exists(logConfigPath) == false)
            {
                string message = string.Format("log4Net config file {0} not exist,", logConfigPath);
                logConfigPath = Path.Combine(Environment.CurrentDirectory, log4NetConfigFile);
                if (File.Exists(logConfigPath) == false)
                {
                    message += string.Format(" and file {0} also not exist.", logConfigPath);
                    throw new FileNotFoundException(message, logConfigPath);
                }
            }
            var log4NetConfig = new FileInfo(logConfigPath);
            XmlConfigurator.ConfigureAndWatch(log4NetConfig);
            ThreadContext.Properties["ThreadId"] = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Log.Info("Starting");
        }

        /// <summary>
        /// The method that contains code to be used after all tests in the assembly
        /// have run and to free resources obtained by the assembly
        /// </summary>
        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            var assemblyTotalDuration = DateTime.Now - _assemblyStartTime;
            Log.InfoFormat("Total assembly run time duration: {0}", assemblyTotalDuration);
        }
        #endregion Assembly methods

        #region Test Methods
        /// <summary>
        /// The method that run before the test to allocate and configure resources
        /// needed by all tests in the test class.
        /// </summary>
        [TestInitialize]
        public void TestInitialize()
        {
            Log.Info("\r\n=========================\r\n>>>>>>>>>>START>>>>>>>>>>\r\n=========================\r\n");
            Log.Info($"\r\nTest location in: {TestContext.TestDir}");
            var calcTestName = TestContext != null ? TestContext.TestName : "\r\nTestContext is null(no test name)";

            ThreadContext.Properties["ThreadId"] = System.Threading.Thread.CurrentThread.ManagedThreadId;
            ThreadContext.Properties["TestName"] = calcTestName;
            ThreadContext.Properties["ForceAddResultFile"] = false;

            Log.InfoFormat("Test {0} start.", calcTestName);
            TestStartTime = DateTime.Now;

            _testingFlow.TransactionManager = TestingFlowFactory.GetTransactionManager(string.Empty, this.TestContext);

            WebTests.SeleniumWrapper.Logger.WriteLine("==========================*** Test Initialize Completed ***==========================");
        }
        /// <summary>
        /// method that contains code that must be used after the test has run
        /// and to free resources obtained by all the tests in the test class.
        /// The method calling the <see cref="ITestingFlow.Cleanup"/> method.
        /// </summary>
        [TestCleanup]
        public void TestCleanup()
        {
            var calcTestName = TestContext != null ? TestContext.TestName : "TestContext is null(no test name)";
            var testOutcome = TestContext != null ? TestContext.CurrentTestOutcome.ToString() : "No TestOutcome";
            Log.InfoFormat("Test {0} end with outcome {1}. starting cleanup process.", calcTestName, testOutcome);

            TestingFlow.Cleanup();

            Log.InfoFormat("Finish cleanup process for test {0}.", calcTestName);
            var testDuration = DateTime.Now - TestStartTime;
            Log.InfoFormat("Test {0} duration: {1}", calcTestName, testDuration);
            ThreadContext.Properties["TestName"] = string.Empty;
            Log.Info("\r\n======================\r\n<<<<<<<<<<END<<<<<<<<<\r\n======================\r\n");
            if ((bool)ThreadContext.Properties["ForceAddResultFile"] == true 
                ||TestContext.CurrentTestOutcome != UnitTestOutcome.Passed)
            {
                var rootAppender = ((Hierarchy)LogManager.GetRepository())
                                         .Root.Appenders.OfType<FileAppender>()
                                         .Where(x => x.File != null).FirstOrDefault();

                string filename = rootAppender != null ? rootAppender.File : string.Empty;
                if (string.IsNullOrEmpty(filename) == false)
                    TestContext.AddResultFile(filename);

                WebTests.SeleniumWrapper.Logger.Fail("Test failed general");
                var webLog = LoggerBase.GetFullPath();
                if (File.Exists(webLog))
                    TestContext.AddResultFile(webLog);
            }
        }
        #endregion Test Methods
    }
}
