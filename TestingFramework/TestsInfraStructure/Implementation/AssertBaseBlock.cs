﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// this class will be used for assert blocks
    /// (default parameters will be set to asserts behavior e.g. not generate random values and use previous properties)
    /// </summary>
    /// <example>
    /// In this example "VeryInmportent" assert class inherets from <see cref="AssertBaseBlock"/>
    /// <code>
    /// public class VeryInmportentAssert : AssertBaseBlock
    /// {
    ///     protected override void ExecuteBlock()
    ///     {
    ///     ...
    ///     }
    /// }
    /// </code>
    /// </example>
    public abstract class AssertBaseBlock : BlockBase
    {
        /// <summary>
        /// the block exception
        /// </summary>
        /// <remarks>
        /// The infrastructure will set automaticly the exception from Test-Block to Assert-Block.
        /// In test block the BlockException PropertyModifier is Output and in Assert-Block it is Mandatory.
        /// </remarks>
        /// <example>
        /// In this example the block executed and in case of fail it the the exception
        /// <code>
        /// private void RunBlock(IBlock block)
        /// {
        ///     try
        ///     {
        ///         block.RunBlock();
        ///     }
        ///     catch (BlockInitException)
        ///     {//exception was catch propery need to rethrow.
        ///         throw;
        ///     }
        ///     catch (AssertFailedException)
        ///     {//exception was catch propery need to rethrow.
        ///         throw;
        ///     }
        ///     catch (Exception ex)
        ///     {
        ///         block.<b>BlockException</b>= ex;
        ///         ...
        ///     }
        /// }
        /// </code>
        /// In this example in case of assert verify the type of the exception
        /// <code>
        /// public class BaseExceptionAssertBlock : AssertBaseBlock
        /// {
        ///     ...
        ///     protected void AssertType()
        ///     {
        ///         if (!(ExpectedType == BlockException.GetType()))
        ///         {
        ///             var errorMsg = string.Format("The exception was of type {0} and not type {1}", BlockException.GetType(), ExpectedType);
        ///             throw new BlockRunTimeException(this, errorMsg, BlockException);
        ///         }
        ///     }
        /// </code>
        /// </example>
        [PropertyTest(EnumPropertyName.BlockException, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public override Exception BlockException { get; set; }
        /// <summary>
        /// Default constractor for assert base class
        /// </summary>
        /// <remarks>
        /// By default ShallGeneratePropertiesWithRandomValues set to false and UsePreviousData set to true
        /// </remarks>
        public AssertBaseBlock()
        {
            ShallGeneratePropertiesWithRandomValues = false;
            UsePreviousData = true;
        }
    }
}
