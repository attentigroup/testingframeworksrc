﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// Implementation of the interface <see cref="ILevelOneBlockMethods"/> and <see cref="ILevelOneActionBlockMethods"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LevelOneActionBlockMethods<T> : LevelOneBlockMethods<T>, ILevelOneActionBlockMethods<T>
    {
        /// <summary>
        /// C'tor for LevelOneActionBlockMethods
        /// </summary>
        /// <param name="testingFlow">the testing flow</param>
        /// <param name="block">>the Block to work on</param>
        public LevelOneActionBlockMethods(ITestingFlow testingFlow, IBlock block)
            : base(testingFlow, block)
        {
        }

        /// <summary>
        /// instruct the framework to all cleanup method of the block or not.
        /// </summary>
        /// <param name="doCleanup">cleanup flag</param>
        /// <returns>ILevelOneActionBlockMethods&lt;T&gt; implementation(this class in this implementation)</returns>
        public ILevelOneActionBlockMethods<T> ShallDoCleanUp(bool doCleanup)
        {
            _block.DoCleanUp = doCleanup;
            return this;
        }
        /// <summary>
        /// Instruct the framework to generate values for unset properties
        /// Set the flag <see cref="IBlock.ShallGeneratePropertiesWithRandomValues"/> to true.
        /// </summary>
        /// <returns>ILevelOneActionBlockMethods&lt;T&gt; implementation(this class in this implementation)</returns>
        public ILevelOneActionBlockMethods<T> ShallGeneratePropertiesWithRandomValues()
        {
            _block.ShallGeneratePropertiesWithRandomValues = true;
            return this;
        }
        /// <summary>
        /// Instruct the framework to set the values for unset fileds with data from previous blocks
        /// </summary>
        /// <returns>ILevelOneActionBlockMethods&lt;T&gt; implementation(this class in this implementation)</returns>
        public ILevelOneActionBlockMethods<T> UsePreviousBlockData()
        {
            if( TestingFlow.Blocks.Count == 1 )
            {//first block - can't set To use previous
                throw new UsePreviousForFirstBlockException(_block);
                //throw new BlockInitException(_block, string.Format("The block {0} can't set \"Use Previous data\" on first block.", _block.BlockName);
            }
            _block.UsePreviousData = true;
            return this;
        }


    }
}
