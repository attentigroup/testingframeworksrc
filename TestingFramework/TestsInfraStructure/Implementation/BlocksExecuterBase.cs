﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using System.Reflection;
using Common.CustomAttributes;
using TestingFramework.TestsInfraStructure.DataGenerators;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using log4net;


namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// basic implementation for <see cref="IBlocksExecuter"/> interface.
    /// </summary>
    public class BlocksExecuterBase : IBlocksExecuter
    {
        #region Properties

        private ITestingFlow _testingFlow;
        /// <summary>
        /// The instance of <see cref="ITestingFlow"/> interface.
        /// will be used to iterate all blocks for execution.
        /// </summary>
        public ITestingFlow TestingFlow
        {
            get { return _testingFlow; }
            set { _testingFlow = value; }
        }

        private List<IBlock> _executedBlocks = new List<IBlock>();

        /// <summary>
        /// Log interface for block executer
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Properties

        #region Methods
        /// <summary>
        /// Implementation of <see cref="IBlocksExecuter.ExecuteBlocks(ITestingFlow)"/>.
        /// Using <see cref="ITestingFlow"/> to iterate all blocks
        /// </summary>
        /// <param name="testingFlow">The <see cref="ITestingFlow"/> object</param>
        public void ExecuteBlocks(ITestingFlow testingFlow)
        {
            _testingFlow = testingFlow;

            for (int i = 0; i < testingFlow.Blocks.Count; i++)
            {
                var block = testingFlow.Blocks[i] as IBlock;
                SetPropertiesValuesFromPreviousBlocks(block);
                GenerateRandomValuesForBlock(block);
                CheckMandatoryFields(block);
                SetProerties2Block(block);
                RunBlock(block);
                _executedBlocks.Add(block);
                UpdateBlockPropertiesInTestingFlow(block, i);
            }
        }
        /// <summary>
        /// set the values from the data generators to the properties
        /// </summary>
        /// <param name="block">the block that the properties will be set to</param>
        private void SetProerties2Block(IBlock block)
        {
            Log.DebugFormat("Set Properties to block {0}", block.BlockName);

            foreach (var dataGeneratorPropertyNameList in block.DataGenerators)
            {
                foreach (var expressionDataGeneratorKeyValue in dataGeneratorPropertyNameList.Value)
                {
                    var propertyAccessor = expressionDataGeneratorKeyValue.Key;
                    var value = expressionDataGeneratorKeyValue.Value.GenerateData(string.Empty);
                    Log.DebugFormat("Set property {0} with value {1}", propertyAccessor, value);
                    propertyAccessor.SetProperty(block, value);
                }
            }
        }

        private void CheckMandatoryFields(IBlock block)
        {
            var errorMsgBuilder = new StringBuilder();
            List<EnumPropertyName> PropertyNames = new List<EnumPropertyName>();

            foreach (var propertyInfo in block.PropertiesNameMapping.Values)
            {
                var propertyAttribute = PropertiesHelper.PropertyInfoToPropertyTestAttribute(propertyInfo);
                if (propertyAttribute.PropertyModifier == EnumPropertyModifier.Mandatory && //if the field is mandatory
                        !block.DataGenerators.ContainsKey(propertyAttribute.Name))          //and wasn't set
                {
                    errorMsgBuilder.AppendLine(String.Format("Mandatory field:{0} wasn't set.\n", propertyAttribute.Name));
                    PropertyNames.Add(propertyAttribute.Name);
                }
            }

            if (errorMsgBuilder.Length > 0)
            {
                throw new MandatoryFieldNotSetException(block, PropertyNames);
            }
        }

        /// <summary>
        /// Implementation of <see cref="IBlocksExecuter.CleanupExecution"/> method that will be called when <see cref="ITestingFlow"/> will call it.
        /// </summary>
        /// <returns></returns>
        public List<Exception> CleanupExecution()
        {
            Log.InfoFormat("Start cleanup {0} blocks.", _executedBlocks.Count);
            var exceptionsList = new List<Exception>();
            for (int i = _executedBlocks.Count - 1; i >= 0; i--)
            {
                var block = _executedBlocks[i];
                try
                {
                    if (block.DoCleanUp == false)
                    {
                        Log.DebugFormat("block name:{0} cleanup skipped because DoClenaup property = false.", block.BlockName);
                    }
                    else
                    {
                        Log.DebugFormat("block name:{0} cleanup start.", block.BlockName);
                        block.CleanUp();
                        Log.DebugFormat("block name:{0} cleanup end.", block.BlockName);
                    }
                }
                catch (Exception ex)
                {
                    Log.InfoFormat("block name:{0} cleanup exception:{1}", block.BlockName, ex.Message);
                    exceptionsList.Add(ex);
                }
            }
            return exceptionsList;
        }

        /// <summary>
        /// update the testing flow block properties list with the output values from current block
        /// </summary>
        /// <param name="block">the Block to work on</param>
        /// <param name="blockIndex">the index of the block</param>
        private void UpdateBlockPropertiesInTestingFlow(IBlock block, int blockIndex)
        {//for each property update the testing-flow with the properties
            foreach (var testBlockPropertyKeyValuePair in block.PropertiesNameMapping)
            {
                //if the property isn't mandatory we can continue to the next property
                var testPropertyModifier = PropertiesHelper.PropertyInfoToPropertyModifier(testBlockPropertyKeyValuePair.Value);
                //if (testPropertyModifier == EnumPropertyModifier.Output)
                //{
                //get the property name and value
                var propertyName = testBlockPropertyKeyValuePair.Key;
                var propertyValue = testBlockPropertyKeyValuePair.Value.GetValue(block, null);
                //find the property list of values per block
                SortedList<int, dynamic> listBlockIndex2Value;
                if (TestingFlow.TestBlocksPropertiesData.ContainsKey(propertyName) == false)
                {
                    TestingFlow.TestBlocksPropertiesData.Add(propertyName, new SortedList<int, dynamic>());
                }
                listBlockIndex2Value = TestingFlow.TestBlocksPropertiesData[propertyName];
                //update the correct block with the value
                if (listBlockIndex2Value.ContainsKey(blockIndex))
                {
                    listBlockIndex2Value[blockIndex] = propertyValue;
                }
                else
                {
                    listBlockIndex2Value.Add(blockIndex, propertyValue);
                }
                //}
            }
        }
        /// <summary>
        /// running the <see cref="IBlock.RunBlock"/> method and handle exceptions in case it happend 
        /// and need to pass to the next block(typicaly need to be from type <see cref="AssertBaseBlock"/>.
        /// </summary>
        /// <param name="block">the block need to run</param>
        private void RunBlock(IBlock block)
        {
            try
            {
                if (block.BeginTransaction)
                    _testingFlow.BeginTransaction(block.TransactionName);
                block.RunBlock();
                if (block.EndTransaction)
                    _testingFlow.EndTransaction(block.TransactionName);
                Log.Info("\r\n***Block Passed!***\r\n");
            }
            catch (BlockInitException)
            {//exception was catch propery need to rethrow.
                throw;
            }
            catch (AssertFailedException ex)
            {//exception was catch propery need to rethrow.
                WebTests.SeleniumWrapper.Logger.Fail($"Assert fail: {ex}");
                throw;
            }
            catch (Exception ex)
            {
                block.BlockException = ex;
                if (!block.RaiseException)
                {
                    //get the inner Most Exception message
                    var innerMostException = ex;
                    while (innerMostException.InnerException != null)
                    {
                        innerMostException = innerMostException.InnerException;
                    }
                    var errMsg = string.Format("Block:{0} failed because unexpected exception: {1} - innermost message {2}",
                        block.BlockName, ex.Message, innerMostException.Message);
                    Log.Error("\r\n\r\n (X) Error \r\n");
                    Log.Error(errMsg, ex);

                    WebTests.SeleniumWrapper.Logger.Fail($"{errMsg}");

                    throw new AssertFailedException(errMsg, ex);
                }
            }
        }
        /// <summary>
        /// run over all properties and in case need to generate value for the property the method assign <see cref="IDataGenerator"/> object 
        /// for the property by calling <see cref="IBlock.AddDataGenerator(PropertyAccessor, IDataGenerator)"/> method.
        /// </summary>
        /// <param name="block">the block need to generate values for</param>
        private void GenerateRandomValuesForBlock(IBlock block)
        {
            if (block.ShallGeneratePropertiesWithRandomValues == true)
            {
                var currentBlockProperties = block.PropertiesNameMapping;
                foreach (var testBlockProperty in currentBlockProperties.Values)
                {
                    var testingProperty = PropertiesHelper.PropertyInfoToPropertyTestAttribute(testBlockProperty);
                    var allreadyCreatedDataGenerator = block.DataGenerators.ContainsKey(testingProperty.Name);

                    if (allreadyCreatedDataGenerator == false)
                    {
                        var need2GenerateData = Need2GenerateData(testingProperty);
                        if (need2GenerateData)
                        {
                            if (DataGeneratorFactory.Instance.DataGenerators.ContainsKey(testingProperty.PropertyType) == false)
                            {
                                throw new BlockInitException(block,
                                    string.Format("block {0} generate random value for property name {1} from type {2} that the generator not exist in data generators list",
                                        block.BlockName, testingProperty.Name, testingProperty.PropertyType));
                            }
                            //get data generator
                            var dataGenerator = DataGeneratorFactory.Instance.GetDataGenerator(testingProperty.PropertyType);
                            //get the property info and property accessor to use in the block data generator lists
                            var propertyInfo = block.PropertiesNameMapping[testingProperty.Name];
                            //Create a propertyAccessor from the notDeepProperty constructor
                            var propertyAccessor = new PropertyAccessor(propertyInfo);
                            block.AddDataGenerator(propertyAccessor, dataGenerator);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// verify if the property need to generate random values for
        /// (for example in case it is Output no generator need to be assigned to the property).
        /// </summary>
        /// <param name="testingProperty"></param>
        /// <returns>true if need to assign data generator. false if not</returns>
        private bool Need2GenerateData(PropertyTestAttribute testingProperty)
        {
            bool result = true;

            switch (testingProperty.PropertyModifier)
            {
                case EnumPropertyModifier.None:
                case EnumPropertyModifier.Output:
                case EnumPropertyModifier.DontGenerateRandomValues:
                case EnumPropertyModifier.DontReceiveFromPreviousBlock:
                    result = false;
                    break;
            }
            return result;
        }
        /// <summary>
        /// in case of using previous values this method will run over all properties and use the last value from list 
        /// and set it to the property.
        /// </summary>
        /// <param name="block">the block to set values to</param>
        private void SetPropertiesValuesFromPreviousBlocks(IBlock block)
        {
            if (block.UsePreviousData == true)  
            {
                var currentBlockProperties = block.PropertiesNameMapping;
                foreach (var testBlockProperty in currentBlockProperties.Values)
                {
                    var testingProperty = PropertiesHelper.PropertyInfoToPropertyTestAttribute(testBlockProperty);
                    var need2TransferData = Need2TransferData(testingProperty, block);
                    var allreadyCreatedDataGenerator = block.DataGenerators.ContainsKey(testingProperty.Name);

                    if (need2TransferData && !allreadyCreatedDataGenerator)
                    {
                        //get the lastest data aviliable in the dataGenerator
                        var lastblockIndexOfProperty = TestingFlow.TestBlocksPropertiesData[testingProperty.Name].Keys.Last();
                        var dataGenerator = new PropertyNameUsePreviousDataGenerator(TestingFlow, lastblockIndexOfProperty, testingProperty.Name);
                        //get the property info and property accessor to use in the block data generator lists
                        var propertyInfo = block.PropertiesNameMapping[testingProperty.Name];
                        //Create a propertyAccessor from the notDeepProperty constructor
                        var propertyAccessor = new PropertyAccessor(propertyInfo);
                        block.AddDataGenerator(propertyAccessor, dataGenerator);
                    }
                }
            }
        }


        /// <summary>
        /// the methid check if the property need to transfer data to it from different block
        /// (for example output property not need to set value to)
        /// </summary>
        /// <param name="propertyTestAttribute"></param>
        /// <param name="block"></param>
        /// <returns>True if not need to transfer data to the property.
        /// False if not need to transfer data to the property.</returns>
        private bool Need2TransferData(PropertyTestAttribute propertyTestAttribute, IBlock block)
        {
            bool result = true;
            //if the property is output or set to don't receive no need to transfer to the current block
            switch (propertyTestAttribute.PropertyModifier)
            {
                case EnumPropertyModifier.Output:
                case EnumPropertyModifier.DontReceiveFromPreviousBlock:
                case EnumPropertyModifier.DontGenerateRandomValues:
                    result = false;
                    break;
            }
            //in case of first block the flow need to use prev data but no block to take it from so the result will be set to false
            if (result == true)
            {
                var prevBlockPropertiesContainsCurrentProperty = TestingFlow.TestBlocksPropertiesData.ContainsKey(propertyTestAttribute.Name);
                if (prevBlockPropertiesContainsCurrentProperty == false)
                {
                    Log.DebugFormat("block {0} need property {1} but no prev block to take it from", block.BlockName, propertyTestAttribute.Name);
                    result = false;
                }
            }
            return result;
        }
        #endregion Methods
    }
}
