﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Exceptions;
using log4net;
using System.Reflection;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// basinc implementation for ITestingFlow
    /// </summary>
    public class TestingFlowBase : ITestingFlow
    {
        #region Properties
        /// <summary>
        /// Log interface for block executer
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private List<BlockBase> blocks;

        IList ITestingFlow.Blocks
        {
            get
            {
                return blocks;
            }

            set
            {
                blocks = value as List<BlockBase>;
            }
        }

        private readonly Dictionary<EnumPropertyName, SortedList<int, dynamic>> testBlocksPropertiesData = new Dictionary<EnumPropertyName, SortedList<int, dynamic>>();
        /// <summary>
        /// Dictionary from EnumPropertyName to sorted list of block index and values
        /// used to save mapping between properties values across different blocks
        /// </summary>
        public Dictionary<EnumPropertyName, SortedList<int, dynamic>> TestBlocksPropertiesData
        {
            get
            {
                return testBlocksPropertiesData;
            }
        }
        internal Dictionary<string, List<int>> BlocksNameToBlockIndexMapping { get; private set; }
        /// <summary>
        /// reference to instance that implement IBlocksExecuter interface.
        /// Will use to execute the blocks
        /// </summary>
        public IBlocksExecuter BlocksExecuter { get; set; }
        public ITransactionManager TransactionManager { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// C'tor for TestingFlowBase class
        /// </summary>
        public TestingFlowBase()
        {
            Init();
            InitBlocExecuter();
        }

        private void InitBlocExecuter()
        {
            BlocksExecuter = TestingFlowFactory.GetBlocksExecuter();
        }

        private void Init()
        {
            blocks = new List<BlockBase>();
            BlocksNameToBlockIndexMapping = new Dictionary<string, List<int>>();            
        }

        /// <summary>
        /// adding block the the testing flow.
        /// The testing Flow responsible for creating the instance of the block
        /// </summary>
        /// <typeparam name="T">the type of the block</typeparam>
        /// <returns>Interface to ILevelOneActionBlockMethods in order perform actions on the block scope</returns>
        public ILevelOneActionBlockMethods<T> AddBlock<T>() where T : IBlock, new()
        {
            var newBlock = new T() as BlockBase;
            return AddBlock<T>(newBlock);
        }
        /// <summary>
        /// adding block the the testing flow.
        /// The caller responsible for creating the instance of the block
        /// </summary>
        /// <example>
        /// This example shows how to add block to the testing flow.
        /// <code>
        /// TestingFlow.
        ///     AddBlock&lt;AlwaysFailTestBlock&gt;().
        ///        AddBlock&lt;BaseExceptionAssertBlock&gt;(TestingFrameworkAssertsFactory.AssertNotImplementedException()).
        ///     ExecuteFlow();
        /// </code>
        /// </example>
        /// <typeparam name="T">the type of the block</typeparam>
        /// <param name="iBlock">the instance of the block</param>
        /// <returns>Interface to ILevelOneActionBlockMethods in order perform actions on the block scope</returns>
        /// <exception cref="AssertFirstBlockException">Thrown when the new block are from type assert and the first block in the flow</exception>
        public ILevelOneActionBlockMethods<T> AddBlock<T>(IBlock iBlock) where T : IBlock, new()
        {
            Log.DebugFormat("start add block {0} (description:{1}) to testing flow", iBlock.BlockName, iBlock.BlockDescription);
            var newBlock = iBlock as BlockBase;
            //if the block is AssertBaseBlock set the previous block RaiseException property to true 
            if (newBlock is AssertBaseBlock)
            {
                if (blocks.Count == 0)
                {
                    throw new AssertFirstBlockException(newBlock);

                }
                blocks.Last().RaiseException = true;
            }
            blocks.Add(newBlock);
            int newBlockIndex = blocks.Count-1;
            Log.DebugFormat("block {0} (description:{1}) index {2}", iBlock.BlockName, iBlock.BlockDescription, newBlockIndex);
            //adding the block to the mapping of name to index
            if (BlocksNameToBlockIndexMapping.ContainsKey(newBlock.BlockName) == false)
            {
                BlocksNameToBlockIndexMapping.Add(newBlock.BlockName, new List<int>());
            }
            var blocksIndexList = BlocksNameToBlockIndexMapping[newBlock.BlockName];
            blocksIndexList.Add(newBlockIndex);
            Log.DebugFormat("end add block {0} (description:{1}) to testing flow", iBlock.BlockName, iBlock.BlockDescription);
            return new LevelOneActionBlockMethods<T>(this, newBlock);
        }
        /// <summary>
        /// helper method to map between name index and int index of the block
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns>int index of the block</returns>
        /// <example>
        /// <code>
        /// result = TestingFlow.BlockNameToIndex(StringBlockIndex);
        /// </code>
        /// </example>
        public int BlockNameToIndex(string blockName)
        {
            int blockIndex = int.MinValue;
            List<int> indexList;

            if( BlocksNameToBlockIndexMapping.TryGetValue(blockName, out indexList) == true)
            {
                blockIndex = indexList[0];
                if( indexList.Count > 1)
                {
                    Log.WarnFormat("Block name {0} used in several blocks ({1})",blockName, string.Join(",", indexList.Select(x => x.ToString()).ToArray()));
                }
            }
            else
            {
                throw new BlockNameNotExistException(this, BlocksNameToBlockIndexMapping.Keys.ToArray(), blockName);
            }
            Log.DebugFormat("BlockNameToIndex for block name {0} return index{1}", blockName, blockIndex);
            return blockIndex;            
        }

        /// <summary>
        /// Method for running the testing flow by using the BlockExecuter object
        /// </summary>
        /// <returns>ITestingFlow of the testing flow</returns>
        public ITestingFlow ExecuteFlow()
        {
            BlocksExecuter.ExecuteBlocks(this);
            return this;
        }
        /// <summary>
        /// Method for running the cleanup flow by using the BlockExecuter object
        /// </summary>
        public void Cleanup()
        {
            BlocksExecuter.CleanupExecution();
        }

        /// <summary>
        /// add block name with it index for later usage.
        /// </summary>
        /// <param name="blockName">the block name</param>
        /// <param name="blockIndex">the block index</param>
        public void AddBlockIndex(string blockName, int blockIndex)
        {
            //adding the block to the mapping of name to index
            if (BlocksNameToBlockIndexMapping.ContainsKey(blockName) == false)
            {
                BlocksNameToBlockIndexMapping.Add(blockName, new List<int>());
            }
            var blocksIndexList = BlocksNameToBlockIndexMapping[blockName];
            blocksIndexList.Add(blockIndex);
        }

        /// <summary>
        /// the action of begin transaction
        /// </summary>
        /// <param name="transactionName"></param>
        public void BeginTransaction(string transactionName)
        {
            TransactionManager.BeginTransaction(transactionName);
        }
        /// <summary>
        /// the action of end transaction
        /// </summary>
        /// <param name="transactionName"></param>
        public void EndTransaction(string transactionName)
        {
            TransactionManager.EndTransaction(transactionName);
        }
        #endregion Methods
    }
}
