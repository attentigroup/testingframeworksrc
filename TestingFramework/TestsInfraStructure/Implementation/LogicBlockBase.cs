﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// this class will be used for logicl blocks - (for assert don't use this as base class)
    /// </summary>
    public abstract class LogicBlockBase : BlockBase
    {
    }
}
