﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using TestingFramework.TestsInfraStructure.Exceptions;
using Common.CustomAttributes;
using System.Collections.Concurrent;
using System.ComponentModel;
using log4net;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// abstract class for all blocks.
    /// implement <see cref="IBlock"/> interface.
    /// All common properties and functionality for <b>all blocks</b> need to implement here
    /// </summary>
    public abstract class BlockBase : IBlock
    {
        #region Properties
        /// <summary>
        /// the name of the block
        /// </summary>
        [PropertyTest(EnumPropertyName.BlockName, EnumPropertyType.String, EnumPropertyModifier.DontGenerateRandomValues)]
        public string BlockName { get; set; }
        /// <summary>
        /// the description of the block
        /// </summary>
        [PropertyTest(EnumPropertyName.BlockDescription, EnumPropertyType.String,EnumPropertyModifier.DontGenerateRandomValues)]
        public string BlockDescription { get; set; }
        /// <summary>
        /// Timestamp of the beginning of the execution of the block
        /// </summary>
        [PropertyTest(EnumPropertyName.BlockStartTime, EnumPropertyType.None, EnumPropertyModifier.DontGenerateRandomValues)]
        public DateTime BlockStartTime { get; set; }
        /// <summary>
        /// Timestamp of the end of the execution of the block
        /// </summary>
        [PropertyTest(EnumPropertyName.BlockEndTime, EnumPropertyType.None, EnumPropertyModifier.DontGenerateRandomValues)]
        public DateTime BlockEndTime { get; set; }
        /// <summary>
        /// A time interval that is equal to the duration of the block execution
        /// </summary>
        [PropertyTest(EnumPropertyName.BlockRunTime, EnumPropertyType.Double, EnumPropertyModifier.DontGenerateRandomValues)]
        public double BlockRunTime
        {
            get
            {
                return BlockEndTime.Subtract(BlockStartTime).TotalMilliseconds;

            }
        }
        /// <summary>
        /// The exception object of the block in case it raise exception.
        /// </summary>
        virtual public Exception BlockException { get; set; }
        /// <summary>
        /// A value that indicates whether the Block should save the exception thrown during the execution for later use
        /// </summary>
        public bool RaiseException { get; set; }
        /// <summary>
        /// A value that indicates whether the blocks cleanup method should be called by the testing framework
        /// </summary>
        public bool DoCleanUp { get; set; }
        /// <summary>
        /// A value that indicates whether the block executer should generate randon values for unset properties
        /// </summary>
        public bool ShallGeneratePropertiesWithRandomValues { get; set; }
        /// <summary>
        /// A value that indicates whether the block executer should use the last know value for each property and set it with that value
        /// </summary>
        public bool UsePreviousData { get; set; }
        /// <summary>
        /// Dictionary between property name and list of data generators that is actualy the values of the properties.
        /// </summary>
        public Dictionary<EnumPropertyName, SortedDictionary<PropertyAccessor, IDataGenerator>> DataGenerators { get; set; }
        /// <summary>
        /// Mapping of all block properties to <see cref="EnumPropertyName"/>.
        /// </summary>
        public Dictionary<EnumPropertyName, PropertyInfo> PropertiesNameMapping
        {
            get
            {
                return TestsBlocksPropertyNameMapping[GetType()];
            }
        }

        private static readonly PropertyAccessorComparer PropertyAccessorComparer = new PropertyAccessorComparer();

        /// <summary>
        ///This dictionary first key is the the type of the class, so we can save a Dictionary with key from type PropertyName and value from type PropertyInfo for each class 
        ///that inherit from this class,This member is static since we want the info to be at the class level instead of the instance level.
        /// </summary>
        private static readonly ConcurrentDictionary<Type, Dictionary<EnumPropertyName, PropertyInfo>> TestsBlocksPropertyNameMapping = 
            new ConcurrentDictionary<Type, Dictionary<EnumPropertyName, PropertyInfo>>();

        /// <summary>
        /// Log interface for all blocks
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// indicate if to start a transaction when the bloc start to run
        /// </summary>
        public bool BeginTransaction { get; set; }
        /// <summary>
        /// indicate if to end a transaction when the bloc finish to run
        /// </summary>
        public bool EndTransaction { get; set; }
        /// <summary>
        /// the transaction name
        /// </summary>
        public string TransactionName { get; set; }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Method to add data generator for property
        /// </summary>
        /// <param name="propertyAccessor">the parameters of the property(name,type and modifier type)</param>
        /// <param name="dataGenerator">object from type <see cref="IDataGenerator"/> to assign to the property</param>
        public void AddDataGenerator(PropertyAccessor propertyAccessor, IDataGenerator dataGenerator)
        {
            Log.DebugFormat("Add Data generator property accessor {0} data generator {1}", propertyAccessor, dataGenerator);
            var propertyHeadPropertyName = propertyAccessor.GetPropertyHeadPropertyName();

            //If the key does not exist it need to be created(a new SortedDictionary)
            if (!DataGenerators.ContainsKey(propertyHeadPropertyName))
            {
                DataGenerators.Add(propertyHeadPropertyName, 
                    new SortedDictionary<PropertyAccessor, IDataGenerator>(PropertyAccessorComparer));
            }
            var sortedDictionary = DataGenerators[propertyHeadPropertyName];
            if (sortedDictionary.ContainsKey(propertyAccessor))
            {
                throw new BlockInitException(String.Format("Error trying to set a data generator with the same path more then once. propertyAccessor={0}", propertyAccessor));
            }
            else
            {
                sortedDictionary.Add(propertyAccessor, dataGenerator);
            }
        }

        /// <summary>
        /// Block Clenaup implementation from <see cref="IBlock"/>
        /// </summary>
        virtual public void CleanUp()
        {
            //Should be called at implementation and not in base class.
        }

        /// <summary>
        /// Run Block implementation from <see cref="IBlock"/>
        /// </summary>
        public void RunBlock()
        {
            Log.InfoFormat("start block {0} execution.", BlockName);
            BlockStartTime = DateTime.Now;
            WritePropertiesToLog("before");
            ExecuteBlock();
            BlockEndTime = DateTime.Now;
            WritePropertiesToLog("after");
            Log.InfoFormat("End block {0} execution.duration {1} mili", BlockName, BlockRunTime);            
        }
        /// <summary>
        /// the method that will be implemented by the derived class
        /// </summary>
        abstract protected void ExecuteBlock();
        /// <summary>
        /// Default C'tor that initalize <see cref="BlockBase"/> instance
        /// </summary>
        public BlockBase()
        {
            Init();
        }

        private void Init()
        {
            //set default
            BlockName = GetType().Name;
            ShallGeneratePropertiesWithRandomValues = false;
            DoCleanUp = true;

            DataGenerators = new Dictionary<EnumPropertyName, SortedDictionary<PropertyAccessor, IDataGenerator>>();
            SetPropertiesMapForBlock();
        }
        /// <summary>
        /// Retrive all properties of the class and only the properties with <see cref="PropertyTestAttribute"/> will be mapped
        /// </summary>
        private void SetPropertiesMapForBlock()
        {
            //in case we use the same class the properties can't changed in run time so the map need to be done only once
            var currentBlockType = GetType();

            if(TestsBlocksPropertyNameMapping.ContainsKey(currentBlockType) == false)
            {
                var blockProperties = currentBlockType.GetProperties();
                var dictionary      = new Dictionary<EnumPropertyName, PropertyInfo>();

                foreach (var testBlockProperty in blockProperties)
                {
                    var testBlockPropertyAttrs = Attribute.GetCustomAttributes(testBlockProperty, typeof(PropertyTestAttribute));

                    foreach (var testBlockPropertyAttr in testBlockPropertyAttrs)
                    {
                        if (testBlockPropertyAttr is PropertyTestAttribute)
                        {
                            var enumPropertyName = (testBlockPropertyAttr as PropertyTestAttribute).Name;
                            dictionary[enumPropertyName] = testBlockProperty;
                        }
                    }
                }
                TestsBlocksPropertyNameMapping.TryAdd(currentBlockType, dictionary);
            }
        }
        /// <summary>
        /// Write the properties data to the log
        /// </summary>
        /// <param name="when">prefix for the data(before or after...)</param>
        public void WritePropertiesToLog(string when)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0} block values {1} executing tests:", BlockName, when);
            foreach (var testBlockPropertyTagPropertyInfo in PropertiesNameMapping)
            {
                sb.AppendFormat("{0} = {1} ,", 
                    testBlockPropertyTagPropertyInfo.Key, 
                    testBlockPropertyTagPropertyInfo.Value.GetValue(this, null).ToLog());
            }
            Log.Info(sb.ToString());
        }

        #endregion Methods
    }
}
