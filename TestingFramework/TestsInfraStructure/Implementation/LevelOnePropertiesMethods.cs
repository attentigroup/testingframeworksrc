﻿using Common.Enum;
using log4net;
using System;
using System.Reflection;
using TestingFramework.TestsInfraStructure.DataGenerators;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    public class LevelOnePropertiesMethods<T> : ILevelOnePropertiesMethods<T>
    {
        #region Properties

        private readonly ILevelOneBlockMethods<T> _levelOneBlockMethods;
        private readonly PropertyAccessor _propertyAccessor;
        private readonly IBlock _block;

        /// <summary>
        /// Log interface
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Properties

        #region Methods
        public LevelOnePropertiesMethods(ILevelOneBlockMethods<T> levelOneBlockMethods, PropertyAccessor propertyAccessor, IBlock block)
        {
            _levelOneBlockMethods = levelOneBlockMethods;
            _propertyAccessor = propertyAccessor;
            _block = block;
        }
        public ILevelOneBlockMethods<T> WithRandomGenerateValue()
        {
            try
            {
                EnumPropertyType propertyHeadPropertyType = _propertyAccessor.GetPropertyHeadPropertyType();
                var dataGenerator = DataGeneratorFactory.Instance.GetDataGenerator(propertyHeadPropertyType);
                _block.AddDataGenerator(_propertyAccessor, dataGenerator);
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error setting property:{0} for Block:{1} with random generate value", _propertyAccessor, _block.BlockName);
                var exception = new PropertyAccessorException(_propertyAccessor, msg, ex);
                Log.Error(msg, exception);
                throw exception;
            }
            return _levelOneBlockMethods;
        }

        public ILevelOneBlockMethods<T> WithValue(dynamic value)
        {
            try
            {
                var dataGenerator = new ConstDataGenerator(value);
                _block.AddDataGenerator(_propertyAccessor, dataGenerator);
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error setting property:{0} for Block:{1} with value:{2}", _propertyAccessor, _block.BlockName, value);
                var exception = new PropertyAccessorException(_propertyAccessor, msg, ex);
                Log.Error(msg, exception);
                throw exception;
            }
            return _levelOneBlockMethods;
        }

        public ILevelTwoPropertyMethods<T> WithValueFromBlockIndex(string blockName)
        {
            return new LevelTwoPropertyMethods<T>(_levelOneBlockMethods, _propertyAccessor, _block, blockName);
        }

        public ILevelOneBlockMethods<T> WithValueFromDifferentDataGenerator(string enumPropertyTypeName)
        {
            try
            {
                EnumPropertyType propertyType;
                Enum.TryParse(enumPropertyTypeName, out propertyType);
                //check if the data generator exist in the list.
                var contains = DataGeneratorFactory.Instance.DataGenerators.ContainsKey(propertyType);
                if( contains == false)
                {
                    throw new DataGeneratorNotExistException(propertyType.ToString(), "Data generator not exist");
                }
                var dataGenerator = DataGeneratorFactory.Instance.GetDataGenerator(propertyType);
                _block.AddDataGenerator(_propertyAccessor, dataGenerator);
            }
            catch(DataGeneratorNotExistException)
            {
                throw;
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error setting property:{0} for Block:{1} with from different data generator {2}", 
                    _propertyAccessor, _block.BlockName, enumPropertyTypeName);
                var exception = new PropertyAccessorException(_propertyAccessor, msg, ex);
                Log.Error(msg, exception);
                throw exception;
            }
            return _levelOneBlockMethods;
        }

        #endregion Methods
    }
}
