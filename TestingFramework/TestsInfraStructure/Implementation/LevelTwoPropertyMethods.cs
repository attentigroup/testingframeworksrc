﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;
using TestingFramework.TestsInfraStructure.DataGenerators;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using log4net;
using System.Reflection;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    public class LevelTwoPropertyMethods<T> : ILevelTwoPropertyMethods<T>
    {
        #region Properties

        private readonly ILevelOneBlockMethods<T> _levelOneBlockMethods;
        private readonly PropertyAccessor _propertyAccessor;
        private readonly IBlock _block;
        private readonly string sourceBlockName;

        /// <summary>
        /// Log interface
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Properties

        #region Methods

        private LevelTwoPropertyMethods(ILevelOneBlockMethods<T> levelOneBlockMethods, PropertyAccessor propertyAccessor, IBlock block)
        {
            _levelOneBlockMethods = levelOneBlockMethods;
            _propertyAccessor = propertyAccessor;
            _block = block;
        }

        public LevelTwoPropertyMethods(ILevelOneBlockMethods<T> levelOneBlockMethods, PropertyAccessor propertyAccessor, IBlock block, string blockName)
            : this(levelOneBlockMethods, propertyAccessor, block)
        {
            sourceBlockName = blockName;
        }

        public ILevelOneBlockMethods<T> WithTheSamePropertyName()
        {
            IDataGenerator dataGenerator;
            try
            {
                var propertyName = PropertiesHelper.PropertyInfoToPropertyName(_propertyAccessor.PropertyInfo);
                dataGenerator = new PropertyNameUsePreviousDataGenerator(_levelOneBlockMethods.TestingFlow, sourceBlockName, propertyName);
                Log.DebugFormat("LevelTwoPropertyMethods:WithTheSamePropertyName - add data generator {0} to property {1}",
                    dataGenerator.GetType(), _propertyAccessor);
                _block.AddDataGenerator(_propertyAccessor, dataGenerator);
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error setting property:{0} for Block:{1} from Block index:{2} with the same property",
                    _propertyAccessor, _block.BlockName, sourceBlockName);
                var exception = new PropertyAccessorException(_propertyAccessor, msg, ex);
                Log.Error(msg, exception);
                throw exception;
            }
            return _levelOneBlockMethods;
        }

        public ILevelOneBlockMethods<T> FromPropertyName(EnumPropertyName propertyName)
        {
            IDataGenerator dataGenerator;
            try
            {
                dataGenerator = new PropertyNameUsePreviousDataGenerator(_levelOneBlockMethods.TestingFlow, sourceBlockName, propertyName);
                Log.DebugFormat("LevelTwoPropertyMethods:FromPropertyName - add data generator {0} from block {1} property {2} to property {3}",
                    dataGenerator.GetType(), sourceBlockName, propertyName, _propertyAccessor);
                _block.AddDataGenerator(_propertyAccessor, dataGenerator);
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error setting property:{0} for Block:{1} from Block index:{2} with the property {3}",
                    _propertyAccessor, _block.BlockName, sourceBlockName, propertyName);
                //TODO : m_Log.Error(msg, ex);
                throw new PropertyAccessorException(_propertyAccessor, msg, ex);
            }
            return _levelOneBlockMethods;
        }
        /// <summary>
        /// getting property accessor from the source property and pass it to <see cref="PropertyAccessorUsePreviousDataGenerator"/>.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="sourceProperty"></param>
        /// <returns></returns>
        public ILevelOneBlockMethods<T> FromDeepProperty<T1>(System.Linq.Expressions.Expression<Func<T1, object>> sourceProperty) where T1 : IBlock
        {
            try
            {
                var propertyAccessor = new PropertyAccessor(sourceProperty);
                IDataGenerator dataGenerator = new PropertyAccessorUsePreviousDataGenerator(_levelOneBlockMethods.TestingFlow, sourceBlockName, propertyAccessor);
                Log.DebugFormat("LevelTwoPropertyMethods:FromDeepProperty - add data generator {0} from block {1} property {2} (source expression {3} ) to property {4}",
                    dataGenerator.GetType(), sourceBlockName, propertyAccessor, sourceProperty, _propertyAccessor);
                _block.AddDataGenerator(_propertyAccessor, dataGenerator);
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error setting property:{0} for Block:{1} from Block index:{2} property:{3}", 
                    _propertyAccessor, _block.BlockName, sourceBlockName, sourceProperty);
                var exception = new PropertyAccessorException(_propertyAccessor, msg, ex);
                Log.Error(msg, exception);
                throw exception;
            }
            return _levelOneBlockMethods;
        }
        #endregion Methods
    }
}
