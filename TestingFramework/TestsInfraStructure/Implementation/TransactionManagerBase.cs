﻿using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Reflection;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// base transaction manager will use Microsoft.VisualStudio.TestTools.UnitTesting.TestContext as the transaction manager
    /// </summary>
    public class TransactionManagerWithTestContext : ITransactionManager
    {
        /// <summary>
        /// the test context of microsoft that will implement the transaction management
        /// </summary>
        public TestContext TestContext { get; protected set; }

        /// <summary>
        /// Log interface for all tests
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// default ctor for later usage
        /// </summary>
        public TransactionManagerWithTestContext(){}
        /// <summary>
        /// parameterized c'tor that the first object is the testContext
        /// </summary>
        /// <param name="objects"></param>
        public TransactionManagerWithTestContext(params object[] objects)
        {
            if (objects[0] is TestContext)
            {
                TestContext = objects[0] as TestContext;
                if ((null == TestContext) || !TestContext.Properties.Contains("$LoadTestUserContext"))
                {
                    Log.Debug("test context does not support transactions");
                    TestContext = null;
                }
                else
                {
                    Log.Debug("TestContext.Properties.Contains $LoadTestUserContext");
                }
            }
                
        }
        /// <summary>
        /// implmentation of the interface method from ITransactionMAnager
        /// </summary>
        /// <param name="transactionName">the transaction name to begin</param>
        public void BeginTransaction(string transactionName)
        {
            if(TestContext != null)
                TestContext.BeginTimer(transactionName);
        }
        /// <summary>
        /// implmentation of the interface method from ITransactionMAnager
        /// </summary>
        /// <param name="transactionName">the transaction name to end</param>
        public void EndTransaction(string transactionName)
        {
            if (TestContext != null)
                TestContext.EndTimer(transactionName);
        }
    }
}
