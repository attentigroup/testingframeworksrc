﻿using Common.Enum;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    public class LevelOneBlockMethods<T> : ILevelOneBlockMethods<T>
    {
        #region Properties

        internal IBlock _block;

        private ITestingFlow _testingFlow;
        public ITestingFlow TestingFlow
        {
            get
            {
                return _testingFlow;
            }
        }

        /// <summary>
        /// Log interface
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Properties

        #region Methods
        public LevelOneBlockMethods(ITestingFlow testingFlow, IBlock block)
        {
            _testingFlow = testingFlow;
            _block = block;
        }
        public ILevelOneActionBlockMethods<T1> AddBlock<T1>() where T1 : IBlock, new()
        {
            return TestingFlow.AddBlock<T1>();
        }
        public ILevelOneActionBlockMethods<T1> AddBlock<T1>(IBlock iBlock) where T1 : IBlock, new()
        {
            return TestingFlow.AddBlock<T1>(iBlock);
        }
        public ITestingFlow ExecuteFlow()
        {
            return _testingFlow.ExecuteFlow();
        }

        public ILevelOneBlockMethods<T> SetName(string name)
        {
            _block.BlockName = name;
            _testingFlow.AddBlockIndex(name, _testingFlow.Blocks.Count - 1);
            return this;
        }
        public ILevelOneBlockMethods<T> SetDescription(string description)
        {
            _block.BlockDescription = description;
            return this;
        }
        public ILevelOnePropertiesMethods<T> SetProperty(Expression<Func<T, object>> destinationProperty)
        {
            Log.DebugFormat("Set property with expression {0} ( block {1} )", destinationProperty, _block.BlockName);

            var propertyAccessor = new PropertyAccessor(destinationProperty);
            var pta = PropertiesHelper.PropertyInfoToPropertyTestAttribute(propertyAccessor.PropertyInfo);

            if (pta.PropertyModifier == EnumPropertyModifier.Output)
            {
                var msg = string.Format(
                    "A SetProperty mathod called on a property that is marked as output modifier property:{0} and the testblock is:{1}.",
                    propertyAccessor, _block.BlockName);
                Log.Error(msg);
                throw new OutputFieldSetException(_block, pta.PropertyType, msg);
            }

            ILevelOnePropertiesMethods<T> levelOnePropertiesMethods = new 
                LevelOnePropertiesMethods<T>(this, propertyAccessor, _block);
            return levelOnePropertiesMethods;
        }

        public ILevelOneBlockMethods<T> BeginTransaction(string transactionName)
        {
            _block.BeginTransaction = true;
            _block.TransactionName = transactionName;
            return this;
        }

        public ILevelOneBlockMethods<T> EndTransaction(string transactionName)
        {
            _block.EndTransaction = true;
            _block.TransactionName = transactionName;
            return this;
        }


        #endregion Methods
    }
}
