﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Implementation
{
    /// <summary>
    /// this class will be used for testing blocks - (for assert don't use this as base class)
    /// </summary>
    public abstract class TestingBlockBase : BlockBase
    {
        /// <summary>
        /// In case of negative tests the block supposed to hold the exception that throwen by the block
        /// </summary>
        [PropertyTest(EnumPropertyName.BlockException, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public override Exception BlockException { get; set; }

    }
}
