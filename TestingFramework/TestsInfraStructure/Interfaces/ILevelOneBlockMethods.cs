﻿using System;
using System.Linq.Expressions;

namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for the level of the blocks in the testing flow
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILevelOneBlockMethods<T>
    {
        #region Properties
        /// <summary>
        /// The testing flow will use to add the block
        /// </summary>
        ITestingFlow TestingFlow { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// add block to the testing flow
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <returns>ILevelOneActionBlockMethods</returns>
        ILevelOneActionBlockMethods<T1> AddBlock<T1>() where T1 : IBlock, new();
        /// <summary>
        /// add existing bloc to the testing flow
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="iBlock"></param>
        /// <returns></returns>
        ILevelOneActionBlockMethods<T1> AddBlock<T1>(IBlock iBlock) where T1 : IBlock, new();
        /// <summary>
        /// set the block name
        /// </summary>
        /// <param name="name">the name of the block - used for addresing secific block to retrive parameters</param>
        /// <returns>ILevelOneBlockMethods</returns>
        ILevelOneBlockMethods<T> SetName(string name);
        /// <summary>
        /// set description for the block
        /// </summary>
        /// <param name="description">the description of the bloc - used for better logs in case of using the same block for different purpose</param>
        /// <returns>ILevelOneBlockMethods</returns>
        ILevelOneBlockMethods<T> SetDescription(string description);
        /// <summary>
        /// set property in the block with value
        /// </summary>
        /// <param name="destinationProperty"></param>
        /// <returns>ILevelOnePropertiesMethods</returns>
        ILevelOnePropertiesMethods<T> SetProperty(Expression<Func<T, object>> destinationProperty);
        /// <summary>
        /// Begin transaction
        /// </summary>
        /// <param name="transactionName"></param>
        /// <returns>ILevelOneBlockMethods</returns>
        ILevelOneBlockMethods<T> BeginTransaction(string transactionName);
        /// <summary>
        /// End transaction
        /// </summary>
        /// <param name="transactionName"></param>
        /// <returns>ILevelOneBlockMethods</returns>
        ILevelOneBlockMethods<T> EndTransaction(string transactionName);
        /// <summary>
        /// run blocks by addition order
        /// </summary>
        /// <returns>ITestingFlow</returns>
        ITestingFlow ExecuteFlow();

        #endregion Methods
    }
}
