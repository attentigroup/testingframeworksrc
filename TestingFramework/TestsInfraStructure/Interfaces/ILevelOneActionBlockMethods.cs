﻿namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for the level of actions that can perform on blocks
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILevelOneActionBlockMethods<T> : ILevelOneBlockMethods<T>
    {
        /// <summary>
        /// Instruct the framework to set the values for unset fileds with data from previous blocks
        /// </summary>
        /// <returns></returns>
        ILevelOneActionBlockMethods<T> UsePreviousBlockData();
        /// <summary>
        /// Instruct the framework to generate values for unset properties
        /// </summary>
        /// <returns></returns>
        ILevelOneActionBlockMethods<T> ShallGeneratePropertiesWithRandomValues();
        /// <summary>
        /// instruct the framework to all cleanup method of the block or not.
        /// </summary>
        /// <param name="doCleanup">cleanup flag</param>
        /// <returns>ILevelOneActionBlockMethods&lt;T&gt; implementation</returns>
        ILevelOneActionBlockMethods<T> ShallDoCleanUp(bool doCleanup);
    }
}