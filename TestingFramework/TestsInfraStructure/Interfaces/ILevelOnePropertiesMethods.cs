﻿namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for the level of the block properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILevelOnePropertiesMethods<T>
    {
        /// <summary>
        /// Set generateed value according to the refelection of the property
        /// </summary>
        /// <returns></returns>
        ILevelOneBlockMethods<T> WithRandomGenerateValue();
        /// <summary>
        /// Set value to the destination property with specific value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        ILevelOneBlockMethods<T> WithValue(dynamic value);
        /// <summary>
        /// Set value to the destination property with value from the same property from specific block
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        ILevelTwoPropertyMethods<T> WithValueFromBlockIndex(string blockName);


        ILevelOneBlockMethods<T> WithValueFromDifferentDataGenerator(string enumPropertyType);


        
    }
}