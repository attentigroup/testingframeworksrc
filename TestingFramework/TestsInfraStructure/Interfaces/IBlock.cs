﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// Interface for the basic unit from which test is built up
    /// </summary>
    public interface IBlock
    {
        #region Properties
        /// <summary>
        /// the name of the block. used for logging and refrence from other blocks
        /// </summary>
        string BlockName { get; set; }
        /// <summary>
        /// the description of the block.used to make different between more the one blocke 
        /// from the same type that used in the same test
        /// </summary>
        string BlockDescription { get; set; }
        /// <summary>
        /// the start time stamp of the block
        /// </summary>
        DateTime BlockStartTime { get; set; }
        /// <summary>
        /// the end time stamp of the block
        /// </summary>
        DateTime BlockEndTime { get; set; }
        /// <summary>
        /// block run time in miliseconds
        /// </summary>
        double BlockRunTime { get; }
        /// <summary>
        /// the exception that was raised by the block.use for the assert to verify the 
        /// exception was raised is the expected exception
        /// </summary>
        Exception BlockException { get; set; }
        /// <summary>
        /// indicate that the block need to rais exception to the next block
        /// </summary>
        bool RaiseException { get; set; }

        /// <summary>
        /// Mapping the properties of the block so other blocks could reach the Property Info by EnumPropertyName
        /// </summary>
        Dictionary<EnumPropertyName, PropertyInfo> PropertiesNameMapping { get; }
        /// <summary>
        /// Mapping the generators for the block for every property name
        /// </summary>
        Dictionary<EnumPropertyName, SortedDictionary<PropertyAccessor, IDataGenerator>> DataGenerators { get; set; }
        /// <summary>
        /// Instruct the framework if to clean this block
        /// </summary>
        bool DoCleanUp { get; set; }
        /// <summary>
        /// indicate the framework to generate random values for all properties that not set with value
        /// </summary>
        bool ShallGeneratePropertiesWithRandomValues { get; set; }
        /// <summary>
        /// A flag to mark if the flow should pass data from previous block to the current block
        /// </summary>
        bool UsePreviousData { get; set; }
        /// <summary>
        /// indicate if to start a transaction when the bloc start to run
        /// </summary>
        bool BeginTransaction { get; set; }
        /// <summary>
        /// indicate if to end a transaction when the bloc finish to run
        /// </summary>
        bool EndTransaction { get; set; }
        /// <summary>
        /// the transaction name
        /// </summary>
        string TransactionName { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// The one and only method that actualy execute the block
        /// </summary>
        void RunBlock();
        /// <summary>
        /// the cleanup of the block.
        /// Should be called at the end of the flow and clean up all changes the test block did
        /// </summary>
        void CleanUp();
        /// <summary>
        /// add data generator for property
        /// </summary>
        /// <param name="propertyAccessor"></param>
        /// <param name="dataGenerator"></param>
        void AddDataGenerator(PropertyAccessor propertyAccessor, IDataGenerator dataGenerator);

        #endregion Methods
    }
}
