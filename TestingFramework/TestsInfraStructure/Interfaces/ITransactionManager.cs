﻿namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for transaction management within a test
    /// </summary>
    public interface ITransactionManager
    {
        void BeginTransaction(string transactionName);

        void EndTransaction(string transactionName);
    }
}
