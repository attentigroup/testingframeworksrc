﻿using Common.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for testing flow
    /// </summary>
    public interface ITestingFlow
    {
        #region Properties
        /// <summary>
        /// List of all blocks in the test flow
        /// </summary>
        IList Blocks { get; set; }
        
        
        /// <summary>
        /// mapping of all values in the test flow the access is based on property type and block index(int)
        /// </summary>
        Dictionary<EnumPropertyName, SortedList<int, dynamic>> TestBlocksPropertiesData { get; }
        
        
        /// <summary>
        /// interface to block executer object
        /// </summary>
        IBlocksExecuter BlocksExecuter { get; set; }

        /// <summary>
        /// interface to the transaction manager
        /// </summary>
        ITransactionManager TransactionManager { get; set; }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Add block to the test flow
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        ILevelOneActionBlockMethods<T> AddBlock<T>() where T : IBlock, new();
        
        
        /// <summary>
        /// add block to the testing flow but the cller responsible for the instance of the new block
        /// this can use in case that the return type different from the block type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="newBlock"></param>
        /// <returns></returns>
        ILevelOneActionBlockMethods<T> AddBlock<T>(IBlock iBlock) where T : IBlock, new();
        
        
        /// <summary>
        /// execute the flow 
        /// e.g. run the blocks in the test flow one by one according the order of addition to the test flow
        /// </summary>
        /// <returns></returns>
        ITestingFlow ExecuteFlow();
        
        
        /// <summary>
        /// call the cleanup mechanizem to clean all test blocks actions
        /// </summary>
        void Cleanup();
        
        
        /// <summary>
        /// Will try to convert the block Name to the block Index, 
        /// it will throw ambiguity if more then 1 block have the requested name or block doesnt exist
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns>block index that refer to the same block</returns>
        int BlockNameToIndex(string blockName);
        /// <summary>
        /// add block name with it index for later usage.
        /// </summary>
        /// <param name="blockName">the block name</param>
        /// <param name="blockIndex">the block index</param>
        void AddBlockIndex(string blockName, int blockIndex);
        /// <summary>
        /// the action of begin transaction
        /// </summary>
        /// <param name="transactionName"></param>
        void BeginTransaction(string transactionName);
        /// <summary>
        /// the action of end transaction
        /// </summary>
        /// <param name="transactionName"></param>
        void EndTransaction(string transactionName);
        #endregion Methods

    }
}
