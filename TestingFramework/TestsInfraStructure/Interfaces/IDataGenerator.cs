﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for data generator
    /// </summary>
    public interface IDataGenerator
    {
        /// <summary>
        /// generate-data method interface
        /// </summary>
        /// <param name="system">indentification of the system to generate data for</param>
        /// <returns>the object that generate</returns>
        object GenerateData(string system);
    }
}
