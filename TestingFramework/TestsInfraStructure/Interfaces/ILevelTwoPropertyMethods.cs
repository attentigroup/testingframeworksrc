﻿using Common.Enum;
using System;
using System.Linq.Expressions;

namespace TestingFramework.TestsInfraStructure.Interfaces
{
    /// <summary>
    /// interface for the level of the block properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILevelTwoPropertyMethods<T>
    {
        /// <summary>
        /// Set value for the current property with value from other specific block from the same property type
        /// (this will only work on non deep properties destionations)
        /// </summary>
        /// <returns></returns>
        ILevelOneBlockMethods<T> WithTheSamePropertyName();

        ILevelOneBlockMethods<T> FromPropertyName(EnumPropertyName propertyName);

        ILevelOneBlockMethods<T> FromDeepProperty<T1>(Expression<Func<T1, object>> sourceProperty) where T1 : IBlock;
    }
}