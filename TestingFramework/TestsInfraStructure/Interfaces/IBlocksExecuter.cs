﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.Interfaces
{
    public interface IBlocksExecuter
    {
        #region Properties
        ITestingFlow TestingFlow { get; set; }

        #endregion Properties

        #region Methods
        void ExecuteBlocks(ITestingFlow testingFlow);

        List<Exception> CleanupExecution();

        #endregion Methods
    }
}
