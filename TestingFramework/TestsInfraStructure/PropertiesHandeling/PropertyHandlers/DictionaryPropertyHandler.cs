﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling.PropertyHandlers
{
    internal static class DictionaryPropertyHandler
    {
        public static void SetValue(object target, int index, object value)
        {
            (target as IDictionary)[index] = value;
        }

        public static object GetValue(object target, int index)
        {
            return (target as IDictionary)[index];
        }
    }
}
