﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling.PropertyHandlers
{
    /// <summary>
    /// implementation of setting string value using reflection
    /// </summary>
    public static class StringPropertyHandler
    {
        /// <summary>
        /// set the value to string property with reflection
        /// </summary>
        /// <param name="targetContainer">container of the property to set(realTarget.<b>targetContainer</b>)</param>
        /// <param name="realTarget">the object that holds the property to be set(<b>realTarget</b>.targetContainer)</param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <param name="propertyInfo"></param>
        public static void SetValue(object targetContainer, object realTarget, int index, object value, PropertyInfo propertyInfo)
        {
            var sb = new StringBuilder((string)targetContainer);
            sb[index] = value.ToString()[0];
            object realValue = sb.ToString();
            propertyInfo.SetValue(realTarget, realValue, null);
        }
        /// <summary>
        /// getting the value of a string in spcific index
        /// </summary>
        /// <param name="target"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static object GetValue(object target, int index)
        {
            return target.ToString()[index];
        }
    }
}
