﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling.PropertyHandlers
{
    public static class ListPropertyHandler
    {
        public static void SetValue(object target, int index, object value)
        {
            (target as IList)[index] = value;
        }

        public static object GetValue(object target, int index)
        {
            return (target as IList)[index];
        }
    }
}
