﻿using Common.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.PropertiesHandeling.PropertyHandlers;
using WebTests.SeleniumWrapper;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling
{
    /// <summary>
    /// This class implement the way to access properties
    /// It divide properties access to two fields: the PropertyInfo member from type proprtyInfo containing the propertyInfo 
    /// for the bottom object and the PropertyPath an LambdaExpression that let us retrieve the object that the PropertyInfo belongs to.
    /// For example an expression like "x => x.AddOffenderRequest.AgencyID" where x is AddOffenderTestBlock 
    /// the PropertyAccessor will hold the info of it.
    /// PropertyInfo it will hold the PropertyInfo of AddOffenderRequest and in the PropertyPath it will hold 
    /// the expression x => x.AgencyID
    /// If it is an expression like "x => x.OffenderID"  where x is DeleteOffenderTestBlock it will hold the info of it
    /// PropertyInfo it will hold the PropertyInfo of OffenderID and in the PropertyPath it will hold the 
    /// expression x => x 
    /// </summary>
    public class PropertyAccessor
    {
        #region Properties
        /// <summary>
        /// the Reflection.PropertyInfo object of the property
        /// </summary>
        internal PropertyInfo PropertyInfo { get; set; }
        /// <summary>
        /// the expression that represent the path to the property
        /// </summary>
        private LambdaExpression PropertyPath { get; set; }

        /// <summary>
        /// Return the length of the PropertyPath
        /// </summary>
        public int Length
        {
            get { return PropertyPath.ToString().Length; }
        }

        internal int? CollectionItemIndex { get; set; }

        /// <summary>
        /// This expression work like the Identity function meaning it will return the parameter of the expression.
        /// </summary>
        private readonly Expression<Func<object, object>> m_IdentityExpression = x => x;

        #endregion Properties

        #region constrcutor
        /// <summary>
        /// Create a PropertyAccessor from a deepPropertyExpression.
        /// This constructor will not work for not a deep property like "x => x.Property"
        /// </summary>
        /// <param name="deepPropertyExpression">The LambdaExpression should be of the following pattern x => x.somePropety1.someProperty </param>
        public PropertyAccessor(LambdaExpression deepPropertyExpression)
        {
            int? collectionItemIndex;
            var memberExpression = PropertiesHelper.GetMemberExpression(deepPropertyExpression, out collectionItemIndex);
            CollectionItemIndex = collectionItemIndex;
            PropertyPath = Expression.Lambda(memberExpression.Expression, deepPropertyExpression.Parameters);
            PropertyInfo = PropertiesHelper.LambdaExpressionAsPropertyInfo(deepPropertyExpression);
        }

        /// <summary>
        /// This constructor is only for properties from the pattern like x.SomeProperty
        /// Do not use this constructor for deep properties like x.HeadProperty.SomeProperty
        /// </summary>
        /// <param name="notDeepPropertyInfo">The PropertyInfo of the desired property</param>
        public PropertyAccessor(PropertyInfo notDeepPropertyInfo)
        {
            PropertyPath = m_IdentityExpression;
            PropertyInfo = notDeepPropertyInfo;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Set a property with value
        /// </summary>
        /// <param name="block">the block to be used</param>
        /// <param name="value">the value to be set</param>
        public void SetProperty(IBlock block, dynamic value)
        {
            try
            {
                var realTarget = GetPropertyObject(block);
                if (CollectionItemIndex == null)
                {
                    PropertyInfo.SetValue(realTarget, value, null);
                }
                else
                {
                    var targetContainer = PropertyInfo.GetValue(realTarget, null);
                    var propertyType = PropertyInfo.PropertyType;

                    if (targetContainer is IList)
                    {
                        ListPropertyHandler.SetValue(targetContainer, CollectionItemIndex.Value, value);
                    }
                    else if (targetContainer is IDictionary)
                    {
                        DictionaryPropertyHandler.SetValue(realTarget, CollectionItemIndex.Value, value);
                    }
                    else if (targetContainer is string)
                    {
                        StringPropertyHandler.SetValue(targetContainer, realTarget, CollectionItemIndex.Value, value, PropertyInfo);
                    }
                    else
                    {
                        throw new PropertyAccessorException(this, string.Format("{0} type is not supported.", propertyType));
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = string.Format("PropertyAccessor:SetProperty-Error trying to set property:{0} for block: {1} w\value:{2} -error:{3}",
                    this, block.BlockName, value, ex.Message);

                Logger.Fail($"{msg} : {ex}");

                throw new PropertyAccessorException(this, msg, ex);
            }
        }
        public override string ToString()
        {
            return string.Format("PropertyAccessor {0} {1}", PropertyInfo, PropertyPath);
        }

        public object GetPropertyValue(IBlock block)
        {
            object result;

            try
            {
                var realTarget = GetPropertyObject(block);
                if (CollectionItemIndex == null)
                {
                    result = PropertyInfo.GetValue(realTarget, null);
                }
                else
                {

                    var targetContainer = PropertyInfo.GetValue(realTarget, null);
                    var propertyType = PropertyInfo.PropertyType;

                    if (targetContainer is IList)
                    {
                        result = ListPropertyHandler.GetValue(targetContainer, CollectionItemIndex.Value);
                    }
                    else if (targetContainer is IDictionary)
                    {
                        result = DictionaryPropertyHandler.GetValue(realTarget, CollectionItemIndex.Value);
                    }
                    else if (targetContainer is string)
                    {
                        result = StringPropertyHandler.GetValue(realTarget, CollectionItemIndex.Value);
                    }
                    else
                    {
                        throw new PropertyAccessorException(this, string.Format("{0} type is not supported.", propertyType));
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = string.Format("PropertyAccessor:GetPropertyValue - Error trying to get property:{0} for testblock: {1}", this, block.BlockName);

                Logger.Fail($"Error in block: {block.BlockName}: {msg}");

                throw new PropertyAccessorException(this, msg, ex);
            }
            return result;
        }

        /// <summary>
        /// invoke the delegate and return the object
        /// Get the real object to manipulate accroding to the propertyPath and the Block
        /// </summary>
        /// <param name="block"></param>
        /// <returns>object that reresent the property</returns>
        private object GetPropertyObject(IBlock block)
        {
            object result;
            try
            {
                Delegate propertyDelegate = PropertyPath.Compile();
                result = propertyDelegate.DynamicInvoke(block);
            }
            catch (Exception ex)
            {
                var msg = string.Format(
                    "Error trying to get the target from Block:{0}, with the propertyPath:{1} and item index:{2}",
                    block.BlockName, PropertyPath, CollectionItemIndex);
                throw new PropertyAccessorException(this, msg, ex);
            }
            return result;
        }
        /// <summary>
        /// Get the propertyHead name
        /// For example for x.Offender.ID it will get the enum-property-name of the Offender property
        /// </summary>
        /// <returns></returns>
        public EnumPropertyName GetPropertyHeadPropertyName()
        {
            return PropertiesHelper.PropertyInfoToPropertyName(GetHeadPropertyInfo());
        }

        /// <summary>
        /// Get the propertyHead type
        /// For example for x.Offender.ID it will get the property modifier of the "Offender" property aka EnumPropertyModifier
        /// </summary>
        /// <returns></returns>
        public EnumPropertyModifier GetPropertyHeadPropertyModifier()
        {
            return PropertiesHelper.PropertyInfoToPropertyModifier(GetHeadPropertyInfo());
        }

        /// <summary>
        /// Get the propertyHead type
        /// For example for x.Offender.ID it will get the type of the "Offender" property aka EnumPropertyType
        /// </summary>
        /// <returns></returns>
        public EnumPropertyType GetPropertyHeadPropertyType()
        {
            return PropertiesHelper.PropertyInfoToPropertyType(GetHeadPropertyInfo());
        }

        /// <summary>
        /// Get the E2EPropertyInfo of the head property
        /// For example for x => x.a.b.c it will get the propertyinfo of a
        /// </summary>
        /// <returns></returns>
        private PropertyInfo GetHeadPropertyInfo()
        {
            PropertyInfo result;
            try
            {//for cases like x => x.a
                if (PropertyPath.Body.NodeType == ExpressionType.Parameter)
                {
                    result = PropertyInfo;
                }
                else
                {
                    int? collectionItemIndex;
                    var memberExpression = PropertiesHelper.GetMemberExpression(PropertyPath, out collectionItemIndex);
                    if (memberExpression == null)
                    {
                        throw new PropertyAccessorException(this, string.Format("Couldnt get memberExpression.", this));
                    }

                    PropertyInfo rootPropertyInfo = null;
                    //we slowly working our way to the head property
                    //for example if we got x.a.b.c
                    //after the first iteration we will have x.a.b
                    while (memberExpression != null)
                    {
                        rootPropertyInfo = (memberExpression.Member as PropertyInfo);
                        memberExpression = (memberExpression.Expression as MemberExpression);
                    }
                    result = rootPropertyInfo;
                }
            }
            catch (Exception ex)
            {
                var msg = string.Format("Error trying to get the head propertyinfo for propertyPath: {0}", PropertyPath);
                throw new PropertyAccessorException(this, msg, ex);
            }
            return result;
        }
    }

    #endregion Methods
}