﻿using System;
using System.Collections;
using System.Reflection;
using System.Text;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling
{
    /// <summary>
    /// extention method to log properties from blocks.
    /// </summary>
    public static class PropertiesLogging
    {
        /// <summary>
        /// Maximum length of the collections/arrays when logged.
        /// </summary>
        public static int CollectionsMaxLength
        {
            get { return m_CollectionMaxLength; }
            set { m_CollectionMaxLength = value; }
        }

        // Tokens used to log a collection/array
        private static readonly char COLLECTIONS_START = '[';
        private static readonly char COLLECTIONS_END = ']';
        private static readonly string COLLECTIONS_SEPARATOR = ", ";
        private static readonly string COLLECTIONS_ELLIPSIS = "...";
        private static int m_CollectionMaxLength = 50;
        private static int m_MaxDepth = 2;

        /// <summary>
        /// Converts an object to a log string.
        /// </summary>
        /// <param name="obj">Object to convert.</param>
        /// <param name="currentDepth"></param>
        /// <returns>String representing the supplied object.</returns>
        static public string ToLog(this object obj, int currentDepth = 0)
        {
            StringBuilder buffer = new StringBuilder();
            if (obj == null)
            {
                // Convert a null value
                buffer.Append("null");
            }
            else if (obj is string)
            {
                // Convert a string type
                buffer.Append('"').Append(obj).Append('"');
            }
            else if (obj is Array)
            {
                // Convert an array type
                var array = obj as Array;
                buffer.Append(COLLECTIONS_START);
                for (int i = 0; i < array.Length; i++)
                {
                    if (i == CollectionsMaxLength)
                    {
                        buffer.Append(COLLECTIONS_ELLIPSIS);
                        break;
                    }

                    if (i != 0) buffer.Append(COLLECTIONS_SEPARATOR);
                    buffer.AppendFormat("{0} {1}", i, ToLog(array.GetValue(i), currentDepth));
                }
                buffer.Append(COLLECTIONS_END);
            }
            else if (obj is IEnumerable)
            {
                // Convert an enumerable type
                int i = 0;
                var enumerable = obj as IEnumerable;
                buffer.Append(COLLECTIONS_START);
                bool success = false;
                int retryNumber = 0;
                StringBuilder tempBuilder = new StringBuilder();
                while (!success)
                {
                    try
                    {
                        foreach (Object value in enumerable)
                        {
                            if (i == CollectionsMaxLength)
                            {
                                tempBuilder.Append(COLLECTIONS_ELLIPSIS);
                                break;
                            }

                            if (i != 0) tempBuilder.Append(COLLECTIONS_SEPARATOR);
                            tempBuilder.Append(ToLog(value, currentDepth));
                            i++;
                        }

                        success = true;
                    }
                    catch (InvalidOperationException exception)
                    {
                        tempBuilder.Clear();
                        if (retryNumber >= 3)
                        {
                            tempBuilder.Append(exception.ToString());
                            break;
                        }

                        retryNumber++;

                    }
                    buffer.Append(tempBuilder);
                    buffer.Append(COLLECTIONS_END);
                }
            }
            else
            {
                bool Converted = false;
                currentDepth++;
                if (currentDepth < m_MaxDepth)
                {
                    Converted = LogProperties(obj, ref buffer, currentDepth);
                }
                // Convert an object
                if (Converted == false)
                    buffer.Append(obj);
            }
            // Return the object converted to a string
            return buffer.ToString();
        }

        static private bool LogProperties(object obj, ref StringBuilder buffer, int currentDepth)
        {
            Type objType = obj.GetType();
            bool Result = false;

            if (objType.IsValueType || objType.IsPrimitive)
            {
                buffer.Append(obj);
                Result = true;
            }
            else
            {
                var properties = objType.GetProperties();
                foreach (var propertyInfo in properties)
                {
                    try
                    {
                        Result = true;
                        object propObj = propertyInfo.GetValue(obj, null);

                        buffer.AppendFormat("{0}={1} ", propertyInfo.Name, ToLog(propObj, currentDepth));
                    }
                    catch (TargetInvocationException)
                    {
                        //Not all properties are valid, so we need to swallow this exception when using refelection
                    }
                    catch (TargetParameterCountException e)
                    {
                        buffer.AppendFormat("Got TargetParameterCountException when trying to getvalue from type {0} ", obj.GetType());
                    }
                }
            }
            return Result;
        }
    }


    public static class ExtentionMethods
    {
        static public bool IsNullOrEmpty( this ICollection collection)
        {
            bool bResult = true;
            if(collection != null || collection.Count > 0)
            {
                bResult = false;
            }
            return bResult;
        }
    }
}
