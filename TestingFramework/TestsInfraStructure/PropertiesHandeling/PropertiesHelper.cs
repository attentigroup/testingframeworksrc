﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Common.Enum;
using Common.CustomAttributes;
using TestingFramework.TestsInfraStructure.Exceptions;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling
{
    /// <summary>
    /// implement the handling testing framework properties and reflection properties information
    /// </summary>
    public class PropertiesHelper
    {
        /// <summary>
        /// convert the LambdaExpression to the MemberExpression
        /// Right now this will not support expression that include index for arrays or collection like s => s.SomeProperty.CollectionProperty[index]
        /// </summary>
        /// <param name="propertyNameExpression">from the pattern of "s => Convert(s.SomeProperty.SomePropertyPropety.valueType)" or
        /// of the pattern "s => s.SomeProperty.SomePropertyPropety.refType"</param>
        /// <param name="collectionItemIndex"></param>
        /// <returns>MemverExpression of the pattern s.SomeProperty.SomePropertyProperty</returns>
        public static MemberExpression GetMemberExpression(LambdaExpression propertyNameExpression, out int? collectionItemIndex)
        {
            MemberExpression memberExpression = null;
            collectionItemIndex = null;
            Expression exp;

            if (propertyNameExpression.Body.NodeType == ExpressionType.Convert || propertyNameExpression.Body.NodeType == ExpressionType.ConvertChecked)
            {
                exp = ((UnaryExpression)propertyNameExpression.Body).Operand;
            }
            else
            {
                exp = propertyNameExpression.Body;
            }

            if (exp.NodeType == ExpressionType.Call)
            {
                var methodCallExpression = exp as MethodCallExpression;
                exp = methodCallExpression.Object;
                collectionItemIndex = Int32.Parse(methodCallExpression.Arguments[0].ToString());

            }

            if (exp.NodeType == ExpressionType.ArrayIndex)
            {
                var arrayExp = exp as BinaryExpression;
                exp = arrayExp.Left;
                //in case that right side is member Access Type we need to invoke and get the value
                if( arrayExp.Right.NodeType == ExpressionType.MemberAccess)
                {
                    collectionItemIndex = Expression.Lambda(arrayExp.Right).Compile().DynamicInvoke() as int?;
                }
                else if( arrayExp.Right.NodeType == ExpressionType.Constant)
                {
                    collectionItemIndex = Int32.Parse(arrayExp.Right.ToString());
                }
                else
                {
                    throw new PropertiesHelperException(
                        propertyNameExpression, 
                        string.Format("arrayExp.Right.NodeType {0} is not supported.", arrayExp.Right.NodeType));
                }                
            }

            memberExpression = exp as MemberExpression;

            if (memberExpression == null)
            {
                //TODO:Add logs
                var errMsg = string.Format("Couldn't extract memberExperssion from {0}", propertyNameExpression);
                //m_Log.Error(errMsg);
                throw new PropertiesHelperException(propertyNameExpression, errMsg);
            }

            return memberExpression;
        }

        /// <summary>
        /// return the property info that represented by the lamdba expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static PropertyInfo LambdaExpressionAsPropertyInfo(LambdaExpression expression)
        {
            MemberExpression memberExpression = null;
            Expression exp;

            if (expression.Body.NodeType == ExpressionType.Convert || expression.Body.NodeType == ExpressionType.ConvertChecked)
            {
                exp = ((UnaryExpression)expression.Body).Operand;
            }
            else
            {
                exp = expression.Body;
            }
            if (exp.NodeType == ExpressionType.Call)
            {
                var methodCallExpression = exp as MethodCallExpression;
                exp = methodCallExpression.Object;
            }
            if (exp.NodeType == ExpressionType.ArrayIndex)
            {
                var arrayExp = exp as BinaryExpression;
                if (arrayExp == null)
                {
                    throw new Exception();//need to change to this to refelect more data
                }

                exp = arrayExp.Left;
            }
            memberExpression = exp as MemberExpression;
            if (memberExpression == null)
            {
                throw new PropertiesHelperException(expression, "memberExpression is null");//todo add more info
            }
            var info = memberExpression.Member as PropertyInfo;

            return info;
        }

        internal static EnumPropertyName PropertyInfoToPropertyName(PropertyInfo propertyInfo)
        {
            PropertyTestAttribute pta = PropertyInfoToPropertyTestAttribute(propertyInfo);
            return pta.Name;
        }

        internal static EnumPropertyModifier PropertyInfoToPropertyModifier(PropertyInfo propertyInfo)
        {
            PropertyTestAttribute pta = PropertyInfoToPropertyTestAttribute(propertyInfo);
            return pta.PropertyModifier;
        }

        internal static EnumPropertyType PropertyInfoToPropertyType(PropertyInfo propertyInfo)
        {
            PropertyTestAttribute pta = PropertyInfoToPropertyTestAttribute(propertyInfo);
            return pta.PropertyType;
        }


        /// <summary>
        /// Extract a PropertyTestAttribute from E2EPropertyInfo
        /// </summary>
        /// <param name="propertyInfo">The E2EPropertyInfo that we want to turn into PropertyTestAttribute</param>
        /// <returns>PropertyTestAttribute</returns>
        public static PropertyTestAttribute PropertyInfoToPropertyTestAttribute(PropertyInfo propertyInfo)
        {
            var propertyTestAttributes = Attribute.GetCustomAttributes(propertyInfo, typeof(PropertyTestAttribute), true);
            if (propertyTestAttributes.Length == 0)
            {
                var msg = string.Format("No PropertyTestAttribute for {0}", propertyInfo);
                //TODO: add logs - m_Log.Error(msg);
                throw new PropertiesHelperException(propertyInfo, msg);
            }
            PropertyTestAttribute propertyTestAttribute = propertyTestAttributes[0] as PropertyTestAttribute;
            return propertyTestAttribute;
        }
    }
}
