﻿using System.Collections.Generic;

namespace TestingFramework.TestsInfraStructure.PropertiesHandeling
{
    /// <summary>
    /// Compare 2 PropertyAccessor according to thier length
    /// </summary>
    public class PropertyAccessorComparer : Comparer<PropertyAccessor>
    {
        /// <summary>
        /// performs a comparison of two objects of the same type and returns a value indicating 
        /// whether one object is less than, equal to, or greater than the other
        /// </summary>
        /// <remarks>
        /// The comparer used in Data Generator dictionary for every property accessor in order to 
        /// scan the property accessors from the shortest property accessor to the longest property accessor(expression length wise).
        /// </remarks>
        /// <param name="x">The first <see cref="PropertyAccessor"/> to compare.</param>
        /// <param name="y">The second <see cref="PropertyAccessor"/> to compare.</param>
        /// <returns>A signed integer that indicates the relative values of x and y, 
        /// as shown in the following table.Value Meaning Less than zero x is less than y.Zero x equals y.Greater than zero x is greater than y.</returns>
        public override int Compare(PropertyAccessor x, PropertyAccessor y)
        {
            const int xIsLonger = 1;
            const int yIsLonger = -1;
            const int equal = 0;

            int result;

            if (x == null)
            {
                result = yIsLonger;
            }
            else if (y == null)
            {
                result = xIsLonger;
            }
            else if (x.Length > y.Length)
            {
                result = xIsLonger;
            }
            else if (x.Length < y.Length)
            {
                result = yIsLonger;
            }
            else
            {
                if (x.PropertyInfo == y.PropertyInfo)
                {
                    if (x.CollectionItemIndex == y.CollectionItemIndex)
                    {
                        result = equal;
                    }
                    else
                    {
                        if (x.CollectionItemIndex == null)
                        {
                            result = yIsLonger;
                        }
                        else if (y.CollectionItemIndex == null)
                        {
                            result = xIsLonger;
                        }
                        else if (x.CollectionItemIndex > y.CollectionItemIndex)
                        {
                            result = xIsLonger;
                        }
                        else
                        {
                            result = yIsLonger;
                        }
                    }
                }
                else
                {
                    result = x.PropertyInfo.ToString().Length > y.PropertyInfo.ToString().Length ? xIsLonger : yIsLonger;
                }
            }
            return result;
        }
    }
}
