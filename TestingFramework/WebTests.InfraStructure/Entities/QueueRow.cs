﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class QueueRow
    {
        public string Request { get; set; }
        public string Type { get; set; }
        public string ReceiverSerialNumber { get; set; }
        public string RequestTime { get; set; }
        public string OffenderName { get; set; }
    }
}
