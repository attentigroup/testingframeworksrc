﻿

using System;
using System.Collections.Generic;

namespace WebTests.InfraStructure.Entities
{
    public class E4CommunicationConfigurationElement
    {
        public E4CommunicationConfigurationElement(bool _Landline, bool _GPRS, bool _SmsCallback, bool _CSD, int? _Landline_Priority, int? _GPRS_Priority)
        {
            Landline = _Landline;
            GPRS = _GPRS;
            SmsCallback = _SmsCallback;
            CSD = _CSD;
            Landline_Priority = _Landline_Priority;
            GPRS_Priority = _GPRS_Priority;
        }

        public bool Landline { get; set; }

        public bool GPRS { get; set; }

        public bool SmsCallback { get; set; }

        public bool CSD { get; set; }

        public int? Landline_Priority { get; set; }

        public int? GPRS_Priority { get; set; }

        public List<E4CommunicationConfigurationElement> E4CommunicationList()
        {
            List<E4CommunicationConfigurationElement> allOptions = new List<E4CommunicationConfigurationElement>();
            E4CommunicationConfigurationElement option1 = new E4CommunicationConfigurationElement(true, false, false, false, 1, null);
            E4CommunicationConfigurationElement option2 = new E4CommunicationConfigurationElement(true, true, false, false, 1, 2);
            E4CommunicationConfigurationElement option3 = new E4CommunicationConfigurationElement(true, true, true, false, 1, 2);
            E4CommunicationConfigurationElement option4 = new E4CommunicationConfigurationElement(false, true, true, false, null, 1);
            E4CommunicationConfigurationElement option5 = new E4CommunicationConfigurationElement(true, true, false, false, 2, 1);
            E4CommunicationConfigurationElement option6 = new E4CommunicationConfigurationElement(true, true, true, false, 2, 1);

            allOptions.Add(option1);
            allOptions.Add(option2);
            allOptions.Add(option3);
            allOptions.Add(option4);
            allOptions.Add(option5);
            allOptions.Add(option6);

            return allOptions;

        }
    }


}
