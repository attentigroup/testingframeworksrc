﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class Equipment
    {
        public string SerialNumber { get; set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public string Protocol { get; set; }
        public string SIMVoiceNumber { get; set; }
        public string SIMDataNumber { get; set; }
        public string GSMEncryptionType { get; set; }
        public string RFEncryptionType { get; set; }
        public string Agency { get; set; }
        public string Offender { get; set; }

    }
}
