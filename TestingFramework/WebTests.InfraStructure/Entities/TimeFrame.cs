﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class TimeFrame
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string RecurseEveryWeek { get; set; }
        public string CopyWithinWeek { get; set; }
        public string Limitation { get; set; }
    }
}
