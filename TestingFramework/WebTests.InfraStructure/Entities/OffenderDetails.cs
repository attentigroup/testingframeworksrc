﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class OffenderDetails
    {
        public string OffenderRefId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string ProgramType { get; set; }
        public string Agency { get; set; }
        public string Officer { get; set; }
        public string Schedule { get; set; }
        public string Tx { get; set; }
        public string Receiver { get; set; }
        public string Zone { get; set; }
        public string MedicalCondition { get; set; }
        public string SubType { get; set; }
        public string RelatedOffenderRefId { get; set; }
        public OffenderLocation Location { get; set; }
        public TrailReport TrailReport { get; set; }
        public string ProgramEnd { get; set; }
        public string DateCreated { get; set; }
        public string Status { get; set; }
    }

    public class OffenderLocation
    {
        public string LastTrailPointDateTime { get; set; }
        public bool IsIntervalBetweenPoints { get; set; }

        public string NonDataLabel { get; set; }

        public bool TrailLastPointOnMapExists { get; set; }

        public Zone Zone { get; set; }

        public DateTime FromDateTime { get; set; }

        public DateTime ToDateTime { get; set; }
    }

    public class Zone
    {
        public string ID { get; set; }
        public string Name { get; set; }

    }

    public class CurrentStatusTableRow
    {
        public string SeverityStatus { get; set; }
        public string EventTime { get; set; }
        public string OffenderRefId { get; set; }
        public string FullName { get; set; }
        public string Message { get; set; }
        public string EventId { get; set; }
        public string ReceiverSN { get; set; }
        public string Agency { get; set; }
        public string UpdateTime { get; set; }
        public string Status { get; set; }
        public string Severity { get; set; }

    }

}
