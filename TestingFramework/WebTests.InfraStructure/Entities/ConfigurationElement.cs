﻿

using System;

namespace WebTests.InfraStructure.Entities
{
    public enum ConfigElemntType
    {
        ConfigElemntType_bool,
        ConfigElemntType_radio_button,
        ConfigElemntType_string,
        ConfigElemntType_dropdown_2_Options,
        ConfigElemntType_dropdown_3_And_More_Options,
        ConfigElemntType_communication_priority,
    }
    public class ConfigurationElement
    {
        public ConfigurationElement(string _ResourceID, ConfigElemntType _configElemntType, string _Value)
        {
            ResourceID = _ResourceID;
            Type = _configElemntType;
            Value = _Value;
        }

        public string ResourceID { get; set; }

        public ConfigElemntType Type { get; set; }

        public string Value { get; set; }

    }



}
