﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class AppVersions
    {
        public string WebVersion { get; set; }
        public string DatabaseVersion { get; set; }
        public string DatabaseServer { get; set; }
    }
}
