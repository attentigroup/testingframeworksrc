﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class MonitorTableRow
    {
        public string SeverityStatus { get; set; }
        public string EventTime { get; set; }
        public string OffenderRefId { get; set; }
        public string FullName { get; set; }
        public string Message { get; set; }
        public string EventId { get; set; }
        public string ReceiverSN { get; set; }
        public string Agency { get; set; }
        public string UpdateTime { get; set; }
        public string EventStatus { get; set; }

    }
    public enum SevirityStatus
    {
        Violation,
        Alarm,
        Action,
        System
    }
}
