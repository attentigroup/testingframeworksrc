﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;

namespace WebTests.InfraStructure.Entities
{
    public class SecurityConfigObject
    {
        public By Locator { get; set; }
        /// <summary>
        /// This value is the exact phrase of the API's full length ResourceID
        /// </summary>
        public EnmModifyOption ModifyOption { get; set; }
        public string ElementName { get; set; }
        public bool ActualVisibleEnabledExposed { get; set; }
        public string SecurityConfigMethodName { get; set; }
        public string ResourceId { get; set; }
    }
}
