﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Entities
{
    public class TrailReport
    {
        public string Header { get; set; }
        public string OffenderDetails { get; set; }
        public bool IsMapExists { get; set; }
        public DateTime From { get; set; }

        public DateTime To { get; set; }

    }
}
