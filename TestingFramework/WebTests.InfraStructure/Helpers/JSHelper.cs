﻿using OpenQA.Selenium;
using System.Threading;
using WebTests.InfraStructure.Pages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Helpers
{
    public class JSHelper
    {
        private readonly Browser _browser;
        IJavaScriptExecutor jse;

        public JSHelper(Browser browser)
        {
            _browser = browser;
            jse = _browser.GetWebDriver() as IJavaScriptExecutor;
        }
        public void ClearCache()
        {
            Logger.WriteLine("Clearing app cache...");
            
            jse.ExecuteScript("MainMenuMaster.clearCache();");

            new AlertPopupPage(_browser).Close();
        }

        public void SetFieldValueById(string elementId, string setValue)
        {
            var jsCommand = $"document.getElementById('{elementId}').value = '{setValue}'";
            jse.ExecuteScript(jsCommand);
        }

        //public void SetFieldValueByClassName(string ElementClassName, string setValue, string resultIndex = "0")
        //{
        //    var jsCommand = $"document.getElementsByClassName('{ElementClassName}')[{resultIndex}].value = '{setValue}';";
        //}
    }
}
