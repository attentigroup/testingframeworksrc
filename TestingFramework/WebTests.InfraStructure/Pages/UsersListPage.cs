﻿using System;
using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class UsersListPage : WOMPage
    {
        public UsersListPage(Browser browser, bool doWaitForLoad = true) : base(browser)
        {
            if(doWaitForLoad)
            Wait.Until(() => GetPageDisplayName() == "Users List");

        }

        internal UserListTablePage UserListTable => new UserListTablePage(_browser);
        internal UserListFilterSectionPage UserListFilterSection => new UserListFilterSectionPage(_browser);
        internal UserListHeaderBottomPage UserListHeaderBottom => new UserListHeaderBottomPage(_browser);


        //TODO: rename this method
        internal void SelectFirstUser(string title)
        {
            UserListFilterSection.SelectFilterByTitle(-1, title);
            _browser.WaitForNotBusyCursor();
            UserListTable.WaitForContent();
            UserListTable.SelectFirstUser();
        }

        public void SelectFirstUserFromTable()
        {
            UserListTable.SelectFirstUser();
        }

        public void ClickOnUserFilters(int sectionNumber, string filterTitle = "Active")
        {

            var filterRowElement = UserListFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);

            filterRowElement.WaitToBeClickable();

            filterRowElement.ClickSafely();

            UserListTable.WaitForContent();
        }

        internal void OpenActionOptionsMenu()
        {

            var handlingOptionBtn = _browser.WaitForElement(By.CssSelector("#bottomActionsBar [title='User Account Actions']"), "User Account Actions option button");
            handlingOptionBtn.MoveCursorToElement();

        }


        internal void ClickForDisableAccount()
        {
            var disableAccount = _browser.WaitForElement(By.CssSelector("#disableAccountBTN"), "Disable Acount Button");
            OpenActionOptionsMenu();
            disableAccount.Click();
        }

        internal void ClickForEnableAccount()
        {
            var disableAccount = _browser.WaitForElement(By.CssSelector("#enableAccountBTN"), "Enable Acount Button");
            OpenActionOptionsMenu();
            disableAccount.Click();
        }

        internal SecurityConfigObject Navigate2Section(string aPIString)
        {
            var container = _browser.WaitForElement(By.Id("divBodyContainer"), "Content container");
            Wait.Until(() => container.Displayed);

            return GetSecurityResult(aPIString);
        }

        internal SecurityConfigObject UserListGetSecurityResult(string APIString)
        {
            UserListHeaderBottom.ShowUserActioMenu();
            //var handlingOptionBtn = _browser.WaitForElement(By.CssSelector("#bottomActionsBar [title='User Account Actions']"), "User Account Actions option button");
            //handlingOptionBtn.MoveCursorToElement();
            return GetSecurityResult(APIString);
        }



        //*** internal classes ***

        internal class UserListHeaderBottomPage : HeaderBottomPageBase
        {
            public UserListHeaderBottomPage(Browser browser) : base(browser)
            {

            }
            protected override By GetActionsButtonLocator()
            {
                throw new NotImplementedException();
            }

            protected override By GetDownloadButtonLocator()
            {
                throw new NotImplementedException();
            }

            protected override By GetOffenderActionsButtonLocator()
            {
                return By.CssSelector("#bottomActionsBar .btn.manual");
            }

            internal void ShowUserActioMenu()
            {
                _browser.WaitForElement(GetOffenderActionsButtonLocator(), "User actions menu button").MoveCursorToElement();
            }
        }

        internal class UserListTablePage : WOMTableBase
        {
            public UserListTablePage(Browser browser) : base(browser)
            {
            }

            public void WaitForContent()
            {
                WaitForContentToLoad();
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("UsersList");
            }

            //TODO:remove duplication
            public void SelectFirstUser()
            {
                var tableRow = GetTableRowElementByNumber(1);

                var clickableColumn = GetClickableColumn(tableRow);
                clickableColumn.WaitToBeClickable();
                clickableColumn.Click();
                clickableColumn.WaitToDisappear();
            }

            protected override string GetClickableColumnCssSelectorString()
            {
                return "div[class~='userNavigate']";
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#tableContainer .row";
            }
        }

        internal class UserListFilterSectionPage : FilterSectionBase
        {
            public UserListFilterSectionPage(Browser browser) : base(browser)
            {
            }
        }
    }
}