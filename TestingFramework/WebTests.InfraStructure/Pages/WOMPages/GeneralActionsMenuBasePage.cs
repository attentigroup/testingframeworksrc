﻿using OpenQA.Selenium;
using System.Linq;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public abstract class GeneralActionsMenuBasePage
    {
        protected readonly Browser _browser;
        protected readonly BrowserElement _container;
        public GeneralActionsMenuBasePage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(GeneralActionsContainerSelector(), "General actions menu");
        }

        protected virtual By GeneralActionsContainerSelector()
        {
            return By.Id("generalActionsMenu");
        }

        public void Refresh()
        {
            var btnRefresh = _container.WaitForElement(GetRefreshButtonSelector(), "Refresh button");
            btnRefresh.WaitToBeClickable();
            btnRefresh.ClickSafely();
        }

        protected virtual By GetRefreshButtonSelector()
        {
            return By.Id("refreshBtn");
        }

        public void Delete()
        {
            var btnDelete = _container.WaitForElement(By.CssSelector(".button.button.square.delete"), "Delete button");
            btnDelete.WaitToBeClickable();
            btnDelete.ClickSafely();

            //var deleteOffenderPopup = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "");
            //var okBtn = deleteOffenderPopup.WaitForElements(By.CssSelector(".buttons"), "").ElementAt(0);
            //okBtn.ClickSafely();

        }

        public void Save()
        {
            var btnSave = _container.WaitForElement(GetSaveButtonSelector(), "Save button");
            btnSave.WaitToBeClickable();
            btnSave.ClickSafely();
        }

        private void ExportToExcel()
        {
            _browser.WaitForElement(By.CssSelector("#exportToExcelBtn button"), "Export to Excel").Click();
        }

        protected abstract By GetSaveButtonSelector();

        public MoveToPopupPage MoveTo()
        {
            var btnMoveTo = _container.WaitForElement(By.CssSelector("#moveToBtn"), "MoveTo button");
            btnMoveTo.ClickSafely();
            return new MoveToPopupPage(_browser);
        }
    }
}
