﻿using OpenQA.Selenium;
using WebTests.SeleniumWrapper;
using WebTests.InfraStructure.Entities;
using WebTests.Common;
using WebTests.InfraStructure.Helpers;
using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using WebTests.InfraStructure.Pages.OffenderPages;
using WebTests.InfraStructure.Pages.EquipmentPages;
using WebTests.InfraStructure.Pages.MonitorPages;
using System.Text.RegularExpressions;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    //|*************************************************|
    //|*** WOM stands for WebOffenderManagement Page ***|
    //|This class serves as the base class for all the  |
    //|application's pages                              |
    //|*************************************************|
    public class WOMPage : WOMPageCommon
    {
        public WOMPage(Browser browser, bool doWaitForLoad = true) : base(browser)
        {
            if (doWaitForLoad)
                Wait.Until(() => ApplicationHeader.Displayed);
        }

        internal bool IsLogoutVisible()
        {
            var menuOptionElement = GetMenuOptionElement(ApplicationHeader, UserMenuButtonLocator, "Logout");//'ChangePassword' is a string contained in the data-modify attribute
            var isDisplayed = (menuOptionElement != null && menuOptionElement.Displayed) ? true : false;

            return isDisplayed;
        }

        //public bool GetShowHideElemntStatus(/*string fullElementFromApi,*/ string section, string type, string title)
        //{
        //    //_browser.Wa
        //    //find the elemnt by any parameter and return the show\hide status
        //    var getSecurityElemntBy = QueueAndLogSecorityConfigData.getSecurityElemntBy(/*fullElementFromApi[0],*/ section, type, title);
        //    return IsElementVisible(getSecurityElemntBy);
        //    //return false;
        //}

        //public bool GetShowHideElemntStatus_EventGrid(/*string fullElementFromApi,*/ string section, string type, string title)
        //{
        //    //_browser.Wa
        //    //find the elemnt by any parameter and return the show\hide status
        //    var getSecurityElemntBy = EventGridSecorityConfigData.getSecurityElemntBy(/*fullElementFromApi[0],*/ section, type, title);
        //    return IsElementVisible(getSecurityElemntBy);
        //    //return false;
        //}

        public BrowserElement HeaderBottom
        {
            get { return ApplicationHeader.WaitForElement(By.ClassName("ApplicationHeader-HeaderBottom"), "Header bottom"); }
        }

        internal bool IsDataBaseVisible()
        {
            ShowInfo();
            var dbVersion = ApplicationHeader.WaitForElements(By.Id("headerAboutToolTipDBVersion"), "DB version", minElementsToGet: 0);
            var isDisplayed = (dbVersion.Any()) ? true : false;
            return isDisplayed;
        }

        internal SecurityConfigObject IsDataBaseVisible_Security(string APIString)
        {
            ShowInfo();
            return base.GetSecurityResult(APIString);
        }

        internal SecurityConfigObject GetGeneralSecurityConfigResults(string section, string apiString)
        {
            string[] patterns = new string[]{
            @".*._(Button_Monitor$|Button_Offenders$|Button_Reports$|Button_System$|Tooltip_About$)\z",  //case 0
            @".*._(Link_Dashboard$|Link_Monitor$)\z",   //case 1
            @".*._(Link_OffenderStatus$|Link_Schedule$)\z",   //case 2
            @".*._(Link_GroupsList$|Link_OffendersList$|ListItem_AddNewOffender$)\z",    //case 3
            @".*._(Link_DVPairsList$|Link_LastKnownLocation$|Link_OpenPanicAlarms$|ListItem_BIReportsLink$|ListItem_WebReportsLink$)\z",    //case 4
            @".*._(Link_EquipmentManagement$|Link_OpenSessions$|Link_SecurityConfig$|ListItem_SystemConfiguration$|Link_SystemTable$)\z",    //case 5
            @".*._(Link_CommunicationServers$|Link_SystemTrackerRules$)\z", //case 6
            @".*._(Button_ChangePassword$|Button_Logout$)\z",   //case7
            @".*._(Label_DatabaseVersion$|Label_DatabseServerName$|Label_WebVersion$)\z",    //case 8
            @".*._(Link_GPSVersionImport$)\z",    //case 9
            @".*._(Link_Log$|Link_Queue$)\z", //case 10
            @".*._(ListItem_User$|Link_UsersList$)\z",   //case 11
            @".*._(Link_SoftwareImport$|Link_GPSSoftwareImport$)\z",    //case 12
            @".*._(Link_GroupDetails$|Link_GroupParametes$)\z",    //case 13
            @".*._(Link_ProgramConfig$|Link_Location$)\z",   //case 14
            @".*._(Link_UserOffenders$)\z", //case 15
            @".*._(Link_UserAgencies$)\z",  //case 16
            @".*._(Link_UserConfiguration$|Link_UserDetails$)\z", //case 17
            @".*._(Button_OffenderCounters$)\z", //case 18
            @".*._(ListItem_AddNewUser$)\z", //case 19
            @".*._(Link_OffenderDetails$)\z" //case 20

            };

            SecurityConfigObject result = null;

            for (int i = 0; i < patterns.Count(); i++)
            {
                var match = FindMatch(apiString, patterns[i]);
                if (match.Success)
                {
                    switch (i)
                    {
                        case 0://Main screen
                            break;

                        case 1://Monitor menu
                            //SelectFromDropdownList(ApplicationHeader, MonitorMenuButtonLocator, null, IsOpenOnly: true);
                            break;

                        case 2://Offender tabs
                            Offenders_AddNewOffender();
                            break;

                        case 3://Offenders menu
                            SelectFromDropdownList(ApplicationHeader, OffendersMenuButtonLocator, null, IsOpenOnly: true);
                            break;

                        case 4://Reports menu
                            SelectFromDropdownList(ApplicationHeader, ReportsMenuButtonLocator, null, IsOpenOnly: true);
                            break;

                        case 5://System menu
                            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, null, IsOpenOnly: true);
                            break;

                        case 6://System menu > System configuration sec menu
                            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "SystemConfiguration", IsOpenOnly: true);
                            break;

                        case 7://User menu
                            SelectFromDropdownList(ApplicationHeader, UserMenuButtonLocator, null, IsOpenOnly: true);
                            break;

                        case 8:
                            new AboutPage(_browser).OpenAboutTooltip();
                            break;

                        case 9://System menu > Receiver Version Import
                            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "receiverVersionsImportSubMenu", IsOpenOnly: true);
                            break;

                        case 10:
                            System_QueueAndLog();
                            break;

                        case 11:
                            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "userSubMenu", IsOpenOnly: true);
                            break;

                        case 12://System menu > receiverVersionsImportSubMenu
                            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "receiverVersionsImportSubMenu", IsOpenOnly: true);
                            break;

                        case 13: //Group
                            Offenders_GroupsList();
                            new OffenderGroupsListPage(_browser).SelectGroup(1);
                            break;

                        case 14: //New Offender page
                            Offenders_AddNewOffender();
                            break;

                        case 15://Users list AgencyManager filter
                            System_Users_UsersList();
                            ShowUserListAgancyManager();
                            break;

                        case 16://Users list MonitorCenter filter
                            System_Users_UsersList();
                            ShowUserListMonitorCenter();
                            break;

                        case 17://Users list Officers filter
                            System_Users_UsersList();
                            ShowUserListOfficer();
                            break;

                        case 18://Offender counter
                            new OffenderCounterPage(_browser).OpenCounterIfClosed();
                            break;

                        case 19: //System -> Users
                            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "userSubMenu", IsOpenOnly: true);
                            break;

                        case 20: //Offenders list
                            Offenders_OffendersList().SelectFirstRow();
                            break;

                        default:
                            ThrowSecurityException(apiString, i);
                            break;
                    }
                    if (FindMatch(apiString, @"General_Screen_Link(\w+)Details").Success)
                    {
                        var elmExistOr = _browser.WaitForElementsOR(5, By.ClassName("groupDetails-workArea-title"), By.CssSelector("li[data-tab='OffenderDetails']"), By.Id("userMasterTabsUserDetails"));
                        result = new SecurityConfigObject() { ActualVisibleEnabledExposed = elmExistOr.Item1.Any(), Locator = elmExistOr.Item2 };
                    }
                    else
                        result = GetSecurityResult(apiString);

                }
                if (result != null)
                    break;
            }

            if (result == null)
                ThrowSecurityException(apiString, -1);

            return result;
        }

        private void ShowUserListOfficer()
        {
            new UsersListPage(_browser).UserListFilterSection.SelectFilterByTitle(1, "Officers");
            new UsersListPage(_browser).SelectFirstUserFromTable();
        }

        private void ShowUserListMonitorCenter()
        {
            new UsersListPage(_browser).UserListFilterSection.SelectFilterByTitle(1, "Monitor Center");
            new UsersListPage(_browser).SelectFirstUserFromTable();
        }

        private void ShowUserListAgancyManager()
        {
            new UsersListPage(_browser).UserListFilterSection.SelectFilterByTitle(1, "Agency Managers");
            new UsersListPage(_browser).SelectFirstUserFromTable();
        }

        private static void ThrowSecurityException(string apiString, int i)
        {
            throw new WebTestingException($"There is no match with the API strings '{apiString}' (case {i})");
        }

        private Match FindMatch(string apiString, string pattern)
        {
            var r = new Regex(pattern);
            return r.Match(apiString);
        }

        private void ShowInfo()
        {
            var info = ApplicationHeader.WaitForElement(By.CssSelector(".infoTemp.ApplicationHeader-InfotooltipOpener"), "Version info");
            Wait.Until(() => info.Displayed);
            info.MoveCursorToElement();
        }

        public AppVersions GetVersions()
        {
            IJavaScriptExecutor jse = _browser.GetWebDriver() as IJavaScriptExecutor;

            var webVersion = jse.ExecuteScript("return UserResources.Login.WebVersion").ToString();

            var dbVersion = jse.ExecuteScript("return UserResources.Login.DBVersion").ToString();

            var dbServer = jse.ExecuteScript("return UserResources.Login.DataSourceName").ToString();
            #region Getversion using gui
            //gui Test
            //var info = _applicationHeader.WaitForElement(By.CssSelector(".infoTemp.ApplicationHeader-InfotooltipOpener"), "Version info");
            //Wait.Until(() => info.Displayed);
            //info.MoveCursorToElement();
            //var webVersionElement = _applicationHeader.WaitForElement(By.Id("headerAboutToolTipWebVersion"), "Web version");
            //Wait.Until(() => webVersionElement.Displayed);
            //var webVersionT = webVersionElement.Text;
            //var dbVersionT = _applicationHeader.WaitForElement(By.Id("headerAboutToolTipDBVersion"), "DB version").Text;
            //var dbServerT = _applicationHeader.WaitForElement(By.Id("headerAboutToolTipDataSourceName"), "DB server").Text;
            #endregion
            return new AppVersions() { WebVersion = webVersion, DatabaseVersion = dbVersion, DatabaseServer = dbServer };
        }

        public string GetPageDisplayName()
        {
            var pageDisplayName = HeaderBottom.WaitForElement(By.Id("page_display_name"), "Page display name");
            Wait.Until(() => pageDisplayName.Displayed && pageDisplayName.Text != string.Empty, "Page display name is not shown");
            var pageNameFromUrl = _browser.Url.Split(new string[] { "/", ".aspx" }, StringSplitOptions.RemoveEmptyEntries).Last();
            var pageName = (pageNameFromUrl == "OffenderDetails" || pageNameFromUrl == "GroupDetails") ? pageNameFromUrl : string.Empty;
            return pageName + pageDisplayName.Text;
        }

        //Menu button locators
        protected static By MonitorMenuButtonLocator => By.Id("monitorLink");

        protected By OffendersMenuButtonLocator => By.Id("usersLink");

        protected By ReportsMenuButtonLocator => By.Id("reportsLink");

        protected By SystemMenuButtonLocator => By.Id("configurationsLink");

        protected By UserMenuButtonLocator => By.ClassName("ApplicationHeader-HeaderUserName");

        protected By PrimaryMenuButtonLocator => By.ClassName("ApplicationHeader-PrimaryMenu");

        public UserMenu UserMenu()
        {
            return new UserMenu(_browser);
        }

        //Monitor
        public MonitorPage Monitor()
        {
            var toTime = DateTime.Now.AddSeconds(12);
            while (GetPageDisplayName() != "Monitor" && DateTime.Now < toTime)
            {
                _browser.WaitForElement(MonitorMenuButtonLocator, "Monitor screen").ClickSafely();
                Wait.Until(() => GetPageDisplayName() == "Monitor", "Monitor page failed to display", 3, false);
            }

            if (GetPageDisplayName() != "Monitor")
                throw new Exception("Couldn't navigate to the Monitor screen");

            return new MonitorPage(_browser);
        }

        public DashboardPage Dashboard()
        {
            SelectFromDropdownList(ApplicationHeader, MonitorMenuButtonLocator, "Dashboard", thisScreenName: GetPageDisplayName(), targetScreenName: "Dashboard");
            return new DashboardPage(_browser);
        }

        public MonitorPage Monitor_NoClick()
        {
            return new MonitorPage(_browser);
        }
        //ortal
        public string GetFilterTitle()
        {
            var titleElement = _browser.WaitForElement(By.Id("selectedFilterName"), "filter title");
            return titleElement.Text;
        }

        //Offenders
        public OffendersListPage Offenders_OffendersList()
        {
            SelectFromDropdownList(ApplicationHeader, OffendersMenuButtonLocator, "OffendersList", thisScreenName: GetPageDisplayName(), targetScreenName: "Offenders List");

            return new OffendersListPage(_browser);
        }

        public OffendersListPage Offenders_OffendersList_NoClick()
        {
            return new OffendersListPage(_browser);
        }

        public OffenderGroupsListPage Offenders_GroupsList(bool doNavigate = true)
        {
            if (doNavigate)
                SelectFromDropdownList(ApplicationHeader, OffendersMenuButtonLocator, "GroupsList", thisScreenName: GetPageDisplayName(), targetScreenName: "Groups");

            return new OffenderGroupsListPage(_browser);
        }
        public AddNewOffenderPage Offenders_AddNewOffender(bool setNewOffender = true, bool doWaitForLoad = true)
        {
            if (setNewOffender)
                SelectFromDropdownList(ApplicationHeader, OffendersMenuButtonLocator, "AddNewOffender", thisScreenName: GetPageDisplayName(), targetScreenName: "Details");

            return new AddNewOffenderPage(_browser, doWaitForLoad);
        }


        public OffenderGroupPage OffenderGroupPage()
        {
            return new OffenderGroupPage(_browser);
        }



        //Reports
        public OpenPanicAlarmPage Reprots_OpenPanicAlarm()
        {
            SelectFromDropdownList(ApplicationHeader, ReportsMenuButtonLocator, "OpenPanicAlarms", thisScreenName: GetPageDisplayName(), targetScreenName: "Open Panic Alarms");
            return new OpenPanicAlarmPage(_browser);
        }

        public DvPairsPage Reports_DvPairs()
        {
            SelectFromDropdownList(ApplicationHeader, ReportsMenuButtonLocator, "DVPairsList", thisScreenName: GetPageDisplayName(), targetScreenName: "DV Pairs");
            return new DvPairsPage(_browser);
        }

        public CurrentKnownLocationPage Reports_CurrentKnownLocation()
        {
            SelectFromDropdownList(ApplicationHeader, ReportsMenuButtonLocator, "LastKnownLocation", thisScreenName: GetPageDisplayName(), targetScreenName: "Current Known Location");
            return new CurrentKnownLocationPage(_browser);
        }

        public OperationalReportsPage Reports_OperationalReports()
        {
            SelectFromDropdownList(ApplicationHeader, ReportsMenuButtonLocator, "WebReportsLink");
            return new OperationalReportsPage(_browser);
        }

        public BiReportsAppPage Reports_BiReportsApp()
        {
            SelectFromDropdownList(ApplicationHeader, ReportsMenuButtonLocator, "BIReportsLink");
            return new BiReportsAppPage(_browser);
        }

        //System
        public QueueAndLogPage System_QueueAndLog()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "QueueAndLog", thisScreenName: GetPageDisplayName(), targetScreenName: "Queue");
            return new QueueAndLogPage(_browser);
        }

        public EquipmentManagementPage System_EquipmentManagement()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "EquipmentManagement", thisScreenName: GetPageDisplayName(), targetScreenName: "Equipment Management");
            return new EquipmentManagementPage(_browser);
        }

        public EquipmentManagementPage EquipmentManagementPage_NoClick()
        {
            return new EquipmentManagementPage(_browser);
        }

        public TrackerRulesPage System_SystemConfiguration_TrackerRules()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "SystemConfiguration", "TrackerRules", thisScreenName: GetPageDisplayName(), targetScreenName: "Tracker Rules");
            return new TrackerRulesPage(_browser);
        }

        public CommunicationServersPage System_SystemConfiguration_CommunicationServers()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "SystemConfiguration", "CommunicationServers", thisScreenName: GetPageDisplayName(), targetScreenName: "Communication Servers");
            return new CommunicationServersPage(_browser);
        }

        public CommunicationServersPage System_SystemConfiguration_CommunicationServers_NoClick()
        {
            var selectOption = GetMenuOptionElement(ApplicationHeader, SystemMenuButtonLocator, "SystemConfiguration", "CommunicationServers");
            return new CommunicationServersPage(_browser);
        }

        public UsersListPage System_SystemConfiguration_UsersList_NoClick()
        {
            return new UsersListPage(_browser, false);
        }

        public SecurityConfigurationPage System_SecurityConfiguration()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "SecurityConfig", thisScreenName: GetPageDisplayName(), targetScreenName: "Security Configuration");
            return new SecurityConfigurationPage(_browser);
        }

        public E4ReceiverVersionImportPage System_E4ReceiverVersionImport()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "SoftwareImport");
            return new E4ReceiverVersionImportPage(_browser);
        }

        public GPSReceiverVersionImportPage System_GPSReceiverVersionImport()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "GPSSoftwareImport");
            return new GPSReceiverVersionImportPage(_browser);
        }

        public UsersListPage System_Users_UsersList()
        {
            Performance.StartPoint(PerformanceTag.UsersList);

            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "ListItem_User", "UsersList", thisScreenName: GetPageDisplayName(), targetScreenName: "Users List");

            _browser.WaitForNotBusyCursor();

            Performance.EndPoint(PerformanceTag.UsersList);

            return new UsersListPage(_browser);
        }

        public AddNewUserPage System_Users_AddNewUser()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "ListItem_User", "AddNewUser");//Add this and target screenName when IDs are available
            return new AddNewUserPage(_browser);
        }

        public AddNewUserPage System_Users_AddNewUser_NoClick()
        {
            return new AddNewUserPage(_browser);
        }

        public SystemTablesPage System_SystemTables(bool doClick = true)
        {
            if(doClick)
                SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "SystemTable", thisScreenName: GetPageDisplayName(), targetScreenName: "System Tables");
            return new SystemTablesPage(_browser);
        }

        public OpenSessionsPage System_OpenSessions()
        {
            SelectFromDropdownList(ApplicationHeader, SystemMenuButtonLocator, "OpenSessions", thisScreenName: GetPageDisplayName(), targetScreenName: "Open Sessions");
            return new OpenSessionsPage(_browser);
        }


        public void ClearCache()
        {
            new JSHelper(_browser).ClearCache();
            _browser.RefreshPage();
        }

        public bool IsChangePasswordVisible()
        {
            var menuOptionElement = GetMenuOptionElement(ApplicationHeader, UserMenuButtonLocator, "ChangePassword");//'ChangePassword' is a string contained in the data-modify attribute
            var isDisplayed = (menuOptionElement != null && menuOptionElement.Displayed) ? true : false;

            return isDisplayed;
        }

        internal bool IsDVPairsMenuVisible()
        {
            //_browser.GetWebDriver().Navigate().Refresh();
            var menuOptionElement = GetMenuOptionElement(ApplicationHeader, ReportsMenuButtonLocator, "DVPairs");//'ChangePassword' is a string contained in the data-modify attribute
            var isDisplayed = (menuOptionElement != null && menuOptionElement.Displayed) ? true : false;
            Logger.WriteLine($"Element is displayed? : {isDisplayed} ");
            return isDisplayed;
        }

        internal bool IsAddNewOffenderVisible()
        {
            var menuOptionElement = GetMenuOptionElement(ApplicationHeader, OffendersMenuButtonLocator, "AddNewOffender");//'ChangePassword' is a string contained in the data-modify attribute
            var isDisplayed = (menuOptionElement != null && menuOptionElement.Displayed) ? true : false;
            Logger.WriteLine($"Element is displayed? : {isDisplayed} ");
            return isDisplayed;
        }

        internal bool IsAboutVisible()
        {
            return _browser.IsElementVisible(By.ClassName("ApplicationHeader-HeaderInfoIcon"), "Application header");
        }

        protected void GoToTab(BrowserElement tab, By frameLocatorToWaitFor)
        {
            Wait.Until(() => tab.Enabled && tab.Displayed);

            if (IsTabSelected(tab) == false)
            {
                tab.WaitToBeClickable();
                tab.MoveCursorToElement();
                tab.WaitToBeClickable();
                tab.Click();
                tab.WaitToDisappear();
            }

            var frameToWaitFor = _browser.WaitForElement(frameLocatorToWaitFor, "Frame to wait for");
            Wait.Until(() => frameToWaitFor.Displayed);
        }

        protected bool IsTabSelected(BrowserElement browserElement)
        {
            var classSelectedAttr = browserElement.GetAttribute("class");

            if (classSelectedAttr.Contains("selected"))
                return true;

            return false;
        }
    }
}