﻿using OpenQA.Selenium;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class ServerStatusPanel
    {
        private readonly Browser _browser;
        private readonly BrowserElement _applicationHeader;
        public ServerStatusPanel(BrowserElement applicationHeaderowser)
        {
            _applicationHeader = applicationHeaderowser;
        }

        private string GetVcommStatus()
        {
            return _applicationHeader.WaitForElement(By.Id("vcommIcon"), "Vcomm status").WaitGetAttribute("class");
        }

        private string GetDccStatus()
        {
            return _applicationHeader.WaitForElement(By.Id("dccIcon"), "DCC status").WaitGetAttribute("class");
        }

        public bool IsVcommStatusGreen()
        {
            if (GetVcommStatus().ToLower().Contains("green"))
                return true;
            return false;
        }

        public bool IsDccStatusGreen()
        {
            if (GetDccStatus().ToLower().Contains("green"))
                return true;
            return false;
        }
    }
}
