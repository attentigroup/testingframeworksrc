﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.Common.Resources;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{

    public abstract class WOMPageCommon
    {
        protected readonly Browser _browser;


        protected virtual string mainMenudropDownListClassName => "dropDownList";

        public WOMPageCommon(Browser browser)
        {
            _browser = browser;
        }

        /// <summary>
        /// Used for dropdown lists like the Main menu, Download menu, Reports menu etc.
        /// </summary>
        /// <param name="ParentContainerElement"></param>
        /// <param name="menuButtonLocator"></param>
        /// <param name="optionStringIdentifier_Level1"></param>
        /// <param name="optionStringIdentifier_Level2"></param>
        /// <param name="containerLocator"></param>
        protected void SelectFromDropdownList(BrowserElement ParentContainerElement, By menuButtonLocator, string optionStringIdentifier_Level1, string optionStringIdentifier_Level2 = null, By containerLocator = null, string targetScreenName = "targetScreen", string thisScreenName = "thisScreen", bool IsOpenOnly = false)
        {
            if (IsOpenOnly == false && targetScreenName.Equals(thisScreenName))
                return;

            var selectOption = GetMenuOptionElement(ParentContainerElement, menuButtonLocator, optionStringIdentifier_Level1, optionStringIdentifier_Level2, containerLocator);

            if (selectOption != null)
            {
                var option = selectOption.WaitForElement(By.CssSelector(":first-child"), "Menu option link");
                option.MoveCursorToElement();

                if (IsOpenOnly == false)
                    option.ClickSafely();
            }
        }

        public BrowserElement ApplicationHeader => _browser.WaitForElement(ApplicationHeaderLocator, "WOM page header");
        public By ApplicationHeaderLocator => By.Id("ApplicationHeader");
        internal BrowserElement ApplicationHeaderActionsButtons => _browser.WaitForElement(By.Id("header_action_buttons"), "WOM page header actions buttons");

        /// <summary>
        /// If container and menuButtonLocator are not the same as in 'Action Buttons' put container with valid selector value
        /// </summary>
        /// <param name="containerLocator"></param>
        /// <param name="menuButtonLocator"></param>
        /// <param name="optionStringIdentifier_Level1"></param>
        /// <param name="optionStringIdentifier_Level2"></param>
        /// <returns></returns>
        protected BrowserElement GetMenuOptionElement(BrowserElement ParentContainerElement, By menuButtonLocator, string optionStringIdentifier_Level1, string optionStringIdentifier_Level2 = null, By containerLocator = null)
        {
            BrowserElement containerElement = null;
            BrowserElement menuOptionElement = null;
            BrowserElement menuButton = null;



            menuButton = ParentContainerElement.WaitForElement(menuButtonLocator, "Dropdown option");

            if (optionStringIdentifier_Level1 == null && optionStringIdentifier_Level2 == null)
                return menuButton;

            if (containerLocator != null)
                containerElement = ParentContainerElement.WaitForElement(containerLocator, "Container for dropdown options");
            else
                containerElement = menuButton;

            menuButton.MoveCursorToElement(); //Changed because of issues in version 12.0.0.145 
            //containerElement.MoveCursorToElement();

            var dropdownList = containerElement.WaitForElement(By.ClassName(mainMenudropDownListClassName), "Dropdown list");

            Wait.Until(() => dropdownList.Displayed);

            var optionsRaw = dropdownList.WaitForElements(By.CssSelector($".{mainMenudropDownListClassName}>li"), "Menu option");
            var options = optionsRaw.Where(o => o.WaitForElements(By.CssSelector("*:first-child"), "first child", minElementsToGet: 0).Count != 0).ToList();

            if (options.Any() && (optionStringIdentifier_Level1 != null || optionStringIdentifier_Level2 != null))
            {
                //Open sub menu to return 
                if (optionStringIdentifier_Level2 != null)
                {
                    var option = GetOptionElementFromMenuList(options, optionStringIdentifier_Level1);
                    if (option != null)
                    {
                        option.MoveCursorToElement();

                        var subOptions = option.WaitForElements(By.CssSelector($"ul>li"), "Sub menu option");

                        menuOptionElement = GetOptionElementFromMenuList(subOptions, optionStringIdentifier_Level2);
                    }
                }
                else
                {
                    menuOptionElement = GetOptionElementFromMenuList(options, optionStringIdentifier_Level1);
                }
            }
            return menuOptionElement;
        }

        public BrowserElement GetOptionElementFromMenuList(List<BrowserElement> options, string optionStringIdentifier)
        {
            var select = options.SingleOrDefault(o => o.GetAttribute("outerHTML").ToLower().Contains(optionStringIdentifier.ToLower()));

            return select;
        }

        //POC Security configuration
        //TODO: delete after POC
        private bool IsElementsVisible(SecurityConfigObject secConfigObject)
        {
            return _browser.IsElementVisible(secConfigObject.Locator, $"Security config element {secConfigObject.ElementName}");
        }

        //public ValueTuple<bool,By> IsElementVisible(params By[] by)
        //{
        //    return _browser.AreAllElementsVisible(1, by);
        //}

        public List<SecurityConfigObject> CheckVisibility(string senderMethod, List<List<SecurityConfigObject>> secConfigListCollection)
        {
            var resultList = new List<SecurityConfigObject>();
            foreach (var list in secConfigListCollection)
            {
                var methodName = GetScreenAndSectionName(list.First().ElementName);
                if (methodName != senderMethod)
                    continue;
                foreach (var item in list)
                {
                    item.SecurityConfigMethodName = senderMethod;
                    item.ActualVisibleEnabledExposed = IsElementsVisible(item);
                    resultList.Add(item);
                }
            }
            return resultList;
        }



        private string GetScreenAndSectionName(string elementName)
        {
            var parts = elementName.Split('_');
            return parts[0] + "_" + parts[1];
        }

        public List<ConfigurationTestMethod> GetSecurityConfigMethods()
        {
            var secConfigMethodList = new List<ConfigurationTestMethod>();
            var tp = GetType();
            var methods = GetType().GetMethods().Where(methodInfo => methodInfo.GetCustomAttributes(typeof(SecurityConfigurationAttribute), true).Length > 0);

            foreach (var method in methods)
            {
                secConfigMethodList.Add(new ConfigurationTestMethod() { MethodName = method.Name, Method = method });
            }

            return secConfigMethodList;
        }

        protected List<List<SecurityConfigObject>> GetSecurityConfigResults(List<List<SecurityConfigObject>> secConfigListCollection, List<ConfigurationTestMethod> securityConfigMethods)
        {
            var resultsList = new List<List<SecurityConfigObject>>();
            foreach (var method in securityConfigMethods)
            {
                List<SecurityConfigObject> result = (List<SecurityConfigObject>)method.Method.Invoke(this, new object[] { secConfigListCollection });
                resultsList.Add(result);
            }

            return resultsList;
        }
       
        public SecurityConfigObject GetSecurityResult(string APIString, EnmModifyOption modifyOption = EnmModifyOption.ShowHideOnly, bool isMoveCursorToElement = true)
        {
            ValueTuple<bool, By> isVisibleEnabledExposed;

            By[] locators;

            if (modifyOption != EnmModifyOption.ShowHideOnly)
            {
                try
                {
                    locators = new By[] { SecurityLocators.SecurityApiToLocator[APIString] };
                }
                catch
                {
                    throw new Exception($"Element API {APIString} is not mapped in the dictionary");
                }
            }
            else
                locators = new By[] { By.CssSelector($"[data-modify='{APIString}']"), By.CssSelector($"[res\\:text='{APIString}']"), By.CssSelector($"[colname='{APIString}']"), By.CssSelector($"[sys-security='{APIString}']") };

            isVisibleEnabledExposed = _browser.AreAllElementsVisible(modifyOption, 1, isMoveCursorToElement, locators);                
                
            return new SecurityConfigObject() { ModifyOption = modifyOption, ActualVisibleEnabledExposed = isVisibleEnabledExposed.Item1, Locator = isVisibleEnabledExposed.Item2 };
        }

        
    }
}
