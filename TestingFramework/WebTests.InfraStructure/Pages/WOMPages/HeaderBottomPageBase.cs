﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public abstract class HeaderBottomPageBase : WOMPageCommon
    {
        protected BrowserElement _container;
        protected BrowserElement _actionsButtonsContainer;
        public HeaderBottomPageBase(Browser browser) : base(browser)
        {
            _container = _browser.WaitForElement(By.ClassName("ApplicationHeader-HeaderBottom"), "Header bottom");
            _actionsButtonsContainer = GetActionsButtonsContainer();
        }

        protected virtual BrowserElement GetActionsButtonsContainer()
        {
            return _container.WaitForElement(By.Id("header_action_buttons"), "Header actions buttons");
        }

        public BrowserElement GetActionsButton()
        {
            return _actionsButtonsContainer.WaitForElement(GetActionsButtonLocator(), "Actions button");
        }

        public BrowserElement GetDownloadButton()
        {
            return _actionsButtonsContainer.WaitForElement(GetDownloadButtonLocator(), "Download button");
        }

        protected abstract By GetActionsButtonLocator();

        public BrowserElement GetOffenderActionsButton()
        {
            return _actionsButtonsContainer.WaitForElement(GetOffenderActionsButtonLocator(), "Offender actions button");
        }
        protected abstract By GetOffenderActionsButtonLocator();

        public virtual void DownloadNow()
        {
            //TODO: Move overriden method from AddNewOffenfderPage
            //Under testing. check if is working or not
            var menuButton = _actionsButtonsContainer.WaitForElement(By.Id("download_menu"), "button download");
            menuButton.MoveCursorToElement();
            menuButton.WaitForElement(By.CssSelector("#download_now_btn>div"), "download now").MoveCursorToElement(true);
            //
            SelectFromDropdownList(ApplicationHeader, GetDownloadButtonLocator(), "Button_DownloadNow");
        }
        //TODO check overriden method from AddNewOffenfderPage
        public virtual void DownloadNextCall()
        {
            SelectFromDropdownList(ApplicationHeader, GetDownloadButtonLocator(), "Button_DownloadNextCall");
        }

        protected virtual By GetDownloadButtonLocator()
        {
            return By.CssSelector("#download_menu button");
        }
    }
}
