﻿using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class ChangePasswordPopup
    {
        private readonly Browser _browser;
        //private readonly BrowserElement _container;
        public ChangePasswordPopup(Browser browser)
        {
            _browser = browser;
            //_container = _browser.WaitForElement(By.Id("PopupContainer"), "Change Password Popup");
            Wait.Until(() => Container.Displayed);

        }


        private BrowserElement Container => _browser.WaitForElement(By.Id("PopupContainer"), "Change Password Popup");
        

        internal void FillDetails(User user)
        {
            var textBoxes = Container.WaitForElements(By.ClassName("change-password-validation"), "Text Box");
            textBoxes[0].Text = user.Password;
            textBoxes[1].Text = user.NewPassword;
            textBoxes[2].Text = user.NewPassword;
            var SaveBtn = Container.WaitForElement(By.Id("passwordSaveBtn"), "Save button");
            SaveBtn.Click();
            SaveBtn.WaitToDisappear();
            var OkBtn = Container.WaitForElement(By.Id("passwordInputCloseBtn"), "OK Button");
            OkBtn.WaitToBeClickable();
            OkBtn.Click();
            Container.WaitToDisappear();
        }
    }
}
