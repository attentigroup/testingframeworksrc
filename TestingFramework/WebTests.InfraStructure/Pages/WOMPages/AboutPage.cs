﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class AboutPage
    {
        protected readonly Browser _browser;

        private BrowserElement container;
        protected BrowserElement _container
        {
            get
            {
                container =  _browser.WaitForElement(By.CssSelector(".infoTemp.ApplicationHeader-InfotooltipOpener"), "Info icon container", 5);
                Wait.Until(() => container.Displayed, seconds:3, failIfConditionNotMet:false);
                return container;
            }
        }
        
        public AboutPage(Browser browser)
        {
            _browser = browser;
        }

        //TODO complete this method to return VersionInfo object
        public void GetAboutInfo()
        {
            OpenAboutTooltip();
            var webVersionElement = _container.WaitForElement(By.Id("headerAboutToolTipWebVersion"), "Web version");
            Wait.Until(() => webVersionElement.Displayed);
            var webVersionT = webVersionElement.Text;
            var dbVersionT = _container.WaitForElement(By.Id("headerAboutToolTipDBVersion"), "DB version").Text;
            var dbServerT = _container.WaitForElement(By.Id("headerAboutToolTipDataSourceName"), "DB server").Text;
        }

        public void OpenAboutTooltip()
        {
            try
            {
                _container.MoveCursorToElement();
            }
            catch { }
        }
        //var info = _applicationHeader.WaitForElement(By.CssSelector(".infoTemp.ApplicationHeader-InfotooltipOpener"), "Version info");
        
        
    }
}
