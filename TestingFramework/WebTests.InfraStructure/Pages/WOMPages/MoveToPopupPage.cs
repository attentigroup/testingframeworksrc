﻿using OpenQA.Selenium;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class MoveToPopupPage
    {
        private Browser _browser;
        private BrowserElement _container;
        public MoveToPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Move to agency popup");
            Wait.Until(() => _container.Displayed);
        }

        internal string SelectAgency()
        {
            var agencyDropDown = _container.WaitForElement(By.Id("addToAgencySelect"), "Add to agency");

            var agency = agencyDropDown.SelectFromComboboxByIndex(1);

            string agencyName = agency.Text;

            var okBtn = _container.WaitForElement(By.CssSelector(".buttons>button>div>span"), "ok button");

            okBtn.Click();

            _container.WaitToDisappear();

            return agencyName;
        }
    }
}
