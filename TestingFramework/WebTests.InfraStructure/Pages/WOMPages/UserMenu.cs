﻿using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class UserMenu : WOMPageCommon
    {
        public UserMenu(Browser browser): base(browser)
        {
            OpenUserMenu();
        }

        protected void OpenUserMenu()
        {
            _browser.WaitForElement(By.TagName("body"), "body").Click();
            ApplicationHeader.WaitForElement(By.Id("userNameSpan"), "User menu").MoveCursorToElement();
            var menu = ApplicationHeader.WaitForElement(By.CssSelector(".dropDownList.ApplicationHeader-HeaderUserNamedropDownList"), "User menu");
            Wait.Until(() => menu.Displayed);
        }

        public void Logout()
        {
            Logger.WriteLine($"Logging out from {_browser.Title}");
            var btnLogout = ApplicationHeader.WaitForElement(By.CssSelector(".dropDownList.ApplicationHeader-HeaderUserNamedropDownList>li[data-modify='General_Toolbar_Button_Logout']"), "Log out");

            btnLogout.ClickSafely();
        }

        public bool IsVcommConnected()
        {
            return new ServerStatusPanel(ApplicationHeader).IsVcommStatusGreen();
        }
        public bool IsDccConnected()
        {
            return new ServerStatusPanel(ApplicationHeader).IsDccStatusGreen();
        }

        public void ChangeUserPassword(User user)
        {
            var popup = ChangePassword();
            popup.FillDetails(user);
        }

        private ChangePasswordPopup ChangePassword()
        {
            var btnChangePassword = ApplicationHeader.WaitForElement(By.CssSelector(".dropDownList.ApplicationHeader-HeaderUserNamedropDownList>li[data-modify='General_AboutToolTip_Button_ChangePassword']"), "Change Password");

            btnChangePassword.ClickSafely();

            return new ChangePasswordPopup(_browser);
        }
    }
}
