﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public abstract class WOMTableBase : WOMPage
    {
        public WOMTableBase(Browser browser) : base(browser)
        {
           
        }
        
        protected void WaitForContentToLoad()
        {
            try
            {
                var container = _browser.WaitForElement(GetContentContainerLocator(), "Content container", 5);
                Wait.Until(() => container.Displayed);
            }
            catch { }
        }

        //
        public BrowserElement GetContentContainerElement()
        {
            return _browser.WaitForElement(GetContentContainerLocator(), "Content container");
        }


        protected abstract By GetContentContainerLocator();

        protected virtual By GetSorterLocator()
        {
            return By.CssSelector(".groupDetails-data.groupDetails-data-Header.sortable.center.groupDetails-data-icon");
        }

        public bool IsTableListGridHeaderExist()
        {
            if (GetTableGridHeadersElements().Any())
                return true;

            return false;
        }

        

        protected List<BrowserElement> GetTableGridHeadersElements()
        {
            var headers = _browser.WaitForElements(GetTableHeaderGridSelector(), "table list grid header", minElementsToGet: 0, explicitWaitForSingleElement: 1);

            return headers;
        }

        protected virtual By GetTableHeaderGridSelector()
        {
            return By.CssSelector("#tblHeader>div");
        }

        public BrowserElement GetTableHeaderElement()
        {
            return _browser.WaitForElement(By.Id("tblHeader"), "Table header");
        }

        protected virtual string GetTableRowCssSelectorString()
        {
            return "#tableContainer .row[style]";
        }

        protected virtual string GetTableSearchCssSelectorString()
        {
            return "#searchBox input";
        }

        protected virtual string GetTableSearchButtonCssSelectorString()
        {
            return "#searchBtn";
        }

        //protected virtual string SortArrowCssSelector()
        //{
        //    return ".general-table-header-titlePadding  .general-table-header-sortable";
        //}

        /// <summary>
        /// Get table row element by 1 based index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected virtual BrowserElement GetTableRowElementByNumber(int rowNumber)
        {
            if (rowNumber <= 0)
                throw new Exception("Row index must be greater than 0");

            var combinedCss = GetTableRowCssSelectorString()+ $":nth-of-type({rowNumber})";

            var tableRow = _browser.WaitForElements(By.CssSelector(combinedCss), "table rows", minElementsToGet:1, explicitWaitForSingleElement:3);
            
            return tableRow.FirstOrDefault();
        }

        [Obsolete("Please consider using the method GetTableRowElementByIndex(rowIndex) if you need a single row")]
        protected List<BrowserElement> GetTableRowsElements()
        {
            var eventTableRows = _browser.WaitForElements(By.CssSelector(GetTableRowCssSelectorString()), "Table rows", minElementsToGet: 0);

            return eventTableRows;
        }

        /// <summary>
        /// Basic table search by string
        /// </summary>
        /// <param name="searchText"></param>
        protected void Search(object searchText)
        {
            var firstRow = GetTableRowElementByNumber(1);

            var searchBox = _browser.WaitForElement(By.CssSelector(GetTableSearchCssSelectorString()), "Search box");
            
            searchBox.Text = searchText.ToString();

            var btnSearch = _browser.WaitForElement(By.CssSelector(GetTableSearchButtonCssSelectorString()), "Search button");

            btnSearch.WaitToBeClickable();

            btnSearch.ClickSafely();

            firstRow.WaitForStailnessOfElement();
        }

        BrowserElement btnSort => _browser.WaitForElement(GetSorterLocator(), "Sort Arrow");

        public void SortByColumn(bool isLowToHigh)
        {
            var expectedOrder = isLowToHigh == true ? "asc" : "desc";
            string actualOrder;
            string sortAttribute = "data-sorting";

            do
            {
                var orderBefore = btnSort.GetAttribute(sortAttribute);

                btnSort.WaitToBeClickable();

                btnSort.Click();

                actualOrder = btnSort.WaitGetAttribute(sortAttribute);

                Wait.Until(() => expectedOrder != orderBefore);
            }
            while (actualOrder != expectedOrder);

        }

        protected void ClearSearch()
        {
            //var firstRow = GetTableRowElementByNumber(1);

            var clearBtn = _browser.WaitForElement(By.CssSelector("._clear_result_button.search-clear-result-button"), "");

            clearBtn.WaitToBeClickable();

            clearBtn.ClickSafely();

            //firstRow.WaitForStailnessOfElement();
        }


        /// <summary>
        /// Use if columns are not identified in the DOM. 'tableTitles' should be the exact column header text.
        /// The 'type' object should have properties named exactly as the column header without spaces in between
        /// </summary>
        /// <param name="rowNumber"></param>
        /// <param name="type"></param>
        /// <param name="tableTitles"></param>
        /// <returns></returns>
        protected object GetEquipmentRowDetailsUsingTableHeader(int rowNumber, Type type, params string[] tableTitles)
        {
            var row = GetTableRowElementByNumber(rowNumber);
            var rowCols = row.WaitForElements(By.CssSelector(".td.ellipsis"), "Column");

            var rowDic = GetColumnsByHeader(1, tableTitles);

            var myObject = Activator.CreateInstance(type);
            foreach (var item in rowDic)
            {

                var prop = myObject.GetType().GetProperty(item.Key.Replace(" ", ""), BindingFlags.Public | BindingFlags.Instance);
                if (prop != null && prop.CanWrite)
                {
                    prop.SetValue(myObject, rowCols[item.Value].Text);
                }
                else
                {
                    Logger.WriteLine($"There is no property for '{item.Key.ToString()}' in {myObject.GetType().Name}");
                }
            }

            return myObject;
        }

        protected Dictionary<string, int> GetColumnsByHeader(int rowNumber, params string[] titles)
        {
            var headerDic = new Dictionary<string, int>();
            //find headers
            var tableHeaderElements = _browser.WaitForElements(GetTableHeaderSelector(), "Table headers");
            var headerTitles = tableHeaderElements/*.Where(h => h.Displayed)*/.Select(h => h.GetAttribute("title")).ToList();

            foreach (var title in titles)
            {
                headerDic.Add(title, headerTitles.IndexOf(title));
            }

            return headerDic;
        }

        protected virtual By GetTableHeaderSelector()
        {
            //return By.CssSelector(".body._head thead>tr td");
            return By.CssSelector("#tblHeader>div");
        }

        /// <summary>
        /// Gets the number of rows as indicated in the pager section
        /// </summary>
        /// <returns></returns>
        protected string GetAmountOfRows()
        {
            return new WOMTablePagingPage(_browser).GetTotalAmountOfRows();
        }

        protected void FilterByRangeOfDates(DateTime startTime, DateTime endTime)
        {
            var filterByTimeControl = _browser.WaitForElement(By.Id("filterByTimeSectionCustom"), "");

            SelectDateAndTime(startTime, "fromDate", "fromTime");
            SelectDateAndTime(endTime, "toDate", "toTime");
           
            var goBtn = filterByTimeControl.WaitForElements(By.TagName("button"), "");
            goBtn.FirstOrDefault().Click();

            GetContentContainerElement().WaitToDisappear();
            GetContentContainerElement();
        }

        public void ExpandRow(int rowNumber)
        {
            var row = GetTableRowElementByNumber(rowNumber);

            var expandIcon = row.WaitForElement(By.ClassName("expand_icon"), "Expand icon");

            expandIcon.MoveCursorToElement(true);

            Thread.Sleep(500);//due to expand animation
        }

        protected BrowserElement GetExpandedRowContentContainer()
        {
            return _browser.WaitForElement(GetExpandedRowContentContainerLocator(), "Expanded row content container");
        }

        protected virtual By GetExpandedRowContentContainerLocator()
        {
            return By.CssSelector(".general-table-body-InfoBox-box.third");
        }
       

        //TODO: move to WOMPageCommon
        public void SelectDateAndTime(DateTime time, string dateId, string timeId)
        {
            var fromDate = _browser.WaitForElement(By.Id(dateId), "");
            fromDate.Click();
            fromDate.SendKeys(Keys.Home);
            fromDate.SendKeys(time.ToString("dd/MM/yyyy"));

            var fromTime = _browser.WaitForElement(By.Id(timeId), "");
            //fromTime.Click();
            fromTime.SendKeys(Keys.Home);
            fromTime.SendKeys(time.ToString("HH:mm"));
        }

        /// <summary>
        /// Filter by time period. recent = html 'value'. For maximum available period enter recent = 'max'
        /// </summary>
        /// <param name="recent"></param>
        public void FilterByPeriod(string recent)
        {
            var timeFilter = _browser.WaitForElement(GetFilterPeriodLocator(), "");
            if (recent == "max")
                timeFilter.SelectFromComboboxByIndex(timeFilter.GetOptionList().Count - 1);
            else
            timeFilter.SelectFromComboboxByValue(recent);

            GetContentContainerElement().WaitToDisappear();
            GetContentContainerElement();
        }

        protected virtual By GetFilterPeriodLocator()
        {
            return By.CssSelector("select[id^= 'filterByTime']");
        }
        public List<ValueTuple<int, int>> GetShownResultsPerPage(int resultsNumberToShow)
        {
            var shownRowsList = new List<ValueTuple<int, int>>();

            var pagingContainer = new WOMTablePagingPage(_browser);

            pagingContainer.ShowResults(resultsNumberToShow);

            do
            {
                Thread.Sleep(250);//To slow down the loop
                var shownRowsString = pagingContainer.GetShownAmountOfRows();

                var parts = shownRowsString.Split('-');

                var from = int.Parse(parts.First());

                var to = int.Parse(parts.Last());

                shownRowsList.Add((from, to));
            }
            while (pagingContainer.MoveToNextPage() == false);

            return shownRowsList;
        }

        public bool PagingAllPages()
        {
            var pagingContainer = new WOMTablePagingPage(_browser);

            return pagingContainer.ClickOnNextButtonForAllPages();

        }

        protected BrowserElement GetClickableColumn(BrowserElement tableRow)
        {
            return tableRow.WaitForElement(By.CssSelector(GetClickableColumnCssSelectorString()), "clickable column");
        }
        protected virtual string GetClickableColumnCssSelectorString()
        {
            return "div[class~='offNavigate']";
        }
    }
}
