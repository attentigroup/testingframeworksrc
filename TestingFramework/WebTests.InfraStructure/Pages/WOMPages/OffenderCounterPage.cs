﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class OffenderCounterPage
    {
        private readonly Browser _browser;
        private BrowserElement _Container;
        public OffenderCounterPage(Browser browser)
        {
            _browser = browser;
            _browser.WaitForElementsOR(10, GetContainerLocator(), GetHandleLocator());
        }

        private By GetHandleLocator()
        {
            return By.ClassName("CountersDock");
        }

        private By GetContainerLocator()
        {
            return By.CssSelector(".Counters.small.ui-draggable");
        }

        List<BrowserElement> container => _browser.WaitForElements(GetContainerLocator(), "Counter container", minElementsToGet: 0, explicitWaitForSingleElement: 5);

        public void CloseCounter()
        {
            var webDriver = _browser.GetWebDriver();

            var jse = webDriver as IJavaScriptExecutor;

            var element = container.Any() ? container[0].GetWebElement() : null;

            if(element != null)
            jse.ExecuteScript("arguments[0].style.visibility = 'hidden';", element);
        }

        public void OpenCounterIfClosed()
        {
            if (Container == null)
            {
                if (CounterDockElement != null && CounterDockElement.Displayed)
                {
                    CounterDockElement.Click();
                    Thread.Sleep(500);//wait for animation
                }
            }
            _Container = Container;
        }

        public BrowserElement CounterDockElement
        {
            get {

                var element = _browser.WaitForElements(GetHandleLocator(), "Counter handle", minElementsToGet: 0, explicitWaitForSingleElement: 2);
                return element.Any() ? element.First() : null;
            }
        }

        public BrowserElement Container
        {
            get
            {
                return (container.Any() && container.First().Displayed) ? container.First() : null;
            }
        }

    }
}
