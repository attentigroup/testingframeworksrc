﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    interface IWOMTable
    {
        By GetContentContainerLocator();
    }
}
