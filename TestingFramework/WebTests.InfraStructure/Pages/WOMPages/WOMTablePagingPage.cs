﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.WOMPages
{
    public class WOMTablePagingPage
    {
        protected readonly Browser _browser;

        public WOMTablePagingPage(Browser browser)
        {
            _browser = browser;
        }

        protected BrowserElement Container => _browser.WaitForElement(By.Id("pagingContainerWrapId"), "pagingContainerWrapId");

        BrowserElement NextButton => Container.WaitForElement(By.CssSelector(".general-table-page_arrows.next"), "next button");

        List<BrowserElement> PagesButtons => Container.WaitForElements(By.CssSelector(".general-table-pagingListContainer>ul>li"), "Pager page buttons");

        BrowserElement AmountOfRowsLineElement => Container.WaitForElement(By.CssSelector(".amountOfRows._amountOfRows"), "Amount Of Rows");

        public List<BrowserElement> GetPagesButtonsContainer()
        {
            return PagesButtons;
        }

        public string GetTotalAmountOfRows()
        {
            Wait.Until(() => AmountOfRowsLineElement.Text != string.Empty);

            string amountOfRowsLineText = AmountOfRowsLineElement.Text;

            Logger.WriteLine($"Total amount Of rows displayed: {amountOfRowsLineText}");

            return amountOfRowsLineText.Split(new string[] { "of" }, StringSplitOptions.RemoveEmptyEntries).Last().Trim();
        }

        public string GetShownAmountOfRows()
        {
            Wait.Until(() => AmountOfRowsLineElement.Text != string.Empty);
            return AmountOfRowsLineElement.Text.Split(new string[] { "of" }, StringSplitOptions.RemoveEmptyEntries).First();
        }

        public void ShowResults(int resultsNumberToShow)
        {
            var showResultsCombobox = Container.WaitForElement(By.Id("numResults"), "Show results combobox");
            Wait.Until(() => showResultsCombobox.Displayed);
            showResultsCombobox.SelectFromComboboxByText(resultsNumberToShow.ToString());
        }

        public BrowserElement[] GetPagesButtons()
        {
            Wait.Until(() => PagesButtons.First().Displayed && PagesButtons.First().Enabled);
            return PagesButtons.ToArray();
        }

        /// <summary>
        /// Returns true when last page in pager is selected
        /// </summary>
        /// <returns></returns>
        public bool MoveToNextPage()
        {
            try
            {
                var firstButton = PagesButtons.First();
                Wait.Until(() => firstButton.Displayed && firstButton.Enabled);
                var selectedIndex = GetSelectedIndex(PagesButtons);

                if (selectedIndex >= PagesButtons.Count - 1)
                    return true;
                else
                {
                    //TODO: replace with wait. not easy
                    Thread.Sleep(500);

                    PagesButtons[selectedIndex + 1].WaitToBeClickable();

                    PagesButtons[selectedIndex + 1].Click();

                    Wait.Until(() => PagesButtons[selectedIndex + 1].WaitGetAttribute("class").Contains("next"), $"Pager button not clicked", 10);

                    if (!PagesButtons[selectedIndex + 1].WaitGetAttribute("class").Contains("next"))
                        return MoveToNextPage();
                }

                return false;
            }
            catch(StaleElementReferenceException)
            {
                return MoveToNextPage();
            }
        }

        public bool ClickOnNextButtonForAllPages()
        {
            int ctr = 0;

            while (!NextButton.GetAttribute("class").Contains("disable"))
            {
                //Thread.Sleep(500);

                NextButton.WaitToBeClickable();

                NextButton.Click();
                ctr++;
            }
            return ctr > 0;
        }


        protected int GetSelectedIndex(List<BrowserElement> pagesButtons)
        {
            int selectedIndex = 0;

            for (int i = 0; i < pagesButtons.Count; i++)
            {
                var attr = pagesButtons[i].WaitGetAttribute("class");
                if (attr.Contains("selected"))
                {
                    selectedIndex = i;
                    break;
                }
            }
            return selectedIndex;
        }
    }
}
