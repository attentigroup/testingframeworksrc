﻿using OpenQA.Selenium;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class OpenSessionsPage : WOMPage
    {
        public OpenSessionsPage(Browser browser) : base(browser)
        {

        }

        internal CurrentOpenSessionsTabPage CurrentOpenSessionsTab => new CurrentOpenSessionsTabPage(_browser);
        internal StatisticsTabPage StatisticsTab => new StatisticsTabPage(_browser);
        

        internal class CurrentOpenSessionsTabPage : StatisticsTabBase
        {
            public CurrentOpenSessionsTabPage(Browser browser) : base(browser)
            {
                var tab = _browser.WaitForElement(By.CssSelector(GetTabsMenuIdString() + ">li:first-child"), "Open session tab");
                GoToTab(tab, ContentContainerLocator);
            }

            private BrowserElement ContentContainer => _browser.WaitForElement(ContentContainerLocator, "Open sessions content container");
            private By ContentContainerLocator => By.Id("sessions");

            protected override By GetContentContainerLocator()
            {
                return By.CssSelector("#sessions>table");
            }
        }

        internal class StatisticsTabPage : StatisticsTabBase
        {
            public StatisticsTabPage(Browser browser) : base(browser)
            {
                var tab = _browser.WaitForElement(By.CssSelector(GetTabsMenuIdString() + ">li:last-child"), "Open session tab");
                GoToTab(tab, ContentContainerLocator);
            }

            BrowserElement ContentContainer => _browser.WaitForElement(By.Id("statistics"), "Statistics content container");
            By ContentContainerLocator => By.Id("statistics");

            protected override By GetContentContainerLocator()
            {
                throw new System.NotImplementedException();
            }
        }

        internal abstract class StatisticsTabBase : WOMTableBase, IWOMTab
        {
            public StatisticsTabBase(Browser browser) : base(browser)
            {

            }

            public string GetTabsMenuIdString()
            {
                return ".sessions-tabs-menu";
            }
        }
    }
}