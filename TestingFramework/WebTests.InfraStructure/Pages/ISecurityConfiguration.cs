﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    interface ISecurityConfiguration
    {
        bool AreElementsVisible(List<By> locatorCollection);
    }
}
