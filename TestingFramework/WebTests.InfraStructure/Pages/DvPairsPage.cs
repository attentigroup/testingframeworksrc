﻿using OpenQA.Selenium;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class DvPairsPage : WOMPage
    {
        public DvPairsPage(Browser browser) : base(browser)
        {
        }

        internal DvPairsActionMenuPage DvPairsPageActionMenu => new DvPairsActionMenuPage(_browser);

        public void Refresh()
        {
            DvPairsPageActionMenu.Refresh();
        }

        internal class DvPairsActionMenuPage : GeneralActionsMenuBasePage
        {
            public DvPairsActionMenuPage(Browser browser) : base(browser)
            {

            }

            protected override By GetRefreshButtonSelector()
            {
                return By.CssSelector("button[skin='button square refresh']");
            }


            protected override By GetSaveButtonSelector()
            {
                throw new System.NotImplementedException();
            }
        }
    }
}