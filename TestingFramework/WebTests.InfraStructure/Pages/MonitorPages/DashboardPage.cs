﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class DashboardPage : WOMPage
    {
        public DashboardPage(Browser browser):base(browser)
        {

        }

        DashboardTablePage DashboardTable => new DashboardTablePage(_browser);

        internal void GetTableRows()
        {
            var tableElements = DashboardTable.GetAllRows();
        }



        //*** internal classes ***
        internal class DashboardTablePage : WOMTableBase
        {
            private BrowserElement _container;

            public DashboardTablePage(Browser browser) : base(browser)
            {
                _container = _browser.WaitForElement(By.ClassName("right-wrapper"),"Table container");

                var contentContainer = _browser.WaitForElement(GetContentContainerLocator(), "Content container");
                Wait.Until(() => contentContainer.Displayed);
            }
            

            protected override By GetContentContainerLocator()
            {
                return By.ClassName("right-table-wrap");
            }

            internal List<BrowserElement> GetAllRows()
            {
                return GetTableRowsElements();
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#result_table>tr";
            }
        }
    }
}
