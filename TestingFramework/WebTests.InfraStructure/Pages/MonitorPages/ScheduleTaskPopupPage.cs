﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class ScheduleTaskPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public ScheduleTaskPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Remark popup");

        }
        public void AddScheduleTask(DateTime time)
        {
            time = DateTime.Now.AddMinutes(2);
            
            var scheduleTypeCombobox = _container.WaitForElement(By.Id("scheduleTypeField"), "");
            scheduleTypeCombobox.SelectFromComboboxByIndex(1);

            var reasonCombobox = _container.WaitForElement(By.Id("scheduleSubTypeField"), "");
            reasonCombobox.SelectFromComboboxByIndex(1);

            var day = time.Day.ToString();

            var dueDate = _container.WaitForElement(By.CssSelector(".event-handling-validation.hasDatepicker"), "Due date");
            dueDate.Click();
            var table = _browser.WaitForElement(By.Id("ui-datepicker-div"), "date picker");
            List<BrowserElement> columns = table.WaitForElements(By.TagName("td"), "date picker cells");

            foreach (var cell in columns)
            {
                if (cell.Text.Equals(day))
                {
                    cell.Click();
                    break;
                }
            }
            table.WaitToDisappear();

            string dueTimeString = time.ToString("HH:mm"); 
            var dueTime = _container.WaitForElement(By.CssSelector("#timeInput0_0"), "due time");
            dueTime.Click();
            dueTime.SendKeys(Keys.Home);
            dueTime.SendKeys(dueTimeString);

            var reminderCheckbox = _container.WaitForElement(By.ClassName("checkbox"), "reminder");
            reminderCheckbox.Click();

            var comment = _container.WaitForElement(By.Id("actionCommentTxtArea"), "comment");
            comment.Text = "schedule task";

            var buttons = _container.WaitForElements(By.ClassName("button"), "ok button");
            var okBtn = buttons[1];
            okBtn.Click();
        }
    }
}
