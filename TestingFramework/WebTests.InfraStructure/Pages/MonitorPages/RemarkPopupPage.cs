﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class RemarkPopupPage : PopupPageBase
    {
        
        public RemarkPopupPage(Browser browser):base(browser)
        {
        }

        public void Remark()
        {
            var comment = _container.WaitForElement(By.Id("actionCommentTxtArea"), "");
            comment.Text = "Automation Remark" + DateTime.Now;

            var buttons = _container.WaitForElements(By.ClassName("button"), "");
            var okBtn = buttons[1];
            okBtn.Click();
        }
    }
}
