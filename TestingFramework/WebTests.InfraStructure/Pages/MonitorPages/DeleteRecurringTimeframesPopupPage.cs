﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class DeleteRecurringTimeframesPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public DeleteRecurringTimeframesPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "Delete Recurring Time Frames Popup");
        }

        public void OK()
        {
            var buttons = _container.WaitForElements(By.TagName("button"), "Buttons");
            var okBtn = buttons[0];
            okBtn.Click();
        }
    }
        
}
