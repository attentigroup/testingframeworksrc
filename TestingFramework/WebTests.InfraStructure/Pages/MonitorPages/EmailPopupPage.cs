﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class EmailPopupPage 
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public EmailPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Remark popup");

        }

        public void SendEmail()
        {
            var combobox = _browser.WaitForElement(By.XPath("//input[@id='selectedUserField']/.."), "");
            var name = combobox.SelectFromCustomComboboxByIndex(By.TagName("input"), By.CssSelector("ul>li"), 0);

            var email = _container.WaitForElement(By.Id("emailAddressField"), "");
            email.Text = "asd@dfg.hjk";

            var comment = _container.WaitForElement(By.Id("actionCommentTxtArea"), "");
            comment.Text = "mail";

            var buttons = _container.WaitForElements(By.ClassName("button"), "");
            var okBtn = buttons[1];
            okBtn.Click();

        }       


    }
}
