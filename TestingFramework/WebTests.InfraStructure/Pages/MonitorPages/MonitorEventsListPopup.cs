﻿using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class MonitorEventsListPopup : PopupPageBase
    {
        public MonitorEventsListPopup(Browser browser):base (browser)
        {
            Wait.Until(()=> _container.Displayed);
        }
    }
}