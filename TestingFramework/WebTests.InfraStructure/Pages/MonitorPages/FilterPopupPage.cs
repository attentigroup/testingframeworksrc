﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class FilterPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public FilterPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Filter popup");
        }

        public void CreateProgramTypeFilter(string [] programTypes, string filterName)
        {
            SetFilterName(filterName);
            BrowserElement FilterByCombobox = SetFilterBy("Program Type");
            SetFilterOperator(FilterByCombobox, "In");

            SelectFromCheckboxesCombobox(programTypes);
            PressOkButton(1);

        }

        private void PressOkButton(int index)
        {
            var buttons = _container.WaitForElements(By.TagName("button"), "");
            buttons[index].Click();
        }

        internal void CreateAgencyIdFilter(string agencyId)
        {
            SetFilterName("Agency Filter");
            BrowserElement FilterByCombobox = SetFilterBy("Agency ID");
            SetFilterOperator(FilterByCombobox, "Equal");

            var agency = _container.WaitForElement(By.Id("TxtValueOf0"), "AgencyId text box");
            agency.Text = agencyId;

            PressOkButton(0);
        }

        internal void CreateOffenderNameFilter(string offenderName, string filterName)
        {
            SetFilterName(filterName);
            BrowserElement FilterByCombobox = SetFilterBy("Offender Name");
            SetFilterOperator(FilterByCombobox, "Contains");

            var offender = _container.WaitForElement(By.Id("TxtValueOf0"), "AgencyId text box");
            offender.Text = offenderName;

            PressOkButton(0);
        }

        internal void CreateOfficerNameFilter(string officerName, string filterName)
        {
           // SetFilterName("Officer Name Filter");
            SetFilterName(filterName);
            BrowserElement FilterByCombobox = SetFilterBy("Officer Name");
            SetFilterOperator(FilterByCombobox, "Contains");

            _container.SelectFromCustomComboboxByText("TxtValueOf0", officerName);

            PressOkButton(0);
        }

        internal void CreateSeverityFilter(string severity)
        {
            SetFilterName("Severity Filter");
            BrowserElement FilterByCombobox = SetFilterBy("Severity");
            SetFilterOperator(FilterByCombobox, "In");
            SelectFromCheckboxesCombobox(new string[] { severity });

            PressOkButton(1);
        }

        internal void CreateEventIDFilter(string eventID)
        {
            SetFilterName("EventID Filter");
            BrowserElement FilterByCombobox = SetFilterBy("Event ID");
            SetFilterOperator(FilterByCombobox, "Less Than or Equal");

            var eventIDTextbox = _container.WaitForElement(By.Id("TxtValueOf0"), "eventID text box");
            eventIDTextbox.Text = eventID;

            PressOkButton(0);
        }

        internal void CreateEventMessageUserFilter(string eventMessage)
        {
            SetFilterName("EventMessage Filter");
            BrowserElement FilterByCombobox = SetFilterBy("Event Message");
            SetFilterOperator(FilterByCombobox, "In");

            //_container.SelectFromCustomComboboxByText("TxtValueOf0", eventMessage);
            var eventIDTextbox = _container.WaitForElement(By.Id("TxtValueOf0"), "eventID text box");
            eventIDTextbox.Click();
            eventIDTextbox.Text = eventMessage;

            PressOkButton(0);
        }

        private static void SetFilterOperator(BrowserElement FilterByCombobox, string operatorName)
        {
            var operatorCombobox = FilterByCombobox.WaitForElement(By.CssSelector(".field.short>select"), "operatorCombobox");
            operatorCombobox.SelectFromComboboxByText(operatorName);
        }

        private void SetFilterName(string filterName)
        {
            var filterNameTextbox = _container.WaitForElement(By.Id("filterNameField"), "Filter name");
            filterNameTextbox.Text = filterName;
        }

        private BrowserElement SetFilterBy(string filterBy)
        {
            var FilterByCombobox = _container.WaitForElement(By.CssSelector("#filterBox>table>tbody"), "");
            var FilterBy = FilterByCombobox.WaitForElement(By.ClassName("filter-validation"), "Filter by");
            FilterBy.SelectFromComboboxByText(filterBy);
            return FilterByCombobox;
        }

        internal void CreateEventTimeFilter(DateTime eventTime)
        {
            SetFilterName("Event Time Filter");
            BrowserElement FilterByCombobox = SetFilterBy("Event Time");
            SetFilterOperator(FilterByCombobox, "Less Than");

            SelectDateAndTime(eventTime, "date0_0_0_0", "time0_0_0_0");

            PressOkButton(0);
        }

        internal void CreateFilterByEndOfProgramForActiveOffender(DateTime time)
        {
            SetFilterName("Program End Date Filter");

            var operatorCombobox = _container.WaitForElement(By.CssSelector(".field>select"), "operatorCombobox");
            operatorCombobox.SelectFromComboboxByText("Active Offenders");

            BrowserElement FilterByCombobox = SetFilterBy("Program End Date");
            SetFilterOperator(FilterByCombobox, "Greater Than");

            SelectDateAndTime(time, "date0_0_0_0", "time0_0_0_0");

            PressOkButton(0);
        }

        public void SelectDateAndTime(DateTime time, string dateId, string timeId)
        {
            var fromDate = _browser.WaitForElement(By.Id(dateId), "");
            fromDate.Click();
            fromDate.SendKeys(Keys.Home);
            fromDate.SendKeys(time.ToString("dd/MM/yyyy"));

            var fromTime = _browser.WaitForElement(By.Id(timeId), "");
            fromTime.Click();
            fromTime.SendKeys(Keys.Home);
            fromTime.SendKeys(time.ToString("HH:mm"));
        }

        

        private void SelectFromCheckboxesCombobox(string[] selections)
        {
            var selectOptionCombobox = _container.WaitForElement(By.CssSelector(".ui-multiselect.ui-widget.ui-state-default.ui-corner-all"), "Select option");
            selectOptionCombobox.Click();
            var dropDownList = _container.WaitForElement(By.CssSelector(".ui-multiselect-menu.ui-widget.ui-widget-content.ui-corner-all"), "Dropdown list");
            var selectOptions = dropDownList.WaitForElements(By.TagName("label"), "");
            foreach (var selection in selections)
            {
                var option = selectOptions.FirstOrDefault(x => x.GetAttribute("title") == selection);
                option.Click();
            }

            var comboboxCloseBtn = _container.WaitForElement(By.CssSelector(".ui-icon.ui-icon-circle-close"), "Select option");
            comboboxCloseBtn.Click();
        }

        
    }
    
}
