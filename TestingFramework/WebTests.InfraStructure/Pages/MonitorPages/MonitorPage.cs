﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.MonitorPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;
using static WebTests.InfraStructure.Pages.OffendersListPage;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class MonitorPage : WOMPage
    {
        private static Browser staticBrowser;

        public static BrowserElement _container
        {
            get
            {
                var container = staticBrowser.WaitForElement(By.TagName("body"), "Container");
                new HighPriorityAlertPopup(staticBrowser).ReleaseAlert();
                return container;
            }
        }

        public MonitorPage(Browser browser) : base(browser)
        {
            staticBrowser = _browser;
            var tableHeader = _browser.WaitForElement(By.Id("tableContainer"), "Table header");//tblHeader
            Wait.Until(() => tableHeader.Displayed);
            Wait.Until(() => MonitorTable.GetContentContainerElement().Displayed);
        }
       
        internal MonitorFilterSectionPage MonitorFilterSection => new MonitorFilterSectionPage(_browser);
        internal MonitorTablePage MonitorTable => new MonitorTablePage(_browser);
        internal MonitorPagingPage MonitorPaging => new MonitorPagingPage(_browser);
        internal OffenderPages.AddNewOffenderPage.CurrentStatusTabPage CurrentStatusTabPage => new OffenderPages.AddNewOffenderPage.CurrentStatusTabPage(_browser);

        public bool IsFilterSectionVisible()
        {
            return new MonitorFilterSectionPage(_browser).IsSectionVisible();
        }

        public MonitorTableRow GetEventDetails(string eventId)
        {
            return GetEventDetailsFromTable(eventId);
        }

        internal string[] GetSystemFilters()
        {
            return MonitorFilterSection.GetSystemFilters();
        }

        internal string GetAmountOfRowsByFilter(int sectionNumber, string title)
        {
            var amountOfRows = MonitorTable.GetAmountOfEvents();
            ClickOnMonitorFilters(sectionNumber, title);
            Wait.Until(() => MonitorTable.GetAmountOfEvents() != amountOfRows);
            return MonitorTable.GetAmountOfEvents();
        }

        internal void NavigateToOffenderThroughEventRow(int sectionNumber, string filterTitle, string eventID)
        {
            var filterRowElement = MonitorFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);
            filterRowElement.Click();
            MonitorTable.NavigateToOffenderThroughEventRow(eventID);
        }

        public string GetAmountOfElementsInSpecificFilter(int sectionNumber, string filterTitle)
        {
            var filterRowElement = MonitorFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);
            filterRowElement.Click();
            var filterCounterElement = filterRowElement.WaitForElements(By.CssSelector(".counter.ellipsis>span"), "filter counter");
            var counter = filterCounterElement[1].GetAttribute("title");
            return counter;
        }

        public MonitorTableRow[] GetEventsDetails(int sectionNumber, string filterTitle, int eventDetailsArrLength)
        {
            MonitorTableRow[] eventDetailsArr = new MonitorTableRow[eventDetailsArrLength];

            var filterRowElement = MonitorFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);
            filterRowElement.Click();
            for (int i = 0; i < eventDetailsArrLength; i++)
            {
                var eventsDetails = MonitorTable.GetEventDetailsByRowNumber(i + 1);
                eventDetailsArr[i] = eventsDetails;
            }

            return eventDetailsArr;
        }

        public MonitorTableRow[] GetLinkedEventsDetails(int eventDetailsArrLength, String eventID)
        {
            MonitorTableRow[] eventDetailsArr;
            var tempList = new List<MonitorTableRow>();
            //TODO: Ortal - try to imporve the usage of list and array.
            eventDetailsArr = new MonitorTableRow[eventDetailsArrLength];
            By locators = By.CssSelector(".general-table-body-row");
            int i = 0;
            List<BrowserElement> ListEventsElements = staticBrowser.WaitForElements(locators, "link event");
           foreach (BrowserElement elm in ListEventsElements)
           {
                string a = elm.WaitGetAttribute("eventid");
                if (a != eventID)
                {
                    tempList.Add(new MonitorTableRow() { EventId = a });

                    eventDetailsArr[i] = new MonitorTableRow();
                    eventDetailsArr[i].EventId = a;
                    i++;
                }
           }
            var ar = tempList.ToArray();
            return eventDetailsArr;
        }

        internal SecurityConfigObject GetSecurityResultFromEventExpandConteiner(string resourceID)
        {
            if (resourceID.EndsWith("Email") || resourceID.EndsWith("Fax") || resourceID.EndsWith("Pager") ||
                resourceID.EndsWith("Phone") || resourceID.EndsWith("Remark") || resourceID.EndsWith("ScheduleTask") ||
                resourceID.EndsWith("SimpleHandle") || resourceID.EndsWith("ThirdPartyAction") ||
                resourceID.EndsWith("UserDefinedAction") || resourceID.EndsWith("Warning"))
                MonitorTable.OpenHandlingOptionMenu();
            return GetSecurityResult(resourceID, isMoveCursorToElement: false);
        }

        private MonitorTableRow GetEventDetailsFromTable(string eventId)
        {
            ClickOnMonitorFilters(1);
            var GetEventsDetails = MonitorTable.GetMonitorTableRowByEventId(eventId);
            return GetEventsDetails;
        }

        internal void CreateProgramTypeUserFilter(string[] ProgramTypes, string filterName)
        {
            MonitorFilterSection.CreateProgramTypeUserFilter(ProgramTypes, filterName);

        }

        internal string CreateAgencyIdUserFilter(string agencyId)
        {
            var amountOfRows = MonitorTable.GetAmountOfEvents();
            MonitorFilterSection.CreateAgencyIdUserFilter(agencyId);
            Wait.Until(() => MonitorTable.GetAmountOfEvents() != amountOfRows);
            return MonitorTable.GetAmountOfEvents();
        }

        internal void CreateOffenderNameUserFilter(string offenderName,string filterName)
        {
            MonitorFilterSection.CreateOffenderNameUserFilter(offenderName,filterName);
        }

        internal SecurityConfigObject GetSecurityResult(string section, string APIString, bool Prepration)
        {
            if(Prepration != true)
            {
                MonitorFilterSection.SelectFilterByTitle(1, "All");

                MonitorTable.FilterByPeriod("max");
            }

            return GetSecurityResult(APIString);
        }

        internal void CreateOfficerNameUserFilter(string officerName, string filterName)
        {
            MonitorFilterSection.CreateOfficerNameUserFilter(officerName,filterName);
        }

        public void ClickOnMonitorFilters(int sectionNumber, string filterTitle = "All")
        {
            var filterRowElement = MonitorFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);

            filterRowElement.WaitToBeClickable();

            filterRowElement.ClickSafely();

        }

        public void FilterEventsByPeriod(string recent)
        {
            SelectMonitorFilterElement(1, "All");
            MonitorTable.FilterEventsByPeriod(recent);
        }
        public void FilterEventByRangeOfDates(DateTime startTime, DateTime endTime)
        {
            SelectMonitorFilterElement(1, "All");
            MonitorTable.FilterEventByRangeOfDates(startTime, endTime);
        }

        public void SelectMonitorFilterElement(int sectionNumber, string title)
        {
            MonitorFilterSection.SelectFilterByTitle(sectionNumber, title);
            MonitorTable.GetContentContainerElement();
            MonitorTable.WaitForContent();
        }

        public List<ValueTuple<int, int>> ShowResults(int resultsNumberToShowv)
        {
            return MonitorTable.GetShownResultsPerPage(resultsNumberToShowv);
        }

        internal bool ClickOnPagerButtons()
        {
            return MonitorTable.PagingAllPages();
        }

        public void DeleteUserFilter(string title)
        {
            var filterRowElement = new MonitorFilterSectionPage(_browser).GetFilterRowByTitle(2, title);


            //filterRowElement.WaitToBeClickable();
            filterRowElement.MoveCursorToElement();
            var deleteBtn = filterRowElement.WaitForElement(By.CssSelector(".icon.delete"), "Delete button");
            deleteBtn.WaitToBeClickable();
            deleteBtn.Click();
            new DeleteFilterPopupPage(_browser).Delete();
        }


        //************************
        //*** Internal classes ***
        //************************

        internal class MonitorTablePage : WOMTableBase
        {
            BrowserElement _container;
            public MonitorTablePage(Browser browser) : base(browser)
            {
                _container = MonitorPage._container;
            }

            internal OffendersListTablePage OffendersListTable => new OffendersListTablePage(_browser);

            public void WaitForContent()
            {
                WaitForContentToLoad();
            }



            protected override By GetContentContainerLocator()
            {
                return By.Id("tableContainer");
                //return By.ClassName("main_content");
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#tbl>.row";
            }

            protected override BrowserElement GetTableRowElementByNumber(int rowNumber)
            {
                if (rowNumber <= 0)
                    throw new Exception("Row index must be greater than 0");

                var tableRow = _container.WaitForElements(By.CssSelector(GetTableRowCssSelectorString()), "table rows", minElementsToGet: 0, explicitWaitForSingleElement: 3);

                return tableRow[rowNumber - 1];
            }

            public void FilterEventByRangeOfDates(DateTime startTime, DateTime endTime)
            {
                FilterByRangeOfDates(startTime, endTime);

                //GetContentContainerElement().WaitToStailnessOfElement();
                WaitForContent();

            }

            public void FilterEventsByPeriod(string recent)
            {
                FilterByPeriod(recent);

            }

            public MonitorTableRow GetScheduleTaskEvent(DateTime time, string ReceiverSN, bool clearSearch = true)
            {
                if(clearSearch)
                    ClearSearch();

                _browser.WaitForNotBusyCursor();
                var tableRow = GetTableRowElementByNumber(1);

                //TODO: move to a method inside WOMTableBase
                var sortButton = _container.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                sortButton.Click();
                if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                    sortButton.Click();

                tableRow.WaitForStailnessOfElement();

                tableRow = GetTableRowElementByNumber(1);

                var monitorTableRow = GetMonitorTableRow(tableRow);
                var firstRowTimeStr = monitorTableRow.EventTime;
                DateTime firstRowTime = DateTime.ParseExact(firstRowTimeStr, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                Wait.Until(() => IsScheduleEventExist(firstRowTime, time, ReceiverSN) == true, "Event not displayed untill timeout", 150);

                return GetMonitorRow();

            }


            public bool IsScheduleEventExist(DateTime previousRowTime, DateTime requestTime, string previousRowReceiver)
            {
                MonitorTableRow monitorRow = GetMonitorRow();
                var RowTimeStr = monitorRow.EventTime;
                DateTime RowTime = DateTime.ParseExact(RowTimeStr, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                var receiver = monitorRow.ReceiverSN;
                var message = monitorRow.Message;
                Logger.WriteLine($"RowTime={RowTime} PreviousRowTime= {previousRowTime} receiver={receiver}  previousRowReceiver={previousRowReceiver} message={ message}");
                if ((RowTime > previousRowTime) && (RowTime >= requestTime) && receiver.Equals(previousRowReceiver) && message.ToLower().Contains(string.Format("Scheduled Task").ToLower())) ;
                return true;
                return false;
            }

            private MonitorTableRow GetMonitorRow()
            {
                MonitorTableRow monitorRow;
                var Row = GetTableRowElementByNumber(1);

                try
                {
                    monitorRow = GetMonitorTableRow(Row);
                }
                catch (StaleElementReferenceException)
                {
                    return GetMonitorRow();
                }

                return monitorRow;
            }

            public MonitorTableRow GetMonitorTableRowByEventId(string eventIdSearch)
            {
                Search(eventIdSearch);

                Wait.Until(() => GetTableRowsElements().Count() == 1 && GetTableRowsElements().First().Displayed);

                var tableRow = GetTableRowElementByNumber(1);

                return GetMonitorTableRow(tableRow);
            }

            internal void ExpandTableRow(string eventIdSearch)
            {
                Search(eventIdSearch);

                _browser.WaitForNotBusyCursor();

                Wait.Until(() => GetTableRowsElements().Any() && GetTableRowElementByNumber(1).Displayed);

                ExpandRow(1);
            }



            internal void FilterByTimeAndExpandTableRow(string eventIdSearch, string recent)
            {
                FilterByPeriod(recent);
                Search(eventIdSearch);

                _browser.WaitForNotBusyCursor();

                Wait.Until(() => GetTableRowsElements().Any() && GetTableRowElementByNumber(1).Displayed);

                var tableRow = GetTableRowElementByNumber(1);

                var expandBtn = tableRow.WaitForElement(By.ClassName("expand_icon"), "expand button");
                expandBtn.WaitToBeClickable();
                expandBtn.MoveCursorToElement(true);
            }

            public void HandleEvent(string handlingOption)
            {
                //TODO: use SelectFromDropdownList() instead

                SelectHandlingOptionFromDropdownList(handlingOption);
                switch (handlingOption)
                {
                    case "Remark":
                        new RemarkPopupPage(_browser).Remark();
                        break;
                    case "Warning":
                        new RemarkPopupPage(_browser).Remark();
                        break;
                    case "Email":
                        new EmailPopupPage(_browser).SendEmail();
                        break;
                    case "Text Message":
                        new TextMessagePopupPage(_browser).SendTextMessage();
                        break;
                    case "Handle":
                        new RemarkPopupPage(_browser).Remark();
                        break;

                }

            }

            private void SelectHandlingOptionFromDropdownList(string handlingOption)
            {
                var expandData = _container.WaitForElement(By.CssSelector(".general-table-body-additionalDataExpand._additionalDataExpand.additionalData_0.body.p_event_expand"), "");
                var handlingOptionBtn = expandData.WaitForElement(By.CssSelector(".button.action.red"), "Handling option button");
                handlingOptionBtn.MoveCursorToElement();

                var dropdownList = expandData.WaitForElement(By.ClassName("dropDownList"), "Dropdown list");

                Wait.Until(() => dropdownList.Displayed);

                var optionsRaw = dropdownList.WaitForElements(By.TagName("li"), "Menu option");

                var menuOptionElement = GetOptionElementFromMenuList(optionsRaw, handlingOption);
                menuOptionElement.Click();
            }

            internal void HandleEventScheduleTask(DateTime time)
            {
                SelectHandlingOptionFromDropdownList("Schedule Task");
                new ScheduleTaskPopupPage(_browser).AddScheduleTask(time);
            }

            public void HandleEventQuickHandling(string eventID)
            {
                Search(eventID);
                var tableRow = GetTableRowElementByNumber(1);
                var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                eventIdElement.MoveCursorToElement();
                var quichBtn = tableRow.WaitForElement(By.ClassName("general-table-body-handleOnRowSelecter"), "");
                Wait.Until(() => quichBtn.Displayed);
                quichBtn.MoveCursorToElement();
                var quickHandle = tableRow.WaitForElement(By.CssSelector("li[id^='quickHandleListItem']"), "");
                quickHandle.Click();
            }


            public void MoveCursorToQuickHandle(string eventID)
            {
                FilterByPeriod("72");
                Search(eventID);
                var tableRow = GetTableRowElementByNumber(1);
                var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                eventIdElement.MoveCursorToElement();
                var quichBtn = tableRow.WaitForElements(By.ClassName("general-table-body-handleOnRowSelecter"), "", minElementsToGet: 0);
                if (quichBtn != null && quichBtn.Count > 0)
                {
                    Wait.Until(() => quichBtn[0].Displayed);
                    quichBtn[0].MoveCursorToElement();
                }

            }

            public string GetEventStatus()
            {
                var eventStatusIcon = _container.WaitForElement(By.CssSelector(".list_grid_icon>span"), "");
                var status = eventStatusIcon.WaitGetAttribute("title");
                return status;
            }

            private MonitorTableRow GetMonitorTableRow(BrowserElement tableRow)
            {
                var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                Wait.Until(() => eventIdElement.Text != string.Empty);

                var eventId = eventIdElement.Text;
                var severityStatus = tableRow.WaitForElement(By.ClassName("severityLed"), "Severity led").WaitGetAttribute("title");
                var eventTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_EventTime'"), "DateTime").Text;
                //var offenderRefId = tableRow.WaitForElement(By.CssSelector(".offenderRefID>span"), "OffenderId").GetAttribute("title");
                var offenderRefId = tableRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.ellipsis.offenderRefID"), "Offender RefId").Text;
                var fullName = tableRow.WaitForElement(By.CssSelector(".offenderFullName>span"), "Offender full name").GetAttribute("title");
                var message = tableRow.WaitForElement(By.CssSelector(".message>span"), "Message").Text;
                var receiverSN = tableRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.ellipsis.eventID>span"), "Serial number").Text;
                var agency = tableRow.WaitForElement(By.CssSelector(".agencyName>span"), "Agency").GetAttribute("title");
                var updateTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_UpdateTime']"), "Update time").Text;
                var eventStatus = _container.WaitForElement(By.CssSelector(".list_grid_icon>span"), "").WaitGetAttribute("title", failIfNoValue: false);

                var monitorTableRow = new MonitorTableRow()
                {
                    EventId = eventId,
                    SeverityStatus = severityStatus,
                    EventTime = eventTime,
                    OffenderRefId = offenderRefId,
                    FullName = fullName,
                    Message = message,
                    ReceiverSN = receiverSN,
                    Agency = agency,
                    UpdateTime = updateTime,
                    EventStatus = eventStatus
                };

                return monitorTableRow;
            }

            internal string[] GetFilterEventsArray()
            {
                string[] datesArr = new string[2];
                var tableRow = GetTableRowElementByNumber(1);

                var sortButton = _container.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                sortButton.WaitToBeClickable();
                sortButton.Click();
                if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                    sortButton.Click();

                tableRow.WaitForStailnessOfElement();

                tableRow = GetTableRowElementByNumber(1);
                var monitorTableRow = GetMonitorTableRow(tableRow);
                var firstRowTimeStr = monitorTableRow.EventTime;
                datesArr[0] = firstRowTimeStr;

                sortButton = _container.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                sortButton.WaitToBeClickable();
                sortButton.Click();
                if (!sortButton.WaitGetAttribute("sort-direction").Equals("0"))
                    sortButton.Click();

                tableRow.WaitForStailnessOfElement();
                tableRow = GetTableRowElementByNumber(1);
                monitorTableRow = GetMonitorTableRow(tableRow);
                firstRowTimeStr = monitorTableRow.EventTime;
                datesArr[1] = firstRowTimeStr;

                return datesArr;

            }

            internal string GetAmountOfEvents()
            {
                return GetAmountOfRows();
            }

            internal void PressOpenRecentEvents()
            {
                var expandData = _container.WaitForElement(By.CssSelector("[class^='general-table-body-additionalDataExpand']"), "expand data");
                var openRecentEventsBtn = expandData.WaitForElement(By.CssSelector(".button.square_text.open_recent"), "open recent event button");
                openRecentEventsBtn.MoveCursorToElement();
                openRecentEventsBtn.Click();
                new MonitorEventsListPopup(_browser);
            }

            internal void OpenHandlingOptionMenu()
            {
                var expandData = _container.WaitForElement(By.CssSelector("[class^='general-table-body-additionalDataExpand']"), "");
                var handlingOptionBtn = expandData.WaitForElement(By.CssSelector(".btn.actionsMenuTPL.Monitor_OffenderAdditionalData_actions li.current>button"), "Handling option button");
                handlingOptionBtn.WaitToBeClickable();
                handlingOptionBtn.MoveCursorToElement();
                var dropDownList = expandData.WaitForElements(By.ClassName("dropDownList"), "handling option menu", minElementsToGet: 0);
                if (dropDownList.Any())
                    Wait.Until(() => dropDownList[0].Displayed);
            }

            internal void PressLinkedEvents()
            {
                var expandData = _container.WaitForElement(By.CssSelector("[class^='general-table-body-additionalDataExpand']"), "expand data");
                var linkedEventsBtn = expandData.WaitForElement(By.CssSelector(".LinkedEventsCheckBox"), "Linked events check box");
                linkedEventsBtn.MoveCursorToElement();
                linkedEventsBtn.Click();
                //new MonitorEventsListPopup(_browser);
            }

            internal void FilterByTimeSelectScheduleTaskEventAndExpand(string recent)
            {
                FilterByPeriod(recent);

                _browser.WaitForNotBusyCursor();

                Wait.Until(() => IsScheduleTaskExist() == true, seconds: 150);
                ExpandRowsUntilHandlingOptionsShown();

            }

            private BrowserElement ExpandRowsUntilHandlingOptionsShown()
            {
                int counter = 1;
                int maxCount = GetTableRowsElements().Count;
                BrowserElement redButon = null;
                //TODO how to catch menu displaying 'Clear Tamper'
                while (counter <= maxCount && redButon == null)
                {
                    ExpandRow(counter++);

                    redButon = GetDisplayedRedButton();

                    Thread.Sleep(250);
                }
                return redButon;
            }

            private BrowserElement GetDisplayedRedButton()
            {
                var redButton = _container.WaitForElements(By.CssSelector(".button.action.red"), "Handling Options red button", minElementsToGet: 0, explicitWaitForSingleElement: 2);

                if (redButton.Any() && redButton.Any(x => x.Displayed))
                    return redButton.FirstOrDefault(x => x.Displayed);

                return null;
            }

            private bool IsScheduleTaskExist()
            {
                bool isExist = false;
                Search("Scheduled Task-");
                _browser.WaitForNotBusyCursor();
                if (GetTableRowsElements().Any() && GetTableRowElementByNumber(1).Displayed)
                    isExist = true;
                else
                {
                    ClearSearch();
                    _browser.WaitForNotBusyCursor();
                }
                return isExist;
            }

            internal void SelectEventByMessageAndOpenHandlingOptionMenu(string message)
            {
                Search(message);

                _browser.WaitForNotBusyCursor();
                WaitForContentToLoad();

                var handlingOptionBtn = ExpandRowsUntilHandlingOptionsShown();

                var expandData = _container.WaitForElement(By.CssSelector("[class^='general-table-body-additionalDataExpand']"), "");

                handlingOptionBtn.MoveCursorToElement();

                //var dropDownList = expandData.WaitForElements(By.ClassName("dropDownList"), "handling option menu", minElementsToGet:0);

                Thread.Sleep(1000);//wait for menu animation
            }

            internal MonitorTableRow GetEventDetailsByRowNumber(int rowNumber)
            {
                var tableRow = GetTableRowElementByNumber(rowNumber);
                var monitorTableRow = GetMonitorTableRow(tableRow);
                return monitorTableRow;
            }

            internal void NavigateToOffenderThroughEventRow(string eventID)
            {
                FilterEventsByPeriod("0");//Select in "Recent" filter "ALL" to always get the evvent
                Search(eventID);
                var tableRow = GetTableRowElementByNumber(1);
                var offenderNavigation = tableRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.ellipsis.offenderFullName>span"), "offender navigator");
                offenderNavigation.Click();
            }

            //internal MonitorTableRow GetLinkedEvent()
            //{
            //    //  var LinkedEventTable = tableRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.ellipsis.offenderFullName>span"), "offender navigator");
            //    var linkedEventTableElement = _browser.WaitForElement(By.CssSelector(".general-table-body-row.row.linked_event"), "");
            //    var tableRow = GetTableRowElementByNumber(1);
            //}
        }
        internal class MonitorFilterSectionPage : FilterSectionBase
        {
            BrowserElement _container;

            public MonitorFilterSectionPage(Browser browser) : base(browser)
            {
                _container = MonitorPage._container;
            }

            public void CreateProgramTypeUserFilter(string[] ProgramTypes, string filterName)
            {
                PressAddFilter();
                new FilterPopupPage(_browser).CreateProgramTypeFilter(ProgramTypes,filterName);
            }

            private void PressAddFilter()
            {
                var addFilterBtn = _container.WaitForElement(By.CssSelector(".icons.button.link"), "Add filter button");
                addFilterBtn.Click();
            }

            internal void CreateAgencyIdUserFilter(string agencyId)
            {
                PressAddFilter();
                new FilterPopupPage(_browser).CreateAgencyIdFilter(agencyId);
            }

            internal void CreateOffenderNameUserFilter(string offenderName, string filterName)
            {
                PressAddFilter();
                new FilterPopupPage(_browser).CreateOffenderNameFilter(offenderName, filterName);
            }

            internal void CreateOfficerNameUserFilter(string officerName, string filterName)
            {
                PressAddFilter();
                new FilterPopupPage(_browser).CreateOfficerNameFilter(officerName, filterName);
            }

            internal string[] GetSystemFilters()
            {
                string filterName = string.Empty;
                var filters = GetFilterRowElementsBySectionNumber(1);
                var filtersNames = new string[filters.Count];
                for (int i = 0; i < filters.Count; i++)
                {
                    var filterTxt = filters[i].Text;
                    if (filterTxt.Contains("\r"))
                        filterName = filterTxt.Substring(0, filterTxt.IndexOf("\r"));
                    else
                        filterName = filterTxt;
                    filtersNames[i] = filterName;
                }
                return filtersNames;
            }
        }

        internal class MonitorPagingPage : WOMTablePagingPage
        {
            public MonitorPagingPage(Browser browser) : base(browser)
            {

            }
        }
    }

    public static class EventGridSecurityConfigData
    {
        internal static By getSecurityElemntBy(/*string[] element, */string Section, string Type, string Title)
        {
            By by = null;

            if (Type == "Button")
                by = By.CssSelector(".btn [title='" + Title + "']");

            else if (Type == "GridHeader")
            {
                if (Title == "Mail")
                    by = By.CssSelector("#tblHeader [title='Email']");
                else if (Title == "ActionComment")
                    by = By.CssSelector("#tblHeader [title='Comment']");
                else if (Title == "OID")
                    by = By.CssSelector("#tblHeader [title='Offender ID']");
                else if (Title == "CommCom" || Title == "CommComp")
                    by = By.CssSelector("#tblHeader [title='Communication Server']");
                else if (Title == "RcvrSN")
                    by = By.CssSelector("#tblHeader [title='Receiver Serial No.']");
                else if (Title == "ProgType")
                    by = By.CssSelector("#tblHeader [title='Program Type']");
                else if (Title == "ScheduleTime")
                    by = By.CssSelector("#tblHeader [title='Scheduled Time']");
                else if (Title == "PreTime")
                    by = By.CssSelector("#tblHeader [title='Reminder Time']");
                else if (Title == "TaskSubType")
                    by = By.CssSelector("#tblHeader [title='Reason']");
                else if (Title == "TaskType")
                    by = By.CssSelector("#tblHeader [title='Task']");
                else if (Title == "DownloadDate")
                    by = By.CssSelector("#tblHeader [title='Download Time']");
                else if (Title == "AgencyName")
                    by = By.CssSelector("#tblHeader [title='Agency']");
                else if (Title == "SeqNo")
                    by = By.CssSelector("#tblHeader [title='Sequence No.']");
                else if (Title == "Seq")
                    by = By.CssSelector("#tblHeader [title='Seq.']");
                else if (Title == "Index")
                    by = By.CssSelector("#tblHeader [title='']");
                else if (Title == "TZ")
                    by = By.CssSelector("#tblHeader [title='TZ']");
                else //(Title.Length == 1)
                    by = By.CssSelector("#tblHeader [title='" + Title + "']");
                //else
                //    by = By.CssSelector("#tblHeader [title='" + Title[0] + " " + Title[1] + "']");

            }

            else if (Type == "Section")
                by = By.CssSelector(".sidePanel");
           
            return by;
        }

    }
}
