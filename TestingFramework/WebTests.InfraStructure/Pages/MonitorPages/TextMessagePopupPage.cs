﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class TextMessagePopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public TextMessagePopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Remark popup");

        }

        public void SendTextMessage()
        {
            var combobox = _browser.WaitForElement(By.XPath("//input[@id='selectedUserField']/.."), "");
            var name = combobox.SelectFromCustomComboboxByIndex(By.TagName("input"), By.CssSelector("ul>li"), 0);

            var providerCombobox = _browser.WaitForElement(By.Id("providerOneSelect"), "");
            var provider = providerCombobox.SelectFromComboboxByIndex(1);

            var pagerCombobox = _browser.WaitForElement(By.Id("PagerTypeField"), "");
            var pager = pagerCombobox.SelectFromComboboxByIndex(1);

            var code = _browser.WaitForElement(By.Id("p1codeField"), "");
            code.Text = "111";

            var comment = _container.WaitForElement(By.Id("actionCommentTxtArea"), "");
            comment.Text = "text message";

            var buttons = _container.WaitForElements(By.ClassName("button"), "");
            var okBtn = buttons[1];
            okBtn.Click();

        }
               
    }
}
