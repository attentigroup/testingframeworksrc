﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.MonitorPages
{
    public class HighPriorityAlertPopup
    {
        private readonly Browser _browser;
        protected By PopupContainerLocator => By.CssSelector(".Popup.primary-popup.ui-draggable");
        private List<BrowserElement> _container => _browser.WaitForElements(PopupContainerLocator, "Popup container", 1, minElementsToGet: 0);

        public HighPriorityAlertPopup(Browser browser)
        {
            _browser = browser;
        }

        public void ReleaseAlert()
        {
            if (_container != null && _container.Any())
            {
                var button = _container.First().WaitForElement(By.ClassName("buttons"), "Popup button", 1);

                button.WaitToBeClickable();

                button.Click();

                button.WaitToDisappear();

                Logger.WriteLine("High priority alert found!");
            }
            else
            {
                Logger.WriteLine("No High priority alert found");
            }
        }
    }
}
