﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.EquipmentPages
{
    public class ImportEquipmentPopup
    {
        protected readonly Browser _browser;

        private BrowserElement IframeUploadContainer
        {
            get { return _browser.WaitForElement(By.TagName("form"), "Upload panel form"); }
        }

        private BrowserElement PopupContainer
        {
            get { return _browser.WaitForElement(By.Id("PopupContainer"), "Popup container"); }
        }
        public ImportEquipmentPopup(Browser browser)
        {
            _browser = browser;
            Wait.Until(() => PopupContainer.Displayed);

        }

        private void SwitchToiFrameForm()
        {
            var iframe = _browser.WaitForElement(By.CssSelector("#PopupContainer iframe"), "Popup container");

            Wait.Until(() => iframe.Displayed);

            iframe.SwitchToThisFrame();
        }

        public void UploadFile(string fileName)
        {
            SwitchToiFrameForm();

            var btnChoosFile = IframeUploadContainer.WaitForElement(By.CssSelector("#UploadPanel>div>input"), "Browse button");

            btnChoosFile.Click();

            new OpenFileDialog(_browser).UploadFile(fileName);

            _browser.SwitchToDefaultFrame();
        }

        public void Validate()
        {
            ValidateInternal();

            PopupContainer.WaitToDisappear();
        }

        public void PopupConfirm()
        {
            ValidateInternal();
        }

        public void ValidateExpectFail()
        {
            throw new NotImplementedException();
        }

        private void ValidateInternal()
        {
            Wait.Until(() => PopupContainer.Displayed);

            SwitchToiFrameForm();

            var btnValidate = IframeUploadContainer.WaitForElement(By.CssSelector("input[type='submit']"), "Validate button");

            btnValidate.ClickSafely();

            _browser.SwitchToDefaultFrame();
        }
    }
}
