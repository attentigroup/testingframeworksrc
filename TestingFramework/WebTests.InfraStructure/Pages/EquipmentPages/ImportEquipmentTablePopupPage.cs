﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.EquipmentPages
{
    public class ImportEquipmentTablePopupPage : WOMTableBase
    {
        private BrowserElement IframeContainer
        {
            get { return _browser.WaitForElement(By.CssSelector("#PopupContainer #poupBox iframe"), "Import equipment iFrame"); }
        }

        public ImportEquipmentTablePopupPage(Browser browser) : base(browser)
        {
        }

        protected override By GetContentContainerLocator()
        {
            return By.Id("popupHeader0");
        }

        protected override string GetTableRowCssSelectorString()
        {
            return "#SucceededList tr";
        }

        public void Save()
        {
            IframeContainer.SwitchToThisFrame();

            var btnSave = _browser.WaitForElement(By.CssSelector("form input[type='submit']"), "Save button");

            btnSave.Click();

            _browser.SwitchToDefaultFrame();
        }

        public void Close()
        {
            IframeContainer.SwitchToThisFrame();

            var btnClose = _browser.WaitForElement(By.ClassName("button close"), "Close button");

            btnClose.Click();

            _browser.SwitchToDefaultFrame();
        }

        /// <summary>
        /// Enter 1 based row index
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public Equipment GetEquipmentDetails(int rowIndex)
        {
            IframeContainer.SwitchToThisFrame();

            var tableRow = GetTableRowElementByNumber(rowIndex + 1);
            var cols = tableRow.WaitForElements(By.TagName("td"), "Table columns");
            var equipment = new Equipment()
            {
                SerialNumber = cols[0].Text,
                Type = cols[1].Text,
                Code = cols[2].Text,
                Protocol = cols[3].Text,
            };

            _browser.SwitchToDefaultFrame();

            return equipment;
        }
    }
}
