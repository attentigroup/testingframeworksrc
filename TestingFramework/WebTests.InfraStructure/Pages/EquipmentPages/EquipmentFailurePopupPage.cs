﻿using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.EquipmentPages
{
    internal class EquipmentFailurePopupPage : WOMPageCommon
    {
        private BrowserElement _container;

        public EquipmentFailurePopupPage(Browser browser):base(browser)
        {
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Popup container");
            Wait.Until(() => _container.Displayed);
        }

        public SecurityConfigObject SecuritySaveFailureData(string apiString)
        {
            var result = GetSecurityResult(apiString);

            Close();

            return result;
        }

        public void Close()
        {
            var btnClose = _container.WaitForElement(By.CssSelector(".button.close"), "Close button");

            btnClose.Click();

            _container.WaitToDisappear();
        }

    }
}