﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.EquipmentPages
{
    public class EquipmentManagementPage : WOMPage
    {
        public EquipmentManagementPage(Browser browser) : base(browser)
        {
        }

        internal EquipmentManagementTablePage EquipmentManagementTable => new EquipmentManagementTablePage(_browser);
        internal EquipmentManagementFilterSectionPage EquipmentManagementFilterSection => new EquipmentManagementFilterSectionPage(_browser);

        public Equipment AddEquipment(string fileName)
        {
            return EquipmentManagementTable.ImportEquipment(fileName);
        }

        public void AddEquipments(string fileName)
        {
            EquipmentManagementTable.ImportEquipments(fileName);
        }

        internal void SelectEquipmentAndOpenVoiceSimDataPopup(string equipmentSN)
        {
            var filterRow = EquipmentManagementFilterSection.GetFilterByContainingText(1, "Equipment_Filters_Label_AllEquipment");
            filterRow.WaitToBeClickable();
            filterRow.ClickSafely();

            EquipmentManagementTable.SelectEquipmentAndOpenVoiceSimDataPopup(equipmentSN);
        }

        internal void AddCellularData(string equipmentSN)
        {
            var filterRow = EquipmentManagementFilterSection.GetFilterByContainingText(1, "Equipment_Filters_Label_AllEquipment");
            filterRow.WaitToBeClickable();
            filterRow.ClickSafely();

            EquipmentManagementTable.AddCellularData(equipmentSN);
        }



        public Equipment GetEquipmentDetails(string equipmentSN)
        {
            var filterRow = EquipmentManagementFilterSection.GetFilterByContainingText(1, "Equipment_Filters_Label_AllEquipment");
            filterRow.WaitToBeClickable();
            filterRow.ClickSafely();

            var equip = (Equipment)EquipmentManagementTable.GetEquipmentDetails(equipmentSN);

            return equip;
        }

        public void DeleteEquipment(string serialNumber)
        {
            EquipmentManagementTable.DeleteEquipment(serialNumber);
        }

        public string AssignAgency(string equipmentSN)
        {
            return EquipmentManagementTable.AssignAgency(equipmentSN);
        }

        public object GetTableRowDataUsingTableHeader()
        {
            var tableRowObj = EquipmentManagementTable.GetEquipmentRowDetailsByTableHeader(1, typeof(Equipment), "Serial Number", "Type", "SIM Voice Number", "SIM Data Number", "Protocol", "GSM Encryption Type", "RF Encryption Type");

            return tableRowObj;
        }

        internal class EquipmentManagementTablePage : WOMTableBase
        {

            GeneralActionsMenuBasePage GeneralActionsMenu => new EquipmentManagementGeneralActionsMenuPage(_browser);
            List<BrowserElement> allRows => GetTableRowsElements();

            public EquipmentManagementTablePage(Browser browser) : base(browser)
            {
                WaitForContentToLoad();
                //Wait.Until(() => _browser.WaitForElement(GetContentContainerLocator(), "Content container").Text != "");
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("Equipment");
            }

            public object GetEquipmentRowDetailsByTableHeader(int rowNumber, Type type, params string[] tableTitles)
            {
                return GetEquipmentRowDetailsUsingTableHeader(1, typeof(Equipment), "Serial Number", "Type", "SIM Voice Number", "SIM Data Number", "Protocol", "GSM Encryption Type", "RF Encryption Type");
            }



            protected override string GetTableRowCssSelectorString()
            {
                return "#tbl>.row";
            }

            //protected override string GetTableSearchCssSelectorString()
            //{
            //    return "#searchInGrid";
            //}

            //protected override string GetTableSearchButtonCssSelectorString()
            //{
            //    return ".Equiptmernt-Header-SearchButton";
            //}

            public Equipment ImportEquipment(string fileName)
            {
                _browser.WaitForElement(By.CssSelector("#buttons #importBtn>div"), "Import button").ClickSafely();

                var importEquipmentPopup = new ImportEquipmentPopup(_browser);
                importEquipmentPopup.UploadFile(fileName);
                importEquipmentPopup.Validate();

                var importEquipmentTablePopupPage = new ImportEquipmentTablePopupPage(_browser);

                var EquipmentDetails = importEquipmentTablePopupPage.GetEquipmentDetails(1);

                importEquipmentTablePopupPage.Save();

                importEquipmentPopup.PopupConfirm();

                return EquipmentDetails;
            }

            public void ImportEquipments(string fileName)
            {
                _browser.WaitForElement(By.CssSelector("#buttons #importBtn>div"), "Import button").ClickSafely();

                var importEquipmentPopup = new ImportEquipmentPopup(_browser);
                importEquipmentPopup.UploadFile(fileName);
                importEquipmentPopup.Validate();

                var importEquipmentTablePopupPage = new ImportEquipmentTablePopupPage(_browser);

                importEquipmentTablePopupPage.Save();

                importEquipmentPopup.PopupConfirm();
            }


            public void DeleteEquipment(string equipmentId)
            {
                Search(equipmentId);

                var firstRowElement = GetTableRowElementByNumber(1);

                ToggleCheckbox(firstRowElement);

                _browser.WaitForElement(By.CssSelector("#buttons #deleteBtn"), "Delete button").Click();
            }

            private void ToggleCheckbox(BrowserElement tableRow)
            {
                var checkBox = tableRow.WaitForElement(By.CssSelector("input[type='checkbox']"), "Check box");
                //TODO ttry WaitTobe clickable before click()
                checkBox.ClickSafely();
            }
            /// <summary>
            /// Returns Agency name
            /// </summary>
            /// <param name="equipmentSN"></param>
            /// <returns></returns>
            public string AssignAgency(string equipmentSN)
            {
                var row = SearchEquipmentBySerialNumber(equipmentSN);

                ToggleCheckbox(row);

                var moveToPopupPage = GeneralActionsMenu.MoveTo();

                return moveToPopupPage.SelectAgency();
            }

            public BrowserElement SearchEquipmentBySerialNumber(string serialNumber)
            {
                Search(serialNumber);
                var rows = GetTableRowsElements();

                BrowserElement tableRow = null;

                int i = -1;

                for (i = 0; i < rows.Capacity; i++)
                {
                    var sn = GetEquipmentRowDetailsUsingTableHeader(i+1, typeof(Equipment), "Serial Number");
                    var a = sn.GetType().GetProperty("SerialNumber").GetValue(sn).ToString();
                    if (a.Equals(serialNumber))
                    {
                        break;
                    }
                }

                tableRow = rows[i];
 
                return tableRow;
            }

            internal void AddCellularData(string equipmentSN)
            {
                var row = SearchEquipmentBySerialNumber(equipmentSN);
                row.DoubleClick();

                var editCellularataPage = new EditCellularDataPopupPage(_browser);

                editCellularataPage.FillForm();

            }

            internal object GetEquipmentDetails(string equipmentSN)
            {

                Search(equipmentSN);

                var row = GetEquipmentRowDetailsUsingTableHeader(1, typeof(Equipment), "Serial Number", "Type", "Agency", "Offender", "SIM Voice Number", "SIM Data Number", "Protocol", "RF Encryption Type", "GSM Encryption Type");
                return row;
            }

            internal BrowserElement GetFirstRowWithFailureButton()
            {
                BrowserElement rowWithFailure = null;

                foreach (var item in allRows)
                {
                    rowWithFailure = item.WaitForElementByAttributeValue(By.TagName("div"), "class", "button", failIfNoValue: false);
                    if (rowWithFailure != null)
                        break;
                }

                return rowWithFailure;
            }

            internal void DoubleClickRow()
            {
               var row = GetTableRowElementByNumber(1);
                var serialNumberColumn = row.WaitForElement(By.CssSelector("[colname='Equipment_List_GridHeader_SerialNumber']"), "Serial number column");
                serialNumberColumn.DoubleClick();
            }

            internal void SelectEquipmentAndOpenVoiceSimDataPopup(string equipmentSN)
            {
                Search(equipmentSN);
                _browser.WaitForNotBusyCursor();
                WaitForContentToLoad();
                //Wait.Until(() => GetTableRowElementByNumber(1) != null);
                var row = GetTableRowElementByNumber(1);

                //row.MoveCursorToElement();

                Wait.Until(() => row.Displayed);

                row.WaitToBeClickable();
                var col = row.WaitForElement(By.CssSelector("div:nth-of-type(1)"), "col");
                col.DoubleClick();

                var editCellularDataPage = new EditCellularDataPopupPage(_browser);
            }

            internal void CloseEditCellularPopup()
            {
                new EditCellularDataPopupPage(_browser).Close();
            }
        }

        internal SecurityConfigObject Navigate2Section(string section, string apiString)
        {
            SecurityConfigObject result = new SecurityConfigObject();

            switch (section)
            {
                case "List":
                    if (apiString.EndsWith("_EncryptionType") || apiString.EndsWith("_InsertedDate"))
                        result = GetUnallocatedSecurityResults(apiString);
                    else if (apiString.EndsWith("_Checkbox") || apiString.EndsWith("_FailureDate") || apiString.EndsWith("_FailureDescription") || apiString.EndsWith("_FailureType") || apiString.EndsWith("GridHeader_Failure"))
                        result = SecuritySaveFailureData(apiString);
                    else
                        result = GetSecurityForColumn(apiString);
                    break;

                case "General":
                    if (apiString.EndsWith("_SaveFailureData"))
                        result = SecuritySaveFailureData(apiString);
                    else
                        result = GetSecurityResult(apiString);
                    break;

                case "Filters":
                    result = GetSecurityResult(apiString);
                    break;

                case "Popup":
                    EquipmentManagementTable.DoubleClickRow();
                    result = new SecurityConfigObject() { ActualVisibleEnabledExposed = new EditCellularDataPopupPage(_browser).IsPopupShown(), ResourceId = apiString };
                    EquipmentManagementTable.CloseEditCellularPopup();
                    break;

                default:
                    Assert.Fail($"Section {section} is not handled yet...");
                    break;
            }

            return result;
        }

        private SecurityConfigObject GetUnallocatedSecurityResults(string apiString)
        {
            EquipmentManagementFilterSection.SelectFilterByContainingText(1, "Filters_Label_Unallocated");
            return GetSecurityResult(apiString);
        }

        private SecurityConfigObject SecuritySaveFailureData(string apiString)
        {
            EquipmentManagementFilterSection.SelectFilterByContainingText(1, "Filters_Label_Failed");
            if (apiString.EndsWith("_SaveFailureData"))
            {
                var rowWithFailureButton = EquipmentManagementTable.GetFirstRowWithFailureButton();
                rowWithFailureButton.WaitToBeClickable();
                rowWithFailureButton.MoveCursorToElement();
                rowWithFailureButton.Click();

                var popup = new EquipmentFailurePopupPage(_browser);

                return popup.SecuritySaveFailureData(apiString);
            }
            else
                return GetSecurityResult(apiString);
        }

        private SecurityConfigObject GetSecurityForColumn(string apiString)
        {
            EquipmentManagementFilterSection.SelectFilterByContainingText(1, "AllEquipment");
            return GetSecurityResult(apiString);
        }

        internal class EquipmentManagementFilterSectionPage : FilterSectionBase
        {
            internal EquipmentManagementFilterSectionPage(Browser browser) : base(browser)
            {
            }
        }
        internal class EquipmentManagementGeneralActionsMenuPage : GeneralActionsMenuBasePage
        {
            public EquipmentManagementGeneralActionsMenuPage(Browser browser) : base(browser)
            {

            }
            protected override By GeneralActionsContainerSelector()
            {
                return By.Id("buttons");
            }

            protected override By GetSaveButtonSelector()
            {
                throw new NotImplementedException();
            }
        }
    }
}