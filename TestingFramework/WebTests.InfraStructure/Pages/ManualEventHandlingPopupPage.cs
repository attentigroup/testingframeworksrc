﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class ManualEventHandlingPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public ManualEventHandlingPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Remark popup");

        }

        public void ManualEvent()
        {
            var messageCombobox = _container.WaitForElement(By.ClassName("manual-event-validation"), "");
            var message = messageCombobox.SelectFromComboboxByIndex(1);

            var subTypeCombobox = _container.WaitForElement(By.Id("SubType"), "");
            var subType = subTypeCombobox.SelectFromComboboxByIndex(1);

            var comment = _container.WaitForElement(By.Id("manualEventTxtArea"), "");
            comment.Text = "manual event";

            var handlingStatusCombobox = _container.WaitForElement(By.ClassName("big"), "");
            var handlingStatus = handlingStatusCombobox.SelectFromComboboxByIndex(2);

            var okBtn = _container.WaitForElement(By.Id("manualEventokBtn"), "");
            okBtn.Click();

        }
    }
}
