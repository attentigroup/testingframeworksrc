﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class OperationalReportsPage : WOMPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;

        public OperationalReportsPage(Browser browser) : base(browser)
        {
            _browser = browser;
            _browser.SwitchToLastWindow();
            _container = _browser.WaitForElement(By.ClassName("rmRootGroup"), "Container");
            Wait.Until(() => _container.Text != "");
        }

        internal object NavigateToReport(string optionText)
        {
         return SelectFromCustomComboboxByText("ctl00_SearchCombo_Input", optionText, "Report options");
        }

        private object SelectFromCustomComboboxByText(string comboboxId, string optionText, string description)
        {
            var selectOption = GetCustomComboboxOptions(comboboxId, description).Find(x => x.Text == optionText);
            selectOption.MoveCursorToElement(true);
            return selectOption;
        }

        protected List<BrowserElement> GetCustomComboboxOptions(string comboboxId, string description)
        {
            var combobox = _browser.WaitForElement(By.XPath($"//input[@id='{comboboxId}']/.."), description);
            var input = combobox.WaitForElement(By.TagName("input"), "Input element");
            Wait.Until(() => input.Displayed);
            input.MoveCursorToElement();
            input.Click();
            Thread.Sleep(300);
            var comboboxDropDown = _browser.WaitForElement(By.Id("ctl00_SearchCombo_DropDown"), description);
            var options = comboboxDropDown.WaitForElements(By.CssSelector("ul>li"), "options");

            return options;
        }

        public void RunReport()
        {
            var RunButton = _browser.WaitForElement(By.Id("ctl00_ctl00_PageContent_RunReportButton"),"Run Button");
            RunButton.ClickSafely();
            Thread.Sleep(3000);
        }

        public void GenerateReport(string optionText)
        {
            NavigateToReport(optionText);
            RunReport();   
        }

    }
}
