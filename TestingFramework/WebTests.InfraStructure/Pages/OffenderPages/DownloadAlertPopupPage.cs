﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class DownloadAlertPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public DownloadAlertPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "Download popup");
            Wait.Until(() => _container.Displayed);
        }

        internal void PressOk()
        {
            var okBtns = _container.WaitForElements(By.TagName("button"), "OK popup button");
            var btnOK = okBtns.First();
            btnOK.ClickSafely();
            _container.WaitToDisappear();
        }
    }
}
