﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class SaveSchedulePopup
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public SaveSchedulePopup(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Address details popup");
            Wait.Until(() => _container.Displayed);
        }

        public void LogAndSave()
        {
            _container.WaitForElement(By.Id("save_log_btn"), "Log & Save button").Click();
        }
    }
}
