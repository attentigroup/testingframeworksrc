﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class SaveGroupSchedulePopup
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public SaveGroupSchedulePopup(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "Save Schedule Popup");
            Wait.Until(() => _container.Displayed);
        }

        public void Save()
        {
            _container.WaitForElement(By.CssSelector(".button.generic"), "Save button").Click();
        }
    }
}
