﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;
using WebTests.Common;

namespace WebTests.InfraStructure.Pages
{
    public class EndOfServicePopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public EndOfServicePopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("poupBox"), "End of service popup");
            Wait.Until(() => _container.Displayed);
        }

        public void FillForm(bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations)
        {
            if (isSuccessful)
            {
                var successful = _container.WaitForElement(By.Id("sucssesRdb"), "Successful EOS");
                successful.Click();
            }
            else
            {
                var unsuccessful = _container.WaitForElement(By.Id("unsucssesRdb"), "Unsuccessful EOS");
                unsuccessful.Click();
            }

            if (isCurfewViolations) {
                var curfewViolations = _container.WaitForElement(By.Id("unsucsses_curfew"), "Curfew Violations EOS");
                curfewViolations.Click();
            }

            var combobox =_container.WaitForElement(By.TagName("select"), "Reason field");

            if(isAutoEOS)
                combobox.SelectFromComboboxByValue("AEOS");
            else
            {
                combobox.SelectFromComboboxByValue("MEOS");
            }

            if (noReceiver)
            {
                var noReciever = _container.WaitForElement(By.Id("noReceiver"), "No Receiver EOS");
                noReciever.Click();
            }

            PressOK();

            var confirmPopupEOS = new EndOfServiceConfirmPopupPage(_browser);
            confirmPopupEOS.OK();

        }

        public void PressOK()
        {
            var btnOK = _container.WaitForElement(By.Id("EOS_OK_btn"), "OK button");
            btnOK.WaitToBeClickable();
            btnOK.Click();
        }
    }
}
