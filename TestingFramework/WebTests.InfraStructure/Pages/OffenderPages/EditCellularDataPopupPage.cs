﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;
using WebTests.Common;

namespace WebTests.InfraStructure.Pages
{
    public class EditCellularDataPopupPage :PopupPageBase
    {
        //private readonly Browser _browser;
        //private readonly BrowserElement _container;
        public EditCellularDataPopupPage(Browser browser):base(browser)
        {
            //_browser = browser;
            //_container = _browser.WaitForElement(By.Id("PopupContainer"), "Edit cellular data popup");
            //Wait.Until(() => _container.Displayed);
        }

        public void FillForm()
        {
            var voiceSim = _container.WaitForElement(By.Id("VoiceSIMPhone0_0"), "");
            voiceSim.Text = RandomGenerator.GenerateNumber(123456).ToString();
            var dataSim = _container.WaitForElement(By.Id("DataSIMPhone0_0"), "");
            dataSim.Text = RandomGenerator.GenerateNumber(123456).ToString();

            var combobox =_container.WaitForElement(By.Id("provider_options_left"), "Service provider dropdown list");

            //combobox.SelectFromComboboxByValue("37");
            combobox.SelectFromComboboxByIndex(1);

            Save();

            var btnClose = _container.WaitForElement(By.CssSelector(".button.close"), "Close button");
            btnClose.WaitToBeClickable();
            btnClose.ClickSafely();
        }

        public void Save()
        {
            var btnSave = _container.WaitForElement(By.CssSelector(".buttons button:first-child"), "Save button");
            btnSave.WaitToBeClickable();
            btnSave.Click();
        }

        public bool IsPopupShown()
        {
            var popup = _browser.WaitForElements(By.Id("PopupContainer"), "Popup container", minElementsToGet: 0, explicitWaitForSingleElement: 3);

            if (popup.Any())
                return true;
            return false;
        }

        internal void Close()
        {
            _container.WaitForElement(By.CssSelector(".button.close"), "Close button").Click();
            _container.WaitToDisappear();
        }
    }
}
