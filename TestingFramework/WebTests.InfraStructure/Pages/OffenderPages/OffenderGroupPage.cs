﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.Common.Resources;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.MonitorPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class OffenderGroupPage : WOMPage
    {
        public OffenderGroupPage(Browser browser) : base(browser)
        {

        }

        internal DetailsTabPage DetailsTab => new DetailsTabPage(_browser);

        internal ParametersTabPage ParametersTab => new ParametersTabPage(_browser);

        internal LocationTabPage LocationTab => new LocationTabPage(_browser);

        internal ScheduleTabPage ScheduleTab => new ScheduleTabPage(_browser);

        internal class DetailsTabPage : WOMTableBase
        {
            public DetailsTabPage(Browser browser) : base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.CssSelector(".Tabs li[data-tab='GroupDetails']"), "Group Details tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
            }

            public bool SelectFirstOffenderRow()
            {
                var row = GetTableRowElementByNumber(1);

                var columns = row?.WaitForElements(By.TagName("td"), "columns");

                var clickableColumn = columns?.FirstOrDefault(x => x.GetAttribute("style").Contains("cursor"));

                clickableColumn?.Click();

                if (clickableColumn != null)
                    return true;

                return false;
            }

            public AddNewOffenderPage SelectOffenderByIndex(int index)
            {
                var row = GetTableRowElementByNumber(index);

                var columns = row?.WaitForElements(By.TagName("td"), "columns");

                var clickableColumn = columns?.FirstOrDefault(x => x.GetAttribute("style").Contains("cursor"));

                clickableColumn?.Click();

                return new AddNewOffenderPage(_browser, false);
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("gridMasterList");
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#OffendersLine tr";
            }

            public void DeleteGroup()
            {
                new GroupPageGeneralActionsMenu(_browser).Delete();

                var popup = new DeleteGroupPopupPage(_browser);
                popup.OK();
            }
        }

        internal class ParametersTabPage : GroupTabBase
        {
            public ParametersTabPage(Browser browser) : base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.CssSelector(".Tabs li[data-tab='GroupParameters']"), "Group Parameters tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
            }
        }

        internal class LocationTabPage : GroupTabBase
        {
            public LocationTabPage(Browser browser) : base(browser)
            {
                var tabLocation = _browser.WaitForElement(By.CssSelector(".Tabs li[data-tab='Location']"), "Group Location tab");
                GoToTab(tabLocation, ApplicationHeaderLocator);
            }

            internal void AddCircularZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
            {
                var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");
                var btnAddCircularZone = _browser.WaitForElement(By.Id("add_circular_from_toolbar"), "Add circular zone");
                btnAddCircularZone.WaitToBeClickable();
                btnAddCircularZone.Click();

                slider.DragAndDropFromToOffset(100, 100);

                new AddNewCircularZonePopupPage(_browser).AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
            }

            internal DeleteZonePopupPage DeleteZone()
            {
                var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");
                var zones = zoneList.WaitForElements(By.ClassName("OffenderLocation-ZoneNameWrapper"), "zones");
                zones[0].MoveCursorToElement();
                var deleteBtn = zoneList.WaitForElements(By.CssSelector(".icon.delete"), "delete button");
                deleteBtn[0].WaitToBeClickable();
                deleteBtn[0].Click();

                return new DeleteZonePopupPage(_browser);
            }

        }

        internal class ScheduleTabPage : GroupTabBase
        {
            public ScheduleTabPage(Browser browser) : base(browser)
            {
                var tabSchedule = _browser.WaitForElement(By.CssSelector(".Tabs li[data-tab='Schedule']"), "Group Schedule tab");
                GoToTab(tabSchedule, ApplicationHeaderLocator);
            }

            ScheduleGeneralActionsMenuPage ScheduleGeneralActionsButtonsMenu => new ScheduleGeneralActionsMenuPage(_browser);

            internal void AddSchedule(TimeFrame timeFrame)
            {
                if (!string.IsNullOrEmpty(timeFrame.Limitation) && timeFrame.Limitation.Equals("Exclusion"))
                {
                    NavigateToExclusionSchedule();
                }

                var AddTimeframePage = PressAddTimeFrame();
                AddTimeframePage.AddTimeFrame(timeFrame);
                var savePopup = ScheduleGeneralActionsButtonsMenu.Save();
                savePopup.Save();
            }


            public void UpdateTimeFrame(TimeFrame timeFrameObj, string timeFrameID, string ProgramType)
            {
                if (!string.IsNullOrEmpty(timeFrameObj.Limitation) && timeFrameObj.Limitation.Equals("Exclusion"))
                {
                    NavigateToExclusionSchedule();
                }

                var timeFrame = _browser.WaitForElement(By.ClassName("timeFrame" + timeFrameID), $"TimeFrame: {timeFrameID}");
                timeFrame.MoveCursorToElement(true);

                var webDriver = _browser.GetWebDriver();

                IJavaScriptExecutor jse = webDriver as IJavaScriptExecutor;

                var contentTableElement = _browser.WaitForElement(By.Id("contentTable"), "Content table");
                var elem = contentTableElement.GetWebElement();

                for (int i = 0; i < 500; i += 100)
                {
                    jse.ExecuteScript($"arguments[0].scrollTo({i},0);", elem);

                    timeFrame.DoubleClick();
                    var popup = _browser.WaitForElement(By.Id("PopupContainer"), "Popup");

                    if (popup.Displayed)
                        break;
                }
                var addTimeframePopup = new AddTimeFramePopupPage(_browser);
                addTimeframePopup.UpdateTimeFrame(timeFrameObj);
                if (ProgramType.Contains("E4") && timeFrameObj.RecurseEveryWeek.Equals("True"))
                {
                    var updatePopup = _browser.WaitForElement(By.Id("PopupContainer"), "Edit cellular data popup");
                    Wait.Until(() => updatePopup.Displayed);
                    var buttons = updatePopup.WaitForElements(By.TagName("button"), "");
                    var updateButton = buttons.First(x => x.Text.Equals("Update"));
                    updateButton.Click();
                }


                SaveScheduleChanges();

            }

            private void SaveScheduleChanges()
            {
                var saveSchedule = _browser.WaitForElement(By.CssSelector(".button.button.square.save"), "Save schedule");
                saveSchedule.Click();
                var skipLogBtn = _browser.WaitForElement(By.Id("skip_log_btn"), "Skip log button");
                skipLogBtn.Click();
            }

            private void NavigateToExclusionSchedule()
            {
                var inclusionNavigationBtn = _browser.WaitForElement(By.CssSelector("div.btn.inclusion"), "Inclusion button");
                inclusionNavigationBtn.Click();
            }

            private AddTimeFramePopupPage PressAddTimeFrame()
            {
                var addTimeFrameBtn = _browser.WaitForElement(By.Id("add_time_frame"), "Add timeframe button");
                addTimeFrameBtn.Click();
                return new AddTimeFramePopupPage(_browser);
            }

            internal DeleteRecurringTimeframesPopupPage DeleteRecurringTimeframes()
            {
                var deleteMenu = _browser.WaitForElement(By.CssSelector(".btn.delete_all"), "Delete Schedule Dropdown button");
                deleteMenu.Click();
                var deleteWeeklyScheduleButton = _browser.WaitForElement(By.Id("delete_weekly_btn"), "Delete Weekly Schedule button");
                deleteWeeklyScheduleButton.ClickSafely();
                return new DeleteRecurringTimeframesPopupPage(_browser);
            }

            internal bool IsTimeframeExist()
            {
                var schedule = _browser.WaitForElement(By.ClassName("Schedule-TimeFrames"), "");
                var timeframes = schedule.WaitForElements(By.ClassName("Schedule-TimeFrames-TimeFrame"), "", minElementsToGet: 0);
                if (timeframes.Any())
                    return true;
                return false;
            }

            internal class ScheduleGeneralActionsMenuPage : GeneralActionsMenuBasePage
            {
                protected override By GeneralActionsContainerSelector()
                {
                    return By.ClassName("actions");
                }

                public ScheduleGeneralActionsMenuPage(Browser browser) : base(browser)
                {

                }

                protected override By GetSaveButtonSelector()
                {
                    return By.Id("group-master-general-button0");
                }

                public new SaveGroupSchedulePopup Save()
                {
                    var btnSave = _browser.WaitForElement(GetSaveButtonSelector(), "Save button");
                    btnSave.WaitToBeClickable();
                    btnSave.ClickSafely();
                    return new SaveGroupSchedulePopup(_browser);
                }
            }

            internal void ShowScheduleTooltip()
            {
                var toTime = DateTime.Now.AddSeconds(9);
                var timeFrameBox = _browser.WaitForElements(By.CssSelector(".Schedule-TimeFrames-TimeFrame"), "Schedule-TimeFrames-TimeFrame", 3, minElementsToGet: 0);
                if (!timeFrameBox.Any() && DateTime.Now < toTime)
                {
                    GotoNextWeek();
                    ShowScheduleTooltip();
                }
                else
                {
                    timeFrameBox.First().MoveCursorToElement();
                    timeFrameBox.First().MoveCursorToElement();

                    var tooltip = _browser.WaitForElement(By.ClassName("myScheduleToolTip"), "Schedule tooltip");

                    Wait.Until(() => tooltip.Displayed);
                }
            }

            public void GotoNextWeek()
            {
                var btn = _browser.WaitForElement(By.Id("next_week_btn"), "Next week button");

                btn.Click();
            }

        }

        internal class GroupTabBase : WOMPage, IWOMTab
        {
            public GroupTabBase(Browser browser) : base(browser)
            {

            }

            public string GetTabsMenuIdString()
            {
                return ".Tabs";
            }
        }

        internal SecurityConfigObject GetGroupDetailsSecurityResult(string section, string aPIString, EnmModifyOption modifyOption)
        {
            if (aPIString.Contains("GroupDetails_Identification_Label_"))
            {
                new ParametersTabPage(_browser);
                bool isEnabledExposed = true;
                if (modifyOption != EnmModifyOption.ShowHideOnly && (GetSeurityLocatorByAPI(aPIString).Displayed
                        || GetSeurityLocatorByAPI(aPIString).GetAttribute("value").Contains("*")))
                {
                    isEnabledExposed = false;
                }

                return new SecurityConfigObject() { ModifyOption = modifyOption, ActualVisibleEnabledExposed = isEnabledExposed, Locator = SecurityLocators.SecurityApiToLocator[aPIString] };
            }

            else if(aPIString== "GroupDetails_List_GridHeader_SelectOffender")
            {
                var edit = _browser.WaitForElement(By.CssSelector("button[title='Edit Group Offenders']"), "edit button");
                edit.WaitToBeClickable();
                edit.ClickSafely();
                _browser.WaitForNotBusyCursor();
                Wait.Until(() => (!edit.Enabled));
                var selectOffender = _browser.WaitForElements(By.CssSelector(".groupDetails-data.groupDetails-data-Header.center._edit_only.groupDetails-data-icon"), "Select offender grid header", loadElementsTimeout: 2, minElementsToGet: 0);
            }

            return GetSecurityResult(aPIString, modifyOption);

        }

        private BrowserElement GetSeurityLocatorByAPI(string aPIString) => _browser.WaitForElement(SecurityLocators.SecurityApiToLocator[aPIString], "Security locator");
        
        public void AddCircularZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
        {
            LocationTab.AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
        }

        public DeleteZonePopupPage DeleteZone()
        {
            return LocationTab.DeleteZone();
        }

        public void AddSchedule(TimeFrame timeFrame)
        {
            ScheduleTab.AddSchedule(timeFrame);
        }

        public DeleteRecurringTimeframesPopupPage DeleteRecurringTimeframes()
        {
            return ScheduleTab.DeleteRecurringTimeframes();
        }

        public void DeleteGroup()
        {
            DetailsTab.DeleteGroup();
        }

        public AddNewOffenderPage SelectOffenderByIndex(int index)
        {
            return DetailsTab.SelectOffenderByIndex(index);
        }



    }
}
