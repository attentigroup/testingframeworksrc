﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class AddressDetailsPopup : PopupPageBase
    {
        
        public AddressDetailsPopup(Browser browser) : base(browser)
        {
          
        }

        public void ClickAddPhone()
        {
            var btnAddPhone = _container.WaitForElements(By.CssSelector("span[title='Add']"), "Add phone button",2, minElementsToGet:0);
            if(btnAddPhone.Any())
            btnAddPhone.SingleOrDefault().Click();
        }


        internal void FillForm(string address)
        {
            _container.WaitForElement(By.Id("street_txt"), "Street text box").Text = address;
            ClickOK();
        }

        private void ClickOK()
        {
            var btnOK = _container.WaitForElement(By.ClassName("_address_ok_btn"), "OK button");

            btnOK.Click();
            _container.WaitToDisappear();
        }
    }
}
