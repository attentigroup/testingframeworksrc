﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;
using WebTests.Common;

namespace WebTests.InfraStructure.Pages
{
    public class EndOfServiceConfirmPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public EndOfServiceConfirmPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "End of service confirm popup");
            Wait.Until(() => _container.Displayed);
        }

        public void OK()
        {
            var btns = _container.WaitForElements(By.TagName("button"), "OK button");
            var btnOK = btns.First();            
            btnOK.WaitToBeClickable();
            btnOK.Click();
        }

        public void Cancel()
        {
            //var btnCancel = _container.WaitForElement(By.Id("cancelButtonbox"), "Cancel button");
            var btns = _container.WaitForElements(By.TagName("button"), "Cancel button");
            var btnCancel = btns.ElementAt(1);
            btnCancel.WaitToBeClickable();
            btnCancel.Click();
        }
    }
}
