﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;
using WebTests.Common;

namespace WebTests.InfraStructure.Pages
{
    public class SuspendProgramPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;

        public SuspendProgramPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("poupBox"), "Suspend program popup");
            Wait.Until(() => _container.Displayed);
        }

        public void FillForm()
        {
            var combobox =_container.WaitForElement(By.TagName("select"), "Reason field");

            combobox.SelectFromComboboxByValue("RSN1");

            var text = _container.WaitForElement(By.TagName("textarea"), "Suspend reason");
            text.Text = "bla bla";          
            
            PressOK();
        }

        public void PressOK()
        {
            var btnOK = _container.WaitForElements(By.CssSelector(".button.generic"), "OK button")[0];
            btnOK.WaitToBeClickable();
            btnOK.Click();
        }
    }
}
