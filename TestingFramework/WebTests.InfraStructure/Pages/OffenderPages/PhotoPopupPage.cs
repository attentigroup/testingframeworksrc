﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class PhotoPopupPage : PopupPageBase
    {
        
        private BrowserElement _frame;

        public PhotoPopupPage(Browser browser):base(browser)
        {

            Wait.Until(() => _container.Displayed);

            _frame = _container.WaitForElement(By.TagName("iframe"), "Popup iFrame");
        }
        public void UploadPhoto()
        {
            _frame.SwitchToThisFrame();

            var btnUpload = _browser.WaitForElement(By.Id("uploadContainer"), "");

            btnUpload.Click();

            new OpenFileDialog(_browser).UploadFile("default.png");

            _container.WaitForElement(By.Id("saveTex"), "Save button").Click();

            _container.WaitToDisappear();

            _browser.SwitchToDefaultFrame();
        }

        public PhotoPopupPage SecurityConfigTest()
        {
            _frame.SwitchToThisFrame();

            var mainImage = _browser.WaitForElement(By.Id("mainImg"), "Main image");

            Wait.Until(() => mainImage.Displayed);
            return this;
        }
    }
}
