﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Helpers;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;


namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class AddTimeFramePopupPage : PopupPageBase
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        

        //DateTimePicker dateTimePicker;
        public AddTimeFramePopupPage(Browser browser) : base(browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Edit cellular data popup");
            Wait.Until(() => _container.Displayed);
        }
         
        public void AddTimeFrame(TimeFrame timeframe)
        {

            var startDate = Convert.ToDateTime(timeframe.StartDate).ToString("dd/MM/yyyy");
            var startDateTxt = _container.WaitForElement(By.Id("fDate"), "");
            new JSHelper(_browser).SetFieldValueById("fDate", startDate);

            var endDate = Convert.ToDateTime(timeframe.StartDate).ToString("dd/MM/yyyy");
            var endDateTxt = _container.WaitForElement(By.Id("tDate"), "");
            new JSHelper(_browser).SetFieldValueById("tDate", endDate);

            var startTime = _container.WaitForElement(By.Id("fTime"), "");
            startTime.Click();
            var sTime =Convert.ToDateTime(timeframe.StartDate).ToString("HH:mm");
            startTime.SendKeys(Keys.Control + "a");
            startTime.SendKeys(sTime);

            var endTime = _container.WaitForElement(By.Id("tTime"), "");
            endTime.Click();
            var eTime = Convert.ToDateTime(timeframe.EndDate).ToString("HH:mm");
            endTime.SendKeys(Keys.Control + "a");
            endTime.SendKeys(eTime);


            var type = _container.WaitForElement(By.Id("tFrameOptions"), "");
            type.SelectFromComboboxByText(timeframe.Type);

            var location = _container.WaitForElement(By.CssSelector(".properties tr:nth-of-type(6) select"), "location");
            if(location.Enabled && (!string.IsNullOrEmpty(timeframe.Location)))
                location.SelectFromComboboxByText(timeframe.Location);


            if (timeframe.RecurseEveryWeek.Equals("True"))
            {
                var recurse = _container.WaitForElement(By.Id("recurrence"), "Recurse every week");
                recurse.Click();
            }
           
            var okBtn = _browser.WaitForElement(By.ClassName("_timeframe_ok_btn"), "ok button");
            okBtn.Click();
        }

        public void UpdateTimeFrame(TimeFrame timeframe)
        {

            var startDate = Convert.ToDateTime(timeframe.StartDate).ToString("dd/MM/yyyy");
            var startDateTxt = _container.WaitForElement(By.Id("fDate"), "");
            new JSHelper(_browser).SetFieldValueById("fDate", startDate);

            var endDate = Convert.ToDateTime(timeframe.StartDate).ToString("dd/MM/yyyy");
            var endDateTxt = _container.WaitForElement(By.Id("tDate"), "");
            new JSHelper(_browser).SetFieldValueById("tDate", endDate);

            var startTime = _container.WaitForElement(By.Id("fTime"), "");
            startTime.Click();
            var s = Convert.ToDateTime(timeframe.StartDate).ToString("HH:mm");
            startTime.SendKeys(Keys.Control + "a");
            startTime.SendKeys(s);

            var endTime = _container.WaitForElement(By.Id("tTime"), "");
            endTime.Click();
            var e = Convert.ToDateTime(timeframe.EndDate).ToString("HH:mm");
            endTime.SendKeys(Keys.Control + "a");
            endTime.SendKeys(e);


            var type = _container.WaitForElement(By.Id("tFrameOptions"), "");
            type.SelectFromComboboxByText(timeframe.Type);

            var location = _container.WaitForElement(By.CssSelector(".properties tr:nth-of-type(6) select"), "location");
            if (location.Enabled && (!string.IsNullOrEmpty(timeframe.Location)))
                location.SelectFromComboboxByText(timeframe.Location);

            var okBtn = _container.WaitForElements(By.TagName("button"), "ok button");
            okBtn[0].Click();

        }

        internal BrowserElement GetContainer()
        {
            return _container;
        }
    }
}
