﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.OffenderPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class OffenderGroupsListPage : WOMPage
    {
        public OffenderGroupsListPage(Browser browser, bool doWaitForLoad = true) : base(browser, doWaitForLoad)
        {

        }

        internal OffenderGroupsListGeneralActionsMenuPage OffenderGroupsListGeneralActionsMenu => new OffenderGroupsListGeneralActionsMenuPage(_browser);

        internal GroupListTablePage GroupListTable => new GroupListTablePage(_browser);

        public GroupsDetailsPage AddNewGroup()
        {
            var btnAdd = _browser.WaitForElement(By.CssSelector(".button.button.square.add_offenders_group"), "Add New Group Button");
            btnAdd.WaitToBeClickable();
            btnAdd.ClickSafely();
            return new GroupsDetailsPage(_browser);
        }

        public OffenderGroupPage SelectGroup(int rowNumber)
        {
            if (GroupListTable.SelectGroup(rowNumber) == false)
                return null;

            return new OffenderGroupPage(_browser);
        }

        public OffenderGroupPage SelectGroupByName(string groupName)
        {
            GroupListTable.SelectGroupByName(groupName);

            return new OffenderGroupPage(_browser);
        }

        public SecurityConfigObject GetSecurityResult(string title, string APIstring)
        {
            return GroupListTable.GetSecurityResult(title, APIstring);
        }

        internal class OffenderGroupsListGeneralActionsMenuPage : GeneralActionsMenuBasePage
        {
            public OffenderGroupsListGeneralActionsMenuPage(Browser browser) : base(browser)
            {

            }
            protected override By GetSaveButtonSelector()
            {
                throw new NotImplementedException();
            }
        }

        internal SecurityConfigObject Navigate2Section(string section, string APIString)
        {
            GroupListTable.SelectGroup();
            if (APIString == "Group_General_Button_SaveGroupParameters")
            {
                //Go to group parameter tab
                var offenderPageHeader = _browser.WaitForElement(By.Id("GroupMasterTemplate"), "");
                var tab = offenderPageHeader.WaitForElement(By.CssSelector(".Tabs li[data-tab='GroupParameters']"), "Group Parameters tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }
            if (APIString == "Schedule_Actions_Button_SaveGroupSchedule")
            {

                var offenderPageHeader = _browser.WaitForElement(By.Id("GroupMasterTemplate"), "");
                var tab = offenderPageHeader.WaitForElement(By.CssSelector(".Tabs li[data-tab='Schedule']"), "Group Schedule tab");
                GoToTab(tab, ApplicationHeaderLocator);

            }
            return GetSecurityResult(APIString);
        }

        internal class GroupListTablePage : WOMTableBase
        {
            public GroupListTablePage(Browser browser) : base(browser)
            {
                WaitForContent();
            }

            public void WaitForContent()
            {
                WaitForContentToLoad();
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("groupsList_DataTable1");
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#groupsListTBody>tr";
            }

            //TODO: are you sure about the name?
            public void SelectGroup()
            {
                var programTypeHeader = _browser.WaitForElement(By.Id("groupsListProgramType"), "program type header");
                programTypeHeader.WaitToBeClickable();
                programTypeHeader.Click();
                Thread.Sleep(250);//animation/transition
                if (programTypeHeader.GetAttribute("data-sorting") != "desc")
                    programTypeHeader.Click();


                var tableRow = GetTableRowElementByNumber(1);

                var clickableColumn = GetClickableColumn(tableRow);
                clickableColumn.WaitToBeClickable();
                clickableColumn.Click();
                clickableColumn.WaitToDisappear();



            }

            protected override string GetClickableColumnCssSelectorString()
            {
                return "td>u";
            }

            internal bool SelectGroup(int rowNumber)
            {
                var groupRow = GetTableRowElementByNumber(rowNumber);
                if (groupRow != null)
                {
                    var clickableCol = GetClickableColumn(groupRow);
                    clickableCol.Click();
                    return true;
                }
                else
                    return false;
            }

            internal void SelectGroupByName(string groupName)
            {
                var rows = GetTableRowsElements();

                BrowserElement tableRow = null;
                
                for (int i = 0; i < rows.Count; i++)
                {
                    if (rows[i].Text.Contains(groupName))
                    {
                        tableRow = rows[i];
                        break;
                    }                                      
                }

                if(tableRow == null)
                    throw new Exception($"No line found for '{groupName}' ");

                var clickableCol = GetClickableColumn(tableRow);
                clickableCol.Click();

            }

            internal SecurityConfigObject GetSecurityResult(string title, string APIString)
            {
                var viewOffenderGroupBtn = GetTableRowElementByNumber(1);
                //TODO replace with GetClickableColumn
                var button = viewOffenderGroupBtn.WaitForElement(By.CssSelector(".groupsList-data-Action.view_offenders_group"), "Memeber list button");
                
                button.ClickSafely();
                //Put in Group memeber popup
                var results = base.GetSecurityResult(APIString);
                //Close popup
                var btnClose = _browser.WaitForElement(By.CssSelector(".button.close"), "Close popup");

                btnClose.WaitToBeClickable();

                btnClose.Click();

                return results;
            }
        }
        
        internal void SelectGroup()
        {
            GroupListTable.SelectGroup();
        }
    }

}
