﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    class GroupListPopupPage
    {

        private readonly Browser _browser;
        private readonly BrowserElement _container;

        public GroupListPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("poupBox"), "Group List Popup Page");
            Wait.Until(() => _container.Displayed);
        }

    }
}

