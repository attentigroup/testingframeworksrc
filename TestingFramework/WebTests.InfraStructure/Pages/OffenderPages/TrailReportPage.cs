﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class TrailReportPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;
        public TrailReportPage(Browser browser)
        {
            _browser = browser;
            _browser.SwitchToLastWindow();
            _container = _browser.WaitForElement(By.ClassName("Wrap"), "Container");
            Wait.Until(() => _container.Text != "");
        }

        public TrailReport GetDetails()
        {
            var rootContainer = _browser.WaitForElement(By.ClassName("Wrap"), "Report root container");
            var header = rootContainer.WaitForElement(By.ClassName("report_header"), "Header").Text;
            var reportDetailElements = rootContainer.WaitForElements(By.CssSelector("div[class='Wrap']>div"), "div");
            var offenderDetails = reportDetailElements[4].Text;
            bool isMapExists = _browser.IsElementVisible(By.TagName("svg"), "Map panel");
            var range = _browser.WaitForElement(By.CssSelector(".body>div:nth-child(2)"), "Range element");
            var rangeDates = range.WaitForElements(By.TagName("h1"), "DateTime");
            var from = DateTime.TryParse(rangeDates[0].Text, new CultureInfo("en-GB"), DateTimeStyles.None, out DateTime fromDateTime);
            var to = DateTime.TryParse(rangeDates[1].Text, new CultureInfo("en-GB"), DateTimeStyles.None, out DateTime toDateTime);

            return new TrailReport() { Header = header, OffenderDetails = offenderDetails, IsMapExists = isMapExists, From = fromDateTime, To = toDateTime };
        }
    }
}
