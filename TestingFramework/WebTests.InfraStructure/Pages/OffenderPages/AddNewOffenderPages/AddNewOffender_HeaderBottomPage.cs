﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        internal class AddNewOffenderHeaderBottomPage : HeaderBottomPageBase
        {
            public AddNewOffenderHeaderBottomPage(Browser browser) : base(browser)
            {
            }
            public override void DownloadNow()
            {
                SelectFromDropdownList(ApplicationHeader, base.GetDownloadButtonLocator(), "Button_DownloadNow", containerLocator: By.CssSelector("#download_menu"));
                var downloadPage = new DownloadAlertPopupPage(_browser);
                downloadPage.PressOk();
            }

            public BrowserElement GetOffenderActionsMenu()
            {
                return GetOffenderActionsButton().WaitForElement(GetOffenderActionsMenuLocator(), "Offender actions menu");
            }

            protected virtual By GetOffenderActionsMenuLocator()
            {
                return By.TagName("ul");
            }

            public override void DownloadNextCall()
            {
                SelectFromDropdownList(ApplicationHeader, base.GetDownloadButtonLocator(), "Button_DownloadNextCall");
            }

            protected override By GetDownloadButtonLocator()
            {
                return By.CssSelector(".btn.download");
            }

            //TODO move to base as abstract
            private By GetOffenderActionsButtonLoactor()
            {
                return By.CssSelector(".btn.manual");

            }

            public void SendEOS(bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations)
            {
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.other"), "Button_SendEndOfService", containerLocator: By.Id("ActionBtn"));

                var EOSPage = new EndOfServicePopupPage(_browser);

                EOSPage.FillForm(noReceiver, isAutoEOS, isSuccessful, isCurfewViolations);
            }

            internal void SendSuspendProgram()
            {
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.other"), "Button_SuspendProgram", containerLocator: By.Id("ActionBtn"));

                var SuspendProgram = new SuspendProgramPopupPage(_browser);

                SuspendProgram.FillForm();
            }

            internal void SendUnsuspendProgram()
            {
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.other"), "Button_UnsuspendProgram", containerLocator: By.Id("ActionBtn"));

                var UnsuspendProgram = new UnsuspendProgramPopupPage(_browser);

                UnsuspendProgram.FillForm();
            }

            internal void SendUpload()
            {
                var btnUpload = _actionsButtonsContainer.WaitForElement(By.CssSelector(".btn.upload"), "Upload");

                btnUpload.WaitToBeClickable();
                btnUpload.Click();

                var UploadAlert = new UploadAlertPopupPage(_browser);

                UploadAlert.PressOK();
            }

            internal void ManualEvent()
            {
                SelectFromDropdownList(ApplicationHeader, GetOffenderActionsButtonLoactor(), "Button_SendManualEvent", containerLocator: By.CssSelector(".current.manual"));
                new ManualEventHandlingPopupPage(_browser).ManualEvent();
            }

            protected override By GetActionsButtonLocator()
            {
                return By.CssSelector("#header_action_buttons>.actions.actionsBar>div:nth-child(2)");
            }

            protected override By GetOffenderActionsButtonLocator()
            {
                return By.CssSelector("#header_action_buttons>.actions.actionsBar>div:nth-child(1)");
            }

            internal ReallocateActiveOffenderPopupPage OpenReallocateOffender()
            {
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.manual"), "Button_ReallocateOffender", containerLocator: By.CssSelector("#header_action_buttons .dropdownNavi.actBut"));

                return new ReallocateActiveOffenderPopupPage(_browser);
            }

            public bool IsSuspendMenuElementVisible()
            {
                var isVisible = GetSuspendMenuElement() != null ? true : false;
                return isVisible;
            }

            private BrowserElement GetSuspendMenuElement()
            {
                var options = GetActionsButton().WaitForElements(By.CssSelector("#ActionBtn>li ul>li"), "Menu item", minElementsToGet:0);
                return options.FirstOrDefault(x => x.GetAttribute("outerHTML").ToLower().Contains("suspendprogram"));
            }
        }
    }
}
