﻿using OpenQA.Selenium;
using System.Linq;
using WebTests.SeleniumWrapper;
using System;
using WebTests.Common;
using System.Collections.Generic;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using System.Drawing;
using System.Threading;
using WebTests.InfraStructure.Pages.MonitorPages;
using static WebTests.InfraStructure.Pages.AddNewUserPage;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public AddNewOffenderPage(Browser browser, bool doWaitForLoad = true) : base(browser, doWaitForLoad)
        {
            if (doWaitForLoad == true)
            {
                var pageTitle = _browser.WaitForElement(By.Id("offenderDetailsTab"), "Add new offender details tab", 7);
                Wait.Until(() => pageTitle.Displayed, "Page is not displayed", 20);
            }
        }

        DetailsTabPage DetailsTab => new DetailsTabPage(_browser);
        DetailsTabPage DetailTab_NoClick => new DetailsTabPage(_browser, true);

        ConfigurationTabPage ConfigurationTab => new ConfigurationTabPage(_browser);

        LocationTabPage LocationTab => new LocationTabPage(_browser);

        ScheduleTabPage ScheduleTab => new ScheduleTabPage(_browser);

        CurrentStatusTabPage CurrentStatusTab => new CurrentStatusTabPage(_browser);

        AddNewOffenderHeaderBottomPage AddNewOffenderHeaderBottom => new AddNewOffenderHeaderBottomPage(_browser);

        OffenderDetailsHeaderPage OffenderDetailsHeader => new OffenderDetailsHeaderPage(_browser);

        AddNewOffenderGeneralActionsMenuPage AddNewOffenderGeneralActionsButtonsMenu => new AddNewOffenderGeneralActionsMenuPage(_browser);

        GroupsDetailsPage GroupsDetails => new GroupsDetailsPage(_browser);

        BrowserElement btnPhoto => _browser.WaitForElement(By.CssSelector(".EntityMaster-EntityHeader-OffenderPhoto.EntityMaster-SmallPhotoSize"), "Photo button");

        //Security methods
        internal SecurityConfigObject GetSecurityResultFromZonesListRow(string resourceID)
        {
            LocationTab.ZonesSecondaryTab.MoveCursorToZonesListRow();
            return GetSecurityResult(resourceID, isMoveCursorToElement: false);
        }

        internal void OpenZoneTooltip()
        {
            LocationTab.ZonesSecondaryTab.OpenZoneTooltip();
        }

        internal void OpenAddZoneMenu()
        {
            LocationTab.ZonesSecondaryTab.OpenAddZoneMenu();
        }

        internal void OpenPointInformationTooltip()
        {
            LocationTab.OpenPointInformationTooltip();
        }

        internal void OpenAddPointsOfInterestPopup()
        {
            LocationTab.ZonesSecondaryTab.OpenAddPointsOfInterestPopup();
        }

        internal void OpenEditCircularZonePopup()
        {
            LocationTab.ZonesSecondaryTab.OpenEditCircularZonePopup();
        }
        //

        internal void CurrentStatusExpandEventById(int eventId)
        {
            CurrentStatusTab.CurrentStatusTable.ExpandEventById(eventId);
        }

        public BrowserElement GetDetailsContentContainer()
        {
            return _browser.WaitForElements(By.Id("offenderDetailsPanel"), "Offender details content container", minElementsToGet: 0, explicitWaitForSingleElement: 3).FirstOrDefault();
        }

        public PhotoPopupPage OpenPhtoPopup()
        {
            GetSmallPhotoElement().Click();
            return new PhotoPopupPage(_browser).SecurityConfigTest();
        }

        internal void ReallocateOffender()
        {
            AddNewOffenderHeaderBottom.OpenReallocateOffender();
        }

        internal void SecurityConfigOffender_Actions()
        {
            AddNewOffenderHeaderBottom.GetActionsButton().MoveCursorToElement();
        }
        
        internal void OffenderDownload()
        {
            AddNewOffenderHeaderBottom.GetDownloadButton().MoveCursorToElement();
        }

        internal void OffenderActions()
        {
            AddNewOffenderHeaderBottom.GetOffenderActionsButton().MoveCursorToElement();
            var menu = AddNewOffenderHeaderBottom.GetOffenderActionsMenu();
            Wait.Until(() => menu.Displayed);
        }

        internal void SecurityConfigOffenderStatus()
        {
            var x = CurrentStatusTab;
        }

        public void GetOffenderScheduleActionsButtonLocator()
        {
            ScheduleTab.ShowScheduleTooltip();
        }

        internal void NavigateToTab(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            var OffenderPageHeader = _browser.WaitForElement(By.Id("OffenderMasterTemplate"), "Offender master page header");
            var tab = OffenderPageHeader.WaitForElement(By.CssSelector("#offender_tabs_menu" + $" li[data-tab='{primaryTabLocatorString}']"), "Details tab");
            GoToTab(tab, ApplicationHeaderLocator);

            if (secondaryTabLocatorString != null)
            {
                var seconaryTab = _browser.WaitForElement(By.Id(secondaryTabLocatorString), $"secondary tab '{secondaryTabLocatorString}'");
                GoToTab(seconaryTab, ApplicationHeaderLocator);
            }
        }

        internal string GetEventStatus(string eventID)
        {
            return CurrentStatusTab.CurrentStatusTable.GetEventStatus(eventID);
        }

        internal string GetEventMessage(string eventMessage)
        {
            return CurrentStatusTab.CurrentStatusTable.GetEventMessage(eventMessage);
        }

        internal void CreateEventIDUserFilter(string eventID)
        {
            CurrentStatusTab.CreateEventIDUserFilter(eventID);
        }

        internal void CreateEventMessageUserFilter(string eventMessage)
        {
            CurrentStatusTab.CreateEventMessageUserFilter(eventMessage);
        }

        internal void DeleteUserFilter(string filterName)
        {
            CurrentStatusTab.DeleteUserFilter(filterName);
        }

        internal void CreateSeverityUserFilter(string severity)
        {
            CurrentStatusTab.CreateSeverityUserFilter(severity);
        }

        internal void CreateEventTimeUserFilter(DateTime eventTime)
        {
            CurrentStatusTab.CreateEventTimeUserFilter(eventTime);
        }

        internal void HandleEvent(string eventId, string handlingOption)
        {
            CurrentStatusTab.CurrentStatusTable.HandleEvent(eventId, handlingOption);
        }

        internal bool VerifyDVLink()
        {
            return OffenderDetailsHeader.VerifyDVLink();
        }

        internal CurrentStatusTableRow GetManualEvent()
        {
            return CurrentStatusTab.CurrentStatusTable.GetManualEvent();
        }
        
        internal string GetFirstEventMessage()
        {
            return CurrentStatusTab.CurrentStatusTable.GetFirstEventMessage();
        }

        internal string GetOffenderTransmitter()
        {
            return DetailsTab.DetailsSecondaryTab.GetOffenderTransmitter();
        }

        internal BrowserElement GetOffenderContactDetails(int index)
        {
            return DetailsTab.DetailsSecondaryTab.GetAddressRowByNumber(index);
        }
        internal OffendersListSceduleTabPage OffendersListSceduleTab => new OffendersListSceduleTabPage(_browser);
        internal SecurityConfigObject Navigate2Section(string section, string aPIString)
        {

            switch (section)
            {
                case "Offender":

                    break;

                case "EventGrid":
                    SecurityConfigOffenderStatus();
                    break;

                case "EventsGrid":
                    SecurityConfigOffenderStatus();
                    break;

                case "TimeFrameTooltip":
                    GetOffenderScheduleActionsButtonLocator();
                    break;
                case "Trail":
                    GetOffenderLocationTrailButtonEventsLocator();
                    break;

                default:
                    break;
            }
            return GetSecurityResult(aPIString);
        }

        internal SecurityConfigObject GetReadOnlyResultForDateTimeElement()
        {
            var dateTimeField = _browser.WaitForElement(By.CssSelector("div[data-modify='OffenderDetails_CustomFields_Label_DateTime1'] input"), "date time custom field");
            bool isEnabled = dateTimeField.Enabled;
            return new SecurityConfigObject() { ModifyOption = EnmModifyOption.EnableDisableOnly, ActualVisibleEnabledExposed = isEnabled, Locator = By.Id("dateID1") };
        }


        private void GetOffenderLocationTrailButtonEventsLocator()
        {
            var btnReports = _browser.WaitForElement(By.CssSelector(".OffenderLocation-TrailExpBtn"), "OffenderLocation-TrailExpBtn");
            btnReports.WaitToBeClickable();
            btnReports.MoveCursorToElement();
        }

        internal string[] GetFilterEventsArray()
        {
            return CurrentStatusTab.CurrentStatusTable.GetFilterEventsArray();
        }

        internal CurrentStatusTableRow[] GetCurrentStatusRowsArray()
        {
            return CurrentStatusTab.CurrentStatusTable.GetCurrentStatusRowsArray();
        }

        internal bool IsMapContainsZone()
        {
            return LocationTab.IsListZoneExist();
        }

        internal CurrentStatusTableRow GetCurrentStatusTableRowByEventId(string getEventId)
        {
            return CurrentStatusTab.CurrentStatusTable.GetCurrentStatusTableRowByEventId(getEventId);

        }

        internal void CreateManualEvent()
        {
            AddNewOffenderHeaderBottom.ManualEvent();
        }

        internal bool IsScheduleContainsTimeFrame()
        {
            return ScheduleTab.IsTimeframeExist();
        }

        public bool IsGroupTimeframeExist(string timeFrameName)
        {
            return ScheduleTab.IsGroupTimeframeExist(timeFrameName);
        }

        internal CurrentStatusTabPage FilterCurrentStatusEventsByPeriod(string recent)
        {
            CurrentStatusTab.FilterEventsByPeriod(recent);

            return CurrentStatusTab;
        }

        public void FilterCurrentStatusEventsByRangeOfDates(DateTime startTime, DateTime endTime)
        {
            CurrentStatusTab.FilterEventByRangeOfDates(startTime, endTime);
        }

        internal string GetVoiceSimData()
        {
            return DetailsTab.DetailsSecondaryTab.GetVoiceSimData();
        }

        internal void DesplayVoiceSimDataTooltip(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            NavigateToTab(primaryTabLocatorString, secondaryTabLocatorString);
            DetailsTab.DetailsSecondaryTab.DesplayVoiceSimDataTooltip();
        }

        internal void OpenCellularDataPopup(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            NavigateToTab(primaryTabLocatorString, secondaryTabLocatorString);
            DetailsTab.DetailsSecondaryTab.OpenCellularDataPopup();
        }
        

        internal SecurityConfigObject GetSecurityResultFromOffenderDetailsRow(string primaryTabLocatorString, string secondaryTabLocatorString, string resourceID)
        {
            NavigateToTab(primaryTabLocatorString, secondaryTabLocatorString);
            if (resourceID.EndsWith("DeleteFile") || resourceID.EndsWith("EditFile"))
                DetailsTab.AdditionalDataSecondaryTab.MoveCursorToOffenderDocumentRow();
            else if(resourceID.EndsWith("DeleteAddress") || resourceID.EndsWith("EditDeleteAddress"))
                DetailsTab.DetailsSecondaryTab.MoveCursorToContactInformationRow();
            else if(resourceID.EndsWith("DeleteContact") || resourceID.EndsWith("EditContact"))
                DetailsTab.AdditionalDataSecondaryTab.MoveCursorToOffenderContactRow();
            return GetSecurityResult(resourceID, isMoveCursorToElement: false);
        }

        internal SecurityConfigObject GetSecurityResultFromCaseManagmentTableRow(string primaryTabLocatorString, string secondaryTabLocatorString, string filterName, string resourceID)
        {
            NavigateToTab(primaryTabLocatorString, secondaryTabLocatorString);
            DetailsTab.CaseManagementSecondaryTab.SelectFilterAndMoveCursorToRow(filterName);
            return GetSecurityResult(resourceID, isMoveCursorToElement: false);
        }

        internal void ClickOnAddPhone(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            NavigateToTab(primaryTabLocatorString, secondaryTabLocatorString);
            DetailsTab.DetailsSecondaryTab.PressEditAddress();

            new AddressDetailsPopup(_browser).ClickAddPhone();
        }

        internal void PressDownloadNow()
        {
            AddNewOffenderHeaderBottom.DownloadNow();
        }

        public void FillWizardAndDownload(OffenderDetails OffenderDetails)
        {
            FillFormFull(OffenderDetails);

            //HeaderActionButtons.DownloadNow();

            AddNewOffenderGeneralActionsButtonsMenu.Delete();
        }

        public OffenderDetails FillFormFull(OffenderDetails offenderDetails)
        {
            FillDetailsFormExtended(offenderDetails);

            //FillRestDetailsForm();           

            var photoPopUp = AddPhoto();
            photoPopUp.UploadPhoto();

            return offenderDetails;
        }

        internal void ClickSendUpload()
        {
            AddNewOffenderHeaderBottom.SendUpload();
        }

        internal void ClickSendSuspendProgram()
        {
            AddNewOffenderHeaderBottom.SendSuspendProgram();
        }

        internal void ClickSendUnsuspendProgram()
        {
            AddNewOffenderHeaderBottom.SendUnsuspendProgram();
        }

        internal string GetOffenderDetailsLabel()
        {
            return OffenderDetailsHeader.GetOffenderDetailsLabel();
        }

        internal string GetOffenderStatus()
        {
            return OffenderDetailsHeader.GetOffenderStatus();
        }

        internal bool isPictureDisplayed()
        {
            return OffenderDetailsHeader.isPictureDisplayed();
        }

        internal List<(int, int)> GetCurrentStatusNumberOfShownResultsPerPage(int showResultsNumber)
        {
            FilterCurrentStatusEventsByPeriod("336"); // 2 weeks in hours
            return CurrentStatusTab.GetShownRows();
        }

        internal bool ClickOnPagerButtons()
        {
            FilterCurrentStatusEventsByPeriod("336"); // 2 weeks in hours
            return CurrentStatusTab.ClickOnPagerButtons();
        }

        internal void MoveToConfigurationTab()
        {
            ConfigurationTab.GoToConfigurationTab();
        }

        internal void MoveToHandlingProceduresTab()
        {
            ConfigurationTab.GoToConfigurationTab();
            ConfigurationTab.GoToHandlingProceduresTab();
        }

        internal void MoveToTrackerRulesTab()
        {
            ConfigurationTab.GoToConfigurationTab();
            ConfigurationTab.GoToTrackerRulesTab();
        }

        internal void UpdateProgramParametersValues(List<ConfigurationElement> list)
        {
            ConfigurationTab.UpdateProgramParametersValues(list);
        }


        public OffenderDetails FillDetailsFormExtended(OffenderDetails OffenderDetails)
        {
            DetailsTab.DetailsSecondaryTab.FillDetailsFormExtended(OffenderDetails);
            SaveForNewOffender();
            return OffenderDetails;
        }

        public OffenderDetails FillDetailsFormBasic(OffenderDetails OffenderDetails)
        {
            DetailsTab.DetailsSecondaryTab.FillDetailsFormBasic(OffenderDetails);
            SaveForNewOffender();
            return OffenderDetails;
        }

        public void InsertDataToTextCustomField(string resourceID, string text)
        {
            DetailTab_NoClick.DetailsSecondaryTab_NoClick.InsertDataToTextCustomField(resourceID, text);
        }

        public void InsertDataToNumericCustomField(string resourceID, string text)
        {
            DetailTab_NoClick.DetailsSecondaryTab_NoClick.InsertDataToNumericCustomField(resourceID, text);
        }

        public void InsertDataToDateTimeCustomField(string resourceID, string text)
        {
            DetailTab_NoClick.DetailsSecondaryTab_NoClick.InsertDataToDateTimeCustomField(resourceID, text);
        }

        public void InsertDataToListCustomField(string resourceID, string text)
        {
            DetailTab_NoClick.DetailsSecondaryTab_NoClick.InsertDataToListCustomField(resourceID, text);
        }

        public void SaveForUpdateOffender()
        {
            AddNewOffenderGeneralActionsButtonsMenu.Save();
        }

        private void SaveForNewOffender()
        {
            AddNewOffenderGeneralActionsButtonsMenu.Save();
            WaitForSave();
        }

        internal void DeleteOffender()
        {
            AddNewOffenderGeneralActionsButtonsMenu.Delete();
        }

        private void FillRestDetailsForm()
        {
            DetailsTab.DetailsSecondaryTab.FillRestDetailsForm();
            AddNewOffenderGeneralActionsButtonsMenu.Save();
            WaitForSave();
        }

        public PhotoPopupPage AddPhoto()
        {
            GetSmallPhotoElement().Click();

            return new PhotoPopupPage(_browser);
        }

        private BrowserElement GetSmallPhotoElement()
        {
            Wait.Until(()=> btnPhoto.Enabled && btnPhoto.Displayed);

            return btnPhoto;
        }

        private void WaitForSave()
        {
            var offenderIdTextBox = GetOffenderIdTextBox();

            offenderIdTextBox.WaitForStailnessOfElement();
            Wait.Until(() => GetOffenderIdTextBox().WaitGetAttribute("disabled", 1, false) == "true");
        }

        private BrowserElement GetOffenderIdTextBox()
        {
            return _browser.WaitForElement(By.Id("ref_id_txt"), "Offender Id textbox");
        }

        public void AddSchedule(TimeFrame timeFrame)
        {
            ScheduleTab.AddSchedule(timeFrame);
        }

        public void UpdateTimeFrame(TimeFrame timeFrameObj, string timeFrameID, string ProgramType)
        {
            ScheduleTab.UpdateTimeFrame(timeFrameObj, timeFrameID, ProgramType);
        }

        internal bool IsTimeframeDisplayedInSchedule(string timeframeID)
        {
            return ScheduleTab.IsTimeframeDisplayedInSchedule(timeframeID);
        }

        public void DeleteTimeFrame(string timeFrameID, bool isE4WeeklyTimeframe)
        {
            ScheduleTab.DeleteTimeFrame(timeFrameID, isE4WeeklyTimeframe);
        }

        public void AddCircularZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
        {
            LocationTab.AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
        }

        public bool CheckGroupZoneExist()
        {
            return LocationTab.CheckGroupZoneExist();
        }

        public void AddPolygonZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule)
        {
            LocationTab.AddPolygonZone(name, limitation, zoneGraceTime, isFullSchedule);
        }

        public bool IsZonesListContainsZones(int zoneID)
        {
            return LocationTab.IsZonesListContainsZones(zoneID);
        }

        public bool IsZoneDisplayedInMap(int zoneID)
        {
            return LocationTab.IsZoneDisplayedInMap(zoneID);
        }

        public void UpdateCircularZone(string name, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
        {
            LocationTab.ZonesSecondaryTab.EditCircularZone(name, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
        }

        public void UpdatePolygonZone(string name, int zoneGraceTime, bool isFullSchedule)
        {
            LocationTab.ZonesSecondaryTab.EditPolygonZone(name, zoneGraceTime, isFullSchedule);
        }

        public void ClickSendEOS(bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations)
        {
            AddNewOffenderHeaderBottom.SendEOS(noReceiver, isAutoEOS, isSuccessful, isCurfewViolations);
        }

        internal OffenderLocation GetOffenderLocationTrailData()
        {
            return LocationTab.GetOffenderLocationTrailData();
        }

        internal void LocationPlayTrail()
        {
            LocationTab.LocationPlayTrail();
        }

        internal TrailReport GetTrailReport()
        {
            return LocationTab.ReportsMenu("OffenderLocation_Trail_Button_TrailReport");
        }

        internal void ClickPrint()
        {
            //LocationTab.GeneralActionsContainerSelector();

        }

        internal void DetailsHeaderSwitchToVictimOrOffender(EnmProgramConcept programConcept)
        {
            OffenderDetailsHeader.SwitchToVictimOrAggressor(programConcept);
        }

        internal SecurityConfigObject GetSecurityResultSuspendProgram()
        {
            return new SecurityConfigObject() { ElementName = "Suspend/UnSuspend Program", ActualVisibleEnabledExposed = AddNewOffenderHeaderBottom.IsSuspendMenuElementVisible() };
        }

        public CurrentStatusTableRow[] GetEventsDetailsFromSpecificSystemFilter(int sectionNumber, string title)
        {
            return CurrentStatusTab.GetEventsDetailsFromSpecificSystemFilter(sectionNumber, title);
        }

        internal string GetAmountOfRowsForSpecificSystemFilter(int sectionNumber, string title)
        {
            return CurrentStatusTab.GetAmountOfRowsForSpecificSystemFilter(sectionNumber, title);
        }

        internal string[] GetSystemFilters()
        {
            return CurrentStatusTab.GetSystemFilters();
        }

        public int GetCurrentStatusFilterCounter(string filterTitle)
        {
            return CurrentStatusTab.GetFilterCounter(filterTitle);
        }

        internal void DeleteZone(string zoneID)
        {
            LocationTab.ZonesSecondaryTab.DeleteZone(zoneID);
        }

        internal void DeleteAllWeeklyTimeframes()
        {
            ScheduleTab.DeleteAllWeeklyTimeframes();
        }

        internal bool GetCustomFieldsVisibilityFromOffenderDetails(string resourceID)
        {
            return DetailTab_NoClick.DetailsSecondaryTab.GetCustomFieldsVisibilityFromOffenderDetails(resourceID);
        }

        internal Dictionary<string, bool> GetCustomFieldsListValuesVisibilityFromOffenderDetails(string resourceID, List<string> values)
        {
            return DetailTab_NoClick.DetailsSecondaryTab.GetCustomFieldsListValuesVisibilityFromOffenderDetails(resourceID, values);
        }

        //************ Internal Tabs Base Class ************
        //*****************************************
        public abstract class AddOffenderTabBasePage : WOMTableBase, IWOMTab
        {
            protected abstract override By GetContentContainerLocator();

            public AddOffenderTabBasePage(Browser browser) : base(browser)
            {
                Wait.Until(() => OffenderPageHeader.Displayed, "Waiting for Offender page to load");
            }

            protected virtual BrowserElement OffenderPageHeader => _browser.WaitForElement(By.Id("OffenderMasterTemplate"), "Offender master page header");
            protected BrowserElement TabsMenuContainer => _browser.WaitForElement(By.CssSelector(GetTabsMenuIdString()), "Tabs menu");

            public string GetTabsMenuIdString()
            {
                return "#offender_tabs_menu";
            }

            public void DetailsTab()
            {
                var tab = GetDetailsTab();

                GoToTab(tab, ApplicationHeaderLocator);
            }

            public BrowserElement GetDetailsTab()
            {
                return OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='OffenderDetails']"), "Details tab");
            }

            public void GoToConfigurationTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='ProgConfig']"), "Configuration tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void GoToHandlingProceduresTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector("[res\\:text='OffenderConfig_General_Tab_HandlingProcedures']"), "Handling Procedures Tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void GoToTrackerRulesTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector("[res\\:text='OffenderConfig_General_Tab_TrackerRules']"), "Tracker Rules Tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }


            public void LocationTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='Location']"), "Location tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void ScheduleTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='Schedule']"), "Schedule tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void CurrentStatusTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='OffenderStatus']"), "Offender status tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            protected void SelectFromCustomComboboxByText(string comboboxId, string optionText, string description)
            {
                var selectOption = GetCustomComboboxOptions(comboboxId, description).FirstOrDefault(x => x.Text == optionText);
                selectOption.MoveCursorToElement(true);
            }

            protected void SelectFromCustomComboboxByContainingText(string comboboxId, string optionText, string description)
            {
                var insertOption = InsertOnComboboxOptions (comboboxId, description, optionText).FirstOrDefault(x => x.Text == optionText);
                var selectOption = FindCustomComboboxOptions(comboboxId, description).FirstOrDefault(x => x.Text.Contains(optionText));
                selectOption.MoveCursorToElement(true);
            }

            protected string SelectFromCustomComboboxByIndex(string comboboxId, int index, string description)
            {
                var selectOption = GetCustomComboboxOptions(comboboxId, description).ElementAt(index);
                var officer = selectOption.Text;
                selectOption.MoveCursorToElement(true);
                return officer;
            }

            protected List<BrowserElement> GetCustomComboboxOptions(string comboboxId, string description)
            {
                var combobox = _browser.WaitForElement(By.XPath($"//input[@id='{comboboxId}']/.."), description);
                var input = combobox.WaitForElement(By.TagName("input"), "Input element");
                Wait.Until(() => input.Displayed);
                input.MoveCursorToElement();
                input.Click();
                Thread.Sleep(300);//Slepp only for animation of combobox!!!
                var options = combobox.WaitForElements(By.CssSelector("ul>li"), "options");

                return options;
            }

            protected List<BrowserElement> InsertOnComboboxOptions(string comboboxId, string description, string optionText)
            {
                var combobox = _browser.WaitForElement(By.XPath($"//input[@id='{comboboxId}']/.."), description);
                var input = combobox.WaitForElement(By.TagName("input"), "Input element");
                Wait.Until(() => input.Displayed);
                input.MoveCursorToElement();
                input.SendKeys(optionText);
                Thread.Sleep(300);
                var options = combobox.WaitForElements(By.CssSelector("ul>li"), "options");

                return options;
            }

            protected List<BrowserElement> FindCustomComboboxOptions(string comboboxId, string description)
            {
                var combobox = _browser.WaitForElement(By.XPath($"//input[@id='{comboboxId}']/.."), description);
                var input = combobox.WaitForElement(By.TagName("input"), "Input element");
                Wait.Until(() => input.Displayed);
                input.MoveCursorToElement();
                Thread.Sleep(300);
                var options = combobox.WaitForElements(By.CssSelector("ul>li"), "options");

                return options;
            }

            public void UpdateProgramParametersValues(List<ConfigurationElement> list)
            {
                bool unclickCommunication = false;

                foreach (var ob in list)
                {
                    BrowserElement eb;

                    try
                    {
                        eb = _browser.WaitForElement(By.Id(ob.ResourceID), ob.ResourceID);
                    }
                    catch(OpenQA.Selenium.NoSuchElementException nsee)
                    {
                        Logger.Warning($"");
                        throw new Exception($"{ob.ResourceID} NoSuchElementException : {nsee.InnerException}", nsee);
                        
                    }
                    catch (Exception e)
                    {
                        Logger.Warning($"Resouce ID({ob.ResourceID}) wasn't found *Might be a correct flow*. error:{e.Message}." + e);
                        continue;
                    }

                    if(ob.Type == ConfigElemntType.ConfigElemntType_radio_button)
                    {
                        var chkd = eb.GetAttribute("checked");
                        if (chkd == null)
                            eb.ClickSafely();
                    }

                    else if (ob.Type == ConfigElemntType.ConfigElemntType_bool)
                    {
                        var chkd = eb.GetAttribute("checked");
                                             
                        if (chkd == null && (ob.Value == "true" || ob.Value == "True"))
                            eb.ClickSafely();

                        else if (chkd != null && (ob.Value == "false" || ob.Value == "False"))
                            eb.ClickSafely();

                    }
                    else if(ob.Type == ConfigElemntType.ConfigElemntType_string)
                    {
                        eb.Text = ob.Value;
                    }
                    else if (ob.Type == ConfigElemntType.ConfigElemntType_dropdown_2_Options)
                    {
                        var optionList = eb.GetOptionList();
                       
                        if (ob.Value == "False")
                            eb.SelectFromComboboxByIndex(1);
                        else
                            eb.SelectFromComboboxByIndex(0);
                    }
                    else if (ob.Type == ConfigElemntType.ConfigElemntType_dropdown_3_And_More_Options)
                    {
                        var  select = new OpenQA.Selenium.Support.UI.SelectElement(eb.GetWebElement());
                        var optionList = select.Options;
                        var listOpetion2Select = new List<int>();

                        for (int i=0; i< optionList.Count; i++)
                        {
                            var x = optionList[i].Selected;
                            if (x == false)
                                listOpetion2Select.Add(i);
                        }
                        Random rnd = new Random();
                        int indexIndexInList = rnd.Next(listOpetion2Select.Count-1);
                        int index = listOpetion2Select[indexIndexInList];
                        eb.SelectFromComboboxByIndex(index);
                    }
                    else if(ob.Type == ConfigElemntType.ConfigElemntType_communication_priority)
                    {
                        // TODO: click on all elements to False, then begin click to True by order of priority

                        if (unclickCommunication == false)
                        {
                            //Click on Landline
                            eb = _browser.WaitForElement(By.Id("chkLandline"), ob.ResourceID);
                            var chkd = eb.GetAttribute("checked");
                            if (chkd != null)
                                eb.ClickSafely();
                            //Click on GPRS
                            eb = _browser.WaitForElement(By.Id("chkGPRS"), ob.ResourceID);
                            chkd = eb.GetAttribute("checked");
                            if (chkd != null)
                                eb.ClickSafely();
                            //Click on CSD
                            eb = _browser.WaitForElement(By.Id("chkCSD"), ob.ResourceID);
                            chkd = eb.GetAttribute("checked");
                            if (chkd != null)
                                eb.ClickSafely();
                            //Click on SMS Callback
                            eb = _browser.WaitForElement(By.Id("chkSMSCallback"), ob.ResourceID);
                            chkd = eb.GetAttribute("checked");
                            if (chkd != null)
                                eb.ClickSafely();

                            unclickCommunication = true;
                        }

                        if(ob.Value == "1")
                        {
                            eb = _browser.WaitForElement(By.Id("chkLandline"), ob.ResourceID);
                            eb.ClickSafely();
                            eb = _browser.WaitForElement(By.Id("chkGPRS"), ob.ResourceID);
                            eb.ClickSafely();
                        }
                        else if(ob.Value == "2")
                        {
                            eb = _browser.WaitForElement(By.Id("chkGPRS"), ob.ResourceID);
                            eb.ClickSafely();
                            eb = _browser.WaitForElement(By.Id("chkLandline"), ob.ResourceID);
                            eb.ClickSafely();
                        }

                    }
                }
                Thread.Sleep(500);
                var saveBtn = _browser.WaitForElement(By.Id("generalSaveBtn"), "Save Button");
                saveBtn.ClickSafely();
            }
        }
    }
}




