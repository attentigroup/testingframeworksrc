﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.OffenderPages.AddNewOffenderPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class DetailsTabPage : AddOffenderTabBasePage
        {
            public DetailsTabPage(Browser browser, bool isNoClick = false) : base(browser)
            {
                if (isNoClick == false)
                    DetailsTab();
            }

            public DetailsSecondaryTabPage DetailsSecondaryTab => new DetailsSecondaryTabPage(_browser);
            public DetailsSecondaryTabPage DetailsSecondaryTab_NoClick => new DetailsSecondaryTabPage(_browser, true);
            public AdditionalDataSecondaryTabPage AdditionalDataSecondaryTab => new AdditionalDataSecondaryTabPage(_browser);
            public CaseManagementSecondaryTabPage CaseManagementSecondaryTab => new CaseManagementSecondaryTabPage(_browser);
            public ProgramDetailsSecondaryTabPage ProgramDetailsSecondaryTab => new ProgramDetailsSecondaryTabPage(_browser);
            public CustomDataSecondaryTabPage CustomDataSecondaryTab => new CustomDataSecondaryTabPage(_browser);
            //public ContactInformationTable ContactInformation => new ContactInformationTable(_browser);

            protected override By GetContentContainerLocator()
            {
                return By.ClassName("OffenderDetailsMain");
            }




            //*** Internal Secondary Tab classes ***

            public class DetailsSecondaryTabPage : AddOffenderTabBasePage
            {
                public DetailsSecondaryTabPage(Browser browser, bool isNoClick = false) : base(browser)
                {
                    if (isNoClick == false)
                    {
                        var detailsSeconaryTab = GetDetailsSecondaryTab();
                        GoToTab(detailsSeconaryTab, ApplicationHeaderLocator);
                    }

                }

                public BrowserElement GetDetailsSecondaryTab()
                {
                    return _browser.WaitForElement(By.Id("offenderDetailsTab"), "'Details' secondary tab");
                }

                public ContactInformationTable ContactInformation => new ContactInformationTable(_browser);

                ProgramDetailsSecondaryTabPage ProgramDetailsSecTab => new ProgramDetailsSecondaryTabPage(_browser);

                internal void FillDetailsFormFull(OffenderDetails OffenderDetails)
                {

                }

                internal OffenderDetails FillDetailsFormExtended(OffenderDetails OffenderDetails)
                {
                    FillBasicFormInternal(OffenderDetails);
                    ProgramDetailsSecTab.FillExtendedForm(OffenderDetails);
                    return OffenderDetails;
                }

                internal OffenderDetails FillDetailsFormBasic(OffenderDetails OffenderDetails)
                {
                    FillBasicFormInternal(OffenderDetails);
                    ProgramDetailsSecTab.FillBasicForm(OffenderDetails);
                    return OffenderDetails;
                }

                internal BrowserElement GetAddressRowByNumber(int index)
                {
                    return ContactInformation.GetAddressRowByNumber(index);
                }


                private OffenderDetails FillBasicFormInternal(OffenderDetails OffenderDetails)
                {
                    //TODO: add only if value is null
                    //TODO: add values in Logic block by generators
                    var offenderId = RandomGenerator.GenerateString(10);
                    GetOffenderIdTextBox().Text = offenderId;
                    OffenderDetails.OffenderRefId = offenderId;

                    var firstName = RandomGenerator.GenerateString(10);
                    _browser.WaitForElement(By.Id("first_name_txt"), "Offender first name").Text = firstName;
                    OffenderDetails.FirstName = firstName;

                    var lastName = RandomGenerator.GenerateString(10);
                    _browser.WaitForElement(By.Id("last_name_txt"), "Offender last name").Text = lastName;
                    OffenderDetails.LastName = lastName;

                    SelectFromCustomComboboxByText("agency_txt", OffenderDetails.Agency, "Agency selection");

                    SelectFromCustomComboboxByText("officer_txt", OffenderDetails.Officer, "Officer selection"); //SelectFromCustomComboboxByIndex("officer_txt", 0, "Officer selection");                   

                    ContactInformation.EditAddress(1, RandomGenerator.GenerateString(10));

                    return OffenderDetails;
                }

                //TODO merge this with FillExtendedForm
                internal void FillRestDetailsForm()
                {
                    //only for e4
                    //transmiter

                    //SelectFromCustomComboboxByText("receiver_txt", serialNumber, "Receiver combobox");
                    SelectFromCustomComboboxByIndex("transmitter_txt", 0, "Receiver combobox");
                    //var options = GetCustomComboboxOptions("transmitter_txt", "Receiver combobox");
                    //int i;

                    //for (i = 0; i < options.Count; i++)
                    //{
                    //    if (options[i].Text.Contains("serialNumber"))
                    //        break;
                    //}

                    //options[i].MoveCursorToElement(true);

                    //landline
                    var landlinePrefix = _browser.WaitForElement(By.Id("landline_prefix_txt"), "");
                    landlinePrefix.Text = RandomGenerator.GenerateNumber(1000).ToString();

                    var landlinePhone = _browser.WaitForElement(By.Id("landline_phone_txt"), "");
                    landlinePrefix.Text = RandomGenerator.GenerateNumber(1000000).ToString();

                    var outsideline = _browser.WaitForElement(By.Id("landline_ext_txt"), "");
                    landlinePrefix.Text = RandomGenerator.GenerateNumber(100).ToString();


                }

                private BrowserElement GetOffenderIdTextBox()
                {
                    return _browser.WaitForElement(By.Id("ref_id_txt"), "Offender Id");
                }

                public string GetVoiceSimData()
                {
                    return ProgramDetailsSecTab.GetVoiceSimData();
                }

                public void DesplayVoiceSimDataTooltip()
                {
                    ProgramDetailsSecTab.DesplayVoiceSimDataTooltip();
                }

                public void OpenCellularDataPopup()
                {
                    ProgramDetailsSecTab.OpenCellularDataPopup();
                }

                public void MoveCursorToContactInformationRow()
                {
                    ContactInformation.MoveCursorToContactInformationRow();
                }

                public string GetOffenderTransmitter()
                {
                    return ProgramDetailsSecTab.GetOffenderTransmitter();
                }

                protected override By GetContentContainerLocator()
                {
                    return By.ClassName("OffenderDetailsMain");
                }


                internal SecurityConfigObject GetDetailsSecondarySecurityResult(string aPIString)
                {
                    return GetSecurityResult(aPIString);
                }

                internal void PressEditAddress()
                {
                    ContactInformation.EditLastAddress();
                }


                internal bool GetCustomFieldsVisibilityFromOffenderDetails(string resourceID)
                {
                    var customFieldElement = _browser.WaitForElements(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element", minElementsToGet: 0);
                    if (customFieldElement.Count() != 0)
                        return customFieldElement[0].Displayed;
                    else
                        return false;
                }

                internal Dictionary<string, bool> GetCustomFieldsListValuesVisibilityFromOffenderDetails(string resourceID, List<string> values)
                {
                    var CustomFieldsListValuesVisibility = new Dictionary<string, bool>();
                    var customFieldElement = _browser.WaitForElement(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element");
                    var listElement = customFieldElement.WaitForElement(By.TagName("select"), "Custom field combobox");
                    var options = listElement.WaitForElements(By.TagName("option"), "List options");
                    
                    foreach (var value in values)
                    {
                        var isExist = options.Any(x => x.Text.Equals(value));
                        CustomFieldsListValuesVisibility.Add(value, isExist);
                    }
                    return CustomFieldsListValuesVisibility;
                    
                }

                internal void InsertDataToCustomField(string resourceID)
                {

                    var customFieldElement = _browser.WaitForElement(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element");
                    if (resourceID.Contains("Text"))
                    {
                        //TODO Noa: Generate string
                        
                        var textFieldStr = "test";
                        var inputElement = customFieldElement.WaitForElement(By.CssSelector("[id^='fieldID']"), "Custom field text box");
                        inputElement.Text = textFieldStr;

                    }
                    else if (resourceID.Contains("Numeric"))
                    {
                        Random rnd = new Random();
                        var number = rnd.Next(1, 100);
                        var inputElement = customFieldElement.WaitForElement(By.TagName("input"), "Custom field numeric text box");
                        inputElement.Text = number.ToString();
                    }
                    else if (resourceID.Contains("DateTime"))
                    {
                        var dateElement = customFieldElement.WaitForElement(By.CssSelector("input[id^='dateID']"), "Custom field DateTime picker");
                        dateElement.WaitToBeClickable();
                        dateElement.Click();
                        dateElement.SendKeys(Keys.Enter);
                        dateElement.SendKeys(Keys.Enter);
                    }
                    else if (resourceID.Contains("ListItems"))
                    {
                        var listElement = customFieldElement.WaitForElement(By.TagName("select"), "Custom field combobox");
                        listElement.SelectFromComboboxByIndex(1);
                    }
                    else
                        throw new Exception($"No matching type found for ResourceID: {resourceID}!");                    

                }

                public void InsertDataToTextCustomField(string resourceID, string text)
                {
                    var customFieldElement = _browser.WaitForElement(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element");

                        var inputElement = customFieldElement.WaitForElement(By.CssSelector("[id^='fieldID']"), "Custom field text box");
                        inputElement.Text = text;
                }

                public void InsertDataToNumericCustomField(string resourceID, string numericText)
                {
                    var customFieldElement = _browser.WaitForElement(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element");

                    var inputElement = customFieldElement.WaitForElement(By.TagName("input"), "Custom field numeric text box");
                    inputElement.Text = numericText;
                }

                public void InsertDataToDateTimeCustomField(string resourceID, string dateTime = "")
                {
                    var customFieldElement = _browser.WaitForElement(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element");

                    var dateElement = customFieldElement.WaitForElement(By.CssSelector("input[id^='dateID']"), "Custom field DateTime picker");
                    dateElement.WaitToBeClickable();
                    dateElement.Click();
                    dateElement.SendKeys(Keys.Enter);
                    dateElement.SendKeys(Keys.Enter);
                }

                public void InsertDataToListCustomField(string resourceID, string listItem)
                {
                    var customFieldElement = _browser.WaitForElement(By.CssSelector($"[data-modify='{resourceID}']"), "Custom field element");

                    var listElement = customFieldElement.WaitForElement(By.TagName("select"), "Custom field combobox");
                    listElement.SelectFromComboboxByIndex(int.Parse(listItem));
                }

                public class ContactInformationTable : WOMTableBase
                {
                    //private readonly Browser _browser;
                    private readonly BrowserElement _container;
                    public ContactInformationTable(Browser browser) : base(browser)
                    {
                        _container = _browser.WaitForElement(By.CssSelector(".offenderDetails_ContactInfoHolder.EntityDetails-Box-content"), "Contact info table");
                    }

                    public BrowserElement GetAddressRowByNumber(int rowNumber)
                    {
                       var row = GetTableRowElementByNumber(rowNumber);
                        //TODO: remove duplications
                        //var row = _container.WaitForElement(By.CssSelector($"table tbody>tr:nth-of-type({rowNumber})"), "Table row");

                        return row;
                    }
                    //TODO: replace street address with object
                    public void EditAddress(int rowNumber, string address)
                    {
                        ClickEditAddresButton(rowNumber);

                        var addressDetailsPopup = new AddressDetailsPopup(_browser);
                        addressDetailsPopup.FillForm(address);
                    }

                    public void ClickEditAddresButton(int rowNumber)
                    {
                        var row = GetAddressRowByNumber(rowNumber);
                        row.MoveCursorToElement(true);

                        var editButton = row.WaitForElement(By.Id("edit_address_btn"), "Edit address");
                        Wait.Until(() => editButton.Displayed);

                        editButton.MoveCursorToElement(true);
                    }

                    public void EditLastAddress()
                    {
                        var rowCount = GetTableRowsElements().Count;
                        ClickEditAddresButton(rowCount);
                    }

                    public SecurityConfigObject GetContactInformationSecurityResult(string aPIString)
                    {
                        ClickEditAddresButton(2);
                        return GetSecurityResult(aPIString);
                    }

                    protected override By GetContentContainerLocator()
                    {
                        return By.CssSelector(".body.offenderDetails_ContactInfoScroll");
                    }

                    protected override string GetTableRowCssSelectorString()
                    {
                        return ".offenderDetails_ContactInfoHolder table tr.tableGeneralDataData";
                    }

                    internal void MoveCursorToContactInformationRow()
                    {
                        var row = GetAddressRowByNumber(2);
                        row.MoveCursorToElement();
                    }
                }               
            }

            public class AdditionalDataSecondaryTabPage : AddOffenderTabBasePage
            {
                public AdditionalDataSecondaryTabPage(Browser browser) : base(browser)
                {
                    var addtionalDataSeconaryTab = _browser.WaitForElement(By.Id("additionalDataTab"), "'Additional Data' secondary tab");
                    GoToTab(addtionalDataSeconaryTab, ApplicationHeaderLocator);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("additionalDataPanel");
                }


                internal SecurityConfigObject GetAdditionalDataSecurityResult(string aPIString)
                {
                    return GetSecurityResult(aPIString);
                }

                internal void MoveCursorToOffenderContactRow()
                {
                    var tableRow = _browser.WaitForElements(By.CssSelector("#offenderContactTbl>tr"), "table row");
                    tableRow[0].MoveCursorToElement();
                }

                internal void MoveCursorToOffenderDocumentRow()
                {
                    var tableRow = _browser.WaitForElements(By.CssSelector("#tbl>div"), "table row");
                    tableRow[0].MoveCursorToElement();
                }
            }

            public class CaseManagementSecondaryTabPage : AddOffenderTabBasePage
            {
                internal CaseManagmentFilterSectionPage CaseManagmentFilterSection => new CaseManagmentFilterSectionPage(_browser);

                protected override By GetContentContainerLocator()
                {
                    return By.Id("tableContainer");
                }

                protected override string GetTableRowCssSelectorString()
                {
                    return "#tableContainer .row";
                }

                internal void AddWarning()
                {
                    BrowserElement buttonElement = null;
                    var idStrings = new string[] {"warningsAddDiv","ScheduleAddDiv", "AbsenceRequestAddDiv" };

                    foreach (var idString in idStrings)
                    {
                        buttonElement = _browser.WaitForElement(By.CssSelector($"#{idString} button"), "Add warning button", 2);
                        if (buttonElement.Displayed)
                            break;
                    }


                    buttonElement?.MoveCursorToElement();
                    buttonElement?.ClickSafely();

                    new AddOffenderWarningPopupPage(_browser).FillForm();
                }

                public CaseManagementSecondaryTabPage(Browser browser) : base(browser)
                {
                    var caseManagementSeconaryTab = _browser.WaitForElement(By.Id("caseManagmentTab"), "Case Managment secondary tab");
                    GoToTab(caseManagementSeconaryTab, ApplicationHeaderLocator);
                }

                internal SecurityConfigObject GetCaseManagmentSecurityResult(string aPIString)
                {
                    return GetSecurityResult(aPIString);
                }

                public void SelectFilterAndMoveCursorToRow(string filterNameIdString)
                {
                    ClickOnCurrentStatusFilters(filterNameIdString);

                    var tableRow = GetTableRowElementByNumber(1);

                    if (tableRow.Text == string.Empty || tableRow.Text == "No Available Data")
                    {
                        AddWarning();
                        tableRow = GetTableRowElementByNumber(1);
                    }

                    tableRow.MoveCursorToElement();
                }
                public void ClickOnCurrentStatusFilters(string filterNameIdString)
                {
                    //TODO move to CaseManagmentFilterSectionPage
                    var filterConteiner = _browser.WaitForElement(By.Id("FilterWrapperCaseManagment"), "filter conteiner");

                    var filterRow = filterConteiner.WaitForElement(By.Id(filterNameIdString + "MenuItem"), $"filter row: {filterNameIdString}");

                    filterRow.WaitToBeClickable();

                    filterRow.ClickSafely();
                }

                internal class CaseManagmentFilterSectionPage : FilterSectionBase
                {
                    public CaseManagmentFilterSectionPage(Browser browser) : base(browser)
                    {

                    }

                    protected override By GetFilterContainerSelector()
                    {
                        return By.Id("FilterWrapperCaseManagment");
                    }
                }
            }

            public class ProgramDetailsSecondaryTabPage : AddOffenderTabBasePage
            {
                public ProgramDetailsSecondaryTabPage(Browser browser) : base(browser)
                {

                }

                public void FillExtendedForm(OffenderDetails offenderDetails)
                {
                    FillBasicForm(offenderDetails);

                    SelectFromCustomComboboxByContainingText("receiver_txt", offenderDetails.Receiver, "Receiver combobox");

                    if(offenderDetails.Tx != null)
                        SelectFromCustomComboboxByContainingText("transmitter_txt", offenderDetails.Tx, "Tx combobox");

                }

                public void FillBasicForm(OffenderDetails offenderDetails)
                {
                    var programTypeSelect = _browser.WaitForElement(By.Id("program_type_txt"), "Program type combobox");
                    programTypeSelect.SelectFromComboboxByText(offenderDetails.ProgramType);

                    if (offenderDetails.SubType != null)
                    {
                        var subTypeSelect = _browser.WaitForElement(By.Id("sub_type_txt"), "Sub type combobox");
                        subTypeSelect.SelectFromComboboxByText(offenderDetails.SubType);
                    }

                    if (offenderDetails.RelatedOffenderRefId != null)
                    {
                        //TODO: check if RelatedOffenderRefId value = offender id
                        var victimSelect = _browser.WaitForElement(By.Id("victim_list_txt"), "Victim combobox");
                        victimSelect.SelectFromComboboxByText(offenderDetails.RelatedOffenderRefId);
                    }
                }

                internal void FillFullForm(OffenderDetails offenderDetails)
                {
                    FillExtendedForm(offenderDetails);
                    //TODO add the remaining form e.g. photo

                }

                internal string GetVoiceSimData()
                {
                    DesplayVoiceSimDataTooltip();

                    var tooltip = _browser.WaitForElement(By.CssSelector("#tooltip"), "info tooltip");
                    Wait.Until(() => tooltip.Text != "");
                    var data = tooltip.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    var voiceSim = data[0].Replace(" ", String.Empty);
                    var simNumber = voiceSim.Split(new string[] { "SIMVoiceNumber" }, StringSplitOptions.RemoveEmptyEntries);
                    var number = simNumber[0].Replace("-", String.Empty);
                    return number;
                }

                public void DesplayVoiceSimDataTooltip()
                {
                    var info = _browser.WaitForElement(By.CssSelector(".general-toolTip-info-i.no_field"), "Info icon");
                    info.MoveCursorToElement();
                    var infoTooltip = _browser.WaitForElement(By.Id("tooltipContainer"), "info tooltip");
                    Wait.Until(() => infoTooltip.Displayed);
                }

                public void OpenCellularDataPopup()
                {
                    DesplayVoiceSimDataTooltip();
                    var tooltip = _browser.WaitForElement(By.Id("tooltipContainer"), "voice sim tooltip");
                    var editButton = tooltip.WaitForElements(By.TagName("button"), "edit button");
                    editButton[0].WaitToBeClickable();
                    editButton[0].Click();
                    new EditCellularDataPopupPage(_browser);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("programDetailsAndCustomFields");
                }

                internal string GetOffenderTransmitter()
                {
                    var txInfo = _browser.WaitForElement(By.Id("transmitter_txt"), "Transmitter");
                    var value = txInfo.WaitGetAttribute("value");

                    return value;
                }
            }

            public class CustomDataSecondaryTabPage : AddOffenderTabBasePage
            {
                public CustomDataSecondaryTabPage(Browser browser) : base(browser)
                {

                }

                protected override By GetContentContainerLocator()
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}
