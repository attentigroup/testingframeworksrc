﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class ConfigurationTabPage : AddOffenderTabBasePage
        {
            public ProgramParametersSecondaryTabPage ProgramParametersSecondaryTab => new ProgramParametersSecondaryTabPage(_browser);

            public HandlingProceduresSecondaryTabPage HandlingProceduresSecondaryTab => new HandlingProceduresSecondaryTabPage(_browser);

            public TrackerRulesSecondaryTabPage TrackerRulesSecondaryTab => new TrackerRulesSecondaryTabPage(_browser);


            public ConfigurationTabPage(Browser browser) : base(browser)
            {
                GoToConfigurationTab();
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("configMainContent");
            }

            //*** Internal Secondary Tab classes ***
            public class ProgramParametersSecondaryTabPage : AddOffenderTabBasePage
            {
                public ProgramParametersSecondaryTabPage(Browser browser) : base(browser)
                {
                    var HandlingProceduresSeconaryTab = _browser.WaitForElement(By.Id("programTab"), "Program Parameters secondary tab");
                    GoToTab(HandlingProceduresSeconaryTab, ApplicationHeaderLocator);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("configMainContent");
                }

                internal SecurityConfigObject GetProgramParametersSecurityResult(string aPIString)
                {
                    return GetSecurityResult(aPIString);
                }

                //method:
                //input: list<ResourceDescForInput>
                //Action: find element by ResourceDescForInput.ResourceID and change by value
                //ResourceDescForInput: ResourceID, Type(bool, string), Value

                
                public void UpdateProgramParametersValues(List<ConfigurationElement> list)
                {
                    foreach(var ob in list)
                    {
                        var eb = _browser.WaitForElement(By.Id(ob.ResourceID), ob.ResourceID);
                        if (ob.Type == ConfigElemntType.ConfigElemntType_bool)
                        {
                            if (!eb.Text.Contains("checked") && ob.Value == "true")
                                eb.ClickSafely();

                            else if(eb.Text.Contains("checked") && ob.Value == "false")
                                eb.ClickSafely();
                        }
                        else
                        {
                            eb.Text = ob.Value;
                        }
                    }
                }
            }

            public class HandlingProceduresSecondaryTabPage : AddOffenderTabBasePage
            {
                public HandlingProceduresSecondaryTabPage(Browser browser) : base(browser)
                {
                    var HandlingProceduresSeconaryTab = _browser.WaitForElement(By.Id("handlingProTab"), "Handling Procedures secondary tab");
                    GoToTab(HandlingProceduresSeconaryTab, ApplicationHeaderLocator);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("handlingProPanel");
                }

                internal SecurityConfigObject GetHandlingProceduresSecurityResult(string aPIString)
                {
                    return GetSecurityResult(aPIString);
                }
            }

            public class TrackerRulesSecondaryTabPage : AddOffenderTabBasePage
            {
                public TrackerRulesSecondaryTabPage(Browser browser) : base(browser)
                {
                    var TrackerRulesSeconaryTab = _browser.WaitForElement(By.Id("handlingProPanel"), "Tracker Rules secondary tab");
                    GoToTab(TrackerRulesSeconaryTab, ApplicationHeaderLocator);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("trackersRulesPanel");
                }

                internal SecurityConfigObject GetTrackerRulesSecurityResult(string aPIString)
                {
                    return GetSecurityResult(aPIString);
                }
            }
        }
    }
}
