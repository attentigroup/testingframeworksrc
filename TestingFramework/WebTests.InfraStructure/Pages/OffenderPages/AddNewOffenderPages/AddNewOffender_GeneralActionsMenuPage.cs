﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class AddNewOffenderGeneralActionsMenuPage : GeneralActionsMenuBasePage
        {
            public AddNewOffenderGeneralActionsMenuPage(Browser browser) : base(browser)
            {

            }

            protected override By GetSaveButtonSelector()
            {
                return By.CssSelector("#generalSaveBtn>button");
            }
        }
    }
}
