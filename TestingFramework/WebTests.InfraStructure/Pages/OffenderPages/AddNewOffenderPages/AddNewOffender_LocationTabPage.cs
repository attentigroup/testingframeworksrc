﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.MonitorPages;
using WebTests.InfraStructure.Pages.OffenderPages.AddNewOffenderPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class LocationTabPage : AddOffenderTabBasePage
        {
            BrowserElement Container;
            public LocationTabPage(Browser browser) : base(browser)
            {
                LocationTab();

                Container = _browser.WaitForElement(By.Id("locationBody"), "Container");

                Wait.Until(() => Container.Displayed && Container.Text != string.Empty);

                _browser.WaitForNotBusyCursor();
            }

            private TrailSecondaryTabPage TrailSecondaryTab => new TrailSecondaryTabPage(_browser);
            public ZonesSecondaryTabPage ZonesSecondaryTab => new ZonesSecondaryTabPage(_browser);
            private MapAreaPage MapArea => new MapAreaPage(_browser);
            private BrowserElement GetZoneContentContainer => _browser.WaitForElement(By.ClassName("OffenderLocation-ZoneContent"), "");

            internal void AddCircularZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
            {
                ZonesSecondaryTab.AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
            }

            internal bool CheckGroupZoneExist()
            {
                return ZonesSecondaryTab.CheckGroupZoneExist();
            }

            public void AddPolygonZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule)
            {
                ZonesSecondaryTab.AddPolygonZone(name, limitation, zoneGraceTime, isFullSchedule);
            }

            public bool IsZonesListContainsZones(int zoneID)
            {
                return ZonesSecondaryTab.IsZonesListContainsZones(zoneID);
            }

            public bool IsZoneDisplayedInMap(int zoneID)
            {
                return ZonesSecondaryTab.IsZoneDisplayedInMap(zoneID);
            }

            internal OffenderLocation GetOffenderLocationTrailData()
            {
                var OffenderLocation = TrailSecondaryTab.GetOffenderLocationTrailData();
                bool isTrailLastPointOnMapExists = GetIsTrailLastPointOnMapExists();
                OffenderLocation.TrailLastPointOnMapExists = isTrailLastPointOnMapExists;
                return OffenderLocation;
            }



            internal void LocationPlayTrail()
            {
                MapArea.PlayTrail();
            }

            private bool GetIsTrailLastPointOnMapExists()
            {
                return MapArea.IsTrailLastPointOnMapExists();
            }

            internal bool IsListZoneExist()
            {
                return ZonesSecondaryTab.IsListZoneExist();
            }

            public TrailReport ReportsMenu(string optionStringIdentifier)
            {
                var trailReport = TrailSecondaryTab.GetReport(optionStringIdentifier);

                return trailReport;
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("locationBody");
            }

            public void GeneralActionsContainerSelector()
            {
                //return By.CssSelector(".navigation_bottom .actions");

                //var actionsBtnsContainer = _browser.WaitForElement(By.Id("generalActionsMenu"), "Import button");
                //var actionsBtnsContainer = _browser.WaitForElements(By.CssSelector(".navigation_bottom.actions"), "generalActionsMenu");

                //var actionsBtns = actionsBtnsContainer.Find(x => x.Text.Contains("Print"));

                //printBtn.WaitToBeClickable();
                //printBtn.ClickSafely();

                //new EditCellularDataPopupPage(_browser);
            }

            internal void OpenPointInformationTooltip()
            {
                var trail = TrailSecondaryTab;
                //TrailSecondaryTab.FilterTrailByRangeOfTime();
                MapArea.OpenPointInformationTooltip();
            }


            // *** Internal tab classes ***

            public class TrailSecondaryTabPage : LocationSecondaryTabBase
            {
                public TrailSecondaryTabPage(Browser browser) : base(browser)
                {
                    TrailTab();
                }

                private TrailReportPage TrailReport => new TrailReportPage(_browser);

                internal OffenderLocation GetOffenderLocationTrailData()
                {
                    var lastPointDataElement = GetLastPointDataElement();

                    var nonDataLabel = ContentContainer.WaitForElements(By.Id("noDataLabel"), "Non data label", minElementsToGet: 0);

                    Wait.Until(() => lastPointDataElement.Text != string.Empty || nonDataLabel.FirstOrDefault().Text != string.Empty);

                    var FromToTuple = DisplayTrailByRecentTime(1);

                    return new OffenderLocation() { NonDataLabel = nonDataLabel.FirstOrDefault().Text, LastTrailPointDateTime = lastPointDataElement.Text, FromDateTime = FromToTuple.Item1, ToDateTime = FromToTuple.Item2 };
                }

                private BrowserElement GetLastPointDataElement()
                {
                    return ContentContainer.WaitForElements(By.Id("lastPointData"), "Last point", minElementsToGet: 0).FirstOrDefault();
                }

                /// <summary>
                /// This methods returns the time range of Recent filtering
                /// </summary>
                /// <param name="selectionIndex"></param>
                /// <returns></returns>
                public ValueTuple<DateTime, DateTime> DisplayTrailByRecentTime(int selectionIndex)
                {
                    var comboboxRecent = ContentContainer.WaitForElement(By.Id("recentDdl"), "Recent combobox");
                    var combobox = comboboxRecent.SelectFromComboboxByIndex(selectionIndex);

                    var btnDisplay = ContentContainer.WaitForElement(By.Id("load_points"), "Display button");
                    btnDisplay.Click();
                    GetLastPointDataElement();

                    var recentTimeSelected = DateTime.Now.AddHours(-1d * double.Parse(combobox.Text.Split(' ')[0]));
                    return (recentTimeSelected, DateTime.Now);
                }

                public TrailReport GetReport(string optionStringIdentifier)
                {
                    SelectFromDropdownList(ContentContainer, By.ClassName("OffenderLocation-MenuTrig"), optionStringIdentifier);

                    return TrailReport.GetDetails();
                }

                internal void FilterTrailByRangeOfTime()
                {
                    var rangeOfTimeRadioButton = _browser.WaitForElement(By.Id("customRadio"), "Range of date radio button");
                    rangeOfTimeRadioButton.Click();
                    var displayBtn = _browser.WaitForElement(By.Id("load_points"), "");
                    displayBtn.Click();
                }
            }

            public class ZonesSecondaryTabPage : LocationSecondaryTabBase
            {
                public ZonesSecondaryTabPage(Browser browser) : base(browser)
                {
                    ZoneTab();
                }

                internal bool IsListZoneExist()
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");

                    var zones = zoneList.WaitForElements(By.CssSelector(".zone.selected"), "", minElementsToGet: 0);

                    if (zones.Any())
                        return true;

                    return false;
                }

                internal void MoveCursorToZonesListRow()
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");
                    var zones = zoneList.WaitForElements(By.ClassName("OffenderLocation-ZoneNameWrapper"), "zones");
                    zones[0].MoveCursorToElement();
                }

                internal void OpenZoneTooltip()
                {
                    var zone = _browser.WaitForElement(By.CssSelector(".esriMapLayers>svg"), "Map zone");
                    zone.WaitToBeClickable();
                    zone.MoveCursorToElement();
                    zone.Click();

                    var mapTooltip = new MapToolTip(_browser);
                }

                internal void OpenAddPointsOfInterestPopup()
                {
                    var addZone = _browser.WaitForElement(By.CssSelector(".draw.add-zone-menue"), "add zone button");
                    addZone.MoveCursorToElement();
                    var addZoneMenu = _browser.WaitForElement(By.CssSelector(".panel.add-zone-menue"), "add zone menu");
                    var menuElements = addZoneMenu.WaitForElements(By.TagName("div"), "menu elements");
                    menuElements.Last().Click();
                }

                internal void OpenAddZoneMenu()
                {
                    var addZone = _browser.WaitForElement(By.CssSelector(".draw.add-zone-menue"), "add zone button");
                    addZone.MoveCursorToElement();
                }

                internal void OpenEditCircularZonePopup()
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");
                    var zones = zoneList.WaitForElements(By.ClassName("OffenderLocation-ZoneNameWrapper"), "zones");
                    zones[0].MoveCursorToElement();
                    var editBtn = zoneList.WaitForElements(By.CssSelector(".icon.edit"), "edit button");
                    editBtn[0].WaitToBeClickable();
                    editBtn[0].Click();
                    new EditCellularDataPopupPage(_browser);
                }

                internal void EditCircularZone(string name, int graceTime, bool isFullSchedule, int realRadious, int bufferRadious)
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");
                    var zones = zoneList.WaitForElements(By.ClassName("OffenderLocation-ZoneNameWrapper"), "zones");
                    zones[0].MoveCursorToElement();
                    var editBtn = zoneList.WaitForElements(By.CssSelector(".icon.edit"), "edit button");
                    editBtn[0].WaitToBeClickable();
                    editBtn[0].Click();
                    new AddNewCircularZonePopupPage(_browser).AddCircularZone(name: name, zoneGraceTime: graceTime, isFullSchedule: isFullSchedule, realRadious: realRadious, bufferRadious: bufferRadious);
                }

                internal void EditPolygonZone(string name, int graceTime, bool isFullSchedule)
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");
                    var zones = zoneList.WaitForElements(By.ClassName("OffenderLocation-ZoneNameWrapper"), "zones");
                    //TODO: Noa- finding specific zone by text
                    zones[0].MoveCursorToElement();
                    var editBtn = zoneList.WaitForElements(By.CssSelector(".icon.edit"), "edit button");
                    editBtn[0].WaitToBeClickable();
                    editBtn[0].Click();
                    new AddNewCircularZonePopupPage(_browser).AddCircularZone(name: name, zoneGraceTime: graceTime, isFullSchedule: isFullSchedule);
                }

                internal void AddPolygonZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule)
                {
                    var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");

                    var btnAddPolygonZone = _browser.WaitForElement(By.Id("add_polygon_from_toolbar"), "Add polygon zone");
                    btnAddPolygonZone.WaitToBeClickable();
                    btnAddPolygonZone.Click();

                    slider.DrawPolygon(new Point[] { new Point(100, 100), new Point(50, 50), new Point(50, 10) });

                    new AddNewCircularZonePopupPage(_browser).AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule);
                }

                internal void AddCircularZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
                {
                    var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");
                    var btnAddCircularZone = _browser.WaitForElement(By.Id("add_circular_from_toolbar"), "Add circular zone");
                    btnAddCircularZone.WaitToBeClickable();
                    btnAddCircularZone.Click();

                    slider.DragAndDropFromToOffset(100, 100);

                    new AddNewCircularZonePopupPage(_browser).AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
                }

                internal bool CheckGroupZoneExist()
                {
                    BrowserElement zone;
                    try
                    {
                        zone = _browser.WaitForElement(By.CssSelector(".sprt_offender.offenders_group"), "Group Zone");
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    if (zone.Displayed)
                        return true;

                    return false;
                }

                internal bool IsZonesListContainsZones(int zoneID)
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");

                    var zone = zoneList.WaitForElement(By.CssSelector($"div[data-zone-id='{zoneID}']"), "");

                    if (zone != null)
                        return true;

                    return false;
                }

                internal bool IsZoneDisplayedInMap(int zoneID)
                {
                    var zonesLayer = _browser.WaitForElement(By.Id("ZONES_layer"), "zones layer");

                    var zone = zonesLayer.WaitForElements(By.TagName("path"), "");

                    if (zone != null)
                        return true;

                    return false;
                }

                internal void DeleteZone(string zoneID)
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "zones list");
                    var zones = zoneList.WaitForElements(By.ClassName("OffenderLocation-ZoneNameWrapper"), "zones");
                    zones[0].MoveCursorToElement();
                    var deleteBtn = zoneList.WaitForElements(By.CssSelector(".icon.delete"), "delete button");
                    deleteBtn[0].WaitToBeClickable();
                    deleteBtn[0].Click();

                    new DeleteZonePopupPage(_browser).OK();
                }
            }

            //*** Location tab base ***
            public class LocationSecondaryTabBase : WOMPage, IWOMTab
            {
                private readonly BrowserElement _container;

                protected override string mainMenudropDownListClassName => "OffenderLocation-TrailExpMenu";

                public LocationSecondaryTabBase(Browser browser) : base(browser)
                {
                    _container = _browser.WaitForElement(ContainerLocator, "Location panel");
                }

                By ContainerLocator => By.Id("left_panel");

                protected BrowserElement ContentContainer => _browser.WaitForElement(By.ClassName("OffenderLocation-LeftPanelBody"), "Location container");


                public string GetTabsMenuIdString()
                {
                    throw new NotImplementedException();
                }

                public void TrailTab()
                {
                    var trailTab = _container.WaitForElement(By.Id("trail_tab_header"), "Trail tab button");
                    GotoTabInternal(trailTab);
                }

                public void ZoneTab()
                {
                    var zoneTab = _container.WaitForElement(By.Id("zones_tab_header"), "Zone tab button");
                    GotoTabInternal(zoneTab);
                }

                private void GotoTabInternal(BrowserElement tab)
                {
                    GoToTab(tab, ContainerLocator);
                    Wait.Until(() => _container.Text != "");
                }
            }

            internal class MapAreaPage
            {
                private readonly Browser _browser;
                private readonly BrowserElement _container;
                public MapAreaPage(Browser browser)
                {
                    _browser = browser;
                    _container = _browser.WaitForElement(By.ClassName("rightPanelMap"), "Google map container");
                }


                private List<BrowserElement> LastTrailPoints => _container.WaitForElements(By.CssSelector("g[id^='TRAIL_LAST_POINT']"), "Trail last point on map", minElementsToGet: 0, explicitWaitForSingleElement: 2);

                public bool IsTrailLastPointOnMapExists()
                {
                    var lastPoints = LastTrailPoints;
                    if (lastPoints.Any())
                        return lastPoints.All(x => x.Displayed);

                    return false;
                }

                //internal void AddPolygonZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule)
                //{
                //    var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");

                //    var btnAddPolygonZone = _browser.WaitForElement(By.Id("add_polygon_from_toolbar"), "Add polygon zone");
                //    btnAddPolygonZone.Click();

                //    slider.DrawPolygon(new Point[] { new Point(100, 100), new Point(50, 50), new Point(50, 10) });

                //    new AddNewCircularZonePopupPage(_browser).AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule);
                //}

                //internal void AddCircularZone(string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
                //{
                //    var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");
                //    var btnAddCircularZone = _browser.WaitForElement(By.Id("add_circular_from_toolbar"), "Add circular zone");
                //    btnAddCircularZone.WaitToBeClickable();
                //    btnAddCircularZone.Click();

                //    slider.DragAndDropFromToOffset(100, 100);

                //    new AddNewCircularZonePopupPage(_browser).AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
                //}

                internal void OpenPointInformationTooltip()
                {
                    if (LastTrailPoints.Count < 1)
                        throw new Exception("LastTrailPoints Array Is Empty");

                    var point = LastTrailPoints[0];
                    var informationBtn = _browser.WaitForElement(By.Id("identify"), "Information button");
                    informationBtn.Click();
                    point.WaitToBeClickable();
                    point.Click();
                }

                //internal bool IsZoneExist()
                //{
                //    var zoneList = _container.WaitForElement(By.Id("zones_list_template"), "");
                //    var zones = zoneList.WaitForElements(By.CssSelector(".zone.selected"), "", minElementsToGet: 0);
                //    if (zones.Any())
                //        return true;
                //    return false;
                //}

                internal void PlayTrail()
                {
                    Wait.Until(() => LastTrailPoints.Any() && LastTrailPoints.All(x => x.Displayed), "Last trail point is not displayed as expected", 60);

                    var playButton = _container.WaitForElement(By.CssSelector(".section.controls"), "Play trail button");

                    playButton.Click();

                    LastTrailPoints[0].WaitToDisappear();
                    if (LastTrailPoints.Count > 1)
                        LastTrailPoints[1].WaitToDisappear();

                    Wait.Until(() => LastTrailPoints.All(x => x.Displayed), "Trail playing is over 180 seconds", 180);
                }
            }
        }
    }
}
