﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages.AddNewOffenderPages
{
    public class AddOffenderWarningPopupPage : PopupPageBase
    {
        public AddOffenderWarningPopupPage(Browser browser) : base(browser)
        {
        }

        public void FillForm()
        {
            var txtBox = _container.WaitForElement(By.Id("warningCommentText"), "Warning text box");

            txtBox.Text = "Automation" + DateTime.Now;

           var buttons = _container.WaitForElements(By.TagName("button"), "Button");

            buttons.First().Click();

            _container.WaitToDisappear();
        }
    }
}
