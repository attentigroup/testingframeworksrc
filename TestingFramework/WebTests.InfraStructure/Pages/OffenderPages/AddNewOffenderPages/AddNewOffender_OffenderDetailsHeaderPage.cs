﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class OffenderDetailsHeaderPage : WOMPage
        {
            BrowserElement _container;
            public OffenderDetailsHeaderPage(Browser browser) : base(browser)
            {
                _container = _browser.WaitForElement(By.Id("OffenderMasterTemplate_details0"), "Offender Master Template Container");
                Wait.Until(() => _container.Text != "");
            }


            public string GetOffenderDetailsLabel()
            {
                var label = _browser.WaitForElement(By.CssSelector(".EntityMaster-EntityHeader-DetailsBlock.ellipsis.right"), "Header Details Label");
                return label.Text;
            }

            public string GetOffenderStatus()
            {
                return _browser.WaitForElement(By.CssSelector(".EntityMaster-DetailsCard .wrapper>div>div>span"), "Offender Status").WaitGetAttribute("title");
            }

            public void SwitchToVictimOrAggressor(EnmProgramConcept programConcept)
            {
                var OffenderConceptLine = _container.WaitForElements(By.CssSelector(".wrapper>.EntityMaster-EntityHeader-DetailsBlock"), "Ofender header details", minElementsToGet: 0);
                if (OffenderConceptLine.Last().GetAttribute("outerHTML").Contains(programConcept.ToString()))
                {
                    var offenderLink = OffenderConceptLine.Last().WaitForElement(By.CssSelector(".button.link"), "Offender link");
                    offenderLink.Click();
                }
            }

            internal bool isPictureDisplayed()
            {
                var a = _browser.WaitForElement(By.Id("smallPhoto"), "Offender picture");

                return a.Displayed;
            }

            internal bool VerifyDVLink()
            {
                BrowserElement AggButton;
                var EntityHeader = _browser.WaitForElements(By.CssSelector(".EntityMaster-EntityHeader-DetailsBlock.ellipsis"), "DV Link");
                var EntityDetails = EntityHeader[1].Text;

                if (EntityDetails.Contains("Aggressor"))
                {
                    AggButton = EntityHeader[1].WaitForElement(By.CssSelector(".button.link"), "Aggressor Button");
                    AggButton.WaitToBeClickable();
                    AggButton.Click();
                }

                EntityHeader = _browser.WaitForElements(By.CssSelector(".EntityMaster-EntityHeader-DetailsBlock.ellipsis"), "DV Link");
                EntityDetails = EntityHeader[1].Text;

                if (EntityDetails.Contains("Victim"))
                {
                    return true;
                }

                return false;
            }
        }
    }
}
