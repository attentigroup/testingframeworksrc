﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages.AddNewOffenderPages
{
    public class MapToolTip
    {
        private readonly Browser _browser;
        private BrowserElement _container => _browser?.WaitForElement(By.Id("mapToolTipContainer"), "Map/Zone tool tip");

        public MapToolTip(Browser browser)
        {
            _browser = browser;

            var container = _container;
        }
    }
}
