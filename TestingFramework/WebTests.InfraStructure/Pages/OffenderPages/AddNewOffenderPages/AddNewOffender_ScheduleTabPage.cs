﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.MonitorPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class ScheduleTabPage : AddOffenderTabBasePage
        {
            public ScheduleTabPage(Browser browser) : base(browser)
            {
                ScheduleTab();
                var scheduleContainer = _browser.WaitForElement(By.Id("schedule_main"), "");
                Wait.Until(() => scheduleContainer.Displayed);
            }
            ScheduleGeneralActionsMenuPage ScheduleGeneralActionsButtonsMenu => new ScheduleGeneralActionsMenuPage(_browser);

            internal void AddSchedule(TimeFrame timeFrame)
            {
                if (!string.IsNullOrEmpty(timeFrame.Limitation) && timeFrame.Limitation.Equals("Exclusion"))
                {
                    NavigateToExclusionSchedule();
                }

                var AddTimeframePage = PressAddTimeFrame();
                AddTimeframePage.AddTimeFrame(timeFrame);
                var savePopup = ScheduleGeneralActionsButtonsMenu.Save();
                savePopup.LogAndSave();

            }

            public void UpdateTimeFrame(TimeFrame timeFrameObj, string timeFrameID, string ProgramType)
            {
                if (!string.IsNullOrEmpty(timeFrameObj.Limitation) && timeFrameObj.Limitation.Equals("Exclusion"))
                {
                    NavigateToExclusionSchedule();
                }

                var timeFrame = _browser.WaitForElement(By.ClassName("timeFrame" + timeFrameID), $"TimeFrame: {timeFrameID}");
                timeFrame.MoveCursorToElement(true);

                var webDriver = _browser.GetWebDriver();

                IJavaScriptExecutor jse = webDriver as IJavaScriptExecutor;

                var contentTableElement = _browser.WaitForElement(By.Id("contentTable"), "Content table");
                var elem = contentTableElement.GetWebElement();

                for (int i = 0; i < 500; i += 100)
                {
                    jse.ExecuteScript($"arguments[0].scrollTo({i},0);", elem);

                    timeFrame.DoubleClick();
                    var popup = _browser.WaitForElement(By.Id("PopupContainer"), "Popup");

                    if (popup.Displayed)
                        break;
                }

                var addTimeframePopup = new AddTimeFramePopupPage(_browser);
                addTimeframePopup.UpdateTimeFrame(timeFrameObj);

                if (ProgramType.Contains("E4") && timeFrameObj.RecurseEveryWeek.Equals("True"))
                {
                    var updatePopup = _browser.WaitForElement(By.Id("PopupContainer"), "Edit cellular data popup");
                    Wait.Until(() => updatePopup.Displayed);
                    var buttons = updatePopup.WaitForElements(By.TagName("button"), "");
                    var updateButton = buttons.First(x => x.Text.Equals("Update"));
                    updateButton.Click();
                }

                SaveScheduleChanges();

            }

            public void DeleteTimeFrame(string timeframeID, bool isE4WeeklyTimeframe = false)
            {
                var webDriver = _browser.GetWebDriver();
                IJavaScriptExecutor jse = webDriver as IJavaScriptExecutor;
                //jse.ExecuteScript("document.body.style.zoom = '67%'");

                var contentTableElement = _browser.WaitForElement(By.Id("contentTable"), "Content table");
                var elem = contentTableElement.GetWebElement();

                 jse.ExecuteScript($"arguments[0].scrollTo(0,0);", elem);

                var timeFrame = _browser.WaitForElement(By.CssSelector($".timeFrame{timeframeID}>div.Schedule-TimeFrames-TimeFrame"), $"TimeFrame: {timeframeID}");
                Wait.Until(() => timeFrame.Displayed);
                timeFrame.MoveCursorToElement(true);

                var tooltipContainer = _browser.WaitForElement(By.ClassName("myScheduleToolTip"), "Schedule tooltip container");
                var tooltip = _browser.WaitForElement(By.ClassName("tootltipDataWrap"), "Schedule tooltip");
                Wait.Until(() => tooltip.Displayed);

                var buttons = tooltipContainer.WaitForElements(By.TagName("button"), "");
                buttons[1].Click();
                
                var popup = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "Confirm popup");
                Wait.Until(() => popup.Displayed);

                var okButton = popup.WaitForElements(By.TagName("button"), "OkButton button");
                okButton[0].Click();

                if (isE4WeeklyTimeframe)
                {
                    var deletePopup = _browser.WaitForElement(By.Id("PopupContainer"), "");
                    Wait.Until(() => deletePopup.Displayed);

                    var deleteButtons = deletePopup.WaitForElements(By.TagName("button"), "");
                    var deleteButton = deleteButtons.First(x => x.Text.Equals("Delete"));
                    deleteButton.Click();

                }

                SaveScheduleChanges();

            }

            private void SaveScheduleChanges()
            {
                var saveSchedule = _browser.WaitForElement(By.CssSelector(".button.button.square.save"), "Save schedule");
                saveSchedule.WaitToBeClickable();
                saveSchedule.Click();

                PressSkipLogButton();
            }

            private void NavigateToExclusionSchedule()
            {
                var inclusionNavigationBtn = _browser.WaitForElement(By.CssSelector("div.btn.inclusion"), "Inclusion button");
                inclusionNavigationBtn.Click();
            }

            private AddTimeFramePopupPage PressAddTimeFrame()
            {
                var addTimeFrameBtn = _browser.WaitForElement(By.Id("add_time_frame"), "Add timeframe button");
                addTimeFrameBtn.Click();
                return new AddTimeFramePopupPage(_browser);
            }

            internal bool IsTimeframeExist()
            {
                var schedule = _browser.WaitForElement(By.ClassName("Schedule-TimeFrames"), "");
                var timeframes = schedule.WaitForElements(By.ClassName("Schedule-TimeFrames-TimeFrame"), "", minElementsToGet: 0);
                if (timeframes.Any())
                    return true;
                return false;
            }

            internal bool IsGroupTimeframeExist(string timeFrameName)
            {
                var container = _browser.WaitForElements(By.Id("columnsTemplate"), "Schedule Table");
                foreach (BrowserElement be in container)
                {
                    if (be.Text.Contains(timeFrameName))
                    {
                        return true;
                    }
                }
                return false;
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("schedule_main");
            }

            internal class ScheduleGeneralActionsMenuPage : AddNewOffenderGeneralActionsMenuPage
            {
                public ScheduleGeneralActionsMenuPage(Browser browser) : base(browser)
                {

                }

                protected override By GetSaveButtonSelector()
                {
                    return By.CssSelector("#generalSaveBtn>button");
                }

                public new SaveSchedulePopup Save()
                {
                    var btnSave = _container.WaitForElement(GetSaveButtonSelector(), "Save button");
                    btnSave.WaitToBeClickable();
                    btnSave.ClickSafely();
                    return new SaveSchedulePopup(_browser);
                }

                public void SaveSchedule()
                {
                    var btnSave = _container.WaitForElement(GetSaveButtonSelector(), "Save button");
                    btnSave.Click();
                }
            }

            internal void ShowScheduleTooltip()
            {
                var toTime = DateTime.Now.AddSeconds(9);
                var timeFrameBox = _browser.WaitForElements(By.CssSelector(".Schedule-TimeFrames-TimeFrame"), "Schedule-TimeFrames-TimeFrame", 3, minElementsToGet:0);
                if (!timeFrameBox.Any() && DateTime.Now < toTime)
                {
                    GotoNextWeek();
                    ShowScheduleTooltip();
                }
                else
                {
                    timeFrameBox.First().MoveCursorToElement();
                    timeFrameBox.First().MoveCursorToElement();

                    var tooltip = _browser.WaitForElement(By.ClassName("myScheduleToolTip"), "Schedule tooltip");

                    Wait.Until(() => tooltip.Displayed);
                }
            }

            public void GotoNextWeek()
            {
                var btn = _browser.WaitForElement(By.Id("next_week_btn"), "Next week button");

                btn.Click();
            }

            public bool IsTimeframeDisplayedInSchedule(string timeframeID)
            {
                var timeFrame = _browser.WaitForElements(By.ClassName("timeFrame" + timeframeID), $"TimeFrame: {timeframeID}", minElementsToGet: 0);
                if (timeFrame != null)
                    return true;
                return false;
            }

            internal void DeleteAllWeeklyTimeframes()
            {
                var deleteMenu = _browser.WaitForElement(By.CssSelector(".btn.delete_all"), "Delete Schedule Dropdown button");
                deleteMenu.Click();
                var deleteWeeklyScheduleButton = _browser.WaitForElement(By.Id("delete_weekly_btn"), "Delete Weekly Schedule button");
                deleteWeeklyScheduleButton.ClickSafely();

                var deleteRecurringTimeframePopup = new DeleteRecurringTimeframesPopupPage(_browser);
                deleteRecurringTimeframePopup.OK();

                PressSkipLogButton();
            }

            private void PressSkipLogButton()
            {
                var skipLogBtn = _browser.WaitForElement(By.Id("skip_log_btn"), "Skip log button");
                skipLogBtn.Click();
            }
        }
    }
}
