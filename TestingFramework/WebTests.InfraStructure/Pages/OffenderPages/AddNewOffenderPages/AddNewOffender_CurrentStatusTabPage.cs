﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.MonitorPages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;
using static WebTests.InfraStructure.Pages.MonitorPages.MonitorPage;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public partial class AddNewOffenderPage : WOMPage
    {
        public class CurrentStatusTabPage : AddOffenderTabBasePage
        {

            internal CurrentStatusTablePage CurrentStatusTable => new CurrentStatusTablePage(_browser);
            internal MonitorFilterSectionPage MonitorFilterSection => new MonitorFilterSectionPage(_browser);
            internal CurrentStatusFilterSectionPage CurrentStatusFilterSection => new CurrentStatusFilterSectionPage(_browser);
            internal CurrentStatusPagingPage CurrentStatusPaging => new CurrentStatusPagingPage(_browser);

            protected override BrowserElement OffenderPageHeader => _browser.WaitForElement(By.Id("ApplicationBody"), "Offender master page header");

            public CurrentStatusTabPage(Browser browser) : base(browser)
            {
                CurrentStatusTab();
                WaitForContentToLoad();
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("divBodyContainer");
            }

            public CurrentStatusTableRow GetEventDetails(string eventId)
            {
                return GetEventDetailsFromTable(eventId);

            }

            private CurrentStatusTableRow GetEventDetailsFromTable(string eventId)
            {
                ClickOnCurrentStatusFilters(1);
                var GetEventsDetails = CurrentStatusTable.GetCurrentStatusTableRowByEventId(eventId);
                return GetEventsDetails;
            }

            public void FilterEventsByPeriod(string recent)
            {
                SelectCurrentStatusFilterElement(1, "All");
                CurrentStatusTable.FilterEventsByPeriod(recent);
            }
            public void FilterEventByRangeOfDates(DateTime startTime, DateTime endTime)
            {
                SelectCurrentStatusFilterElement(1, "All");
                CurrentStatusTable.FilterEventByRangeOfDates(startTime, endTime);
            }

            private void SelectCurrentStatusFilterElement(int sectionNumber, string title)
            {
                var filterRowElement = CurrentStatusFilterSection.GetFilterRowByTitle(sectionNumber, title);

                filterRowElement.WaitToBeClickable();

                filterRowElement.ClickSafely();

                CurrentStatusTable.GetContentContainerElement();
                CurrentStatusTable.WaitForContent();
            }
            public void ClickOnCurrentStatusFilters(int sectionNumber, string filterTitle = "All")
            {
                MonitorFilterSection.SelectFilterByTitle(sectionNumber, filterTitle);
            }

            internal List<ValueTuple<int, int>> GetShownRows()
            {
                return GetShownResultsPerPage(100);
            }

            internal bool ClickOnPagerButtons()
            {
                return PagingAllPages();
            }

            internal void CreateEventTimeUserFilter(DateTime eventTime)
            {
                CurrentStatusFilterSection.CreateEventTimeUserFilter(eventTime);
            }

            internal void CreateSeverityUserFilter(string severity)
            {
                CurrentStatusTable.GetContentContainerElement();
                CurrentStatusTable.WaitForContent();
                CurrentStatusFilterSection.CreateSeverityUserFilter(severity);
            }

            internal void CreateEventIDUserFilter(string eventID)
            {
                CurrentStatusFilterSection.CreateEventIDUserFilter(eventID);
            }

            internal void CreateEventMessageUserFilter(string eventMessage)
            {
                CurrentStatusFilterSection.CreateEventMessageUserFilter(eventMessage);
            }

            internal void DeleteUserFilter(string filterName)
            {
                var filterRowElement = CurrentStatusFilterSection.GetFilterRowByTitle(2, filterName);
                filterRowElement.MoveCursorToElement();
                var deleteBtn = filterRowElement.WaitForElement(By.CssSelector(".icon.delete"), "Delete button");
                deleteBtn.WaitToBeClickable();
                deleteBtn.Click();
                new DeleteFilterPopupPage(_browser).Delete();
            }

            internal CurrentStatusTableRow[] GetEventsDetailsFromSpecificSystemFilter(int sectionNumber, string title)
            {
                var filterRowElement = CurrentStatusFilterSection.GetFilterRowByTitle(sectionNumber, title);
                filterRowElement.Click();
                return CurrentStatusTable.GetCurrentStatusRowsDetailsArr(1);
            }

            internal int GetFilterCounter(string filterTitle)
            {

                var currentFilters = CurrentStatusFilterSection.GetFilterCounter(filterTitle);

                CurrentStatusTable.FilterByPeriod("max");

                WaitForContentToLoad();

                Wait.Until(() => CurrentStatusFilterSection.GetFilterCounter(filterTitle) != currentFilters, seconds: 30, failIfConditionNotMet: false);

                return CurrentStatusFilterSection.GetFilterCounter(filterTitle);
            }

            //*** internal classess ***

            internal class CurrentStatusTablePage : WOMTableBase
            {
                public CurrentStatusTablePage(Browser browser) : base(browser)
                {

                }

                CurrentStatusPagingPage CurrentStatusPaging => new CurrentStatusPagingPage(_browser);
                CurrentStatusFilterSectionPage CurrentStatusFilterSection => new CurrentStatusFilterSectionPage(_browser);

                public void WaitForContent()
                {
                    WaitForContentToLoad();
                }

                protected override By GetContentContainerLocator()
                {
                    return By.ClassName("center_content");
                }

                public void FilterEventByRangeOfDates(DateTime startTime, DateTime endTime)
                {
                    FilterByRangeOfDates(startTime, endTime);
                    WaitForContent();

                }

                internal void FilterEventsByPeriod(string recent)
                {
                    FilterByPeriod(recent);
                    WaitForContent();

                }

                public CurrentStatusTableRow GetCurrentStatusTableRowByEventId(string eventIdSearch)
                {
                    Search(eventIdSearch);

                    Wait.Until(() => GetTableRowsElements().Count() == 1 && GetTableRowsElements().First().Displayed);

                    var tableRow = GetTableRowElementByNumber(1);

                    return GetCurrentStatusTableleRow(tableRow);
                }

                private CurrentStatusTableRow GetCurrentStatusTableleRow(BrowserElement tableRow)
                {
                    var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                    Wait.Until(() => eventIdElement.Text != string.Empty);

                    var eventId = eventIdElement.Text;
                    var eventTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_EventTime'"), "DateTime").Text;
                    var message = tableRow.WaitForElement(By.CssSelector(".message>span"), "Message").Text;
                    var updateTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_UpdateTime']"), "Update time").Text;
                    var status = tableRow.WaitForElement(By.CssSelector(".list_grid_icon>span"), "").WaitGetAttribute("title");

                    var currentStatusTableleRow = new CurrentStatusTableRow()
                    {
                        EventId = eventId,
                        EventTime = eventTime,
                        Message = message,
                        UpdateTime = updateTime,
                        Status = status

                    };

                    return currentStatusTableleRow;
                }

                private CurrentStatusTableRow GetCurrentMeassageTableleRow(BrowserElement tableRow)
                {
                    var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                    Wait.Until(() => eventIdElement.Text != string.Empty);

                    var eventId = eventIdElement.Text;
                    var eventTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_EventTime'"), "DateTime").Text;
                    var message = tableRow.WaitForElement(By.CssSelector(".message>span"), "Message").Text;

                    var currentStatusTableleRow = new CurrentStatusTableRow()
                    {
                        EventId = eventId,
                        EventTime = eventTime,
                        Message = message,

                    };

                    return currentStatusTableleRow;
                }

                internal string[] GetFilterEventsArray()
                {
                    string[] datesArr = new string[2];
                    var tableRow = GetTableRowElementByNumber(1);

                    var sortButton = _browser.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();

                    tableRow = GetTableRowElementByNumber(1);
                    var monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    var firstRowTimeStr = monitorTableRow.EventTime;
                    datesArr[0] = firstRowTimeStr;

                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("0"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();
                    tableRow = GetTableRowElementByNumber(1);
                    monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    firstRowTimeStr = monitorTableRow.EventTime;
                    datesArr[1] = firstRowTimeStr;

                    return datesArr;

                }

                public string GetEventStatus(string eventId)
                {
                    Search(eventId);
                    var tableRow = GetTableRowElementByNumber(1);
                    var currentStatusTableRow = GetCurrentStatusTableleRow(tableRow);
                    var status = currentStatusTableRow.Status;
                    return status;
                }

                public string GetEventMessage(string eventMessage)
                {

                    Search(eventMessage);
                    var tableRow = GetTableRowElementByNumber(1);
                    var currentStatusTableRow = GetCurrentMeassageTableleRow(tableRow);
                    var Message = currentStatusTableRow.Message;
                    return Message;

                }

                private CurrentStatusTableRow GetCurrentStatusTableRow(BrowserElement tableRow)
                {
                    var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                    Wait.Until(() => eventIdElement.Text != string.Empty);

                    var eventId = eventIdElement.Text;
                    var eventTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_EventTime'"), "DateTime").Text;
                    var message = tableRow.WaitForElement(By.CssSelector(".message>span"), "Message").Text;
                    var updateTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_UpdateTime']"), "Update time").Text;
                    var status = tableRow.WaitForElement(By.CssSelector(".list_grid_icon>span"), "status").GetAttribute("title");
                    var severity = tableRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.center.severityStatus>div"), "Severity").GetAttribute("title");


                    var currentStatusTableRow = new CurrentStatusTableRow()
                    {
                        EventId = eventId,
                        EventTime = eventTime,
                        Message = message,
                        UpdateTime = updateTime,
                        Status = status,
                        Severity = severity
                    };

                    return currentStatusTableRow;
                }


                internal CurrentStatusTableRow GetManualEvent()
                {
                    var tableRow = GetTableRowElementByNumber(1);
                    var currentStatusTableRow = GetCurrentStatusTableRow(tableRow);
                    DateTime time = DateTime.ParseExact(currentStatusTableRow.EventTime, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    if (time >= DateTime.Now.AddMinutes(-5) && time <= DateTime.Now && currentStatusTableRow.Message.StartsWith("Violation LVL 1"))
                        return currentStatusTableRow;

                    var sortButton = _browser.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();

                    tableRow = GetTableRowElementByNumber(1);

                    currentStatusTableRow = GetCurrentStatusTableRow(tableRow);
                    var firstRowTimeStr = currentStatusTableRow.EventTime;
                    DateTime firstRowTime = DateTime.ParseExact(firstRowTimeStr, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                    Wait.Until(() => IsManualEventExist(firstRowTime) == true);

                    return GetCurrentStatusRow();

                }

                
                internal string GetFirstEventMessage()
                {
                    return GetCurrentStatusRow().Message;
                }


                public bool IsManualEventExist(DateTime previousRowTime)
                {
                    CurrentStatusTableRow currentStatusTableRow = GetCurrentStatusRow();
                    var RowTimeStr = currentStatusTableRow.EventTime;
                    DateTime RowTime = DateTime.ParseExact(RowTimeStr, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                    var receiver = currentStatusTableRow.ReceiverSN;
                    var message = currentStatusTableRow.Message;
                    if ((RowTime > previousRowTime) && currentStatusTableRow.Message.StartsWith("Violation LVL 1"))
                        return true;
                    return false;
                }

                private CurrentStatusTableRow GetCurrentStatusRow()
                {
                    CurrentStatusTableRow monitorRow;
                    var Row = GetTableRowElementByNumber(1);

                    try
                    {
                        monitorRow = GetCurrentStatusTableRow(Row);
                    }
                    catch (StaleElementReferenceException)
                    {
                        return GetCurrentStatusRow();
                    }

                    return monitorRow;
                }

                internal void HandleEvent(string eventId, string handlingOption)
                {
                    FilterByPeriod("max");

                    Search(eventId);

                    ExpandRow(1);
                    SelectFromDropdownList(GetExpandedRowContentContainer(), By.CssSelector(".button.action.red"), "Handle", containerLocator: By.ClassName("dropdownNavi"));
                    new RemarkPopupPage(_browser).Remark();
                }

                protected override By GetExpandedRowContentContainerLocator()
                {
                    return By.CssSelector(".general-table-body-additionalDataExpand._additionalDataExpand.additionalData_0.body.p_event_expand");
                }

                internal CurrentStatusTableRow[] GetCurrentStatusRowsArray()
                {
                    CurrentStatusTableRow[] eventsArr = new CurrentStatusTableRow[2];
                    var tableRow = GetTableRowElementByNumber(1);

                    var sortButton = _browser.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();

                    tableRow = GetTableRowElementByNumber(1);
                    var monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    eventsArr[0] = monitorTableRow;

                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("0"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();
                    tableRow = GetTableRowElementByNumber(1);
                    monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    eventsArr[1] = monitorTableRow;

                    return eventsArr;
                }

                internal CurrentStatusTableRow[] GetCurrentStatusRowsDetailsArr(int arrLength)
                {
                    
                    var rows = GetTableRowsElements();
                    if (rows.Count() <= arrLength)
                        arrLength = rows.Count();

                    CurrentStatusTableRow[] eventsArr = new CurrentStatusTableRow[arrLength];

                    for (int i = 0; i < arrLength; i++)
                    {
                        var tableRow = GetTableRowElementByNumber(i*2+1);
                        var monitorTableRow = GetCurrentStatusTableRow(tableRow);
                        eventsArr[i] = monitorTableRow;
                    }

                    return eventsArr;
                }

                protected override string GetTableRowCssSelectorString()
                {
                    return "#tableContainer .row";
                }

                internal void ExpandEventById(int eventId, string filter = "All")
                {
                    FilterByPeriod("max");
                    CurrentStatusFilterSection.SelectFilterByTitle(1, filter);
                    Search(eventId);
                    ExpandRow(1);
                }
            }

            internal class CurrentStatusFilterSectionPage : FilterSectionBase
            {
                public CurrentStatusFilterSectionPage(Browser browser) : base(browser)
                {

                }

                internal void CreateEventIDUserFilter(string eventID)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateEventIDFilter(eventID);
                }

                internal void CreateEventMessageUserFilter(string eventMessage)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateEventMessageUserFilter(eventMessage);
                }

                internal void CreateEventTimeUserFilter(DateTime eventTime)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateEventTimeFilter(eventTime);
                }

                internal void CreateSeverityUserFilter(string severity)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateSeverityFilter(severity);
                }

                private void PressAddFilter()
                {
                    var addFilterBtn = _browser.WaitForElement(By.CssSelector(".icons.button.link"), "Add filter button");
                    addFilterBtn.WaitToBeClickable();
                    addFilterBtn.ClickSafely();
                }

                internal string[] GetSystemFiltersCurrentStatus()
                {
                    return GetSystemFilters();
                }
            }

            internal string[] GetSystemFilters()
            {
                return CurrentStatusFilterSection.GetSystemFiltersCurrentStatus();
            }

            internal string GetAmountOfRowsForSpecificSystemFilter(int sectionNumber, string filterTitle)
            {
                var filterRowElement = CurrentStatusFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);
                filterRowElement.Click();
                var filterCounterElement = filterRowElement.WaitForElements(By.CssSelector(".counter.ellipsis>span"), "filter counter");
                var counter = filterCounterElement[1].GetAttribute("title");
                return counter;
            }

            internal class CurrentStatusPagingPage : WOMTablePagingPage
            {
                public CurrentStatusPagingPage(Browser browser) : base(browser)
                {

                }
            }            
        }
    }
}
