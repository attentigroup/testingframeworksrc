﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;
using WebTests.Common;

namespace WebTests.InfraStructure.Pages
{
    public class UploadAlertPopupPage
    {
        private readonly Browser _browser;
        private readonly BrowserElement _container;

        public UploadAlertPopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "Upload alert popup");
            Wait.Until(() => _container.Displayed);
        }

        public void PressOK()
        {
            var btnOK = _container.WaitForElements(By.ClassName("buttons"), "OK button")[0];
            btnOK.WaitToBeClickable();
            btnOK.Click();
        }
    }
}
