﻿using OpenQA.Selenium;
using System.Linq;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;
using System;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.InfraStructure.Pages.OffenderPages;
using System.Collections.Generic;
using WebTests.InfraStructure.Pages.MonitorPages;
using WebTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using System.Threading;

namespace WebTests.InfraStructure.Pages
{
    public class OffendersListPage : WOMPage
    {
        public OffendersListPage(Browser browser) : base(browser)
        {
            new OffenderCounterPage(_browser).CloseCounter();
        }

        internal OffendersListTablePage OffendersListTable => new OffendersListTablePage(_browser);
        internal OffenderListFilterSectionPage OffenderListFilterSection => new OffenderListFilterSectionPage(_browser);
        internal OffendersListGeneralActionsMenuPage OffendersListGeneralActionsMenu => new OffendersListGeneralActionsMenuPage(_browser);

        public void SelectFirstRow()
        {
            OffendersListTable.SelectFirstRow();
        }

        //public void SelectFirstRow()
        //{
        //    OffendersListTable.Get();
        //}

        public void SelectOffenderBySearchTerm(string searchTerm, string filterTitle = "All")
        {
            OffenderListFilterSection.SelectFilterByTitle(1, filterTitle);
            OffendersListTable.SelectOffenderBySearchTerm(searchTerm);
        }

        public void SearchOffenderWithoutSelecting(string searchTerm, string filterTitle = "All")
        {
            OffenderListFilterSection.SelectFilterByTitle(1, filterTitle);
            OffendersListTable.SearchOffender(searchTerm);
        }

        public bool IsFilterSectionVisible()
        {
            return new OffenderListFilterSectionPage(_browser).IsSectionVisible();
        }

        internal bool IsCustomFieldDisplay(string resourceID)
        {
            return OffendersListTable.IsCustomFieldDisplay(resourceID);
        }

        internal bool IsCustomFieldDataDisplayed(string resourceID, string searchTerm)
        {
            return OffendersListTable.IsCustomFieldDataDisplayed(resourceID, searchTerm);
        }

        internal OffenderDetails[] GetOffenderDetailsRows()
        {
            return OffendersListTable.GetRowsOffendersDetails();
        }

        internal OffenderDetails GetOffenderDetails(string refId)
        {
            return GetOffenderDetailsFromTable(refId);
        }

        internal void CreateFilterByEndOfProgramForActiveOffender(DateTime time)
        {
            OffenderListFilterSection.CreateFilterByEndOfProgramForActiveOffender(time);
        }

        /// <summary>
        /// Use filterSectionNumber = -1 to ignore filter sections and use search from all filter panel
        /// </summary>
        /// <param name="sectionNumber"></param>
        /// <param name="title"></param>
        public void ClickOnOffenderFilters(int sectionNumber, string filterTitle = "All")
        {
            //TODO: remove duplication. Use OffenderListFilterSectio.SelectFilterByTitle instead
            var filterRowElement = OffenderListFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);

            filterRowElement.WaitToBeClickable();

            filterRowElement.ClickSafely();

            OffendersListTable.WaitForContent();
        }

        public string GetPagerAmountOfRows()
        {
            return OffendersListTable.GetPagerAmountOfRows();
        }

        public OffenderDetails GetOffenderByOffenderRefId(string offenderRefId)
        {
            return OffendersListTable.GetOffenderDetailsByRefId(offenderRefId);
        }

        public string GetAmountOfRowsByFilter(int sectionNumber, string filterTitle)
        {
            ClickOnOffenderFilters(sectionNumber, filterTitle);
            return OffendersListTable.GetPagerAmountOfRows();
        }

        internal SecurityConfigObject Navigate2Section(string section, string aPIString)
        {
            BrowserElement tableHeader;
            SecurityConfigObject result = null;
            string[] patterns = new string[]{
            @".*._(LastConnection$|SanityCycle$|SanitySensitivity$|TimePassed$)\z",  //case 0
            @".*._(LastPoint$|ViolationDescription$|ViolationLed$|OffenderFullName$|Legend$|BatteryStatus$)\z",//case 1
            @".*._(ReceiverViolationLed$|ScheduleViolationLed$|TxViolationLed$|ZoneViolationLed$)\z", //case 2
            @".*._(MultipleDownload$|Button_DownloadAll$|ExportToExcel$|Refresh$)\z",    //case 3
            @".*._(ReceiverSerialNumber$)\z"  //case 4
            };

            for (int i = 0; i < patterns.Length; i++)
            {
                var match = FindMatch(aPIString, patterns[i]);
                if (match.Success)
                {
                    tableHeader = OffendersListTable.GetTableHeaderElement();
                    switch (i)
                    {
                        case 0:
                            OffenderListFilterSection.SelectFilterByTitle(1, "Missed Call");
                            break;

                        case 1:
                            OffenderListFilterSection.SelectFilterByTitle(1, "Offender Summary");
                            break;

                        case 2:
                            OffenderListFilterSection.SelectFilterByTitle(1, "Open Violation");
                            break;

                        case 3:
                            OffenderListFilterSection.SelectFilterByTitle(1, "Download Recommended");
                            var elems = _browser.WaitForElements(By.Id("downloadAllBtn"), "DownloadAll button", explicitWaitForSingleElement: 2, minElementsToGet: 0);
                            if (elems.Any())
                                Wait.Until(() => elems.First().Displayed);
                            break;

                        case 4:
                            OffenderListFilterSection.SelectFilterByTitle(1, "Active Offenders");
                            break;

                        default:
                            throw new WebTestingException("Not all cases are implemented yet...");
                    }
                    WaitForContentToLoad();
                    result = GetSecurityForColumn(aPIString);
                }

                if (result != null)
                {
                    result.ResourceId = aPIString;
                    break;
                }
            }
            if (result == null)
            {
                OffenderListFilterSection.SelectFilterByTitle(1, "All");
                WaitForContentToLoad();
                result = GetSecurityForColumn(aPIString);
            }

            return result;
        }
        //ortal
        //public string GetFilterTitle()
        //{
        //    var titleElement = _browser.WaitForElement(By.Id("selectedFilterName"), "filter title");
        //    return titleElement.Text;
        //}

        private Match FindMatch(string apiString, string pattern)
        {
            var r = new Regex(pattern);
            return r.Match(apiString);
        }

        private void WaitForContentToLoad()
        {
            Thread.Sleep(150);
            Wait.Until(() => OffendersListTable.GetContentContainerElement().Text != string.Empty);
        }

        private SecurityConfigObject FilterPanelSecurity(string aPIString)
        {
            var filterPanelVisible = GetSecurityResult(aPIString);
            return filterPanelVisible;
        }

        private SecurityConfigObject RefreshSecurity(string aPIString)
        {
            var isBtnRefreshVisible = GetSecurityResult(aPIString);
            return isBtnRefreshVisible;
        }

        private SecurityConfigObject LegendSecurity(string aPIString)
        {
            OffenderListFilterSection.SelectFilterByTitle(1, "Offender Summary");
            return GetSecurityResult(aPIString);
        }

        private SecurityConfigObject DownloadAllSecurity(string aPIString)
        {
            OffenderListFilterSection.SelectFilterByTitle(1, "Download Recommended");
            return GetSecurityResult(aPIString);
        }

        private SecurityConfigObject GetSecurityForColumn(string aPIString)
        {
            return GetSecurityResult(aPIString);
        }

        public OffenderDetails GetOffenderDetailsFromTable(string refId)
        {
            ClickOnOffenderFilters(1);
            var GetOffenderDetails = OffendersListTable.GetOffenderDetailsByRefId(refId);
            OffendersListTable.ClearSearch();
            return GetOffenderDetails;
        }

        internal List<ValueTuple<int, int>> ShowResults(int showResultsNumber)
        {
            return OffendersListTable.GetShownResultsPerPage(showResultsNumber);
        }

        internal bool ClickOnPagerButtons()
        {
            return OffendersListTable.PagingAllPages();
        }

        public AddNewOffenderPage SearchOffender(string searchTerm, int filterSectionNumber, string filterTitle = "All", string secondarySearchTerm = null)
        {
            ClickOnOffenderFilters(filterSectionNumber, filterTitle);

            OffendersListTable.SelectOffenderBySearchTerm(searchTerm, secondarySearchTerm);
            return new AddNewOffenderPage(_browser, false);
        }

        public AddNewOffenderPage SearchOffenderByDateCreate(string searchTerm, int filterSectionNumber, DateTime? createDate, string filterTitle = "All")
        {
            ClickOnOffenderFilters(filterSectionNumber, filterTitle);

            OffendersListTable.SelectOffenderBySearchTermAndDateCreated(searchTerm, createDate);
            return new AddNewOffenderPage(_browser, false);
        }

        internal class OffendersListTablePage : WOMTableBase
        {
            public OffendersListTablePage(Browser browser) : base(browser)
            {
                WaitForContentToLoad();
            }

            protected override By GetContentContainerLocator()
            {
                return By.ClassName("main_content");
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#tableContainer .row";
            }

            public void WaitForContent()
            {
                WaitForContentToLoad();
            }

            public List<BrowserElement> GetAllRows()
            {
                return GetTableRowsElements();
            }

            public OffenderDetails GetOffenderDetailsByRefId(string refId)
            {
                Search(refId);
                var tableRow = GetTableRowElementByNumber(1);
                return GetOffenderDetails(tableRow);
            }

            public void SelectOffenderBySearchTermAndDateCreated(string searchTerm, DateTime? createDate)
            {
                var row = GetOffenderRowBySearchTermAndDateCreated(searchTerm, createDate);
                var clickableCol = GetClickableColumn(row);

                clickableCol.Click();

                clickableCol.WaitToDisappear();

                _browser.WaitForNotBusyCursor();

            }

            public void SelectOffenderBySearchTerm(string searchTerm, string secondarySearchTerm)
            {
                if (secondarySearchTerm == null)
                    SelectOffenderBySearchTerm(searchTerm);
                else
                {
                    Search(searchTerm);

                    var rows = GetTableRowsElements();

                    BrowserElement tableRow = null;

                    if (secondarySearchTerm == null)
                    {
                        secondarySearchTerm = "";
                    }


                    for (int i = 0; i < rows.Count; i++)
                    {
                        if (rows[i].Text.Contains(searchTerm) && rows[i].Text.Contains(secondarySearchTerm))
                        {
                            tableRow = rows[i];
                            break;
                        }
                        else
                            throw new Exception($"No line found for '{searchTerm}' with Secondary Search '{secondarySearchTerm}'");
                    }

                    //var tableRow = GetTableRowElementByIndex(1);

                    //string data = tableRow.Text;
                    var row = tableRow.WaitForElement(By.CssSelector("div[class~='offNavigate']"), "clickable column");

                    //Wait.Until(() => row.Displayed);

                    row.WaitToBeClickable();
                    row.Click();

                    //new OffendersListTablePage(_browser).WaitForContent();

                    //TODO this waiting to the wrong element!!!
                    //var container = _browser.WaitForElement(GetContentContainerLocator(), "Content container");
                    //Wait.Until(() => container.Displayed);

                    //return GetOffenderDetails(tableRow);\
                }
            }

            private BrowserElement GetOffenderRowBySearchTermAndDateCreated(string searchTerm, DateTime? dateCreated)
            {
                DateTime? _dateCreated = null;
                BrowserElement _row = null;
                Search(searchTerm);
                var trunactedDateCreated = new DateTime(
                    dateCreated.Value.Year,
                    dateCreated.Value.Month,
                    dateCreated.Value.Day,
                    dateCreated.Value.Hour,
                    dateCreated.Value.Minute,
                    0
                    );
                var rows = GetTableRowsElements();

                foreach (BrowserElement row in rows)
                {
                    var offenderDetails = GetOffenderDetails(row);
                    var isParsed = DateTime.TryParse(offenderDetails.DateCreated, out DateTime result);
                    if (isParsed)
                    {
                        if (result.CompareTo(trunactedDateCreated) == 0)
                        {
                            _dateCreated = result;
                            _row = row;
                            break;
                        }

                    }
                    else
                        throw new Exception($"Couldn't parse {offenderDetails.DateCreated}");
                }
                if (_dateCreated == null)
                    throw new Exception($"Offender row with Create Date {dateCreated} not found");

                return _row;
            }


            public void SelectOffenderBySearchTerm(string searchTerm)
            {
                Search(searchTerm);

                var tableRow = GetTableRowElementByNumber(1);

                BrowserElement rowClickableColumn = GetClickableColumn(tableRow);

                rowClickableColumn.WaitToBeClickable();

                Performance.StartPoint(PerformanceTag.GotoOffender);

                rowClickableColumn.Click();

                rowClickableColumn.WaitToDisappear();

                _browser.WaitForNotBusyCursor();

                Performance.EndPoint(PerformanceTag.GotoOffender);
            }

            internal void SearchOffender(string searchTerm)
            {
                Search(searchTerm);

                _browser.WaitForNotBusyCursor();
            }

            protected override string GetClickableColumnCssSelectorString()
            {
                return "div[class~='offNavigate']";
            }

            private OffenderDetails GetOffenderDetails(BrowserElement tableRow)
            {
                var offenderRefId = tableRow.WaitForElement(By.CssSelector(".OffendersList_List_GridHeader_OffenderRefID"), "Offender reference Id").WaitGetAttribute("title");

                var lastNameElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_LastName"), "Last name", minElementsToGet: 0);
                var lastName = lastNameElement.Any() ? lastNameElement.Single().GetAttribute("title") : "";


                var firstNameElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_FirstName"), "First name", minElementsToGet: 0);
                var firstName = firstNameElement.Any() ? firstNameElement.Single().GetAttribute("title") : "";

                var middleNameElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_MiddleName"), "Middle name", minElementsToGet: 0);
                var middleName = middleNameElement.Any() ? middleNameElement.Single().Text : "";

                var phoneNumberElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_PhoneNumbers"), "Phone number", minElementsToGet: 0);
                var phoneNumber = phoneNumberElement.Any() ? phoneNumberElement.Single().GetAttribute("title") : "";

                var programTypeElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_ProgramType"), "Program type", minElementsToGet: 0);
                var programType = programTypeElement.Any() ? programTypeElement.Single().Text : "";

                var agencyElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_Agency"), "Agency", minElementsToGet: 0);
                var agency = agencyElement.Any() ? agencyElement.Single().Text : "";

                var officerElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_Officer"), "Officed", minElementsToGet: 0);
                var officer = officerElement.Any() ? officerElement.Single().Text : "";

                var receiverElement = tableRow.WaitForElements(By.CssSelector(".OffendersList_List_GridHeader_ReceiverSerialNumber"), "Officed", minElementsToGet: 0);
                var receiver = receiverElement.Any() ? receiverElement.Single().Text : "";

                var programEnd = tableRow.WaitForElement(By.CssSelector(".contentTpl.td.general-table-body-cell.ellipsis.OffendersList_List_GridHeader_ProgramEnd>span"), "").Text;

                var dateCreated = tableRow.WaitForElement(By.CssSelector(".contentTpl.td.general-table-body-cell.ellipsis.OffendersList_List_GridHeader_DateCreated>span"), "Date created").Text;

                var status = tableRow.WaitForElement(By.CssSelector(".iconTpl.td.general-table-body-cell.ellipsis.OffendersList_List_GridHeader_OffenderStatus.offender_status.sprt_offender"), "").GetAttribute("title");

                var offenderDetails = new OffenderDetails()
                {
                    OffenderRefId = offenderRefId,
                    LastName = lastName,
                    FirstName = firstName,
                    MiddleName = middleName,
                    PhoneNumber = phoneNumber,
                    ProgramType = programType,
                    Agency = agency,
                    Officer = officer,
                    Receiver = receiver,
                    ProgramEnd = programEnd,
                    DateCreated = dateCreated,
                    Status = status
                };

                return offenderDetails;
            }

            public string GetPagerAmountOfRows()
            {
                return GetAmountOfRows();
            }

            //TODO: move to WOMTableBase
            internal void ClearSearch()
            {
                var clearBtn = _browser.WaitForElement(By.CssSelector("._clear_result_button.search-clear-result-button"), "");
                clearBtn.Click();
            }

            internal OffenderDetails[] GetRowsOffendersDetails()
            {
                OffenderDetails[] offendersDetails = new OffenderDetails[2];

                var sortButton = _browser.WaitForElement(By.CssSelector("#tblHeader>div[colname='OffendersList_List_GridHeader_ProgramEnd']>div"), "");
                sortButton.Click();
                if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                    sortButton.Click();

                var tableRow = GetTableRowElementByNumber(1);
                offendersDetails[0] = GetOffenderDetails(tableRow);

                //TODO move to general sort method
                sortButton = _browser.WaitForElement(By.CssSelector("#tblHeader>div[colname='OffendersList_List_GridHeader_ProgramEnd']>div"), "");
                sortButton.Click();
                if (!sortButton.WaitGetAttribute("sort-direction").Equals("0"))
                    sortButton.Click();

                tableRow = GetTableRowElementByNumber(1);
                offendersDetails[1] = GetOffenderDetails(tableRow);

                return offendersDetails;
            }

            internal void SelectFirstRow()
            {
                var firstRow = GetTableRowElementByNumber(1);

                var clickableColumn = GetClickableColumn(firstRow);

                clickableColumn.WaitToBeClickable();

                clickableColumn.Click();
            }

            internal bool GetCustomFieldsVisibilityFromOffendersListLeftToolbar(string resourceID)
            {
                var columnListBtn = _browser.WaitForElement(By.ClassName("general-table-ShowHide-resetShowHideIn"), "");
                columnListBtn.MoveCursorToElement();

                var columnListContainer = _browser.WaitForElement(By.ClassName("general-table-ShowHide-checkboxContainer"), "");
                
                var customFieldColumn = columnListContainer.WaitForElement(By.CssSelector($"div.shbox>label[for='{resourceID}']"), "");
                var dis = customFieldColumn.Displayed;
                return dis;
            }

            internal bool IsCustomFieldDisplay (string resourceID)
            {
                bool isVisible = false;
                var offenderListTable = _browser.WaitForElement(By.Id("divBodyContainer"), "Offender list table");

                var requiredElementInOffenderListTable = offenderListTable.WaitForElements(By.CssSelector($"[data-modify='{resourceID}']"), "Required element", minElementsToGet: 0);
                if (requiredElementInOffenderListTable.Count > 0)
                {
                    requiredElementInOffenderListTable[0].MoveCursorToElement();
                    isVisible = requiredElementInOffenderListTable[0].Displayed;
                }

                return isVisible;
            }

            internal bool IsCustomFieldDataDisplayed(string resourceID, string searchTerm)
            {

                var tableRow = GetTableRowElementByNumber(1);

                var columnElement = tableRow.WaitForElement(By.CssSelector($"[colname='{resourceID}']>span"), "Required element");
                var columnElementTxt = columnElement.Text;
                if (!string.IsNullOrEmpty(columnElementTxt))
                    return true;
                return false;

            }

            
        }

        internal class OffenderListFilterSectionPage : FilterSectionBase
        {
            public OffenderListFilterSectionPage(Browser browser) : base(browser)
            {

            }

            internal void CreateFilterByEndOfProgramForActiveOffender(DateTime time)
            {
                PressAddFilter();
                new FilterPopupPage(_browser).CreateFilterByEndOfProgramForActiveOffender(time);
            }

            internal string[] GetSystemFiltersOffendersList()
            {
                return GetSystemFilters();
            }

            private void PressAddFilter()
            {
                var addFilterBtn = _browser.WaitForElement(By.CssSelector(".icons.button.link"), "Add filter button");
                addFilterBtn.Click();
            }
        }


        internal class OffendersListGeneralActionsMenuPage : GeneralActionsMenuBasePage
        {
            public OffendersListGeneralActionsMenuPage(Browser browser) : base(browser)
            {

            }

            public void Refresh()
            {
                _browser.WaitForElement(By.CssSelector("#refreshBtn button"), "Refresh button").Click();
            }
            protected override By GetSaveButtonSelector()
            {
                throw new NotImplementedException();
            }
        }
    }
}