﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class AddNewCircularZonePopupPage : AddZonePopupPage
    {
        public AddNewCircularZonePopupPage(Browser browser):base(browser)
        {

        }

        public void AddCircularZone(string name = "", string limitation = "", int zoneGraceTime = 0, bool isFullSchedule = false, int realRadious = 0, int bufferRadious = 0)
        {
            AddZone(name, limitation, zoneGraceTime, isFullSchedule);

            if (realRadious != 0)
            {
                var radious = _container.WaitForElement(By.Id("RealRadiusField"), "Real radious");
                radious.DoubleClick();
                radious.SendKeys(Keys.Delete);
                radious.Text = realRadious.ToString();
            }

            if (bufferRadious != 0)
            {
                var buffer = _container.WaitForElement(By.Id("BufferRadiusField"), "Buffer radious");
                buffer.Text = bufferRadious.ToString();
            }

            Save();
        }
    }
}
