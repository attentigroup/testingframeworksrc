﻿using OpenQA.Selenium;
using System.Linq;
using WebTests.SeleniumWrapper;
using System;
using WebTests.Common;
using System.Collections.Generic;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using System.Drawing;
using System.Threading;
using WebTests.InfraStructure.Pages.MonitorPages;
using static WebTests.InfraStructure.Pages.MonitorPages.MonitorPage;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class AddNewOffenderPage : WOMPage
    {

        public AddNewOffenderPage(Browser browser) : base(browser)
        {
            //TODO wait for COntent of Details tab because it's the default page
        }

        DetailsTabPage DetailsTab => new DetailsTabPage(_browser);
        ConfigurationTabPage ConfigurationTab => new ConfigurationTabPage(_browser);
        LocationTabPage LocationTab => new LocationTabPage(_browser);

        ScheduleTabPage ScheduleTab => new ScheduleTabPage(_browser);
        CurrentStatusTabPage CurrentStatusTab => new CurrentStatusTabPage(_browser);

        AddNewOffenderHeaderActionButtonsPage AddNewOffenderHeaderActionButtons => new AddNewOffenderHeaderActionButtonsPage(_browser);
        OffenderDetailsHeaderPage OffenderDetailsHeader => new OffenderDetailsHeaderPage(_browser);
        AddNewOffenderGeneralActionsMenuPage AddNewOffenderGeneralActionsButtonsMenu => new AddNewOffenderGeneralActionsMenuPage(_browser);

        internal string GetEventStatus(string eventID)
        {
            return CurrentStatusTab.CurrentStatusTable.GetEventStatus(eventID);
        }

        internal void CreateEventIDUserFilter(string eventID)
        {
            CurrentStatusTab.CreateEventIDUserFilter(eventID);
        }

        internal void CreateEventMessageUserFilter(string eventMessage)
        {
            CurrentStatusTab.CreateEventMessageUserFilter(eventMessage);
        }

        internal void DeleteUserFilter(string filterName)
        {
            CurrentStatusTab.DeleteUserFilter(filterName);
        }

        internal void CreateSeverityUserFilter(string severity)
        {
            CurrentStatusTab.CreateSeverityUserFilter(severity);
        }

        internal void CreateEventTimeUserFilter(DateTime eventTime)
        {
            CurrentStatusTab.CreateEventTimeUserFilter(eventTime);
        }



        internal void HandleEvent(string eventId, string handlingOption)
        {
            CurrentStatusTab.CurrentStatusTable.HandleEvent(eventId, handlingOption);
        }

        internal bool VerifyDVLink()
        {
            return OffenderDetailsHeader.VerifyDVLink();
        }

        internal CurrentStatusTableRow GetManualEvent()
        {
            return CurrentStatusTab.CurrentStatusTable.GetManualEvent();
        }

        internal string GetOffenderTransmitter()
        {
            return DetailsTab.DetailsSecondaryTab.GetOffenderTransmitter();
        }

        internal BrowserElement GetOffenderContactDetails(int index)
        {
            return DetailsTab.DetailsSecondaryTab.GetAddressRowByNumber(index);
        }

        internal SecurityConfigObject Navigate2Section(string section, string aPIString)
        {
            switch (section)
            {
                case "AdditionalData":
                    DetailsTab.AdditionalDataSecondaryTab.DoSomething();
                    break;
                case "CaseManagment":
                    DetailsTab.CaseManagementSecondaryTab.DoSomething();
                    break;
                case "ContactInfoScreen":
                    DetailsTab.DetailsSecondaryTab.ContactInformation.ClickEditAddresButton(1);
                    break;
                case "ContactInformation":
                    DetailsTab.DetailsSecondaryTab.DoSomething();
                    break;
                case "CustomFields":
                    DetailsTab.DetailsSecondaryTab.DoSomething();
                    break;
                case "FilesList":
                    DetailsTab.AdditionalDataSecondaryTab.DoSomething();
                    break;
                case "Identification":
                    DetailsTab.DetailsSecondaryTab.DoSomething();
                    break;
                case "ProgramDetails":
                    DetailsTab.DetailsSecondaryTab.DoSomething();
                    if (aPIString == "OffenderDetails_ProgramDetails_Button_EditCellularNumbers")
                        DetailsTab.ProgramDetailsSecondaryTab.DesplayVoiceSimDataTooltip();
                    //delete, save
                    break;
                default:
                    switch (aPIString)
                    {
                        case "OffenderDetails_General_Button_SaveAdditionalData":
                            DetailsTab.AdditionalDataSecondaryTab.DoSomething();
                            break;
                        case "OffenderDetails_General_Button_SaveOffenderData":
                            break;
                        default:
                            break;
                    }
                    break;
            }            
            return GetSecurityResult(aPIString);
        }

        internal string[] GetFilterEventsArray()
        {
            return CurrentStatusTab.CurrentStatusTable.GetFilterEventsArray();
        }

        internal CurrentStatusTableRow[] GetCurrentStatusRowsArray()
        {
            return CurrentStatusTab.CurrentStatusTable.GetCurrentStatusRowsArray();
        }

        internal bool IsMapContainsZone()
        {
            return LocationTab.IsListZoneExist();
        }

        internal CurrentStatusTableRow GetCurrentStatusTableRowByEventId(string getEventId)
        {
            return CurrentStatusTab.CurrentStatusTable.GetCurrentStatusTableRowByEventId(getEventId);

        }

        internal void CreateManualEvent()
        {
            AddNewOffenderHeaderActionButtons.ManualEvent();
        }

        internal bool IsScheduleContainsTimeFrame()
        {
            return ScheduleTab.IsTimeframeExist();
        }

        internal CurrentStatusTabPage FilterCurrentStatusEventsByPeriod(string recent)
        {
            CurrentStatusTab.FilterEventsByPeriod(recent);

            return CurrentStatusTab;
        }

        public void FilterCurrentStatusEventsByRangeOfDates(DateTime startTime, DateTime endTime)
        {
            CurrentStatusTab.FilterEventByRangeOfDates(startTime, endTime);
        }

        internal string GetVoiceSimData()
        {
            return DetailsTab.DetailsSecondaryTab.GetVoiceSimData();
        }

        internal void PressDownloadNow()
        {
            AddNewOffenderHeaderActionButtons.DownloadNow();
        }

        public void FillWizardAndDownload(OffenderDetails OffenderDetails)
        {
            FillFormFull(OffenderDetails);

            //HeaderActionButtons.DownloadNow();

            AddNewOffenderGeneralActionsButtonsMenu.Delete();
        }

        public OffenderDetails FillFormFull(OffenderDetails offenderDetails)
        {
            FillDetailsFormExtended(offenderDetails);

            //FillRestDetailsForm();           

            var photoPopUp = AddPhoto();
            photoPopUp.UploadPhoto();

            return offenderDetails;
        }

        internal void ClickSendUpload()
        {
            AddNewOffenderHeaderActionButtons.SendUpload();
        }

        internal void ClickSendSuspendProgram()
        {
            AddNewOffenderHeaderActionButtons.SendSuspendProgram();
        }

        internal void ClickSendUnsuspendProgram()
        {
            AddNewOffenderHeaderActionButtons.SendUnsuspendProgram();
        }

        internal string GetOffenderDetailsLabel()
        {
            return OffenderDetailsHeader.GetOffenderDetailsLabel();
        }

        internal string GetOffenderStatus()
        {
            return OffenderDetailsHeader.GetOffenderStatus();
        }

        internal bool isPictureDisplayed()
        {
            return OffenderDetailsHeader.isPictureDisplayed();
        }

        internal List<(int, int)> GetCurrentStatusNumberOfShownResultsPerPage(int showResultsNumber)
        {
            FilterCurrentStatusEventsByPeriod("336");
            return CurrentStatusTab.GetShownRows();
        }

        public OffenderDetails FillDetailsFormExtended(OffenderDetails OffenderDetails)
        {
            DetailsTab.DetailsSecondaryTab.FillDetailsFormExtended(OffenderDetails);
            Save();
            return OffenderDetails;
        }

        public OffenderDetails FillDetailsFormBasic(OffenderDetails OffenderDetails)
        {
            DetailsTab.DetailsSecondaryTab.FillDetailsFormBasic(OffenderDetails);
            Save();
            return OffenderDetails;
        }

        private void Save()
        {
            AddNewOffenderGeneralActionsButtonsMenu.Save();
            WaitForSave();
        }

        internal void DeleteOffender()
        {
            AddNewOffenderGeneralActionsButtonsMenu.Delete();
        }

        private void FillRestDetailsForm()
        {
            DetailsTab.DetailsSecondaryTab.FillRestDetailsForm();
            AddNewOffenderGeneralActionsButtonsMenu.Save();
            WaitForSave();
        }

        public PhotoPopupPage AddPhoto()
        {
            _browser.WaitForElement(By.CssSelector(".EntityMaster-EntityHeader-OffenderPhoto.EntityMaster-SmallPhotoSize"), "Photo button").Click();

            return new PhotoPopupPage(_browser);
        }

        private void WaitForSave()
        {
            var offenderIdTextBox = GetOffenderIdTextBox();

            offenderIdTextBox.WaitForStailnessOfElement();
            Wait.Until(() => GetOffenderIdTextBox().WaitGetAttribute("disabled", 1, false) == "true");
        }

        private BrowserElement GetOffenderIdTextBox()
        {
            return _browser.WaitForElement(By.Id("ref_id_txt"), "Offender Id textbox");
        }

        public void AddSchedule(TimeFrame timeFrame)
        {
            ScheduleTab.AddSchedule(timeFrame);
        }

        public void AddZone()
        {
            LocationTab.AddZone();
        }

        public void AddPolygonZone()
        {
            LocationTab.AddPolygonZone();
        }


        public void ClickSendEOS(bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations)
        {
            AddNewOffenderHeaderActionButtons.SendEOS(noReceiver, isAutoEOS, isSuccessful, isCurfewViolations);
        }

        internal OffenderLocation GetOffenderLocationTrailData()
        {
            return LocationTab.GetOffenderLocationTrailData();
        }

        internal void LocationPlayTrail()
        {
            LocationTab.LocationPlayTrail();
        }

        internal TrailReport GetTrailReport()
        {
            return LocationTab.ReportsMenu("OffenderLocation_Trail_Button_TrailReport");
        }

        internal void DetailsHeaderSwitchToVictimOrOffender(EnmProgramConcept programConcept)
        {
            OffenderDetailsHeader.SwitchToVictimOrAggressor(programConcept);
        }

        //************ Internal Tab Classes ************
        //**********************************************
        public class DetailsTabPage : AddOffenderTabBasePage
        {
            public DetailsTabPage(Browser browser) : base(browser)
            {
                DetailsTab();
            }

            public DetailsSecondaryTabPage DetailsSecondaryTab => new DetailsSecondaryTabPage(_browser);
            public AdditionalDataSecondaryTabPage AdditionalDataSecondaryTab => new AdditionalDataSecondaryTabPage(_browser);
            public CaseManagementSecondaryTabPage CaseManagementSecondaryTab => new CaseManagementSecondaryTabPage(_browser);
            public ProgramDetailsSecondaryTabPage ProgramDetailsSecondaryTab => new ProgramDetailsSecondaryTabPage(_browser);
            public CustomDataSecondaryTabPage CustomDataSecondaryTab => new CustomDataSecondaryTabPage(_browser);
            //public ContactInformationTable ContactInformation => new ContactInformationTable(_browser);

            protected override By GetContentContainerLocator()
            {
                return By.ClassName("OffenderDetailsMain");
            }




            //*** Internal Secondary Tab classes ***

            public class DetailsSecondaryTabPage : AddOffenderTabBasePage
            {
                public DetailsSecondaryTabPage(Browser browser) : base(browser)
                {
                    var detailsSeconaryTab = _browser.WaitForElement(By.Id("offenderDetailsTab"), "'Details' secondary tab");
                    GoToTab(detailsSeconaryTab, ApplicationHeaderLocator);
                }

                public ContactInformationTable ContactInformation => new ContactInformationTable(_browser);

                ProgramDetailsSecondaryTabPage ProgramDetailsSecTab => new ProgramDetailsSecondaryTabPage(_browser);

                internal void FillDetailsFormFull(OffenderDetails OffenderDetails)
                {

                }

                internal OffenderDetails FillDetailsFormExtended(OffenderDetails OffenderDetails)
                {
                    FillBasicFormInternal(OffenderDetails);
                    ProgramDetailsSecTab.FillExtendedForm(OffenderDetails);
                    return OffenderDetails;
                }

                internal OffenderDetails FillDetailsFormBasic(OffenderDetails OffenderDetails)
                {
                    FillBasicFormInternal(OffenderDetails);
                    ProgramDetailsSecTab.FillBasicForm(OffenderDetails);
                    return OffenderDetails;
                }

                internal BrowserElement GetAddressRowByNumber(int index)
                {
                    return ContactInformation.GetAddressRowByNumber(index);
                }


                private OffenderDetails FillBasicFormInternal(OffenderDetails OffenderDetails)
                {
                    //TODO: add only if value is null
                    //TODO: add values in Logic block by generators
                    var offenderId = RandomGenerator.GenerateString(10);
                    GetOffenderIdTextBox().Text = offenderId;
                    OffenderDetails.OffenderRefId = offenderId;

                    var firstName = RandomGenerator.GenerateString(10);
                    _browser.WaitForElement(By.Id("first_name_txt"), "Offender first name").Text = firstName;
                    OffenderDetails.FirstName = firstName;

                    var lastName = RandomGenerator.GenerateString(10);
                    _browser.WaitForElement(By.Id("last_name_txt"), "Offender last name").Text = lastName;
                    OffenderDetails.LastName = lastName;

                    SelectFromCustomComboboxByText("agency_txt", OffenderDetails.Agency, "Agency selection");

                    var officer = SelectFromCustomComboboxByIndex("officer_txt", 0, "Officer selection");
                    OffenderDetails.Officer = officer;

                    ContactInformation.EditAddress(1, RandomGenerator.GenerateString(10));

                    return OffenderDetails;
                }

                //TODO merge this with FillExtendedForm
                internal void FillRestDetailsForm()
                {
                    //only for e4
                    //transmiter

                    //SelectFromCustomComboboxByText("receiver_txt", serialNumber, "Receiver combobox");
                    SelectFromCustomComboboxByIndex("transmitter_txt", 0, "Receiver combobox");
                    //var options = GetCustomComboboxOptions("transmitter_txt", "Receiver combobox");
                    //int i;

                    //for (i = 0; i < options.Count; i++)
                    //{
                    //    if (options[i].Text.Contains("serialNumber"))
                    //        break;
                    //}

                    //options[i].MoveCursorToElement(true);

                    //landline
                    var landlinePrefix = _browser.WaitForElement(By.Id("landline_prefix_txt"), "");
                    landlinePrefix.Text = RandomGenerator.GenerateNumber(1000).ToString();

                    var landlinePhone = _browser.WaitForElement(By.Id("landline_phone_txt"), "");
                    landlinePrefix.Text = RandomGenerator.GenerateNumber(1000000).ToString();

                    var outsideline = _browser.WaitForElement(By.Id("landline_ext_txt"), "");
                    landlinePrefix.Text = RandomGenerator.GenerateNumber(100).ToString();


                }

                private BrowserElement GetOffenderIdTextBox()
                {
                    return _browser.WaitForElement(By.Id("ref_id_txt"), "Offender Id");
                }

                public string GetVoiceSimData()
                {
                    return ProgramDetailsSecTab.GetVoiceSimData();
                }

                public string GetOffenderTransmitter()
                {
                    return ProgramDetailsSecTab.GetOffenderTransmitter();
                }

                protected override By GetContentContainerLocator()
                {
                    return By.ClassName("OffenderDetailsMain");
                }

                internal void DoSomething()
                {
                    throw new NotImplementedException();
                }

                public class ContactInformationTable
                {
                    private readonly Browser _browser;
                    private readonly BrowserElement _container;
                    public ContactInformationTable(Browser browser)
                    {
                        _browser = browser;
                        _container = _browser.WaitForElement(By.CssSelector(".offenderDetails_ContactInfoHolder.EntityDetails-Box-content"), "Contact info table");
                    }

                    public BrowserElement GetAddressRowByNumber(int rowNumber)
                    {
                        var row = _container.WaitForElement(By.CssSelector($"table tbody>tr:nth-of-type({rowNumber})"), "Table row");

                        return row;
                    }
                    //TODO: replace street address with object
                    public void EditAddress(int rowNumber, string address)
                    {
                        ClickEditAddresButton(rowNumber);

                        var addressDetailsPopup = new AddressDetailsPopup(_browser);
                    }

                    public void ClickEditAddresButton(int rowNumber)
                    {
                        var row = GetAddressRowByNumber(rowNumber);
                        row.MoveCursorToElement(true);

                        var editButton = row.WaitForElement(By.Id("edit_address_btn"), "Edit address");
                        Wait.Until(() => editButton.Displayed);

                        editButton.MoveCursorToElement(true);
                    }
                }
            }
            public class AdditionalDataSecondaryTabPage : AddOffenderTabBasePage
            {
                public AdditionalDataSecondaryTabPage(Browser browser) : base(browser)
                {
                    var addtionalDataSeconaryTab = _browser.WaitForElement(By.Id("additionalDataTab"), "'Additional Data' secondary tab");
                    GoToTab(addtionalDataSeconaryTab, ApplicationHeaderLocator);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("additionalDataPanel");
                }

                internal void DoSomething()
                {
                    throw new NotImplementedException();
                }
            }

            public class CaseManagementSecondaryTabPage : AddOffenderTabBasePage
            {
                public CaseManagementSecondaryTabPage(Browser browser) : base(browser)
                {
                    var caseManagementSeconaryTab = _browser.WaitForElement(By.Id("caseManagmentTab"), "Case Managment secondary tab");
                    GoToTab(caseManagementSeconaryTab, ApplicationHeaderLocator);
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("tableContainer");
                }

                internal void DoSomething()
                {
                    throw new NotImplementedException();
                }
            }

            public class ProgramDetailsSecondaryTabPage : AddOffenderTabBasePage
            {
                public ProgramDetailsSecondaryTabPage(Browser browser) : base(browser)
                {

                }

                public void FillExtendedForm(OffenderDetails offenderDetails)
                {
                    FillBasicForm(offenderDetails);

                    SelectFromCustomComboboxByContainingText("receiver_txt", offenderDetails.Receiver, "Receiver combobox");

                }

                public void FillBasicForm(OffenderDetails offenderDetails)
                {
                    var programTypeSelect = _browser.WaitForElement(By.Id("program_type_txt"), "Program type combobox");
                    programTypeSelect.SelectFromComboboxByText(offenderDetails.ProgramType);

                    if (offenderDetails.SubType != null)
                    {
                        var subTypeSelect = _browser.WaitForElement(By.Id("sub_type_txt"), "Sub type combobox");
                        subTypeSelect.SelectFromComboboxByText(offenderDetails.SubType);
                    }

                    if (offenderDetails.RelatedOffenderRefId != null)
                    {
                        //TODO: check if RelatedOffenderRefId value = offender id
                        var victimSelect = _browser.WaitForElement(By.Id("victim_list_txt"), "Victim combobox");
                        victimSelect.SelectFromComboboxByText(offenderDetails.RelatedOffenderRefId);
                    }
                }

                internal void FillFullForm(OffenderDetails offenderDetails)
                {
                    FillExtendedForm(offenderDetails);
                    //TODO add the remaining form e.g. photo

                }

                internal string GetVoiceSimData()
                {
                    DesplayVoiceSimDataTooltip();

                    var tooltip = _browser.WaitForElement(By.CssSelector("#tooltip"), "info tooltip");
                    Wait.Until(() => tooltip.Text != "");
                    var data = tooltip.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    var voiceSim = data[0].Replace(" ", String.Empty);
                    var simNumber = voiceSim.Split(new string[] { "SIMVoiceNumber" }, StringSplitOptions.RemoveEmptyEntries);
                    var number = simNumber[0].Replace("-", String.Empty);
                    return number;
                }

                public void DesplayVoiceSimDataTooltip()
                {
                    var info = _browser.WaitForElement(By.CssSelector(".general-toolTip-info-i.no_field"), "Info icon");
                    info.MoveCursorToElement();
                }

                protected override By GetContentContainerLocator()
                {
                    return By.Id("programDetailsAndCustomFields");
                }

                internal string GetOffenderTransmitter()
                {
                    var txInfo = _browser.WaitForElement(By.Id("transmitter_txt"), "Transmitter");
                    var value = txInfo.WaitGetAttribute("value");

                    return value;
                }
            }

            public class CustomDataSecondaryTabPage : AddOffenderTabBasePage
            {
                public CustomDataSecondaryTabPage(Browser browser) : base(browser)
                {

                }

                protected override By GetContentContainerLocator()
                {
                    throw new NotImplementedException();
                }
            }
        }

        public class ConfigurationTabPage : AddOffenderTabBasePage
        {
            public ConfigurationTabPage(Browser browser) : base(browser)
            {
                ConfigurationTab();
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("configMainContent");
            }
        }

        private class LocationTabPage : AddOffenderTabBasePage
        {
            BrowserElement Container;
            public LocationTabPage(Browser browser) : base(browser)
            {
                LocationTab();

                Container = _browser.WaitForElement(By.Id("locationBody"), "Container");

                Wait.Until(() => Container.Displayed && Container.Text != string.Empty);
            }

            private TrailSecondaryTabPage TrailSecondaryTab => new TrailSecondaryTabPage(_browser);
            private ZonesSecondaryTabPage ZonesSecondaryTab => new ZonesSecondaryTabPage(_browser);
            private MapAreaPage MapArea => new MapAreaPage(_browser);
            private BrowserElement GetZoneContentContainer => _browser.WaitForElement(By.ClassName("OffenderLocation-ZoneContent"), "");

            internal void AddZone()
            {
                MapArea.AddZone();
            }

            public void AddPolygonZone()
            {
                MapArea.AddPolygonZone();
            }
            internal OffenderLocation GetOffenderLocationTrailData()
            {
                var OffenderLocation = TrailSecondaryTab.GetOffenderLocationTrailData();
                bool isTrailLastPointOnMapExists = GetIsTrailLastPointOnMapExists();
                OffenderLocation.TrailLastPointOnMapExists = isTrailLastPointOnMapExists;
                return OffenderLocation;
            }



            internal void LocationPlayTrail()
            {
                MapArea.PlayTrail();
            }

            private bool GetIsTrailLastPointOnMapExists()
            {
                return MapArea.IsTrailLastPointOnMapExists();
            }

            internal bool IsListZoneExist()
            {
                return ZonesSecondaryTab.IsListZoneExist();
            }

            public TrailReport ReportsMenu(string optionStringIdentifier)
            {
                var trailReport = TrailSecondaryTab.GetReport(optionStringIdentifier);

                return trailReport;
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("locationBody");
            }

            // *** Internal tab classes ***

            private class TrailSecondaryTabPage : LocationSecondaryTabBase
            {
                public TrailSecondaryTabPage(Browser browser) : base(browser)
                {
                    TrailTab();
                }

                private TrailReportPage TrailReport => new TrailReportPage(_browser);

                internal OffenderLocation GetOffenderLocationTrailData()
                {
                    var lastPointDataElement = GetLastPointDataElement();

                    var nonDataLabel = ContentContainer.WaitForElements(By.Id("noDataLabel"), "Non data label", minElementsToGet: 0);

                    Wait.Until(() => lastPointDataElement.Text != string.Empty || nonDataLabel.FirstOrDefault().Text != string.Empty);

                    var FromToTuple = DisplayTrailByRecentTime(1);

                    return new OffenderLocation() { NonDataLabel = nonDataLabel.FirstOrDefault().Text, LastTrailPointDateTime = lastPointDataElement.Text, FromDateTime = FromToTuple.Item1, ToDateTime = FromToTuple.Item2 };
                }

                private BrowserElement GetLastPointDataElement()
                {
                    return ContentContainer.WaitForElements(By.Id("lastPointData"), "Last point", minElementsToGet: 0).FirstOrDefault();
                }

                /// <summary>
                /// This methods returns the time range of Recent filtering
                /// </summary>
                /// <param name="selectionIndex"></param>
                /// <returns></returns>
                public ValueTuple<DateTime, DateTime> DisplayTrailByRecentTime(int selectionIndex)
                {
                    var comboboxRecent = ContentContainer.WaitForElement(By.Id("recentDdl"), "Recent combobox");
                    var combobox = comboboxRecent.SelectFromComboboxByIndex(selectionIndex);

                    var btnDisplay = ContentContainer.WaitForElement(By.Id("load_points"), "Display button");
                    btnDisplay.Click();
                    GetLastPointDataElement();

                    var recentTimeSelected = DateTime.Now.AddHours(-1d * double.Parse(combobox.Text.Split(' ')[0]));
                    return (recentTimeSelected, DateTime.Now);
                }

                public TrailReport GetReport(string optionStringIdentifier)
                {
                    SelectFromDropdownList(ContentContainer, By.ClassName("OffenderLocation-MenuTrig"), optionStringIdentifier);

                    return TrailReport.GetDetails();
                }
            }

            private class ZonesSecondaryTabPage : LocationSecondaryTabBase
            {
                public ZonesSecondaryTabPage(Browser browser) : base(browser)
                {
                    ZoneTab();
                }

                internal bool IsListZoneExist()
                {
                    var zoneList = _browser.WaitForElement(By.Id("zones_list_template"), "");

                    var zones = zoneList.WaitForElements(By.CssSelector(".zone.selected"), "", minElementsToGet: 0);

                    if (zones.Any())
                        return true;

                    return false;
                }
            }

            //*** Location tab base ***
            internal class LocationSecondaryTabBase : WOMPage, IWOMTab
            {
                private readonly BrowserElement _container;

                protected override string mainMenudropDownListClassName => "OffenderLocation-TrailExpMenu";

                public LocationSecondaryTabBase(Browser browser) : base(browser)
                {
                    _container = _browser.WaitForElement(ContainerLocator, "Location panel");
                }

                By ContainerLocator => By.Id("left_panel");

                protected BrowserElement ContentContainer => _browser.WaitForElement(By.ClassName("OffenderLocation-LeftPanelBody"), "Location container");


                public string GetTabsMenuIdString()
                {
                    throw new NotImplementedException();
                }

                public void TrailTab()
                {
                    var trailTab = _container.WaitForElement(By.Id("trail_tab_header"), "Trail tab button");
                    GotoTabInternal(trailTab);
                }

                public void ZoneTab()
                {
                    var zoneTab = _container.WaitForElement(By.Id("zones_tab_header"), "Zone tab button");
                    GotoTabInternal(zoneTab);
                }

                private void GotoTabInternal(BrowserElement tab)
                {
                    GoToTab(tab, ContainerLocator);
                    Wait.Until(() => _container.Text != "");
                }
            }

            internal class MapAreaPage
            {
                private readonly Browser _browser;
                private readonly BrowserElement _container;
                public MapAreaPage(Browser browser)
                {
                    _browser = browser;
                    _container = _browser.WaitForElement(By.ClassName("rightPanelMap"), "Google map container");
                }


                private List<BrowserElement> LastTrailPoints => _container.WaitForElements(By.CssSelector("g[id^='TRAIL_LAST_POINT']"), "Trail last point on map", minElementsToGet: 0, explicitWaitForSingleElement: 3);

                public bool IsTrailLastPointOnMapExists()
                {
                    var lastPoints = LastTrailPoints;
                    if (lastPoints.Any())
                        return lastPoints.All(x => x.Displayed);

                    return false;
                }

                internal void AddPolygonZone()
                {
                    var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");

                    var btnAddPolygonZone = _browser.WaitForElement(By.Id("add_polygon_from_toolbar"), "Add polygon zone");
                    btnAddPolygonZone.Click();

                    slider.DrawPolygon(new Point[] { new Point(100, 100), new Point(50, 50), new Point(50, 10) });

                    new AddNewCircularZonePopupPage(_browser).AddCircularZone();
                }

                internal void AddZone()
                {
                    var slider = _browser.WaitForElement(By.CssSelector(".esriSimpleSlider.esriSimpleSliderVertical.esriSimpleSliderTL"), "Slider");
                    var btnAddCircularZone = _browser.WaitForElement(By.Id("add_circular_from_toolbar"), "Add circular zone");

                    btnAddCircularZone.Click();

                    slider.DragAndDropFromToOffset(100, 100);

                    new AddNewCircularZonePopupPage(_browser).AddCircularZone();
                }

                //internal bool IsZoneExist()
                //{
                //    var zoneList = _container.WaitForElement(By.Id("zones_list_template"), "");
                //    var zones = zoneList.WaitForElements(By.CssSelector(".zone.selected"), "", minElementsToGet: 0);
                //    if (zones.Any())
                //        return true;
                //    return false;
                //}

                internal void PlayTrail()
                {
                    Wait.Until(() => LastTrailPoints.Any() && LastTrailPoints.All(x => x.Displayed), "Last trail point is not displayed as expected");

                    var playButton = _container.WaitForElement(By.CssSelector(".section.controls"), "Play trail button");

                    playButton.Click();

                    LastTrailPoints[0].WaitToDisappear();
                    if (LastTrailPoints.Count > 1)
                        LastTrailPoints[1].WaitToDisappear();

                    Wait.Until(() => LastTrailPoints.All(x => x.Displayed), "Trail playing is over 60 seconds", 60);
                }
            }
        }

        public class ScheduleTabPage : AddOffenderTabBasePage
        {
            public ScheduleTabPage(Browser browser) : base(browser)
            {
                ScheduleTab();
                var scheduleContainer = _browser.WaitForElement(By.Id("schedule_main"), "");
                Wait.Until(() => scheduleContainer.Displayed);
            }
            ScheduleGeneralActionsMenuPage ScheduleGeneralActionsButtonsMenu => new ScheduleGeneralActionsMenuPage(_browser);

            internal void AddSchedule(TimeFrame timeFrame)
            {
                var AddTimeframePage = PressAddTimeFrame();
                AddTimeframePage.AddTimeFrame(timeFrame);
                var savePopup = ScheduleGeneralActionsButtonsMenu.Save();
                savePopup.LogAndSave();

            }

            private AddTimeFramePopupPage PressAddTimeFrame()
            {
                var addTimeFrameBtn = _browser.WaitForElement(By.Id("add_time_frame"), "Add timeframe button");
                addTimeFrameBtn.Click();
                return new AddTimeFramePopupPage(_browser);
            }

            internal bool IsTimeframeExist()
            {
                var schedule = _browser.WaitForElement(By.ClassName("Schedule-TimeFrames"), "");
                var timeframes = schedule.WaitForElements(By.ClassName("Schedule-TimeFrames-TimeFrame"), "", minElementsToGet: 0);
                if (timeframes.Any())
                    return true;
                return false;
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("schedule_main");
            }

            internal class ScheduleGeneralActionsMenuPage : AddNewOffenderGeneralActionsMenuPage
            {
                public ScheduleGeneralActionsMenuPage(Browser browser) : base(browser)
                {

                }

                protected override By GetSaveButtonSelector()
                {
                    return By.CssSelector("#generalSaveBtn>button");
                }

                public new SaveSchedulePopup Save()
                {
                    var btnSave = _container.WaitForElement(GetSaveButtonSelector(), "Save button");
                    btnSave.WaitToBeClickable();
                    btnSave.ClickSafely();
                    return new SaveSchedulePopup(_browser);
                }
            }
        }

        internal class CurrentStatusTabPage : AddOffenderTabBasePage
        {

            internal CurrentStatusTablePage CurrentStatusTable => new CurrentStatusTablePage(_browser);
            internal MonitorFilterSectionPage MonitorFilterSection => new MonitorFilterSectionPage(_browser);
            internal CurrentStatusFilterSectionPage CurrentStatusFilterSection => new CurrentStatusFilterSectionPage(_browser);
            internal CurrentStatusPagingPage CurrentStatusPaging => new CurrentStatusPagingPage(_browser);


            public CurrentStatusTabPage(Browser browser) : base(browser)
            {
                CurrentStatusTab();
                WaitForContentToLoad();
            }

            public CurrentStatusTableRow GetEventDetails(string eventId)
            {
                return GetEventDetailsFromTable(eventId);

            }

            private CurrentStatusTableRow GetEventDetailsFromTable(string eventId)
            {
                ClickOnCurrentStatusFilters(1);
                var GetEventsDetails = CurrentStatusTable.GetCurrentStatusTableRowByEventId(eventId);
                return GetEventsDetails;
            }

            public void FilterEventsByPeriod(string recent)
            {
                SelectCurrentStatusFilterElement(1, "All");
                CurrentStatusTable.FilterEventsByPeriod(recent);
            }
            public void FilterEventByRangeOfDates(DateTime startTime, DateTime endTime)
            {
                SelectCurrentStatusFilterElement(1, "All");
                CurrentStatusTable.FilterEventByRangeOfDates(startTime, endTime);
            }

            private void SelectCurrentStatusFilterElement(int sectionNumber, string title)
            {
                var filterRowElement = CurrentStatusFilterSection.GetFilterRowByTitle(sectionNumber, title);

                filterRowElement.WaitToBeClickable();

                filterRowElement.ClickSafely();

                CurrentStatusTable.GetContentContainerElement();
                CurrentStatusTable.WaitForContent();
            }
            public void ClickOnCurrentStatusFilters(int sectionNumber, string filterTitle = "All")
            {
                var filterRowElement = MonitorFilterSection.GetFilterRowByTitle(sectionNumber, filterTitle);

                filterRowElement.WaitToBeClickable();

                filterRowElement.ClickSafely();

            }

            internal List<ValueTuple<int, int>> GetShownRows()
            {
                return GetShownResultsPerPage(100);
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("tableContainer");
            }
            
            internal void CreateEventTimeUserFilter(DateTime eventTime)
            {
                CurrentStatusFilterSection.CreateEventTimeUserFilter(eventTime);
            }

            internal void CreateSeverityUserFilter(string severity)
            {
                CurrentStatusTable.GetContentContainerElement();
                CurrentStatusTable.WaitForContent();
                CurrentStatusFilterSection.CreateSeverityUserFilter(severity);
            }

            internal void CreateEventIDUserFilter(string eventID)
            {
                CurrentStatusFilterSection.CreateEventIDUserFilter(eventID);
            }

            internal void CreateEventMessageUserFilter(string eventMessage)
            {
                CurrentStatusFilterSection.CreateEventMessageUserFilter(eventMessage);
            }

            internal void DeleteUserFilter(string filterName)
            {
                var filterRowElement = CurrentStatusFilterSection.GetFilterRowByTitle(2, filterName);
                filterRowElement.MoveCursorToElement();
                var deleteBtn = filterRowElement.WaitForElement(By.CssSelector(".icon.delete"), "Delete button");
                deleteBtn.WaitToBeClickable();
                deleteBtn.Click();
                new DeleteFilterPopupPage(_browser).Delete();
            }


            //*** internal classess ***

            internal class CurrentStatusTablePage : WOMTableBase
            {
                public CurrentStatusTablePage(Browser browser) : base(browser)
                {

                }

                CurrentStatusPagingPage CurrentStatusPaging => new CurrentStatusPagingPage(_browser);
                CurrentStatusFilterSectionPage CurrentStatusFilterSection => new CurrentStatusFilterSectionPage(_browser);

                public void WaitForContent()
                {
                    WaitForContentToLoad();
                }

                protected override By GetContentContainerLocator()
                {
                    return By.ClassName("center_content");
                }

                public void FilterEventByRangeOfDates(DateTime startTime, DateTime endTime)
                {
                    FilterByRangeOfDates(startTime, endTime);
                    WaitForContent();

                }

                internal void FilterEventsByPeriod(string recent)
                {
                    FilterByPeriod(recent);
                    WaitForContent();

                }

                public CurrentStatusTableRow GetCurrentStatusTableRowByEventId(string eventIdSearch)
                {
                    Search(eventIdSearch);

                    Wait.Until(() => GetTableRowsElements().Count() == 1 && GetTableRowsElements().First().Displayed);

                    var tableRow = GetTableRowElementByNumber(1);

                    return GetCurrentStatusTableleRow(tableRow);
                }

                private CurrentStatusTableRow GetCurrentStatusTableleRow(BrowserElement tableRow)
                {
                    var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                    Wait.Until(() => eventIdElement.Text != string.Empty);

                    var eventId = eventIdElement.Text;
                    var eventTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_EventTime'"), "DateTime").Text;
                    var message = tableRow.WaitForElement(By.CssSelector(".message>span"), "Message").Text;
                    var updateTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_UpdateTime']"), "Update time").Text;
                    var status = tableRow.WaitForElement(By.CssSelector(".list_grid_icon>span"), "").WaitGetAttribute("title");

                    var currentStatusTableleRow = new CurrentStatusTableRow()
                    {
                        EventId = eventId,
                        EventTime = eventTime,
                        Message = message,
                        UpdateTime = updateTime,
                        Status = status

                    };

                    return currentStatusTableleRow;
                }

                internal string[] GetFilterEventsArray()
                {
                    string[] datesArr = new string[2];
                    var tableRow = GetTableRowElementByNumber(1);

                    var sortButton = _browser.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();

                    tableRow = GetTableRowElementByNumber(1);
                    var monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    var firstRowTimeStr = monitorTableRow.EventTime;
                    datesArr[0] = firstRowTimeStr;

                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("0"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();
                    tableRow = GetTableRowElementByNumber(1);
                    monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    firstRowTimeStr = monitorTableRow.EventTime;
                    datesArr[1] = firstRowTimeStr;

                    return datesArr;

                }

                public string GetEventStatus(string eventId)
                {

                    Search(eventId);
                    var tableRow = GetTableRowElementByNumber(1);
                    var currentStatusTableRow = GetCurrentStatusTableleRow(tableRow);
                    var status = currentStatusTableRow.Status;
                    return status;


                }

                private CurrentStatusTableRow GetCurrentStatusTableRow(BrowserElement tableRow)
                {
                    var eventIdElement = tableRow.WaitForElement(By.CssSelector(".ellipsis.eventID[colname='EventGrid_List_GridHeader_EventID']"), "Event Id");
                    Wait.Until(() => eventIdElement.Text != string.Empty);

                    var eventId = eventIdElement.Text;
                    var eventTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_EventTime'"), "DateTime").Text;
                    var message = tableRow.WaitForElement(By.CssSelector(".message>span"), "Message").Text;
                    var updateTime = tableRow.WaitForElement(By.CssSelector(".eventTime[colname='EventGrid_List_GridHeader_UpdateTime']"), "Update time").Text;
                    var status = tableRow.WaitForElement(By.CssSelector(".list_grid_icon>span"), "status").GetAttribute("title");
                    var severity = tableRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.center.severityStatus>div"), "Severity").GetAttribute("title");


                    var currentStatusTableRow = new CurrentStatusTableRow()
                    {
                        EventId = eventId,
                        EventTime = eventTime,
                        Message = message,
                        UpdateTime = updateTime,
                        Status = status,
                        Severity = severity
                    };

                    return currentStatusTableRow;
                }


                internal CurrentStatusTableRow GetManualEvent()
                {
                    var tableRow = GetTableRowElementByNumber(1);
                    var currentStatusTableRow = GetCurrentStatusTableRow(tableRow);
                    DateTime time = DateTime.ParseExact(currentStatusTableRow.EventTime, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    if (time >= DateTime.Now.AddMinutes(-5) && time <= DateTime.Now && currentStatusTableRow.Message.StartsWith("Violation LVL 1"))
                        return currentStatusTableRow;

                    var sortButton = _browser.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();

                    tableRow = GetTableRowElementByNumber(1);

                    currentStatusTableRow = GetCurrentStatusTableRow(tableRow);
                    var firstRowTimeStr = currentStatusTableRow.EventTime;
                    DateTime firstRowTime = DateTime.ParseExact(firstRowTimeStr, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                    Wait.Until(() => IsManualEventExist(firstRowTime) == true);

                    return GetCurrentStatusRow();

                }


                public bool IsManualEventExist(DateTime previousRowTime)
                {
                    CurrentStatusTableRow currentStatusTableRow = GetCurrentStatusRow();
                    var RowTimeStr = currentStatusTableRow.EventTime;
                    DateTime RowTime = DateTime.ParseExact(RowTimeStr, "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                    var receiver = currentStatusTableRow.ReceiverSN;
                    var message = currentStatusTableRow.Message;
                    if ((RowTime > previousRowTime) && currentStatusTableRow.Message.StartsWith("Violation LVL 1"))
                        return true;
                    return false;
                }

                private CurrentStatusTableRow GetCurrentStatusRow()
                {
                    CurrentStatusTableRow monitorRow;
                    var Row = GetTableRowElementByNumber(1);

                    try
                    {
                        monitorRow = GetCurrentStatusTableRow(Row);
                    }
                    catch (StaleElementReferenceException)
                    {
                        return GetCurrentStatusRow();
                    }

                    return monitorRow;
                }

                internal void HandleEvent(string eventId, string handlingOption)
                {
                    Search(eventId);
                    var tableRow = GetTableRowElementByNumber(1);
                    var expandBtn = tableRow.WaitForElement(By.CssSelector(".expand_icon"), "expand button");
                    expandBtn.Click();
                    var expandData = _browser.WaitForElement(By.CssSelector(".general-table-body-additionalDataExpand._additionalDataExpand.additionalData_0.body.p_event_expand"), "");
                    SelectFromDropdownList(expandData, By.CssSelector(".button.action.red"), "Handle", containerLocator: By.ClassName("dropdownNavi"));
                    new RemarkPopupPage(_browser).Remark();
                }

                internal CurrentStatusTableRow[] GetCurrentStatusRowsArray()
                {
                    CurrentStatusTableRow[] eventsArr = new CurrentStatusTableRow[2];
                    var tableRow = GetTableRowElementByNumber(1);

                    var sortButton = _browser.WaitForElement(By.CssSelector(".td.ellipsis.mainTableHeader.general-table-header-titlePadding.general-table-header-cell>div"), "");
                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("1"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();

                    tableRow = GetTableRowElementByNumber(1);
                    var monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    eventsArr[0] = monitorTableRow;

                    sortButton.Click();
                    if (!sortButton.WaitGetAttribute("sort-direction").Equals("0"))
                        sortButton.Click();

                    tableRow.WaitForStailnessOfElement();
                    tableRow = GetTableRowElementByNumber(1);
                    monitorTableRow = GetCurrentStatusTableRow(tableRow);
                    eventsArr[1] = monitorTableRow;

                    return eventsArr;
                }
            }

            internal class CurrentStatusFilterSectionPage : FilterSectionBase
            {
                public CurrentStatusFilterSectionPage(Browser browser) : base(browser)
                {

                }

                internal void CreateEventIDUserFilter(string eventID)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateEventIDFilter(eventID);
                }

                internal void CreateEventMessageUserFilter(string eventMessage)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateEventMessageUserFilter(eventMessage);
                }

                internal void CreateEventTimeUserFilter(DateTime eventTime)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateEventTimeFilter(eventTime);
                }

                internal void CreateSeverityUserFilter(string severity)
                {
                    PressAddFilter();
                    new FilterPopupPage(_browser).CreateSeverityFilter(severity);
                }

                private void PressAddFilter()
                {
                    var addFilterBtn = _browser.WaitForElement(By.CssSelector(".icons.button.link"), "Add filter button");
                    addFilterBtn.WaitToBeClickable();
                    addFilterBtn.ClickSafely();
                }
            }


            internal class CurrentStatusPagingPage : WOMTablePagingPage
            {
                public CurrentStatusPagingPage(Browser browser) : base(browser)
                {

                }
            }
        }



        //************ Tabs Base Class ************
        //*****************************************
        public abstract class AddOffenderTabBasePage : WOMTableBase, IWOMTab
        {
            protected abstract override By GetContentContainerLocator();

            public AddOffenderTabBasePage(Browser browser) : base(browser)
            {
                Wait.Until(() => OffenderPageHeader.Displayed, "Waiting for Offender page to load");
            }

            protected BrowserElement OffenderPageHeader => _browser.WaitForElement(By.Id("OffenderMasterTemplate"), "Offender master page header");
            protected BrowserElement TabsMenuContainer => _browser.WaitForElement(By.CssSelector(GetTabsMenuIdString()), "Tabs menu");

            public string GetTabsMenuIdString()
            {
                return "#offender_tabs_menu";
            }

            public void DetailsTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='OffenderDetails']"), "Details tab");

                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void ConfigurationTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='ProgConfig']"), "Configuration tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void LocationTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='Location']"), "Location tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void ScheduleTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='Schedule']"), "Schedule tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void CurrentStatusTab()
            {
                var tab = OffenderPageHeader.WaitForElement(By.CssSelector(GetTabsMenuIdString() + " li[data-tab='OffenderStatus']"), "Offender status tab");
                GoToTab(tab, ApplicationHeaderLocator);
            }

            protected void SelectFromCustomComboboxByText(string comboboxId, string optionText, string description)
            {
                var selectOption = GetCustomComboboxOptions(comboboxId, description).FirstOrDefault(x => x.Text == optionText);
                selectOption.MoveCursorToElement(true);
            }

            protected void SelectFromCustomComboboxByContainingText(string comboboxId, string optionText, string description)
            {
                var selectOption = GetCustomComboboxOptions(comboboxId, description).FirstOrDefault(x => x.Text.Contains(optionText));
                selectOption.MoveCursorToElement(true);
            }

            protected string SelectFromCustomComboboxByIndex(string comboboxId, int index, string description)
            {
                var selectOption = GetCustomComboboxOptions(comboboxId, description).ElementAt(index);
                var officer = selectOption.Text;
                selectOption.MoveCursorToElement(true);
                return officer;
            }

            protected List<BrowserElement> GetCustomComboboxOptions(string comboboxId, string description)
            {
                var combobox = _browser.WaitForElement(By.XPath($"//input[@id='{comboboxId}']/.."), description);
                var input = combobox.WaitForElement(By.TagName("input"), "Input element");
                Wait.Until(() => input.Displayed);
                input.MoveCursorToElement();
                input.DoubleClick();
                Thread.Sleep(200);//Slepp only for animation of combobox!!!
                var options = combobox.WaitForElements(By.CssSelector("ul>li"), "options");

                return options;
            }
        }

        internal class AddNewOffenderGeneralActionsMenuPage : GeneralActionsMenuBasePage
        {
            public AddNewOffenderGeneralActionsMenuPage(Browser browser) : base(browser)
            {

            }

            protected override By GetSaveButtonSelector()
            {
                return By.CssSelector("#generalSaveBtn>button");
            }
        }

        internal class AddNewOffenderHeaderActionButtonsPage : HeaderBottomPageBase
        {
            BrowserElement _buttonsContainer;
            public AddNewOffenderHeaderActionButtonsPage(Browser browser) : base(browser)
            {
                _buttonsContainer = _browser.WaitForElement(By.Id("header_action_buttons"), "Buttons container");
            }
            public void DownloadNow()
            {
                SelectFromDropdownList(ApplicationHeader, GetDownloadButtonLoactor(), "Button_DownloadNow", containerLocator: By.Id("download_menu"));
                var downloadPage = new DownloadAlertPopupPage(_browser);
                downloadPage.PressOk();
            }

            public void DownloadNextCall()
            {
                SelectFromDropdownList(ApplicationHeader, GetDownloadButtonLoactor(), "Button_DownloadNextCall");
            }

            private By GetDownloadButtonLoactor()
            {
                return By.CssSelector(".btn.download");

            }

            private By GetOffenderActionsButtonLoactor()
            {
                return By.CssSelector(".btn.manual");

            }

            public void SendEOS(bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations)
            {

                //var menuButton = _buttonsContainer.WaitForElement(By.Id("ActionBtn"), "Action Button");
                //menuButton.MoveCursorToElement();
                //menuButton.WaitForElement(By.CssSelector("res:text=\"Offender_Actions_Button_SendEndOfService\""), "Send EOS").MoveCursorToElement(true);
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.other"), "Button_SendEndOfService", containerLocator: By.Id("ActionBtn"));

                var EOSPage = new EndOfServicePopupPage(_browser);

                EOSPage.FillForm(noReceiver, isAutoEOS, isSuccessful, isCurfewViolations);


            }

            internal void SendSuspendProgram()
            {
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.other"), "Button_SuspendProgram", containerLocator: By.Id("ActionBtn"));

                var SuspendProgram = new SuspendProgramPopupPage(_browser);

                SuspendProgram.FillForm();
            }

            internal void SendUnsuspendProgram()
            {
                SelectFromDropdownList(ApplicationHeader, By.CssSelector(".btn.other"), "Button_UnsuspendProgram", containerLocator: By.Id("ActionBtn"));

                var UnsuspendProgram = new UnsuspendProgramPopupPage(_browser);

                UnsuspendProgram.FillForm();
            }

            internal void SendUpload()
            {
                var btnUpload = _buttonsContainer.WaitForElement(By.CssSelector(".btn.upload"), "Upload");

                btnUpload.WaitToBeClickable();
                btnUpload.Click();

                var UploadAlert = new UploadAlertPopupPage(_browser);

                UploadAlert.PressOK();
            }

            internal void ManualEvent()
            {
                SelectFromDropdownList(ApplicationHeader, GetOffenderActionsButtonLoactor(), "Button_SendManualEvent", containerLocator: By.CssSelector(".current.manual"));
                new ManualEventHandlingPopupPage(_browser).ManualEvent();
            }
        }


        internal class OffenderDetailsHeaderPage : WOMPage
        {
            BrowserElement _container;
            public OffenderDetailsHeaderPage(Browser browser) : base(browser)
            {
                _container = _browser.WaitForElement(By.Id("OffenderMasterTemplate_details0"), "Offender Master Template Container");
                Wait.Until(() => _container.Text != "");
            }


            public string GetOffenderDetailsLabel()
            {
                var label = _browser.WaitForElement(By.CssSelector(".EntityMaster-EntityHeader-DetailsBlock.ellipsis.right"), "Header Details Label");
                return label.Text;
            }

            public string GetOffenderStatus()
            {
                return _browser.WaitForElement(By.CssSelector(".EntityMaster-DetailsCard .wrapper>div>div>span"), "Offender Status").WaitGetAttribute("title");
            }

            public void SwitchToVictimOrAggressor(EnmProgramConcept programConcept)
            {
                var OffenderConceptLine = _container.WaitForElements(By.CssSelector(".wrapper>.EntityMaster-EntityHeader-DetailsBlock"), "Ofender header details", minElementsToGet: 0);
                if (OffenderConceptLine.Last().GetAttribute("outerHTML").Contains(programConcept.ToString()))
                {
                    var offenderLink = OffenderConceptLine.Last().WaitForElement(By.CssSelector(".button.link"), "Offender link");
                    offenderLink.Click();
                }
            }

            internal bool isPictureDisplayed()
            {
                var a = _browser.WaitForElement(By.Id("smallPhoto"), "Offender picture");

                return a.Displayed;
            }

            internal bool VerifyDVLink()
            {
                BrowserElement AggButton;
                var EntityHeader = _browser.WaitForElements(By.CssSelector(".EntityMaster-EntityHeader-DetailsBlock.ellipsis"), "DV Link");
                var EntityDetails = EntityHeader[1].Text;

                if (EntityDetails.Contains("Aggressor"))
                {
                    AggButton = EntityHeader[1].WaitForElement(By.CssSelector(".button.link"), "Aggressor Button");
                    AggButton.WaitToBeClickable();
                    AggButton.Click();
                }

                EntityHeader = _browser.WaitForElements(By.CssSelector(".EntityMaster-EntityHeader-DetailsBlock.ellipsis"), "DV Link");
                EntityDetails = EntityHeader[1].Text;

                if (EntityDetails.Contains("Victim"))
                {
                    return true;
                }

                return false;
            }
        }
    }

    public enum ScheduleAPIElementName
    {
        Schedule_Actions_Button_AddTimeFrame,
        Schedule_Actions_Button_DeleteAllTimeFrames,
        Schedule_Actions_Button_OpenTimeFrameBox,
        Schedule_Actions_Button_RepeatSchedule,
        Schedule_Actions_Button_RepeatVoiceTests,
        Schedule_Actions_Button_SaveGroupSchedule,
        Schedule_Actions_Button_SaveOffenderSchedule,
        Schedule_TimeFrameTooltip_Button_Delete,
        Schedule_TimeFrameTooltip_Button_Edit
    }

    public enum OffenderDetailsAPIElementName
    {
        OffenderDetails_AdditionalData_Button_AddContact,
        OffenderDetails_AdditionalData_Button_DeleteContact,
        OffenderDetails_AdditionalData_Button_EditContact,
        OffenderDetails_AdditionalData_Section_OffenderContacts,
        OffenderDetails_CaseManagment_Button_AddAbsenceRequest,
        OffenderDetails_CaseManagment_Button_AddSceduleChangeReason,
        OffenderDetails_CaseManagment_Button_AddWarning,
        OffenderDetails_CaseManagment_Button_DeleteAbsenceRequest,
        OffenderDetails_CaseManagment_Button_DeleteScheduleChangeReason,
        OffenderDetails_CaseManagment_Button_DeleteWarning,
        OffenderDetails_CaseManagment_Button_UpdateAbsenceRequest,
        OffenderDetails_CaseManagment_Button_UpdateScheduleChangeReason,
        OffenderDetails_CaseManagment_ListItem_AbsenceRequests,
        OffenderDetails_CaseManagment_ListItem_ProgramSuspends,
        OffenderDetails_CaseManagment_ListItem_ReallocateOffenderLog,
        OffenderDetails_CaseManagment_ListItem_ScheduleChangeReasons,
        OffenderDetails_CaseManagment_ListItem_Warnings,
        OffenderDetails_ContactInfoScreen_Label_City,
        OffenderDetails_ContactInfoScreen_Label_Country,
        OffenderDetails_ContactInfoScreen_Label_Phone,
        OffenderDetails_ContactInfoScreen_Label_PhoneName,
        OffenderDetails_ContactInfoScreen_Label_PrefixPhone,
        OffenderDetails_ContactInfoScreen_Label_State,
        OffenderDetails_ContactInfoScreen_Label_Street,
        OffenderDetails_ContactInfoScreen_Label_TimeZone,
        OffenderDetails_ContactInfoScreen_Label_Title,
        OffenderDetails_ContactInfoScreen_Label_ZipCode,
        OffenderDetails_ContactInformation_Button_AddNewAddress,
        OffenderDetails_ContactInformation_Button_AddPhone,
        OffenderDetails_ContactInformation_Button_AddZone,
        OffenderDetails_ContactInformation_Button_DeleteAddress,
        OffenderDetails_ContactInformation_Button_DeletePhone,
        OffenderDetails_ContactInformation_Button_OpenMap,
        OffenderDetails_ContactInformation_Button_UpdateAddress,
        OffenderDetails_ContactInformation_Header_EditDeleteAddress,
        OffenderDetails_FilesList_GridHeader_Description,
        OffenderDetails_FilesList_GridHeader_FileName,
        OffenderDetails_FilesList_GridHeader_FileSizeInKB,
        OffenderDetails_FilesList_GridHeader_Index,
        OffenderDetails_FilesList_GridHeader_LastModifiedDate,
        OffenderDetails_FilesList_GridHeader_LastModifiedUser,
        OffenderDetails_General_Button_SaveAdditionalData,
        OffenderDetails_General_Button_SaveOffenderData,
        OffenderDetails_General_Tab_AdditionalData,
        OffenderDetails_General_Tab_CaseManagement,
        OffenderDetails_Identification_Label_Agency,
        OffenderDetails_Identification_Label_DOC,
        OffenderDetails_Identification_Label_DateCreated,
        OffenderDetails_Identification_Label_DateOfBirth,
        OffenderDetails_Identification_Label_FirstName,
        OffenderDetails_Identification_Label_Gender,
        OffenderDetails_Identification_Label_ID,
        OffenderDetails_Identification_Label_Language,
        OffenderDetails_Identification_Label_LastName,
        OffenderDetails_Identification_Label_MiddleName,
        OffenderDetails_Identification_Label_Officer,
        OffenderDetails_Identification_Label_ProgramEnd,
        OffenderDetails_Identification_Label_ProgramStart,
        OffenderDetails_Identification_Label_SocialSecurity,
        OffenderDetails_Identification_Label_Title,
        OffenderDetails_ProgramDetails_Button_DeleteCellularData,
        OffenderDetails_ProgramDetails_Button_EditCellularNumbers,
        OffenderDetails_ProgramDetails_Button_SaveCellularData,
        OffenderDetails_ProgramDetails_Label_BaseUnit,
        OffenderDetails_ProgramDetails_Label_CourtOrder,
        OffenderDetails_ProgramDetails_Label_CurfewUnit,
        OffenderDetails_ProgramDetails_Label_Landline,
        OffenderDetails_ProgramDetails_Label_OutsideLine,
        OffenderDetails_ProgramDetails_Label_ProgramType,
        OffenderDetails_ProgramDetails_Label_Receiver,
        OffenderDetails_ProgramDetails_Label_SubType,
        OffenderDetails_ProgramDetails_Label_Transmitter,
        OffenderDetails_ProgramDetails_Label_Victim,
        OffenderDetails_ProgramDetails_Label_VoiceSIM


    }
}




