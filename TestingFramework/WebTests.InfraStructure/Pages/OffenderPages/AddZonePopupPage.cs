﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public abstract class AddZonePopupPage
    {
        protected readonly Browser _browser;
        protected readonly BrowserElement _container;
        public AddZonePopupPage(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(By.Id("PopupContainer"), "Edit cellular data popup");
            Wait.Until(() => _container.Displayed);
        }

        protected void AddZone(string name = "", string limitation = "", int zoneGraceTime = 0, bool isFullSchedule = false)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var zoneName = _container.WaitForElement(By.CssSelector("input[data-property='Name']"), "zone name");
                zoneName.Click();
                zoneName.SendKeys(Keys.Control + "a");
                //zoneName.Text = name;
                zoneName.SendKeys(name);
            }

            if (!string.IsNullOrEmpty(limitation))
            {
                var type = _container.WaitForElement(By.CssSelector("select[data-property='Limitation']"), "Limitation");
                type.SelectFromComboboxByText(limitation);
            }

            if (zoneGraceTime != 0)
            {
                var graceTime = _container.WaitForElement(By.Id("GraceField"), "Grace time");
                graceTime.Clear();
                graceTime.Text = zoneGraceTime.ToString();
            }

            if (isFullSchedule)
            {
                var fullSchedule = _container.WaitForElement(By.Id("checkboxFullSchedule"), "Full schedule");
                fullSchedule.Click();
            }
            
        }

        protected void Save()
        {
            var popupButtons = _container.WaitForElements(By.ClassName("buttons"), "");
            var buttons = _container.WaitForElements(By.TagName("button"), "");
            var saveBtn = buttons.FirstOrDefault(x => x.Text.Equals("Save")); //buttons[2];
            saveBtn.Click();
        } 
    }
}
