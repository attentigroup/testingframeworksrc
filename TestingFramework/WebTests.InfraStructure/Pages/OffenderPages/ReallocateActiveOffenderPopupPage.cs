﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages.OffenderPages
{
    public class ReallocateActiveOffenderPopupPage : PopupPageBase
    {
        public ReallocateActiveOffenderPopupPage(Browser browser) : base(browser)
        {
        }

    }
}
