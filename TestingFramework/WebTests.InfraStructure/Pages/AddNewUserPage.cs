﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class AddNewUserPage : WOMPage
    {
        public AddNewUserPage(Browser browser) : base(browser)
        {
            var detailsTab = DetailsTab.GetDetailsTab();
            if (IsTabSelected(detailsTab))
                Wait.Until(() => GetDetailsContentContainer().Displayed);
        }

        private BrowserElement GetDetailsContentContainer()
        {
            return _browser.WaitForElements(By.ClassName("EntityMaster-EntityContent"), "user details content container", minElementsToGet: 0, explicitWaitForSingleElement: 3).FirstOrDefault();
        }

        internal DetailsTabPage DetailsTab => new DetailsTabPage(_browser);
        internal OffendersListTabPage OffendersListTab => new OffendersListTabPage(_browser);
        internal AgenciesTabPage AgenciesTab => new AgenciesTabPage(_browser);
        internal OffendersListSceduleTabPage OffendersListSceduleTab => new OffendersListSceduleTabPage(_browser);

        public void FillForm()
        {
            DetailsTab.FillForm();
        }

        internal SecurityConfigObject Navigate2Section(string ScreenName, string section, string APIString)
        {
            if (ScreenName == "UserOffendersGrid")
                new OffendersListTabPage(_browser);
            else if (ScreenName == "UserAgenciesGrid")
                AgenciesTab.PressEditButton();
            else if (section == "TimeFrameTooltip")
                OffendersListSceduleTab.GetOffenderScheduleActionsButtonLocator();

            return GetSecurityResult(APIString);
        }

       
        internal class DetailsTabPage : AddNewUserTabBase
        {
            public DetailsTabPage(Browser browser):base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.Id("userMasterTabsUserDetails"), "Details tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
            }

            internal IdentificationSectionPage IdentificationSection => new IdentificationSectionPage(_browser);
            internal ProgramDetailsPage ProgramDetails => new ProgramDetailsPage(_browser);
            internal ContactInformationPage ContactInformation => new ContactInformationPage(_browser);

            internal void FillForm()
            {
                throw new NotImplementedException();
            }

            public BrowserElement GetDetailsTab()
            {
                return _browser.WaitForElement(By.Id("userMasterTabsUserDetails"), "Details tab");
            }

            internal class IdentificationSectionPage
            {
                private Browser _browser;

                public IdentificationSectionPage(Browser browser)
                {
                    _browser = browser;
                }
            }

            internal class ProgramDetailsPage
            {
                private Browser _browser;

                public ProgramDetailsPage(Browser browser)
                {
                    _browser = browser;
                }
            }

            internal class ContactInformationPage
            {
                private Browser _browser;

                public ContactInformationPage(Browser browser)
                {
                    _browser = browser;
                }
            }
        }

        internal class AgenciesTabPage : AddNewUserTabBase
        {
            public AgenciesTabPage(Browser browser) : base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.Id("userAgenciesTab"), "Details tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
            }

            internal void PressEditButton()
            {
                var edit = _browser.WaitForElement(By.Id("editBtn"), "Edit button");
                edit.WaitToBeClickable();
                edit.Click();
            }
        }

        internal class OffendersListTabPage : AddNewUserTabBase
        {
            public OffendersListTabPage(Browser browser) : base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.Id("userOffendersTab"), "Details tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
            }

        }

        internal class OffendersListSceduleTabPage : AddNewUserTabBase
        {
            public OffendersListSceduleTabPage(Browser browser) : base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.Id("userOffendersTab"), "Schedule tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
            }
            public void GetOffenderScheduleActionsButtonLocator()
            {
                var TimeFrameBox = _browser.WaitForElement(By.CssSelector(".Schedule-TimeFrames-TimeFrame"), "Schedule-TimeFrames-TimeFrame");
                TimeFrameBox.MoveCursorToElement();
            }
           

        }

        



        internal class AddNewUserTabBase : WOMPage, IWOMTab
        {
            public AddNewUserTabBase(Browser browser) : base(browser)
            {

            }

            public string GetTabsMenuIdString()
            {
                return ".Tabs";
            }
        }

        
    }
}
