﻿using OpenQA.Selenium;
using System;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class E4ReceiverVersionImportPage : WOMPage
    {
        public E4ReceiverVersionImportPage(Browser browser):base(browser)
        {
            
        }
        
        internal E4ReceiverVersionImportPopupPage E4ReceiverVersionImportPopup => new E4ReceiverVersionImportPopupPage(_browser);
        
        public void Import(string filePath)
        {
            E4ReceiverVersionImportPopup.ImportFile(filePath);
        }

        internal class E4ReceiverVersionImportPopupPage
        {
            private readonly Browser _browser;

            public E4ReceiverVersionImportPopupPage(Browser browser)
            {
                _browser = browser;
            }

            BrowserElement Container => _browser.WaitForElement(By.Id("PopupContainer"), "Popup container");

            internal void ImportFile(string filePath)
            {
                var btnBrowse = Container.WaitForElement(By.Id("fileDownload"), "Browse button");
                btnBrowse.Click();

                new OpenFileDialog(_browser).UploadFile(filePath);

                Save();
            }

            private void Save()
            {
                var btnSave = Container.WaitForElement(By.Id("saveTex"),"Save button");

                btnSave.Click();

                Container.WaitToDisappear();

                //TODO: check that no further confirmation is needed
            }
        }
    }
}