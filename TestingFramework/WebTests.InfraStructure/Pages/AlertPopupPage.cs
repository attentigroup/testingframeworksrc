﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class AlertPopupPage
    {
        private readonly Browser _browser;

        public AlertPopupPage(Browser browser)
        {
            _browser = browser;
        }

        public BrowserElement AlertPopupWindow
        {
            get
            {
                var popupWindows = _browser.WaitForElements(By.CssSelector(".Popup.primary-popup.ui-draggable"), "All popup windows");

                Wait.Until(() => popupWindows.Any(p => p.Displayed));

                var alertPopup = popupWindows.Single(p => p.Displayed);

                Wait.Until(() => alertPopup.Enabled);

                return alertPopup;
            }
        }

        /// <summary>
        /// Use for SINGLE button Alert/Confirm window
        /// </summary>
        public void Confirm()
        {
            var btnConfirm = GetAllButtons().First(b => b.Displayed);

            ClickButton(btnConfirm);
        }

        public void Cancel()
        {
            var btnCancel = GetAllButtons().Last(b => b.GetAttribute("onclick").Contains("cancel"));

            ClickButton(btnCancel);
        }

        private void ClickButton(BrowserElement button)
        {
            button.ClickSafely();

            button.WaitToDisappear();
        }

        private List<BrowserElement> GetAllButtons()
        {
            return AlertPopupWindow.WaitForElements(By.TagName("button"), "All buttons", 4);
        }
        
        public void Close()
        {
            var closeButton = AlertPopupWindow.WaitForElement(By.CssSelector(".button.close"), "Close button");

            var popAlertBackground = _browser.WaitForElement(By.Id("popupAlertBackGround"), "Alert popup background");

            closeButton.ClickSafely();

            closeButton.WaitToDisappear(1);

            popAlertBackground.WaitToDisappear(1);
        }
    }
}
