﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;
using static WebTests.InfraStructure.Pages.QueueAndLogPage.QueueTabPage;
//using static WebTests.InfraStructure.Pages.QueueTabPage;

namespace WebTests.InfraStructure.Pages
{
    public class QueueAndLogPage : WOMPage
    {
        public QueueAndLogPage(Browser browser) : base(browser)
        {
        }

        public LogTabPage LogTab => new LogTabPage(_browser);
        public QueueTabPage QueueTab => new QueueTabPage(_browser);

        public QueueAndLogFilterSectionPage QueueAndLogFilterSection => new QueueAndLogFilterSectionPage(_browser);

        //public static object QueFilterSection { get; internal set; }
        QueFilterSectionPage QueFilterSection => new QueFilterSectionPage(_browser);

        internal LogFilterSectionPage LogFilterSection => new LogFilterSectionPage(_browser);

        

        //*** POC Security Configuration tests ***
        public List<List<SecurityConfigObject>> GetSecurityConfigurationResults()
        {
            var secConfigListCollection = QueueAndLogSecorityConfigData.SecConfigData();

            var securityConfigMethods = GetSecurityConfigMethods();

            return GetSecurityConfigResults(secConfigListCollection, securityConfigMethods);
        }
        Dictionary<string, string> Section2filter = new Dictionary<string, string>()
            {
                {"ListSchedule","Scheduled Tasks" },
                {"ListTrackers","1 Piece GPS" },
                {"List","" },
                {"ListVoice","Voice" },
                {"ListDownload","Auto Download" },
                {"ListNotification","Fax" },
                {"ListNotification1","Email" },
                {"General","RF Curfew Dual (E4)" },
                {"ListAsyc","" },
                {"ListRF","RF Curfew Dual (E4)" },
            };
       

        //public object ConfirmChangeForElement(/*string[] element,*/ string Section, string Type, string Title)
        //{

        //    if (Section.Equals("ListSchedule"))
        //    {
        //        Navigate2Section("Scheduled Tasks");
        //    }

        //    var getSecurityElemntBy = QueueAndLogSecorityConfigData.getSecurityElemntBy(/*element,*/ Section, Type, Title);
        //    return IsElementsVisibles(getSecurityElemntBy, Title);

        //}

        internal SecurityConfigObject GetSecurityResult(string section, string APIString, string title)
        {
            if (title == "Mail")
            {
                section = "ListNotification1";
            }
            if(title != "Filter")
            {
                QueFilterSection.SelectFilterByTitle(-1, Section2filter[section]);
            }
                
                return GetSecurityResult(APIString);
        }



        [SecurityConfiguration]
        public List<SecurityConfigObject> Queue_ListSchedule(List<List<SecurityConfigObject>> secConfigListCollection)
        {
            return QueueTab.Queue_ListSchedule(secConfigListCollection);
        }

        [SecurityConfiguration]
        public List<SecurityConfigObject> Log_ListSchedule(List<List<SecurityConfigObject>> secConfigListCollection)
        {
            return LogTab.Log_ListSchedule(secConfigListCollection);
        }
            //***


        //************ Internal Tab Classes ************
        //**********************************************

        public class QueueTabPage : QueAndLogTabBase
        {


            public QueueTabPage(Browser browser) : base(browser)
            {
                QueueTab();
            }

            internal QueFilterSectionPage QueFilterSection => new QueFilterSectionPage(_browser);

            protected override By GetContentContainerLocator()
            {
                return By.Id("divBodyContainer");
            }

            protected override string GetTableSearchCssSelectorString()
            {
                return "#searchInput";
            }

            internal class QueFilterSectionPage : QueAndLogFilterSectionBasePage
            {
                public QueFilterSectionPage(Browser browser) : base(browser)
                {

                }


            }

            internal class LogFilterSectionPage : QueAndLogFilterSectionBasePage
            {
                public LogFilterSectionPage(Browser browser) : base(browser)
                {

                }

            }

            //POC security Configuration test
            public List<SecurityConfigObject> Queue_ListSchedule(List<List<SecurityConfigObject>> secConfigListCollection)
            {
                //Get this method's name
                var thisMethodName = MethodBase.GetCurrentMethod().Name;

                var scheduleFilter = QueFilterSection.GetFilterRowByTitle(3, "Scheduled Tasks");
                scheduleFilter.Click();

                return CheckVisibility(thisMethodName, secConfigListCollection);
            }

            internal List<QueueRow> GetQueueRows(int filterSectionNumber, string filterTitle)
            {
                var queueList = new List<QueueRow>();
                QueFilterSection.SelectFilterByTitle(filterSectionNumber, filterTitle);

                var rows = GetAllRowElements();

                foreach (var row in rows)
                {
                    var queueRow = new QueueRow();
                    
                    if (filterTitle.Contains("E4"))
                    {
                        queueRow.Request = row.WaitForElement(By.CssSelector("[colname = 'Queue_ListRF_GridHeader_Request']"), "Request").Text;
                        queueRow.RequestTime = row.WaitForElement(By.CssSelector("[colname = 'Queue_ListRF_GridHeader_RequestTime']"), "Request time").Text;
                        queueRow.ReceiverSerialNumber = row.WaitForElement(By.CssSelector("[colname = 'Queue_ListRF_GridHeader_RcvrSN']"), "Receiver SN").Text;
                    }
                    else
                    {
                        queueRow.Request = row.WaitForElement(By.CssSelector("[colname = 'Queue_ListTrackers_GridHeader_Request']"), "Request").Text;
                        queueRow.RequestTime = row.WaitForElement(By.CssSelector("[colname = 'Queue_ListTrackers_GridHeader_RequestTime']"), "Request time").Text;
                        queueRow.OffenderName = row.WaitForElement(By.CssSelector("[colname = 'Queue_ListTrackers_GridHeader_OffenderName']"), "Offender Name").Text;
                    }
                    
                    queueList.Add(queueRow);
                }

                return queueList;
            }
        }

        public class LogTabPage : QueAndLogTabBase
        {

            Dictionary<string, string> Section2filter = new Dictionary<string, string>()
            {
                {"ListEmail","Email" },
                {"ListFax","Fax" },
                {"ListNotification","Fax" },
                {"ListPager","Text Message" },
                {"ListProgType","2 Piece GPS" },
                {"ListSchedule","Scheduled Tasks" },
                {"ListVoice","Voice" },
                {"List","" },
            };




            public LogTabPage(Browser browser) : base(browser)
            {
                LogTab();
            }

            LogFilterSectionPage LogFilterSection => new LogFilterSectionPage(_browser);

            protected override By GetContentContainerLocator()
            {
                return By.Id("divBodyContainer");
            }

            //*** POC Security Configuration test ***
            internal List<SecurityConfigObject> Log_ListSchedule(List<List<SecurityConfigObject>> secConfigListCollection)
            {
                var thisMethodName = MethodBase.GetCurrentMethod().Name;

                var scheduleFilter = LogFilterSection.GetFilterRowByTitle(3, "Scheduled Tasks");
                scheduleFilter.Click();

                return CheckVisibility(thisMethodName, secConfigListCollection);
            }

            internal SecurityConfigObject GetSecurityResult(string section, string APIString, string title)
            {
                if (title != "Filter")
                {
                    LogFilterSection.SelectFilterByTitle(-1, Section2filter[section]);
                }
                return GetSecurityResult(APIString);
            } 

            //***

            internal class LogFilterSectionPage : QueAndLogFilterSectionBasePage
            {
                public LogFilterSectionPage(Browser browser) : base(browser)
                {

                }

            }
        }
    }

        //************ Tabs Base Class ************
        //*************************************
        public abstract class QueAndLogTabBase : WOMTableBase, IWOMTab
        {
            public QueAndLogTabBase(Browser browser) : base(browser)
            {

            }
            public void QueueTab()
            {
                var tab = GetTabLinks(1);

                GoToTab(tab, ApplicationHeaderLocator);
            }

            public void LogTab()
            {
                var tab = GetTabLinks(2);
                GoToTab(tab, ApplicationHeaderLocator);
            }

            private BrowserElement GetTabLinks(int index)
            {
                return ApplicationHeader.WaitForElements(By.CssSelector(GetTabsMenuIdString() + " li"), "Log tab")[index];
            }

            public string GetTabsMenuIdString()
            {
                return "#navigation_tabs";
            }

            public List<BrowserElement> GetAllRowElements()
            {
                var rows = GetTableRowsElements();

                return rows;
            }

            internal abstract class QueAndLogFilterSectionBasePage : FilterSectionBase
            {
                public QueAndLogFilterSectionBasePage(Browser browser) : base(browser)
                {

                }
            }

            protected abstract override By GetContentContainerLocator();

        protected override string GetTableRowCssSelectorString()
        {
            return "#tableContainer .row";
        }
    }

        public class QueueAndLogFilterSectionPage : FilterSectionBase, ISecurityConfiguration
        {
            public QueueAndLogFilterSectionPage(Browser browser) : base(browser)
            {

            }

            public bool AreElementsVisible(List<By> locatorCollection)
            {

                foreach (var item in locatorCollection)
                {
                    _browser.WaitForElements(item, "config test", minElementsToGet: 0);
                }

                return true;
            }

            internal void SecurityConfig_Tab1()
            {
                throw new NotImplementedException();
            }
        }
    }

    //TODO: move to separate file
    public class ConfigurationTestMethod
    {
        public ConfigurationTestMethod()
        {
            //ExpectedVisible = expectedVisible;
        }
        public string MethodName { get; set; }
        public MethodInfo Method { get; set; }

    }

    public static class QueueAndLogSecorityConfigData
    {
        public static List<List<SecurityConfigObject>> SecConfigData()
        {
            var secConfigListCollection = new List<List<SecurityConfigObject>>();
            secConfigListCollection.Add(new List<SecurityConfigObject>() { new SecurityConfigObject() { ElementName = QueueAndLogAPIElementName.Queue_ListSchedule_GridHeader_Status.ToString(), Locator = By.CssSelector("#tblHeader>div[title='Status']") }, new SecurityConfigObject() { ElementName = QueueAndLogAPIElementName.Queue_ListSchedule_GridHeader_Other.ToString(), Locator = By.ClassName("head_Bottom") } });
            secConfigListCollection.Add(new List<SecurityConfigObject>() { new SecurityConfigObject() { ElementName = QueueAndLogAPIElementName.Log_ListSchedule_GridHeader_Status.ToString(), Locator = By.CssSelector("#tblHeader>div[title='Status']") }, new SecurityConfigObject() { ElementName = QueueAndLogAPIElementName.Log_ListSchedule_GridHeader_Other.ToString(), Locator = By.CssSelector(".head_Bottom button[title='Export To Excel']") } });
            return secConfigListCollection;
        }

        internal static By getSecurityElemntBy(/*string[] element, */string Section, string Type, string Title)
        {
            By by = null;

            //if (element.Contains("General"))
            //{

            //    List<WebElement> links = browser.findElement(By.CssSelector("#navigation_tabs li"));
            //    for (WebElement link : links)
            //    {
            //        if (link.getText().equals(Title))
            //            return link;
            //    }

            //    by = By.CssSelector("[data-modify='" + element + "']");
            //}
            //else
            //{
               
                if (Type == "Button")
                    by = By.CssSelector(".btn [title='" + Title + "']");

                else if (Type == "GridHeader")
                {
                if (Title == "Mail")
                    by = By.CssSelector("#tblHeader [title='Email']");
                else if (Title == "ActionComment")
                    by = By.CssSelector("#tblHeader [title='Comment']");
                else if (Title == "OID")
                    by = By.CssSelector("#tblHeader [title='Offender ID']");
                else if (Title == "CommCom" || Title == "CommComp")
                    by = By.CssSelector("#tblHeader [title='Communication Server']");
                else if (Title == "RcvrSN")
                    by = By.CssSelector("#tblHeader [title='Receiver Serial No.']");
                else if (Title == "ProgType")
                    by = By.CssSelector("#tblHeader [title='Program Type']");
                else if (Title == "ScheduleTime")
                    by = By.CssSelector("#tblHeader [title='Scheduled Time']");
                else if (Title == "PreTime")
                    by = By.CssSelector("#tblHeader [title='Reminder Time']");
                else if (Title == "TaskSubType")
                    by = By.CssSelector("#tblHeader [title='Reason']");
                else if (Title == "TaskType")
                    by = By.CssSelector("#tblHeader [title='Task']");
                else if (Title == "DownloadDate")
                    by = By.CssSelector("#tblHeader [title='Download Time']");
                else if (Title == "AgencyName")
                    by = By.CssSelector("#tblHeader [title='Agency']");
                else if (Title == "SeqNo")
                    by = By.CssSelector("#tblHeader [title='Sequence No.']");
                else if (Title == "Seq")
                    by = By.CssSelector("#tblHeader [title='Seq.']");
                else if (Title == "Index")
                    by = By.CssSelector("#tblHeader [title='']");
                else if (Title == "TZ")
                    by = By.CssSelector("#tblHeader [title='TZ']");
                /////////
                //OffendersList
               
                ////////
                else //(Title.Length == 1)
                        by = By.CssSelector("#tblHeader [title='" + Title + "']");
                //else
                //    by = By.CssSelector("#tblHeader [title='" + Title[0] + " " + Title[1] + "']");
                
                }

                   else if (Type == "Section")
                          by = By.CssSelector(".sidePanel");
            //var tableRow = GetTableRowElementByNumber(1);
            //By webElemnet = tableRow.WaitForElement(by);

            //if (Type == "GridHeader")
            //{

            //    WebdriverUtils.scrollToElement(webElemnet, driver);
            //}


            return by;//driver.findElement(by);

           
        }




        public enum QueueAndLogAPIElementName
        {
            Queue_ListSchedule_GridHeader_Status,
            Log_ListSchedule_GridHeader_Status,
            Queue_ListSchedule_GridHeader_Other,
            Log_ListSchedule_GridHeader_Other,
            Queue_ListTrackers_GridHeader_TZ,
            Queue_General_Button_Delete,
            Queue_ListTrackers_GridHeader,
            Queue_ListTrackers,
            Queue_List_Section_Filter
        }

    }