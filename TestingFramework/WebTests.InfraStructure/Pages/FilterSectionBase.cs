﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public abstract class FilterSectionBase
    {
        protected readonly Browser _browser;


        public FilterSectionBase(Browser browser)
        {
            _browser = browser;
        }

        internal BrowserElement PanelContainer => _browser.WaitForElements(GetFilterContainerSelector(), "Panel container", minElementsToGet: 0, explicitWaitForSingleElement: 3)?.FirstOrDefault();

        protected virtual By GetFilterContainerSelector()
        {
            return By.Id("FilterWrapperCollection");
        }

        public bool IsSectionVisible()
        {
            var filterSectionResult = _browser.WaitForElements(GetFilterContainerSelector(), "Filter section", minElementsToGet: 0, explicitWaitForSingleElement: 3);

            if (filterSectionResult.Any() && filterSectionResult.Single().Displayed)
                return true;

            return false;
        }

        public void SelectFilterByTitle(int sectionNumber, string filterTitle)
        {
            var filter = GetFilterRowByTitle(sectionNumber, filterTitle);
            if (filter != null && !filter.WaitGetAttribute("class").Contains("selected"))
            {
                filter.MoveCursorToElement();
                filter.ClickSafely();
                Thread.Sleep(100);//TODO check if can be removed
            }
        }

        public void SelectFilterByContainingText(int sectionNumber, string filterText)
        {
            var filter = GetFilterByContainingText(sectionNumber, filterText);
            if (!filter.WaitGetAttribute("class").Contains("selected"))
            {
                filter.ClickSafely();
            }
        }

        /// <summary>
        /// Use sectionNumber = -1 to ignore filter sections
        /// </summary>
        /// <param name="sectionNumber"></param>
        /// <param name="filterTitle"></param>
        /// <returns></returns>
        public BrowserElement GetFilterRowByTitle(int sectionNumber, string filterTitle)
        {
            var rows = GetFilterRowElementsBySectionNumber(sectionNumber);

            var selectedRow = rows?.FirstOrDefault(r => r.WaitForElementByAttributeValue(By.TagName("div"), "title", filterTitle, IsExactValue: true, failIfNoValue: false, seconds: 0) != null);

            return selectedRow;
        }

        public BrowserElement GetFilterByContainingText(int sectionNumber, string title)
        {
            int i;
            string element = string.Empty;
            var rows = GetFilterRowElementsBySectionNumber(sectionNumber);
            for (i = 0; i < rows.Count; i++)
            {
                element = rows[i].GetAttribute("outerHTML");
                if (element.Contains(title))
                    break;
            }
            return rows[i];
        }


        protected List<BrowserElement> GetFilterRowElementsBySectionNumber(int sectionNumber)
        {

            if (sectionNumber == 0)
                throw new Exception("Filter Section number should be greater than 0 or -1 to ignore sections");

            List<BrowserElement> rows = null;

            if (PanelContainer != null)
            {
                var sections = PanelContainer.WaitForElements(By.CssSelector("div[class~='content']"), "Filter rows");
                if (sectionNumber > 0)
                {
                    rows = sections[--sectionNumber].WaitForElements(GetRowsSelector(), "Filter section row");
                }
                else
                {
                    rows = PanelContainer.WaitForElements(GetRowsSelector(), "All rows");
                }
            }
            return rows;
        }

        protected virtual By GetRowsSelector()
        {
            return By.CssSelector("div[class^='row']");
        }

        public void Search(string title)
        {
            var searchBox = _browser.WaitForElement(By.CssSelector("#search_agencies_list #searchField"), "Search box");

            searchBox.Text = title;

            var searchButton = _browser.WaitForElement(By.CssSelector("#search_agencies_list>.search .search"), "Search button");

            searchButton.Click();
        }

        public int GetFilterCounter(string filterTitle)
        {
            var filterElement = GetFilterRowByTitle(1, filterTitle);
            var counterElement = filterElement.WaitForElement(GetFilterCounterLocator(), "Filter counter text");

            var isNumber = int.TryParse(counterElement.Text, out int result);

            if (isNumber)
                return result;
            else
                return 0;
        }

        protected virtual By GetFilterCounterLocator()
        {
            return By.CssSelector(".counter.ellipsis");
        }

        protected virtual string[] GetSystemFilters()
        {
            string filterName = string.Empty;
            var filters = GetFilterRowElementsBySectionNumber(1);
            var filtersNames = new string[filters.Count];
            for (int i = 0; i < filters.Count; i++)
            {
                var filterTxt = filters[i].Text;
                if (filterTxt.Contains("\r"))
                    filterName = filterTxt.Substring(0, filterTxt.IndexOf("\r"));
                else
                    filterName = filterTxt;
                filtersNames[i] = filterName;
            }
            return filtersNames;
        }
    }
}
