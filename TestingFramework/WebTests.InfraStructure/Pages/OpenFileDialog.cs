﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;
using WebTests.Common;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class OpenFileDialog
    {
        private readonly Browser _browser;
        public OpenFileDialog(Browser browser)
        {
            _browser = browser;
        }

        public void UploadFile(string fileName)
        {
            var windowTitle = GetWindowTitle(_browser.BrowserName);
            Logger.WriteLine("UIAutomation message: Searching for Window Title name: *{0}*", windowTitle);
            var path = Path.GetFullPath("." + @"\Resources\" + fileName);

            Thread.Sleep(3000);

            var dialogWindow = AutomationElement.RootElement.GetUIAutomationElementByPropertyCondition(new PropertyCondition(AutomationElement.LocalizedControlTypeProperty, "Dialog"));
            Logger.WriteLine($"UIAutomation message: Found element for File Dialoge with name *{dialogWindow.Current.Name}* and Localized Control Type *{dialogWindow.Current.LocalizedControlType}*");

            var dialogTextBox = dialogWindow.GetUIAutomationElementByAutomationId("1148", TreeScope.Descendants);
            dialogTextBox.InsertTextUsingUIAutomation(path);
            var btns = dialogWindow.GetAllButtonsFromUIAutomationElement(TreeScope.Children);
            (btns[0]).UIAutomationClick();
        }

        private string GetWindowTitle(BrowserName browserName)
        {
            switch (browserName)
            {
                case BrowserName.Chrome:
                    return "Open";
                case BrowserName.Firefox:
                    return "File Upload";
                case BrowserName.IE:
                    return "Choose File to Upload";
                default:
                    throw new Exception($"Browser name '{browserName.ToString()}'not found" );
            }
        }
    }
}
