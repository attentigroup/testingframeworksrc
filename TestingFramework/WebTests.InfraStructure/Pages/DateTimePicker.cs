﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class DateTimePicker 
    {
        private Browser _browser;

        protected BrowserElement _container;
        public DateTimePicker(Browser browser)
        {
            _browser = browser;
            _container = _browser.WaitForElement(GetContainerLocater(), "Calender Container");
            
        }

        protected virtual By GetContainerLocater()
        {
           return By.Id("ui-datepicker-div");
        }

        public void ClickNextMonth()
        {
            var iconForward = _container.WaitForElement(By.ClassName("ui-datepicker-next"), "icon-next");
                iconForward.WaitToBeClickable();
                iconForward.Click();
        }

        public void PickToDate(string day)
        {
            var days = _container.WaitForElements(By.ClassName("ui-state-default"), "pick a day");
            var selectedDay = days.First(x => x.Text == day);
            selectedDay.WaitToBeClickable();
            selectedDay.Click();
        }

        public void PickFromDate(string day)
        {

            var unselectableDays = _container.WaitForElements(By.CssSelector(".ui-datepicker-unselectable"), "pick a day");
            //var unselectableDays = _container.WaitForElements(By.ClassName("ui-state-default"), "pick a day");
            var unselectableDay = unselectableDays.FirstOrDefault(x => x.Text == day);
            if(unselectableDay == null)
                PickToDate(day);
            else
            {
                var checkIfDayDisplay = unselectableDay.GetAttribute("class");
                if (!checkIfDayDisplay.Contains("disabled"))
                {
                    PickToDate(day);
                }
                else
                {
                    ClickNextMonth();
                    PickToDate(day);
                }
            }
            

        }

        
    }
}
