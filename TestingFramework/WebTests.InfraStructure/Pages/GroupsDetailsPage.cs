﻿using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;
using WebTests.Common;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;

namespace WebTests.InfraStructure.Pages
{
    public class GroupsDetailsPage : WOMPage
    {
        public DetailsTabPage DetailsTab => new DetailsTabPage(_browser);

        public GroupsDetailsPage(Browser browser) : base(browser)
        {
            var detailsTab = _browser.WaitForElement(By.CssSelector("[data-modify='General_Screen_Link_GroupDetails']"), "Details tab");

            if (IsTabSelected(detailsTab))
                Wait.Until(() => GetDetailsContentContainer().Displayed, seconds: 5);
        }

        public BrowserElement GetDetailsContentContainer()
        {
            return _browser.WaitForElements(By.CssSelector(".groupDetails-workArea-box>.groupDetails-workArea-content"), "Group details content container", minElementsToGet: 0, explicitWaitForSingleElement: 3).FirstOrDefault();
        }
                
        public void FillForm(GroupTypeEnum GroupType, string GroupName,string GroupDescription)
        {
            DetailsTab.FillForm(GroupType, GroupName, GroupDescription);
        }

        public class DetailsTabPage : WOMTableBase
        {
            public DetailsTabPage(Browser browser) : base(browser)
            {
                var tabDetails = _browser.WaitForElement(By.CssSelector("[data-modify='General_Screen_Link_GroupDetails']"), "Details tab");
                GoToTab(tabDetails, ApplicationHeaderLocator);
                WaitForContentToLoad();
            }

            protected override By GetContentContainerLocator()
            {
                throw new NotImplementedException("No Locator Container");
            }

            /// <summary>
            /// Translate from choice in combobox to index for SelectFromCustomComboboxByIndex method
            /// </summary>
            public enum GroupTypeEnum
            {
                Tracker = 0,
                RFCurfew_E4_Family = 1,
            }

            internal void FillForm(GroupTypeEnum GroupType, string GroupName, string GroupDescription)
            {
                int index=(int)GroupType ;

                var tabDetails = _browser.WaitForElement(By.CssSelector(".groupDetails-upperBox-box"), "Details tab");
                tabDetails.SelectFromCustomComboboxByIndex(By.Id("GroupTypeField"), By.CssSelector(".ac_results.ellipsis>ul>li"), index);

                var groupName = _browser.WaitForElement(By.Id("GroupNameField"), "Group Name Field");
                groupName.Text = GroupName;

                var groupDescription = _browser.WaitForElement(By.Id("GroupDescriptionField"), "Group Description Field");
                groupDescription.Text = GroupDescription;

            }

            private void ToggleCheckbox(BrowserElement tableRow)
            {
                var checkBox = tableRow.WaitForElement(By.CssSelector("input[type='checkbox']"), "Check box");
                checkBox.WaitToBeClickable();
                checkBox.ClickSafely();
            }

            public void selectOffenderByIndex(int index)
            {
                Thread.Sleep(1000);
                SortByColumn(false);//false = desc
                for (int i = 1; i <= index; i++)
                {
                    var row = GetTableRowElementByNumber(i);
                    
                    ToggleCheckbox(row);
                }
            }

            public void Save()
            {
                new GroupPageGeneralActionsMenu(_browser).Save();
            }

            public void Delete()
            {
                new GroupPageGeneralActionsMenu(_browser).Delete();
            }

            protected override string GetTableRowCssSelectorString()
            {
                return "#OffendersLine>tr";
            }

        }
    }

    public class GroupPageGeneralActionsMenu : GeneralActionsMenuBasePage
    {
        public GroupPageGeneralActionsMenu(Browser browser) : base(browser)
        {
        }

        protected override By GeneralActionsContainerSelector()
        {
            return By.CssSelector(".navigation_bottom .actions");
        }

        protected override By GetSaveButtonSelector()
        {

            return By.CssSelector(".actions button[title='Save'] ");
        }
    }
}
