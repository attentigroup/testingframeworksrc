﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class SystemTablesPage : WOMPage
    {
        public SystemTablesPage(Browser browser):base(browser)
        {
            
        }


        internal SystemTablesFilterSectionPage SystemTablesFilterPanel => new SystemTablesFilterSectionPage(_browser);
        internal SystemTablesTablePage SystemTablesTable => new SystemTablesTablePage(_browser);

        internal void ChangeCustomFieldsVisibility(List<string> customFieldsVisibilityList, bool ShowInOffenderDetails, bool ShowInOffendersList)
        {
            SystemTablesTable.ChangeCustomFieldsVisibility(customFieldsVisibilityList, ShowInOffenderDetails, ShowInOffendersList);
            
        }

        //TODO put methods here
        public void SelectColorsDemo()
        {
            SystemTablesFilterPanel.SelectFilterByTitle("Application Colors");
        }

        internal void UpdateCustomFieldsNames(Dictionary<string, string> customFieldsNames)
        {
            SystemTablesTable.UpdateCustomFieldsNames(customFieldsNames);
        }

        internal void AddCustomFieldsListValues(string tableIdName, List<string> customFieldsListValues)
        {
            SystemTablesTable.AddCustomFieldsListValues(tableIdName, customFieldsListValues);
        }

        internal void UpdateCustomFieldsListValues(string tableIdName, string description, string updatedValueDescription)
        {
            SystemTablesTable.UpdateCustomFieldsListValues(tableIdName, description, updatedValueDescription);
        }

        public bool CheckIfItsPossibleToDeleteCustomFieldListValueUsedByOffender(string description)
        {
            return SystemTablesTable.CheckIfItsPossibleToDeleteCustomFieldListValueUsedByOffender(description);

        }

        internal void DeleteCustomFieldsListValues(string description)
        {
            SystemTablesTable.DeleteCustomFieldsListValues(description);
        }

        internal Dictionary<string, bool> GetExistanceStatusOfUpdatedCustomFieldsNames(List<string> customFieldsNamesList)
        {
            return SystemTablesTable.GetUpdatedCustomFieldsNames(customFieldsNamesList);
        }

        internal class SystemTablesFilterSectionPage : FilterSectionBase
        {
            public SystemTablesFilterSectionPage(Browser browser):base(browser)
            {
            }

            public void SelectFilterByTitle(string title)
            {
                var filter = GetFilterRowByTitle(1, title);

                filter.WaitToBeClickable();

                filter.Click();
            }
        }

        internal class SystemTablesTablePage : WOMTableBase
        {
            public SystemTablesTablePage(Browser browser) : base(browser)
            {
                WaitForContentToLoad();
            }

            protected override By GetContentContainerLocator()
            {
                return By.ClassName("main_content");
            }

            internal void ChangeCustomFieldsVisibility(List<string> customFieldsVisibilityList, bool showInOffenderDetails, bool showInOffendersList)
            {
                string errors = string.Empty;

                foreach (var customField in customFieldsVisibilityList)
                {
                    Search(customField);
                    var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows", minElementsToGet: 0);
                    if(rows.Count == 0)
                    {
                        errors += $"Search for custom field: {customField} returned no value, custom field not found. ";
                        continue;
                    }
                        
                    BrowserElement customFieldRow = null;

                    foreach (var row in rows)
                    {
                        var nameElement = row.WaitForElement(By.CssSelector("div[key='Name']>input"), customField);
                        var nameTxt = nameElement.GetAttribute("value");
                        if (customField.Equals(nameTxt))
                        {
                            customFieldRow = row;
                            break;
                        }

                    }
                    if (customFieldRow == null)
                    {
                        errors += $"Custom field {customField} row not found. ";
                        continue;
                    }

                    var ShowInOffendersListCheckbox = customFieldRow.WaitForElement(By.CssSelector("input[id^='ShowInOffendersList_']"), "Show In Offenders List checkbox");
                    var ShowInOffenderDetailsCheckbox = customFieldRow.WaitForElement(By.CssSelector("input[id^='ShowInOffenderDetails_']"), "Show In Offender Details checkbox");

                    if (showInOffenderDetails)
                    {

                        if (!ShowInOffenderDetailsCheckbox.Selected)
                            ShowInOffenderDetailsCheckbox.Click();
                    }
                    else
                    {
                        if (ShowInOffenderDetailsCheckbox.Selected)
                            ShowInOffenderDetailsCheckbox.Click();
                    }

                    if (showInOffendersList)
                    {                        
                        if (!ShowInOffendersListCheckbox.Selected)
                            ShowInOffendersListCheckbox.Click();
                    }
                    else
                    {                        
                        if (ShowInOffendersListCheckbox.Selected)
                            ShowInOffendersListCheckbox.Click();
                    }

                    Save();
                }

                if(! string.IsNullOrEmpty(errors))
                {
                    throw new Exception($"ChangeCustomFieldsVisibility failed with errors: {errors}.");
                }
            }

            private void Save()
            {
                var saveBtn = _browser.WaitForElement(By.CssSelector(".button.button.square.save"), "save button");
                saveBtn.WaitToBeClickable();
                saveBtn.Click();
            }

            protected override string GetTableSearchButtonCssSelectorString()
            {
                return ".search_button.ng-pristine.ng-untouched.ng-valid";
            }

            internal void UpdateCustomFieldsNames(Dictionary<string, string> customFieldsNames)
            {
                foreach (var customField in customFieldsNames)
                {
                    Search(customField.Key);
                    var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows");
                    var firstRow = rows[0];

                    var fieldName = firstRow.WaitForElement(By.CssSelector("div[key='Name']>input"), "Field name");
                    fieldName.Text = customField.Value;

                    Save();
                }
            }

            internal void AddCustomFieldsListValues(string tableIdName, List<string> customFieldsListValues)
            {
                foreach (var customField in customFieldsListValues)
                {
                    var addBtn = _browser.WaitForElement(By.Id("Div4"), "Add custom field list button");
                    addBtn.WaitToBeClickable();
                    addBtn.Click();

                    var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows");
                    var firstRow = rows[0];

                    var listName = firstRow.WaitForElement(By.CssSelector("div[key='TableID']>input"), "List name");
                    listName.Text = tableIdName;

                    var valueDescription = firstRow.WaitForElement(By.CssSelector("div[key='ValueDescription']>input"), "Value description");
                    valueDescription.Text = customField;

                    Save();
                }
            }

            internal void UpdateCustomFieldsListValues(string tableIdName, string description, string updatedValueDescription)
            {
                Search(description);

                var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows");
                var firstRow = rows[0];
                var valueDescription = firstRow.WaitForElement(By.CssSelector("div[key='ValueDescription']>input"), "Value description");
                valueDescription.Text = updatedValueDescription;

                Save();
            }

            public bool CheckIfItsPossibleToDeleteCustomFieldListValueUsedByOffender(string description)
            {
                bool isNotDeletable = false;
                Search(description);

                var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows");
                var firstRow = rows[0];
                var deleteColumn = firstRow.WaitForElement(By.CssSelector(".td.general-table-body-cell.ellipsis.delete"), "DeleteColumn");
                var deleteBtn = deleteColumn.WaitForElements(By.ClassName("deleteImg"), "Delete button", minElementsToGet: 0);
                if (deleteBtn.Count == 0)
                    isNotDeletable = true;
                return isNotDeletable;

            }

            internal void DeleteCustomFieldsListValues(string description)
            {
                Search(description);

                var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows");
                var firstRow = rows[0];

                var deleteValueBtn = firstRow.WaitForElement(By.CssSelector("div[pos='0']>div"), "Delete value button");
                deleteValueBtn.Click();

                var deleteConfirmPopup = _browser.WaitForElement(By.CssSelector(".Popup.primary-popup.ui-draggable"), "Delete confirm popup");
                var okBtn = deleteConfirmPopup.WaitForElements(By.TagName("button"), "OK button");
                okBtn[0].Click();

                Save();
            }
            //public BrowserElement SelectRowByListNameAndValue(string listName, string value)
            //{
            //    Search(value);

            //    var rows = GetTableRowsElements();

            //    BrowserElement tableRow = null;

            //    for (int i = 0; i < rows.Count; i++)
            //    {
            //        var name = rows[i].WaitForElement(By.CssSelector("div[key='TableID']>div"), "List name column");
            //        var valueDescription = rows[i].WaitForElement(By.CssSelector("div[key='ValueDescription']>input"), "Value description");
            //        if (name.Text.Equals(listName) && valueDescription.Text.Equals(value))
            //        {
            //            tableRow = rows[i];
            //            break;
            //        }
            //        else
            //            throw new Exception($"No line found for list name: '{listName}' with value: '{value}'");
            //    }

            //    return tableRow;
            //}

            protected override string GetTableRowCssSelectorString()
            {
                return "#tableContainer .general-table-body-row.ng-scope";
            }

            internal Dictionary<string, bool> GetUpdatedCustomFieldsNames(List<string> customFieldsNamesList)
            {
                var isCustomFieldNameChangeDictionary = new Dictionary<string, bool>();

                foreach(var fieldName in customFieldsNamesList)
                {
                    Search(fieldName);

                    var rows = _browser.WaitForElements(By.CssSelector("#tbl>div"), "rows");

                    bool isExist = false;

                    foreach (var row in rows)
                    {
                        //var firstRow = rows[0];

                        var nameElement = row.WaitForElement(By.CssSelector("div[key='Name']>input"), "");
                        var nameTxt = nameElement.GetAttribute("value");

                        
                        if (fieldName.Equals(nameTxt))
                        {
                            isExist = true;
                            break;
                        }
                            
                    }
                    
                    isCustomFieldNameChangeDictionary.Add(fieldName, isExist);

                }

                return isCustomFieldNameChangeDictionary;
            }
        }
    }
}