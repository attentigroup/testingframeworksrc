﻿using System;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class CurrentKnownLocationPage : WOMPage
    {
        public CurrentKnownLocationPage(Browser browser):base(browser)
        {
        }

        internal SecurityConfigObject Navigate2Section(string section, string APIString)
        {
            return GetSecurityResult(APIString);
        }
    }
}