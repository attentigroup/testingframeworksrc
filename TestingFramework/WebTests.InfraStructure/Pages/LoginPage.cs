﻿using OpenQA.Selenium;
using WebTests.SeleniumWrapper;
using WebTests.InfraStructure.Entities;
using WebTests.Common;
using System;
using System.Configuration;
using WebTests.InfraStructure.Pages.WOMPages;

namespace WebTests.InfraStructure.Pages
{
    public class LoginPage : WOMPage
    {
        private readonly BrowserElement _container;

        private User _user;
        
        private string _server;

        public LoginPage(Browser browser) : base(browser, false)
        {
            _container = GetLoginContainer();
            _user = new User() { UserName = "emsprc", Password = "Q1w2e3r4" };
            _server = ConfigurationManager.AppSettings["Server"];// "vmSolaries_Env_13";
        }

        public BrowserElement GetLoginContainer()
        {
            var container = _browser.WaitForElement(By.CssSelector("center>.login_wrap"), "Login page");

            Wait.Until(() => container.Displayed && container.Enabled);

            return container;
        }

        private void LoginInternal(User user, string server)
        {
            ValidateServer(server);
            var container = _browser.WaitForElement(By.CssSelector("center>.login_wrap"), "Login page");
            container.WaitForElement(By.Id("Login1_UserName"), "Username box").Text = user.UserName;

            container.WaitForElement(By.Id("Login1_Password"), "Password box").Text = user.Password;

            var btnLogin = container.WaitForElement(By.Id("Login1_LoginButton"), "Login button");

            Performance.StartPoint( PerformanceTag.Login);

            btnLogin.Click();

            btnLogin.WaitToDisappear();

            Performance.EndPoint(PerformanceTag.Login);

            //Print version to log SysInfo
            //var version = GetVersions();
            //Logger.SysInfo("Web version", version.WebVersion);
            //Logger.SysInfo("DB version", version.DatabaseVersion);
            //Logger.SysInfo("DB server", version.DatabaseServer);
            //Logger.WriteLine($"Web version: {version.WebVersion}; DB version: {version.DatabaseVersion}; DB server: {version.DatabaseServer}");

        }

        private void ForgotPasswordInternal(User user)
        {
            ForGotPasswordLink();
            _container.WaitForElement(By.Id("ResetPasswordUsername"), "Username box").Text = user.UserName;

            _container.WaitForElement(By.Id("ResetPasswordEmail"), "E-mail box").Text = user.Email;

            var btnLogin = _container.WaitForElement(By.Id("ResetPasswordOKBtn"), "OK button");

            btnLogin.Click();

            btnLogin.WaitToDisappear();
        }

        public void Login()
        {
            LoginInternal(_user, _server);
        }

        public void ForGotPassword(User user)
        {
            ForgotPasswordInternal(user);
        }

        public void Login(User user, string server = "")
        {
            LoginInternal(user, server);
        }

        private void ValidateServer(string serverValue)
        {
            serverValue = (serverValue == string.Empty) ? _server : serverValue;
            OpenLoginOptions();

            var select = _container.WaitForElement(By.Id("Login1_DataSourceDDL"), "Server combobox");
            Wait.Until(() => select.Displayed, "Server combobox is not displayed");

            if (select.WaitGetAttribute("disabled", failIfNoValue: false) != "true")
                select.SelectFromComboboxByValueWithStale(serverValue);
        }

        private void ForGotPasswordLink()
        {
            var forGotPasswordLink = _container.WaitForElement(By.Id("Login1_ForgotPasswordLinkBtn"),"ForGotPassword Link");

            forGotPasswordLink.WaitToBeClickable();
            forGotPasswordLink.Click();
            forGotPasswordLink.WaitToDisappear();
        }

        private bool OpenLoginOptions()
        {
            if (IsLoginOptionOpen() == false)
            {
                var button = _container.WaitForElement(By.Id("Login1_OptionsLinkBtn"), "Toggle login options");
                button.MoveCursorToElement();
                button.ClickSafely();
            }

            if (IsLoginOptionOpen() == false)

                Wait.Until(() => OpenLoginOptions());

            return IsLoginOptionOpen();
        }

        private bool IsLoginOptionOpen()
        {
            var loginOptionsState = _container.WaitForElement(By.Id("Login1_OptionsRow"), "Login options").WaitGetAttribute("style", failIfNoValue: false);
            if (loginOptionsState != "") //login options is not visible
                return false;
            return true;
        }
    }
}
