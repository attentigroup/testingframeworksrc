﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class CommunicationServersPage : WOMPage
    {
        public CommunicationServersPage(Browser browser):base(browser)
        {

        }

        internal CommunicationServersTablePage CommunicationServersTable => new CommunicationServersTablePage(_browser);

        internal class CommunicationServersTablePage : WOMTableBase
        {
            public CommunicationServersTablePage(Browser browser):base(browser)
            {
            }

            protected override By GetContentContainerLocator()
            {
                return By.Id("divBodyContainer");
            }

            public AddServerPopupPage AddServer()
            {
                var addServerLink = _browser.WaitForElement(By.Id("commServersAddServer"), "Add server link");

                addServerLink.Click();

                return new AddServerPopupPage(_browser);
            }

            internal SecurityConfigObject GetSecurityResult( string APIString)
            {
                return base.GetSecurityResult(APIString);
            }
        }
    }

    internal class AddServerPopupPage
    {
        private Browser _browser;

        public AddServerPopupPage(Browser browser)
        {
            _browser = browser;
        }

        //TODO fill methods to add server
    }
}
