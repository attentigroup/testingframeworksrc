﻿using System;
using OpenQA.Selenium;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class TrackerRulesPage : WOMPage
    {
        public TrackerRulesPage(Browser browser) : base(browser)
        {

        }

        internal TrackerRulesTablePage TrackerRulesTable => new TrackerRulesTablePage(_browser);
        internal SidePanelPage SidePanel => new SidePanelPage(_browser);

        public SecurityConfigObject Navigate2Section(string aPIString, EnmModifyOption modifyOption)
        {
            if (aPIString.EndsWith("_DisplayMessage") || aPIString.EndsWith("_GracePeriod") || aPIString.EndsWith("_ImmediateUpload"))
                TrackerRulesTable.SelectProgramType("TwoPiece");
            else
                TrackerRulesTable.SelectProgramType("OnePiece");


            return SidePanel.GetIsElementVisibleEnabledExposed(aPIString, modifyOption);
        }

        internal class TrackerRulesTablePage : WOMTableBase
        {

            public TrackerRulesTablePage(Browser browser) : base(browser)
            {
            }
            
            protected override By GetContentContainerLocator()
            {
                return By.Id("TrackingHolder");
            }

            public void SelectProgramType(string optionValue)
            {
                var combobox = _browser.WaitForElement(By.Id("programTypeBox"), "Program type dropdown list");

                combobox.SelectFromComboboxByValue(optionValue);
            }

            internal SecurityConfigObject GetSecurityResult(string APIString)
            {
                return base.GetSecurityResult(APIString);
            }
        }

        internal class SidePanelPage : WOMPageCommon
        {
            
            private readonly BrowserElement _container;
            public SidePanelPage(Browser browser):base(browser)
            {
                _container = _browser.WaitForElement(By.Id("sidePanel"), "Side panel");
            }

            internal SecurityConfigObject GetIsElementVisibleEnabledExposed(string aPIString, EnmModifyOption modifyOption)
            {
                return GetSecurityResult(aPIString, modifyOption);
            }
        }
    }
}