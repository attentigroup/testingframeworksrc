﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public abstract class PopupPageBase : WOMPageCommon
    {
        protected virtual By PopupContainerLocator => By.Id("PopupContainer");
        protected BrowserElement _container => _browser.WaitForElement(PopupContainerLocator, "Popup container");

        public PopupPageBase(Browser browser):base(browser)
        {
            Wait.Until(() => _container.Displayed);
        }

        protected BrowserElement GetButtonByIndex (int index)
        {
            var buttons = GetAllEnabledButtons();

            if (buttons.Any())
                return buttons[index];

            return null;
        }

        private List<BrowserElement> GetAllEnabledButtons()
        {
            var buttonsList = _container.WaitForElements(By.ClassName("buttons"), "Popup button", 2);

            return buttonsList.Where(x => x.Enabled).ToList();
        }
    }
}
