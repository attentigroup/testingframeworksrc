﻿using OpenQA.Selenium;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class OpenPanicAlarmPage : WOMPage
    {
        public OpenPanicAlarmPage(Browser browser) : base(browser)
        {

        }

        internal OpenPanicAlarmGeneralActionsMenuPage OpenPanicAlarmGeneralActionsMenu => new OpenPanicAlarmGeneralActionsMenuPage(_browser);

        internal class OpenPanicAlarmGeneralActionsMenuPage : GeneralActionsMenuBasePage
        {
            public OpenPanicAlarmGeneralActionsMenuPage(Browser browser) : base(browser)
            {

            }
            protected override By GetSaveButtonSelector()
            {
                throw new System.NotImplementedException();
            }
        }
    }
}