﻿using OpenQA.Selenium;
using WebTests.Common;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.Pages
{
    public class SecurityConfigurationPage : WOMPage
    {
        public SecurityConfigurationPage(Browser browser):base(browser)
        {

        }

        internal SecurityConfigurationTablePage SecurityConfigurationTable => new SecurityConfigurationTablePage(_browser);
        internal SecurityConfigurationFilterSectionPage SecurityConfigurationFilterSection => new SecurityConfigurationFilterSectionPage(_browser);

        internal class SecurityConfigurationTablePage : WOMTableBase
        {
            public SecurityConfigurationTablePage(Browser browser):base(browser)
            {
            }

            protected override By GetContentContainerLocator()
            {
                throw new System.NotImplementedException();
            }

            //TODO: add select Screen Name and Section

            //TODO: add Hide/Show slider checking
        }

        internal class SecurityConfigurationFilterSectionPage : FilterSectionBase
        {

            public SecurityConfigurationFilterSectionPage(Browser browser):base(browser)
            {
            }

            protected override By GetFilterContainerSelector()
            {
                return By.Id("sidePanel");
            }

            public void SelectUserFromDropdownListByValue(string value)
            {
                var combobox = PanelContainer.WaitForElement(By.Id("groupsSelectList"), "User dropdown list");

                combobox.SelectFromComboboxByValue(value);
            }
        }
    }
}