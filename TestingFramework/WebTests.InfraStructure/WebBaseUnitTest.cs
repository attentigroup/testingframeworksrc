﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;

namespace WebTests.InfraStructure
{
    [TestClass]
    public class WebBaseUnitTest : BaseUnitTest
    {
        [TestCleanup]
        public void TestCleanup()
        {
            base.TestCleanup();

            if (WebApplication.IsInstantiated)
            {
                SeleniumWrapper.Logger.WriteLine("===========*** Test Cleanup done ***===========");
                WebApplication.Instance.Dispose();
            }
        }
    }
}
