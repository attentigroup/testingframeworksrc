﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure
{
    public class WebTestingException : Exception
    {
        public WebTestingException()
        {

        }

        public WebTestingException(string message):base(message)
        {

        }

        public WebTestingException(string message, Exception inner) :  base (message, inner)
        {

        }
    }
}
