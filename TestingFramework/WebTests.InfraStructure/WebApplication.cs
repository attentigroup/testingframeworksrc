﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using TP.AutomationTests.Infra.Common;
using WebTests.Common.Browsers;
using WebTests.InfraStructure.Pages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure
{
    public class WebApplication : IDisposable
    {
        private const int defaultTimeout = 30;
        private static WebApplication instance;
        private static bool isInstantiated;
        public Browser Browser { get; set; }
        private List<Browser> BrowserList = new List<Browser>();
        private ExtentReportsLogger extentLogger;

        //*******************************
        //*Take from config             *
        private string browserNameFromConfig = ConfigurationManager.AppSettings["BrowserName"]; /*BrowserName.Chrome.ToString();*/ /*BrowserName.IE.ToString();*/ /*BrowserName.Firefox.ToString();*/
        private string _url = ConfigurationManager.AppSettings["WebUrl"];//"https://10.10.8.124:8020/"; "https://31.154.54.103";
        //*Take from config             *
        //*******************************

        private WebApplication()
        {
            var testName = GetTestName();
            extentLogger = new ExtentReportsLogger(testName);
            
            Browser = GenerateBrowser(_url);

            Logger.Initialize(l => extentLogger.Info(l));
            Logger.InitializeFail(l => extentLogger.Failed(l));
            Logger.InitializeWarning(l => extentLogger.Warning(l));
            Logger.InitializeTakeScreenshot(() => extentLogger.TakeScreeShot(Browser.GetWebDriver()));
            Logger.InitializeSysInfo((p, v) => extentLogger.SysInfo(p, v));

            IsInstantiated = true;
        }

        private string GetTestName()
        {
            var stackTrace = new StackTrace();
            foreach (var stackFrame in stackTrace.GetFrames())
            {
                MethodBase methodBase = stackFrame.GetMethod();
                Object[] attributes = methodBase.GetCustomAttributes(
                                          typeof(TestMethodAttribute), false);
                if (attributes.Length >= 1)
                {
                    return methodBase.Name;
                }
            }
            return "Not called from a test method";
        }

        /// <summary>
        /// For default URL enter null
        /// </summary>
        /// <param name="url"></param>
        public Browser GenerateBrowser(string url)
        {
            BrowserName browserName = (BrowserName)Enum.Parse(typeof(BrowserName), browserNameFromConfig);

            url = url ?? _url;

            var browserInitializer = new BrowserFactory(browserName).GetInitializeBrowser();

            browserInitializer.NavigateToUrl(url);

            var generatedBrowser = new Browser(browserInitializer.GetWebDriver(), browserInitializer.Name, defaultTimeout);

            extentLogger.Info($"Created Browser: {generatedBrowser.BrowserName}");

            BrowserList.Add(generatedBrowser);

            return generatedBrowser;
        }

        public void SwitchToBrowser(Browser browser)
        {
            Browser = browser;
            Logger.WriteLine($"Browser switched. URL = {Browser.Url}");
        }

        public void SwitchToOriginalBrowser()
        {
            if (BrowserList.Any())
                Browser = BrowserList.First();
        }

        public static WebApplication Instance
        {
            get
            {
                return instance ?? (instance = new WebApplication());
            }
        }

        public static bool IsInstantiated
        {
            get { return isInstantiated; }
            set { isInstantiated = value; }
        }

        public void Dispose()
        {
            Browser.Dispose();
            CloseBrowsers();
            instance = null;
            isInstantiated = false;
            extentLogger.Dispose();
            Logger.Dispose();
        }

        public Browser GetBrowser()
        {
            return Browser;
        }

        public void CloseBrowsers()
        {
            foreach (var browser in BrowserList)
            {
                browser.Quit();
            }
        }

        public WOMPage WOMPage { get { return new WOMPage(Browser, false); } }

        public LoginPage LoginPage
        {
            get { return new LoginPage(Browser); }
        }
    }

}
