﻿using System;
using WebTests.InfraStructure.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Pages;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class UserMenuTestLogic : WebTestLogicBase
    {
        public void Logout()
        {
            webApp.WOMPage.UserMenu().Logout();
        }

        //TODO:move to WOMPageTestLogic
        public bool IsDVPairsVisible()
        {
            return webApp.WOMPage.IsDVPairsMenuVisible();
        }

        //TODO:move to WOMPageTestLogic
        public bool IsAddNewOffenderVisible()
        {
            return webApp.WOMPage.IsAddNewOffenderVisible();
        }


        public bool IsChangePasswordVisible()
        {
           return webApp.WOMPage.IsChangePasswordVisible();
            
        }

        public void ChangePassword(User user)
        {
          webApp.WOMPage.UserMenu().ChangeUserPassword(user);
        }

        public bool IsDataBaseVisible()
        {
            return webApp.WOMPage.IsDataBaseVisible();
        }

        public void ClearCache()
        {
            webApp.WOMPage.ClearCache();
        }

        public bool isLogoutVisible()
        {
            return webApp.WOMPage.IsLogoutVisible();
        }
    }
}
