﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages.MonitorPages;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class MonitorScreenTestLogic : WebTestLogicBase
    {

        private MonitorPage MonitorPage => webApp.WOMPage.Monitor();
        public bool IsFilterSectionVisible(User user)
        {
            var monitorPage = webApp.WOMPage.Monitor();

            return monitorPage.IsFilterSectionVisible();
        }

        public bool IsEventListGridHeaderExist(User user)
        {
            return MonitorPage.MonitorTable.IsTableListGridHeaderExist();
        }

        public void SelectEventByMessageAndOpenHandlingOptionMenu(string message, bool Prepration)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().FilterEventsByPeriod("24");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.SelectEventByMessageAndOpenHandlingOptionMenu(message);
        }

        public void FilterByTimeSelectEventAndExpand(string eventID, string recent)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.FilterByTimeAndExpandTableRow(eventID, recent);
        }

        public void FilterByTimeSelectScheduleTaskEventAndExpand(string recent)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.FilterByTimeSelectScheduleTaskEventAndExpand(recent);

        }

        public AddNewOffenderTestLogic NavigateToOffenderThroughEventRow(int sectionNumber, string title, string eventID)
        {
            MonitorPage.NavigateToOffenderThroughEventRow(sectionNumber, title, eventID);
            return AddNewOffenderTestLogic_NoClick;
        }

        public void FilterByTimeSelectEventAndOpenHandlingOptionMenu(string eventID, string recent)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.FilterByTimeAndExpandTableRow(eventID, recent);
            webApp.WOMPage.Monitor_NoClick().MonitorTable.OpenHandlingOptionMenu();
        }
        public string GetFilterTitle()
        {
            return MonitorPage.GetFilterTitle();
        }
        public void SelectEventAndOpenHandlingOptionMenu(string eventID)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.ExpandTableRow(eventID);
            webApp.WOMPage.Monitor_NoClick().MonitorTable.OpenHandlingOptionMenu();
        }

        public void SelectEventAndPressOnLinkedEvents(string eventID)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.ExpandTableRow(eventID);
            webApp.WOMPage.Monitor_NoClick().MonitorTable.PressLinkedEvents();
        }

        public void MoveCursorToQuickHandle(string eventID)
        {
            MonitorPage.MonitorTable.MoveCursorToQuickHandle(eventID);
        }

        public void HandleEvent(string eventID, string HandlingOption)
        {
            //TODO: move Expand inside HandleEvent
            MonitorPage.MonitorTable.ExpandTableRow(eventID);
            webApp.WOMPage.Monitor_NoClick().MonitorTable.HandleEvent(HandlingOption);

        }

        public SecurityConfigObject GetSecurityResultFromEventExpandConteiner(string resourceID)
        {
            return webApp.WOMPage.Monitor_NoClick().GetSecurityResultFromEventExpandConteiner(resourceID);   
            
        }

        public void SelectEventAndExpandTableRow(string eventID)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            FilterEventsByPeriod("72");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.ExpandTableRow(eventID);
        }

        public void SelectEventAndPressOpenRecentEvent(string eventID)
        {
            MonitorPage.SelectMonitorFilterElement(1, "All");
            webApp.WOMPage.Monitor_NoClick().MonitorTable.ExpandTableRow(eventID);
            webApp.WOMPage.Monitor_NoClick().MonitorTable.PressOpenRecentEvents();
        }

        public List<ValueTuple<int,int>> ShowResults(int resultsNumberToShow)
        {
            return MonitorPage.ShowResults(resultsNumberToShow);
        }

        public bool PagingAllPages()
        {
            return MonitorPage.ClickOnPagerButtons();
        }

        public string[] GetSystemFilters()
        {
            return MonitorPage.GetSystemFilters();
        }

        public string GetAmountOfRowsByFilter(int sectionNumber, string title)
        {
            return MonitorPage.GetAmountOfElementsInSpecificFilter(sectionNumber, title);

        }

        public string[] GetFilterEventsArray()
        {
            return webApp.WOMPage.Monitor_NoClick().MonitorTable.GetFilterEventsArray();
        }

        public void DeleteUserFilter(string userFilterName)
        {
            webApp.WOMPage.Monitor_NoClick().DeleteUserFilter(userFilterName);
        }

        public void FilterEventsByRangeOfDates(DateTime startTime, DateTime endTime)
        {
            webApp.WOMPage.Monitor().FilterEventByRangeOfDates(startTime, endTime);
        }

        public void FilterEventsByPeriod(string recent)
        {
            webApp.WOMPage.Monitor().FilterEventsByPeriod(recent);
        }

        public void CreateProgramTypeUserFilter(string[] ProgramTypes, string filterName)
        {
            webApp.WOMPage.Monitor().CreateProgramTypeUserFilter(ProgramTypes,filterName);
        }

        public string CreateAgencyIdUserFilter(string agencyId)
        {
            return webApp.WOMPage.Monitor().CreateAgencyIdUserFilter(agencyId);
        }

        public void CreateOffenderNameUserFilter(string offenderName, string filterName)
        {
           webApp.WOMPage.Monitor().CreateOffenderNameUserFilter(offenderName, filterName);
        }

        public void CreateOfficerNameUserFilter(string officerName, string filterName)
        {
            webApp.WOMPage.Monitor().CreateOfficerNameUserFilter(officerName, filterName);
        }
      
        public void HandleScheduleTaskEvent(string eventID, DateTime time)
        {

            webApp.WOMPage.Monitor().MonitorTable.ExpandTableRow(eventID);
            webApp.WOMPage.Monitor_NoClick().MonitorTable.HandleEventScheduleTask(time);
        }

        public MonitorTableRow SearchEventByEventId(int EventId)
        {
            string GetEventId = Convert.ToString(EventId);

            var monitorTableRow = MonitorPage.GetEventDetails(GetEventId);
            return monitorTableRow;
        }
        public void HandleEventQuickHandling(string eventID)
        {
            MonitorPage.MonitorTable.HandleEventQuickHandling(eventID);
        }

        public MonitorTableRow GetScheduleTaskEvent(DateTime time, string ReceiverSN, bool clearSearch = true)
        {
            return webApp.WOMPage.Monitor_NoClick().MonitorTable.GetScheduleTaskEvent(time, ReceiverSN, clearSearch);
        }

        public string GetEventStatus()
        {
           return webApp.WOMPage.Monitor_NoClick().MonitorTable.GetEventStatus();
        }

        public MonitorTableRow[] GetEventsDetailsArr(int sectionNumber, string title, int eventDetailsArrLength)
        {
            return MonitorPage.GetEventsDetails(sectionNumber, title, eventDetailsArrLength);
        }
        public MonitorTableRow[] GetLinkedEventsDetailsArr(String eventID, int ArrayLength)
        {
            return MonitorPage.GetLinkedEventsDetails(ArrayLength, eventID);
        }

    }
}
