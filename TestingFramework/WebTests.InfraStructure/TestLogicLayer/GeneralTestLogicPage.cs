﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.TestLogicLayer
{
   public class GeneralTestLogicPage : WebTestLogicBase
    {
        public void RefreshPage()
        {
            webApp.Browser.RefreshPage();
        }
    }
}
