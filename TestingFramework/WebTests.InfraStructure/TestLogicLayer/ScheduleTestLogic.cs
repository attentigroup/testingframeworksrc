﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class ScheduleTestLogic : WebTestLogicBase
    {
        public void AddSchedule(string offenderRefId, TimeFrame timeFrame)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).AddSchedule(timeFrame);
        }

        public void AddTimeframe(TimeFrame timeFrame)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).AddSchedule(timeFrame);
        }

        public bool IsScheduleContainsTimeFrames()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false).IsScheduleContainsTimeFrame();
        }

        public void UpdateTimeFrame(string offenderRefId, TimeFrame timeFrame, string timeFrameID, string ProgramType)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).UpdateTimeFrame(timeFrame, timeFrameID, ProgramType);
        }

        public bool IsTimeframeDisplayedInSchedule(string searchTerm, string timeframeID)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(searchTerm, -1);
            return webApp.WOMPage.Offenders_AddNewOffender(false).IsTimeframeDisplayedInSchedule(timeframeID);
        }

        public bool IsGroupTimeframeExist(string timeFrameName)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).IsGroupTimeframeExist(timeFrameName);
        }

        public void DeleteTimeFrame(string offenderRefId, string timeFrameID, bool isE4WeeklyTimeframe)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).DeleteTimeFrame(timeFrameID, isE4WeeklyTimeframe);
        }
    }
}
