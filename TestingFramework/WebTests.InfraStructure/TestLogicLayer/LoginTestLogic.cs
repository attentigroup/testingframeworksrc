﻿using WebTests.InfraStructure.Entities;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class LoginTestLogic :WebTestLogicBase
    {
        /// <summary>
        /// Enter null for default user
        /// </summary>
        /// <param name="user"></param>
        public void Login(User user)
        {
            if (user.UserName != null && user.Password != null)
                webApp.LoginPage.Login(user);
            else
                webApp.LoginPage.Login();
        }

        public void LoginForgotPassword(User user)
        {
            webApp.LoginPage.ForGotPassword(user);
        }
        
    }
}
