﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;
using WebTests.InfraStructure.Pages.OffenderPages;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class AddNewOffenderTestLogic : WebTestLogicBase
    {
        private bool _doClick = true;
        public AddNewOffenderTestLogic()
        { }

        public SecurityConfigObject GetSecurityResultSuspendProgram()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false).GetSecurityResultSuspendProgram();
        }

        public int GetCurrentStatusFilterCounter(string filterTitle)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetCurrentStatusFilterCounter(filterTitle);
        }

        public void DeleteAllWeeklyTimeframes()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).DeleteAllWeeklyTimeframes();
        }

        public void CurrentStatusExpandEventById(int eventId)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false,false).CurrentStatusExpandEventById(eventId);
        }

        public void DeleteZone(string zoneID)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).DeleteZone(zoneID);
        }

        public bool GetCustomFieldsVisibilityFromOffenderDetails(string resourceID)
        {

            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetCustomFieldsVisibilityFromOffenderDetails(resourceID);
        }

        public Dictionary<string, bool> GetCustomFieldsListValuesVisibilityFromOffenderDetails(string resourceID, List<string> values)
        {

            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetCustomFieldsListValuesVisibilityFromOffenderDetails(resourceID, values);
        }

        public void InsertDataToTextCustomField(string resourceID, string text)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).InsertDataToTextCustomField(resourceID, text);
        }

        public void InsertDataToNumericCustomField(string resourceID, string text)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).InsertDataToNumericCustomField(resourceID, text);
        }

        public void InsertDataToDateTimeCustomField(string resourceID, string text)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).InsertDataToDateTimeCustomField(resourceID, text);
        }

        public void InsertDataToListCustomField(string resourceID, string text)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).InsertDataToListCustomField(resourceID, text);
        }

        public void SaveForUpdateOffender()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).SaveForUpdateOffender();
        }

        AddNewOffenderPage AddNewOffenderPage => webApp.WOMPage.Offenders_AddNewOffender(_doClick);

        OperationalReportsPage OperationalReportsPage => webApp.WOMPage.Reports_OperationalReports();

        public AddNewOffenderTestLogic(bool doClick)
        {
            _doClick = doClick;
        }

        public AddNewOffenderPage AddNewOffenderPage_NoCLick()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false);
        }

        public string[] GetSystemFilters()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false).GetSystemFilters();
        }

        public void ReallocateOffender()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false,false).ReallocateOffender();
        }

        public void OpenPointInformationTooltip()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).OpenPointInformationTooltip();
        }


        public void OpenAddPointsOfInterestPopup()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).OpenAddPointsOfInterestPopup();
        }

        public string GetAmountOfRowsForSpecificSystemFilter(int sectionNumber, string title)
        {
           return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetAmountOfRowsForSpecificSystemFilter(sectionNumber, title);
        }

        public void OpenAddZoneMenu()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).OpenAddZoneMenu();
        }

        public CurrentStatusTableRow[] GetEventsDetailsFromSpecificSystemFilter(int sectionNumber, string title)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetEventsDetailsFromSpecificSystemFilter(sectionNumber, title);
        }

        public void OpenEditCircularZonePopup()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).OpenEditCircularZonePopup();
        }

        public void SecurityConfigOffender_Picture()
        {
            var popup = webApp.WOMPage.Offenders_AddNewOffender(false, false).OpenPhtoPopup();
        }

        public void SecurityConfigOffender_Download()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).OffenderDownload();
        }

        public SecurityConfigObject GetSecurityResultFromZonesListRow(string resourceID)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false,false).GetSecurityResultFromZonesListRow(resourceID);
        }

        public void OpenZoneTooltip()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).OpenZoneTooltip();
        }

        public void SecurityConfigOffender_OffenderActions()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).OffenderActions();
        }

        public List<(int, int)> GetShownResultsPerPage(int showResultsNumber)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetCurrentStatusNumberOfShownResultsPerPage(showResultsNumber);
        }

        public bool ClickOnPagerButtons()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).ClickOnPagerButtons();
        }

        public SecurityConfigObject GetSecurityResultFromCaseManagmentTableRow(string primaryTabLocatorString, string secondaryTabLocatorString, string filterName, string resourceID)
        {
           return  webApp.WOMPage.Offenders_AddNewOffender(false).GetSecurityResultFromCaseManagmentTableRow(primaryTabLocatorString, secondaryTabLocatorString, filterName, resourceID);
        }

        public void SecurityConfigOffenderStatus()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).SecurityConfigOffenderStatus();
        }
        public void NavigateToTab(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).NavigateToTab(primaryTabLocatorString, secondaryTabLocatorString);
        }

        public void DesplayVoiceSimDataTooltip(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).DesplayVoiceSimDataTooltip(primaryTabLocatorString, secondaryTabLocatorString);
        }

        public void OpenCellularDataPopup(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).OpenCellularDataPopup(primaryTabLocatorString, secondaryTabLocatorString);
        }

        public SecurityConfigObject GetSecurityResultFromOffenderDetailsRow(string primaryTabLocatorString, string secondaryTabLocatorString, string resourceID)
        {
           return webApp.WOMPage.Offenders_AddNewOffender(false).GetSecurityResultFromOffenderDetailsRow(primaryTabLocatorString, secondaryTabLocatorString, resourceID);
        }
        

        public void PressEditAddress(string primaryTabLocatorString, string secondaryTabLocatorString)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).ClickOnAddPhone(primaryTabLocatorString, secondaryTabLocatorString);
        }

        public void SecurityConfigOffender_Actions()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).SecurityConfigOffender_Actions();
        }

        public CurrentStatusTableRow GetManualEvent()
        {
            return AddNewOffenderPage_NoCLick().GetManualEvent();
        }

        public string GetFirstEventMessage()
        {
            return AddNewOffenderPage_NoCLick().GetFirstEventMessage();
        }

        public string GetEventStatus(string eventID)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetEventStatus(eventID);
        }

        public string GetEventMessage(string eventMessage)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetEventMessage(eventMessage);
        }

        

        public void CreateEventTimeUserFilter(DateTime eventTime)
        {
            AddNewOffenderPage_NoCLick().CreateEventTimeUserFilter(eventTime);
        }

        public void CreateSeverityUserFilter(string severity)
        {
            AddNewOffenderPage_NoCLick().CreateSeverityUserFilter(severity);
        }

        public void CreateEventMessageUserFilter(string eventMessage)
        {
            AddNewOffenderPage_NoCLick().CreateEventMessageUserFilter(eventMessage);
        }

        public void DeleteUserFilter(string filterName)
        {
            AddNewOffenderPage_NoCLick().DeleteUserFilter(filterName);
        }

        public void CreateEventIDUserFilter(string eventID)
        {
            AddNewOffenderPage_NoCLick().CreateEventIDUserFilter(eventID);
        }

        public void HandleEvent(string eventId, string handlingOption)
        {
            webApp.WOMPage.Offenders_AddNewOffender(false, false).HandleEvent(eventId, handlingOption);
        }

        public OffenderDetails AddNewOffenderExtended(OffenderDetails OffenderDetails)
        {
            return AddNewOffenderPage.FillDetailsFormExtended(OffenderDetails);
        }

        public string GetOffenderDetailsLabel()
        {
            return AddNewOffenderPage.GetOffenderDetailsLabel();
        }

        public bool VerifyDVLink()
        {
            return AddNewOffenderPage.VerifyDVLink();
        }

        public bool IsPictureDisplayed()
        {
            return AddNewOffenderPage.isPictureDisplayed();
        }

        public string GetOffenderTransmitter()
        {
            return AddNewOffenderPage.GetOffenderTransmitter();
        }

        public string GetOffenderContactInformation(int index)
        {
            return AddNewOffenderPage.GetOffenderContactDetails(index).Text;
        }

        public string GetOffenderStatus()
        {
            return AddNewOffenderPage.GetOffenderStatus();
        }

        public void AddNewOffenderFull(OffenderDetails OffenderDetails)
        {
            AddNewOffenderPage.FillWizardAndDownload(OffenderDetails);
        }

        public void CreateManualEvent()
        {
            AddNewOffenderPage_NoCLick().CreateManualEvent();
        }

        public void FilterCurrentStatusEventsByRangeOfDates(DateTime startTime, DateTime endTime)
        {
            AddNewOffenderPage_NoCLick().FilterCurrentStatusEventsByRangeOfDates(startTime, endTime);
        }

        public void FilterCurrentStatusEventsByTimePeriod(string recent)
        {
            AddNewOffenderPage_NoCLick().FilterCurrentStatusEventsByPeriod(recent);
        }

        public string GetVoiceSimData()
        {
            return AddNewOffenderPage_NoCLick().GetVoiceSimData();
        }

        public OffenderDetails AddNewOffenderBasic(OffenderDetails OffenderDetails)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(true).FillDetailsFormBasic(OffenderDetails);
        }

        public void SendDownloadRequest()
        {
            webApp.WOMPage.Offenders_AddNewOffender(false).PressDownloadNow();
        }

        public void SendEOS(bool noReceiver, bool isAutoEOS, bool isSuccessful, bool isCurfewViolations)
        {
            AddNewOffenderPage_NoCLick().ClickSendEOS(noReceiver, isAutoEOS, isSuccessful, isCurfewViolations);
        }

        public void SendUpload()
        {
            AddNewOffenderPage_NoCLick().ClickSendUpload();
        }

        public void SendSuspendProgram()
        {
            AddNewOffenderPage_NoCLick().ClickSendSuspendProgram();
        }

        public void SendUnsuspendProgram()
        {
            AddNewOffenderPage_NoCLick().ClickSendUnsuspendProgram();
        }

        public OffenderLocation GetOffenderLocationTrailData()
        {
            return AddNewOffenderPage.GetOffenderLocationTrailData();
        }

        public void LocationPlayTrail()
        {
            AddNewOffenderPage.LocationPlayTrail();
        }

        public TrailReport GetLocationTrailReport()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetTrailReport();
        }

        public void ClickPrint()
        {
            //webApp.WOMPage.Offenders_AddNewOffender(false, false).ClickPrint();            
        }

        public CurrentStatusTableRow SearchEventByEventId(bool navToCurrentStatus, int EventId, string RefId, string equipmentSerialNumber = null)
        {
            string GetEventId = Convert.ToString(EventId);

            if (!navToCurrentStatus)
                webApp.WOMPage.Offenders_OffendersList().SearchOffender(RefId, -1, equipmentSerialNumber);

            var currentStatusTableRow = webApp.WOMPage.Offenders_AddNewOffender(false).GetCurrentStatusTableRowByEventId(GetEventId);

            return currentStatusTableRow;
        }
        public List<(int, int)> GetCurrentStatusShownResultsPerPage(int showResultsNumber)
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false).GetCurrentStatusNumberOfShownResultsPerPage(showResultsNumber);
        }


        public void MoveToConfigurationTab()
        {
            //ConfigurationTabPage
            AddNewOffenderPage_NoCLick().MoveToConfigurationTab();
        }

        public void UpdateProgramParametersValues(List<ConfigurationElement> list)
        {
            AddNewOffenderPage_NoCLick().UpdateProgramParametersValues(list);
        }

        public void MoveToHandlingProceduresTab()
        {
            //ConfigurationTabPage
            AddNewOffenderPage_NoCLick().MoveToHandlingProceduresTab();
        }

        public void MoveToTrackerRulesTab()
        {
            //ConfigurationTabPage
            AddNewOffenderPage_NoCLick().MoveToTrackerRulesTab();
        }

        

    }
}
