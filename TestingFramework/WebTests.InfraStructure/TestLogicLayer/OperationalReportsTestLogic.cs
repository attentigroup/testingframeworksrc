﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class OperationalReportsTestLogic : WebTestLogicBase
    {

        public void SelectOperationalReports(string reportName)
        {
            var reportPage = webApp.WOMPage.Reports_OperationalReports();
             reportPage.GenerateReport(reportName);
        }
    }
}
