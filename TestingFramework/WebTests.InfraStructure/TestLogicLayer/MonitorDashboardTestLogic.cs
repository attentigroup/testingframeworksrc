﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Pages.MonitorPages;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class MonitorDashboardTestLogic : WebTestLogicBase
    {
        DashboardPage DashboardPage => webApp.WOMPage.Dashboard();

        public void GetTableRows()
        {
            DashboardPage.GetTableRows();
        }
    }
}
