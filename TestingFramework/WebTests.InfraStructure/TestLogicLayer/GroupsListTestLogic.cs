﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class GroupsListTestLogic : WebTestLogicBase
    {
        public void SelectGroup()
        {
            webApp.WOMPage.Offenders_GroupsList().SelectGroup();
        }
    }
}
