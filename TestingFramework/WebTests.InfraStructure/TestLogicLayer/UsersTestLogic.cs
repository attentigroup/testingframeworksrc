﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Pages;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class UsersTestLogic : WebTestLogicBase
    {
        private UsersListPage UsersListPage => webApp.WOMPage.System_Users_UsersList();
        private UsersListPage UsersListPageNoClick => webApp.WOMPage.System_SystemConfiguration_UsersList_NoClick();

        public void SelectUserByTerm(string AccountChangeStatus)

        {
            if (AccountChangeStatus == "Disable")
                UsersListPage.SelectFirstUser("Active");                       
            else
                UsersListPage.SelectFirstUser("Not Active");

            UsersListPageNoClick.OpenActionOptionsMenu();
        }

        public void SearchUser(string filterTitle)
        {
            UsersListPage.SelectFirstUser(filterTitle);
        }

        public void CleanUp()
        {
            UsersListPage.SelectFirstUser("Not Active");
            UsersListPageNoClick.OpenActionOptionsMenu();
        }
    }

}
