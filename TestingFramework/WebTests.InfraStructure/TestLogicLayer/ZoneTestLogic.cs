﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class ZoneTestLogic : WebTestLogicBase
    {
        public void AddCircularZone(string offenderRefId, string name, string limitation, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).AddCircularZone(name, limitation, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
        }

        public bool IsMapContainsZones()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).IsMapContainsZone();
        }

        public bool IsZonesListContainsZones(string offenderRefId, int zoneID)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            return webApp.WOMPage.Offenders_AddNewOffender(false).IsZonesListContainsZones(zoneID);
        }

        public bool IsZoneDisplayedInMap(string offenderRefId, int zoneID)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            return webApp.WOMPage.Offenders_AddNewOffender(false).IsZoneDisplayedInMap(zoneID);
        }

        public void AddPolygonZone(string offenderRefId, string name, string limitation, int zoneGraceTime, bool isFullSchedule)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).AddPolygonZone(name, limitation, zoneGraceTime, isFullSchedule);
        }

        public void UpdateCircularZone(string offenderRefId, string name, int zoneGraceTime, bool isFullSchedule, int realRadious, int bufferRadious)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).UpdateCircularZone(name, zoneGraceTime, isFullSchedule, realRadious, bufferRadious);
        }

        public void UpdatePolygonZone(string offenderRefId, string name, int zoneGraceTime, bool isFullSchedule)
        {
            var offenderListPage = webApp.WOMPage.Offenders_OffendersList();
            offenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).UpdatePolygonZone(name, zoneGraceTime, isFullSchedule);
        }
    }
}
