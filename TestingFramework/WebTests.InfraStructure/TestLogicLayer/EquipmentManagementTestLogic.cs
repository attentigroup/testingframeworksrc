﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class EquipmentManagementTestLogic : WebTestLogicBase
    {

        public Equipment AddEquipment(string fileName, string equipmentSN)
        {
            var equipmentManagementPage = webApp.WOMPage.System_EquipmentManagement();

            var equipment = equipmentManagementPage.AddEquipment(fileName);
            equipment.Agency = AssignAgency(equipmentSN);
            AddCellularData(equipmentSN);

            return equipment;
        }

        public void SelectEquipmentAndOpenVoiceSimDataPopup(string equipmentSN)
        {
            webApp.WOMPage.System_EquipmentManagement().SelectEquipmentAndOpenVoiceSimDataPopup(equipmentSN);
        }

        public void AddEquipments(string fileName, string[] receiver, string[] transmitter)
        {
            var equipmentManagementPage = webApp.WOMPage.System_EquipmentManagement();

            equipmentManagementPage.AddEquipments(fileName);
            Thread.Sleep(2000);

            if (receiver != null)
            {
                foreach (var equip in receiver)
                {
                    AssignAgency(equip);
                    AddCellularData(equip);

                }
            }
            if (transmitter != null)
            {
                foreach (var equip in transmitter)
                {
                    AssignAgency(equip);
                }
            }

        }

        private void AddCellularData(string equipmentSN)
        {
            webApp.WOMPage.EquipmentManagementPage_NoClick().AddCellularData(equipmentSN);
        }

        public Equipment GetEquipmentDetailsFromTable(string equipmentSN)
        {
           return webApp.WOMPage.System_EquipmentManagement().GetEquipmentDetails(equipmentSN);
        }

        public void DeleteEquipment(string equipmentSN)
        {
            webApp.WOMPage.System_EquipmentManagement().DeleteEquipment(equipmentSN);
        }

        public string AssignAgency(string equipmentSN)
        {
            return webApp.WOMPage.EquipmentManagementPage_NoClick().AssignAgency(equipmentSN);
        }

        public Equipment GetEquipmentRow()
        {
            var equip = webApp.WOMPage.EquipmentManagementPage_NoClick().GetTableRowDataUsingTableHeader();
            return equip as Equipment;
        }
    }
}
