﻿using WebTests.InfraStructure.Pages.OffenderPages;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;
using static WebTests.InfraStructure.Pages.OffenderPages.OffenderGroupPage;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class GroupPageTestLogic : WebTestLogicBase
    {
        public void GotoFirstOffenderInFirstGroup()
        {
            OffenderGroupPage offenderGroupPage = null;

            var rowNumber = 1;
            while (offenderGroupPage == null)
            {
                var groupListPage = webApp.WOMPage.Offenders_GroupsList();
                offenderGroupPage = groupListPage.SelectGroup(rowNumber);
                if (offenderGroupPage == null || offenderGroupPage?.DetailsTab.SelectFirstOffenderRow() == false)
                {
                    rowNumber++;
                    offenderGroupPage = null;
                }
               
            }
        }

        public void AddNewGroup(GroupTypeEnum GroupType, string GroupName, string GroupDescription, int numOfOffenders)
        {
            var groupListPage = webApp.WOMPage.Offenders_GroupsList();
            var newGroupPage = groupListPage.AddNewGroup();
            newGroupPage.FillForm(GroupType, GroupName, GroupDescription);

            newGroupPage.DetailsTab.selectOffenderByIndex(numOfOffenders);
            newGroupPage.DetailsTab.Save();
        }


        public OffenderGroupPage SelectGroupByName(string GroupName)
        {
            var groupListPage = webApp.WOMPage.Offenders_GroupsList();
            var GroupPage = groupListPage.SelectGroupByName(GroupName);
            return GroupPage;
        }

        public void DeleteGroupByName(string GroupName)
        {
            var groupListPage = webApp.WOMPage.Offenders_GroupsList();
            var GroupPage = groupListPage.SelectGroupByName(GroupName);
            GroupPage.DetailsTab.DeleteGroup();
        }


    }
}
