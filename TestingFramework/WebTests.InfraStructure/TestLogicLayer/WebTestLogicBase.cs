﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class WebTestLogicBase
    {
        protected readonly WebApplication webApp = WebApplication.Instance;
        protected readonly int _versionCategory;

        //Factory
        public GetAppVersionsTestLogic GetAppVersionsTestLogic { get { return new GetAppVersionsTestLogic(); } }

        public AboutTooltipTestLogic AboutTooltipTestLogic { get { return new AboutTooltipTestLogic(); } }

        public OffendersListTestLogic OffendersListTestLogic { get { return new OffendersListTestLogic(); } }

        public MonitorScreenTestLogic MonitorScreenTestLogic { get { return new MonitorScreenTestLogic(); } }
        public MonitorDashboardTestLogic MonitorDashboardTestLogic { get { return new MonitorDashboardTestLogic(); } }
        public UserMenuTestLogic UserMenuTestLogic { get { return new UserMenuTestLogic(); } }

        public LoginTestLogic Login { get { return new LoginTestLogic(); } }

        public EquipmentManagementTestLogic EquipmentManagementTestLogic { get { return new EquipmentManagementTestLogic(); } }

        public Training_CreateOffenderTestLogic Training_CreateOffenderTestLogic { get { return new Training_CreateOffenderTestLogic(); } }

        public AddNewOffenderTestLogic AddNewOffenderTestLogic { get { return new AddNewOffenderTestLogic(); } }

        public AddNewOffenderTestLogic AddNewOffenderTestLogic_NoClick { get { return new AddNewOffenderTestLogic(false); } }
        public QueueAndLogTestLogic SystemQueAndLog { get { return new QueueAndLogTestLogic(); } }

        public ScheduleTestLogic Schedule { get { return new ScheduleTestLogic(); } }

        public ZoneTestLogic Zone { get { return new ZoneTestLogic(); } }

        public UsersTestLogic UsersListTestLogic { get { return new UsersTestLogic(); } }

        public SecurityConfigLogic_Test SecurityConfigLogic_Test { get { return new SecurityConfigLogic_Test(); } }

        public GroupsListTestLogic GroupsListTestLogic { get { return new GroupsListTestLogic(); } }

        public OperationalReportsTestLogic OperationalReportsTestLogic { get { return new OperationalReportsTestLogic(); } }  

        public GroupPageTestLogic GroupPageTestLogic { get { return new GroupPageTestLogic(); } }

        public GeneralTestLogicPage GeneralTestLogic { get { return new GeneralTestLogicPage(); } }

        public SystemTablesTestLogic SystemTablesTestLogic { get { return new SystemTablesTestLogic(); } }
    }
}
