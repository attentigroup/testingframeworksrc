﻿using System;
using WebTests.InfraStructure.Entities;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class OffenderDetailsScreenTestLogic : WebTestLogicBase
    {

        public CurrentStatusTableRow SearchEventByEventId(int EventId, string RefId)
        {
            string GetEventId = Convert.ToString(EventId);

            webApp.WOMPage.Offenders_OffendersList().SelectOffenderFromTable(RefId);

            var currentStatusTableRow = webApp.WOMPage.OffenderPage().GetCurrentStatusTableRowByEventId(GetEventId);

            return currentStatusTableRow;
        }

    }
}
