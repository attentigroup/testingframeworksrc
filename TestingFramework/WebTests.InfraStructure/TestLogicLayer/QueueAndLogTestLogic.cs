﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.XTSimulator;
using OpenQA.Selenium;
using WebTests.InfraStructure.Entities;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class QueueAndLogTestLogic : WebTestLogicBase
    {
        public void VerifySecurityConfiguration()
        {
        }

        public List<QueueRow> GetQueueRows(int filterSectionNumber, string filterTitle)
        {
            var queueList = webApp.WOMPage.System_QueueAndLog().QueueTab.GetQueueRows(filterSectionNumber, filterTitle);

            return queueList;
        }
    }
}
