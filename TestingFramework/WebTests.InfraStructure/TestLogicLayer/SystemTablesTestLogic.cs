﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class SystemTablesTestLogic : WebTestLogicBase
    {
        public void ChangeCustomFieldsVisibility(List<string> customFieldsVisibilityList, bool ShowInOffenderDetails, bool ShowInOffendersList)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields");
            webApp.WOMPage.System_SystemTables(false).ChangeCustomFieldsVisibility(customFieldsVisibilityList, ShowInOffenderDetails, ShowInOffendersList);
        }

        public void UpdateCustomFieldsNames(Dictionary<string, string> customFieldsNames)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields");
            webApp.WOMPage.System_SystemTables(false).UpdateCustomFieldsNames(customFieldsNames);
        }

        public void AddCustomFieldsListValues(string tableIdName, List<string> customFieldsListValues)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields List Values");
            webApp.WOMPage.System_SystemTables(false).AddCustomFieldsListValues(tableIdName, customFieldsListValues);
        }

        public void UpdateCustomFieldsListValues(string tableIdName, string valueDescription, string updatedValueDescription)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields List Values");
            webApp.WOMPage.System_SystemTables(false).UpdateCustomFieldsListValues(tableIdName, valueDescription, updatedValueDescription);
        }

        public Dictionary<string, bool> GetUpdatedCustomFieldsNames(List<string> customFieldsNamesList)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields");
            return webApp.WOMPage.System_SystemTables(false).GetExistanceStatusOfUpdatedCustomFieldsNames(customFieldsNamesList);
        }

        public bool CheckIfItsPossibleToDeleteCustomFieldListValueUsedByOffender(string description)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields List Values");
            return webApp.WOMPage.System_SystemTables(false).CheckIfItsPossibleToDeleteCustomFieldListValueUsedByOffender(description);

        }

        public void DeleteCustomFieldsListValues(string valueDescription)
        {
            webApp.WOMPage.System_SystemTables().SystemTablesFilterPanel.SelectFilterByTitle("Custom Fields List Values");
            webApp.WOMPage.System_SystemTables(false).DeleteCustomFieldsListValues(valueDescription);
        }
    }
}
