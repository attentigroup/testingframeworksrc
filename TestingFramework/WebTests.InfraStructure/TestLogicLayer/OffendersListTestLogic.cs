﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;
using WebTests.InfraStructure.Pages.OffenderPages;
using WebTests.SeleniumWrapper;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class OffendersListTestLogic : WebTestLogicBase
    {
        OffendersListPage OffenderListPage => webApp.WOMPage.Offenders_OffendersList();
        
        public void ShowList()
        {
            webApp.WOMPage.Offenders_OffendersList();
            webApp.Dispose();
        }

        public void SelectFirstOffenderUsingFilter(int filterSectionNumber, string filterTitle)
        {
            OffenderListPage.OffenderListFilterSection.SelectFilterByTitle(filterSectionNumber,filterTitle);
            SelectFirstOffender();
        }

        public string[] GetSystemFilters()
        {
            return OffenderListPage.OffenderListFilterSection.GetSystemFiltersOffendersList();
        }

        public void ExpandRow()
        {
            OffenderListPage.OffendersListTable.ExpandRow(1);
        }

        public void SelectOffenderByTerm(string searchTerm, string filterTitle = "All")
        {
            OffenderListPage.SelectOffenderBySearchTerm(searchTerm, filterTitle);
        }

        public void SearchOffender(string searchTerm, string filterTitle = "All")
        {
            OffenderListPage.SearchOffenderWithoutSelecting(searchTerm, filterTitle);
        }

        public bool IsFilterSectionVisible(User user)
        {
            return OffenderListPage.IsFilterSectionVisible();
        }

        public bool IsCustomFieldDisplay(string resourceID)
        {
            return OffenderListPage.IsCustomFieldDisplay(resourceID);
        }

        public bool IsCustomFieldDataDisplayed(string resourceID, string searchTerm)
        {
            return webApp.WOMPage.Offenders_OffendersList_NoClick().IsCustomFieldDataDisplayed(resourceID, searchTerm);
        }

        public void SelectFirstOffender()
        {
            OffenderListPage.SelectFirstRow();
        }

        public OffenderDetails[] GetOffenderDetailsRows()
        {
            return webApp.WOMPage.Offenders_OffendersList_NoClick().GetOffenderDetailsRows();
        }

        public CurrentStatusTableRow[] GetCurrentStatusRowsArray()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetCurrentStatusRowsArray();
        }

        public string[] GetFilterEventsArray()
        {
            return webApp.WOMPage.Offenders_AddNewOffender(false, false).GetFilterEventsArray();
        }

        public void SelectOffenderByRefId(string offenderRefId)
        {
            OffenderListPage.SearchOffender(offenderRefId, -1);
        }

        public void CreateFilterByEndOfProgramForActiveOffender(DateTime time)
        {
            OffenderListPage.CreateFilterByEndOfProgramForActiveOffender(time);
        }

        public OffenderDetails GetOffenderDetailsFromTable(string refId)
        {
            var offenderDetails = OffenderListPage.GetOffenderDetails(refId);
            return offenderDetails;
        }

        public string GetAmountOfRowsByFilter(int sectionNumber, string title)
        {
            return OffenderListPage.GetAmountOfRowsByFilter(sectionNumber, title);
        }

        public string GetAmountOfRows()
        {

            return OffenderListPage.OffendersListTable.GetPagerAmountOfRows();
        }

        /// <summary>
        /// Use filterSectionNumber = -1 to ignore filter sections (wil search within the whole panel)
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="filterSectionNumber"></param>
        /// <param name="filterTitle"></param>
        /// <returns></returns>
        public AddNewOffenderTestLogic SerachOffenderUsingFilter(string searchTerm, int filterSectionNumber, string filterTitle = "All", string secondarySearchTerm = null)
        {
            filterTitle = (filterTitle == null) ? "All" : filterTitle;
            OffenderListPage.SearchOffender(searchTerm, filterSectionNumber, filterTitle, secondarySearchTerm);
            return AddNewOffenderTestLogic_NoClick;
        }

        public AddNewOffenderTestLogic SerachOffenderUsingFilterAndDateCreated(string searchTerm, int filterSectionNumber, DateTime? dateCreated, string filterTitle = "All")
        {
            filterTitle = (filterTitle == null) ? "All" : filterTitle;
            OffenderListPage.SearchOffenderByDateCreate(searchTerm, filterSectionNumber, dateCreated, filterTitle );
            return AddNewOffenderTestLogic_NoClick;
        }

        public void DeleteOffender(string offenderRefId)
        {
            OffenderListPage.SearchOffender(offenderRefId, -1);
            webApp.WOMPage.Offenders_AddNewOffender(false).DeleteOffender();
        }
        
        public string GetFilterTitle()
        {
            return OffenderListPage.GetFilterTitle();
        }

        public List<(int, int)> ShowResults(int showResultsNumber)
        {
            return OffenderListPage.ShowResults(showResultsNumber);
        }

        public bool PagingAllPages()
        {
            return OffenderListPage.ClickOnPagerButtons();
        }

        public void ClickOnFilter(int sectionNumber, string filterTitle)
        {
            OffenderListPage.ClickOnOffenderFilters(sectionNumber, filterTitle);
        }

        public List<BrowserElement> GetAllRows()
        {
            return OffenderListPage.OffendersListTable.GetAllRows();
        }

    }
}
