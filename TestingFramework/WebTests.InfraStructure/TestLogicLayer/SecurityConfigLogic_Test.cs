﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.Common;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;
using WebTests.InfraStructure.Pages.WOMPages;
using WebTests.SeleniumWrapper;
using static WebTests.InfraStructure.Pages.QueueAndLogPage;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class SecurityConfigLogic_Test : WebTestLogicBase
    {
        public string[] ScreenName { get; private set; }

        public List<List<SecurityConfigObject>> SecurityConfigTest()
        {
            bool expectedResult = false;

            //Call the SecurityConfig method of a parent Page Object
            var results = webApp.WOMPage.System_QueueAndLog().GetSecurityConfigurationResults();

            return results;
        }

        //public bool ConfirmChangeForElement(string fullElementFromApi,string Section,string Type,string Title)
        //{
        //    //var results = webApp.WOMPage.System_QueueAndLog().ConfirmChangeForElement(elment,Section, Type, Title);

        //    //return results;


        //    WOMPage page = webApp.WOMPage.System_QueueAndLog();
        //    var status = page.GetShowHideElemntStatus(/*fullElementFromApi,*/ Section, Type, Title);
        //    return status;
        //}

        public SecurityConfigObject GetSecurityElementResult(string ScreenName, string section, string APIString, string title, bool Prepration, EnmModifyOption modifyOption = EnmModifyOption.ShowHideOnly)
        {
            Logger.WriteLine($"Modify option is : '{modifyOption.ToString()}'");
            SecurityConfigObject result = new SecurityConfigObject();

            switch (ScreenName)
            {
                case "Queue":
                    result = webApp.WOMPage.System_QueueAndLog().GetSecurityResult(section, APIString, title);
                    break;

                case "Log":
                    result = webApp.WOMPage.System_QueueAndLog().LogTab.GetSecurityResult(section, APIString, title);
                    break;

                case "OffendersList":
                    result = webApp.WOMPage.Offenders_OffendersList().Navigate2Section(section, APIString);
                    break;

                case "LastKnownLocation":
                    result = webApp.WOMPage.Reports_CurrentKnownLocation().Navigate2Section(section, APIString);
                    break;

                case "EventGrid":
                    if (APIString.StartsWith("EventGrid_List_GridHeader_"))
                        result = webApp.WOMPage.Monitor().GetSecurityResult(section, APIString, Prepration);
                    else
                        result = webApp.WOMPage.Monitor_NoClick().GetSecurityResult(section, APIString, Prepration);
                    break;

                case "GroupsList":
                    //if(title == "AgencyName")
                    //result = webApp.WOMPage.Offenders_GroupsList().GetSecurityResult(title, APIString);
                    //else
                    result = webApp.WOMPage.Offenders_GroupsList().GetSecurityResult(title, APIString);
                    break;

                case "Monitor":
                        result = webApp.WOMPage.Monitor_NoClick().GetSecurityResult(section, APIString, Prepration);
                    result = webApp.WOMPage.Monitor().GetSecurityResult(section, APIString, Prepration);
                    break;

                case "Group":
                    result = webApp.WOMPage.Offenders_GroupsList().Navigate2Section(section, APIString);
                    break;

                case "OffenderDetails":
                    result = GetOffenderDetailsSecurityResult(APIString, modifyOption);
                    break;

                case "UserGrid":
                    result = webApp.WOMPage.System_Users_UsersList().Navigate2Section(APIString);
                    break;

                case "UserOffendersGrid":
                    webApp.WOMPage.System_Users_UsersList().SelectFirstUser("Officers");
                    result = webApp.WOMPage.System_Users_AddNewUser_NoClick().Navigate2Section(ScreenName, section, APIString);
                    break;

                case "UserAgenciesGrid":
                    webApp.WOMPage.System_Users_UsersList().SelectFirstUser("Monitor Center");
                    result = webApp.WOMPage.System_Users_AddNewUser_NoClick().Navigate2Section(ScreenName, section, APIString);
                    break;

                case "TrackerRules":
                    if (modifyOption == EnmModifyOption.ShowHideOnly)
                        result = webApp.WOMPage.System_SystemConfiguration_TrackerRules().TrackerRulesTable.GetSecurityResult(APIString, modifyOption);
                    else
                        result = webApp.WOMPage.System_SystemConfiguration_TrackerRules().Navigate2Section(APIString, modifyOption);
                    break;
                case "User":
                    result = webApp.WOMPage.System_SystemConfiguration_UsersList_NoClick().UserListGetSecurityResult(APIString);
                    break;

                case "Schedule":
                    if (section == "TimeFrameTooltip")
                        result = webApp.WOMPage.Offenders_AddNewOffender(false,false).Navigate2Section(section, APIString);
                    if (title == "SaveGroupSchedule")
                        result = webApp.WOMPage.Offenders_GroupsList().Navigate2Section(section, APIString);
                    else
                        result = webApp.WOMPage.Offenders_AddNewOffender(false,false).GetSecurityResult(APIString);
                    break;

                case "General":
                    result = webApp.WOMPage.GetGeneralSecurityConfigResults(section, APIString);
                    break;

                case "Equipment":
                    result = webApp.WOMPage.System_EquipmentManagement().Navigate2Section(section, APIString);
                    break;

                case "OffenderStatus":
                    result = webApp.WOMPage.Offenders_AddNewOffender(false,false).Navigate2Section(section, APIString);
                    break;

                case "GroupDetails":
                    webApp.WOMPage.Offenders_GroupsList().SelectGroup();
                    result = webApp.WOMPage.OffenderGroupPage().GetGroupDetailsSecurityResult(section, APIString, modifyOption);
                    break;

                case "Offender":
                    result = webApp.WOMPage.Offenders_AddNewOffender(false, false).GetSecurityResult(APIString);
                    break;

                case "OffenderConfig":
                    result = webApp.WOMPage.Offenders_AddNewOffender(false, false).GetSecurityResult(APIString, modifyOption);
                    break;

                case "OffenderLocation":
                    if (title == "Events")
                        result = webApp.WOMPage.Offenders_AddNewOffender(false, false).Navigate2Section(section, APIString);
                    else
                        result = webApp.WOMPage.Offenders_AddNewOffender(false, false).GetSecurityResult(APIString);
                    break;

                default:
                    Assert.Fail($"The test for screen name {ScreenName} is not implemented yet...");
                    break;
            }
            return result;
        }


        private SecurityConfigObject GetOffenderDetailsSecurityResult(string APIString, EnmModifyOption modifyOption)
        {
            SecurityConfigObject result;
            if (APIString == "OffenderDetails_ContactInformation_Button_UpdateAddress")
                result = webApp.WOMPage.Offenders_AddNewOffender().GetSecurityResult(APIString);
            //else if (APIString == "OffenderDetails_ProgramDetails_Button_DeleteCellularData")
            //    result = webApp.WOMPage.EquipmentManagementPage_NoClick().GetSecurityResult(APIString);
            else if (APIString == "OffenderDetails_CustomFields_Label_DateTime1" && modifyOption == EnmModifyOption.EnableDisableOnly)
                result = webApp.WOMPage.Offenders_AddNewOffender(false,false).GetReadOnlyResultForDateTimeElement();
            else if(APIString.Contains("OffenderDetails_CaseManagment"))
                result = webApp.WOMPage.Offenders_AddNewOffender(false, false).GetSecurityResult(APIString, modifyOption, false);
            else
                result = webApp.WOMPage.Offenders_AddNewOffender(false,false).GetSecurityResult(APIString, modifyOption);
            return result;
        }
    }

}

