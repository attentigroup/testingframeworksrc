﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.InfraStructure.Entities;
using WebTests.InfraStructure.Pages;

namespace WebTests.InfraStructure.TestLogicLayer
{
    public class GetAppVersionsTestLogic : WebTestLogicBase
    {
        public AppVersions GetVersions()
        {
            //Test logic here
            return webApp.WOMPage.GetVersions();
            
        }
    }
}
