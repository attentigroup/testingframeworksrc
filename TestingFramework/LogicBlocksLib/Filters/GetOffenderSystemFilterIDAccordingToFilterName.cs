﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Filters12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.Filters
{
    public class GetOffenderSystemFilterIDAccordingToFilterName : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.GetDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GetDataResponse GetDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FilterName { get; set; }

        [PropertyTest(EnumPropertyName.filterID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int FilterID { get; set; }


        protected override void ExecuteBlock()
        {
            if (GetDataResponse.GetDataResult.Any(x => x.Name == "System"))
            {
                var systemFiltersLst = GetDataResponse.GetDataResult.First(x => x.Name == "System");
                if (systemFiltersLst.FilterWrappers.Any(x=> x.Name==FilterName))
                {
                    var requestedFilter = systemFiltersLst.FilterWrappers.First(x => x.Name == FilterName);
                    FilterID = requestedFilter.ID;
                }
                else
                    throw new Exception($"Filter name ={FilterName}  not fount in System filters ");

            }
            else
                throw new Exception("No System filter exist");
                
        }
    }
}
