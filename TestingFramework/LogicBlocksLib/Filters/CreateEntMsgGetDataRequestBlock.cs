﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Filters12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.Filters
{
    public class CreateEntMsgGetDataRequestBlock: LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ScreenName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnumFiltersScreenNameOptions ScreenName { get; set; }

        [PropertyTest(EnumPropertyName.GetDataRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public  GetDataRequest GetDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetDataRequest = new GetDataRequest();
            GetDataRequest.screenName = ScreenName.ToString();
        }
    }
}
