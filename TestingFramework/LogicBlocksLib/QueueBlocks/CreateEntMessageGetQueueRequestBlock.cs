﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;

#endregion

namespace LogicBlocksLib.QueueBlocks
{
    public class CreateEntMessageGetQueueRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int MaximumRows { get; set; }


        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int StartRowIndex { get; set; }


        [PropertyTest(EnumPropertyName.filterID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int filterID { get; set; }

        [PropertyTest(EnumPropertyName.resultCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Queue0.EnmResultCode resultCode { get; set; }

        [PropertyTest(EnumPropertyName.GetQueueRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Queue0.EntMessageGetQueueRequest GetQueueRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetQueueRequest = new Queue0.EntMessageGetQueueRequest();
            GetQueueRequest.MaximumRows = MaximumRows;
            GetQueueRequest.StartRowIndex = StartRowIndex;
            GetQueueRequest.filterID = filterID;
            GetQueueRequest.resultCode = resultCode;
        }
    }
}
