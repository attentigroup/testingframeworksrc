﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgGetZonesByEntityIDRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.EnmEntityType EntityType { get; set; }
        
        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? Version { get; set; }
        
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }
        
        protected override void ExecuteBlock()
        {
            GetZonesByEntityIDRequest = new Zones0.MsgGetZonesByEntityIDRequest();
            GetZonesByEntityIDRequest.EntityID = EntityID;
            GetZonesByEntityIDRequest.EntityType = EntityType;
            GetZonesByEntityIDRequest.Version = Version;
        }

    }


    public class CreateMsgGetZonesByEntityIDRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.EnmEntityType EntityType { get; set; }


        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int? Version { get; set; }


        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }



        protected override void ExecuteBlock()
        {
            GetZonesByEntityIDRequest = new Zones1.MsgGetZonesByEntityIDRequest();
            GetZonesByEntityIDRequest.EntityID = EntityID;
            GetZonesByEntityIDRequest.EntityType = EntityType;
            GetZonesByEntityIDRequest.Version = Version;
        }

    }


    public class CreateMsgGetZonesByEntityIDRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.EnmEntityType EntityType { get; set; }


        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int? Version { get; set; }


        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }



        protected override void ExecuteBlock()
        {
            GetZonesByEntityIDRequest = new Zones2.MsgGetZonesByEntityIDRequest();
            GetZonesByEntityIDRequest.EntityID = EntityID;
            GetZonesByEntityIDRequest.EntityType = EntityType;
            GetZonesByEntityIDRequest.Version = Version;
        }

    }
}

