﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgUpdatePolygonZoneRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }


        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Points, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.EntPoint[] Points { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }

        public CreateMsgUpdatePolygonZoneRequestBlock()
        {
            Points = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones0.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones0.EntPoint() { LAT = 31.105820, LON = 33.834568 } };
        }

        protected override void ExecuteBlock()
        {
            UpdatePolygonZoneRequest = new Zones0.MsgUpdatePolygonZoneRequest();
            UpdatePolygonZoneRequest.BLP = BLP;
            UpdatePolygonZoneRequest.EntityID = EntityID;
            UpdatePolygonZoneRequest.EntityType = EntityType;
            UpdatePolygonZoneRequest.FullSchedule = FullSchedule;
            UpdatePolygonZoneRequest.GraceTime = GraceTime;
            UpdatePolygonZoneRequest.IsCurfewZone = IsCurfewZone;
            UpdatePolygonZoneRequest.Name = Name;
            UpdatePolygonZoneRequest.Points = Points;
            UpdatePolygonZoneRequest.ZoneID = ZoneID;
        }

    }


    public class CreateMsgUpdatePolygonZoneRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }


        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Points, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.EntPoint[] Points { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdatePolygonZoneRequest = new Zones1.MsgUpdatePolygonZoneRequest();
            UpdatePolygonZoneRequest.BLP = BLP;
            UpdatePolygonZoneRequest.EntityID = EntityID;
            UpdatePolygonZoneRequest.EntityType = EntityType;
            UpdatePolygonZoneRequest.FullSchedule = FullSchedule;
            UpdatePolygonZoneRequest.GraceTime = GraceTime;
            UpdatePolygonZoneRequest.IsCurfewZone = IsCurfewZone;
            UpdatePolygonZoneRequest.Name = Name;
            UpdatePolygonZoneRequest.Points = Points;
            UpdatePolygonZoneRequest.ZoneID = ZoneID;
        }

    }


    public class CreateMsgUpdatePolygonZoneRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }


        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Points, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.EntPoint[] Points { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdatePolygonZoneRequest = new Zones2.MsgUpdatePolygonZoneRequest();
            UpdatePolygonZoneRequest.BLP = BLP;
            UpdatePolygonZoneRequest.EntityID = EntityID;
            UpdatePolygonZoneRequest.EntityType = EntityType;
            UpdatePolygonZoneRequest.FullSchedule = FullSchedule;
            UpdatePolygonZoneRequest.GraceTime = GraceTime;
            UpdatePolygonZoneRequest.IsCurfewZone = IsCurfewZone;
            UpdatePolygonZoneRequest.Name = Name;
            UpdatePolygonZoneRequest.Points = Points;
            UpdatePolygonZoneRequest.ZoneID = ZoneID;
        }

    }
}

