﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgUpdateInclusionCircularZoneRequestBlock : CreateMsgUpdateCircularZoneRequestBlock
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
        }
    }

    public class CreateMsgUpdateInclusionCircularZoneRequestBlock_1 : CreateMsgUpdateCircularZoneRequestBlock_1
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
        }
    }

    public class CreateMsgUpdateInclusionCircularZoneRequestBlock_2 : CreateMsgUpdateCircularZoneRequestBlock_2
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
        }
    }
}
