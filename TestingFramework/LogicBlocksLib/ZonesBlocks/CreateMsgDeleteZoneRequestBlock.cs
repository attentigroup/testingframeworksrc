﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgDeleteZoneRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.DeleteZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgDeleteZoneRequest DeleteZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteZoneRequest = new Zones0.MsgDeleteZoneRequest();
            DeleteZoneRequest.EntityID = EntityID;
            DeleteZoneRequest.EntityType = EntityType;
            DeleteZoneRequest.ZoneID = ZoneID;
        }

    }


    public class CreateMsgDeleteZoneRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.DeleteZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgDeleteZoneRequest DeleteZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteZoneRequest = new Zones1.MsgDeleteZoneRequest();
            DeleteZoneRequest.EntityID = EntityID;
            DeleteZoneRequest.EntityType = EntityType;
            DeleteZoneRequest.ZoneID = ZoneID;
        }

    }


    public class CreateMsgDeleteZoneRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.DeleteZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgDeleteZoneRequest DeleteZoneRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteZoneRequest = new Zones2.MsgDeleteZoneRequest();
            DeleteZoneRequest.EntityID = EntityID;
            DeleteZoneRequest.EntityType = EntityType;
            DeleteZoneRequest.ZoneID = ZoneID;
        }

    }
}

