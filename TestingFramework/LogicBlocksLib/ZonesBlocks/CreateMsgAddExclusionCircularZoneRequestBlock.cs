﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgAddExclusionCircularZoneRequestBlock : CreateMsgAddCircularZoneRequestBlock
    {
        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.BufferRadius, EnumPropertyModifier.Mandatory)]
        public int BufferRadius { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            AddCircularZoneRequest.Limitation = Zones0.EnmLimitationType.Exclusion;
            AddCircularZoneRequest.BufferRadius = BufferRadius;
        }

    }

    public class CreateMsgAddExclusionCircularZoneRequestBlock_1 : CreateMsgAddCircularZoneRequestBlock_1
    {
        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.BufferRadius, EnumPropertyModifier.Mandatory)]
        public int BufferRadius { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            AddCircularZoneRequest.Limitation = Zones1.EnmZoneLimitationType.Exclusion;
            AddCircularZoneRequest.BufferRadius = BufferRadius;
        }

    }

    public class CreateMsgAddExclusionCircularZoneRequestBlock_2 : CreateMsgAddCircularZoneRequestBlock_2
    {
        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.BufferRadius, EnumPropertyModifier.Mandatory)]
        public int BufferRadius { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            AddCircularZoneRequest.Limitation = Zones2.EnmZoneLimitationType.Exclusion;
            AddCircularZoneRequest.BufferRadius = BufferRadius;
        }

    }
}
