﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgAddPolygonZoneRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EnmLimitationType Limitation { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Points, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntPoint[] Points { get; set; }



        [PropertyTest(EnumPropertyName.AddPolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgAddPolygonZoneRequest AddPolygonZoneRequest { get; set; }

        public CreateMsgAddPolygonZoneRequestBlock()
        {
            Points = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones0.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones0.EntPoint() { LAT = 32.105820, LON = 34.834568 } };
        }


        protected override void ExecuteBlock()
        {
            AddPolygonZoneRequest = new Zones0.MsgAddPolygonZoneRequest();
            AddPolygonZoneRequest.BLP = BLP;
            AddPolygonZoneRequest.EntityID = EntityID;
            AddPolygonZoneRequest.EntityType = EntityType;
            AddPolygonZoneRequest.FullSchedule = FullSchedule;
            AddPolygonZoneRequest.GraceTime = GraceTime;
            AddPolygonZoneRequest.IsCurfewZone = IsCurfewZone;
            AddPolygonZoneRequest.Limitation = Limitation;
            AddPolygonZoneRequest.Name = Name;
            AddPolygonZoneRequest.Points = Points;
        }

    }


    public class CreateMsgAddPolygonZoneRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EnmZoneLimitationType Limitation { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Points, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.EntPoint[] Points { get; set; }



        [PropertyTest(EnumPropertyName.AddPolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgAddPolygonZoneRequest AddPolygonZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            AddPolygonZoneRequest = new Zones1.MsgAddPolygonZoneRequest();
            AddPolygonZoneRequest.BLP = BLP;
            AddPolygonZoneRequest.EntityID = EntityID;
            AddPolygonZoneRequest.EntityType = EntityType;
            AddPolygonZoneRequest.FullSchedule = FullSchedule;
            AddPolygonZoneRequest.GraceTime = GraceTime;
            AddPolygonZoneRequest.IsCurfewZone = IsCurfewZone;
            AddPolygonZoneRequest.Limitation = Limitation;
            AddPolygonZoneRequest.Name = Name;
            AddPolygonZoneRequest.Points = Points;
        }

    }


    public class CreateMsgAddPolygonZoneRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EnmZoneLimitationType Limitation { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Points, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.EntPoint[] Points { get; set; }



        [PropertyTest(EnumPropertyName.AddPolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgAddPolygonZoneRequest AddPolygonZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            AddPolygonZoneRequest = new Zones2.MsgAddPolygonZoneRequest();
            AddPolygonZoneRequest.BLP = BLP;
            AddPolygonZoneRequest.EntityID = EntityID;
            AddPolygonZoneRequest.EntityType = EntityType;
            AddPolygonZoneRequest.FullSchedule = FullSchedule;
            AddPolygonZoneRequest.GraceTime = GraceTime;
            AddPolygonZoneRequest.IsCurfewZone = IsCurfewZone;
            AddPolygonZoneRequest.Limitation = Limitation;
            AddPolygonZoneRequest.Name = Name;
            AddPolygonZoneRequest.Points = Points;
        }

    }
}
