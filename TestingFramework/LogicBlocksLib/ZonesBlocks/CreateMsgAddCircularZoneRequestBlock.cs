﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace LogicBlocksLib.ZonesBlocks
{
    public abstract class CreateMsgAddCircularZoneRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntPoint BLP { get; set; }


        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.ZoneGraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EnmLimitationType Limitation { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.ZoneName, EnumPropertyModifier.Mandatory)]
        public string ZoneName { get; set; }

        [PropertyTest(EnumPropertyName.Point, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntPoint Point { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.RealRadius, EnumPropertyModifier.Mandatory)]
        public int RealRadius { get; set; }

       
        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }

        public CreateMsgAddCircularZoneRequestBlock()
        {
            Point = new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };
        }


        protected override void ExecuteBlock()
        {
            AddCircularZoneRequest = new Zones0.MsgAddCircularZoneRequest();
            AddCircularZoneRequest.BLP = BLP;
            AddCircularZoneRequest.EntityID = EntityID;
            AddCircularZoneRequest.EntityType = EntityType;
            AddCircularZoneRequest.FullSchedule = FullSchedule;
            AddCircularZoneRequest.GraceTime = GraceTime;
            AddCircularZoneRequest.IsCurfewZone = IsCurfewZone;
            AddCircularZoneRequest.Name = ZoneName;
            AddCircularZoneRequest.Point = Point;
            AddCircularZoneRequest.RealRadius = RealRadius;
        }

    }


    public abstract class CreateMsgAddCircularZoneRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EntPoint BLP { get; set; }


        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.ZoneGraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EnmZoneLimitationType Limitation { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.ZoneName, EnumPropertyModifier.Mandatory)]
        public string ZoneName { get; set; }

        [PropertyTest(EnumPropertyName.Point, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.EntPoint Point { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.RealRadius, EnumPropertyModifier.Mandatory)]
        public int RealRadius { get; set; }


        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            AddCircularZoneRequest = new Zones1.MsgAddCircularZoneRequest();
            AddCircularZoneRequest.BLP = BLP;
            AddCircularZoneRequest.EntityID = EntityID;
            AddCircularZoneRequest.EntityType = EntityType;
            AddCircularZoneRequest.FullSchedule = FullSchedule;
            AddCircularZoneRequest.GraceTime = GraceTime;
            AddCircularZoneRequest.IsCurfewZone = IsCurfewZone;
            AddCircularZoneRequest.Name = ZoneName;
            AddCircularZoneRequest.Point = Point;
            AddCircularZoneRequest.RealRadius = RealRadius;
        }

    }


    public abstract class CreateMsgAddCircularZoneRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EntPoint BLP { get; set; }


        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.ZoneGraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EnmZoneLimitationType Limitation { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.ZoneName, EnumPropertyModifier.Mandatory)]
        public string ZoneName { get; set; }

        [PropertyTest(EnumPropertyName.Point, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.EntPoint Point { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.RealRadius, EnumPropertyModifier.Mandatory)]
        public int RealRadius { get; set; }


        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            AddCircularZoneRequest = new Zones2.MsgAddCircularZoneRequest();
            AddCircularZoneRequest.BLP = BLP;
            AddCircularZoneRequest.EntityID = EntityID;
            AddCircularZoneRequest.EntityType = EntityType;
            AddCircularZoneRequest.FullSchedule = FullSchedule;
            AddCircularZoneRequest.GraceTime = GraceTime;
            AddCircularZoneRequest.IsCurfewZone = IsCurfewZone;
            AddCircularZoneRequest.Name = ZoneName;
            AddCircularZoneRequest.Point = Point;
            AddCircularZoneRequest.RealRadius = RealRadius;
        }

    }
}
