﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgUpdateExclusionCircularZoneRequestBlock : CreateMsgUpdateCircularZoneRequestBlock
    {
        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.BufferRadius, EnumPropertyModifier.Mandatory)]
        public int BufferRadius { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            UpdateCircularZoneRequest.BufferRadius = BufferRadius;
        }
    }

    public class CreateMsgUpdateExclusionCircularZoneRequestBlock_1 : CreateMsgUpdateCircularZoneRequestBlock_1
    {
        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.BufferRadius, EnumPropertyModifier.Mandatory)]
        public int BufferRadius { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            UpdateCircularZoneRequest.BufferRadius = BufferRadius;
        }
    }

    public class CreateMsgUpdateExclusionCircularZoneRequestBlock_2 : CreateMsgUpdateCircularZoneRequestBlock_2
    {
        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.BufferRadius, EnumPropertyModifier.Mandatory)]
        public int BufferRadius { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            UpdateCircularZoneRequest.BufferRadius = BufferRadius;
        }
    }
}
