﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace LogicBlocksLib.ZonesBlocks
{
    public abstract class CreateMsgUpdateCircularZoneRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }


        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Point, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.EntPoint Point { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.RealRadius, EnumPropertyModifier.Mandatory)]
        public int RealRadius { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones0.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }

        public CreateMsgUpdateCircularZoneRequestBlock()
        {
            Point = new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 };
        }


        protected override void ExecuteBlock()
        {
            UpdateCircularZoneRequest = new Zones0.MsgUpdateCircularZoneRequest();
            UpdateCircularZoneRequest.BLP = BLP;
            UpdateCircularZoneRequest.EntityID = EntityID;
            UpdateCircularZoneRequest.EntityType = EntityType;
            UpdateCircularZoneRequest.FullSchedule = FullSchedule;
            UpdateCircularZoneRequest.GraceTime = GraceTime;
            UpdateCircularZoneRequest.IsCurfewZone = IsCurfewZone;
            UpdateCircularZoneRequest.Name = Name;
            UpdateCircularZoneRequest.Point = Point;
            UpdateCircularZoneRequest.RealRadius = RealRadius;
            UpdateCircularZoneRequest.ZoneID = ZoneID;
        }

    }


    public abstract class CreateMsgUpdateCircularZoneRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }


        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Point, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.EntPoint Point { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RealRadius { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones1.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdateCircularZoneRequest = new Zones1.MsgUpdateCircularZoneRequest();
            UpdateCircularZoneRequest.BLP = BLP;
            UpdateCircularZoneRequest.EntityID = EntityID;
            UpdateCircularZoneRequest.EntityType = EntityType;
            UpdateCircularZoneRequest.FullSchedule = FullSchedule;
            UpdateCircularZoneRequest.GraceTime = GraceTime;
            UpdateCircularZoneRequest.IsCurfewZone = IsCurfewZone;
            UpdateCircularZoneRequest.Name = Name;
            UpdateCircularZoneRequest.Point = Point;
            UpdateCircularZoneRequest.RealRadius = RealRadius;
            UpdateCircularZoneRequest.ZoneID = ZoneID;
        }

    }


    public abstract class CreateMsgUpdateCircularZoneRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BLP, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EntPoint BLP { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool FullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsCurfewZone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsCurfewZone { get; set; }


        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Point, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.EntPoint Point { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RealRadius { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Zones2.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdateCircularZoneRequest = new Zones2.MsgUpdateCircularZoneRequest();
            UpdateCircularZoneRequest.BLP = BLP;
            UpdateCircularZoneRequest.EntityID = EntityID;
            UpdateCircularZoneRequest.EntityType = EntityType;
            UpdateCircularZoneRequest.FullSchedule = FullSchedule;
            UpdateCircularZoneRequest.GraceTime = GraceTime;
            UpdateCircularZoneRequest.IsCurfewZone = IsCurfewZone;
            UpdateCircularZoneRequest.Name = Name;
            UpdateCircularZoneRequest.Point = Point;
            UpdateCircularZoneRequest.RealRadius = RealRadius;
            UpdateCircularZoneRequest.ZoneID = ZoneID;
        }

    }
}

