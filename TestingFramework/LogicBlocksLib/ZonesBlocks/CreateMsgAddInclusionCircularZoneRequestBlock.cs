﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;

namespace LogicBlocksLib.ZonesBlocks
{
    public class CreateMsgAddInclusionCircularZoneRequestBlock : CreateMsgAddCircularZoneRequestBlock
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            AddCircularZoneRequest.Limitation = Zones0.EnmLimitationType.Inclusion;
        }

    }

    public class CreateMsgAddInclusionCircularZoneRequestBlock_1 : CreateMsgAddCircularZoneRequestBlock_1
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            AddCircularZoneRequest.Limitation = Zones1.EnmZoneLimitationType.Inclusion;
        }

    }

    public class CreateMsgAddInclusionCircularZoneRequestBlock_2 : CreateMsgAddCircularZoneRequestBlock_2
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            AddCircularZoneRequest.Limitation = Zones2.EnmZoneLimitationType.Inclusion;
        }

    }
}
