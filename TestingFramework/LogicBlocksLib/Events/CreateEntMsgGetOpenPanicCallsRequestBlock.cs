﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgGetOpenPanicCallsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.GetOpenPanicCallsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetOpenPanicCallsRequest GetOpenPanicCallsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetOpenPanicCallsRequest = new Events0.EntMsgGetOpenPanicCallsRequest();
            GetOpenPanicCallsRequest.MaximumRows = MaximumRows;
            GetOpenPanicCallsRequest.StartRowIndex = StartRowIndex;
        }
    }

    public class CreateEntMsgGetOpenPanicCallsRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.GetOpenPanicCallsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetOpenPanicCallsRequest GetOpenPanicCallsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetOpenPanicCallsRequest = new Events1.EntMsgGetOpenPanicCallsRequest();
            GetOpenPanicCallsRequest.MaximumRows = MaximumRows;
            GetOpenPanicCallsRequest.StartRowIndex = StartRowIndex;
        }
    }

    public class CreateEntMsgGetOpenPanicCallsRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.GetOpenPanicCallsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetOpenPanicCallsRequest GetOpenPanicCallsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetOpenPanicCallsRequest = new Events2.EntMsgGetOpenPanicCallsRequest();
            GetOpenPanicCallsRequest.MaximumRows = MaximumRows;
            GetOpenPanicCallsRequest.StartRowIndex = StartRowIndex;
        }
    }
}
