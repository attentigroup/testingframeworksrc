﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgGetAlcoholTestResultRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }
        
        [PropertyTest(EnumPropertyName.GetAlcoholTestResultRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetAlcoholTestResultRequest GetAlcoholTestResultRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetAlcoholTestResultRequest = new Events0.EntMsgGetAlcoholTestResultRequest() { EventID = EventID };
        }
    }

    public class CreateEntMsgGetAlcoholTestResultRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.GetAlcoholTestResultRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetAlcoholTestResultRequest GetAlcoholTestResultRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetAlcoholTestResultRequest = new Events1.EntMsgGetAlcoholTestResultRequest() { EventID = EventID };
        }
    }
}
