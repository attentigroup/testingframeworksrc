﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgAddHandlingActionRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.Comment, EnumPropertyModifier.Mandatory)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ContactID { get; set; }

        [PropertyTest(EnumPropertyName.ContactType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmContactType? ContactType { get; set; }

        [PropertyTest(EnumPropertyName.EmailAddress, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EmailAddress { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.PagerCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PagerCode { get; set; }

        [PropertyTest(EnumPropertyName.PagerType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? PagerType { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.ReminderTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long ReminderTime { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskSubType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScheduledTaskSubType { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ScheduledTaskTime { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScheduledTaskType { get; set; }

        [PropertyTest(EnumPropertyName.ThirdPartyActionCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ThirdPartyActionCode { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EnmHandlingActionType Type { get; set; }

        [PropertyTest(EnumPropertyName.UserDefinedActionCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string UserDefinedActionCode { get; set; }

        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddHandlingActionRequest = new Events0.EntMsgAddHandlingActionRequest();
            AddHandlingActionRequest.Comment = Comment;
            AddHandlingActionRequest.ContactID = ContactID;
            AddHandlingActionRequest.ContactType = ContactType;
            AddHandlingActionRequest.EmailAddress = EmailAddress;
            AddHandlingActionRequest.EventID = EventID;
            AddHandlingActionRequest.PagerCode = PagerCode;
            if (ProviderID != null && PagerType == null)
                AddHandlingActionRequest.PagerType = PagerTypeGenerator.GetPagerTypeByProviderID(ProviderID.Value);
            else
                AddHandlingActionRequest.PagerType = PagerType;
            AddHandlingActionRequest.Phone = Phone;
            AddHandlingActionRequest.PrefixPhone = PrefixPhone;
            AddHandlingActionRequest.ProviderID = ProviderID;
            AddHandlingActionRequest.ReminderTime = ReminderTime;
            if (ScheduledTaskType != null && ScheduledTaskSubType == null)
                AddHandlingActionRequest.ScheduledTaskSubType = ScheduleTaskTypeGenerator.GetScheduleTaskSubTypeByScheduleTaskType(ScheduledTaskType);
            else
                AddHandlingActionRequest.ScheduledTaskSubType = ScheduledTaskSubType;
            AddHandlingActionRequest.ScheduledTaskTime = ScheduledTaskTime;
            AddHandlingActionRequest.ScheduledTaskType = ScheduledTaskType;
            AddHandlingActionRequest.ThirdPartyActionCode = ThirdPartyActionCode;
            AddHandlingActionRequest.Type = Type;
            AddHandlingActionRequest.UserDefinedActionCode = UserDefinedActionCode;
          
        }
    }

    public class CreateEntMsgAddHandlingActionRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.Comment, EnumPropertyModifier.Mandatory)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ContactID { get; set; }

        [PropertyTest(EnumPropertyName.ContactType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.EnmContactType? ContactType { get; set; }

        [PropertyTest(EnumPropertyName.EmailAddress, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EmailAddress { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.PagerCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PagerCode { get; set; }

        [PropertyTest(EnumPropertyName.PagerType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? PagerType { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.ReminderTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long ReminderTime { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskSubType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScheduledTaskSubType { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ScheduledTaskTime { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScheduledTaskType { get; set; }

        [PropertyTest(EnumPropertyName.ThirdPartyActionCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ThirdPartyActionCode { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EnmHandlingActionType Type { get; set; }

        [PropertyTest(EnumPropertyName.UserDefinedActionCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string UserDefinedActionCode { get; set; }

        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }
        protected override void ExecuteBlock()
        {
            AddHandlingActionRequest = new Events1.EntMsgAddHandlingActionRequest();
            AddHandlingActionRequest.Comment = Comment;
            AddHandlingActionRequest.ContactID = ContactID;
            AddHandlingActionRequest.ContactType = ContactType;
            AddHandlingActionRequest.EmailAddress = EmailAddress;
            AddHandlingActionRequest.EventID = EventID;
            AddHandlingActionRequest.PagerCode = PagerCode;
            AddHandlingActionRequest.PagerType = PagerType;
            AddHandlingActionRequest.Phone = Phone;
            AddHandlingActionRequest.PrefixPhone = PrefixPhone;
            AddHandlingActionRequest.ProviderID = ProviderID;
            AddHandlingActionRequest.ReminderTime = ReminderTime;
            AddHandlingActionRequest.ScheduledTaskSubType = ScheduledTaskSubType;
            AddHandlingActionRequest.ScheduledTaskTime = ScheduledTaskTime;
            AddHandlingActionRequest.ScheduledTaskType = ScheduledTaskType;
            AddHandlingActionRequest.ThirdPartyActionCode = ThirdPartyActionCode;
            AddHandlingActionRequest.Type = Type;
            AddHandlingActionRequest.UserDefinedActionCode = UserDefinedActionCode;
        }
    }

    public class CreateEntMsgAddHandlingActionRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.Comment, EnumPropertyModifier.Mandatory)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ContactID { get; set; }

        [PropertyTest(EnumPropertyName.ContactType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EnmContactType? ContactType { get; set; }

        [PropertyTest(EnumPropertyName.EmailAddress, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EmailAddress { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.PagerCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PagerCode { get; set; }

        [PropertyTest(EnumPropertyName.PagerType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? PagerType { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.ReminderTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long ReminderTime { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskSubType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScheduledTaskSubType { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ScheduledTaskTime { get; set; }

        [PropertyTest(EnumPropertyName.ScheduledTaskType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ScheduledTaskType { get; set; }

        [PropertyTest(EnumPropertyName.ThirdPartyActionCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ThirdPartyActionCode { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EnmHandlingActionType Type { get; set; }

        [PropertyTest(EnumPropertyName.UserDefinedActionCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string UserDefinedActionCode { get; set; }

        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }
        protected override void ExecuteBlock()
        {
            AddHandlingActionRequest = new Events2.EntMsgAddHandlingActionRequest();
            AddHandlingActionRequest.Comment = Comment;
            AddHandlingActionRequest.ContactID = ContactID;
            AddHandlingActionRequest.ContactType = ContactType;
            AddHandlingActionRequest.EmailAddress = EmailAddress;
            AddHandlingActionRequest.EventID = EventID;
            AddHandlingActionRequest.PagerCode = PagerCode;
            AddHandlingActionRequest.PagerType = PagerType;
            AddHandlingActionRequest.Phone = Phone;
            AddHandlingActionRequest.PrefixPhone = PrefixPhone;
            AddHandlingActionRequest.ProviderID = ProviderID;
            AddHandlingActionRequest.ReminderTime = ReminderTime;
            AddHandlingActionRequest.ScheduledTaskSubType = ScheduledTaskSubType;
            AddHandlingActionRequest.ScheduledTaskTime = ScheduledTaskTime;
            AddHandlingActionRequest.ScheduledTaskType = ScheduledTaskType;
            AddHandlingActionRequest.ThirdPartyActionCode = ThirdPartyActionCode;
            AddHandlingActionRequest.Type = Type;
            AddHandlingActionRequest.UserDefinedActionCode = UserDefinedActionCode;
        }
    }
}
