﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgGetOffenderEventStatusRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderEventStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetOffenderEventStatusRequest GetOffenderEventStatusRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderEventStatusRequest = new Events0.EntMsgGetOffenderEventStatusRequest();
            GetOffenderEventStatusRequest.EventID = EventID;
            
        }
    }

    public class CreateEntMsgGetOffenderEventStatusRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderEventStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetOffenderEventStatusRequest GetOffenderEventStatusRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetOffenderEventStatusRequest = new Events1.EntMsgGetOffenderEventStatusRequest();
            GetOffenderEventStatusRequest.EventID = EventID;
        }
    }

    public class CreateEntMsgGetOffenderEventStatusRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderEventStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetOffenderEventStatusRequest GetOffenderEventStatusRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetOffenderEventStatusRequest = new Events2.EntMsgGetOffenderEventStatusRequest();
            GetOffenderEventStatusRequest.EventID = EventID;
        }
    }
}
