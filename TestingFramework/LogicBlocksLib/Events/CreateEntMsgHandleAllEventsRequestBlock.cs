﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgHandleAllEventsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.HandleAllEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgHandleAllEventsRequest HandleAllEventsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            HandleAllEventsRequest = new Events0.EntMsgHandleAllEventsRequest();
            HandleAllEventsRequest.OffenderID = OffenderID;
        }
    }

    public class CreateEntMsgHandleAllEventsRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.HandleAllEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgHandleAllEventsRequest HandleAllEventsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            HandleAllEventsRequest = new Events1.EntMsgHandleAllEventsRequest();
            HandleAllEventsRequest.OffenderID = OffenderID;
        }
    }

    public class CreateEntMsgHandleAllEventsRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.HandleAllEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgHandleAllEventsRequest HandleAllEventsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            HandleAllEventsRequest = new Events2.EntMsgHandleAllEventsRequest();
            HandleAllEventsRequest.OffenderID = OffenderID;
        }
    }
}
