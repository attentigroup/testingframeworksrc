﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgGetHandlingActionByQueryRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ActionTypeList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmHandlingActionType [] ActionTypeList { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int [] EventIDList { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.ActionFromTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ActionFromTime { get; set; }

        [PropertyTest(EnumPropertyName.ActionToTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ActionToTime { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetHandlingActionsByQueryRequest GetHandlingActionsByQueryRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingActionsByQueryRequest = new Events0.EntMsgGetHandlingActionsByQueryRequest();
            GetHandlingActionsByQueryRequest.ActionTypeList = ActionTypeList;
            GetHandlingActionsByQueryRequest.EventToTime = EndTime;
            if (EventID != 0)
                GetHandlingActionsByQueryRequest.EventIDList = new int[] { EventID };
            else
                GetHandlingActionsByQueryRequest.EventIDList = EventIDList;
            //for(int i=0; i<EventIDList.Length-1;i++)
            //{
            //    GetHandlingActionsByQueryRequest.EventIDList[i] = EventIDList[i];
            //}
            GetHandlingActionsByQueryRequest.EventFromTime = StartTime;
            GetHandlingActionsByQueryRequest.ActionFromTime = ActionFromTime;
            GetHandlingActionsByQueryRequest.ActionToTime = ActionToTime;
        }
    }

    public class CreateEntMsgGetHandlingActionByQueryRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ActionTypeList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.EnmHandlingActionType[] ActionTypeList { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] EventIDList { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetHandlingActionsByQueryRequest GetHandlingActionsByQueryRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingActionsByQueryRequest = new Events1.EntMsgGetHandlingActionsByQueryRequest();
            GetHandlingActionsByQueryRequest.ActionTypeList = ActionTypeList;
            GetHandlingActionsByQueryRequest.EndTime = EndTime;
            GetHandlingActionsByQueryRequest.EventIDList = new int[] { EventID };
            GetHandlingActionsByQueryRequest.StartTime = StartTime;
        }
    }

    public class CreateEntMsgGetHandlingActionByQueryRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ActionTypeList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EnmHandlingActionType[] ActionTypeList { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] EventIDList { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetHandlingActionsByQueryRequest GetHandlingActionsByQueryRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingActionsByQueryRequest = new Events2.EntMsgGetHandlingActionsByQueryRequest();
            GetHandlingActionsByQueryRequest.ActionTypeList = new Events2.EnmHandlingActionType[] { };
            GetHandlingActionsByQueryRequest.EndTime = EndTime;
            GetHandlingActionsByQueryRequest.EventIDList = new int[] { EventID };
            GetHandlingActionsByQueryRequest.StartTime = StartTime;
        }
    }


}
