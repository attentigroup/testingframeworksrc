﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace LogicBlocksLib.Events
{
    public class CreateEntMsgGetEventsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventCodes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string [] EventCodes { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.IncludeHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeHistory { get; set; }

        [PropertyTest(EnumPropertyName.LastEventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LastEventID { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Message { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmProgramType? ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.Severity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmEventSeverity? Severity { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ListSortDirection SortDirection  { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmEventsSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.Status, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmHandlingStatusType? Status { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events0.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        public CreateEntMsgGetEventsRequestBlock()
        {

            GetEventsRequest = new Events0.EntMsgGetEventsRequest();

            GetEventsRequest.SortDirection = ListSortDirection.Descending;
        }

        protected override void ExecuteBlock()
        {
            GetEventsRequest = new Events0.EntMsgGetEventsRequest();
            GetEventsRequest.AgencyID = AgencyID;
            GetEventsRequest.EndTime = EndTime;
            GetEventsRequest.EventCodes = EventCodes;
            GetEventsRequest.EventID = EventID;
            GetEventsRequest.IncludeHistory = IncludeHistory;
            GetEventsRequest.LastEventID = LastEventID;
            GetEventsRequest.LastTimeStamp = LastTimeStamp;
            GetEventsRequest.Limit = Limit;
            GetEventsRequest.Message = Message;
            GetEventsRequest.OffenderID = OffenderID;
            GetEventsRequest.OffenderRefID = OffenderRefID;
            GetEventsRequest.OfficerID = OfficerID;
            GetEventsRequest.ProgramType = ProgramType;
            GetEventsRequest.Receiver = Receiver;
            GetEventsRequest.Severity = Severity;
            GetEventsRequest.SortDirection = SortDirection;
            GetEventsRequest.SortField = SortField;
            GetEventsRequest.StartTime = StartTime;
            GetEventsRequest.Status = Status;
        }


    }

    public class CreateEntMsgGetEventsRequestBlockNewEvent2Weeks : CreateEntMsgGetEventsRequestBlock
    {

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            CreateEntMsgGetEventsRequestBlock2Weeks();
        }
        public void CreateEntMsgGetEventsRequestBlock2Weeks()
        {
            GetEventsRequest.StartTime = DateTime.Now.AddDays(-14);
            GetEventsRequest.EndTime = DateTime.Now;
            GetEventsRequest.Status = Events0.EnmHandlingStatusType.New;
            GetEventsRequest.SortDirection = ListSortDirection.Descending;
        }

    }
    public class CreateEntMsgGetEventsRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventCodes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] EventCodes { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.IncludeHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeHistory { get; set; }

        [PropertyTest(EnumPropertyName.LastEventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LastEventID { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Message { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.EnmProgramType? ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.Severity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.EnmEventSeverity? Severity { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.EnmEventsSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.Status, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events1.EnmHandlingStatusType? Status { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events1.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        public CreateEntMsgGetEventsRequestBlock_1()
        {
            //GetEventsRequest.SortDirection = Events1.ListSortDirection.Descending;
        }

        protected override void ExecuteBlock()
        {
            GetEventsRequest = new Events1.EntMsgGetEventsRequest();
            GetEventsRequest.AgencyID = AgencyID;
            GetEventsRequest.EndTime = EndTime;
            GetEventsRequest.EventCodes = EventCodes;
            GetEventsRequest.EventID = EventID;
            GetEventsRequest.IncludeHistory = IncludeHistory;
            GetEventsRequest.LastEventID = LastEventID;
            GetEventsRequest.LastTimeStamp = LastTimeStamp;
            GetEventsRequest.Limit = Limit;
            GetEventsRequest.Message = Message;
            GetEventsRequest.OffenderID = OffenderID;
            GetEventsRequest.OffenderRefID = OffenderRefID;
            GetEventsRequest.OfficerID = OfficerID;
            GetEventsRequest.ProgramType = ProgramType;
            GetEventsRequest.Receiver = Receiver;
            GetEventsRequest.Severity = Severity;
            GetEventsRequest.SortDirection = SortDirection;
            GetEventsRequest.SortField = SortField;
            GetEventsRequest.StartTime = StartTime;
            GetEventsRequest.Status = Status;
        }


    }

    public class CreateEntMsgGetEventsRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventCodes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EntStringQueryListParameter EventCode { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.IncludeHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeHistory { get; set; }

        [PropertyTest(EnumPropertyName.IncludeOffenderDetails, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeOffenderDetails { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long? LastTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Message { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.Parameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Parameter { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EnmProgramType? ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.Severity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EnmEventSeverity? Severity { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EnmEventsSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.Status, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events2.EnmHandlingStatusType? Status { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Events2.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        public CreateEntMsgGetEventsRequestBlock_2()
        {
            //GetEventsRequest.SortDirection = Events2.ListSortDirection.Descending;
        }

        protected override void ExecuteBlock()
        {
            GetEventsRequest = new Events2.EntMsgGetEventsRequest();
            GetEventsRequest.AgencyID = AgencyID != 0 ? new Events2.EntNumericQueryParameter() { Operator = Events2.EnmSqlOperatorNum.Equal, Value = AgencyID } : null; 
            GetEventsRequest.ToTime = EndTime;
            GetEventsRequest.EventCode = EventCode;
            GetEventsRequest.EventID = EventID != 0 ? new Events2.EntNumericQueryParameter() { Operator = Events2.EnmSqlOperatorNum.Equal, Value = EventID } : null;
            GetEventsRequest.IncludeHistory = IncludeHistory;
            GetEventsRequest.IncludeOffenderDetails = IncludeOffenderDetails;
            GetEventsRequest.LastTimeStamp = LastTimeStamp;
            GetEventsRequest.MaximumRows = MaximumRows;
            GetEventsRequest.Message = Message != null ? new Events2.EntStringQueryParameter() { Operator = Events2.EnmSqlOperatorStr.Equal, Value = Message } : null; ;
            //TODO: noa talk to shimon
            GetEventsRequest.OffenderID = OffenderID != 0 ? new Events2.EntNumericQueryParameter() { Operator = Events2.EnmSqlOperatorNum.Equal, Value = OffenderID } : null;
            GetEventsRequest.OffenderRefID = OffenderRefID != null ? new Events2.EntStringQueryParameter() { Operator = Events2.EnmSqlOperatorStr.Equal, Value = OffenderRefID } : null; ;
            GetEventsRequest.OfficerID = OfficerID != 0 ? new Events2.EntNumericQueryParameter() { Operator = Events2.EnmSqlOperatorNum.Equal, Value = OfficerID } : null;
            GetEventsRequest.Parameter = Parameter != null ? new Events2.EntStringQueryParameter() { Operator = Events2.EnmSqlOperatorStr.Equal, Value = Parameter } : null; ;
            GetEventsRequest.ProgramType = ProgramType;
            GetEventsRequest.Receiver = Receiver != null ? new Events2.EntStringQueryParameter() { Operator = Events2.EnmSqlOperatorStr.Equal, Value = Receiver } : null; ;
            GetEventsRequest.Severity = Severity;
            GetEventsRequest.SortDirection = SortDirection;
            GetEventsRequest.SortField = SortField;
            GetEventsRequest.StartRowIndex = StartRowIndex;
            GetEventsRequest.FromTime = StartTime;
            GetEventsRequest.HandlingStatus = Status;
        }


    }
}
