﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;


namespace LogicBlocksLib.Group
{
    public class CreateEntMsgAddGroupRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.GroupName, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string GroupName { get; set; }

        [PropertyTest(EnumPropertyName.OffendersListIDs, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] OffendersListIDs { get; set; }

        [PropertyTest(EnumPropertyName.ProgramGroup, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EnmProgramGroup ProgramGroup { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetUnallocatedOffenderListResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntGroupOffender> GetUnallocatedOffenderListResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Groups0.EntMsgAddGroupDetailsRequest AddGroupDetailsRequest { get; set; }


        protected override void ExecuteBlock()
        {
            AddGroupDetailsRequest = new Groups0.EntMsgAddGroupDetailsRequest();
            AddGroupDetailsRequest.Description = Description;
            AddGroupDetailsRequest.Name = GroupName;
            AddGroupDetailsRequest.ProgramGroup = ProgramGroup;

            if (OffendersListIDs.IsNullOrEmpty() == false )
            {//nothing to do - use it.
                AddGroupDetailsRequest.OffendersList = OffendersListIDs;
            }
            else if (GetOffendersResponse != null || GetOffendersResponse.OffendersList.Length > 0)
            {
                AddGroupDetailsRequest.OffendersList = GetOffendersResponse.OffendersList.Select(x => x.ID).ToArray();
            }
            else if (!GetUnallocatedOffenderListResponse.IsNullOrEmpty())
            {
                AddGroupDetailsRequest.OffendersList = new int[GetUnallocatedOffenderListResponse.Count()];
                var res2arr = GetUnallocatedOffenderListResponse.ToArray();
                for (int i = 0; i < res2arr.Length; i++)
                {
                    AddGroupDetailsRequest.OffendersList[i] = res2arr[i].ID;
                }
            }
            

        }
    }
}
