﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace LogicBlocksLib.Group
{
    /// <summary>
    /// Add Group ID to get a specific group data, 0 = all groups
    /// </summary>
    public class CreateEntMsgGetGroupRequestBlock : LogicBlockBase
    {   
        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? GroupID { get; set; }
        
        [PropertyTest(EnumPropertyName.GetGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Groups0.EntMsgGetGroupDetailsRequest GetGroupDetailsRequest { get; set; }
               
        protected override void ExecuteBlock()
        {
            GetGroupDetailsRequest = new Groups0.EntMsgGetGroupDetailsRequest();

            if (GroupID == null)
                GroupID = 0;
                //throw new Exception("Cannot Get Group Details For GroupID = null");

            GetGroupDetailsRequest.GroupID = GroupID;

        }
    }
}
