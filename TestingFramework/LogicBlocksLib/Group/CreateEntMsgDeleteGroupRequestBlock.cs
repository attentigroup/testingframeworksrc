﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace LogicBlocksLib.Group
{
    /// <summary>
    /// Add Group ID to get a specific group data, 0 = all groups
    /// </summary>
    public class CreateEntMsgDeleteGroupRequestBlock : LogicBlockBase
    {   
        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int GroupID { get; set; }
        
        [PropertyTest(EnumPropertyName.DeleteGroupDetailsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Groups0.EntMsgDeleteGroupDetailsRequest DeleteGroupDetailsRequest { get; set; }
               
        protected override void ExecuteBlock()
        {
            DeleteGroupDetailsRequest = new Groups0.EntMsgDeleteGroupDetailsRequest();
            DeleteGroupDetailsRequest.GroupID = GroupID;
        }
    }
}
