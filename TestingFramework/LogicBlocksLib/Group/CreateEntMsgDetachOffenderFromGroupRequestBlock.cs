﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;


namespace LogicBlocksLib.Group
{
 
    public class CreateEntMsgDetachOffenderFromGroupRequestBlock : LogicBlockBase
    {   
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.DetachOffenderFromGroupRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Groups0.EntMsgDetachOffenderFromGroupRequest DetachOffenderFromGroupRequest { get; set; }
               
        protected override void ExecuteBlock()
        {
            DetachOffenderFromGroupRequest = new Groups0.EntMsgDetachOffenderFromGroupRequest();
            DetachOffenderFromGroupRequest.OffenderID = OffenderID;
        }
    }
}
