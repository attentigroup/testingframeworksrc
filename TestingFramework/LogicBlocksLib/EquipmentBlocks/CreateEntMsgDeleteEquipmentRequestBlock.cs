﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace LogicBlocksLib.EquipmentBlocks
{

    public class CreateEntMsgDeleteEquipmentRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntMsgAddEquipmentResponse AddEquipmentResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgDeleteEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgDeleteEquipmentRequest DeleteEquipmentRequest { get; set; }



        protected override void ExecuteBlock()
        {
            DeleteEquipmentRequest = new EntMsgDeleteEquipmentRequest();

            DeleteEquipmentRequest.EquipmentID = AddEquipmentResponse.NewEquipmentID;
        }
    }


    public class CreateEntMsgDeleteEquipmentRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgDeleteEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgDeleteEquipmentRequest DeleteEquipmentRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteEquipmentRequest = new Equipment_1.EntMsgDeleteEquipmentRequest();

            DeleteEquipmentRequest.EquipmentID = EquipmentID;
        }
    }


    public class CreateEntMsgDeleteEquipmentRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgDeleteEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgDeleteEquipmentRequest DeleteEquipmentRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteEquipmentRequest = new Equipment_2.EntMsgDeleteEquipmentRequest();

            DeleteEquipmentRequest.EquipmentID = EquipmentID;
        }
    }
}
