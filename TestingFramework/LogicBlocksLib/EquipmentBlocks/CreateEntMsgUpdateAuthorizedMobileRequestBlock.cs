﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgUpdateAuthorizedMobileRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmMobileApplication ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.IsActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsActive { get; set; }

        [PropertyTest(EnumPropertyName.CellularNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string CellularNumber { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Model { get; set; }

        [PropertyTest(EnumPropertyName.OperatingSystem, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string OperatingSystem { get; set; }

        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.AppVersion, EnumPropertyModifier.Mandatory)]
        public string AppVersion { get; set; }

        [PropertyTest(EnumPropertyName.DeviceFCMToken, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string DeviceFCMToken { get; set; }



        [PropertyTest(EnumPropertyName.EntMsgUpdateAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgUpdateAuthorizedMobileRequest UpdateAuthorizedMobileRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateAuthorizedMobileRequest = new EntMsgUpdateAuthorizedMobileRequest();
            UpdateAuthorizedMobileRequest.ApplicationID = ApplicationID;
            UpdateAuthorizedMobileRequest.ID = MobileID;
            UpdateAuthorizedMobileRequest.IsActive = IsActive;
            UpdateAuthorizedMobileRequest.CellularNumber = CellularNumber;
            UpdateAuthorizedMobileRequest.Model = Model;
            UpdateAuthorizedMobileRequest.OperatingSystem = OperatingSystem;
            UpdateAuthorizedMobileRequest.Comment = Comment;
            UpdateAuthorizedMobileRequest.AppVersion = AppVersion;
            UpdateAuthorizedMobileRequest.DeviceFCMToken = DeviceFCMToken;
        }
    }
}
