﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgUpdateEquipmentRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int? AgencyId { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentLocation, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentLocation { get; set; }

        [PropertyTest(EnumPropertyName.FailureDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? FailureDate { get; set; }

        [PropertyTest(EnumPropertyName.FailureDescription, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string FailureDescription { get; set; }

        [PropertyTest(EnumPropertyName.FailureType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmFailureType FailureType { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.Bool, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }

        [PropertyTest(EnumPropertyName.ReturnFromRepairDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime?  ReturnFromRepairDate { get; set; }

        [PropertyTest(EnumPropertyName.SentToRepairDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? SentToRepairDate { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }


        protected override void ExecuteBlock()
        {
            UpdateEquipmentRequest = new EntMsgUpdateEquipmentRequest();

            UpdateEquipmentRequest.AgencyID = AgencyId;
            UpdateEquipmentRequest.EquipmentID = EquipmentID;
            UpdateEquipmentRequest.EquipmentLocation = EquipmentLocation;
            UpdateEquipmentRequest.FailureDate = FailureDate;
            UpdateEquipmentRequest.FailureDescription = FailureDescription;
            UpdateEquipmentRequest.FailureType = FailureType;
            UpdateEquipmentRequest.IsDamaged = IsDamaged;
            UpdateEquipmentRequest.ReturnFromRepairDate = ReturnFromRepairDate;
            UpdateEquipmentRequest.SentToRepairDate = SentToRepairDate;

        }
    }


    public class CreateEntMsgUpdateEquipmentRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.AgencyId, EnumPropertyModifier.None)]
        public int? AgencyId { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentLocation, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentLocation { get; set; }

        [PropertyTest(EnumPropertyName.FailureDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? FailureDate { get; set; }

        [PropertyTest(EnumPropertyName.FailureDescription, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string FailureDescription { get; set; }

        [PropertyTest(EnumPropertyName.FailureType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EnmFailureType FailureType { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.Bool, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }

        [PropertyTest(EnumPropertyName.ReturnFromRepairDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ReturnFromRepairDate { get; set; }

        [PropertyTest(EnumPropertyName.SentToRepairDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? SentToRepairDate { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }

        public CreateEntMsgUpdateEquipmentRequestBlock_1()
        {
            AgencyId = -1;
        }

        protected override void ExecuteBlock()
        {
            UpdateEquipmentRequest = new Equipment_1.EntMsgUpdateEquipmentRequest();
            
            UpdateEquipmentRequest.AgencyID = AgencyId;
            UpdateEquipmentRequest.EquipmentID = EquipmentID;
            UpdateEquipmentRequest.EquipmentLocation = EquipmentLocation;
            UpdateEquipmentRequest.FailureDate = FailureDate;
            UpdateEquipmentRequest.FailureDescription = FailureDescription;
            UpdateEquipmentRequest.FailureType = FailureType;
            UpdateEquipmentRequest.IsDamaged = IsDamaged;
            UpdateEquipmentRequest.ReturnFromRepairDate = ReturnFromRepairDate;
            UpdateEquipmentRequest.SentToRepairDate = SentToRepairDate;

        }
    }


    public class CreateEntMsgUpdateEquipmentRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int? AgencyId { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentLocation, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentLocation { get; set; }

        [PropertyTest(EnumPropertyName.FailureDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? FailureDate { get; set; }

        [PropertyTest(EnumPropertyName.FailureDescription, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string FailureDescription { get; set; }

        [PropertyTest(EnumPropertyName.FailureType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EnmFailureType FailureType { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.Bool, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }

        [PropertyTest(EnumPropertyName.ReturnFromRepairDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ReturnFromRepairDate { get; set; }

        [PropertyTest(EnumPropertyName.SentToRepairDate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? SentToRepairDate { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }


        protected override void ExecuteBlock()
        {
            UpdateEquipmentRequest = new Equipment_2.EntMsgUpdateEquipmentRequest();

            UpdateEquipmentRequest.AgencyID = AgencyId;
            UpdateEquipmentRequest.EquipmentID = EquipmentID;
            UpdateEquipmentRequest.EquipmentLocation = EquipmentLocation;
            UpdateEquipmentRequest.FailureDate = FailureDate;
            UpdateEquipmentRequest.FailureDescription = FailureDescription;
            UpdateEquipmentRequest.FailureType = FailureType;
            UpdateEquipmentRequest.IsDamaged = IsDamaged;
            UpdateEquipmentRequest.ReturnFromRepairDate = ReturnFromRepairDate;
            UpdateEquipmentRequest.SentToRepairDate = SentToRepairDate;

        }
    }
}
