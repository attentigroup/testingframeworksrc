﻿using Common.CustomAttributes;
using Common.Enum;
using System;

using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace LogicBlocksLib.EquipmentBlocks
{
    /// <summary>
    /// CreateEntMsgAddEquipmentRequestBlock for latest version
    /// </summary>
    public class CreateEntMsgAddEquipmentRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int? AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentDescription, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentEncryptionGSM, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_0.EnmEncryptionType EquipmentEncryptionGSM { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentEncryptionRF, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_0.EnmEncryptionType EquipmentEncryptionRF { get; set; }

        [PropertyTest(EnumPropertyName.Manufacturer, EnumPropertyType.Manufacturer, EnumPropertyModifier.None)]
        public string Manufacturer { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_0.EnmEquipmentModel EnmEquipmentModel { get; set; }

        [PropertyTest(EnumPropertyName.EnmModemType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_0.EnmModemType ModemType { get; set; }

        [PropertyTest(EnumPropertyName.EnmProtocolType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_0.EnmProtocolType ProtocolType { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSupportEncryption, EnumPropertyType.Bool, EnumPropertyModifier.None)]
        public bool SupportEncryption { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_0.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.IsEquipmentExists, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsEquipmentExists { get; set; }

        public CreateEntMsgAddEquipmentRequestBlock()
        {
            AgencyID = null;

            EquipmentEncryptionGSM = Equipment_0.EnmEncryptionType.NoEncryption;
            EquipmentEncryptionRF = Equipment_0.EnmEncryptionType.NoEncryption;
        }


        protected override void ExecuteBlock()
        {
            if (!IsEquipmentExists)
            {
                AddEquipmentRequest = new Equipment_0.EntMsgAddEquipmentRequest();

                AddEquipmentRequest.AgencyID = AgencyID;
                AddEquipmentRequest.Description = Description;
                AddEquipmentRequest.EquipmentEncryptionGSM = EquipmentEncryptionGSM;
                AddEquipmentRequest.EquipmentEncryptionRF = EquipmentEncryptionRF;
                AddEquipmentRequest.Manufacturer = Manufacturer;
                AddEquipmentRequest.Model = EnmEquipmentModel;
                AddEquipmentRequest.ModemType = ModemType;
                AddEquipmentRequest.ProtocolType = ProtocolType;
                AddEquipmentRequest.SerialNumber = EquipmentSerialNumber;
                AddEquipmentRequest.SupportEncryption = SupportEncryption;
            }
            
        }
    }


    /// <summary>
    /// CreateEntMsgAddEquipmentRequestBlock for one version older
    /// </summary>
    public class CreateEntMsgAddEquipmentRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int? AgencyId { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentDescription, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentEncryptionGSM, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EnmEncryptionType EquipmentEncryptionGSM { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentEncryptionRF, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EnmEncryptionType EquipmentEncryptionRF { get; set; }

        [PropertyTest(EnumPropertyName.Manufacturer, EnumPropertyType.Manufacturer, EnumPropertyModifier.None)]
        public string Manufacturer { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EnmEquipmentModel EnmEquipmentModel { get; set; }

        [PropertyTest(EnumPropertyName.EnmModemType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EnmModemType ModemType { get; set; }

        [PropertyTest(EnumPropertyName.EnmProtocolType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_1.EnmProtocolType ProtocolType { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSupportEncryption, EnumPropertyType.Bool, EnumPropertyModifier.None)]
        public bool SupportEncryption { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }

        public CreateEntMsgAddEquipmentRequestBlock_1()
        {
            AgencyId = null;

            EquipmentEncryptionGSM = Equipment_1.EnmEncryptionType.NoEncryption;
            EquipmentEncryptionRF = Equipment_1.EnmEncryptionType.NoEncryption;
        }

        protected override void ExecuteBlock()
        {
            AddEquipmentRequest = new Equipment_1.EntMsgAddEquipmentRequest();

            AddEquipmentRequest.AgencyID = AgencyId;
            AddEquipmentRequest.Description = Description;
            AddEquipmentRequest.EquipmentEncryptionGSM = EquipmentEncryptionGSM;
            AddEquipmentRequest.EquipmentEncryptionRF = EquipmentEncryptionRF;
            AddEquipmentRequest.Manufacturer = Manufacturer;
            AddEquipmentRequest.Model = EnmEquipmentModel;
            AddEquipmentRequest.ModemType = ModemType;
            AddEquipmentRequest.ProtocolType = ProtocolType;
            AddEquipmentRequest.SerialNumber = EquipmentSerialNumber;
            AddEquipmentRequest.SupportEncryption = SupportEncryption;
        }
    }



    /// <summary>
    /// CreateEntMsgAddEquipmentRequestBlock for two versions older
    /// </summary>
    public class CreateEntMsgAddEquipmentRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int? AgencyId { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentDescription, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentEncryptionGSM, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EnmEncryptionType EquipmentEncryptionGSM { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentEncryptionRF, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EnmEncryptionType EquipmentEncryptionRF { get; set; }

        [PropertyTest(EnumPropertyName.Manufacturer, EnumPropertyType.Manufacturer, EnumPropertyModifier.None)]
        public string Manufacturer { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EnmEquipmentModel EnmEquipmentModel { get; set; }

        [PropertyTest(EnumPropertyName.EnmModemType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EnmModemType ModemType { get; set; }

        [PropertyTest(EnumPropertyName.EnmProtocolType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment_2.EnmProtocolType ProtocolType { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSupportEncryption, EnumPropertyType.Bool, EnumPropertyModifier.None)]
        public bool SupportEncryption { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }


        public CreateEntMsgAddEquipmentRequestBlock_2()
        {
            AgencyId = null;

            EquipmentEncryptionGSM = Equipment_2.EnmEncryptionType.NoEncryption;
            EquipmentEncryptionRF = Equipment_2.EnmEncryptionType.NoEncryption;
        }


        protected override void ExecuteBlock()
        {
            AddEquipmentRequest = new Equipment_2.EntMsgAddEquipmentRequest();

            AddEquipmentRequest.AgencyID = AgencyId;
            AddEquipmentRequest.Description = Description;
            AddEquipmentRequest.EquipmentEncryptionGSM = EquipmentEncryptionGSM;
            AddEquipmentRequest.EquipmentEncryptionRF = EquipmentEncryptionRF;
            AddEquipmentRequest.Manufacturer = Manufacturer;
            AddEquipmentRequest.Model = EnmEquipmentModel;
            AddEquipmentRequest.ModemType = ModemType;
            AddEquipmentRequest.ProtocolType = ProtocolType;
            AddEquipmentRequest.SerialNumber = EquipmentSerialNumber;
            AddEquipmentRequest.SupportEncryption = SupportEncryption;
        }
    }

}

