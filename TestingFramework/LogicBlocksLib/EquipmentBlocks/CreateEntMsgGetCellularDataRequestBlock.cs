﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgGetCellularDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.DeviceIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] DeviceIDList { get; set; }

        [PropertyTest(EnumPropertyName.SerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.SIMID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int SIMID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetCellularDataRequest GetCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            GetCellularDataRequest = new EntMsgGetCellularDataRequest();
            GetCellularDataRequest.DeviceIDList = DeviceIDList;
            GetCellularDataRequest.SerialNumber = SerialNumber;
            GetCellularDataRequest.SIMID = SIMID;
        }
    }


    public class CreateEntMsgGetCellularDataRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ReceiverIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] ReceiverIDList { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgGetCellularDataRequest GetCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            GetCellularDataRequest = new Equipment_1.EntMsgGetCellularDataRequest();
            GetCellularDataRequest.ReceiverIDList = ReceiverIDList;
            GetCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;
        }
    }


    public class CreateEntMsgGetCellularDataRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ReceiverIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] ReceiverIDList { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgGetCellularDataRequest GetCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            GetCellularDataRequest = new Equipment_2.EntMsgGetCellularDataRequest();
            GetCellularDataRequest.ReceiverIDList = ReceiverIDList;
            GetCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;
        }
    }
}
