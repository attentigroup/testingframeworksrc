﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgAddAuthorizedMobileRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmMobileApplication ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.MobileID, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.CellularNumber, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string CellularNumber { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string Model { get; set; }

        [PropertyTest(EnumPropertyName.OperatingSystem, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string OperatingSystem { get; set; }

        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.AppVersion, EnumPropertyModifier.Mandatory)]
        public string AppVersion { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgAddAuthorizedMobileRequest AddAuthorizedMobileRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetAuthorizedMobileListRequest GetAuthorizedMobileListRequest { get; set; }


        protected override void ExecuteBlock()
        {
            AddAuthorizedMobileRequest.ApplicationID = ApplicationID;
            AddAuthorizedMobileRequest.ID = MobileID;
            AddAuthorizedMobileRequest.CellularNumber = CellularNumber;
            AddAuthorizedMobileRequest.Model = Model;
            AddAuthorizedMobileRequest.OperatingSystem = OperatingSystem;
            AddAuthorizedMobileRequest.Comment = Comment;
            AddAuthorizedMobileRequest.AppVersion = AppVersion;

            GetAuthorizedMobileListRequest.ApplicationID = ApplicationID;
            GetAuthorizedMobileListRequest.MobileID = MobileID;
        }

        public CreateEntMsgAddAuthorizedMobileRequestBlock()
        {
            AddAuthorizedMobileRequest = new EntMsgAddAuthorizedMobileRequest();
            GetAuthorizedMobileListRequest = new EntMsgGetAuthorizedMobileListRequest();
        }
    }
}
