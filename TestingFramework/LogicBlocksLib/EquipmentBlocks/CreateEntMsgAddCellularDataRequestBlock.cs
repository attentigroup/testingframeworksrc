﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.EquipmentBlocks
{

    /// <summary>
    /// Create EntMsgAddCellularDataRequest Block
    /// </summary>
    public class CreateEntMsgAddCellularDataRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.DataPhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string DataPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.DataPrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string DataPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.IsPrimarySIM, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsPrimarySIM { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.ProviderID, EnumPropertyModifier.Mandatory)]
        public int ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.VoicePhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgAddCellularDataRequest AddCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            AddCellularDataRequest = new EntMsgAddCellularDataRequest();
            AddCellularDataRequest.DataPhoneNumber = DataPhoneNumber;
            AddCellularDataRequest.DataPrefixPhone = DataPrefixPhone;
            AddCellularDataRequest.EquipmentID = EquipmentID;
            AddCellularDataRequest.IsPrimarySIM = IsPrimarySIM;
            AddCellularDataRequest.ProviderID = ProviderID;
            AddCellularDataRequest.VoicePhoneNumber = VoicePhoneNumber;
            AddCellularDataRequest.VoicePrefixPhone = VoicePrefixPhone;

        }
    }
}
