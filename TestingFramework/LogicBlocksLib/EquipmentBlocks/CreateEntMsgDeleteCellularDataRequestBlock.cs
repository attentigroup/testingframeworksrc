﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgDeleteCellularDataRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }


        [PropertyTest(EnumPropertyName.ID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ID { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgDeleteCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgDeleteCellularDataRequest DeleteCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteCellularDataRequest = new EntMsgDeleteCellularDataRequest();
            DeleteCellularDataRequest.EquipmentID = EquipmentID;
            DeleteCellularDataRequest.ID = ID;
        }
    }


    public class CreateEntMsgDeleteCellularDataRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ReceiverSerialNumber { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgDeleteCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgDeleteCellularDataRequest DeleteCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteCellularDataRequest = new Equipment_1.EntMsgDeleteCellularDataRequest();
            DeleteCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;
        }
    }


    public class CreateEntMsgDeleteCellularDataRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ReceiverSerialNumber { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgDeleteCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgDeleteCellularDataRequest DeleteCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteCellularDataRequest = new Equipment_2.EntMsgDeleteCellularDataRequest();
            DeleteCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;
        }
    }
}
