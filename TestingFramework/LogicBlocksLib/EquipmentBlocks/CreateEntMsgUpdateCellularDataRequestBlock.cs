﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgUpdateCellularDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.DataPhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string DataPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.DataPrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string DataPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ID, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int ID { get; set; }

        [PropertyTest(EnumPropertyName.IsPrimarySIM, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsPrimarySIM { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.ProviderID, EnumPropertyModifier.Mandatory)]
        public int ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.VoicePhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            UpdateCellularDataRequest = new EntMsgUpdateCellularDataRequest();
            UpdateCellularDataRequest.DataPhoneNumber = DataPhoneNumber;
            UpdateCellularDataRequest.DataPrefixPhone = DataPrefixPhone;
            UpdateCellularDataRequest.ID = ID;
            UpdateCellularDataRequest. IsPrimarySIM = true;
            UpdateCellularDataRequest.ProviderID = ProviderID;
            UpdateCellularDataRequest.VoicePhoneNumber = VoicePhoneNumber;
            UpdateCellularDataRequest.VoicePrefixPhone = VoicePrefixPhone;
        }
    }


    public class CreateEntMsgUpdateCellularDataRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CSDPhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string CSDPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.CSDPrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string CSDPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.ProviderID, EnumPropertyModifier.Mandatory)]
        public int ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public string ReceiverSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePrefixPhone { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_1.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            UpdateCellularDataRequest = new Equipment_1.EntMsgUpdateCellularDataRequest();
            UpdateCellularDataRequest.CSDPhoneNumber = CSDPhoneNumber;
            UpdateCellularDataRequest.CSDPrefixPhone = CSDPrefixPhone;
            UpdateCellularDataRequest.ProviderID = ProviderID;
            UpdateCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;
            UpdateCellularDataRequest.VoicePhoneNumber = VoicePhoneNumber;
            UpdateCellularDataRequest.VoicePrefixPhone = VoicePrefixPhone;
        }
    }


    public class CreateEntMsgUpdateCellularDataRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CSDPhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string CSDPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.CSDPrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string CSDPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.ProviderID, EnumPropertyModifier.Mandatory)]
        public int ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSerialNumber, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public string ReceiverSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePhoneNumber, EnumPropertyType.PhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePrefixPhone, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string VoicePrefixPhone { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Equipment_2.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }


        protected override void ExecuteBlock()
        {
            UpdateCellularDataRequest = new Equipment_2.EntMsgUpdateCellularDataRequest();
            UpdateCellularDataRequest.CSDPhoneNumber = CSDPhoneNumber;
            UpdateCellularDataRequest.CSDPrefixPhone = CSDPrefixPhone;
            UpdateCellularDataRequest.ProviderID = ProviderID;
            UpdateCellularDataRequest.ReceiverSerialNumber = ReceiverSerialNumber;
            UpdateCellularDataRequest.VoicePhoneNumber = VoicePhoneNumber;
            UpdateCellularDataRequest.VoicePrefixPhone = VoicePrefixPhone;
        }
    }
}
