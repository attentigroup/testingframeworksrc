﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgGetAuthorizedMobileListRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmMobileApplication ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetAuthorizedMobileListRequest GetAuthorizedMobileListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetAuthorizedMobileListRequest = new EntMsgGetAuthorizedMobileListRequest();
            GetAuthorizedMobileListRequest.ApplicationID = ApplicationID;
            GetAuthorizedMobileListRequest.MobileID = MobileID;
        }
    }
}
