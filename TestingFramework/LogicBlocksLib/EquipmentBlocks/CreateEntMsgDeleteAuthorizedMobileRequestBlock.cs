﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgDeleteAuthorizedMobileRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmMobileApplication ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgDeleteAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgDeleteAuthorizedMobileRequest DeleteAuthorizedMobileRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteAuthorizedMobileRequest.ApplicationID = ApplicationID;
            DeleteAuthorizedMobileRequest.MobileID = MobileID;
        }

        public CreateEntMsgDeleteAuthorizedMobileRequestBlock()
        {
            DeleteAuthorizedMobileRequest = new EntMsgDeleteAuthorizedMobileRequest();
        }
    }
}
