﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

using Equipment12_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace LogicBlocksLib.EquipmentBlocks
{
    public class CreateEntMsgGetEquipmentListRequest : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentModelArray, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment12_0.EnmEquipmentModel[] EquipmentModels { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment12_0.EnmEquipmentType EquipmentType { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment12_0.EnmProgramType[] ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }

        [PropertyTest(EnumPropertyName.ReturnDamageData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnDamageData { get; set; }

        [PropertyTest(EnumPropertyName.ReturnOnlyNotAllocated, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnOnlyNotAllocated { get; set; }

        [PropertyTest(EnumPropertyName.ReturnOnlyDeletable, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnOnlyDeletable { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListRequest, EnumPropertyType.EntMsgGetEquipmentListRequest, EnumPropertyModifier.Output)]
        public Equipment12_0.EntMsgGetEquipmentListRequest GetEquipmentListRequest { get; set; }

        public CreateEntMsgGetEquipmentListRequest()
        {
            AgencyID = -1;
        }
        protected override void ExecuteBlock()
        {
            GetEquipmentListRequest = new Equipment12_0.EntMsgGetEquipmentListRequest();
            if( AgencyID != -1)
            {
                GetEquipmentListRequest.AgencyID = new Nullable<int>(AgencyID);
            }
            GetEquipmentListRequest.Models = EquipmentModels;
            GetEquipmentListRequest.Type = EquipmentType;
            GetEquipmentListRequest.EquipmentID = EquipmentID;
            GetEquipmentListRequest.OffenderID = OffenderID;
            GetEquipmentListRequest.SerialNumber = EquipmentSerialNumber;
            GetEquipmentListRequest.IsDamaged = IsDamaged;
            GetEquipmentListRequest.ReturnDamageData = ReturnDamageData;
            GetEquipmentListRequest.ReturnOnlyNotAllocated = ReturnOnlyNotAllocated;
            GetEquipmentListRequest.Programs = ProgramType;
            
        }
    }
}
