﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgGetScheduleParameterRequest : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Version { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleParametersRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgGetScheduleParametersRequest GetScheduleParametersRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleParametersRequest = new Schedule0.EntMsgGetScheduleParametersRequest();
            GetScheduleParametersRequest.EntityID = EntityID;
            GetScheduleParametersRequest.EntityType = EntityType;
            GetScheduleParametersRequest.Version = Version;
        }
    }
}
