﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgUpdateScheduleRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AlcoholTestTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AlcoholTestTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.E4TimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string E4TimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.RFTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string RFTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.SkipDates, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule0.EntIgnoreTimeFrame[] SkipDates { get; set; }

        [PropertyTest(EnumPropertyName.Timeframes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule0.EntTimeFrameData[] Timeframes { get; set; }

        [PropertyTest(EnumPropertyName.UpdateEndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? UpdateEndTime { get; set; }

        [PropertyTest(EnumPropertyName.UpdateStartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? UpdateStartTime { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string VoiceTestTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.IsWeekly, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsWeekly { get; set; }
        
        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule0.EnmTimeFrameType TimeFrameType { get; set; }
        
        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? TimeframeID { get; set; }
        
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartTime { get; set; }
        
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int NumberOfTests { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateScheduleRequest = new Schedule0.EntMsgUpdateScheduleRequest();
            UpdateScheduleRequest.Timeframes = new Schedule0.EntTimeFrameData[]{new Schedule0.EntTimeFrameData()};
            UpdateScheduleRequest.AlcoholTestTimeStamp = AlcoholTestTimeStamp;
            UpdateScheduleRequest.E4TimeStamp = E4TimeStamp;
            UpdateScheduleRequest.EntityID = EntityID;
            UpdateScheduleRequest.EntityType = EntityType;
            UpdateScheduleRequest.RFTimeStamp = RFTimeStamp;
            UpdateScheduleRequest.SkipDates = SkipDates;
            //
            UpdateScheduleRequest.Timeframes[0].LocationID = LocationID;
            UpdateScheduleRequest.Timeframes[0].IsWeekly = IsWeekly;
            UpdateScheduleRequest.Timeframes[0].TimeFrameType = TimeFrameType;
            UpdateScheduleRequest.Timeframes[0].TimeframeID = TimeframeID;
            UpdateScheduleRequest.Timeframes[0].StartTime = StartTime;
            UpdateScheduleRequest.Timeframes[0].EndTime = EndTime;
            UpdateScheduleRequest.Timeframes[0].NumberOfTests = NumberOfTests;
            //
            if (UpdateScheduleRequest.Timeframes[0].TimeframeID == null)
                UpdateScheduleRequest.Timeframes = null;
            UpdateScheduleRequest.UpdateEndTime = UpdateEndTime;
            UpdateScheduleRequest.UpdateStartTime = UpdateStartTime;
            UpdateScheduleRequest.VoiceTestTimeStamp = VoiceTestTimeStamp;
        }
    }

    public class CreateEntMsgUpdateScheduleRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AlcoholTestTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AlcoholTestTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.E4TimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string E4TimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.RFTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string RFTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.SkipDates, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule1.EntIgnoreTimeFrame[] SkipDates { get; set; }

        [PropertyTest(EnumPropertyName.Timeframes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule1.EntTimeFrameData[] Timeframes { get; set; }

        [PropertyTest(EnumPropertyName.UpdateEndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? UpdateEndTime { get; set; }

        [PropertyTest(EnumPropertyName.UpdateStartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? UpdateStartTime { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string VoiceTestTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.IsWeekly, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsWeekly { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule1.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndTime { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateScheduleRequest = new Schedule1.EntMsgUpdateScheduleRequest();
            UpdateScheduleRequest.Timeframes = new Schedule1.EntTimeFrameData[] { new Schedule1.EntTimeFrameData() };
            UpdateScheduleRequest.AlcoholTestTimeStamp = AlcoholTestTimeStamp;
            UpdateScheduleRequest.E4TimeStamp = E4TimeStamp;
            UpdateScheduleRequest.EntityID = EntityID;
            UpdateScheduleRequest.EntityType = EntityType;
            UpdateScheduleRequest.RFTimeStamp = RFTimeStamp;
            UpdateScheduleRequest.SkipDates = SkipDates;
            //
            UpdateScheduleRequest.Timeframes[0].LocationID = LocationID;
            UpdateScheduleRequest.Timeframes[0].IsWeekly = IsWeekly;
            UpdateScheduleRequest.Timeframes[0].TimeFrameType = TimeFrameType;
            UpdateScheduleRequest.Timeframes[0].TimeframeID = TimeframeID;
            UpdateScheduleRequest.Timeframes[0].StartTime = StartTime;
            UpdateScheduleRequest.Timeframes[0].EndTime = EndTime;
            //
            if (UpdateScheduleRequest.Timeframes[0].TimeframeID == null)
                UpdateScheduleRequest.Timeframes = null;
            UpdateScheduleRequest.UpdateEndTime = UpdateEndTime;
            UpdateScheduleRequest.UpdateStartTime = UpdateStartTime;
            UpdateScheduleRequest.VoiceTestTimeStamp = VoiceTestTimeStamp;
        }
    }

    public class CreateEntMsgUpdateScheduleRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AlcoholTestTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AlcoholTestTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.E4TimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string E4TimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.RFTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string RFTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.SkipDates, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule2.EntIgnoreTimeFrame[] SkipDates { get; set; }

        [PropertyTest(EnumPropertyName.Timeframes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule2.EntTimeFrameData[] Timeframes { get; set; }

        [PropertyTest(EnumPropertyName.UpdateEndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? UpdateEndTime { get; set; }

        [PropertyTest(EnumPropertyName.UpdateStartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? UpdateStartTime { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string VoiceTestTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.IsWeekly, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsWeekly { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule2.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndTime { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateScheduleRequest = new Schedule2.EntMsgUpdateScheduleRequest();
            UpdateScheduleRequest.Timeframes = new Schedule2.EntTimeFrameData[] { new Schedule2.EntTimeFrameData() };
            UpdateScheduleRequest.AlcoholTestTimeStamp = AlcoholTestTimeStamp;
            UpdateScheduleRequest.E4TimeStamp = E4TimeStamp;
            UpdateScheduleRequest.EntityID = EntityID;
            UpdateScheduleRequest.EntityType = EntityType;
            UpdateScheduleRequest.RFTimeStamp = RFTimeStamp;
            UpdateScheduleRequest.SkipDates = SkipDates;
            //
            UpdateScheduleRequest.Timeframes[0].LocationID = LocationID;
            UpdateScheduleRequest.Timeframes[0].IsWeekly = IsWeekly;
            UpdateScheduleRequest.Timeframes[0].TimeFrameType = TimeFrameType;
            UpdateScheduleRequest.Timeframes[0].TimeframeID = TimeframeID;
            UpdateScheduleRequest.Timeframes[0].StartTime = StartTime;
            UpdateScheduleRequest.Timeframes[0].EndTime = EndTime;
            //
            if (UpdateScheduleRequest.Timeframes[0].TimeframeID == null)
                UpdateScheduleRequest.Timeframes = null;
            UpdateScheduleRequest.UpdateEndTime = UpdateEndTime;
            UpdateScheduleRequest.UpdateStartTime = UpdateStartTime;
            UpdateScheduleRequest.VoiceTestTimeStamp = VoiceTestTimeStamp;
        }
    }
}
