﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgDeleteTimeFrameRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TimeframeID { get; set; }
        
        [PropertyTest(EnumPropertyName.TimeframeIDsList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<int> TimeFrameIdList { get; set; }
        
        [PropertyTest(EnumPropertyName.DeleteTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgDeleteTimeFrameRequest DeleteTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteTimeFrameRequest = new Schedule0.EntMsgDeleteTimeFrameRequest();
            DeleteTimeFrameRequest.EntityID = EntityID;
            DeleteTimeFrameRequest.EntityType = EntityType;
            if(TimeframeID != 0)
                DeleteTimeFrameRequest.TimeframeID = TimeframeID;
            else if (!TimeFrameIdList.IsNullOrEmpty())
                DeleteTimeFrameRequest.TimeframeID = TimeFrameIdList.ToArray()[0];
        }
    }


    public class CreateEntMsgDeleteTimeFrameRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgDeleteTimeFrameRequest DeleteTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteTimeFrameRequest = new Schedule1.EntMsgDeleteTimeFrameRequest();
            DeleteTimeFrameRequest.EntityID = EntityID;
            DeleteTimeFrameRequest.EntityType = EntityType;
            DeleteTimeFrameRequest.TimeframeID = TimeframeID;
        }
    }


    public class CreateEntMsgDeleteTimeFrameRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgDeleteTimeFrameRequest DeleteTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteTimeFrameRequest = new Schedule2.EntMsgDeleteTimeFrameRequest();
            DeleteTimeFrameRequest.EntityID = EntityID;
            DeleteTimeFrameRequest.EntityType = EntityType;
            DeleteTimeFrameRequest.TimeframeID = TimeframeID;
        }
    }
}
