﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgGetScheduleRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Version { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleRequest = new Schedule0.EntMsgGetScheduleRequest();
            GetScheduleRequest.EntityID = EntityID;
            GetScheduleRequest.EntityType = EntityType;
            GetScheduleRequest.StartDate = StartDate;
            GetScheduleRequest.EndDate = EndDate;
            GetScheduleRequest.Version = Version;
        }
    }

    public class CreateEntMsgGetScheduleRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Version { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleRequest = new Schedule1.EntMsgGetScheduleRequest();
            GetScheduleRequest.EntityID = EntityID;
            GetScheduleRequest.EntityType = EntityType;
            GetScheduleRequest.StartDate = StartDate;
            GetScheduleRequest.EndDate = EndDate;
            GetScheduleRequest.Version = Version;
        }
    }

    public class CreateEntMsgGetScheduleRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Version { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetScheduleRequest = new Schedule2.EntMsgGetScheduleRequest();
            GetScheduleRequest.EntityID = EntityID;
            GetScheduleRequest.EntityType = EntityType;
            GetScheduleRequest.StartDate = StartDate;
            GetScheduleRequest.EndDate = EndDate;
            GetScheduleRequest.Version = Version;
        }
    }
}
