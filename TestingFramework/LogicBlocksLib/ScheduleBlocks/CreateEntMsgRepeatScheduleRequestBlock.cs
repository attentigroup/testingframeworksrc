﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgRepeatScheduleRequestBlock : LogicBlockBase

    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime Date { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }

        protected override void ExecuteBlock()
        {
            RepeatScheduleRequest = new Schedule0.EntMsgRepeatScheduleRequest();
            RepeatScheduleRequest.Date = Date;
            RepeatScheduleRequest.OffenderID = EntityID;
        }
    }

    public class CreateEntMsgRepeatScheduleRequestBlock_1 : LogicBlockBase

    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime Date { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }

        protected override void ExecuteBlock()
        {
            RepeatScheduleRequest = new Schedule1.EntMsgRepeatScheduleRequest();
            RepeatScheduleRequest.Date = Date;
            RepeatScheduleRequest.OffenderID = EntityID;
        }
    }

    public class CreateEntMsgRepeatScheduleRequestBlock_2 : LogicBlockBase

    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime Date { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }

        protected override void ExecuteBlock()
        {
            RepeatScheduleRequest = new Schedule2.EntMsgRepeatScheduleRequest();
            RepeatScheduleRequest.Date = Date;
            RepeatScheduleRequest.OffenderID = EntityID;
        }
    }
}
