﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgDeleteRecurringTimeFrameRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Date, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime Date { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteRecurringTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgDeleteRecurringTimeFrameInstance DeleteRequrringTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteRequrringTimeFrameRequest = new Schedule0.EntMsgDeleteRecurringTimeFrameInstance();
            DeleteRequrringTimeFrameRequest.Date = Date;
            DeleteRequrringTimeFrameRequest.EntityID = EntityID;
            DeleteRequrringTimeFrameRequest.EntityType = EntityType;
            DeleteRequrringTimeFrameRequest.TimeframeID = TimeframeID;
        }
    }


    public class CreateEntMsgDeleteRecurringTimeFrameRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Date, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime Date { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteRecurringTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgDeleteRecurringTimeFrameInstance DeleteRequrringTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteRequrringTimeFrameRequest = new Schedule1.EntMsgDeleteRecurringTimeFrameInstance();
            DeleteRequrringTimeFrameRequest.Date = Date;
            DeleteRequrringTimeFrameRequest.EntityID = EntityID;
            DeleteRequrringTimeFrameRequest.EntityType = EntityType;
            DeleteRequrringTimeFrameRequest.TimeframeID = TimeframeID;
        }
    }


    public class CreateEntMsgDeleteRecurringTimeFrameRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Date, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime Date { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteRecurringTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgDeleteRecurringTimeFrameInstance DeleteRequrringTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteRequrringTimeFrameRequest = new Schedule2.EntMsgDeleteRecurringTimeFrameInstance();
            DeleteRequrringTimeFrameRequest.Date = Date;
            DeleteRequrringTimeFrameRequest.EntityID = EntityID;
            DeleteRequrringTimeFrameRequest.EntityType = EntityType;
            DeleteRequrringTimeFrameRequest.TimeframeID = TimeframeID;
        }
    }


}
