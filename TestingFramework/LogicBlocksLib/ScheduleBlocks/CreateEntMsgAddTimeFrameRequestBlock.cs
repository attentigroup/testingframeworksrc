﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
using DataGenerators.Schedule;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgAddTimeFrameRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.IsWeekly, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsWeekly { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? NumberOfTests { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestCallType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule0.EnmVoiceTestCallType? VoiceTestCallType { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddTimeFrameRequest = new Schedule0.EntMsgAddTimeFrameRequest();
            AddTimeFrameRequest.EndTime = EndTime;
            AddTimeFrameRequest.EntityID = EntityID;
            AddTimeFrameRequest.EntityType = EntityType;
            AddTimeFrameRequest.IsWeekly = IsWeekly;
            AddTimeFrameRequest.LocationID = LocationID;
            if(NumberOfTests.HasValue == false)
                AddTimeFrameRequest.NumberOfTests = NumberOfTestsGenerator.GetNumberOfTestsByTimeRange(StartTime, EndTime, TimeFrameType, VoiceTestCallType);
            else
                AddTimeFrameRequest.NumberOfTests = NumberOfTests;
            AddTimeFrameRequest.StartTime = StartTime;
            AddTimeFrameRequest.TimeFrameType = TimeFrameType;
            AddTimeFrameRequest.VoiceTestCallType = VoiceTestCallType;
        }
    }


    public class CreateEntMsgAddTimeFrameRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.IsWeekly, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsWeekly { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? NumberOfTests { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestCallType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule1.EnmVoiceTestCallType? VoiceTestCallType { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddTimeFrameRequest = new Schedule1.EntMsgAddTimeFrameRequest();
            AddTimeFrameRequest.EndTime = EndTime;
            AddTimeFrameRequest.EntityID = EntityID;
            AddTimeFrameRequest.EntityType = EntityType;
            AddTimeFrameRequest.IsWeekly = IsWeekly;
            AddTimeFrameRequest.LocationID = LocationID;
            AddTimeFrameRequest.NumberOfTests = NumberOfTests;
            AddTimeFrameRequest.StartTime = StartTime;
            AddTimeFrameRequest.TimeFrameType = TimeFrameType;
            AddTimeFrameRequest.VoiceTestCallType = VoiceTestCallType;
        }
    }


    public class CreateEntMsgAddTimeFrameRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.IsWeekly, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsWeekly { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? NumberOfTests { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestCallType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule2.EnmVoiceTestCallType? VoiceTestCallType { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddTimeFrameRequest = new Schedule2.EntMsgAddTimeFrameRequest();
            AddTimeFrameRequest.EndTime = EndTime;
            AddTimeFrameRequest.EntityID = EntityID;
            AddTimeFrameRequest.EntityType = EntityType;
            AddTimeFrameRequest.IsWeekly = IsWeekly;
            AddTimeFrameRequest.LocationID = LocationID;
            AddTimeFrameRequest.NumberOfTests = NumberOfTests;
            AddTimeFrameRequest.StartTime = StartTime;
            AddTimeFrameRequest.TimeFrameType = TimeFrameType;
            AddTimeFrameRequest.VoiceTestCallType = VoiceTestCallType;
        }
    }
}
