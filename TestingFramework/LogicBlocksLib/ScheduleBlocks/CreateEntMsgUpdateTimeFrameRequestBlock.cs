﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
#endregion

namespace LogicBlocksLib.ScheduleBlocks
{
    public class CreateEntMsgUpdateTimeFrameRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? NumberOfTests { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestCallType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule0.EnmVoiceTestCallType? VoiceTestCallType { get; set; }


        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule0.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdateTimeFrameRequest = new Schedule0.EntMsgUpdateTimeFrameRequest();
            UpdateTimeFrameRequest.EndTime = EndTime;
            UpdateTimeFrameRequest.EntityID = EntityID;
            UpdateTimeFrameRequest.EntityType = EntityType;
            UpdateTimeFrameRequest.LocationID = LocationID;
            UpdateTimeFrameRequest.NumberOfTests = NumberOfTests;
            UpdateTimeFrameRequest.StartTime = StartTime;
            UpdateTimeFrameRequest.TimeframeID = TimeframeID;
            UpdateTimeFrameRequest.TimeFrameType = TimeFrameType;
            UpdateTimeFrameRequest.VoiceTestCallType = VoiceTestCallType;
        }
    }


    public class CreateEntMsgUpdateTimeFrameRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? NumberOfTests { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestCallType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule1.EnmVoiceTestCallType? VoiceTestCallType { get; set; }


        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule1.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdateTimeFrameRequest = new Schedule1.EntMsgUpdateTimeFrameRequest();
            UpdateTimeFrameRequest.EndTime = EndTime;
            UpdateTimeFrameRequest.EntityID = EntityID;
            UpdateTimeFrameRequest.EntityType = EntityType;
            UpdateTimeFrameRequest.LocationID = LocationID;
            UpdateTimeFrameRequest.NumberOfTests = NumberOfTests;
            UpdateTimeFrameRequest.StartTime = StartTime;
            UpdateTimeFrameRequest.TimeframeID = TimeframeID;
            UpdateTimeFrameRequest.TimeFrameType = TimeFrameType;
            UpdateTimeFrameRequest.VoiceTestCallType = VoiceTestCallType;
        }
    }


    public class CreateEntMsgUpdateTimeFrameRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EntityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EntityID { get; set; }

        [PropertyTest(EnumPropertyName.EntityType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmEntityType EntityType { get; set; }

        [PropertyTest(EnumPropertyName.LocationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? LocationID { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? NumberOfTests { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeframeID { get; set; }

        [PropertyTest(EnumPropertyName.TimeFrameType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EnmTimeFrameType TimeFrameType { get; set; }

        [PropertyTest(EnumPropertyName.VoiceTestCallType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule2.EnmVoiceTestCallType? VoiceTestCallType { get; set; }


        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Schedule2.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }



        protected override void ExecuteBlock()
        {
            UpdateTimeFrameRequest = new Schedule2.EntMsgUpdateTimeFrameRequest();
            UpdateTimeFrameRequest.EndTime = EndTime;
            UpdateTimeFrameRequest.EntityID = EntityID;
            UpdateTimeFrameRequest.EntityType = EntityType;
            UpdateTimeFrameRequest.LocationID = LocationID;
            UpdateTimeFrameRequest.NumberOfTests = NumberOfTests;
            UpdateTimeFrameRequest.StartTime = StartTime;
            UpdateTimeFrameRequest.TimeframeID = TimeframeID;
            UpdateTimeFrameRequest.TimeFrameType = TimeFrameType;
            UpdateTimeFrameRequest.VoiceTestCallType = VoiceTestCallType;
        }
    }
}
