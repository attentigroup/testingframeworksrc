﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.Common;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;
#endregion

namespace LogicBlocksLib.Information
{
    public class CreateAdditionalDataAddFileRequestBlock : LogicBlockBase
    {
       

        [PropertyTest(EnumPropertyName.FileDataField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FileDataField { get; set; }

        [PropertyTest(EnumPropertyName.FileDisplayName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FileDisplayName { get; set; }

        [PropertyTest(EnumPropertyName.FileName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FileName { get; set; }

        [PropertyTest(EnumPropertyName.OwnerEntity, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmOwnerEntity OwnerEntity { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.FilePath, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FilePath { get; set; }

         [PropertyTest(EnumPropertyName.UploadFileRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Information0.EntMsgUploadFileRequest UploadFileRequest { get; set; }

        [PropertyTest(EnumPropertyName.FileData, EnumPropertyType.None, EnumPropertyModifier.Output )]
        public Information0.EntFileData FileData { get; set; }


        protected override void ExecuteBlock()
        {
            UploadFileRequest = new Information0.EntMsgUploadFileRequest();
            UploadFileRequest.OwnerEntityID = OffenderID;
            UploadFileRequest.FileDisplayName = FileDisplayName;
            UploadFileRequest.FileName = FileName;
            UploadFileRequest.OwnerEntity = OwnerEntity;

            FileData = new Information0.EntFileData();
            string dirBase = System.AppDomain.CurrentDomain.BaseDirectory;
            var dir = Path.Combine(dirBase, @FilePath);
            FileData.FileContent = ImageConverter.GetBytesFromImage(dir);
            UploadFileRequest.FileData = FileData;


        }
    }
}
