﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace LogicBlocksLib.Information
{
    public class CreateChangeVisibilityForCustomFieldsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.ShowInOffendersList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowInOffendersList { get; set; }

        [PropertyTest(EnumPropertyName.ShowInOffenderDetails, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowInOffenderDetails { get; set; }

        protected override void ExecuteBlock()
        {
            foreach (var customField in CustomFields)
            {
                customField.ShowInOffenderDetails = ShowInOffenderDetails;
                customField.ShowInOffendersList = ShowInOffendersList;
            }

        }
    }
}
