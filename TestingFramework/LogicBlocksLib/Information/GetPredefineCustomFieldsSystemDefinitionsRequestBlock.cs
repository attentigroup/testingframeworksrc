﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestBlocksLib.InformationBlock
{
    public class GetPredefineCustomFieldsSystemDefinitionsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsSystemDefinitions { get; set; }

        [PropertyTest(EnumPropertyName.PredefineCustomFields, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntCustomFieldSystemDefinition> PredefinedCustomFields { get; set; }

        protected override void ExecuteBlock()
        {
            PredefinedCustomFields = CustomFieldsSystemDefinitions.FindAll(x => x.Type == EnmFieldType.Predefiend);

        }
    }
}
