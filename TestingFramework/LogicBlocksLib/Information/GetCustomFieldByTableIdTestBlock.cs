﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;


namespace TestBlocksLib.InformationBlock
{
    public class GetCustomFieldByTableIdTestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsDefinitions { get; set; }

        [PropertyTest(EnumPropertyName.TableID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string TableID { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTableID, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTableID { get; set; }

        public GetCustomFieldByTableIdTestBlock()
        {
            CustomFieldsByTableID = new List<EntCustomFieldSystemDefinition>();
        }
        protected override void ExecuteBlock()
        {
            var customField = CustomFieldsDefinitions.FirstOrDefault(x => x.TableID == TableID);
            CustomFieldsByTableID.Add(customField);

        }
    }
}
