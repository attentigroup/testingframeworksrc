﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace LogicBlocksLib.Information
{
    public class CreateEntMsgAddOffenderWarningRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderWarningRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Information0.EntMsgAddOffenderWarningRequest AddOffenderWarningRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderWarningRequest = new Information0.EntMsgAddOffenderWarningRequest();
            AddOffenderWarningRequest.OffenderID = OffenderID;
            AddOffenderWarningRequest.Description = Description;
           
        }
    }
}
