﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace LogicBlocksLib.Information
{
    public class CreateEntMsgAddAuthorizedAbsenceDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ActionIfAbsenceConfirmViolated, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ActionIfAbsenceConfirmViolated { get; set; }

        [PropertyTest(EnumPropertyName.ActionToConfirmFollowUp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ActionToConfirmFollowUp { get; set; }

        [PropertyTest(EnumPropertyName.Code, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Code { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.IsActionAuthenticationConfirmed, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool? IsActionAuthenticationConfirmed { get; set; }

        [PropertyTest(EnumPropertyName.IsApproved, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool? IsApproved { get; set; }

        [PropertyTest(EnumPropertyName.IsFollowUpCompleted, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool? IsFollowUpCompleted { get; set; }

        [PropertyTest(EnumPropertyName.NonApprovalDescription, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string NonApprovalDescription { get; set; }

        [PropertyTest(EnumPropertyName.AddAuthorizedAbsenceDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Information0.EntMsgAddAuthorizedAbsenceDataRequest AddAuthorizedAbsenceDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddAuthorizedAbsenceDataRequest = new Information0.EntMsgAddAuthorizedAbsenceDataRequest();
            AddAuthorizedAbsenceDataRequest.OffenderID = OffenderID;
            AddAuthorizedAbsenceDataRequest.ActionIfAbsenceConfirmViolated = ActionIfAbsenceConfirmViolated;
            AddAuthorizedAbsenceDataRequest.ActionToConfirmFollowUp = ActionToConfirmFollowUp;
            AddAuthorizedAbsenceDataRequest.Code = Code;
            AddAuthorizedAbsenceDataRequest.Description = Description;
            AddAuthorizedAbsenceDataRequest.IsActionAuthenticationConfirmed = IsActionAuthenticationConfirmed;
            AddAuthorizedAbsenceDataRequest.IsApproved = IsApproved;
            AddAuthorizedAbsenceDataRequest.IsFollowUpCompleted = IsFollowUpCompleted;
            AddAuthorizedAbsenceDataRequest.NonApprovalDescription = NonApprovalDescription;

        }
    }
}
