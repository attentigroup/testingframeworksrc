﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Information0 = TestingFramework.Proxies.EM.Interfaces.Information;

namespace LogicBlocksLib.Information
{
    public class CreateEntMsgAddScheduleChangeRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.Code, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Code { get; set; }

        [PropertyTest(EnumPropertyName.AuthorizedAbsenceRequestID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? AuthorizedAbsenceRequestID { get; set; }

        [PropertyTest(EnumPropertyName.AddScheduleChangeRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Information0.EntMsgAddScheduleChangeRequest AddScheduleChangeRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddScheduleChangeRequest = new Information0.EntMsgAddScheduleChangeRequest();
            AddScheduleChangeRequest.OffenderID = OffenderID;
            AddScheduleChangeRequest.Description = Description;
            AddScheduleChangeRequest.Code = Code;
            AddScheduleChangeRequest.AuthorizedAbsenceRequestID = AuthorizedAbsenceRequestID;

        }
    }
}
