﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgDeleteOffenderPictureRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PhotoID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int PhotoID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderPicture, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgDeletePictureRequest DeleteOffenderPicture { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteOffenderPicture = new Offenders0.EntMsgDeletePictureRequest();
            DeleteOffenderPicture.OffenderID = OffenderID;
            DeleteOffenderPicture.PhotoID = PhotoID;
        }
    }


    public class CreateEntMsgDeleteOffenderPictureRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PhotoID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int PhotoID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderPicture, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgDeleteOffenderPicture DeleteOffenderPicture { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteOffenderPicture = new Offenders1.EntMsgDeleteOffenderPicture();
            DeleteOffenderPicture.OffenderID = OffenderID;
            DeleteOffenderPicture.PictureID = PhotoID;
        }
    }


    public class CreateEntMsgDeleteOffenderPictureRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PhotoID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int PhotoID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderPicture, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgDeleteOffenderPicture DeleteOffenderPicture { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteOffenderPicture = new Offenders2.EntMsgDeleteOffenderPicture();
            DeleteOffenderPicture.OffenderID = OffenderID;
            DeleteOffenderPicture.PictureID = PhotoID;
        }
    }
}
