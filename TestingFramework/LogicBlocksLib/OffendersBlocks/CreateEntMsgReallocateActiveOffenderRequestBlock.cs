﻿using System;
using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;
using DataGenerators.Agency;

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgReallocateActiveOffenderRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.NewAgencyID, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int NewAgencyID { get; set; }

        [PropertyTest(EnumPropertyName.AgencyId, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int oldAgencyId { get; set; }

        [PropertyTest(EnumPropertyName.NewOfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int NewOfficerID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Reason, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Reason { get; set; }



        [PropertyTest(EnumPropertyName.ReallocateActiveOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders12_0.EntMsgReallocateActiveOffenderRequest ReallocateActiveOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ReallocateActiveOffenderRequest = new Offenders12_0.EntMsgReallocateActiveOffenderRequest();
            ReallocateActiveOffenderRequest.Comment = Comment;
            ReallocateActiveOffenderRequest.NewAgencyID = AgencyIdGenerator.GenerateDiffenetAgencyId(oldAgencyId);
            ReallocateActiveOffenderRequest.NewOfficerID = AgencyIdGenerator.GetOfficerByAgencyId(ReallocateActiveOffenderRequest.NewAgencyID);
            ReallocateActiveOffenderRequest.OffenderID = OffenderID;
            ReallocateActiveOffenderRequest.Reason = Reason;
        }
    }
}
