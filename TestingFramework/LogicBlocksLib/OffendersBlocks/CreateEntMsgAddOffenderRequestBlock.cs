﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using DataGenerators.Agency;
using LogicBlocksLib.ZonesBlocks;
using TestingFramework.Proxies.EM.Interfaces.Zones12_0;
using TestingFramework.Proxies.API.Zones;
using DataGenerators.Offender;
using System.Collections.Generic;
using TestingFramework.TestsInfraStructure.Factories;
using DataGenerators;
using System.Linq;
#endregion


namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgAddOffenderRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.None)] //CityID is generated from the ExecuteBlock
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.AllowVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AllowVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.ContactPhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string ContactPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.ContactPrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CourtOrderNumber, EnumPropertyType.CourtOrderNumber, EnumPropertyModifier.Mandatory)]
        public string CourtOrderNumber { get; set; }

        [PropertyTest(EnumPropertyName.DateOfBirth, EnumPropertyType.DateOfBirth, EnumPropertyModifier.Mandatory)]
        public DateTime? DateOfBirth { get; set; }

        [PropertyTest(EnumPropertyName.DepartmentOfCorrection, EnumPropertyType.DepartmentOfCorrection, EnumPropertyModifier.Mandatory)]
        public string DepartmentOfCorrection { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Gender, EnumPropertyType.Gender, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmGender Gender { get; set; }

        [PropertyTest(EnumPropertyName.HomeUnit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HomeUnit { get; set; }

        [PropertyTest(EnumPropertyName.IsSecondaryLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsSecondaryLocation { get; set; }

        [PropertyTest(EnumPropertyName.LandlineDialOutPrefix, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LandlineDialOutPrefix { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string LandlinePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LandlinePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.Language, EnumPropertyModifier.Mandatory)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramConcept ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.ProgramEnd, EnumPropertyType.ProgramEnd, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramEnd { get; set; }

        [PropertyTest(EnumPropertyName.ProgramStart, EnumPropertyType.ProgramStart, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramStart { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.OffenderRefID, EnumPropertyModifier.Mandatory)]
        public string RefID { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.SocialSecurity, EnumPropertyModifier.Mandatory)]
        public string SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.StateId, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.Title, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmTitle Title { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.VisibleCustomFieldsDefinitions, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntCustomFieldDefinition[] VisibleCustomFieldsDefinitions { get; set; }



        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }
        
        public CreateEntMsgAddOffenderRequestBlock()
        {
            OfficerID = -1;
            RelatedOffenderID = -1;
        }
        
        protected override void ExecuteBlock() {
            
            AddOffenderRequest = new Offenders0.EntMsgAddOffenderRequest();
            AddOffenderRequest.AgencyID = AgencyID; //
            if (CityID == null)
                AddOffenderRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                AddOffenderRequest.CityID = CityID;
            AddOffenderRequest.ContactPhoneNumber = ContactPhoneNumber; //
            AddOffenderRequest.ContactPrefixPhone = ContactPrefixPhone; //
            AddOffenderRequest.CourtOrderNumber = CourtOrderNumber; //
            AddOffenderRequest.DateOfBirth = DateOfBirth; //
            AddOffenderRequest.DepartmentOfCorrection = DepartmentOfCorrection; //
            AddOffenderRequest.FirstName = FirstName; //
            AddOffenderRequest.Gender = Gender; //
            AddOffenderRequest.HomeUnit = HomeUnit; //
            AddOffenderRequest.IsSecondaryLocation = IsSecondaryLocation; //
            AddOffenderRequest.LandlineDialOutPrefix = LandlineDialOutPrefix; //
            AddOffenderRequest.LandlinePhoneNumber = LandlinePhoneNumber; //
            AddOffenderRequest.LandlinePrefixPhone = LandlinePrefixPhone; //
            AddOffenderRequest.Language = Language; //
            AddOffenderRequest.LastName = LastName; //
            AddOffenderRequest.MiddleName = MiddleName; // 

            if(AgencyID != 0 && OfficerID == -1)//in case officer id was not supplied get the officer that fit the agency
            {
                AddOffenderRequest.OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID);
                OfficerID = AddOffenderRequest.OfficerID;////update the class property for later use
            }       
            AddOffenderRequest.OfficerID = OfficerID;

            AddOffenderRequest.ProgramConcept = ProgramConcept; //
            AddOffenderRequest.ProgramEnd = ProgramEnd; //
            AddOffenderRequest.ProgramStart = ProgramStart; //
            AddOffenderRequest.ProgramType = ProgramType; //
            AddOffenderRequest.Receiver = Receiver; //
            AddOffenderRequest.RefID = RefID; //
            if(RelatedOffenderID != -1)
                AddOffenderRequest.RelatedOffenderID = new Nullable<int>(RelatedOffenderID); //
            AddOffenderRequest.SocialSecurity = SocialSecurity; //
            AddOffenderRequest.StateID = StateID; //
            AddOffenderRequest.Street = Street; //
            AddOffenderRequest.Title = Title; //
            AddOffenderRequest.Transmitter = Transmitter; //
            AddOffenderRequest.ZipCode = ZipCode; //
            AddOffenderRequest.AllowVoiceTests = AllowVoiceTests; //
            if (VisibleCustomFieldsDefinitions != null && VisibleCustomFieldsDefinitions.Length > 0)
            {
                AddOffenderRequest.CustomFields = new Dictionary<int, object>();
                foreach (var field in VisibleCustomFieldsDefinitions)
                {
                    var value = GenerateRandomValueForCustomField(field);
                    AddOffenderRequest.CustomFields.Add(field.ID, value);

                }
            }
            else
                AddOffenderRequest.CustomFields = null;
        }

        private object GenerateRandomValueForCustomField(Offenders0.EntCustomFieldDefinition cfd)
        {
            object result = null;
            var dgf = DataGeneratorFactory.Instance;
            switch (cfd.Type)
            {
                case Offenders0.EnmFieldType.Numeric:
                    result = dgf.GenerateData(EnumPropertyType.Int, null);
                    break;
                case Offenders0.EnmFieldType.String:
                    result = dgf.GenerateData(EnumPropertyType.String, null);
                    break;
                case Offenders0.EnmFieldType.Date:
                    result = DateTime.Now;
                    break;
                case Offenders0.EnmFieldType.Predefiend:
                    result = GenerateRandomValueForListCustomField((Offenders0.EntCustomFieldListDefinition)cfd);
                    break;
                default:
                    Log.Error($"EnmFieldType {cfd.Type} not supported");
                    break;
            }
            return result;
        }

        private object GenerateRandomValueForListCustomField(Offenders0.EntCustomFieldListDefinition cfd)
        {
            object result = null;
            var dgf = DataGeneratorFactory.Instance;
            var intGenerator = dgf.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var listValues = cfd.ListValues.Keys.ToList();
            int selectedValue = intGenerator.GenerateData(0, listValues.Count - 1);

            result = listValues[selectedValue];

            return result;
        }
    }
    
    public class CreateEntMsgAddOffenderRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.AllowVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AllowVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.ContactPhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string ContactPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.ContactPrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CourtOrderNumber, EnumPropertyType.CourtOrderNumber, EnumPropertyModifier.Mandatory)]
        public string CourtOrderNumber { get; set; }

        [PropertyTest(EnumPropertyName.DateOfBirth, EnumPropertyType.DateOfBirth, EnumPropertyModifier.Mandatory)]
        public DateTime? DateOfBirth { get; set; }

        [PropertyTest(EnumPropertyName.DepartmentOfCorrection, EnumPropertyType.DepartmentOfCorrection, EnumPropertyModifier.Mandatory)]
        public string DepartmentOfCorrection { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Gender, EnumPropertyType.Gender_1, EnumPropertyModifier.Mandatory)]
        public Offenders1.EnmGender Gender { get; set; }

        [PropertyTest(EnumPropertyName.HomeUnit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HomeUnit { get; set; }

        [PropertyTest(EnumPropertyName.IsSecondaryLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsSecondaryLocation { get; set; }

        [PropertyTest(EnumPropertyName.LandlineDialOutPrefix, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LandlineDialOutPrefix { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string LandlinePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LandlinePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.Language, EnumPropertyModifier.Mandatory)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EnmProgramConcept ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.ProgramEnd, EnumPropertyType.ProgramEnd, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramEnd { get; set; }

        [PropertyTest(EnumPropertyName.ProgramStart, EnumPropertyType.ProgramStart, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramStart { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.OffenderRefID, EnumPropertyModifier.Mandatory)]
        public string RefID { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.SocialSecurity, EnumPropertyModifier.Mandatory)]
        public string SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.StateId, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.Title_1, EnumPropertyModifier.Mandatory)]
        public Offenders1.EnmTitle Title { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }


        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        public CreateEntMsgAddOffenderRequestBlock_1()
        {
            RelatedOffenderID = -1;
        }


        protected override void ExecuteBlock()
        {

            AddOffenderRequest = new Offenders1.EntMsgAddOffenderRequest();
            AddOffenderRequest.AgencyID = AgencyID; //

            if (CityID == null)
                AddOffenderRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                AddOffenderRequest.CityID = CityID;

            AddOffenderRequest.ContactPhoneNumber = ContactPhoneNumber; //
            AddOffenderRequest.ContactPrefixPhone = ContactPrefixPhone; //
            AddOffenderRequest.CourtOrderNumber = CourtOrderNumber; //
            AddOffenderRequest.DateOfBirth = DateOfBirth; //
            AddOffenderRequest.DepartmentOfCorrection = DepartmentOfCorrection; //
            AddOffenderRequest.FirstName = FirstName; //
            AddOffenderRequest.Gender = Gender; //
            AddOffenderRequest.HomeUnit = HomeUnit; //
            AddOffenderRequest.IsSecondaryLocation = IsSecondaryLocation; //
            AddOffenderRequest.LandlineDialOutPrefix = LandlineDialOutPrefix; //
            AddOffenderRequest.LandlinePhoneNumber = LandlinePhoneNumber; //
            AddOffenderRequest.LandlinePrefixPhone = LandlinePrefixPhone; //
            AddOffenderRequest.Language = Language; //
            AddOffenderRequest.LastName = LastName; //
            AddOffenderRequest.MiddleName = MiddleName; //
            //get the officer that fit the agency
            AddOffenderRequest.OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID); //
            AddOffenderRequest.ProgramConcept = ProgramConcept; //
            AddOffenderRequest.ProgramEnd = ProgramEnd; //
            AddOffenderRequest.ProgramStart = ProgramStart; //
            AddOffenderRequest.ProgramType = ProgramType; //
            AddOffenderRequest.Receiver = Receiver; //
            AddOffenderRequest.RefID = RefID; //
            if (RelatedOffenderID != -1)
                AddOffenderRequest.RelatedOffenderID = new Nullable<int>(RelatedOffenderID); //
            AddOffenderRequest.SocialSecurity = SocialSecurity; //
            AddOffenderRequest.StateID = StateID; //
            AddOffenderRequest.Street = Street; //
            AddOffenderRequest.Title = Title; //
            AddOffenderRequest.Transmitter = Transmitter; //
            AddOffenderRequest.ZipCode = ZipCode; //
            AddOffenderRequest.AllowVoiceTests = AllowVoiceTests; //
        }
    }


    public class CreateEntMsgAddOffenderRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.AllowVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AllowVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.ContactPhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string ContactPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.ContactPrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CourtOrderNumber, EnumPropertyType.CourtOrderNumber, EnumPropertyModifier.Mandatory)]
        public string CourtOrderNumber { get; set; }

        [PropertyTest(EnumPropertyName.CSDPrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CSDPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CSDPhoneNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CSDPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.DateOfBirth, EnumPropertyType.DateOfBirth, EnumPropertyModifier.Mandatory)]
        public DateTime? DateOfBirth { get; set; }

        [PropertyTest(EnumPropertyName.DepartmentOfCorrection, EnumPropertyType.DepartmentOfCorrection, EnumPropertyModifier.Mandatory)]
        public string DepartmentOfCorrection { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Gender, EnumPropertyType.Gender_2, EnumPropertyModifier.Mandatory)]
        public Offenders2.EnmGender Gender { get; set; }

        [PropertyTest(EnumPropertyName.HomeUnit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HomeUnit { get; set; }

        [PropertyTest(EnumPropertyName.IsPrimaryLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsPrimaryLocation { get; set; }

        [PropertyTest(EnumPropertyName.LandlineDialOutPrefix, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LandlineDialOutPrefix { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string LandlinePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LandlinePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.Language, EnumPropertyModifier.Mandatory)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EnmProgramConcept ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.ProgramEnd, EnumPropertyType.ProgramEnd, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramEnd { get; set; }

        [PropertyTest(EnumPropertyName.ProgramStart, EnumPropertyType.ProgramStart, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramStart { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.ProviderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ProviderID { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.OffenderRefID, EnumPropertyModifier.Mandatory)]
        public string RefID { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.SocialSecurity, EnumPropertyModifier.Mandatory)]
        public string SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.StateId, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.Title_2, EnumPropertyModifier.Mandatory)]
        public Offenders2.EnmTitle Title { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.VoicePhoneNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string VoicePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string VoicePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }


        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }
        
        public CreateEntMsgAddOffenderRequestBlock_2()
        {
            RelatedOffenderID = -1;
        }


        protected override void ExecuteBlock()
        {
            AddOffenderRequest = new Offenders2.EntMsgAddOffenderRequest();
            AddOffenderRequest.AgencyID = AgencyID; //

            if (CityID == null)
                AddOffenderRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                AddOffenderRequest.CityID = CityID;

            AddOffenderRequest.ContactPhoneNumber = ContactPhoneNumber; //
            AddOffenderRequest.ContactPrefixPhone = ContactPrefixPhone; //
            AddOffenderRequest.CourtOrderNumber = CourtOrderNumber; //
            AddOffenderRequest.DateOfBirth = DateOfBirth; //
            AddOffenderRequest.DepartmentOfCorrection = DepartmentOfCorrection; //
            AddOffenderRequest.FirstName = FirstName; //
            AddOffenderRequest.Gender = Gender; //
            AddOffenderRequest.HomeUnit = HomeUnit; //
            AddOffenderRequest.IsPrimaryLocation = IsPrimaryLocation; //
            AddOffenderRequest.LandlineDialOutPrefix = LandlineDialOutPrefix; //
            AddOffenderRequest.LandlinePhoneNumber = LandlinePhoneNumber; //
            AddOffenderRequest.LandlinePrefixPhone = LandlinePrefixPhone; //
            AddOffenderRequest.Language = Language; //
            AddOffenderRequest.LastName = LastName; //
            AddOffenderRequest.MiddleName = MiddleName; //
            //get the officer that fit the agency
            AddOffenderRequest.OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID); //
            AddOffenderRequest.ProgramConcept = ProgramConcept; //
            AddOffenderRequest.ProgramEnd = ProgramEnd; //
            AddOffenderRequest.ProgramStart = ProgramStart; //
            AddOffenderRequest.ProgramType = ProgramType; //
            AddOffenderRequest.Receiver = Receiver; //
            AddOffenderRequest.RefID = RefID; //
            if (RelatedOffenderID != -1)
                AddOffenderRequest.RelatedOffenderID = new Nullable<int>(RelatedOffenderID); //
            AddOffenderRequest.SocialSecurity = SocialSecurity; //
            AddOffenderRequest.StateID = StateID; //
            AddOffenderRequest.Street = Street; //
            AddOffenderRequest.Title = Title; //
            AddOffenderRequest.Transmitter = Transmitter; //
            AddOffenderRequest.ZipCode = ZipCode; //
            AddOffenderRequest.AllowVoiceTests = AllowVoiceTests; //
            AddOffenderRequest.VoicePhoneNumber = VoicePhoneNumber;
            AddOffenderRequest.VoicePrefixPhone = VoicePrefixPhone;
            AddOffenderRequest.ProviderID = ProviderID;
            AddOffenderRequest.CSDPhoneNumber = CSDPhoneNumber;
            AddOffenderRequest.CSDPrefixPhone = CSDPrefixPhone;
        }
    }
}
