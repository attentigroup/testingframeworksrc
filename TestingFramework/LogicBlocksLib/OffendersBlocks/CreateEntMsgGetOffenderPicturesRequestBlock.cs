﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgGetOffenderPicturesRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PictureID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int PictureID { get; set; }

        [PropertyTest(EnumPropertyName.GetPicturesRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetPicturesRequest GetPicturesRequest { get; set; }


        protected override void ExecuteBlock()
        {
            GetPicturesRequest = new Offenders0.EntMsgGetPicturesRequest();
            GetPicturesRequest.OffenderID = OffenderID;
            GetPicturesRequest.PictureID = PictureID != 0 ? PictureID : (int?)null;
        }
    }
}
