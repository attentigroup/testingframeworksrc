﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.Collections.Generic;
using DataGenerators.Agency;
using TestingFramework.TestsInfraStructure.Factories;
using DataGenerators;
using System.Linq;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using DataGenerators.Offender;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgUpdateOffenderWithSpecificCustomFieldValueRequestBlock : CreateEntMsgUpdateOffenderRequestBlock
    {
        
        [PropertyTest(EnumPropertyName.CustomFieldID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int CustomFieldID { get; set; }


        [PropertyTest(EnumPropertyName.CustomFieldValue, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CustomFieldValueCode { get; set; }

        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            
            if (CustomFieldID != 0 && CustomFieldValueCode != null)
            {
                UpdateOffenderRequest.CustomFields = new Dictionary<int, object>();
                var field = VisibleCustomFieldsDefinitions.First(x => x.ID == CustomFieldID);

                var value = InsertValueToCustomField(field, CustomFieldValueCode);
                UpdateOffenderRequest.CustomFields.Add(field.ID, value);

            }
            else
                UpdateOffenderRequest.CustomFields = null;
        }

        private object InsertValueToCustomField(Offenders0.EntCustomFieldDefinition cfd, string customFieldValue)
        {
            object result = null;
            switch (cfd.Type)
            {
                case Offenders0.EnmFieldType.Numeric:
                    result = int.Parse(customFieldValue); 
                    break;
                case Offenders0.EnmFieldType.String:
                    result = customFieldValue;
                    break;
                case Offenders0.EnmFieldType.Date:
                    result = DateTime.Parse(customFieldValue);
                    break;
                case Offenders0.EnmFieldType.Predefiend:
                    result = InsertValueToCustomFieldList((Offenders0.EntCustomFieldListDefinition)cfd, customFieldValue);
                    break;
                default:
                    Log.Error($"EnmFieldType {cfd.Type} not supported");
                    break;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cfd"></param>
        /// <param name="customFieldValue"></param>
        /// <returns></returns>
        private object InsertValueToCustomFieldList(Offenders0.EntCustomFieldListDefinition cfd, string customFieldValue)
        {
            object result = null;
            var listValues = cfd.ListValues.Keys.ToList();
            var isExist = listValues.Contains(customFieldValue);
            result = customFieldValue;
            return result;
        }

        //
        private object GenerateRandomValueForListCustomField(Offenders0.EntCustomFieldListDefinition cfd)
        {
            object result = null;
            var dgf = DataGeneratorFactory.Instance;
            var intGenerator = dgf.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var listValues = cfd.ListValues.Keys.ToList();
            int selectedValue = intGenerator.GenerateData(0, listValues.Count - 1);

            result = listValues[selectedValue];

            return result;
        }
    }

}
