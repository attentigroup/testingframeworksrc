﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgGetOffendersListRequest : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EnmProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramStatus[] ProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramConcept? ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramType[] ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.OffendersSortOptionsSortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmOffendersSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.ListSortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.AgencyIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntNumericQueryParameter AgencyIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.AgencyIDNumericOperator, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmNumericOperator AgencyIDNumericOperator { get; set; }

        [PropertyTest(EnumPropertyName.AgencyIDValue, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? AgencyIDValue { get; set; }


        [PropertyTest(EnumPropertyName.EndOfServiceCodeQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter EndOfServiceCodeQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? GroupID { get; set; }

        [PropertyTest(EnumPropertyName.FirstNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter FirstNameQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.IncludeAddressList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeAddressList { get; set; }

        [PropertyTest(EnumPropertyName.LastNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter LastNameQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimeStamp { get; set; }

        //[PropertyTest(EnumPropertyName.OffenderIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        //public Offenders0.EntNumericQueryParameter OffenderIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter OffenderRefIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OfficerIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntNumericQueryParameter OfficerIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OfficerIDNumericOperator, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmNumericOperator OfficerIDNumericOperator { get; set; }

        [PropertyTest(EnumPropertyName.OfficerIDValue, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OfficerIDValue { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter ReceiverQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurityNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter SocialSecurityNumberQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter TransmitterQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.IncludeProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramPhoneNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringQueryParameter ProgramPhoneNumberQueryParameter { get; set; }


        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        public CreateEntMsgGetOffendersListRequest()
        {
            //OffenderID = -1;
            //Offenders0.EntNumericQueryParameter OffenderIDQueryParameter = null;
        }

        protected override void ExecuteBlock()
        {
            GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            Offenders0.EntNumericQueryParameter OffenderIDQueryParameter = new Offenders0.EntNumericQueryParameter();
            /// handle the  OffenderIDQueryParameter ///
            if (OffenderID != null && OffenderID != 0) //OffenderIDQueryParameter.Value != OffenderID
            {
                OffenderIDQueryParameter = new Offenders0.EntNumericQueryParameter();
                OffenderIDQueryParameter.Operator = Offenders0.EnmNumericOperator.Equal;
                OffenderIDQueryParameter.Value = OffenderID;
                GetOffendersListRequest.OffenderID = OffenderIDQueryParameter;
            }                
            else /*if (OffenderID != -1)*/
            {
                OffenderIDQueryParameter = null;
                GetOffendersListRequest.OffenderID = OffenderIDQueryParameter;
            }
            //////////////////////////////////////////

            /// handle the  AgencyIDQueryParameter ///
            if (AgencyIDQueryParameter != null)
                GetOffendersListRequest.AgencyID = AgencyIDQueryParameter;
            else if(AgencyIDValue != null)
            {
                AgencyIDQueryParameter = new Offenders0.EntNumericQueryParameter();
                AgencyIDQueryParameter.Value = (long)AgencyIDValue;
                AgencyIDQueryParameter.Operator = AgencyIDNumericOperator.Equals(null) ? AgencyIDNumericOperator = Offenders0.EnmNumericOperator.Equal : AgencyIDNumericOperator;
                GetOffendersListRequest.AgencyID = AgencyIDQueryParameter;
            }
            //////////////////////////////////////////

            /// handle the  OffenderIDQueryParameter ///
            if (OfficerIDQueryParameter != null)
                GetOffendersListRequest.OfficerID = OfficerIDQueryParameter;
            else if(OfficerIDValue != null)
            {
                OfficerIDQueryParameter = new Offenders0.EntNumericQueryParameter();
                OfficerIDQueryParameter.Value = (long)OfficerIDValue;
                OfficerIDQueryParameter.Operator = OfficerIDNumericOperator.Equals(null) ? OfficerIDNumericOperator = Offenders0.EnmNumericOperator.Equal : OfficerIDNumericOperator;
                GetOffendersListRequest.OfficerID = OfficerIDQueryParameter;
            }
            //////////////////////////////////////////

            GetOffendersListRequest.ProgramStatus = ProgramStatus;
            GetOffendersListRequest.ProgramType = ProgramType;
            GetOffendersListRequest.SortField = SortField;
            GetOffendersListRequest.SortDirection = SortDirection;
            GetOffendersListRequest.Limit = Limit;
            GetOffendersListRequest.EndOfServiceCode = EndOfServiceCodeQueryParameter;
            GetOffendersListRequest.FirstName = FirstNameQueryParameter;
            GetOffendersListRequest.GroupID = GroupID;
            GetOffendersListRequest.IncludeAddressList = IncludeAddressList;
            GetOffendersListRequest.IncludeProgramStatus = IncludeProgramStatus;
            GetOffendersListRequest.LastName = LastNameQueryParameter;
            GetOffendersListRequest.LastTimeStamp = LastTimeStamp;
            GetOffendersListRequest.OffenderRefID = OffenderRefIDQueryParameter;
            GetOffendersListRequest.ProgramConcept = ProgramConcept;
            GetOffendersListRequest.ProgramPhoneNumber = ProgramPhoneNumberQueryParameter;
            GetOffendersListRequest.Receiver = ReceiverQueryParameter;
            GetOffendersListRequest.SocialSecurityNumber = SocialSecurityNumberQueryParameter;
            GetOffendersListRequest.Transmitter = TransmitterQueryParameter;

        }
    }


    public class CreateEntMsgGetOffendersListRequest_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EnmProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EnmProgramStatus[] ProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EnmProgramConcept? ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EnmProgramType[] ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.OffendersSortOptionsSortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EnmOffendersSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.ListSortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.AgencyIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntNumericQueryParameter AgencyIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.EndOfServiceCodeQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter EndOfServiceCodeQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? GroupID { get; set; }

        [PropertyTest(EnumPropertyName.FirstNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter FirstNameQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.IncludeAddressList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeAddressList { get; set; }

        [PropertyTest(EnumPropertyName.LastNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter LastNameQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.OffenderIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntNumericQueryParameter OffenderIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter OffenderRefIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OfficerIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntNumericQueryParameter OfficerIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter ReceiverQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurityNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter SocialSecurityNumberQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter TransmitterQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.IncludeProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramPhoneNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntStringQueryParameter ProgramPhoneNumberQueryParameter { get; set; }


        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        public CreateEntMsgGetOffendersListRequest_1()
        {
            OffenderID = -1;
        }

        protected override void ExecuteBlock()
        {
            GetOffendersListRequest = new Offenders1.EntMsgGetOffendersListRequest();
            GetOffendersListRequest.ProgramStatus = ProgramStatus;
            GetOffendersListRequest.ProgramType = ProgramType;
            GetOffendersListRequest.SortField = SortField;
            GetOffendersListRequest.SortDirection = SortDirection;
            GetOffendersListRequest.Limit = Limit;
            GetOffendersListRequest.AgencyID = AgencyIDQueryParameter;
            GetOffendersListRequest.EndOfServiceCode = EndOfServiceCodeQueryParameter;
            GetOffendersListRequest.FirstName = FirstNameQueryParameter;
            GetOffendersListRequest.GroupID = GroupID;
            GetOffendersListRequest.IncludeAddressList = IncludeAddressList;
            GetOffendersListRequest.IncludeProgramStatus = IncludeProgramStatus;
            GetOffendersListRequest.LastName = LastNameQueryParameter;
            GetOffendersListRequest.LastTimeStamp = LastTimeStamp;
            if (OffenderIDQueryParameter != null)
                GetOffendersListRequest.OffenderID = OffenderIDQueryParameter;
            else if (OffenderID != -1)
                GetOffendersListRequest.OffenderID = new Offenders1.EntNumericQueryParameter() { Operator = Offenders1.EnmSqlOperatorNum.Equal, Value = OffenderID };

            GetOffendersListRequest.OffenderRefID = OffenderRefIDQueryParameter;
            GetOffendersListRequest.OfficerID = OfficerIDQueryParameter;
            GetOffendersListRequest.ProgramConcept = ProgramConcept;
            GetOffendersListRequest.ProgramPhoneNumber = ProgramPhoneNumberQueryParameter;
            GetOffendersListRequest.Receiver = ReceiverQueryParameter;
            GetOffendersListRequest.SocialSecurityNumber = SocialSecurityNumberQueryParameter;
            GetOffendersListRequest.Transmitter = TransmitterQueryParameter;

        }
    }


    public class CreateEntMsgGetOffendersListRequest_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.EnmProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EnmProgramStatus[] ProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EnmProgramConcept? ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EnmProgramType[] ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.OffendersSortOptionsSortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EnmOffendersSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.ListSortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.ListRequestLimit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.AgencyIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntNumericQueryParameter AgencyIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.EndOfServiceCodeQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter EndOfServiceCodeQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? GroupID { get; set; }

        [PropertyTest(EnumPropertyName.FirstNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter FirstNameQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.IncludeAddressList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeAddressList { get; set; }

        [PropertyTest(EnumPropertyName.LastNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter LastNameQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.OffenderIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntNumericQueryParameter OffenderIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter OffenderRefIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.OfficerIDQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntNumericQueryParameter OfficerIDQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter ReceiverQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurityNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter SocialSecurityNumberQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter TransmitterQueryParameter { get; set; }

        [PropertyTest(EnumPropertyName.IncludeProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramPhoneNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntStringQueryParameter ProgramPhoneNumberQueryParameter { get; set; }


        [PropertyTest(EnumPropertyName.GetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffendersListRequest GetOffendersListRequest { get; set; }

        public CreateEntMsgGetOffendersListRequest_2()
        {
            OffenderID = -1;
        }

        protected override void ExecuteBlock()
        {
            GetOffendersListRequest = new Offenders2.EntMsgGetOffendersListRequest();
            GetOffendersListRequest.ProgramStatus = ProgramStatus;
            GetOffendersListRequest.ProgramType = ProgramType;
            GetOffendersListRequest.SortField = SortField;
            GetOffendersListRequest.SortDirection = SortDirection;
            //GetOffendersListRequest.Limit = Limit;
            GetOffendersListRequest.AgencyID = AgencyIDQueryParameter;
            GetOffendersListRequest.EndOfServiceCode = EndOfServiceCodeQueryParameter;
            GetOffendersListRequest.FirstName = FirstNameQueryParameter;
            GetOffendersListRequest.GroupID = GroupID;
            GetOffendersListRequest.IncludeAddressList = IncludeAddressList;
            GetOffendersListRequest.IncludeProgramStatus = IncludeProgramStatus;
            GetOffendersListRequest.LastName = LastNameQueryParameter;
            GetOffendersListRequest.LastTimeStamp = (long?) LastTimeStamp;
            if (OffenderIDQueryParameter != null)
                GetOffendersListRequest.OffenderID = OffenderIDQueryParameter;
            else if (OffenderID != -1)
                GetOffendersListRequest.OffenderID = new Offenders2.EntNumericQueryParameter() { Operator = Offenders2.EnmSqlOperatorNum.Equal, Value = OffenderID };

            GetOffendersListRequest.OffenderRefID = OffenderRefIDQueryParameter;
            GetOffendersListRequest.OfficerID = OfficerIDQueryParameter;
            GetOffendersListRequest.ProgramConcept = ProgramConcept;
            GetOffendersListRequest.ProgramPhoneNumber = ProgramPhoneNumberQueryParameter;
            GetOffendersListRequest.Receiver = ReceiverQueryParameter;
            GetOffendersListRequest.SocialSecurityNumber = SocialSecurityNumberQueryParameter;
            GetOffendersListRequest.Transmitter = TransmitterQueryParameter;
        }
    }
}
