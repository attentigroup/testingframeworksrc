﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgDeleteOffenderContactRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ContactID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgDeleteContactRequest DeleteOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteOffenderContactRequest = new Offenders0.EntMsgDeleteContactRequest();
            DeleteOffenderContactRequest.ContactID = ContactID;
        }
    }


    public class CreateEntMsgDeleteOffenderContactRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ContactID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgDeleteOffenderContactRequest DeleteOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteOffenderContactRequest = new Offenders1.EntMsgDeleteOffenderContactRequest();
            DeleteOffenderContactRequest.ContactID = ContactID;
        }
    }


    public class CreateEntMsgDeleteOffenderContactRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ContactID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgDeleteOffenderContactRequest DeleteOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteOffenderContactRequest = new Offenders2.EntMsgDeleteOffenderContactRequest();
            DeleteOffenderContactRequest.ContactID = ContactID;
        }
    }
}
