﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgGetOffenderCurrentStatusRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderCurrentStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetOffenderCurrentStatusRequest GetOffenderCurrentStatusRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderCurrentStatusRequest = new Offenders0.EntMsgGetOffenderCurrentStatusRequest();
            GetOffenderCurrentStatusRequest.OffenderID = OffenderID;
        }
    }
}
