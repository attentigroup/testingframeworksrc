﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgGetOffenderContactsListRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ContactID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetContactsListRequest GetOffenderContactsListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderContactsListRequest = new Offenders0.EntMsgGetContactsListRequest();
            GetOffenderContactsListRequest.ContactID = ContactID;
            GetOffenderContactsListRequest.OffenderID = OffenderID;
        }
    }


    public class CreateEntMsgGetOffenderContactsListRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ContactID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetOffenderContactsListRequest GetOffenderContactsListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderContactsListRequest = new Offenders1.EntMsgGetOffenderContactsListRequest();
            GetOffenderContactsListRequest.ContactID = ContactID;
            GetOffenderContactsListRequest.OffenderID = OffenderID;
        }
    }


    public class CreateEntMsgGetOffenderContactsListRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ContactID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetOffenderContactsListRequest GetOffenderContactsListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderContactsListRequest = new Offenders2.EntMsgGetOffenderContactsListRequest();
            GetOffenderContactsListRequest.ContactID = ContactID;
            GetOffenderContactsListRequest.OffenderID = OffenderID;
        }
    }
}
