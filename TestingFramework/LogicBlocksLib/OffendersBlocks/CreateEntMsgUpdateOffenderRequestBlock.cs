﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.Collections.Generic;
using DataGenerators.Agency;
using TestingFramework.TestsInfraStructure.Factories;
using DataGenerators;
using System.Linq;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using DataGenerators.Offender;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgUpdateOffenderRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.AgencyId, EnumPropertyModifier.Mandatory)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.AllowVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AllowVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.ContactPhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string ContactPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.ContactPrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CourtOrderNumber, EnumPropertyType.CourtOrderNumber, EnumPropertyModifier.Mandatory)]
        public string CourtOrderNumber { get; set; }

        [PropertyTest(EnumPropertyName.DateOfBirth, EnumPropertyType.DateOfBirth, EnumPropertyModifier.Mandatory)]
        public DateTime DateOfBirth { get; set; }

        [PropertyTest(EnumPropertyName.DepartmentOfCorrection, EnumPropertyType.DepartmentOfCorrection, EnumPropertyModifier.Mandatory)]
        public string DepartmentOfCorrection { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.UpdatedFirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Gender, EnumPropertyType.Gender, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmGender Gender { get; set; }

        [PropertyTest(EnumPropertyName.HomeUnit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HomeUnit { get; set; }

        [PropertyTest(EnumPropertyName.LandlineDialOutPrefix, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string LandlineDialOutPrefix { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string LandlinePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string LandlinePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.Language, EnumPropertyModifier.Mandatory)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.PhotoID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? PhotoID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramEnd, EnumPropertyType.ProgramEnd, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramEnd { get; set; }

        [PropertyTest(EnumPropertyName.ProgramStart, EnumPropertyType.ProgramStart, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramStart { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.SocialSecurity, EnumPropertyModifier.Mandatory)]
        public string SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ulong Timestamp { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.Title, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmTitle Title { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.VisibleCustomFieldsDefinitions, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntCustomFieldDefinition[] VisibleCustomFieldsDefinitions { get; set; }


        [PropertyTest(EnumPropertyName.UpdateOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgUpdateOffenderRequest UpdateOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderRequest = new Offenders0.EntMsgUpdateOffenderRequest();

            UpdateOffenderRequest.AgencyID = AgencyID;
            UpdateOffenderRequest.AllowVoiceTests = AllowVoiceTests;
            if (CityID == null)
                UpdateOffenderRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                UpdateOffenderRequest.CityID = CityID;
            UpdateOffenderRequest.ContactPhoneNumber = ContactPhoneNumber;
            UpdateOffenderRequest.ContactPrefixPhone = ContactPrefixPhone;
            UpdateOffenderRequest.CourtOrderNumber = CourtOrderNumber;
            UpdateOffenderRequest.DateOfBirth = DateOfBirth;
            UpdateOffenderRequest.DepartmentOfCorrection = DepartmentOfCorrection;
            UpdateOffenderRequest.FirstName = FirstName;
            UpdateOffenderRequest.Gender = Gender;
            UpdateOffenderRequest.HomeUnit = HomeUnit;
            UpdateOffenderRequest.LandlineDialOutPrefix = LandlineDialOutPrefix;
            UpdateOffenderRequest.LandlinePhoneNumber = LandlinePhoneNumber;
            UpdateOffenderRequest.LandlinePrefixPhone = LandlinePrefixPhone;
            UpdateOffenderRequest.Language = Language;
            UpdateOffenderRequest.LastName = LastName;
            UpdateOffenderRequest.MiddleName = MiddleName;
            UpdateOffenderRequest.OffenderID = OffenderID;
            if( OfficerID > 0 )
            {
                UpdateOffenderRequest.OfficerID = OfficerID;
            }
            else
            {
                UpdateOffenderRequest.OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID);
            }            
            UpdateOffenderRequest.PhotoID = PhotoID;
            UpdateOffenderRequest.ProgramEnd = ProgramEnd;
            UpdateOffenderRequest.ProgramStart = ProgramStart;
            UpdateOffenderRequest.ProgramType = ProgramType;
            UpdateOffenderRequest.Receiver = Receiver;
            if(RelatedOffenderID != -1)
                UpdateOffenderRequest.RelatedOffenderID = new Nullable<int>(RelatedOffenderID);
            UpdateOffenderRequest.SocialSecurity = SocialSecurity;
            UpdateOffenderRequest.StateID = StateID;
            UpdateOffenderRequest.Street = Street;
            UpdateOffenderRequest.Timestamp = Timestamp;
            UpdateOffenderRequest.Title = Title;
            UpdateOffenderRequest.Transmitter = Transmitter;
            UpdateOffenderRequest.ZipCode = ZipCode;

            if (VisibleCustomFieldsDefinitions != null && VisibleCustomFieldsDefinitions.Length > 0)
            {
                UpdateOffenderRequest.CustomFields = new Dictionary<int, object>();
                foreach (var field in VisibleCustomFieldsDefinitions)
                {
                    var value = GenerateRandomValueForCustomField(field);
                    UpdateOffenderRequest.CustomFields.Add(field.ID, value);

                }
            }
            else
                UpdateOffenderRequest.CustomFields = null;
        }
        
        private object GenerateRandomValueForCustomField(Offenders0.EntCustomFieldDefinition cfd)
        {
            object result = null;
            var dgf = DataGeneratorFactory.Instance;
            switch (cfd.Type)
            {
                case Offenders0.EnmFieldType.Numeric:
                    result = dgf.GenerateData(EnumPropertyType.Int, null);
                    break;
                case Offenders0.EnmFieldType.String:
                    result = dgf.GenerateData(EnumPropertyType.String, null);
                    break;
                case Offenders0.EnmFieldType.Date:
                    result = DateTime.Now;
                    break;
                case Offenders0.EnmFieldType.Predefiend:
                    result = GenerateRandomValueForListCustomField((Offenders0.EntCustomFieldListDefinition)cfd);
                    break;
                default:
                    Log.Error($"EnmFieldType {cfd.Type} not supported");
                    break;
            }
            return result;
        }

        private object GenerateRandomValueForListCustomField(Offenders0.EntCustomFieldListDefinition cfd)
        {
            object  result = null;
            var     dgf = DataGeneratorFactory.Instance;
            var     intGenerator = dgf.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var     listValues = cfd.ListValues.Keys.ToList();
            int     selectedValue = intGenerator.GenerateData(0, listValues.Count - 1);

            result = listValues[selectedValue];
            
            return result;
        }
    }


    public class CreateEntMsgUpdateOffenderRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.AllowVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AllowVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.ContactPhoneNumber, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.ContactPrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CourtOrderNumber, EnumPropertyType.CourtOrderNumber, EnumPropertyModifier.Mandatory)]
        public string CourtOrderNumber { get; set; }

        [PropertyTest(EnumPropertyName.DateOfBirth, EnumPropertyType.DateOfBirth, EnumPropertyModifier.Mandatory)]
        public DateTime DateOfBirth { get; set; }

        [PropertyTest(EnumPropertyName.DepartmentOfCorrection, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string DepartmentOfCorrection { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.UpdatedFirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Gender, EnumPropertyType.Gender_1, EnumPropertyModifier.Mandatory)]
        public Offenders1.EnmGender Gender { get; set; }

        [PropertyTest(EnumPropertyName.HomeUnit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HomeUnit { get; set; }

        [PropertyTest(EnumPropertyName.LandlineDialOutPrefix, EnumPropertyType.PrefixPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string LandlineDialOutPrefix { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePhoneNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string LandlinePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string LandlinePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.Language, EnumPropertyModifier.Mandatory)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.PhotoID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? PhotoID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramEnd, EnumPropertyType.ProgramEnd, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramEnd { get; set; }

        [PropertyTest(EnumPropertyName.ProgramStart, EnumPropertyType.ProgramStart, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramStart { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ulong Timestamp { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.Title_1, EnumPropertyModifier.Mandatory)]
        public Offenders1.EnmTitle Title { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }


        [PropertyTest(EnumPropertyName.UpdateOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgUpdateOffenderRequest UpdateOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderRequest = new Offenders1.EntMsgUpdateOffenderRequest();

            UpdateOffenderRequest.AgencyID = AgencyID;
            UpdateOffenderRequest.AllowVoiceTests = AllowVoiceTests;
            if (CityID == null)
                UpdateOffenderRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                UpdateOffenderRequest.CityID = CityID;
            UpdateOffenderRequest.ContactPhoneNumber = ContactPhoneNumber;
            UpdateOffenderRequest.ContactPrefixPhone = ContactPrefixPhone;
            UpdateOffenderRequest.CourtOrderNumber = CourtOrderNumber;
            UpdateOffenderRequest.DateOfBirth = DateOfBirth;
            UpdateOffenderRequest.DepartmentOfCorrection = DepartmentOfCorrection;
            UpdateOffenderRequest.FirstName = FirstName;
            UpdateOffenderRequest.Gender = Gender;
            UpdateOffenderRequest.HomeUnit = HomeUnit;
            UpdateOffenderRequest.LandlineDialOutPrefix = LandlineDialOutPrefix;
            UpdateOffenderRequest.LandlinePhoneNumber = LandlinePhoneNumber;
            UpdateOffenderRequest.LandlinePrefixPhone = LandlinePrefixPhone;
            UpdateOffenderRequest.Language = Language;
            UpdateOffenderRequest.LastName = LastName;
            UpdateOffenderRequest.MiddleName = MiddleName;
            UpdateOffenderRequest.OffenderID = OffenderID;
            if( OfficerID > 0 )
            {
                UpdateOffenderRequest.OfficerID = OfficerID;
            }
            else
            {
                UpdateOffenderRequest.OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID);
            }
            
            UpdateOffenderRequest.PhotoID = PhotoID;
            UpdateOffenderRequest.ProgramEnd = ProgramEnd;
            UpdateOffenderRequest.ProgramStart = ProgramStart;
            UpdateOffenderRequest.ProgramType = ProgramType;
            UpdateOffenderRequest.Receiver = Receiver;
            if (RelatedOffenderID != -1)
                UpdateOffenderRequest.RelatedOffenderID = new Nullable<int>(RelatedOffenderID);
            UpdateOffenderRequest.SocialSecurity = SocialSecurity;
            UpdateOffenderRequest.StateID = StateID;
            UpdateOffenderRequest.Street = Street;
            UpdateOffenderRequest.Timestamp = Timestamp;
            UpdateOffenderRequest.Title = Title;
            UpdateOffenderRequest.Transmitter = Transmitter;
            UpdateOffenderRequest.ZipCode = ZipCode;

        }
    }


    public class CreateEntMsgUpdateOffenderRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.AllowVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AllowVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.ContactPhoneNumber, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.ContactPrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string ContactPrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.CourtOrderNumber, EnumPropertyType.CourtOrderNumber, EnumPropertyModifier.Mandatory)]
        public string CourtOrderNumber { get; set; }

        [PropertyTest(EnumPropertyName.DateOfBirth, EnumPropertyType.DateOfBirth, EnumPropertyModifier.Mandatory)]
        public DateTime DateOfBirth { get; set; }

        [PropertyTest(EnumPropertyName.DepartmentOfCorrection, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string DepartmentOfCorrection { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.UpdatedFirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Gender, EnumPropertyType.Gender_1, EnumPropertyModifier.Mandatory)]
        public Offenders2.EnmGender Gender { get; set; }

        [PropertyTest(EnumPropertyName.HomeUnit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string HomeUnit { get; set; }

        [PropertyTest(EnumPropertyName.LandlineDialOutPrefix, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string LandlineDialOutPrefix { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePhoneNumber, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string LandlinePhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string LandlinePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.Language, EnumPropertyModifier.Mandatory)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.PhotoID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? PhotoID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramEnd, EnumPropertyType.ProgramEnd, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramEnd { get; set; }

        [PropertyTest(EnumPropertyName.ProgramStart, EnumPropertyType.ProgramStart, EnumPropertyModifier.Mandatory)]
        public DateTime ProgramStart { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Receiver { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public long Timestamp { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.Title_1, EnumPropertyModifier.Mandatory)]
        public Offenders2.EnmTitle Title { get; set; }

        [PropertyTest(EnumPropertyName.Transmitter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Transmitter { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }


        [PropertyTest(EnumPropertyName.UpdateOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgUpdateOffenderRequest UpdateOffenderRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderRequest = new Offenders2.EntMsgUpdateOffenderRequest();

            UpdateOffenderRequest.AgencyID = AgencyID;
            UpdateOffenderRequest.AllowVoiceTests = AllowVoiceTests;
            if (CityID == null)
                UpdateOffenderRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                UpdateOffenderRequest.CityID = CityID;
            UpdateOffenderRequest.ContactPhoneNumber = ContactPhoneNumber;
            UpdateOffenderRequest.ContactPrefixPhone = ContactPrefixPhone;
            UpdateOffenderRequest.CourtOrderNumber = CourtOrderNumber;
            UpdateOffenderRequest.DateOfBirth = DateOfBirth;
            UpdateOffenderRequest.DepartmentOfCorrection = DepartmentOfCorrection;
            UpdateOffenderRequest.FirstName = FirstName;
            UpdateOffenderRequest.Gender = Gender;
            UpdateOffenderRequest.HomeUnit = HomeUnit;
            UpdateOffenderRequest.LandlineDialOutPrefix = LandlineDialOutPrefix;
            UpdateOffenderRequest.LandlinePhoneNumber = LandlinePhoneNumber;
            UpdateOffenderRequest.LandlinePrefixPhone = LandlinePrefixPhone;
            UpdateOffenderRequest.Language = Language;
            UpdateOffenderRequest.LastName = LastName;
            UpdateOffenderRequest.MiddleName = MiddleName;
            UpdateOffenderRequest.OffenderID = OffenderID;
            if( OfficerID > 0 )
            {
                UpdateOffenderRequest.OfficerID = OfficerID;
            }
            else
            {
                UpdateOffenderRequest.OfficerID = AgencyIdGenerator.GetOfficerByAgencyId(AgencyID);
            }
            
            UpdateOffenderRequest.PhotoID = PhotoID;
            UpdateOffenderRequest.ProgramEnd = ProgramEnd;
            UpdateOffenderRequest.ProgramStart = ProgramStart;
            UpdateOffenderRequest.ProgramType = ProgramType;
            UpdateOffenderRequest.Receiver = Receiver;
            if (RelatedOffenderID != -1)
                UpdateOffenderRequest.RelatedOffenderID = new Nullable<int>(RelatedOffenderID);
            UpdateOffenderRequest.SocialSecurity = SocialSecurity;
            UpdateOffenderRequest.StateID = StateID;
            UpdateOffenderRequest.Street = Street;
            UpdateOffenderRequest.Timestamp = Timestamp;
            UpdateOffenderRequest.Title = Title;
            UpdateOffenderRequest.Transmitter = Transmitter;
            UpdateOffenderRequest.ZipCode = ZipCode;

        }
    }
}
