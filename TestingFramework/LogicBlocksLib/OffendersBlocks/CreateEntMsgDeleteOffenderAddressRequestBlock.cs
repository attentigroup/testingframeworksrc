﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgDeleteOffenderAddressRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AddressID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AddressID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgDeleteOffenderAddressRequest DeleteOffenderAddressRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteOffenderAddressRequest = new Offenders0.EntMsgDeleteOffenderAddressRequest();
            DeleteOffenderAddressRequest.AddressID = AddressID;
            DeleteOffenderAddressRequest.OffenderID = OffenderID;
        }
    }


    public class CreateEntMsgDeleteOffenderAddressRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AddressID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AddressID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgDeleteOffenderAddressRequest DeleteOffenderAddressRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteOffenderAddressRequest = new Offenders1.EntMsgDeleteOffenderAddressRequest();
            DeleteOffenderAddressRequest.AddressID = AddressID;
            DeleteOffenderAddressRequest.OffenderID = OffenderID;
        }
    }


    public class CreateEntMsgDeleteOffenderAddressRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AddressID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AddressID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgDeleteOffenderAddressRequest DeleteOffenderAddressRequest { get; set; }


        protected override void ExecuteBlock()
        {
            DeleteOffenderAddressRequest = new Offenders2.EntMsgDeleteOffenderAddressRequest();
            DeleteOffenderAddressRequest.AddressID = AddressID;
            DeleteOffenderAddressRequest.OffenderID = OffenderID;
        }
    }
}
