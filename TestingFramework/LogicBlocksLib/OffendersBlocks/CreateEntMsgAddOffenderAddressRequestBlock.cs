﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using DataGenerators.Offender;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgAddOffenderAddressRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.AddressDescription, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.Phones, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntPhone[] Phones { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.StateId, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.AddressZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ZoneID { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfPhones, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int amountOfPhonesToGenerate { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }

        public CreateEntMsgAddOffenderAddressRequestBlock()
        {
            amountOfPhonesToGenerate = 1;
        }


        protected override void ExecuteBlock()
        {
            AddOffenderAddressRequest = new Offenders0.EntMsgAddOffenderAddressRequest();
            if (CityID == null)
                AddOffenderAddressRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                AddOffenderAddressRequest.CityID = CityID;
            AddOffenderAddressRequest.Description = Description;
            AddOffenderAddressRequest.OffenderID = OffenderID;
            if (Phones == null)
                AddOffenderAddressRequest.Phones = OffenderAddressPhoneNumberGenerator.GetPhones(amountOfPhonesToGenerate);
            else
                AddOffenderAddressRequest.Phones = Phones;
            AddOffenderAddressRequest.StateID = StateID;
            AddOffenderAddressRequest.Street = Street;
            AddOffenderAddressRequest.ZipCode = ZipCode;
            AddOffenderAddressRequest.ZoneID = ZoneID;
        }
    }


    public class CreateEntMsgAddOffenderAddressRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.AddressDescription, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.Phones, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders1.EntPhone[] Phones { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.StateId, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.AddressZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ZoneID { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }



        protected override void ExecuteBlock()
        {
            AddOffenderAddressRequest = new Offenders1.EntMsgAddOffenderAddressRequest();
            if (CityID == null)
                AddOffenderAddressRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                AddOffenderAddressRequest.CityID = CityID;
            AddOffenderAddressRequest.Description = Description;
            AddOffenderAddressRequest.OffenderID = OffenderID;
            AddOffenderAddressRequest.Phones = Phones;
            AddOffenderAddressRequest.StateID = StateID;
            AddOffenderAddressRequest.Street = Street;
            AddOffenderAddressRequest.ZipCode = ZipCode;
            AddOffenderAddressRequest.ZoneID = ZoneID;
        }
    }


    public class CreateEntMsgAddOffenderAddressRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.AddressDescription, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.Phones, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders2.EntPhone[] Phones { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.AddressStreet, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.AddressZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ZoneID { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }



        protected override void ExecuteBlock()
        {
            AddOffenderAddressRequest = new Offenders2.EntMsgAddOffenderAddressRequest();
            if (CityID == null)
                AddOffenderAddressRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                AddOffenderAddressRequest.CityID = CityID;
            AddOffenderAddressRequest.Description = Description;
            AddOffenderAddressRequest.OffenderID = OffenderID;
            AddOffenderAddressRequest.Phones = Phones;
            AddOffenderAddressRequest.StateID = StateID;
            AddOffenderAddressRequest.Street = Street;
            AddOffenderAddressRequest.ZipCode = ZipCode;
            AddOffenderAddressRequest.ZoneID = ZoneID;
        }
    }
}
