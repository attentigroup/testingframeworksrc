﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.Common;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgAddOffenderPictureRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Photo, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string PhotoPath { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddPictureRequest AddOffenderPictureRequest { get; set; }


        protected override void ExecuteBlock()
        {
            AddOffenderPictureRequest = new Offenders0.EntMsgAddPictureRequest();
            AddOffenderPictureRequest.OffenderID = OffenderID;
            AddOffenderPictureRequest.Photo = ImageConverter.GetBytesFromImage(PhotoPath);
        }
    }


    public class CreateEntMsgAddOffenderPictureRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Photo, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string PhotoPath { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgAddOffenderPictureRequest AddOffenderPictureRequest { get; set; }


        protected override void ExecuteBlock()
        {
            AddOffenderPictureRequest = new Offenders1.EntMsgAddOffenderPictureRequest();
            AddOffenderPictureRequest.OffenderID = OffenderID;
            AddOffenderPictureRequest.Photo = ImageConverter.GetBytesFromImage(PhotoPath);
        }
    }


    public class CreateEntMsgAddOffenderPictureRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Photo, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string PhotoPath { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgAddOffenderPictureRequest AddOffenderPictureRequest { get; set; }


        protected override void ExecuteBlock()
        {
            AddOffenderPictureRequest = new Offenders2.EntMsgAddOffenderPictureRequest();
            AddOffenderPictureRequest.OffenderID = OffenderID;
            AddOffenderPictureRequest.Photo = ImageConverter.GetBytesFromImage(PhotoPath);
        }
    }
}
