﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgUpdateOffenderContactRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ContactID { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Priority, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Priority { get; set; }

        [PropertyTest(EnumPropertyName.Relation, EnumPropertyType.ContactRelation, EnumPropertyModifier.Mandatory)]
        public string Relation { get; set; }

        [PropertyTest(EnumPropertyName.UpdateOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgUpdateContactRequest UpdateOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderContactRequest = new Offenders0.EntMsgUpdateContactRequest();
            UpdateOffenderContactRequest.FirstName = FirstName;
            UpdateOffenderContactRequest.LastName = LastName;
            UpdateOffenderContactRequest.MiddleName = MiddleName;
            UpdateOffenderContactRequest.ContactID = ContactID;
            UpdateOffenderContactRequest.Phone = Phone;
            UpdateOffenderContactRequest.PrefixPhone = PrefixPhone;
            UpdateOffenderContactRequest.Priority = Priority;
            UpdateOffenderContactRequest.Relation = Relation;
        }
    }


    public class CreateEntMsgUpdateOffenderContactRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ContactID { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Priority, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Priority { get; set; }

        [PropertyTest(EnumPropertyName.Relation, EnumPropertyType.ContactRelation, EnumPropertyModifier.Mandatory)]
        public string Relation { get; set; }

        [PropertyTest(EnumPropertyName.UpdateOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgUpdateOffenderContactRequest UpdateOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderContactRequest = new Offenders1.EntMsgUpdateOffenderContactRequest();
            UpdateOffenderContactRequest.FirstName = FirstName;
            UpdateOffenderContactRequest.LastName = LastName;
            UpdateOffenderContactRequest.MiddleName = MiddleName;
            UpdateOffenderContactRequest.ContactID = ContactID;
            UpdateOffenderContactRequest.Phone = Phone;
            UpdateOffenderContactRequest.PrefixPhone = PrefixPhone;
            UpdateOffenderContactRequest.Priority = Priority;
            UpdateOffenderContactRequest.Relation = Relation;
        }
    }


    public class CreateEntMsgUpdateOffenderContactRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.ContactID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ContactID { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.MiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Priority, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Priority { get; set; }

        [PropertyTest(EnumPropertyName.Relation, EnumPropertyType.ContactRelation, EnumPropertyModifier.Mandatory)]
        public string Relation { get; set; }

        [PropertyTest(EnumPropertyName.UpdateOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgUpdateOffenderContactRequest UpdateOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderContactRequest = new Offenders2.EntMsgUpdateOffenderContactRequest();
            UpdateOffenderContactRequest.FirstName = FirstName;
            UpdateOffenderContactRequest.LastName = LastName;
            UpdateOffenderContactRequest.MiddleName = MiddleName;
            UpdateOffenderContactRequest.ContactID = ContactID;
            UpdateOffenderContactRequest.Phone = Phone;
            UpdateOffenderContactRequest.PrefixPhone = PrefixPhone;
            UpdateOffenderContactRequest.Priority = Priority;
            UpdateOffenderContactRequest.Relation = Relation;
        }
    }
}
