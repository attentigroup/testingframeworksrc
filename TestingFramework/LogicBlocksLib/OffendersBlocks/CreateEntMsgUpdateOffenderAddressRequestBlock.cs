﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using DataGenerators.Offender;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgUpdateOffenderAddressRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AddressID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int AddressID { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.AddressDescription, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Phones, EnumPropertyType.AddressPhoneNumber, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntPhone[] Phones { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.StateId, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdateOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgUpdateOffenderAddressRequest UpdateOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderAddressRequest = new Offenders0.EntMsgUpdateOffenderAddressRequest();
            UpdateOffenderAddressRequest.AddressID = AddressID;
            if (CityID == null)
                UpdateOffenderAddressRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                UpdateOffenderAddressRequest.CityID = CityID;
            UpdateOffenderAddressRequest.Description = Description;
            UpdateOffenderAddressRequest.OffenderID = OffenderID;
            
            UpdateOffenderAddressRequest.StateID = StateID;
            UpdateOffenderAddressRequest.Street = Street;
            UpdateOffenderAddressRequest.ZipCode = ZipCode;
            UpdateOffenderAddressRequest.ZoneID = ZoneID;
            UpdateOffenderAddressRequest.Phones = Phones;
            //if (PhoneNumber != null)
            //{
            //    var EntPhone = new Offenders0.EntPhone();
            //    EntPhone.PhoneNumber = PhoneNumber;
            //    Offenders0.EntPhone[] ListEntPhones = { EntPhone };
            //    UpdateOffenderAddressRequest.Phones = ListEntPhones;
            //}
        }
    }


    public class CreateEntMsgUpdateOffenderAddressRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AddressID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int AddressID { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Phones, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdateOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgUpdateOffenderAddressRequest UpdateOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderAddressRequest = new Offenders1.EntMsgUpdateOffenderAddressRequest();
            UpdateOffenderAddressRequest.AddressID = AddressID;
            if (CityID == null)
                UpdateOffenderAddressRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                UpdateOffenderAddressRequest.CityID = CityID;
            UpdateOffenderAddressRequest.Description = Description;
            UpdateOffenderAddressRequest.OffenderID = OffenderID;

            UpdateOffenderAddressRequest.StateID = StateID;
            UpdateOffenderAddressRequest.Street = Street;
            UpdateOffenderAddressRequest.ZipCode = ZipCode;
            UpdateOffenderAddressRequest.ZoneID = ZoneID;

            if (PhoneNumber != null)
            {
                var EntPhone = new Offenders1.EntPhone();
                EntPhone.PhoneNumber = PhoneNumber;
                Offenders1.EntPhone[] ListEntPhones = { EntPhone };
                UpdateOffenderAddressRequest.Phones = ListEntPhones;
            }
        }
    }


    public class CreateEntMsgUpdateOffenderAddressRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AddressID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int AddressID { get; set; }

        [PropertyTest(EnumPropertyName.CityID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CityID { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Phones, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string PhoneNumber { get; set; }

        [PropertyTest(EnumPropertyName.StateID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string StateID { get; set; }

        [PropertyTest(EnumPropertyName.Street, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Street { get; set; }

        [PropertyTest(EnumPropertyName.ZipCode, EnumPropertyType.ZipCode, EnumPropertyModifier.Mandatory)]
        public string ZipCode { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? ZoneID { get; set; }


        [PropertyTest(EnumPropertyName.UpdateOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgUpdateOffenderAddressRequest UpdateOffenderAddressRequest { get; set; }

        protected override void ExecuteBlock()
        {
            UpdateOffenderAddressRequest = new Offenders2.EntMsgUpdateOffenderAddressRequest();
            UpdateOffenderAddressRequest.AddressID = AddressID;
            if (CityID == null)
                UpdateOffenderAddressRequest.CityID = OffenderStateAndCityGenerator.GetCityByStateId(StateID);
            else
                UpdateOffenderAddressRequest.CityID = CityID;
            UpdateOffenderAddressRequest.Description = Description;
            UpdateOffenderAddressRequest.OffenderID = OffenderID;

            UpdateOffenderAddressRequest.StateID = StateID;
            UpdateOffenderAddressRequest.Street = Street;
            UpdateOffenderAddressRequest.ZipCode = ZipCode;
            UpdateOffenderAddressRequest.ZoneID = ZoneID;

            if (PhoneNumber != null)
            {
                var EntPhone = new Offenders2.EntPhone();
                EntPhone.PhoneNumber = PhoneNumber;
                Offenders2.EntPhone[] ListEntPhones = { EntPhone };
                UpdateOffenderAddressRequest.Phones = ListEntPhones;
            }
        }
    }
}
