﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using System.Collections.Generic;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgAddOffenderContactRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactFirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.ContactLastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.ContactMiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Priority, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Priority { get; set; }

        [PropertyTest(EnumPropertyName.Relation, EnumPropertyType.ContactRelation, EnumPropertyModifier.Mandatory)]
        public string Relation { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgAddContactRequest AddOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderContactRequest = new Offenders0.EntMsgAddContactRequest();
            AddOffenderContactRequest.FirstName = FirstName;
            AddOffenderContactRequest.LastName = LastName;
            AddOffenderContactRequest.MiddleName = MiddleName;
            AddOffenderContactRequest.OffenderID = OffenderID;
            AddOffenderContactRequest.Phone = Phone;
            AddOffenderContactRequest.PrefixPhone = PrefixPhone;
            AddOffenderContactRequest.Priority = Priority;
            AddOffenderContactRequest.Relation = Relation;
        }
    }


    public class CreateEntMsgAddOffenderContactRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactFirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.ContactLastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.ContactMiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Priority, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Priority { get; set; }

        [PropertyTest(EnumPropertyName.Relation, EnumPropertyType.ContactRelation, EnumPropertyModifier.Mandatory)]
        public string Relation { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgAddOffenderContactRequest AddOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderContactRequest = new Offenders1.EntMsgAddOffenderContactRequest();
            AddOffenderContactRequest.FirstName = FirstName;
            AddOffenderContactRequest.LastName = LastName;
            AddOffenderContactRequest.MiddleName = MiddleName;
            AddOffenderContactRequest.OffenderID = OffenderID;
            AddOffenderContactRequest.Phone = Phone;
            AddOffenderContactRequest.PrefixPhone = PrefixPhone;
            AddOffenderContactRequest.Priority = Priority;
            AddOffenderContactRequest.Relation = Relation;
        }
    }


    public class CreateEntMsgAddOffenderContactRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ContactFirstName, EnumPropertyType.FirstName, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.ContactLastName, EnumPropertyType.LastName, EnumPropertyModifier.Mandatory)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.ContactMiddleName, EnumPropertyType.MiddleName, EnumPropertyModifier.Mandatory)]
        public string MiddleName { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.ContactPhoneNumber, EnumPropertyModifier.Mandatory)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.PrefixPhone, EnumPropertyType.ContactPrefixPhone, EnumPropertyModifier.Mandatory)]
        public string PrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.Priority, EnumPropertyType.Int, EnumPropertyModifier.Mandatory)]
        public int Priority { get; set; }

        [PropertyTest(EnumPropertyName.Relation, EnumPropertyType.ContactRelation, EnumPropertyModifier.Mandatory)]
        public string Relation { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgAddOffenderContactRequest AddOffenderContactRequest { get; set; }

        protected override void ExecuteBlock()
        {
            AddOffenderContactRequest = new Offenders2.EntMsgAddOffenderContactRequest();
            AddOffenderContactRequest.FirstName = FirstName;
            AddOffenderContactRequest.LastName = LastName;
            AddOffenderContactRequest.MiddleName = MiddleName;
            AddOffenderContactRequest.OffenderID = OffenderID;
            AddOffenderContactRequest.Phone = Phone;
            AddOffenderContactRequest.PrefixPhone = PrefixPhone;
            AddOffenderContactRequest.Priority = Priority;
            AddOffenderContactRequest.Relation = Relation;
        }
    }
}
