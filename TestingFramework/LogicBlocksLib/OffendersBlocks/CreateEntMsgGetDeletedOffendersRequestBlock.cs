﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgGetDeletedOffendersRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimestamp { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgGetDeletedOffendersRequest GetDeletedOffendersRequest { get; set; }
        
        protected override void ExecuteBlock()
        {
            GetDeletedOffendersRequest = new Offenders0.EntMsgGetDeletedOffendersRequest();
            GetDeletedOffendersRequest.LastTimeStamp = (long)LastTimestamp;
        }
    }


    public class CreateEntMsgGetDeletedOffendersRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long LastTimestamp { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgGetDeletedOffendersRequest GetDeletedOffendersRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetDeletedOffendersRequest = new Offenders1.EntMsgGetDeletedOffendersRequest();
            GetDeletedOffendersRequest.LastTimeStamp = new Nullable<long>(LastTimestamp);
        }
    }


    public class CreateEntMsgGetDeletedOffendersRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.LastTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long LastTimestamp { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgGetDeletedOffendersRequest GetDeletedOffendersRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetDeletedOffendersRequest = new Offenders2.EntMsgGetDeletedOffendersRequest();
            GetDeletedOffendersRequest.LastTimeStamp = new Nullable<long>(LastTimestamp);
        }
    }
}
