﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.ComponentModel;
using TestingFramework.Proxies.API.Offenders;
#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion


namespace LogicBlocksLib.OffendersBlocks
{
    public class CreateEntMsgDeleteOffenderRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ulong Timestamp { get; set; }


        [PropertyTest(EnumPropertyName.DeleteOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntMsgDeleteOffenderRequest DeleteOffenderRequest { get; set; }



        protected override void ExecuteBlock()
        {
            DeleteOffenderRequest = new Offenders0.EntMsgDeleteOffenderRequest();
            DeleteOffenderRequest.OffenderID = OffenderID;
            DeleteOffenderRequest.Timestamp = Timestamp;
        }


    }


    public class CreateEntMsgDeleteOffenderRequestBlock_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong Timestamp { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders1.EntMsgDeleteOffenderRequest DeleteOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            DeleteOffenderRequest = new Offenders1.EntMsgDeleteOffenderRequest();
            DeleteOffenderRequest.OffenderID = OffenderID;
            DeleteOffenderRequest.Timestamp = Timestamp;
        }
    }


    public class CreateEntMsgDeleteOffenderRequestBlock_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Timestamp, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public long Timestamp { get; set; }

        [PropertyTest(EnumPropertyName.DeleteOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders2.EntMsgDeleteOffenderRequest DeleteOffenderRequest { get; set; }



        protected override void ExecuteBlock()
        {
            DeleteOffenderRequest = new Offenders2.EntMsgDeleteOffenderRequest();
            DeleteOffenderRequest.OffenderID = OffenderID;
            DeleteOffenderRequest.Timestamp = Timestamp;
        }
    }
}
