﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure;

namespace LogicBlocksLib.WebTestBlocks
{
    public abstract class Web_LogicBlockBase : LogicBlockBase
    {
        protected override void ExecuteBlock(){ }

        protected void Run(Action action)
        {
            try
            {
                action();
            }
            catch (Exception exfail)
            {
                WebTests.SeleniumWrapper.Logger.Fail(exfail);
                throw new WebTestingException("Web testing exception: See Web log for details");
                throw;
            }
        }
    }
}
