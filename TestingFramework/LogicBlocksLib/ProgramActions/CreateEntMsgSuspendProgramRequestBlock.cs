﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSuspendProgramRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.Reason, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Reason { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgSuspendProgramRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgSuspendProgramRequest EntMsgSuspendProgramRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgSuspendProgramRequest = new ProgramActions0.EntMsgSuspendProgramRequest();
            EntMsgSuspendProgramRequest.Comment = Comment;
            EntMsgSuspendProgramRequest.OffenderID = OffenderID;
            EntMsgSuspendProgramRequest.Reason = Reason;
        }
    }
}
