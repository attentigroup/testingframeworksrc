﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendDownloadRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Immediate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool Immediate { get; set; }

        [PropertyTest(EnumPropertyName.IsMultipleDownload, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsMultipleDownload { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendDownloadRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgSendDownloadRequest EntMsgSendDownloadRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgSendDownloadRequest = new ProgramActions0.EntMsgSendDownloadRequest();
            EntMsgSendDownloadRequest.Immediate = Immediate;
            EntMsgSendDownloadRequest.IsMultipleDownload = IsMultipleDownload;
            EntMsgSendDownloadRequest.OffenderID = OffenderID;
        }

    }
}
