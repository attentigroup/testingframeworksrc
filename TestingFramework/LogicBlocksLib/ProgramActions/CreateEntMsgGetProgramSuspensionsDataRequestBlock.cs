﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgGetProgramSuspensionsDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.SuspendID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int SuspendID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetProgramSuspensionsDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetProgramSuspensionsDataRequest EntMsgGetProgramSuspensionsDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetProgramSuspensionsDataRequest = new ProgramActions0.EntMsgGetProgramSuspensionsDataRequest();
            EntMsgGetProgramSuspensionsDataRequest.OffenderID = OffenderID;
            EntMsgGetProgramSuspensionsDataRequest.SuspendID = SuspendID;
        }
    }
}
