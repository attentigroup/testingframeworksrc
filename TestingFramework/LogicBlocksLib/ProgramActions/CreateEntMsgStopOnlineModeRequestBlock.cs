﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgStopOnlineModeRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ReceiverSN, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ReceiverSN { get; set; }

        [PropertyTest(EnumPropertyName.StopOnlineModeRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgStopOnlineModeRequest StopOnlineModeRequest { get; set; }
        protected override void ExecuteBlock()
        {
            StopOnlineModeRequest = new ProgramActions0.EntMsgStopOnlineModeRequest();
            StopOnlineModeRequest.ReceiverSN = ReceiverSN;
        }
    }
}
