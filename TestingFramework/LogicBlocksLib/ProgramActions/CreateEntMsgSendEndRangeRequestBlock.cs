﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendEndRangeRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendEndRangeTestRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgSendEndRangeTestRequest EntMsgSendEndRangeTestRequest { get; set; }
        protected override void ExecuteBlock()
        {
            EntMsgSendEndRangeTestRequest = new ProgramActions0.EntMsgSendEndRangeTestRequest();
            EntMsgSendEndRangeTestRequest.OffenderID = OffenderID;
        }
    }
}
