﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgGetEndOfServiceDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? Version { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEndOfServiceDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgGetEndOfServiceDataRequest EntMsgGetEndOfServiceDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetEndOfServiceDataRequest = new ProgramActions0.EntMsgGetEndOfServiceDataRequest();
            EntMsgGetEndOfServiceDataRequest.OffenderID = OffenderID;
            EntMsgGetEndOfServiceDataRequest.Version = Version;
        }
    }
}
