﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgClearCurfewUnitTamperRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgClearCurfewUnitTamperRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgClearCurfewUnitTamperRequest EntMsgClearCurfewUnitTamperRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgClearCurfewUnitTamperRequest = new ProgramActions0.EntMsgClearCurfewUnitTamperRequest();
            EntMsgClearCurfewUnitTamperRequest.OffenderID = OffenderID;
        }
    }
}
