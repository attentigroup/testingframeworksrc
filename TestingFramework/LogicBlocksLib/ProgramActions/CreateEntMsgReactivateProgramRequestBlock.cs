﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgReactivateProgramRequestBlock : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.Reason, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Reason { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgReactivateProgramRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgReactivateProgramRequest EntMsgReactivateProgramRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgReactivateProgramRequest = new ProgramActions0.EntMsgReactivateProgramRequest();
            EntMsgReactivateProgramRequest.Comment = Comment;
            EntMsgReactivateProgramRequest.OffenderID = OffenderID;
            EntMsgReactivateProgramRequest.Reason = Reason;
        }
    }
}
