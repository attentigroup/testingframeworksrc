﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendEndOfServiceRequest : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.EndOfServiceCode, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EndOfServiceCode { get; set; }

        [PropertyTest(EnumPropertyName.EndOfServiceType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EnmEOSType EndOfServiceType { get; set; }
        
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PendingRequestID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int PendingRequestID { get; set; }

        [PropertyTest(EnumPropertyName.StopProgramWithoutDeactivation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool StopProgramWithoutDeactivation { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgSendEndOfServiceRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgSendEndOfServiceRequest EntMsgSendEndOfServiceRequest

        { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgSendEndOfServiceRequest = new ProgramActions0.EntMsgSendEndOfServiceRequest();
            EntMsgSendEndOfServiceRequest.Comment = Comment;
            EntMsgSendEndOfServiceRequest.EndOfServiceCode = EndOfServiceCode;
            EntMsgSendEndOfServiceRequest.EndOfServiceType = EndOfServiceType;
            EntMsgSendEndOfServiceRequest.OffenderID = OffenderID;
            EntMsgSendEndOfServiceRequest.PendingRequestID = PendingRequestID;
            EntMsgSendEndOfServiceRequest.StopProgramWithoutDeactivation = StopProgramWithoutDeactivation;
        }
    }
}
