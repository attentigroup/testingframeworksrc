﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendVibrationRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendVibrationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgSendVibrationRequest EntMsgSendVibrationRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgSendVibrationRequest = new ProgramActions0.EntMsgSendVibrationRequest();
            EntMsgSendVibrationRequest.OffenderID = OffenderID;
        }
    }
}
