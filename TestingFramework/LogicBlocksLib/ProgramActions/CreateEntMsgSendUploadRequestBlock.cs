﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendUploadRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendUploadRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgSendUploadRequest EntMsgSendUploadRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgSendUploadRequest = new ProgramActions0.EntMsgSendUploadRequest();
            EntMsgSendUploadRequest.OffenderID = OffenderID;
        }
    }
}
