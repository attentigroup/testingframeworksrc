﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendMessageRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Message { get; set; }

        [PropertyTest(EnumPropertyName.Immediate, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool Immediate { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendMessageRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public ProgramActions0.EntMsgSendMessageRequest EntMsgSendMessageRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgSendMessageRequest = new ProgramActions0.EntMsgSendMessageRequest();
            EntMsgSendMessageRequest.OffenderID = OffenderID;
            EntMsgSendMessageRequest.Message = Message;
            EntMsgSendMessageRequest.Immediate = Immediate;
        }
    }
}
