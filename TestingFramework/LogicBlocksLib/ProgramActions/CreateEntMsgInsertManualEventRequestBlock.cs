﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgInsertManualEventRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Comment, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comment { get; set; }

        [PropertyTest(EnumPropertyName.EventCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EventCode { get; set; }

        [PropertyTest(EnumPropertyName.EventSubTypeCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EventSubTypeCode { get; set; }

        [PropertyTest(EnumPropertyName.HandlingStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EnmHandlingStatusType HandlingStatus { get; set; }


        [PropertyTest(EnumPropertyName.InsertManualEventRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgInsertManualEventRequest InsertManualEventRequest { get; set; }

        protected override void ExecuteBlock()
        {
            InsertManualEventRequest = new ProgramActions0.EntMsgInsertManualEventRequest();
            InsertManualEventRequest.Comment = Comment;
            InsertManualEventRequest.EventCode = EventCode;
            InsertManualEventRequest.EventSubTypeCode = EventSubTypeCode;
            InsertManualEventRequest.HandlingStatus = HandlingStatus;
            InsertManualEventRequest.OffenderID = OffenderID;
        }
    }
}
