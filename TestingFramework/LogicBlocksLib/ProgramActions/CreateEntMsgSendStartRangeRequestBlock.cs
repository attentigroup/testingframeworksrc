﻿using System;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgSendStartRangeRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Buzzer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool Buzzer { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.Range, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Range { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendStartRangeTestRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgSendStartRangeTestRequest EntMsgSendStartRangeTestRequest { get; set; }
        protected override void ExecuteBlock()
        {
            EntMsgSendStartRangeTestRequest = new ProgramActions0.EntMsgSendStartRangeTestRequest();
            EntMsgSendStartRangeTestRequest.Buzzer = Buzzer;
            EntMsgSendStartRangeTestRequest.OffenderID = OffenderID;
            EntMsgSendStartRangeTestRequest.Range = Range;

        }
    }
}
