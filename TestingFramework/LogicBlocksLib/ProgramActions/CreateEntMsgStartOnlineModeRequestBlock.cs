﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace LogicBlocksLib.ProgramActions
{
    public class CreateEntMsgStartOnlineModeRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ReceiverSN, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ReceiverSN { get; set; }

        [PropertyTest(EnumPropertyName.StartOnlineModeRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgStartOnlineModeRequest StartOnlineModeRequest { get; set; }
        protected override void ExecuteBlock()
        {
            StartOnlineModeRequest = new ProgramActions0.EntMsgStartOnlineModeRequest();
            StartOnlineModeRequest.ReceiverSN = ReceiverSN;
        }
    }
}
