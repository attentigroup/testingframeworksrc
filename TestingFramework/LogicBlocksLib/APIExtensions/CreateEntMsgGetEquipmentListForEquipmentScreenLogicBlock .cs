﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.APIExtensions
{
    public class CreateEntMsgGetEquipmentListForEquipmentScreenLogicBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.SearchString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public String FreeSearchString { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }

        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmEquipmentModel Model { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.ReturnDamageData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnDamageData { get; set; }

        [PropertyTest(EnumPropertyName.ReturnOnlyDeletable, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnOnlyDeletable { get; set; }

        [PropertyTest(EnumPropertyName.ReturnOnlyNotAllocated, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnOnlyNotAllocated { get; set; }

        [PropertyTest(EnumPropertyName.SerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EntSortExpression SortExpression { get; set; }

        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmEquipmentType Type { get; set; }

        [PropertyTest(EnumPropertyName.GetEquipmentListRequestForEquipmentScreen, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgGetEquipmentListRequestForEquipmentScreen GetEquipmentListRequestForEquipmentScreen { get; set; }

        protected override void ExecuteBlock()
        {

            GetEquipmentListRequestForEquipmentScreen = new EntMsgGetEquipmentListRequestForEquipmentScreen();
            GetEquipmentListRequestForEquipmentScreen.AgencyID = AgencyID;
            GetEquipmentListRequestForEquipmentScreen.EquipmentID = EquipmentID;
            GetEquipmentListRequestForEquipmentScreen.FreeSearchString = FreeSearchString;
            GetEquipmentListRequestForEquipmentScreen.IsDamaged = IsDamaged;
            GetEquipmentListRequestForEquipmentScreen.MaximumRows = MaximumRows;
            if (Model != 0)
                GetEquipmentListRequestForEquipmentScreen.Models = new List<EnmEquipmentModel>() { Model };
            else
                GetEquipmentListRequestForEquipmentScreen.Models = null;
            GetEquipmentListRequestForEquipmentScreen.OffenderID = OffenderID;
            if (ProgramType != 0)
                GetEquipmentListRequestForEquipmentScreen.Programs = new List<EnmProgramType>() { ProgramType };
            else
                GetEquipmentListRequestForEquipmentScreen.Programs = null;
            GetEquipmentListRequestForEquipmentScreen.ReturnDamageData = ReturnDamageData;
            GetEquipmentListRequestForEquipmentScreen.ReturnOnlyDeletable = ReturnOnlyDeletable;
            GetEquipmentListRequestForEquipmentScreen.ReturnOnlyNotAllocated = ReturnOnlyNotAllocated;
            GetEquipmentListRequestForEquipmentScreen.SerialNumber = SerialNumber;
            GetEquipmentListRequestForEquipmentScreen.SortExpression = SortExpression;
            GetEquipmentListRequestForEquipmentScreen.StartRowIndex = StartRowIndex;
            GetEquipmentListRequestForEquipmentScreen.Type = Type;
        }
    }
}
