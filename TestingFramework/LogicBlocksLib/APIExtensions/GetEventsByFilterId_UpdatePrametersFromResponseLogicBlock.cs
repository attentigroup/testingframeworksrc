﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.APIExtensions
{
    public class GetEventsByFilterId_UpdatePrametersFromResponseLogicBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.EntEventDataResult, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntEventDataResult EventDataResult { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_PreviousTimeStamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public byte[] PreviousTimeStamp { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsByFilterID_PageUpperLimitEventId, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int PageUpperLimitEventId { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ForceRefresh { get; set; }//in case of changes in ui for example first request with out searchString and in the second time with searchString so  need to be true.
        protected override void ExecuteBlock()
        {
            PreviousTimeStamp = EventDataResult.CurrentTimeStamp;
            PageUpperLimitEventId = EventDataResult.CurrentMaxEventID;
        }
    }
}
