﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.API.APIExtensions.Security;
using TestingFramework.Proxies.DataObjects;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.APIExtensions
{
    public class GetGUISecurityDataLogicBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.UserType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmUserType UserType { get; set; }

        protected override void ExecuteBlock(){}
    }
}
