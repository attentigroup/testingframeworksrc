﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.APIExtensions
{
    public class SetGUISecurityDataLogicBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string UserName { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.GuiSecurityResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntSecurity> GuiSecurityResponse { get; set; }

        [PropertyTest(EnumPropertyName.ModifyOption, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModifyOption ModifyOption { get; set; }

        [PropertyTest(EnumPropertyName.ModifyStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModify ModifyStatus { get; set; }

        [PropertyTest(EnumPropertyName.State, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmDataRecordStatus State { get; set; }

        protected override void ExecuteBlock()
        {
        }
    }
}
