﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.MwsBlocks
{
    public class CreateEntMsgGetMobileStatusRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AppVersion { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgMwsGetMobileStatusRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsGetMobileStatusRequest GetMobileStatusRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetMobileStatusRequest = new EntMsgMwsGetMobileStatusRequest();
            GetMobileStatusRequest.ApplicationID = ApplicationID;
            GetMobileStatusRequest.MobileID = MobileID;
            GetMobileStatusRequest.AppVersion = AppVersion;
        }
    }
}