﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.MwsBlocks
{
    public class CreateEntMsgGetOffendersListRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.SessionToken, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SessionToken { get; set; }

        [PropertyTest(EnumPropertyName.STT, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string STT { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AppVersion { get; set; }

        [PropertyTest(EnumPropertyName.isOnlyActive, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool isOnlyActive { get; set; }

        [PropertyTest(EnumPropertyName.isOnlyViolation, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool isOnlyViolation { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgMwsGetOffendersListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsGetOffendersListRequest GetOffendersListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffendersListRequest = new EntMsgMwsGetOffendersListRequest();
            GetOffendersListRequest.ApplicationID = ApplicationID;
            GetOffendersListRequest.SessionToken = SessionToken;
            //GetOffendersListRequest.STT = STT;
            GetOffendersListRequest.MobileID = MobileID;
            GetOffendersListRequest.AppVersion = AppVersion;
            GetOffendersListRequest.isOnlyActive = isOnlyActive;
            GetOffendersListRequest.isOnlyViolation = isOnlyViolation;
        }
    }
}