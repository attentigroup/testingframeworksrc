﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.MwsBlocks
{
    public class CreateEntMsgGetTranslationsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.SessionToken, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SessionToken { get; set; }

        [PropertyTest(EnumPropertyName.STT, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string STT { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AppVersion { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgMwsGetTranslationsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsGetTranslationsRequest GetTranslationsRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetTranslationsRequest = new EntMsgMwsGetTranslationsRequest();
            GetTranslationsRequest.ApplicationID = ApplicationID;
            GetTranslationsRequest.SessionToken = SessionToken;
            //GetTranslationsRequest.STT = STT;
            GetTranslationsRequest.MobileID = MobileID;
            GetTranslationsRequest.AppVersion = AppVersion;
        }
    }
}