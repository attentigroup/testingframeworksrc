﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.MwsBlocks
{
    public class CreateEntMsgRegisterRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.Username, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Username { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.Model, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Model { get; set; }

        [PropertyTest(EnumPropertyName.OperatingSystem, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string OperatingSystem { get; set; }

        [PropertyTest(EnumPropertyName.CellularNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string CellularNumber { get; set; }

        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.AppVersion, EnumPropertyModifier.Mandatory)]
        public string AppVersion { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgMwsRegisterRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsRegisterRequest RegisterRequest { get; set; }

        protected override void ExecuteBlock()
        {
            RegisterRequest = new EntMsgMwsRegisterRequest();
            var credentials = new Credentials();
            var mobileData = new MobileData();

            credentials.UserName = Username;
            credentials.Password = Password;

            mobileData.Model = Model;
            mobileData.OperatingSystem = OperatingSystem;
            mobileData.CellularNumber = CellularNumber;

            RegisterRequest.credentials = credentials;
            RegisterRequest.mobileData = mobileData;

            RegisterRequest.ApplicationID = ApplicationID;
            RegisterRequest.MobileID = MobileID;
            RegisterRequest.AppVersion = AppVersion;
        }
    }
}
