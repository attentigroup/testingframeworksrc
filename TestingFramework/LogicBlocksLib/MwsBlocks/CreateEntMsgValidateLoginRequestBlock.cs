﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.MwsBlocks
{
    public class CreateEntMsgValidateLoginRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.Username, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Username { get; set; }

        [PropertyTest(EnumPropertyName.Password, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Password { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string AppVersion { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgValidateLoginRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsValidateLoginRequest ValidateLoginRequest { get; set; }


        protected override void ExecuteBlock()
        {
            ValidateLoginRequest = new EntMsgMwsValidateLoginRequest();
            ValidateLoginRequest.ApplicationID = ApplicationID;
            ValidateLoginRequest.Username = Username;
            ValidateLoginRequest.Password = Password;
            ValidateLoginRequest.MobileID = MobileID;
            ValidateLoginRequest.AppVersion = AppVersion;
        }

    }
}
