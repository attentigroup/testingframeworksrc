﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.Mws;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.MwsBlocks
{
    public class CreateEntMsgLogoutRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.SessionToken, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string SessionToken { get; set; }

        [PropertyTest(EnumPropertyName.STT, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string STT { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string MobileID { get; set; }

        [PropertyTest(EnumPropertyName.AppVersion, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string AppVersion { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgMwsLogoutRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public EntMsgMwsLogoutRequest LogoutRequest { get; set; }

        protected override void ExecuteBlock()
        {
            LogoutRequest = new EntMsgMwsLogoutRequest();
            LogoutRequest.ApplicationID = ApplicationID;
            LogoutRequest.SessionToken = SessionToken;
            //LogoutRequest.STT = STT;
            LogoutRequest.MobileID = MobileID;
            LogoutRequest.AppVersion = AppVersion;
        }
    }
}
