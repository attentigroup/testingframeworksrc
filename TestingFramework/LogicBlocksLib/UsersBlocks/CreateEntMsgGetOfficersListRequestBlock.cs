﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.UsersBlocks
{
    public class CreateEntMsgGetOfficersListRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetOfficerRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntMsgGetOfficersListRequest GetOfficersListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOfficersListRequest = new Users0.EntMsgGetOfficersListRequest();
            GetOfficersListRequest.OfficerID = OfficerID;
            GetOfficersListRequest.AgencyID = AgencyID;
        }
    }
}
