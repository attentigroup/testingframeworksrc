﻿using System;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.ProgramActions;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion


namespace LogicBlocksLib.UsersBlocks
{
    public class CreateEntMsgGetUsersListRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.StartRowIndex, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int StartRowIndex { get; set; }

        [PropertyTest(EnumPropertyName.MaximumRows, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MaximumRows { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Users0.EnmUsersSortOptions? SortField { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Users0.ListSortDirection? SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.UserID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? UserID { get; set; }

        [PropertyTest(EnumPropertyName.UserType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Users0.EnmUserType UserType { get; set; }

        [PropertyTest(EnumPropertyName.UserName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string UserName { get; set; }

        [PropertyTest(EnumPropertyName.FirstName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FirstName { get; set; }

        [PropertyTest(EnumPropertyName.LastName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string LastName { get; set; }

        [PropertyTest(EnumPropertyName.UserStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Users0.EnmUserStatus? UserStatus { get; set; }

        [PropertyTest(EnumPropertyName.IncludeDeletedUsers, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeDeletedUsers { get; set; }

        [PropertyTest(EnumPropertyName.Phone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Phone { get; set; }

        [PropertyTest(EnumPropertyName.Email, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Email { get; set; }

        [PropertyTest(EnumPropertyName.MrdSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string MrdSN { get; set; }

        [PropertyTest(EnumPropertyName.SearchString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SearchString { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetUsersListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntMsgGetUsersListRequest EntMsgGetUsersListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetUsersListRequest = new Users0.EntMsgGetUsersListRequest();
            EntMsgGetUsersListRequest.Email = Email;
            EntMsgGetUsersListRequest.FirstName = FirstName;
            EntMsgGetUsersListRequest.IncludeDeletedUsers = IncludeDeletedUsers;
            EntMsgGetUsersListRequest.LastName = LastName;
            EntMsgGetUsersListRequest.MaximumRows = MaximumRows;
            EntMsgGetUsersListRequest.MrdSN = MrdSN;
            EntMsgGetUsersListRequest.Phone = Phone;
            EntMsgGetUsersListRequest.SearchString = SearchString;
            EntMsgGetUsersListRequest.SortDirection = SortDirection;
            EntMsgGetUsersListRequest.SortField = SortField;
            EntMsgGetUsersListRequest.StartRowIndex = StartRowIndex;
            EntMsgGetUsersListRequest.UserID = UserID;
            EntMsgGetUsersListRequest.UserName = UserName;
            EntMsgGetUsersListRequest.UserStatus = UserStatus;
            EntMsgGetUsersListRequest.UserType = UserType;
        }
    }
}
