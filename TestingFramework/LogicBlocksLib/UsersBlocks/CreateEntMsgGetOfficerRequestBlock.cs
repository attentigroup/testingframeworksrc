﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;

namespace LogicBlocksLib.UsersBlocks
{
    public class CreateEntMsgGetOfficerRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetOfficerRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Users0.EntMsgGetOfficerRequest EntMsgGetOfficerRequest { get; set; }

        protected override void ExecuteBlock()
        {
            EntMsgGetOfficerRequest = new Users0.EntMsgGetOfficerRequest();
            EntMsgGetOfficerRequest.OfficerID = OfficerID;
            EntMsgGetOfficerRequest.AgencyID = AgencyID;
        }
    }
}
