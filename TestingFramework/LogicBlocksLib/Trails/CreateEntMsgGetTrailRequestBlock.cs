﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Agency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace LogicBlocksLib.Trails
{
    public class CreateEntMsgGetTrailRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CoordinatesFormat, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EnmCoordinatesFormat CoordinatesFormat { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.MinimumMeters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MinimumMeters { get; set; }

        [PropertyTest(EnumPropertyName.MinimumSeconds, EnumPropertyType.MinimumSeconds, EnumPropertyModifier.Mandatory)]
        public int MinimumSeconds { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyViolation { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.IncludeNoLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeNoLocation { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails0.EntMsgGetTrailRequest GetTrailRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetTrailRequest = new Trails0.EntMsgGetTrailRequest();
            GetTrailRequest.CoordinatesFormat = CoordinatesFormat;
            GetTrailRequest.EndTime = EndTime;
            GetTrailRequest.MinimumMeters = MinimumMeters;
            GetTrailRequest.MinimumSeconds = MinimumSeconds;
            GetTrailRequest.OffenderID = OffenderID;
            GetTrailRequest.OnlyViolation = OnlyViolation;
            GetTrailRequest.RelatedOffenderID = RelatedOffenderID;
            GetTrailRequest.StartTime = StartTime;
            GetTrailRequest.IncludeNoLocation = IncludeNoLocation;
        }
    }

    public class CreateEntMsgGetTrailRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CoordinatesFormat, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails1.EnmCoordinatesFormat CoordinatesFormat { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.MinimumMeters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MinimumMeters { get; set; }

        [PropertyTest(EnumPropertyName.MinimumSeconds, EnumPropertyType.MinimumSeconds, EnumPropertyModifier.Mandatory)]
        public int MinimumSeconds { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyViolation { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails1.EntMsgGetTrailRequest GetTrailRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetTrailRequest = new Trails1.EntMsgGetTrailRequest();
            GetTrailRequest.CoordinatesFormat = CoordinatesFormat;
            GetTrailRequest.EndTime = EndTime;
            GetTrailRequest.MinimumMeters = MinimumMeters;
            GetTrailRequest.MinimumSeconds = MinimumSeconds;
            GetTrailRequest.OffenderID = OffenderID;
            GetTrailRequest.OnlyViolation = OnlyViolation;
            GetTrailRequest.RelatedOffenderID = RelatedOffenderID;
            GetTrailRequest.StartTime = StartTime;
        }
    }

    public class CreateEntMsgGetTrailRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.CoordinatesFormat, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EnmCoordinatesFormat CoordinatesFormat { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.MinimumMeters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MinimumMeters { get; set; }

        [PropertyTest(EnumPropertyName.MinimumSeconds, EnumPropertyType.MinimumSeconds, EnumPropertyModifier.Mandatory)]
        public int MinimumSeconds { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyViolation { get; set; }

        [PropertyTest(EnumPropertyName.RelatedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? RelatedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails2.EntMsgGetTrailRequest GetTrailRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetTrailRequest = new Trails2.EntMsgGetTrailRequest();
            GetTrailRequest.CoordinatesFormat = CoordinatesFormat;
            GetTrailRequest.EndTime = EndTime;
            GetTrailRequest.MinimumMeters = MinimumMeters;
            GetTrailRequest.MinimumSeconds = MinimumSeconds;
            GetTrailRequest.OffenderID = OffenderID;
            GetTrailRequest.OnlyViolation = OnlyViolation;
            GetTrailRequest.RelatedOffenderID = RelatedOffenderID;
            GetTrailRequest.StartTime = StartTime;
        }
    }
}
