﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace LogicBlocksLib.Trails
{
    public class CreateEntMsgGetGeographicLocationsRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.IncludeHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeHistory { get; set; }

        [PropertyTest(EnumPropertyName.LastTimestamp, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ulong LastTimestamp { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? Limit { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.OffendersIDs, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int []  OffendersIDs { get; set; }

        [PropertyTest(EnumPropertyName.PositionsIDs, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long []  PositionsIDs { get; set; }

        [PropertyTest(EnumPropertyName.ProgramTypes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmProgramType []  ProgramTypes { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmPositionsSortOptions SortField { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.GetGeographicLocationsRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails0.EntMsgGetGeographicLocationsRequest GetGeographicLocationsRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetGeographicLocationsRequest = new Trails0.EntMsgGetGeographicLocationsRequest();
            GetGeographicLocationsRequest.AgencyID = AgencyID;
            GetGeographicLocationsRequest.EndTime = EndTime;
            GetGeographicLocationsRequest.IncludeHistory = IncludeHistory;
            GetGeographicLocationsRequest.LastTimestamp = LastTimestamp;
            GetGeographicLocationsRequest.Limit = Limit;
            GetGeographicLocationsRequest.OffenderRefID = OffenderRefID;
            GetGeographicLocationsRequest.OffendersIDs = OffendersIDs;
            GetGeographicLocationsRequest.PositionsIDs = PositionsIDs;
            GetGeographicLocationsRequest.ProgramTypes = ProgramTypes;
            GetGeographicLocationsRequest.SortDirection = SortDirection;
            GetGeographicLocationsRequest.SortField = SortField;
            GetGeographicLocationsRequest.StartTime = StartTime;

        }
    }
}
