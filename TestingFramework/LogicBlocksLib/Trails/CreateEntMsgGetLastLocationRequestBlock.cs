﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators.Agency;
using DataGenerators.Offender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace LogicBlocksLib.Trails
{
    public class CreateEntMsgGetLastLocationRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.CoordinatesFormat, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmCoordinatesFormat CoordinatesFormat { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInUnknownLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInUnknownLocation { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInViolation { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmLastLocationProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails0.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetLastLocationRequest = new Trails0.EntMsgGetLastLocationRequest();
             GetLastLocationRequest.AgencyID = AgencyID;
            GetLastLocationRequest.CoordinatesFormat = CoordinatesFormat;
            if (AgencyID != 0 && OfficerID == 0)
                GetLastLocationRequest.OfficerID = OffenderWithTrailIDGenerator.GetOffenderWithTrailOfficerID(AgencyID);
            else
                GetLastLocationRequest.OfficerID = OfficerID;
            GetLastLocationRequest.OnlyInUnknownLocation = OnlyInUnknownLocation;
            GetLastLocationRequest.OnlyInViolation = OnlyInViolation;
            GetLastLocationRequest.ProgramType = ProgramType;
        }
    }

    public class CreateEntMsgGetLastLocationRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.CoordinatesFormat, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails1.EnmCoordinatesFormat CoordinatesFormat { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInUnknownLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInUnknownLocation { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInViolation { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails1.EnmLastLocationProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails1.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetLastLocationRequest = new Trails1.EntMsgGetLastLocationRequest();
            if (AgencyID == 0)
            {
                GetLastLocationRequest.AgencyID = OffenderWithTrailIDGenerator.GetOffenderWithTrailAgencyID();

            }
            AgencyID = GetLastLocationRequest.AgencyID;
            GetLastLocationRequest.CoordinatesFormat = CoordinatesFormat;
            if (OfficerID == 0)
            {
                GetLastLocationRequest.OfficerID = OffenderWithTrailIDGenerator.GetOffenderWithTrailOfficerID(AgencyID);
            }
            GetLastLocationRequest.OnlyInUnknownLocation = OnlyInUnknownLocation;
            GetLastLocationRequest.OnlyInViolation = OnlyInViolation;
            GetLastLocationRequest.ProgramType = ProgramType;
        }
    }

    public class CreateEntMsgGetLastLocationRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.CoordinatesFormat, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails2.EnmCoordinatesFormat CoordinatesFormat { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInUnknownLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInUnknownLocation { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInViolation { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails2.EnmLastLocationProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Trails2.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetLastLocationRequest = new Trails2.EntMsgGetLastLocationRequest();
            if (AgencyID == 0)
            {
                GetLastLocationRequest.AgencyID = OffenderWithTrailIDGenerator.GetOffenderWithTrailAgencyID();

            }
            AgencyID = GetLastLocationRequest.AgencyID;
            GetLastLocationRequest.CoordinatesFormat = CoordinatesFormat;
            if (OfficerID == 0)
            {
                GetLastLocationRequest.OfficerID = OffenderWithTrailIDGenerator.GetOffenderWithTrailOfficerID(AgencyID);
            }
            GetLastLocationRequest.OnlyInUnknownLocation = OnlyInUnknownLocation;
            GetLastLocationRequest.OnlyInViolation = OnlyInViolation;
            GetLastLocationRequest.ProgramType = ProgramType;
        }
    }
}
