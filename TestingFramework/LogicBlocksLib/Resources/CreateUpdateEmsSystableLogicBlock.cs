﻿using Common.CustomAttributes;
using Common.Enum;
using DataGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Implementation;

namespace LogicBlocksLib.Resources
{
    public class CreateUpdateEmsSystableLogicBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.TableID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableID { get; set; }

        [PropertyTest(EnumPropertyName.ValueCode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ValueCode { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Description { get; set; }

        [PropertyTest(EnumPropertyName.Language, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Language { get; set; }

        [PropertyTest(EnumPropertyName.EnmDBAction, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmDBAction Action { get; set; }

        [PropertyTest(EnumPropertyName.EmsSystableDictionary, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Dictionary<EnmDBAction,List<EntSysTableItem>> InputCustomFieldsDictionary { get; set; }

        [PropertyTest(EnumPropertyName.EmsSystableDictionary, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Dictionary<EnmDBAction, List<EntSysTableItem>> CleanUp_CustomFieldDictionary { get; set; }


        protected override void ExecuteBlock()
        {
            ValueCode = CreateNewValueCode();
 
            if(InputCustomFieldsDictionary == null)
            {
                InputCustomFieldsDictionary = new Dictionary<EnmDBAction, List<EntSysTableItem>>();
                CleanUp_CustomFieldDictionary = new Dictionary<EnmDBAction, List<EntSysTableItem>>();

                InputCustomFieldsDictionary.Add(EnmDBAction.I, null);
                InputCustomFieldsDictionary.Add(EnmDBAction.U, null);
                InputCustomFieldsDictionary.Add(EnmDBAction.D, null);

                CleanUp_CustomFieldDictionary.Add(EnmDBAction.I, null);
                CleanUp_CustomFieldDictionary.Add(EnmDBAction.U, null);
                CleanUp_CustomFieldDictionary.Add(EnmDBAction.D, null);
            }

            var sysTableItem = new EntSysTableItem() { TableID = TableID, ValueCode = ValueCode, LanguageCode = Language, Description = Description };

            switch (Action)
            {
                case EnmDBAction.I:
                    if (InputCustomFieldsDictionary[EnmDBAction.I] == null)
                        InputCustomFieldsDictionary[EnmDBAction.I] = new List<EntSysTableItem>();

                    InputCustomFieldsDictionary[EnmDBAction.I].Add(sysTableItem);

                    if(CleanUp_CustomFieldDictionary[EnmDBAction.D] == null)
                        CleanUp_CustomFieldDictionary[EnmDBAction.D] = new List<EntSysTableItem>();

                    CleanUp_CustomFieldDictionary[EnmDBAction.D].Add(sysTableItem);
                    break;
                case EnmDBAction.R:
                    break;
                case EnmDBAction.U:
                    if(InputCustomFieldsDictionary[EnmDBAction.U] == null)
                        InputCustomFieldsDictionary[EnmDBAction.U] = new List<EntSysTableItem>();

                    InputCustomFieldsDictionary[EnmDBAction.U].Add(sysTableItem);
                    break;
                case EnmDBAction.D:
                    if (InputCustomFieldsDictionary[EnmDBAction.D] == null)
                        InputCustomFieldsDictionary[EnmDBAction.D] = new List<EntSysTableItem>();

                    InputCustomFieldsDictionary[EnmDBAction.D].Add(sysTableItem);

                    if (CleanUp_CustomFieldDictionary[EnmDBAction.I] == null)
                        CleanUp_CustomFieldDictionary[EnmDBAction.I] = new List<EntSysTableItem>();

                    CleanUp_CustomFieldDictionary[EnmDBAction.I].Add(sysTableItem);
                    break;
                default:
                    break;
            }
        }

        private string CreateNewValueCode()
        {
            var stringGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.String) as StringGenerator;
            var specialString = stringGenerator.GenerateRandomStringWithLength(10);
            return specialString;  
        }
    }
}
