﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.TestsInfraStructure.Implementation;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

using WebTests.InfraStructure.Entities;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
#endregion


namespace LogicBlocksLib.ConfigurationBlocks
{
   public class CreateConfigurationListToUpdatedListTestBlocks : LogicBlockBase
    {
       
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse EntMsgGetOffenderConfigurationDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.ElementsList, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public List<WebTests.InfraStructure.Entities.ConfigurationElement> ElementsList { get; set; }

        public static int ONE_PIECE_GEN_2 = 46;
        public static int ONE_PIECE_GEN_39 = 80;
        public static int ONE_PIECE_TD4I = 84;
        public static int TWO_PIECE_DV = 42;
        public static int TWO_PIECE = 82;


        protected override void ExecuteBlock()
        {
            var offender = GetOffendersResponse.OffendersList[0];

            addToListTrackerElements(offender);
            addToListE4Elements(offender);
        }

        public void addToListTrackerElements(Offenders0.EntOffender offender)
        {
            if (offender.ProgramType == Offenders0.EnmProgramType.One_Piece_RF || offender.ProgramType == Offenders0.EnmProgramType.Two_Piece)
            {

                var res = (Configuration0.EntConfigurationGPS)EntMsgGetOffenderConfigurationDataResponse.ConfigurationData;
                ElementsList = new List<WebTests.InfraStructure.Entities.ConfigurationElement>();

                if (offender.EquipmentInfo.HomeUnitSN != null || offender.EquipmentInfo.ReceiverModel == TWO_PIECE)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("range", ConfigElemntType.ConfigElemntType_dropdown_3_And_More_Options, res.ReceiverRange.ToString()));

                if (offender.EquipmentInfo.ReceiverModel == TWO_PIECE || offender.EquipmentInfo.ReceiverModel == TWO_PIECE_DV)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("txDisappearTime0", ConfigElemntType.ConfigElemntType_string, (res.TransmitterDisappearTime + 1).ToString()));

                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("normalLoggingRate0", ConfigElemntType.ConfigElemntType_string, (res.NormalLoggingRate + 1).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("violationLoggingRate0", ConfigElemntType.ConfigElemntType_string, (res.ViolationLoggingRate + 1).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("timeBetweenUploads0", ConfigElemntType.ConfigElemntType_string, (res.TimeBetweenUploads - 1).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("scheduleActive0", ConfigElemntType.ConfigElemntType_dropdown_2_Options, (!res.ScheduleActive).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkPassiveMode", ConfigElemntType.ConfigElemntType_bool, (!res.PassiveMode).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkExtraTX", ConfigElemntType.ConfigElemntType_bool, (!res.DetectOtherTransmitters).ToString()));

                if (offender.EquipmentInfo.ReceiverModel == ONE_PIECE_GEN_2 || offender.EquipmentInfo.ReceiverModel == ONE_PIECE_GEN_39 || offender.EquipmentInfo.ReceiverModel == TWO_PIECE_DV)
                {
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkGPRS", ConfigElemntType.ConfigElemntType_bool, (!res.SupportGPRS).ToString()));
                    if (res.SupportGPRS == true)
                        ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkCSD", ConfigElemntType.ConfigElemntType_bool, (true).ToString()));
                    else
                        ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkCSD", ConfigElemntType.ConfigElemntType_bool, (false).ToString()));
                }

                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkEnable", ConfigElemntType.ConfigElemntType_bool, (!res.LBSActive).ToString()));

                if (res.LBSActive == false)
                {
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkEnableViolations", ConfigElemntType.ConfigElemntType_bool, (!res.LBSForViolations).ToString()));
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("gpsDisappearTime0", ConfigElemntType.ConfigElemntType_string, (res.GPSDisappearTime - 1).ToString()));
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("lbsSamplingRate0", ConfigElemntType.ConfigElemntType_string, (res.LBSSamplingRate + 1).ToString()));
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("lbsRequestTimeout0", ConfigElemntType.ConfigElemntType_string, (res.LBSRequestTimeout + 1).ToString()));
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("lbsNoPositionTimeout0", ConfigElemntType.ConfigElemntType_string, (res.LBSNoPositionTimeout + 1).ToString()));
                }

                if (offender.EquipmentInfo.ReceiverModel == TWO_PIECE_DV || offender.EquipmentInfo.ReceiverModel == TWO_PIECE)
                {
                    if (res.DisplayOffenderName == false)
                        ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("radOffenderName", ConfigElemntType.ConfigElemntType_radio_button, ("radFreeText").ToString()));

                    else
                    {
                        ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("radFreeText", ConfigElemntType.ConfigElemntType_radio_button, ("radOffenderName").ToString()));
                    }

                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("2track_icon", ConfigElemntType.ConfigElemntType_bool, (!res.ReceptionIconsActive).ToString()));


                    if (offender.ProgramConcept == Offenders0.EnmProgramConcept.Victim)
                    {
                        ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("GpsProximityField", ConfigElemntType.ConfigElemntType_string, (res.GPSProximity + 1).ToString()));
                        ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("GpsProximityBufferField", ConfigElemntType.ConfigElemntType_string, (res.GPSProximityBuffer + 1).ToString()));
                    }
                }

            }
        }

        public void addToListE4Elements(Offenders0.EntOffender offender)
        {
            if (offender.ProgramType == Offenders0.EnmProgramType.E4_Dual)
            {
                var res = (Configuration0.EntConfigurationRF)EntMsgGetOffenderConfigurationDataResponse.ConfigurationData;
                ElementsList = new List<WebTests.InfraStructure.Entities.ConfigurationElement>();
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("range", ConfigElemntType.ConfigElemntType_dropdown_3_And_More_Options, res.ReceiverRange.ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("txDisappearTime0", ConfigElemntType.ConfigElemntType_string, (res.TransmitterDisappearTime + 1).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("numberOfRings0", ConfigElemntType.ConfigElemntType_string, (res.NumberOfRings + 1).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("maxTimeBetweenUploads0", ConfigElemntType.ConfigElemntType_string, (res.NumberOfRings + 1).ToString()));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("ringVolume", ConfigElemntType.ConfigElemntType_dropdown_3_And_More_Options, null));
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkIsKosher", ConfigElemntType.ConfigElemntType_bool, (!res.IsKosher).ToString()));
                var val = (res.GraceBeforeTimeFrameStart == 60) ? 1 : res.GraceBeforeTimeFrameStart + 1;
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("gracePreStart0", ConfigElemntType.ConfigElemntType_string, (val).ToString()));
                val = (res.GraceAfterTimeFrameStart == 60) ? 1 : res.GraceAfterTimeFrameStart + 1;
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("gracePostStart0", ConfigElemntType.ConfigElemntType_string, (val).ToString()));
                val = (res.GraceBeforeTimeFrameEnd == 60) ? 1 : res.GraceBeforeTimeFrameEnd + 1;
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("gracePreEnd0", ConfigElemntType.ConfigElemntType_string, (val).ToString()));
                val = (res.GraceAfterTimeFrameEnd == 60) ? 1 : res.GraceAfterTimeFrameEnd + 1;
                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("gracePostEnd0", ConfigElemntType.ConfigElemntType_string, (val).ToString()));

                // Landline
                if (res.SupportLandline == true && res.LandlinePriority == 1)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkLandline", ConfigElemntType.ConfigElemntType_communication_priority, (2).ToString()));
                else if (res.SupportLandline == true && res.LandlinePriority == 2)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkLandline", ConfigElemntType.ConfigElemntType_communication_priority, (1).ToString()));
                else if (res.SupportLandline == true && res.LandlinePriority == 3 && res.GPRSPriority == 1)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkLandline", ConfigElemntType.ConfigElemntType_communication_priority, (1).ToString()));
                else if (res.SupportLandline == true && res.LandlinePriority == 3 && res.GPRSPriority == 2)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkLandline", ConfigElemntType.ConfigElemntType_communication_priority, (2).ToString()));
                else if (res.SupportLandline == false && res.GPRSPriority == 1)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkLandline", ConfigElemntType.ConfigElemntType_communication_priority, (1).ToString()));
                else if (res.SupportLandline == false && res.GPRSPriority == 2)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkLandline", ConfigElemntType.ConfigElemntType_communication_priority, (2).ToString()));


                // SMS Callback
                if (res.SupportSMSCallback == false)
                    ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkSMSCallback", ConfigElemntType.ConfigElemntType_bool, (true).ToString()));

                ElementsList.Add(new WebTests.InfraStructure.Entities.ConfigurationElement("chkOfficerScreenEnable", ConfigElemntType.ConfigElemntType_bool, (!res.OfficerScreenActive).ToString()));


            }
        }
    }

}

