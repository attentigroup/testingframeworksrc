﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using System.Linq;



#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Factories;
using DataGenerators;
using System;
#endregion

namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgUpdateGPSRuleConfigurationRequest : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.GPSDisappearTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateLED, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateLED { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateVibration, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateVibration { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateWarningMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateWarningMessage { get; set; }

        [PropertyTest(EnumPropertyName.IsImmediateUpload, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsImmediateUpload { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.RuleID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RuleID { get; set; }

        [PropertyTest(EnumPropertyName.WarningMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string WarningMessage { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }


        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int GeneratedGraceTime { get; set; }



        protected override void ExecuteBlock()
        {
           
             IsImmediateUpload = !GetGPSRuleConfigurationListResponse.RuleConfigList[0].IsImmediateUpload;

            if (ProgramType == Offenders0.EnmProgramType.One_Piece_RF)
            {
                IsGenerateVibration = !GetGPSRuleConfigurationListResponse.RuleConfigList[0].IsGenerateVibration;
            }
            else if (ProgramType == Offenders0.EnmProgramType.Two_Piece || ProgramType == Offenders0.EnmProgramType.Two_Piece_Passive)
            {
                IsGenerateWarningMessage = !GetGPSRuleConfigurationListResponse.RuleConfigList[0].IsGenerateWarningMessage;
                WarningMessage = "This is a warning message!!!";
            }
            else
            {
                Console.WriteLine("The Program Type does not suitable for use this method");

            }
            UpdateGPSRuleConfigurationRequest = new Configuration0.EntMsgUpdateGPSRuleConfigurationRequest();
            UpdateGPSRuleConfigurationRequest.GraceTime = GraceTime;
            UpdateGPSRuleConfigurationRequest.IsGenerateLED = IsGenerateLED;
            UpdateGPSRuleConfigurationRequest.IsGenerateVibration = IsGenerateVibration;
            UpdateGPSRuleConfigurationRequest.IsGenerateWarningMessage = IsGenerateWarningMessage;
            UpdateGPSRuleConfigurationRequest.IsImmediateUpload =IsImmediateUpload;
            UpdateGPSRuleConfigurationRequest.OffenderID = OffenderID;
            UpdateGPSRuleConfigurationRequest.RuleID = RuleID;
            UpdateGPSRuleConfigurationRequest.WarningMessage = WarningMessage;

        }
    }

    public class CreateEntMsgUpdateGPSRuleConfigurationRequest_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.GPSDisappearTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateLED, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateLED { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateVibration, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateVibration { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateWarningMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateWarningMessage { get; set; }

        [PropertyTest(EnumPropertyName.IsImmediateUpload, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsImmediateUpload { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.RuleID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RuleID { get; set; }

        [PropertyTest(EnumPropertyName.WarningMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string WarningMessage { get; set; }

        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int GeneratedGraceTime { get; set; }

        protected override void ExecuteBlock()
        {
            
            UpdateGPSRuleConfigurationRequest = new Configuration1.EntMsgUpdateGPSRuleConfigurationRequest();
            UpdateGPSRuleConfigurationRequest.GraceTime = GraceTime;
            UpdateGPSRuleConfigurationRequest.IsGenerateLED = IsGenerateLED;
            UpdateGPSRuleConfigurationRequest.IsGenerateVibration = IsGenerateVibration;
            UpdateGPSRuleConfigurationRequest.IsGenerateWarningMessage = IsGenerateWarningMessage;
            UpdateGPSRuleConfigurationRequest.IsImmediateUpload = IsImmediateUpload;
            UpdateGPSRuleConfigurationRequest.OffenderID = OffenderID;
            UpdateGPSRuleConfigurationRequest.RuleID = RuleID;
            UpdateGPSRuleConfigurationRequest.WarningMessage = WarningMessage;

        }
    }

    public class CreateEntMsgUpdateGPSRuleConfigurationRequest_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.GPSDisappearTime, EnumPropertyModifier.Mandatory)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateLED, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateLED { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateVibration, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateVibration { get; set; }

        [PropertyTest(EnumPropertyName.IsGenerateWarningMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsGenerateWarningMessage { get; set; }

        [PropertyTest(EnumPropertyName.IsImmediateUpload, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsImmediateUpload { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.RuleID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RuleID { get; set; }

        [PropertyTest(EnumPropertyName.WarningMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string WarningMessage { get; set; }

        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public int GeneratedGraceTime { get; set; }

        public CreateEntMsgUpdateGPSRuleConfigurationRequest_2()
        {
            GraceTime = -1;
        }

        protected override void ExecuteBlock()
        {
            var responseGraceTime = GetGPSRuleConfigurationListResponse.RuleConfigList.First(x => x.ID == RuleID).GraceTime;

            if (GraceTime == -1)
            {
                int graceTime = (int)60 * DataGeneratorFactory.Instance.RNG.Next(1, 1440);
                GraceTime = graceTime;
                if (responseGraceTime == GraceTime)
                {
                    GraceTime = responseGraceTime + 60;
                }
            }
            UpdateGPSRuleConfigurationRequest = new Configuration2.EntMsgUpdateGPSRuleConfigurationRequest();
            UpdateGPSRuleConfigurationRequest.GraceTime = GraceTime;
            UpdateGPSRuleConfigurationRequest.IsGenerateLED = IsGenerateLED;
            UpdateGPSRuleConfigurationRequest.IsGenerateVibration = IsGenerateVibration;
            UpdateGPSRuleConfigurationRequest.IsGenerateWarningMessage = IsGenerateWarningMessage;
            UpdateGPSRuleConfigurationRequest.IsImmediateUpload = IsImmediateUpload;
            UpdateGPSRuleConfigurationRequest.OffenderID = OffenderID;
            UpdateGPSRuleConfigurationRequest.RuleID = RuleID;
            UpdateGPSRuleConfigurationRequest.WarningMessage = WarningMessage;

        }
    }
}
