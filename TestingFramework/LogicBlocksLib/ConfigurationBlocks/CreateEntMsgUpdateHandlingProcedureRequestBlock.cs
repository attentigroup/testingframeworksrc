﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgUpdateHandlingProcedureRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AutoEmailNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoEmailNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoEmailNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoEmailNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.AutoFaxNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoFaxNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoFaxNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool  AutoFaxNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.AutoPagerNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoPagerNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoPagerNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoPagerNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.Comments, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comments { get; set; }

        [PropertyTest(EnumPropertyName.InheritAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool InheritAgency { get; set; }

        [PropertyTest(EnumPropertyName.InheritOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool InheritOfficer { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ViolationHandling, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ViolationHandling { get; set; }



        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }


        protected override void ExecuteBlock()
        {

            AutoEmailNotificationToAgency = !GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToAgency;
            AutoEmailNotificationToOfficer = !GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToOfficer;
            AutoFaxNotificationToAgency = !GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToAgency;
            AutoFaxNotificationToOfficer = !GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToOfficer;
            AutoPagerNotificationToAgency = !GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToAgency;
            AutoPagerNotificationToOfficer = !GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToOfficer;
            Comments = "This is Automation Change Test";
            InheritAgency = !GetHandlingProcedureResponse.HandlingProcedure.InheritAgency;
            InheritOfficer = !GetHandlingProcedureResponse.HandlingProcedure.InheritOfficer;
            ViolationHandling = "The offender is in violation!!!";

            UpdateHandlingProcedureRequest = new Configuration0.EntMsgUpdateHandlingProcedureRequest();
            UpdateHandlingProcedureRequest.AutoEmailNotificationToAgency = AutoEmailNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoEmailNotificationToOfficer = AutoEmailNotificationToOfficer;
            UpdateHandlingProcedureRequest.AutoFaxNotificationToAgency = AutoFaxNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoFaxNotificationToOfficer = AutoFaxNotificationToOfficer;
            UpdateHandlingProcedureRequest.AutoPagerNotificationToAgency = AutoPagerNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoPagerNotificationToOfficer = AutoPagerNotificationToOfficer;
            UpdateHandlingProcedureRequest.Comments = Comments;
            UpdateHandlingProcedureRequest.InheritAgency = InheritAgency;
            UpdateHandlingProcedureRequest.InheritOfficer = InheritOfficer;
            UpdateHandlingProcedureRequest.OffenderID = OffenderID;
            UpdateHandlingProcedureRequest.ViolationHandling = ViolationHandling;
        }
    }

    public class CreateEntMsgUpdateHandlingProcedureRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AutoEmailNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoEmailNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoEmailNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoEmailNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.AutoFaxNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoFaxNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoPagerNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoPagerNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoPagerNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoPagerNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.Comments, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comments { get; set; }

        [PropertyTest(EnumPropertyName.InheritAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool InheritAgency { get; set; }

        [PropertyTest(EnumPropertyName.InheritOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool InheritOfficer { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ViolationHandling, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ViolationHandling { get; set; }

        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            UpdateHandlingProcedureRequest = new Configuration1.EntMsgUpdateHandlingProcedureRequest();
            UpdateHandlingProcedureRequest.AutoEmailNotificationToAgency = AutoEmailNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoEmailNotificationToOfficer = AutoEmailNotificationToOfficer;
            UpdateHandlingProcedureRequest.AutoFaxNotificationToAgency = AutoFaxNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoFaxNotificationToOfficer = AutoPagerNotificationToOfficer;
            UpdateHandlingProcedureRequest.AutoPagerNotificationToAgency = AutoPagerNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoPagerNotificationToOfficer = AutoPagerNotificationToOfficer;
            UpdateHandlingProcedureRequest.Comments = Comments;
            UpdateHandlingProcedureRequest.InheritAgency = InheritAgency;
            UpdateHandlingProcedureRequest.InheritOfficer = InheritOfficer;
            UpdateHandlingProcedureRequest.OffenderID = OffenderID;
            UpdateHandlingProcedureRequest.ViolationHandling = ViolationHandling;
        }
    }

    public class CreateEntMsgUpdateHandlingProcedureRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.AutoEmailNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoEmailNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoEmailNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoEmailNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.AutoFaxNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoFaxNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoPagerNotificationToAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoPagerNotificationToAgency { get; set; }

        [PropertyTest(EnumPropertyName.AutoPagerNotificationToOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool AutoPagerNotificationToOfficer { get; set; }

        [PropertyTest(EnumPropertyName.Comments, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Comments { get; set; }

        [PropertyTest(EnumPropertyName.InheritAgency, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool InheritAgency { get; set; }

        [PropertyTest(EnumPropertyName.InheritOfficer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool InheritOfficer { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ViolationHandling, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ViolationHandling { get; set; }

        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            UpdateHandlingProcedureRequest = new Configuration2.EntMsgUpdateHandlingProcedureRequest();
            UpdateHandlingProcedureRequest.AutoEmailNotificationToAgency = AutoEmailNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoEmailNotificationToOfficer = AutoEmailNotificationToOfficer;
            UpdateHandlingProcedureRequest.AutoFaxNotificationToAgency = AutoFaxNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoFaxNotificationToOfficer = AutoPagerNotificationToOfficer;
            UpdateHandlingProcedureRequest.AutoPagerNotificationToAgency = AutoPagerNotificationToAgency;
            UpdateHandlingProcedureRequest.AutoPagerNotificationToOfficer = AutoPagerNotificationToOfficer;
            UpdateHandlingProcedureRequest.Comments = Comments;
            UpdateHandlingProcedureRequest.InheritAgency = InheritAgency;
            UpdateHandlingProcedureRequest.InheritOfficer = InheritOfficer;
            UpdateHandlingProcedureRequest.OffenderID = OffenderID;
            UpdateHandlingProcedureRequest.ViolationHandling = ViolationHandling;
        }
    }
}
