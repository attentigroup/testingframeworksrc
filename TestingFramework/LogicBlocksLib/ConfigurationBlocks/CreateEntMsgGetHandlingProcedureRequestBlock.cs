﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;

#endregion

namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgGetHandlingProcedureRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetHandlingProcedureRequest GetHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingProcedureRequest = new Configuration0.EntMsgGetHandlingProcedureRequest();
            GetHandlingProcedureRequest.OffenderID = OffenderID;
        }
    }

    public class CreateEntMsgGetHandlingProcedureRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetHandlingProcedureRequest GetHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingProcedureRequest = new Configuration1.EntMsgGetHandlingProcedureRequest();
            GetHandlingProcedureRequest.OffenderID = OffenderID;
        }
    }

    public class CreateEntMsgGetHandlingProcedureRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetHandlingProcedureRequest GetHandlingProcedureRequest { get; set; }
        protected override void ExecuteBlock()
        {
            GetHandlingProcedureRequest = new Configuration2.EntMsgGetHandlingProcedureRequest();
            GetHandlingProcedureRequest.OffenderID = OffenderID;
        }
    }
}
