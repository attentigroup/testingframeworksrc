﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
#endregion

namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BaseUnitActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool BaseUnitActive { get; set; }

        [PropertyTest(EnumPropertyName.BaseUnitRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EnmRFRange BaseUnitRange { get; set; }

        [PropertyTest(EnumPropertyName.DefaultMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DefaultMessage { get; set; }

        [PropertyTest(EnumPropertyName.DetectOtherTransmitters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool DetectOtherTransmitters { get; set; }

        [PropertyTest(EnumPropertyName.DisplayOffenderName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool DisplayOffenderName { get; set; }

        [PropertyTest(EnumPropertyName.GPSDisappearTime, EnumPropertyType.GPSDisappearTime, EnumPropertyModifier.Mandatory)]
        public int GPSDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.GPSProximity, EnumPropertyType.GPSProximity, EnumPropertyModifier.Mandatory)]
        public int GPSProximity { get; set; }

        [PropertyTest(EnumPropertyName.GPSProximityBuffer, EnumPropertyType.GPSProximity, EnumPropertyModifier.Mandatory)]
        public int GPSProximityBuffer { get; set; }

        [PropertyTest(EnumPropertyName.LandlineActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LandlineActive { get; set; }

        [PropertyTest(EnumPropertyName.LandlineCallerIDRequired, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LandlineCallerIDRequired { get; set; }

        [PropertyTest(EnumPropertyName.LBSActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LBSActive { get; set; }

        [PropertyTest(EnumPropertyName.LBSForViolations, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LBSForViolations { get; set; }

        [PropertyTest(EnumPropertyName.LBSNoPositionTimeout, EnumPropertyType.LBSNoPositionTimeout, EnumPropertyModifier.Mandatory)]
        public int LBSNoPositionTimeout { get; set; }

        [PropertyTest(EnumPropertyName.LBSRequestTimeout, EnumPropertyType.LBSRequestTimeout, EnumPropertyModifier.Mandatory)]
        public int LBSRequestTimeout { get; set; }

        [PropertyTest(EnumPropertyName.LBSSamplingRate, EnumPropertyType.LBSRequestTimeout, EnumPropertyModifier.Mandatory)]
        public int LBSSamplingRate { get; set; }

        [PropertyTest(EnumPropertyName.NormalLoggingRate, EnumPropertyType.NormalLoggingRate, EnumPropertyModifier.Mandatory)]
        public int NormalLoggingRate { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PassiveMode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool PassiveMode { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EnmRFRange ReceiverRange { get; set; }

        [PropertyTest(EnumPropertyName.ReceptionIconsActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReceptionIconsActive { get; set; }

        [PropertyTest(EnumPropertyName.ScheduleActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ScheduleActive { get; set; }

        [PropertyTest(EnumPropertyName.SupportCSD, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportCSD { get; set; }

        [PropertyTest(EnumPropertyName.SupportGPRS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportGPRS { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenBaseUnitUploads, EnumPropertyType.TimeBetweenBaseUnitUploads, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenBaseUnitUploads { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenUploads, EnumPropertyType.TimeBetweenUploads, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenUploads { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterDisappearTime, EnumPropertyType.TransmitterDisappearTime, EnumPropertyModifier.Mandatory)]
        public int TransmitterDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.ViolationLoggingRate, EnumPropertyType.ViolationLoggingRate, EnumPropertyModifier.Mandatory)]
        public int ViolationLoggingRate { get; set; }

        [PropertyTest(EnumPropertyName.RFProximity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string RFProximity { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }


        protected override void ExecuteBlock()
        {

            var getOffenderConfigurationDataResponse = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration0.EntConfigurationGPS;
            var baseUnitActive = getOffenderConfigurationDataResponse.BaseUnitActive;
            var detectOtherTransmitters = getOffenderConfigurationDataResponse.DetectOtherTransmitters;
            var displayOffenderName = getOffenderConfigurationDataResponse.DisplayOffenderName;
            var lBSActive = getOffenderConfigurationDataResponse.LBSActive;
            var lBSForViolations = getOffenderConfigurationDataResponse.LBSForViolations;
            var landlineActive = getOffenderConfigurationDataResponse.LandlineActive;
            var landlineCallerIDRequired = getOffenderConfigurationDataResponse.LandlineCallerIDRequired;
            var passiveMode = getOffenderConfigurationDataResponse.PassiveMode;
            var receptionIconsActive = getOffenderConfigurationDataResponse.ReceptionIconsActive;
            var scheduleActive = getOffenderConfigurationDataResponse.ScheduleActive;
            var supportCSD = getOffenderConfigurationDataResponse.SupportCSD;
            var supportGPRS = getOffenderConfigurationDataResponse.SupportGPRS;


            BaseUnitActive = !(bool)baseUnitActive;
            DetectOtherTransmitters = !(bool)detectOtherTransmitters;
            DisplayOffenderName = !(bool)displayOffenderName;
            LBSActive = !(bool)lBSActive;
            LBSForViolations = !(bool)lBSForViolations;
            LandlineActive = !(bool)landlineActive;
            LandlineCallerIDRequired = !(bool)landlineCallerIDRequired;
            PassiveMode = !(bool)passiveMode;
            ReceptionIconsActive = !(bool)receptionIconsActive;
            ScheduleActive = !(bool)scheduleActive;
            DefaultMessage = "configuration Test Running";
            SupportGPRS = true;

            UpdateGPSOffenderConfigurationDataRequest = new Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest();
            UpdateGPSOffenderConfigurationDataRequest.BaseUnitActive = BaseUnitActive;
            UpdateGPSOffenderConfigurationDataRequest.BaseUnitRange = BaseUnitRange;
            UpdateGPSOffenderConfigurationDataRequest.DefaultMessage = DefaultMessage;
            UpdateGPSOffenderConfigurationDataRequest.DetectOtherTransmitters = DetectOtherTransmitters;
            UpdateGPSOffenderConfigurationDataRequest.DisplayOffenderName = DisplayOffenderName;
            UpdateGPSOffenderConfigurationDataRequest.GPSDisappearTime = GPSDisappearTime;
            UpdateGPSOffenderConfigurationDataRequest.GPSProximity = GPSProximity;
            UpdateGPSOffenderConfigurationDataRequest.GPSProximityBuffer = GPSProximityBuffer;
            UpdateGPSOffenderConfigurationDataRequest.LandlineActive = LandlineActive;
            UpdateGPSOffenderConfigurationDataRequest.LandlineCallerIDRequired = LandlineCallerIDRequired;
            UpdateGPSOffenderConfigurationDataRequest.LBSActive = LBSActive;
            UpdateGPSOffenderConfigurationDataRequest.LBSForViolations = LBSForViolations;
            UpdateGPSOffenderConfigurationDataRequest.LBSNoPositionTimeout = LBSNoPositionTimeout;
            UpdateGPSOffenderConfigurationDataRequest.LBSRequestTimeout = LBSRequestTimeout;
            UpdateGPSOffenderConfigurationDataRequest.LBSSamplingRate = LBSSamplingRate;
            UpdateGPSOffenderConfigurationDataRequest.NormalLoggingRate = NormalLoggingRate;
            UpdateGPSOffenderConfigurationDataRequest.OffenderID = OffenderID;
            UpdateGPSOffenderConfigurationDataRequest.PassiveMode = PassiveMode;
            UpdateGPSOffenderConfigurationDataRequest.ReceiverRange = ReceiverRange;
            UpdateGPSOffenderConfigurationDataRequest.ReceptionIconsActive = ReceptionIconsActive;
            UpdateGPSOffenderConfigurationDataRequest.ScheduleActive = ScheduleActive;
            UpdateGPSOffenderConfigurationDataRequest.SupportCSD = SupportCSD;
            UpdateGPSOffenderConfigurationDataRequest.SupportGPRS = SupportGPRS;
            UpdateGPSOffenderConfigurationDataRequest.TimeBetweenBaseUnitUploads = TimeBetweenBaseUnitUploads;
            UpdateGPSOffenderConfigurationDataRequest.TimeBetweenUploads = TimeBetweenUploads;
            UpdateGPSOffenderConfigurationDataRequest.TransmitterDisappearTime = TransmitterDisappearTime;
            UpdateGPSOffenderConfigurationDataRequest.ViolationLoggingRate = ViolationLoggingRate;
            
        }
    }

    public class CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BaseUnitActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool BaseUnitActive { get; set; }

        [PropertyTest(EnumPropertyName.BaseUnitRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration1.EnmRFRange BaseUnitRange { get; set; }

        [PropertyTest(EnumPropertyName.DefaultMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DefaultMessage { get; set; }

        [PropertyTest(EnumPropertyName.DetectOtherTransmitters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool DetectOtherTransmitters { get; set; }

        [PropertyTest(EnumPropertyName.DisplayOffenderName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool DisplayOffenderName { get; set; }

        [PropertyTest(EnumPropertyName.GPSDisappearTime, EnumPropertyType.GPSDisappearTime, EnumPropertyModifier.Mandatory)]
        public int GPSDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.GPSProximity, EnumPropertyType.GPSProximity, EnumPropertyModifier.Mandatory)]
        public int GPSProximity { get; set; }

        [PropertyTest(EnumPropertyName.GPSProximityBuffer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GPSProximityBuffer { get; set; }

        [PropertyTest(EnumPropertyName.LandlineActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LandlineActive { get; set; }

        [PropertyTest(EnumPropertyName.LandlineCallerIDRequired, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LandlineCallerIDRequired { get; set; }

        [PropertyTest(EnumPropertyName.LBSActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LBSActive { get; set; }

        [PropertyTest(EnumPropertyName.LBSForViolations, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LBSForViolations { get; set; }

        [PropertyTest(EnumPropertyName.LBSNoPositionTimeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int LBSNoPositionTimeout { get; set; }

        [PropertyTest(EnumPropertyName.LBSRequestTimeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int LBSRequestTimeout { get; set; }

        [PropertyTest(EnumPropertyName.LBSSamplingRate, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int LBSSamplingRate { get; set; }

        [PropertyTest(EnumPropertyName.NormalLoggingRate, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int NormalLoggingRate { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PassiveMode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool PassiveMode { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration1.EnmRFRange ReceiverRange { get; set; }

        [PropertyTest(EnumPropertyName.ReceptionIconsActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReceptionIconsActive { get; set; }

        [PropertyTest(EnumPropertyName.ScheduleActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ScheduleActive { get; set; }

        [PropertyTest(EnumPropertyName.SupportCSD, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportCSD { get; set; }

        [PropertyTest(EnumPropertyName.SupportGPRS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportGPRS { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenBaseUnitUploads, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TimeBetweenBaseUnitUploads { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenUploads, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenUploads { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterDisappearTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TransmitterDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.ViolationLoggingRate, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ViolationLoggingRate { get; set; }

        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }
        protected override void ExecuteBlock()
        {
            UpdateGPSOffenderConfigurationDataRequest = new Configuration1.EntMsgUpdateGPSOffenderConfigurationDataRequest();
            UpdateGPSOffenderConfigurationDataRequest.BaseUnitActive = BaseUnitActive;
            UpdateGPSOffenderConfigurationDataRequest.BaseUnitRange = BaseUnitRange;
            UpdateGPSOffenderConfigurationDataRequest.DefaultMessage = DefaultMessage;
            UpdateGPSOffenderConfigurationDataRequest.DetectOtherTransmitters = DetectOtherTransmitters;
            UpdateGPSOffenderConfigurationDataRequest.DisplayOffenderName = DisplayOffenderName;
            UpdateGPSOffenderConfigurationDataRequest.GPSDisappearTime = GPSDisappearTime;
            UpdateGPSOffenderConfigurationDataRequest.GPSProximity = GPSProximity;
            UpdateGPSOffenderConfigurationDataRequest.GPSProximityBuffer = GPSProximityBuffer;
            UpdateGPSOffenderConfigurationDataRequest.LandlineActive = LandlineActive;
            UpdateGPSOffenderConfigurationDataRequest.LandlineCallerIDRequired = LandlineCallerIDRequired;
            UpdateGPSOffenderConfigurationDataRequest.LBSActive = LBSActive;
            UpdateGPSOffenderConfigurationDataRequest.LBSNoPositionTimeout = LBSNoPositionTimeout;
            UpdateGPSOffenderConfigurationDataRequest.LBSRequestTimeout = LBSRequestTimeout;
            UpdateGPSOffenderConfigurationDataRequest.LBSSamplingRate = LBSSamplingRate;
            UpdateGPSOffenderConfigurationDataRequest.NormalLoggingRate = NormalLoggingRate;
            UpdateGPSOffenderConfigurationDataRequest.OffenderID = OffenderID;
            UpdateGPSOffenderConfigurationDataRequest.PassiveMode = PassiveMode;
            UpdateGPSOffenderConfigurationDataRequest.ReceiverRange = ReceiverRange;
            UpdateGPSOffenderConfigurationDataRequest.ReceptionIconsActive = ReceptionIconsActive;
            UpdateGPSOffenderConfigurationDataRequest.ScheduleActive = ScheduleActive;
            UpdateGPSOffenderConfigurationDataRequest.SupportCSD = SupportCSD;
            UpdateGPSOffenderConfigurationDataRequest.SupportGPRS = SupportGPRS;
            UpdateGPSOffenderConfigurationDataRequest.TimeBetweenBaseUnitUploads = TimeBetweenBaseUnitUploads;
            UpdateGPSOffenderConfigurationDataRequest.TimeBetweenUploads = TimeBetweenUploads;
            UpdateGPSOffenderConfigurationDataRequest.TransmitterDisappearTime = TransmitterDisappearTime;
            UpdateGPSOffenderConfigurationDataRequest.ViolationLoggingRate = ViolationLoggingRate;
        }
    }

    public class CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.BaseUnitActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool BaseUnitActive { get; set; }

        [PropertyTest(EnumPropertyName.BaseUnitRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration2.EnmRFRange BaseUnitRange { get; set; }

        [PropertyTest(EnumPropertyName.DefaultMessage, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string DefaultMessage { get; set; }

        [PropertyTest(EnumPropertyName.DetectOtherTransmitters, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool DetectOtherTransmitters { get; set; }

        [PropertyTest(EnumPropertyName.DisplayOffenderName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool DisplayOffenderName { get; set; }

        [PropertyTest(EnumPropertyName.GPSDisappearTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int GPSDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.GPSProximity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GPSProximity { get; set; }

        [PropertyTest(EnumPropertyName.GPSProximityBuffer, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GPSProximityBuffer { get; set; }

        [PropertyTest(EnumPropertyName.LandlineActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LandlineActive { get; set; }

        [PropertyTest(EnumPropertyName.LandlineCallerIDRequired, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LandlineCallerIDRequired { get; set; }

        [PropertyTest(EnumPropertyName.LBSActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LBSActive { get; set; }

        [PropertyTest(EnumPropertyName.LBSForViolations, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool LBSForViolations { get; set; }

        [PropertyTest(EnumPropertyName.LBSNoPositionTimeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int LBSNoPositionTimeout { get; set; }

        [PropertyTest(EnumPropertyName.LBSRequestTimeout, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int LBSRequestTimeout { get; set; }

        [PropertyTest(EnumPropertyName.LBSSamplingRate, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int LBSSamplingRate { get; set; }

        [PropertyTest(EnumPropertyName.NormalLoggingRate, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int NormalLoggingRate { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.PassiveMode, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool PassiveMode { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration2.EnmRFRange ReceiverRange { get; set; }

        [PropertyTest(EnumPropertyName.ReceptionIconsActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReceptionIconsActive { get; set; }

        [PropertyTest(EnumPropertyName.ScheduleActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ScheduleActive { get; set; }

        [PropertyTest(EnumPropertyName.SupportCSD, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportCSD { get; set; }

        [PropertyTest(EnumPropertyName.SupportGPRS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportGPRS { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenBaseUnitUploads, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TimeBetweenBaseUnitUploads { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenUploads, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenUploads { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterDisappearTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int TransmitterDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.ViolationLoggingRate, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ViolationLoggingRate { get; set; }

        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }
        protected override void ExecuteBlock()
        {
            UpdateGPSOffenderConfigurationDataRequest = new Configuration2.EntMsgUpdateGPSOffenderConfigurationDataRequest();
            UpdateGPSOffenderConfigurationDataRequest.BaseUnitActive = BaseUnitActive;
            UpdateGPSOffenderConfigurationDataRequest.BaseUnitRange = BaseUnitRange;
            UpdateGPSOffenderConfigurationDataRequest.DefaultMessage = DefaultMessage;
            UpdateGPSOffenderConfigurationDataRequest.DetectOtherTransmitters = DetectOtherTransmitters;
            UpdateGPSOffenderConfigurationDataRequest.DisplayOffenderName = DisplayOffenderName;
            UpdateGPSOffenderConfigurationDataRequest.GPSDisappearTime = GPSDisappearTime;
            UpdateGPSOffenderConfigurationDataRequest.GPSProximity = GPSProximity;
            UpdateGPSOffenderConfigurationDataRequest.GPSProximityBuffer = GPSProximityBuffer;
            UpdateGPSOffenderConfigurationDataRequest.LandlineActive = LandlineActive;
            UpdateGPSOffenderConfigurationDataRequest.LandlineCallerIDRequired = LandlineCallerIDRequired;
            UpdateGPSOffenderConfigurationDataRequest.LBSActive = LBSActive;
            UpdateGPSOffenderConfigurationDataRequest.LBSNoPositionTimeout = LBSNoPositionTimeout;
            UpdateGPSOffenderConfigurationDataRequest.LBSRequestTimeout = LBSRequestTimeout;
            UpdateGPSOffenderConfigurationDataRequest.LBSSamplingRate = LBSSamplingRate;
            UpdateGPSOffenderConfigurationDataRequest.NormalLoggingRate = NormalLoggingRate;
            UpdateGPSOffenderConfigurationDataRequest.OffenderID = OffenderID;
            UpdateGPSOffenderConfigurationDataRequest.PassiveMode = PassiveMode;
            UpdateGPSOffenderConfigurationDataRequest.ReceiverRange = ReceiverRange;
            UpdateGPSOffenderConfigurationDataRequest.ReceptionIconsActive = ReceptionIconsActive;
            UpdateGPSOffenderConfigurationDataRequest.ScheduleActive = ScheduleActive;
            UpdateGPSOffenderConfigurationDataRequest.SupportCSD = SupportCSD;
            UpdateGPSOffenderConfigurationDataRequest.SupportGPRS = SupportGPRS;
            UpdateGPSOffenderConfigurationDataRequest.TimeBetweenBaseUnitUploads = TimeBetweenBaseUnitUploads;
            UpdateGPSOffenderConfigurationDataRequest.TimeBetweenUploads = TimeBetweenUploads;
            UpdateGPSOffenderConfigurationDataRequest.TransmitterDisappearTime = TransmitterDisappearTime;
            UpdateGPSOffenderConfigurationDataRequest.ViolationLoggingRate = ViolationLoggingRate;
        }
    }
}
