﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion
namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgResetGPSRuleConfigurationRequest : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.RuleID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RuleID { get; set; }

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgResetGPSRuleConfigurationRequest ResetGPSRuleConfigurationListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ResetGPSRuleConfigurationListRequest = new Configuration0.EntMsgResetGPSRuleConfigurationRequest();
            ResetGPSRuleConfigurationListRequest.OffenderID = OffenderID;
            ResetGPSRuleConfigurationListRequest.RuleID = RuleID;


        }

    }

    public class CreateEntMsgResetGPSRuleConfigurationRequest_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.RuleID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RuleID { get; set; }

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgResetGPSRuleConfigurationnRequest ResetGPSRuleConfigurationListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ResetGPSRuleConfigurationListRequest = new Configuration1.EntMsgResetGPSRuleConfigurationnRequest();
            ResetGPSRuleConfigurationListRequest.OffenderID = OffenderID;
            ResetGPSRuleConfigurationListRequest.RuleID = RuleID;


        }

    }

    public class CreateEntMsgResetGPSRuleConfigurationRequest_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.RuleID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int RuleID { get; set; }

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgResetGPSRuleConfigurationnRequest ResetGPSRuleConfigurationListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            ResetGPSRuleConfigurationListRequest = new Configuration2.EntMsgResetGPSRuleConfigurationnRequest();
            ResetGPSRuleConfigurationListRequest.OffenderID = OffenderID;
            ResetGPSRuleConfigurationListRequest.RuleID = RuleID;


        }

    }
}
