﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
#endregion

namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgGetOffenderConfigurationDataRequestBlock : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.VersionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int VersionNumber { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderConfigurationDataRequest = new Configuration0.EntMsgGetOffenderConfigurationDataRequest();
            GetOffenderConfigurationDataRequest.OffenderID = OffenderID;
            GetOffenderConfigurationDataRequest.Version = VersionNumber;


        }
    }

    public class CreateEntMsgGetOffenderConfigurationDataRequestBlock_1 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.VersionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int VersionNumber { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderConfigurationDataRequest = new Configuration1.EntMsgGetOffenderConfigurationDataRequest();
            GetOffenderConfigurationDataRequest.OffenderID = OffenderID;
            GetOffenderConfigurationDataRequest.Version = VersionNumber;


        }
    }

    public class CreateEntMsgGetOffenderConfigurationDataRequestBlock_2 : LogicBlockBase
    {
        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.VersionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int VersionNumber { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetOffenderConfigurationDataRequest = new Configuration2.EntMsgGetOffenderConfigurationDataRequest();
            GetOffenderConfigurationDataRequest.OffenderID = OffenderID;
            GetOffenderConfigurationDataRequest.Version = VersionNumber;


        }
    }
}
