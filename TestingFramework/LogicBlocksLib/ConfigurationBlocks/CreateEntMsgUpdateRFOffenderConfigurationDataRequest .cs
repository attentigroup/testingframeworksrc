﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;


#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
#endregion


namespace LogicBlocksLib.ConfigurationBlocks
{
    public class CreateEntMsgUpdateRFOffenderConfigurationDataRequest : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.CSDPriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int CSDPriority { get; set; }

        [PropertyTest(EnumPropertyName.CommunicationMethodForVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EnmCommunicationMethod CommunicationMethodForVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CallerIDRequired, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool CallerIDRequired { get; set; }

        [PropertyTest(EnumPropertyName.GPRSPriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GPRSPriority { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterTimeFrameEnd, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceAfterTimeFrameEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterTimeFrameStart, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceAfterTimeFrameStart { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterVoiceTestEnd, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceAfterVoiceTestEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeTimeFrameEnd, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceBeforeTimeFrameEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeTimeFrameStart, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceBeforeTimeFrameStart { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeVoiceTestStart, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceBeforeVoiceTestStart { get; set; }

        [PropertyTest(EnumPropertyName.HangupOutOfSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool HangupOutOfSchedule { get; set; }

        [PropertyTest(EnumPropertyName.IsKosher, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsKosher { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int LandlinePriority { get; set; }

        [PropertyTest(EnumPropertyName.MinimumTimeBetweenAlcoholTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MinimumTimeBetweenAlcoholTests { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfPhrases, EnumPropertyType.NumberOfPhrases, EnumPropertyModifier.Mandatory)]
        public int NumberOfPhrases { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfRings, EnumPropertyType.NumberOfRings, EnumPropertyModifier.Mandatory)]
        public int NumberOfRings { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }
        
        [PropertyTest(EnumPropertyName.OfficerPassword, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OfficerPassword { get; set; }

        [PropertyTest(EnumPropertyName.OfficerScreenActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OfficerScreenActive { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EnmRFRange ReceiverRange { get; set; }

        [PropertyTest(EnumPropertyName.RingVolume, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RingVolume { get; set; }

        [PropertyTest(EnumPropertyName.SupportCSD, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportCSD { get; set; }

        [PropertyTest(EnumPropertyName.SupportGPRS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportGPRS { get; set; }

        [PropertyTest(EnumPropertyName.SupportLandline, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportLandline { get; set; }

        [PropertyTest(EnumPropertyName.SupportSMSCallback, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportSMSCallback { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenUploads, EnumPropertyType.TimeBetweenBaseUnitUploads, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenUploads { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterDisappearTime, EnumPropertyType.TransmitterDisappearTime, EnumPropertyModifier.Mandatory)]
        public int TransmitterDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.VerifyAfterEnroll, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool VerifyAfterEnroll { get; set; }


        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

       

        protected override void ExecuteBlock()
        {
            
            var configurationRF = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration0.EntConfigurationRF;
            var supportCSD = configurationRF.SupportCSD;
            var supportGPRS = configurationRF.SupportGPRS;
            var supportSMSCallback = configurationRF.SupportSMSCallback;
            var callerIDRequired = configurationRF.CallerIDRequired;
            var verifyAfterEnroll = configurationRF.VerifyAfterEnroll;

            UpdateRFOffenderConfigurationDataRequest = new Configuration0.EntMsgUpdateRFOffenderConfigurationDataRequest();
            UpdateRFOffenderConfigurationDataRequest.CallerIDRequired = CallerIDRequired;
            UpdateRFOffenderConfigurationDataRequest.CommunicationMethodForVoiceTests = CommunicationMethodForVoiceTests;
            UpdateRFOffenderConfigurationDataRequest.CSDPriority = CSDPriority;
            UpdateRFOffenderConfigurationDataRequest.GPRSPriority = GPRSPriority;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd = GraceAfterTimeFrameEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart = GraceAfterTimeFrameStart;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterVoiceTestEnd = GraceAfterVoiceTestEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd = GraceBeforeTimeFrameEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart = GraceBeforeTimeFrameStart;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeVoiceTestStart = GraceBeforeVoiceTestStart;
            UpdateRFOffenderConfigurationDataRequest.HangupOutOfSchedule = HangupOutOfSchedule;
            UpdateRFOffenderConfigurationDataRequest.IsKosher = IsKosher;
            UpdateRFOffenderConfigurationDataRequest.LandlinePriority = LandlinePriority;
            UpdateRFOffenderConfigurationDataRequest.MinimumTimeBetweenAlcoholTests = MinimumTimeBetweenAlcoholTests;
            UpdateRFOffenderConfigurationDataRequest.NumberOfPhrases = NumberOfPhrases;
            UpdateRFOffenderConfigurationDataRequest.NumberOfRings = NumberOfRings;
            UpdateRFOffenderConfigurationDataRequest.OffenderID = OffenderID;
            UpdateRFOffenderConfigurationDataRequest.OfficerPassword = OfficerPassword;
            UpdateRFOffenderConfigurationDataRequest.OfficerScreenActive = OfficerScreenActive;
            UpdateRFOffenderConfigurationDataRequest.ReceiverRange = ReceiverRange;
            UpdateRFOffenderConfigurationDataRequest.RingVolume = RingVolume;
            UpdateRFOffenderConfigurationDataRequest.SupportCSD = SupportCSD;
            UpdateRFOffenderConfigurationDataRequest.SupportGPRS = SupportGPRS;
            UpdateRFOffenderConfigurationDataRequest.SupportLandline = SupportLandline;
            UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback = SupportSMSCallback;
            UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads = TimeBetweenUploads;
            UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime = TransmitterDisappearTime;
            UpdateRFOffenderConfigurationDataRequest.VerifyAfterEnroll = VerifyAfterEnroll;


        }
    }
    public class CreateEntMsgUpdateRFOffenderConfigurationDataRequest_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.CSDPriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int CSDPriority { get; set; }

        [PropertyTest(EnumPropertyName.CommunicationMethodForVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration1.EnmCommunicationMethod CommunicationMethodForVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CallerIDRequired, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool CallerIDRequired { get; set; }

        [PropertyTest(EnumPropertyName.GPRSPriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GPRSPriority { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterTimeFrameEnd, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceAfterTimeFrameEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterTimeFrameStart, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceAfterTimeFrameStart { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterVoiceTestEnd, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceAfterVoiceTestEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeTimeFrameEnd, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceBeforeTimeFrameEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeTimeFrameStart, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceBeforeTimeFrameStart { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeVoiceTestStart, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceBeforeVoiceTestStart { get; set; }

        [PropertyTest(EnumPropertyName.HangupOutOfSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool HangupOutOfSchedule { get; set; }

        [PropertyTest(EnumPropertyName.IsKosher, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsKosher { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int LandlinePriority { get; set; }

        [PropertyTest(EnumPropertyName.MinimumTimeBetweenAlcoholTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MinimumTimeBetweenAlcoholTests { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfPhrases, EnumPropertyType.NumberOfPhrases, EnumPropertyModifier.Mandatory)]
        public int NumberOfPhrases { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfRings, EnumPropertyType.NumberOfRings, EnumPropertyModifier.Mandatory)]
        public int NumberOfRings { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.OfficerPassword, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OfficerPassword { get; set; }

        [PropertyTest(EnumPropertyName.OfficerScreenActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OfficerScreenActive { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EnmRFRange ReceiverRange { get; set; }

        [PropertyTest(EnumPropertyName.RingVolume, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RingVolume { get; set; }

        [PropertyTest(EnumPropertyName.SupportCSD, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportCSD { get; set; }

        [PropertyTest(EnumPropertyName.SupportGPRS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportGPRS { get; set; }

        [PropertyTest(EnumPropertyName.SupportLandline, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportLandline { get; set; }

        [PropertyTest(EnumPropertyName.SupportSMSCallback, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportSMSCallback { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenUploads, EnumPropertyType.TimeBetweenBaseUnitUploads, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenUploads { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterDisappearTime, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int TransmitterDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.VerifyAfterEnroll, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool VerifyAfterEnroll { get; set; }


        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration1.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var configurationRF = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration1.EntConfigurationRF;
            var supportCSD = configurationRF.SupportCSD;
            var supportGPRS = configurationRF.SupportGPRS;
            var supportSMSCallback = configurationRF.SupportSMSCallback;
            var callerIDRequired = configurationRF.CallerIDRequired;
            var verifyAfterEnroll = configurationRF.VerifyAfterEnroll;

            UpdateRFOffenderConfigurationDataRequest = new Configuration1.EntMsgUpdateRFOffenderConfigurationDataRequest();
            UpdateRFOffenderConfigurationDataRequest.CallerIDRequired = CallerIDRequired;
            UpdateRFOffenderConfigurationDataRequest.CSDPriority = CSDPriority;
            UpdateRFOffenderConfigurationDataRequest.GPRSPriority = GPRSPriority;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd = GraceAfterTimeFrameEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart = GraceAfterTimeFrameStart;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterVoiceTestEnd = GraceAfterVoiceTestEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd = GraceBeforeTimeFrameEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart = GraceBeforeTimeFrameStart;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeVoiceTestStart = GraceBeforeVoiceTestStart;
            UpdateRFOffenderConfigurationDataRequest.HangupOutOfSchedule = HangupOutOfSchedule;
            UpdateRFOffenderConfigurationDataRequest.IsKosher = IsKosher;
            UpdateRFOffenderConfigurationDataRequest.LandlinePriority = LandlinePriority;
            UpdateRFOffenderConfigurationDataRequest.MinimumTimeBetweenAlcoholTests = MinimumTimeBetweenAlcoholTests;
            UpdateRFOffenderConfigurationDataRequest.NumberOfPhrases = NumberOfPhrases;
            UpdateRFOffenderConfigurationDataRequest.NumberOfRings = NumberOfRings;
            UpdateRFOffenderConfigurationDataRequest.OffenderID = OffenderID;
            UpdateRFOffenderConfigurationDataRequest.OfficerPassword = OfficerPassword;
            UpdateRFOffenderConfigurationDataRequest.OfficerScreenActive = OfficerScreenActive;
            UpdateRFOffenderConfigurationDataRequest.RingVolume = RingVolume;
            UpdateRFOffenderConfigurationDataRequest.SupportCSD = SupportCSD;
            UpdateRFOffenderConfigurationDataRequest.SupportGPRS = SupportGPRS;
            UpdateRFOffenderConfigurationDataRequest.SupportLandline = SupportLandline;
            UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback = SupportSMSCallback;
            UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads = TimeBetweenUploads;
            UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime = TransmitterDisappearTime;
            UpdateRFOffenderConfigurationDataRequest.VerifyAfterEnroll = VerifyAfterEnroll;
        }
    }


    public class CreateEntMsgUpdateRFOffenderConfigurationDataRequest_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.CSDPriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int CSDPriority { get; set; }

        [PropertyTest(EnumPropertyName.CommunicationMethodForVoiceTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration2.EnmCommunicationMethod CommunicationMethodForVoiceTests { get; set; }

        [PropertyTest(EnumPropertyName.CallerIDRequired, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool CallerIDRequired { get; set; }

        [PropertyTest(EnumPropertyName.GPRSPriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GPRSPriority { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterTimeFrameEnd, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceAfterTimeFrameEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterTimeFrameStart, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceAfterTimeFrameStart { get; set; }

        [PropertyTest(EnumPropertyName.GraceAfterVoiceTestEnd, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceAfterVoiceTestEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeTimeFrameEnd, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceBeforeTimeFrameEnd { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeTimeFrameStart, EnumPropertyType.GraceTime, EnumPropertyModifier.Mandatory)]
        public int GraceBeforeTimeFrameStart { get; set; }

        [PropertyTest(EnumPropertyName.GraceBeforeVoiceTestStart, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceBeforeVoiceTestStart { get; set; }

        [PropertyTest(EnumPropertyName.HangupOutOfSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool HangupOutOfSchedule { get; set; }

        [PropertyTest(EnumPropertyName.IsKosher, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsKosher { get; set; }

        [PropertyTest(EnumPropertyName.LandlinePriority, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int LandlinePriority { get; set; }

        [PropertyTest(EnumPropertyName.MinimumTimeBetweenAlcoholTests, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int MinimumTimeBetweenAlcoholTests { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfPhrases, EnumPropertyType.NumberOfPhrases, EnumPropertyModifier.Mandatory)]
        public int NumberOfPhrases { get; set; }

        [PropertyTest(EnumPropertyName.NumberOfRings, EnumPropertyType.NumberOfRings, EnumPropertyModifier.Mandatory)]
        public int NumberOfRings { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.OfficerPassword, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OfficerPassword { get; set; }

        [PropertyTest(EnumPropertyName.OfficerScreenActive, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OfficerScreenActive { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverRange, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration0.EnmRFRange ReceiverRange { get; set; }

        [PropertyTest(EnumPropertyName.RingVolume, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RingVolume { get; set; }

        [PropertyTest(EnumPropertyName.SupportCSD, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportCSD { get; set; }

        [PropertyTest(EnumPropertyName.SupportGPRS, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportGPRS { get; set; }

        [PropertyTest(EnumPropertyName.SupportLandline, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportLandline { get; set; }

        [PropertyTest(EnumPropertyName.SupportSMSCallback, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool SupportSMSCallback { get; set; }

        [PropertyTest(EnumPropertyName.TimeBetweenUploads, EnumPropertyType.TimeBetweenBaseUnitUploads, EnumPropertyModifier.Mandatory)]
        public int TimeBetweenUploads { get; set; }

        [PropertyTest(EnumPropertyName.TransmitterDisappearTime, EnumPropertyType.TransmitterDisappearTime, EnumPropertyModifier.Mandatory)]
        public int TransmitterDisappearTime { get; set; }

        [PropertyTest(EnumPropertyName.VerifyAfterEnroll, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool VerifyAfterEnroll { get; set; }

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }


        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Configuration2.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var configurationRF = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration2.EntConfigurationRF;
            var supportCSD = configurationRF.SupportCSD;
            var supportGPRS = configurationRF.SupportGPRS;
            var supportSMSCallback = configurationRF.SupportSMSCallback;
            var callerIDRequired = configurationRF.CallerIDRequired;
            var verifyAfterEnroll = configurationRF.VerifyAfterEnroll;


            UpdateRFOffenderConfigurationDataRequest = new Configuration2.EntMsgUpdateRFOffenderConfigurationDataRequest();
            UpdateRFOffenderConfigurationDataRequest.CallerIDRequired = CallerIDRequired;
            UpdateRFOffenderConfigurationDataRequest.CSDPriority = CSDPriority;
            UpdateRFOffenderConfigurationDataRequest.GPRSPriority = GPRSPriority;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd = GraceAfterTimeFrameEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart = GraceAfterTimeFrameStart;
            UpdateRFOffenderConfigurationDataRequest.GraceAfterVoiceTestEnd = GraceAfterVoiceTestEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd = GraceBeforeTimeFrameEnd;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart = GraceBeforeTimeFrameStart;
            UpdateRFOffenderConfigurationDataRequest.GraceBeforeVoiceTestStart = GraceBeforeVoiceTestStart;
            UpdateRFOffenderConfigurationDataRequest.HangupOutOfSchedule = HangupOutOfSchedule;
            UpdateRFOffenderConfigurationDataRequest.IsKosher = IsKosher;
            UpdateRFOffenderConfigurationDataRequest.LandlinePriority = LandlinePriority;
            UpdateRFOffenderConfigurationDataRequest.MinimumTimeBetweenAlcoholTests = MinimumTimeBetweenAlcoholTests;
            UpdateRFOffenderConfigurationDataRequest.NumberOfPhrases = NumberOfPhrases;
            UpdateRFOffenderConfigurationDataRequest.NumberOfRings = NumberOfRings;
            UpdateRFOffenderConfigurationDataRequest.OffenderID = OffenderID;
            UpdateRFOffenderConfigurationDataRequest.OfficerPassword = OfficerPassword;
            UpdateRFOffenderConfigurationDataRequest.OfficerScreenActive = OfficerScreenActive;
            UpdateRFOffenderConfigurationDataRequest.RingVolume = RingVolume;
            UpdateRFOffenderConfigurationDataRequest.SupportCSD = SupportCSD;
            UpdateRFOffenderConfigurationDataRequest.SupportGPRS = SupportGPRS;
            UpdateRFOffenderConfigurationDataRequest.SupportLandline = SupportLandline;
            UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback = SupportSMSCallback;
            UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads = TimeBetweenUploads;
            UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime = TransmitterDisappearTime;
            UpdateRFOffenderConfigurationDataRequest.VerifyAfterEnroll = VerifyAfterEnroll;


        }
    }
}
