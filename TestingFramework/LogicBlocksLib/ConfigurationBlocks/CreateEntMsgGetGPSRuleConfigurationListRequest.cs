﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;

#endregion

namespace LogicBlocksLib.ConfigurationBlocks
{
    public  class CreateEntMsgGetGPSRuleConfigurationListRequest : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.VersionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int VersionNumber { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListRequest GetGPSRuleConfigurationListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetGPSRuleConfigurationListRequest = new Configuration0.EntMsgGetGPSRuleConfigurationListRequest();
            GetGPSRuleConfigurationListRequest.OffenderID = OffenderID;
            GetGPSRuleConfigurationListRequest.VersionNumber = VersionNumber;
           

        }
    }

    public class CreateEntMsgGetGPSRuleConfigurationListRequest_1 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.VersionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int VersionNumber { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListRequest GetGPSRuleConfigurationListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetGPSRuleConfigurationListRequest = new Configuration1.EntMsgGetGPSRuleConfigurationListRequest();
            GetGPSRuleConfigurationListRequest.OffenderID = OffenderID;
            GetGPSRuleConfigurationListRequest.VersionNumber = VersionNumber;


        }
    }

    public class CreateEntMsgGetGPSRuleConfigurationListRequest_2 : LogicBlockBase
    {

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.VersionNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int VersionNumber { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListRequest, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListRequest GetGPSRuleConfigurationListRequest { get; set; }

        protected override void ExecuteBlock()
        {
            GetGPSRuleConfigurationListRequest = new Configuration2.EntMsgGetGPSRuleConfigurationListRequest();
            GetGPSRuleConfigurationListRequest.OffenderID = OffenderID;
            GetGPSRuleConfigurationListRequest.VersionNumber = VersionNumber;
            


        }
    }
}
