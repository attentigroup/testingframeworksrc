﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.EventsBlocks;

using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events12_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using System.Configuration;
using Common.Categories;

namespace TestingFramework.IntegrationTests.BI
{
    [Ignore]
    [TestClass]
    public class HandlingEventsTests : BaseUnitTest
    {
        public static int OFFENDERS_COUNT_TO_GET = 20;
        public static string OFFENDERS_COUNT_TO_GET_KEY = "OFFENDERS_COUNT_TO_GET";
        public static int EVENTS_PRECENTAGE_TO_HANDLE = 10;
        public static string EVENTS_PRECENTAGE_TO_HANDLE_KEY = "EVENTS_PRECENTAGE_TO_HANDLE";

        public static int OffendersCount2Get { get; set; }

        public static int EventsPrecentage2Handle { get; set; }

        [ClassInitialize]
        public static void HandlingEventsTestsClassInit( TestContext testContext)
        {
            OffendersCount2Get = OFFENDERS_COUNT_TO_GET;
            var value = ConfigurationManager.AppSettings.Get(OFFENDERS_COUNT_TO_GET_KEY);
            if( value != null )
            {
                OffendersCount2Get = int.Parse(value);
            }
            EventsPrecentage2Handle = EVENTS_PRECENTAGE_TO_HANDLE;
            value = ConfigurationManager.AppSettings.Get(EVENTS_PRECENTAGE_TO_HANDLE_KEY);
            if (value != null)
            {
                EventsPrecentage2Handle = int.Parse(value);
            }
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingNewEvent2Warning()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.New).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x=> x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x=>x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.CreateWarning).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingNewEvent2Remark()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.New).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.CreateRemark).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingNewEvent2SendEmail()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.New).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.SendEmail).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingInProcessEvent2Handle()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.InProcess).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.Handle).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingNewEvent2QuickHandle()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.New).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.QuickHandle).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingInProcessEvent2Warning()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.InProcess).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.CreateWarning).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingInProcessEvent2Remark()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.InProcess).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.CreateRemark).
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.BI_HandlingEvents)]
        [TestMethod]
        public void HandlingInProcessEvent2SendEmail()
        {
            TestingFlow.
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get Offenders by events list").
                    SetProperty(x => x.Limit).WithValue(OffendersCount2Get).
                AddBlock<GetEventsByOffendersIdsTestBlock>().
                SetDescription("Get New events for offenders list").
                    SetProperty(x => x.HandlingStatusType).WithValue(Events12_0.EnmHandlingStatusType.InProcess).
                AddBlock<AddHandlingActionForEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Handling Action For Events").
                    SetProperty(x => x.EventsPercentageToHandle).WithValue(EventsPrecentage2Handle).
                    SetProperty(x => x.HandlingActionType).WithValue(Events12_0.EnmHandlingActionType.SendEmail).
                ExecuteFlow();
        }
    }
}
