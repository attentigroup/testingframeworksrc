﻿using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions.BI;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestingFramework.IntegrationTests.BI
{
    [TestClass]
    [DeploymentItem(BiApiMethodsAndParameters_CsvFileName)]
    public class BiApiSanityTests : BaseUnitTest
    {
        public const string BiApiMethodsAndParameters_CsvFileName = @"DataSource\\BiApiMethodsAndParameters.csv";

        [TestCategory(CategoriesNames.BI_BiApiSanity)]
        [TestMethod]
        public void BiApiSanity_ApiMethodNoParameters()
        {
            TestingFlow.
                AddBlock<ExecuteAllNonParmetersMethodsTestBlock>().
            ExecuteFlow();
        }

        [TestMethod,            
            DeploymentItem(BiApiMethodsAndParameters_CsvFileName)]
        [TestCategory(CategoriesNames.BI_BiApiSanity)]
        public void BiApiSanity_ApiMethodWithParameters()
        {
            TestingFlow.
                AddBlock<ExecuteAllMethodsWithParmetersTestBlock>().
            ExecuteFlow();
        }
    }
}
