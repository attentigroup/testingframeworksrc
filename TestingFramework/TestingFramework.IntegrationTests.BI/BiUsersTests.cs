﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common.Categories;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.Tools;
using TestingFramework.Proxies.EM.Interfaces.Users;
using TestingFramework.Proxies.API.Users;

namespace TestingFramework.IntegrationTests.BI
{
    [TestClass]
    public class BiUsersTests : BaseUnitTest
    {
        


        public const string Users2LoadCsvFileName = @"DataSource\\users2Load.csv";


        [Ignore]
        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\"+ Users2LoadCsvFileName, "users2Load#csv", DataAccessMethod.Sequential), 
            DeploymentItem(Users2LoadCsvFileName)]
        [TestCategory(CategoriesNames.BI_UsersTests)]
        public void LoadUsers2AD()
        {
            var username = TestContext.DataRow["user"].ToString();
            var password = TestContext.DataRow["password"].ToString();
            var isEnabled = false;
            bool.TryParse(TestContext.DataRow["isEnabled"].ToString(), out isEnabled);

            var adh = new ActiveDirectiryHelper();
            adh.AddUser(username, password, isEnabled);
        }

        [Ignore]
        [TestMethod]
        public void AddUpdateUser()
        {
            EntMsgGetUsersListRequest entMsgGetUsersListRequest = new EntMsgGetUsersListRequest();
            EntMsgGetUsersListRequest entMsgGetUsersListRequestAfterAdd = new EntMsgGetUsersListRequest();

            entMsgGetUsersListRequest.UserName = "bla";
            entMsgGetUsersListRequest.IncludeDeletedUsers = true;
            var getUsersResponse = UsersProxy.Instance.GetUsersList(entMsgGetUsersListRequest);

            var theUser = new EntUser();
            if ( getUsersResponse.UsersList.Length == 0 )
            {//need to add the user
                EntMsgAddUserRequest entMsgAddUserRequest = new EntMsgAddUserRequest();
                entMsgAddUserRequest.NewUser = new EntUser();
                
                entMsgGetUsersListRequestAfterAdd.UserID = UsersProxy.Instance.AddUser(entMsgAddUserRequest);                
                getUsersResponse = UsersProxy.Instance.GetUsersList(entMsgGetUsersListRequestAfterAdd);

                theUser = getUsersResponse.UsersList[0];//only 1 expected
            }
            else
            {
                theUser = getUsersResponse.UsersList[0];
            }
            //activate the user
            theUser.UserStatus = Proxies.EM.Interfaces.Users.EnmUserStatus.Active;
            EntMsgUpdateUserRequest entMsgUpdateUserRequest = new EntMsgUpdateUserRequest() { UpdatedUser = theUser };
            UsersProxy.Instance.UpdateUser(entMsgUpdateUserRequest);
        }
    }
}
