﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using log4net.Repository.Hierarchy;
using System.IO;
using System.Reflection;

namespace AssertBlocksLib.MaskingPrivateData
{
    public class ValidateMaskingAssertBlock : AssertBaseBlock
    {
        public static string PROPERTY_TYPE_PICTURE = "Picture";
        public static string PROPERTY_TYPE_DATE_OF_BIRTH = "DateOfBirth";


        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public object Object { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Message { get; set; }


        private Dictionary<string, List<string>> dicType2Properties = null;

        public ValidateMaskingAssertBlock()
        {

        }

        protected override void ExecuteBlock()
        {
            dicType2Properties = new Dictionary<string, List<string>>();

            var dir = AppDomain.CurrentDomain.BaseDirectory;
            string path = Path.Combine(dir, @"MaskingPrivateData", @"ObjectsAndProperties_1.csv");
            
            var reader = new StreamReader(/*path*/@"C:\Users\nbraun\Documents\ObjectsAndProperties_1.csv");
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                var objectName = values[0];
                var property = values[1];
                if (!dicType2Properties.ContainsKey(objectName))
                    dicType2Properties.Add(objectName, new List<string>());
                dicType2Properties[objectName].Add(property);
            }

                var rootObjType = Object.GetType().Name;

                var results = AnalyzeType(Object, dicType2Properties[Object.GetType().Name]);

                var failedList = new List<string>();

                foreach (var kvp in results)
                {
                    if (kvp.Key == PROPERTY_TYPE_PICTURE || kvp.Key == PROPERTY_TYPE_DATE_OF_BIRTH)
                    {
                        foreach (var val in kvp.Value)
                        {
                            if (val != null)
                            {
                                failedList.Add(kvp.Key);
                            }
                        }
                        
                    }
                    else
                    {
                        foreach (var val in kvp.Value)
                        {
                            if (val.ToString().Equals("******") == false)
                            {
                                failedList.Add(kvp.Key);
                            }
                        }

                    }

                }

                if (failedList.Any())
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var failedItem in failedList)
                    {
                        sb.Append($"{failedItem}, ");
                    }
                    Assert.Fail($"The following properties are not masked: {sb.ToString()}");
                }

            }

            private Dictionary<string, List<object>> AnalyzeType(object obj, List<string> properties)
            {
                var tmpDic = new Dictionary<string, List<object>>();
                foreach (var rootProp in properties)
                {
                    object prop = obj.GetType().GetProperty(rootProp);
                    if (prop != null)
                    {
                        object propValue = obj.GetType().GetProperty(rootProp).GetValue(obj);
                        if (propValue != null)
                        {
                            if (propValue is string)
                            {
                                if (tmpDic.ContainsKey(rootProp) == false)
                                {
                                    tmpDic.Add(rootProp, new List<object>());
                                }
                                tmpDic[rootProp].Add(propValue);
                            }
                            else if (propValue is byte[])
                            {
                                if (tmpDic.ContainsKey(rootProp) == false)
                                {
                                    tmpDic.Add(rootProp, new List<object>());
                                }
                                tmpDic[rootProp].Add(null);
                            }
                            else if(propValue is IEnumerable<object>)
                            {
                                var propArr = propValue as IEnumerable<object>;
                                if (((IEnumerable<object>)propValue).Count<object>() > 0)
                                {
                                    foreach (var arrItem in propArr)
                                    {
                                        var innerPropDic = AnalyzeType(arrItem, dicType2Properties[arrItem.GetType().Name]);
                                        foreach (var kvp in innerPropDic)
                                        {
                                            if (tmpDic.ContainsKey(kvp.Key) == false)
                                            {
                                                tmpDic.Add(kvp.Key, new List<object>());
                                            }
                                            tmpDic[kvp.Key].AddRange(kvp.Value);
                                        }
                                    }
                                }
                            }
                            else if (propValue is Array)
                            {
                                var propArr = propValue as Array;
                                if (propArr.Length > 0)
                                {
                                    foreach (var arrItem in propArr)
                                    {
                                        var innerPropDic = AnalyzeType(arrItem, dicType2Properties[arrItem.GetType().Name]);
                                        foreach (var kvp in innerPropDic)
                                        {
                                            if (tmpDic.ContainsKey(kvp.Key) == false)
                                            {
                                                tmpDic.Add(kvp.Key, new List<object>());
                                            }
                                            tmpDic[kvp.Key].AddRange(kvp.Value);
                                        }
                                    }
                                }

                            }
                            else//analyze objects
                            {
                                var innerPropDic = AnalyzeType(propValue, dicType2Properties[propValue.GetType().Name]);
                                foreach (var kvp in innerPropDic)
                                {
                                    if (tmpDic.ContainsKey(kvp.Key) == false)
                                    {
                                        tmpDic.Add(kvp.Key, new List<object>());
                                    }
                                    tmpDic[kvp.Key].AddRange(kvp.Value);
                                }
                            }
                        }
                        else
                        {
                        var propInfo = prop as PropertyInfo;
                        var propInfoName = propInfo.PropertyType.Name;
                            if (propInfoName.Equals("Nullable`1"))
                            {
                                if (tmpDic.ContainsKey(rootProp) == false)
                                {
                                    tmpDic.Add(rootProp, new List<object>());
                                }
                                tmpDic[rootProp].Add(null);
                            }
                            else if (propInfoName.Equals("byte[]"))
                            {
                                if (tmpDic.ContainsKey(rootProp) == false)
                                {
                                    tmpDic.Add(rootProp, new List<object>());
                                }
                                tmpDic[rootProp].Add(null);
                            }
                        }
                    }
                    else
                        throw new Exception($"Property: {rootProp} not exist!");

                }
                return tmpDic;
            }


            public void Test()
            {
                //var offender = new EntOffender();
                //offender.FirstName = "Omer";
                //offender.LastName = "Avni";
                //offender.MiddleName = "******";
                //offender.ContactInfo = new EntContactInfo()
                //{
                //    LandLine = new EntLandlineData() { Phone = "972123456789" },
                //    Addresses = new EntOffenderAddress[]
                //        {   new EntOffenderAddress() { CityName = "TEL AVIV" },
                //            new EntOffenderAddress() { CityName = "TEL AVIV" }
                //        }
                //};

                //Object = offender;
                //ExecuteBlock();

                var entOffenderPicture = new EntOffenderPicture() { Picture = new byte[] { 0, 0, 0 } };

                Object = entOffenderPicture;
                ExecuteBlock();

                foreach (var item in entOffenderPicture.Picture)
                {
                    if (item != 0)
                    {

                    }
                }
            }
        }
    }

