﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.TestingFrameworkAsserts
{
    /// <summary>
    /// verify assert block functionality works properly
    /// </summary>
    public class AssertEmptyAssert : AssertBaseBlock
    {
        protected override void ExecuteBlock()
        {            
        }
    }
}
