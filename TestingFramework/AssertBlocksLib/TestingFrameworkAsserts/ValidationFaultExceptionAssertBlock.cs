﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ServiceModel;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Exceptions;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.TestingFrameworkAsserts
{
    /// <summary>
    /// base exception class to use in different validation faults when the exception type is the same as FaultException&lt;ValidationFault&gt;.
    /// but the field and Rule data different
    /// </summary>
    public class ValidationFaultExceptionAssertBlock : BaseExceptionAssertBlock
    {
        /// <summary>
        /// The expected "Field" value 
        /// </summary>
        public string[] Field { get; set; }
        /// <summary>
        /// The expected "Rule" value
        /// </summary>
        public string Rule { get; set; }
        /// <summary>
        /// Default C'tor for ValidationFaultExceptionAssertBlock. It set the ExpectedType property to be typeof(FaultException&lt;ValidationFault&gt;)
        /// </summary>

        public System.ServiceModel.FaultException ServiceException{get;set;}
        
        public ValidationFaultExceptionAssertBlock()
        {
            //ExpectedType = typeof(FaultException<Schedule0.ValidationFault>);
        }

        /// <summary>
        /// C'tor for ValidationFaultExceptionAssertBlock
        /// </summary>
        /// <param name="field">field to be compare when verify the exception</param>
        /// <param name="rule">rule to be compare when verify the exception</param>
        /// <example>
        /// <code>
        ///     public static ValidationFaultExceptionAssertBlock ValidateEquipment_SerialNumber_WS001()
        ///     {
        ///         return new ValidationFaultExceptionAssertBlock("SerialNumber", "WS001");
        ///     }
        ///
        ///     
        ///     TestingFlow.
        ///         ...
        ///         ...   
        ///        AddBlock&lt;ValidationFaultExceptionAssertBlock&gt;(EquipmentAssertsFactory.ValidateEquipment_SerialNumber_WS001()).
        ///    ExecuteFlow();
        /// </code>
        /// </example>

        public ValidationFaultExceptionAssertBlock(string[] field, string rule, Type servicetypetovalidate)
       : this()
        {
            Rule = rule;
            Field = field;
            ExpectedType = servicetypetovalidate;
            
        }

        //public ValidationFaultExceptionAssertBlock(string[] field, string rule)
        //    :this()
        //{
        //    Rule = rule;
        //    Field = field;
        //}
        /// <summary>
        /// override the AssertMessage default that implemented in base class because the assertion is different then default 
        /// </summary>
        //protected virtual void AssertMessage() { }

        //{
        //var blockException = BlockException as FaultException<Schedule0.ValidationFault>;

        //if ( blockException == null)//it might be the inner exception
        //    blockException = BlockException.InnerException as FaultException<Schedule0.ValidationFault>;
        //var validationError = blockException.Detail.Errors[0];
        //Assert.AreEqual(Field.Length, validationError.Field.Length);

        //Array.ForEach(Field, e => Array.Exists(validationError.Field, a => a == e));


        ////validationError.Field.ForEach(Assert.IsTrue(Array.Exists(Field, x => x == errorField)));

        ////foreach (var errorField in validationError.Field)
        ////{
        ////    Assert.IsTrue(Array.Exists(Field, x => x == errorField));
        ////}
        //// Assert.AreEqual(Field, validationError.Field);
        //Assert.AreEqual(Rule, validationError.Rule);
        //}

        protected override void AssertType()
        {
            try
            {
                base.AssertType();
            }
            catch(MismatchExceptionsException mee)
            {//check inner exception
                var innerException = BlockException.InnerException;
                if (innerException == null)
                    throw;
                var innerExceptionType = innerException.GetType();
                if (!(ExpectedType == innerExceptionType))
                {
                    throw new MismatchExceptionsException(this, innerExceptionType, ExpectedType, mee);
                }
            }
            catch (Exception){  throw;  }
        }
    }
}
