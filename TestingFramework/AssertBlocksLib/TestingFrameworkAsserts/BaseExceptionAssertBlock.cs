﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.TestingFrameworkAsserts
{
    public class BaseExceptionAssertBlock : AssertBaseBlock
    {
        /// <summary>
        /// Describes the expected exception type
        /// </summary>
        public Type ExpectedType { get; set; }

        /// <summary>
        /// Describes the expected message for the exception
        /// </summary>
        public Regex ExpectedMessage { get; set; }

        /// <summary>
        /// Describes the message that will appear in the test result log
        /// </summary>
        public string FailMessage { get; set; }

        public BaseExceptionAssertBlock(){}

        public BaseExceptionAssertBlock(Type expectedType, string failMesage, Regex expectedMessage = null)
        {
            ExpectedType = expectedType;
            ExpectedMessage = expectedMessage;
            FailMessage = failMesage;
        }

        protected override void ExecuteBlock()
        {
            if( BlockException != null)
            {
                if( ExpectedType != null)
                {
                    AssertType();
                    AssertMessage();
                }
                else
                {
                    var errorMsg = "The expected type was not set!";
                    throw new BlockRunTimeException(this, errorMsg, BlockException);
                }
            }
            else
            {
                throw new BlockRunTimeException(
                    this,
                    string.Format("Exception from previous block was expected, but no exception was received. ExpectedType={0} ExpectedMessage={1}", 
                        ExpectedType, ExpectedMessage));
            }
        }
        protected virtual void AssertMessage()
        {
            if (ExpectedMessage != null && !ExpectedMessage.IsMatch(BlockException.Message))
            {
                throw new BlockRunTimeException(this, FailMessage, BlockException);
            }
        }

        protected virtual void AssertType()
        {
            Type actual = BlockException.GetType();
            if (!(ExpectedType == actual))
            {
                throw new MismatchExceptionsException(this, actual, ExpectedType);
                //var errorMsg = string.Format("The exception was of type {0} and not type {1}", BlockException.GetType(), ExpectedType);
                //throw new BlockRunTimeException(this, errorMsg, BlockException);
            }
        }
    }
}
