﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Exceptions;

namespace AssertBlocksLib.TestingFrameworkAsserts
{
    /// <summary>
    /// Factory for assertions
    /// </summary>
    /// <example>
    /// <code>
    ///     TestingFlow.
    ///         AddBlock AlwaysFailTestBlock ().
    ///         AddBlock BaseExceptionAssertBlock (TestingFrameworkAssertsFactory.AssertNotImplementedException()).
    ///     ExecuteFlow();
    /// </code>
    /// </example>
    public static class TestingFrameworkAssertsFactory
    {
        /// <summary>
        /// static method to create BaseExceptionAssertBlock instance for First Block Exception
        /// </summary>
        /// <returns></returns>
        public static BaseExceptionAssertBlock AssertFirstBlockException()
        {
            var failMessage = "it is the first block in testing flow";//fix part in the message
            Regex expectedMessage = new Regex("Can't add assert from type * because it is the first block in testing flow");//dynamic part in the message
            return new BaseExceptionAssertBlock(typeof(AssertFirstBlockException), failMessage, expectedMessage);
        }
        /// <summary>
        /// static method to create BaseExceptionAssertBlock instance for Not Implemented Exception
        /// </summary>
        /// <returns></returns>
        public static BaseExceptionAssertBlock AssertNotImplementedException()
        {
            var failMessage = string.Empty;
            return new BaseExceptionAssertBlock(typeof(NotImplementedException), failMessage);
        }
    }
}
