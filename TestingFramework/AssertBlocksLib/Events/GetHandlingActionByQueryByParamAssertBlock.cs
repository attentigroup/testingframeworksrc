﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.Events
{
    public class GetHandlingActionByQueryByParamAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.HandlingActionType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmHandlingActionType ActionType { get; set; }

        [PropertyTest(EnumPropertyName.ContainsActionType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ContainsActionType { get; set; }

        [PropertyTest(EnumPropertyName.ActionFromTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ActionFromTime { get; set; }

        [PropertyTest(EnumPropertyName.ActionToTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? ActionToTime { get; set; }

        //[PropertyTest(EnumPropertyName.ActionTypeList, EnumPropertyType.None, EnumPropertyModifier.None)]
        //public Events0.EnmHandlingActionType[] ActionTypeList { get; set; }

        public GetHandlingActionByQueryByParamAssertBlock()
        {
            ContainsActionType = false;
        }

        protected override void ExecuteBlock()
        {
            if(EventID != 0)
            {
                foreach (var action in GetHandlingActionsByQueryResponse.ActionsList)
                {
                    Assert.AreEqual(EventID, action.EventID, $"Expected event ID: {EventID}, Actual event ID: {action.EventID}");
                }
            }

            if (ContainsActionType)
            {
                foreach (var action in GetHandlingActionsByQueryResponse.ActionsList)
                {
                    Assert.AreEqual(ActionType, action.Type, $"Expected Action Type: {ActionType}, Actual Action Type: {action.Type}");
                }
            }

            if(StartTime!=null && EndTime != null)
            {
                foreach (var action in GetHandlingActionsByQueryResponse.ActionsList)
                {
                    Assert.IsTrue(action.EventTime >= StartTime, $"Event ID: { action.EventID }Event time: {action.EventTime} smaller than start time: {StartTime}");
                    Assert.IsTrue(action.EventTime <= EndTime, $"Event ID: { action.EventID }Event time: {action.EventTime} greater than end time: {EndTime}");
                }
            }

            if (ActionFromTime != null && ActionToTime != null)
            {
                foreach (var action in GetHandlingActionsByQueryResponse.ActionsList)
                {
                    Assert.IsTrue(action.Time >= ActionFromTime,$"Event ID: { action.EventID} Event time: {action.EventTime} smaller than action from time: {ActionFromTime}");
                    Assert.IsTrue(action.Time <= ActionToTime, $"Event ID: { action.EventID} Event time: {action.EventTime} greater than action to time: {ActionToTime}");
                }
            }




        }
    }
}
