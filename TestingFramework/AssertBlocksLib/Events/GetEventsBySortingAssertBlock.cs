﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.Events
{
    public class GetEventsBySortingAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmEventsSortOptions SortField { get; set; }
        protected override void ExecuteBlock()
        {
            Events0.EntEvent[] events = new Events0.EntEvent[GetEventsResponse.EventsList.Length];
            switch (SortField)
            {
                case Events0.EnmEventsSortOptions.EventID:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.ID).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.ID).ToArray();
                    break;
                case Events0.EnmEventsSortOptions.Severity:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.Severity).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.Severity).ToArray();
                    break;
                case Events0.EnmEventsSortOptions.Status:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.Status).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.Status).ToArray();
                    break;
                case Events0.EnmEventsSortOptions.Message:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.Message).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.Message).ToArray();
                    break;
                case Events0.EnmEventsSortOptions.EventTime:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.Time).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.Time).ToArray();
                    break;
                case Events0.EnmEventsSortOptions.EventCode:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.Code).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.Code).ToArray();
                    break;
                case Events0.EnmEventsSortOptions.Timestamp:
                    if (SortDirection == ListSortDirection.Ascending)
                        events = GetEventsResponse.EventsList.OrderBy(x => x.Timestamp).ToArray();
                    else
                        events = GetEventsResponse.EventsList.OrderByDescending(x => x.Timestamp).ToArray();
                    break;
                default:
                    break;
            }
            

            for (int i = 0; i < events.Length; i++)
            {
                Assert.AreEqual(GetEventsResponse.EventsList[i].ID, events[i].ID);
            }
        }
    }
}
