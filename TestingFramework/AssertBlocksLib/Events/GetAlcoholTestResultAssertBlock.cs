﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
#endregion

namespace AssertBlocksLib.Events
{
    public class GetAlcoholTestResultAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetAlcoholTestResultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetAlcoholTestResultResponse.TestResult!=null);
        }
    }

    public class GetAlcoholTestResultAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetAlcoholTestResultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetAlcoholTestResultResponse GetAlcoholTestResultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetAlcoholTestResultResponse.TestResult != null);
        }
    }
}
