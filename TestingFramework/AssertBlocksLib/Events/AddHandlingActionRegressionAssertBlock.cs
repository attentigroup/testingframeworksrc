﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.Events
{
    public class AddHandlingActionRegressionAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddHandlingActionRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgAddHandlingActionRequest AddHandlingActionRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var getHandlingAction = GetHandlingActionsByQueryResponse.ActionsList.LastOrDefault();

            switch (AddHandlingActionRequest.Type)
            {
                case Events0.EnmHandlingActionType.QuickHandle:
                    Assert.AreEqual("Event was \"quick\" handled", getHandlingAction.Comment);
                    CheckCommonParameters(getHandlingAction);
                    break; 
                case Events0.EnmHandlingActionType.Unhandled:
                    Assert.AreEqual("Event was \"Un-Handled\"", getHandlingAction.Comment);
                    CheckCommonParameters(getHandlingAction);
                    break;
                case Events0.EnmHandlingActionType.BatchHandling:
                    Assert.AreEqual("Event was \"Batch-Handled\"", getHandlingAction.Comment);
                    CheckCommonParameters(getHandlingAction);
                    break;
                case Events0.EnmHandlingActionType.CustomAction:
                    Assert.IsTrue(getHandlingAction.Comment.Contains(AddHandlingActionRequest.Comment), $"Expected Comment: {AddHandlingActionRequest.Comment}, Actual Comment: {getHandlingAction.Comment}");
                    CheckCustomActionParameters(getHandlingAction);
                    break;
                case Events0.EnmHandlingActionType.Handle:  
                case Events0.EnmHandlingActionType.PhoneCall:   
                case Events0.EnmHandlingActionType.SendFax: 
                case Events0.EnmHandlingActionType.SendTextMessage: 
                case Events0.EnmHandlingActionType.SendEmail:    
                case Events0.EnmHandlingActionType.CreateRemark:    
                case Events0.EnmHandlingActionType.CreateWarning:   
                case Events0.EnmHandlingActionType.ScheduleAlcoholTest: 
                case Events0.EnmHandlingActionType.ScheduleVoiceTest:
                case Events0.EnmHandlingActionType.ClearPanicAlert:
                case Events0.EnmHandlingActionType.ScheduleTask:
                case Events0.EnmHandlingActionType.SendToThirdParty:    
                case Events0.EnmHandlingActionType.ReturnFromThirdParty:
                case Events0.EnmHandlingActionType.ClearTamper:
                    CheckCommonParameters(getHandlingAction);
                    break;
                default:
                    Assert.AreEqual(AddHandlingActionRequest.Comment, getHandlingAction.Comment, $"Expected Comment: {AddHandlingActionRequest.Comment}, Actual Comment: {getHandlingAction.Comment}");
                    break;
            }
 
        }

        private void CheckCommonParameters(Events0.EntHandlingAction getHandlingAction)
        {

           
            Assert.AreEqual(AddHandlingActionRequest.EventID, getHandlingAction.EventID, $"Expected EventID: {AddHandlingActionRequest.EventID}, Actual EventID: {getHandlingAction.EventID}");
            Assert.AreEqual(AddHandlingActionRequest.Type, getHandlingAction.Type, $"Expected Type: {AddHandlingActionRequest.Type}, Actual Type: {getHandlingAction.Type}");
        }

        private void CheckPhoneCallParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            CheckContactParams(getHandlingAction);
            CheckPhoneParams(getHandlingAction);
        }

        private void CheckSendTextMessageParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            CheckContactParams(getHandlingAction);
            Assert.AreEqual(AddHandlingActionRequest.PagerCode.ToString(), getHandlingAction.PagerCode, $"Expected Pager Code: {AddHandlingActionRequest.PagerCode}, Actual Pager Code: {getHandlingAction.PagerCode}");
            Assert.AreEqual(AddHandlingActionRequest.PagerType, getHandlingAction.PagerType, $"Expected Pager Type: {AddHandlingActionRequest.PagerType}, Actual Pager Type: {getHandlingAction.PagerType}");
            Assert.AreEqual(AddHandlingActionRequest.ProviderID, getHandlingAction.ProviderID, $"Expected Provider ID: {AddHandlingActionRequest.ProviderID}, Actual Provider ID: {getHandlingAction.ProviderID}");

        }

        private void CheckSendFaxParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            CheckContactParams(getHandlingAction);
            CheckPhoneParams(getHandlingAction);
        }

        private void CheckSendEmailParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            CheckContactParams(getHandlingAction);
            Assert.AreEqual(AddHandlingActionRequest.EmailAddress, getHandlingAction.EmailAddress, $"Expected Email Address: {AddHandlingActionRequest.EmailAddress}, Actual Email Address: {getHandlingAction.EmailAddress}");
        }

        private void CheckScheduleTaskParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            Assert.AreEqual(AddHandlingActionRequest.ReminderTime, getHandlingAction.ReminderTime, $"Expected Reminder Time: {AddHandlingActionRequest.ReminderTime}, Actual Reminder Time: {getHandlingAction.ReminderTime}");
            Assert.AreEqual(AddHandlingActionRequest.ScheduledTaskSubType, getHandlingAction.ScheduledTaskSubType, $"Expected Scheduled Task Sub Type: {AddHandlingActionRequest.ScheduledTaskSubType}, Actual Scheduled Task Sub Type: {getHandlingAction.ScheduledTaskSubType}");
            Assert.AreEqual(AddHandlingActionRequest.ScheduledTaskType, getHandlingAction.ScheduledTaskType, $"Expected Scheduled Task Type: {AddHandlingActionRequest.ScheduledTaskType}, Actual Scheduled Task Type: {getHandlingAction.ScheduledTaskType}");
        }

        private void CheckCustomActionParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            Assert.AreEqual(AddHandlingActionRequest.UserDefinedActionCode, getHandlingAction.UserDefinedActionCode);
        }

        private void CheckSendToThirdPartyParameters(Events0.EntHandlingAction getHandlingAction)
        {
            CheckCommonParameters(getHandlingAction);
            Assert.AreEqual(AddHandlingActionRequest.ThirdPartyActionCode, getHandlingAction.ThirdPartyActionCode);
        }

        private void CheckPhoneParams(Events0.EntHandlingAction getHandlingAction)
        {
            Assert.AreEqual(AddHandlingActionRequest.Phone, getHandlingAction.Phone);
            Assert.AreEqual(AddHandlingActionRequest.PrefixPhone, getHandlingAction.PrefixPhone);
        }

        private void CheckContactParams(Events0.EntHandlingAction getHandlingAction)
        {
            Assert.AreEqual(AddHandlingActionRequest.ContactID, getHandlingAction.ContactID);
            Assert.AreEqual(AddHandlingActionRequest.ContactType, getHandlingAction.ContactType);
        }
    }
}
