﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.Events
{
    public class GetEventsByParamAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        [PropertyTest(EnumPropertyName.IncludeHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeHistory { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmProgramType? ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.Status, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmHandlingStatusType Status { get; set; }

        [PropertyTest(EnumPropertyName.Severity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EnmEventSeverity? Severity { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EventID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Message { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.EventCodes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] EventCodes { get; set; }

        protected override void ExecuteBlock()
        {
            if (!IncludeHistory)
            {
                foreach(var eve in GetEventsResponse.EventsList)
                {
                    Assert.AreEqual(false, eve.History, $"event history id {eve.ID} ");
                }
            }

            if (Limit != 0)
                Assert.IsTrue(GetEventsResponse.EventsList.Length <= Limit, $"Event list length greater than limit. Limit: {Limit}, Event list length: {GetEventsResponse.EventsList.Length}");

            if (ProgramType != null)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new EntNumericParameter() {Operator = EnmNumericOperator.Equal, Value = eve.OffenderID.Value }
                    });
                    if(getOffender != null && getOffender.OffendersList.Length > 0)
                        Assert.AreEqual(ProgramType.ToString(), getOffender.OffendersList[0].ProgramType.ToString(), $"Expected program type: {ProgramType}, Actual program type: {getOffender.OffendersList[0].ProgramType}");
                }
            }

            if (Status != Events0.EnmHandlingStatusType.New)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    Assert.AreEqual(Status, eve.Status, $"Expected status: {Status}, Actual status: {eve.Status}");
                }
            }

            if (Severity != null)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    Assert.AreEqual(Severity, eve.Severity, $"Expected severity: {Severity}, Actual severity: {eve.Severity}");
                }
            }

            if (AgencyID != 0)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new EntNumericParameter() { Operator = EnmNumericOperator.Equal, Value = eve.OffenderID.Value }
                    });
                    Assert.AreEqual(AgencyID, getOffender.OffendersList[0].AgencyID, $"Expected AgencyID: {AgencyID}, Actual AgencyID: {getOffender.OffendersList[0].AgencyID}");
                }
            }

            if(StartTime!=null && EndTime != null)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    Assert.IsTrue(eve.Time >= StartTime, $"Event time: {eve.Time} smaller than start time: {StartTime}");
                    Assert.IsTrue(eve.Time <= EndTime, $"Event time: {eve.Time} greater than end time: {EndTime}");
                }
            }

            if (EventID != 0)
            {
                Assert.AreEqual(EventID, GetEventsResponse.EventsList[0].ID, $"Expected event ID: {EventID}, Actual event ID: {GetEventsResponse.EventsList[0].ID}");
            }

            if (OfficerID != 0)
            {
                EntMsgGetOffendersResponse getOffender = new EntMsgGetOffendersResponse();
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    try
                    {
                        getOffender = OffendersProxy.Instance.GetOffenders(new EntMsgGetOffendersListRequest()
                        {
                            OffenderID = new EntNumericParameter() { Operator = EnmNumericOperator.Equal, Value = eve.OffenderID.Value }
                        });
                    }
                    catch
                    {
                        throw new Exception($"Failed to GetOffender for offender ID: {eve.OffenderID.Value}");
                    }
                    Assert.AreEqual(OfficerID, getOffender.OffendersList[0].OfficerID, $"Expected officer ID: {OfficerID}, Actual officer ID: {getOffender.OffendersList[0].OfficerID}");
                }
            }

            if (OffenderID != 0)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    Assert.AreEqual(OffenderID, eve.OffenderID, $"Expected offender ID: {OffenderID}, Actual offender ID: {eve.OffenderID}");
                }
            }

            if (!string.IsNullOrEmpty(OffenderRefID))
            {
                EntMsgGetOffendersResponse getOffender = new EntMsgGetOffendersResponse();
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    try
                    {
                        getOffender = OffendersProxy.Instance.GetOffenders(new EntMsgGetOffendersListRequest()
                        {
                            OffenderID = new EntNumericParameter() { Operator = EnmNumericOperator.Equal, Value = eve.OffenderID.Value }
                        });
                    }
                    catch
                    {
                        throw new Exception($"Failed to GetOffender for offender ID: {eve.OffenderID.Value}");
                    }
                    Assert.AreEqual(OffenderRefID, getOffender.OffendersList[0].RefID, $"Expected offender refID: {OffenderRefID}, Actual offender refID: {getOffender.OffendersList[0].RefID}");
                }
            }

            if (!string.IsNullOrEmpty(Message))
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    Assert.AreEqual(Message, eve.Message, $"Expected message: {Message}, Actual message: {eve.Message}");
                }
            }

            if(EventCodes!=null && EventCodes.Length > 0)
            {
                foreach (var eve in GetEventsResponse.EventsList)
                {
                    Assert.IsTrue(EventCodes.Contains(eve.Code), $"Event code: {eve.Code}");
                }
            }
        }
    }
}
