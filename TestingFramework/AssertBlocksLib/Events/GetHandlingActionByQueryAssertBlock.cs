﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace AssertBlocksLib.Events
{
    public class GetHandlingActionByQueryAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var action in GetHandlingActionsByQueryResponse.ActionsList)
            {
                Assert.IsTrue(action.Status == "Done" || action.Status == "Not done", $"Event ID: {action.EventID}, Action ID: {action.ActionID}, Status: {action.Status}");
                if (!(action.OffenderID == null))
                    Assert.AreEqual(action.OffenderID, GetEventsResponse.EventsList[0].OffenderID, $"Offender ID: expected: {GetEventsResponse.EventsList[0].OffenderID}, actual: {action.OffenderID}");
                
            }
        }
    }

    public class GetHandlingActionByQueryAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var action in GetHandlingActionsByQueryResponse.ActionsList)
            {
                Assert.IsTrue(action.Status == "Done" || action.Status == "Not done", $"Event ID: {action.EventID}, Action ID: {action.ActionID}, Status: {action.Status}");
                if (!(action.OffenderID == null))
                    Assert.AreEqual(action.OffenderID, GetEventsResponse.EventsList[0].OffenderID, $"Offender ID: expected: {GetEventsResponse.EventsList[0].OffenderID}, actual: {action.OffenderID}");


            }
        }
    }

    public class GetHandlingActionByQueryAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetHandlingActionsByQueryResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetHandlingActionsByQueryResponse GetHandlingActionsByQueryResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var action in GetHandlingActionsByQueryResponse.ActionsList)
            {
                Assert.IsTrue(action.Status == "Done" || action.Status == "Not done", $"Event ID: {action.EventID}, Action ID: {action.ActionID}, Status: {action.Status}");


            }
        }
    }
}
