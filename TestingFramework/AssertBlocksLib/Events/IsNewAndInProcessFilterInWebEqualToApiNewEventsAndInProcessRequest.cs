﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.Events
{
    public class IsNewAndInProcessFilterInWebEqualToApiNewEventsAndInProcessRequest : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ApiNewFilterCounter{ get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int ApiInProcessFilterCounter { get; set; }
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int WebNewAndInProcessFilterCounter { get; set; }
        protected override void ExecuteBlock()
        {
            throw new NotImplementedException();
        }
    }
}
