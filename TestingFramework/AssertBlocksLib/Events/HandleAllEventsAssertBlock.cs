﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace AssertBlocksLib.Events
{
    public class HandleAllEventsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var offenderEvent in GetEventsResponse.EventsList)
            {
                if (offenderEvent.Status != null)
                {
                    var getHandlingAction = EventsProxy.Instance.GetHandlingActionsByQuery(new Events0.EntMsgGetHandlingActionsByQueryRequest()
                    {
                        EventIDList = new int[] { offenderEvent.ID }
                    });
                    foreach (var action in getHandlingAction.ActionsList)
                    {
                        Assert.IsTrue(action.Status == "Done");

                    }

                    var last = getHandlingAction.ActionsList.Last();
                    Assert.IsTrue(last.Type == Events0.EnmHandlingActionType.BatchHandling);
                    Assert.IsTrue(last.Comment == "Event was \"Batch-Handled\"");
                    //Assert.IsTrue(last.Time.Value.Date == DateTime.Today.Date);

                }

            }
        }
   
    }

    public class HandleAllEventsAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var offenderEvent in GetEventsResponse.EventsList)
            {
                if (offenderEvent.Status != null)
                {
                    var getHandlingAction = EventsProxy_1.Instance.GetHandlingActionsByQuery(new Events1.EntMsgGetHandlingActionsByQueryRequest()
                    {
                        EventIDList = new int[] { offenderEvent.ID }
                    });
                    foreach (var action in getHandlingAction.ActionsList)
                    {
                        Assert.IsTrue(action.Status == "Done");

                    }

                    var last = getHandlingAction.ActionsList.Last();
                    Assert.IsTrue(last.Type == Events1.EnmHandlingActionType.BatchHandling);
                    Assert.IsTrue(last.Comment == "Event was \"Batch-Handled\"");
                    Assert.IsTrue(last.Time.Value.Date == DateTime.Today.Date);

                }

            }
        }

    }

    public class HandleAllEventsAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var offenderEvent in GetEventsResponse.EventsList)
            {
                if (offenderEvent.HandlingStatus != null)
                {
                    var getHandlingAction = EventsProxy_2.Instance.GetHandlingActionsByQuery(new Events2.EntMsgGetHandlingActionsByQueryRequest()
                    {
                        EventIDList = new int[] { offenderEvent.ID }
                    });
                    foreach (var action in getHandlingAction.ActionsList)
                    {
                        Assert.IsTrue(action.Status == "Done");

                    }

                    var last = getHandlingAction.ActionsList.Last();
                    Assert.IsTrue(last.Type == Events2.EnmHandlingActionType.BatchHandling);
                    Assert.IsTrue(last.Comment == "Event was \"Batch-Handled\"");
                    //Assert.IsTrue(last.Time.Value.Date == DateTime.Today.Date);

                }

            }
        }        
       
    }
}
