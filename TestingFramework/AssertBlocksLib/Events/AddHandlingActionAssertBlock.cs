﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace AssertBlocksLib.Events
{
    public class AddHandlingActionAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetEventsResponse.EventsList[0].Status == Events0.EnmHandlingStatusType.Handled);            
        }
    }

    public class AddHandlingActionAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetEventsResponse.EventsList[0].Status == Events1.EnmHandlingStatusType.Handled);
        }
    }

    public class AddHandlingActionAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetEventsResponse.EventsList[0].HandlingStatus == Events2.EnmHandlingStatusType.Handled);
        }
    }
}
