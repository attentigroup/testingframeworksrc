﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace AssertBlocksLib.Events
{
    public class GetEventsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(Events0.EntEvent Event in GetEventsResponse.EventsList)
            {
                Assert.IsTrue(Event.Time >= GetEventsRequest.StartTime);
                Assert.IsTrue(Event.Time <= GetEventsRequest.EndTime);
                Assert.AreEqual(Event.Status, GetEventsRequest.Status);                
            }
        }
    }

    public class GetEventsAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (Events1.EntEvent Event in GetEventsResponse.EventsList)
            {
                Assert.IsTrue(Event.Time >= GetEventsRequest.StartTime);
                Assert.IsTrue(Event.Time <= GetEventsRequest.EndTime);
                Assert.AreEqual(Event.Status, GetEventsRequest.Status);
            }
        }
    }

    public class GetEventsAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetEventsRequest GetEventsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (Events2.EntEvent Event in GetEventsResponse.EventsList)
            {
                Assert.IsTrue(Event.UpdateTime >= GetEventsRequest.FromTime);
                Assert.IsTrue(Event.UpdateTime <= GetEventsRequest.ToTime);
                Assert.AreEqual(Event.HandlingStatus, GetEventsRequest.HandlingStatus);
            }
        }
    }
}
