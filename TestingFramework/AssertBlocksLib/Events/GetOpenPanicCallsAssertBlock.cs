﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Events;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;
#endregion

namespace AssertBlocksLib.Events
{
    public class GetOpenPanicCallsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOpenPanicCallsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetOpenPanicCallsResponse GetOpenPanicCallsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var openPanicCall in GetOpenPanicCallsResponse.PanicCallDataList)
            {
                HandleOpenPanicCall(openPanicCall);

            }
        }

        private void HandleOpenPanicCall(Events0.EntPanicCallData openPanicCall)
        {
            Events0.EntMsgGetEventsResponse getEvents = null;

            if (!(openPanicCall.PanicPressTime == null))
            {
                HandleOpenPanicWithPanicPressTime(openPanicCall, getEvents);
            }
        }

        private void HandleOpenPanicWithPanicPressTime(Events0.EntPanicCallData openPanicCall, Events0.EntMsgGetEventsResponse getEvents)
        {
            getEvents = EventsProxy.Instance.GetEvents(new Events0.EntMsgGetEventsRequest()
            {
                Limit = 100,
                StartTime = openPanicCall.PanicPressTime,
                EndTime = openPanicCall.PanicPressTime,
                OffenderRefID = openPanicCall.RefID,

            });

            foreach (var offenderEvent in getEvents.EventsList)
            {
                Assert.IsTrue(offenderEvent.Message == "Victim Panic Alert", $"Offender ID: {offenderEvent.OffenderID}, Event ID: {offenderEvent.ID}, Message: {offenderEvent.Message}");
                Assert.IsTrue(offenderEvent.Severity == Events0.EnmEventSeverity.Violation, $"Event ID: { offenderEvent.ID}, Severity: {offenderEvent.Severity}");
                //Assert.IsTrue(offenderEvent.Status == Events0.EnmHandlingStatusType.New);
            }

        }
    }

    public class GetOpenPanicCallsAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOpenPanicCallsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events1.EntMsgGetOpenPanicCallsResponse GetOpenPanicCallsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var openPanicCall in GetOpenPanicCallsResponse.PanicCallDataList.Entities)
            {
                HandleOpenPanicCall(openPanicCall);

            }
        }

        private void HandleOpenPanicCall(Events1.EntPanicCallData openPanicCall)
        {
            Events1.EntMsgGetEventsResponse getEvents = null;

            if (!(openPanicCall.PanicPressTime == null))
            {
                HandleOpenPanicWithPanicPressTime(openPanicCall, getEvents);
            }
        }

        private void HandleOpenPanicWithPanicPressTime(Events1.EntPanicCallData openPanicCall, Events1.EntMsgGetEventsResponse getEvents)
        {
            getEvents = EventsProxy_1.Instance.GetEvents(new Events1.EntMsgGetEventsRequest()
            {
                Limit = 100,
                StartTime = openPanicCall.PanicPressTime,
                EndTime = openPanicCall.PanicPressTime,
                OffenderRefID = openPanicCall.RefID,

            });

            foreach (var offenderEvent in getEvents.EventsList)
            {
                Assert.IsTrue(offenderEvent.Message == "Victim Panic Alert");
                Assert.IsTrue(offenderEvent.Severity == Events1.EnmEventSeverity.Violation);
                //Assert.IsTrue(offenderEvent.Status == Events1.EnmHandlingStatusType.New);
            }

        }
    }

    public class GetOpenPanicCallsAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOpenPanicCallsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events2.EntMsgGetOpenPanicCallsResponse GetOpenPanicCallsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var openPanicCall in GetOpenPanicCallsResponse.PanicCallDataList.Entities)
            {
                HandleOpenPanicCall(openPanicCall);

            }
        }

        private void HandleOpenPanicCall(Events2.EntPanicCallData openPanicCall)
        {
            Events2.EntMsgGetEventsResponse getEvents = null;

            if (!(openPanicCall.PanicPressTime == null))
            {
                HandleOpenPanicWithPanicPressTime(openPanicCall, getEvents);
            }
        }

        private void HandleOpenPanicWithPanicPressTime(Events2.EntPanicCallData openPanicCall, Events2.EntMsgGetEventsResponse getEvents)
        {
            getEvents = EventsProxy_2.Instance.GetEvents(new Events2.EntMsgGetEventsRequest()
            {
                MaximumRows = 100,
                FromTime = openPanicCall.PanicPressTime,
                ToTime = openPanicCall.PanicPressTime,
                OffenderRefID = new Events2.EntStringQueryParameter() { Operator = Events2.EnmSqlOperatorStr.Equal, Value = openPanicCall.RefID } 

            });

            foreach (var offenderEvent in getEvents.EventsList)
            {
                Assert.IsTrue(offenderEvent.Message == "Victim Panic Alert");
                Assert.IsTrue(offenderEvent.SeverityID == Events2.EnmEventSeverity.Violation);
                //Assert.IsTrue(offenderEvent.HandlingStatus == Events2.EnmHandlingStatusType.New);
            }

        }
    }
}
