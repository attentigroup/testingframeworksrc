﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion


namespace AssertBlocksLib.OffendersAsserts
{
    public class AddOffenderRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            Assert.AreEqual(AddOffenderRequest.FirstName, getOffenderResponse.FirstName, $"Expected first name: {AddOffenderRequest.FirstName}, Actual first name: {getOffenderResponse.FirstName}");
            Assert.AreEqual(AddOffenderRequest.Gender, getOffenderResponse.Gender, $"Expected gender: {AddOffenderRequest.Gender}, Actual gender: {getOffenderResponse.Gender}");
            Assert.AreEqual(AddOffenderRequest.LastName, getOffenderResponse.LastName, $"Expected last name: {AddOffenderRequest.LastName}, Actual last name: {getOffenderResponse.LastName}");
            Assert.AreEqual(AddOffenderRequest.OfficerID, getOffenderResponse.OfficerID, $"Expected Officer ID: {AddOffenderRequest.OfficerID}, Actual Officer ID: {getOffenderResponse.OfficerID}");
            Assert.AreEqual(AddOffenderRequest.ProgramType, getOffenderResponse.ProgramType, $"Expected program type: {AddOffenderRequest.ProgramType}, Actual program type: {getOffenderResponse.ProgramType}");
            Assert.AreEqual(AddOffenderRequest.RefID, getOffenderResponse.RefID, $"Expected ref ID: {AddOffenderRequest.RefID}, Actual ref ID: {getOffenderResponse.RefID}");

            Assert.AreEqual(AddOffenderRequest.AgencyID, getOffenderResponse.AgencyID, $"Expected agency ID: {AddOffenderRequest.AgencyID}, Actual agency ID: {getOffenderResponse.AgencyID}");
            //Assert.AreEqual(getOffenderResponse.DateOfBirth, AddOffenderRequest.DateOfBirth);
            Assert.AreEqual(AddOffenderRequest.DepartmentOfCorrection, getOffenderResponse.DepartmentOfCorrection, $"Expected department of correction: {AddOffenderRequest.DepartmentOfCorrection}, Actual department of correction: {getOffenderResponse.DepartmentOfCorrection}");
            Assert.AreEqual(AddOffenderRequest.HomeUnit, getOffenderResponse.EquipmentInfo.HomeUnitSN, $"Expected home unit: {AddOffenderRequest.HomeUnit}, Actual home unit: {getOffenderResponse.EquipmentInfo.HomeUnitSN}");
            Assert.AreNotEqual(AddOffenderRequest.IsSecondaryLocation, getOffenderResponse.IsPrimaryLocation);
            Assert.AreEqual(AddOffenderRequest.Language, getOffenderResponse.Language, $"Expected Language: {AddOffenderRequest.Language}, Actual Language: {getOffenderResponse.Language}");
            Assert.AreEqual(AddOffenderRequest.MiddleName, getOffenderResponse.MiddleName, $"Expected Middle Name: {AddOffenderRequest.MiddleName}, Actual Middle Name: {getOffenderResponse.MiddleName}");
            Assert.AreEqual(AddOffenderRequest.Receiver, getOffenderResponse.EquipmentInfo.ReceiverSN, $"Expected Receiver: {AddOffenderRequest.Receiver}, Actual Receiver: {getOffenderResponse.EquipmentInfo.ReceiverSN}");
            Assert.AreEqual(AddOffenderRequest.SocialSecurity, getOffenderResponse.SocialSecurity, $"Expected Social Security: {AddOffenderRequest.SocialSecurity}, Actual Social Security: {getOffenderResponse.SocialSecurity}");
            Assert.AreEqual(AddOffenderRequest.Transmitter, getOffenderResponse.EquipmentInfo.TransmitterSN, $"Expected Transmitter: {AddOffenderRequest.Transmitter}, Actual Transmitter: {getOffenderResponse.EquipmentInfo.TransmitterSN}");  


        }
    }


    public class AddOffenderRequestAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            Assert.AreEqual(getOffenderResponse.FirstName, AddOffenderRequest.FirstName);
            Assert.AreEqual(getOffenderResponse.Gender, AddOffenderRequest.Gender);
            Assert.AreEqual(getOffenderResponse.LastName, AddOffenderRequest.LastName);
            Assert.AreEqual(getOffenderResponse.OfficerID, AddOffenderRequest.OfficerID);
            Assert.AreEqual(getOffenderResponse.ProgramType, AddOffenderRequest.ProgramType);
            Assert.AreEqual(getOffenderResponse.RefID, AddOffenderRequest.RefID);
        }
    }


    public class AddOffenderRequestAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            Assert.AreEqual(getOffenderResponse.FirstName, AddOffenderRequest.FirstName);
            Assert.AreEqual(getOffenderResponse.Gender, AddOffenderRequest.Gender);
            Assert.AreEqual(getOffenderResponse.LastName, AddOffenderRequest.LastName);
            Assert.AreEqual(getOffenderResponse.OfficerID, AddOffenderRequest.OfficerID);
            Assert.AreEqual(getOffenderResponse.ProgramType, AddOffenderRequest.ProgramType);
            Assert.AreEqual(getOffenderResponse.RefID, AddOffenderRequest.RefID);
        }
    }
}
