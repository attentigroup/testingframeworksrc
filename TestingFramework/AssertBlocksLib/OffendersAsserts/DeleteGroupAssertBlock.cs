﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;


namespace AssertBlocksLib.OffendersAsserts
{
    public class DeleteGroupAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetGroupDetailsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntMsgGetGroupDetailsResponse GetGroupDetailsResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetGroupDetailsResponse.GroupDetails.Length == 0);
                      
        }

    }
}
