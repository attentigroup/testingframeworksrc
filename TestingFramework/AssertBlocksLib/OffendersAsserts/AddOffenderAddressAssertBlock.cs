﻿using Common.CustomAttributes;
using Common.Enum;
using System.Linq;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class AddOffenderAddressAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderAddressRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderAddressRequest AddOffenderAddressRequest { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderAddressResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderAddressResponse AddOffenderAddressResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            var offenderAddress = getOffenderResponse.ContactInfo.Addresses.FirstOrDefault(o => o.ID == AddOffenderAddressResponse.NewAddressID);
            Assert.AreEqual(AddOffenderAddressRequest.CityID, offenderAddress.CityID);
            Assert.AreEqual(AddOffenderAddressRequest.Description, offenderAddress.Description);
            Assert.AreEqual(AddOffenderAddressRequest.OffenderID, offenderAddress.OffenderID);
            Assert.AreEqual(AddOffenderAddressRequest.StateID, offenderAddress.StateID);
            Assert.AreEqual(AddOffenderAddressRequest.Street, offenderAddress.Street);
            Assert.AreEqual(AddOffenderAddressRequest.ZipCode, offenderAddress.ZipCode);
            Assert.AreEqual(AddOffenderAddressRequest.ZoneID, offenderAddress.ZoneID);
        }
    }
}
