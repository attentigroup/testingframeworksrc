﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetDeletedOffendersAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetDeletedOffendersResponse GetDeletedOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Offenders0.EntDeletedOffenderData deletedOffender;
            int deletedOffenderID = -1, offenderID = -2;
            for (int i = 0; i < GetDeletedOffendersResponse.DeletedOffendersList.Length; i++) {
                deletedOffender = GetDeletedOffendersResponse.DeletedOffendersList[i];
                if(deletedOffender.OffenderID == GetOffendersResponse.OffendersList[0].ID)
                {
                    deletedOffenderID = deletedOffender.OffenderID;
                    offenderID = GetOffendersResponse.OffendersList[0].ID;
                    break;
                }
            }
            Assert.AreEqual(deletedOffenderID, offenderID);
        }
    }


    public class GetDeletedOffendersAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetDeletedOffendersResponse GetDeletedOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Offenders1.EntDeletedOffenderData deletedOffender;
            int deletedOffenderID = -1, offenderID = -2;
            for (int i = 0; i < GetDeletedOffendersResponse.DeletedOffendersList.Length; i++)
            {
                deletedOffender = GetDeletedOffendersResponse.DeletedOffendersList[i];
                if (deletedOffender.OffenderID == GetOffendersResponse.OffendersList[0].ID)
                {
                    deletedOffenderID = deletedOffender.OffenderID;
                    offenderID = GetOffendersResponse.OffendersList[0].ID;
                    break;
                }
            }
            Assert.AreEqual(deletedOffenderID, offenderID);
        }
    }


    public class GetDeletedOffendersAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetDeletedOffendersResponse GetDeletedOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Offenders2.EntDeletedOffenderData deletedOffender;
            int deletedOffenderID = -1, offenderID = -2;
            for (int i = 0; i < GetDeletedOffendersResponse.DeletedOffendersList.Length; i++)
            {
                deletedOffender = GetDeletedOffendersResponse.DeletedOffendersList[i];
                if (deletedOffender.OffenderID == GetOffendersResponse.OffendersList[0].ID)
                {
                    deletedOffenderID = deletedOffender.OffenderID;
                    offenderID = GetOffendersResponse.OffendersList[0].ID;
                    break;
                }
            }
            Assert.AreEqual(deletedOffenderID, offenderID);
        }
    }
}
