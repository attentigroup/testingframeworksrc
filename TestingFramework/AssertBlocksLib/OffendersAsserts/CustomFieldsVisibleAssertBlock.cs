﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    /// <summary>
    /// verify that all visible custom fields return for every offender.
    /// </summary>
    public class CustomFieldsVisibleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.VisibleCustomFieldsDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntCustomFieldDefinition[] VisibleCustomFieldsDefinitions { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        /// <summary>
        /// verify that all visible custom fields return for every offender.
        /// </summary>
        protected override void ExecuteBlock()
        {
            foreach (var offender in GetOffendersResponse.OffendersList)
            {
                Assert.AreEqual(VisibleCustomFieldsDefinitions.Length, offender.CustomFields.Length,
                    $"Visible Custom Fields Length ({VisibleCustomFieldsDefinitions.Length}) different from offender.CustomFields Length ({offender.CustomFields.Length})");
                var dicFieldsNames = offender.CustomFields.ToDictionary(field => field.ID, field => field.FieldName);
                foreach (var customField in VisibleCustomFieldsDefinitions)
                {//check if the field exist in the offender record
                    string fieldName = null;
                    var getFieldNameResult = dicFieldsNames.TryGetValue(customField.ID, out fieldName);
                    Assert.IsTrue(getFieldNameResult, 
                        $"Field {customField.ID} name {customField.Name} type {customField.Type} not exist in offender {offender.ID} refId {offender.RefID}");
                    Log.Info($"Field {customField.ID} name {customField.Name} type {customField.Type} exist for offender id {offender.ID} refId {offender.RefID}");
                }
            }
        }
    }
}
