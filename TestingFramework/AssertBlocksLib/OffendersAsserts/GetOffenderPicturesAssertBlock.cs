﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffenderPicturesAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.ExpectedPictureID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ExpectedPictureID { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ExpectedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetPicturesResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetPicturesResponse GetPicturesResponse { get; set; }

        protected override void ExecuteBlock()
        {
            if (ExpectedPictureID != 0 && ExpectedOffenderID != null)
            {
                var offenderPicture = GetPicturesResponse.PicturesList[0];
                CheckGetContactByContactID(offenderPicture);
                Assert.AreEqual(ExpectedOffenderID, offenderPicture.OffenderID);
            }
            else if (ExpectedPictureID != 0)
            {
                var offenderContact = GetPicturesResponse.PicturesList[0];
                CheckGetContactByContactID(offenderContact);
            }
            else if (ExpectedOffenderID != null)
            {
                foreach (var contact in GetPicturesResponse.PicturesList)
                {
                    Assert.AreEqual(ExpectedOffenderID, contact.OffenderID);
                }
            }

        }

        private void CheckGetContactByContactID(Offenders0.EntOffenderPicture offenderPicture)
        {
            Assert.IsTrue(GetPicturesResponse.PicturesList.Count() == 1);
            Assert.AreEqual(ExpectedPictureID, offenderPicture.PictureID);
        }
    }
}
