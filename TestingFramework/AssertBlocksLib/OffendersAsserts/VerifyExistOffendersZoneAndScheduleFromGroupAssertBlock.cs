﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.Proxies.API.Schedule;


#region API refs

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.Zones12_0;
using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace AssertBlocksLib.GroupsBlocks
{
    public class VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock : VerifyOffendersZoneAndScheduleAssertBlockBase
    {

        public override void ExecuteSpecific(bool exist)
        {
            Assert.IsTrue(exist, "Group's Zone or Schedule Doesn't Exist");
        }
    }
}
