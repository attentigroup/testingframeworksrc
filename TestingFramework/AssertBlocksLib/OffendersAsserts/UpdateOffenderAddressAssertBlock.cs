﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.OffendersAsserts
{
    public class UpdateOffenderAddressAssertBlock : AssertBaseBlock
    {
        
        [PropertyTest(EnumPropertyName.AddOffenderAddressResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderAddressResponse AddOffenderAddressResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponseAfterUpdate { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            var offenderAddress = getOffenderResponse.ContactInfo.Addresses.FirstOrDefault(o => o.ID == AddOffenderAddressResponse.NewAddressID);
            var getOffenderResponseAfterUpdate = GetOffendersResponseAfterUpdate.OffendersList[0];
            var offenderAddressAfterUpdate = getOffenderResponseAfterUpdate.ContactInfo.Addresses.FirstOrDefault(o => o.ID == AddOffenderAddressResponse.NewAddressID);
            Assert.AreNotEqual(offenderAddress.CityID, offenderAddressAfterUpdate.CityID);
            Assert.AreNotEqual(offenderAddress.Description, offenderAddressAfterUpdate.Description);
            Assert.AreEqual(offenderAddress.OffenderID, offenderAddressAfterUpdate.OffenderID);
            Assert.AreNotEqual(offenderAddress.StateID, offenderAddressAfterUpdate.StateID);
            Assert.AreNotEqual(offenderAddress.Street, offenderAddressAfterUpdate.Street);
            Assert.AreNotEqual(offenderAddress.ZipCode, offenderAddressAfterUpdate.ZipCode);
            //Assert.AreNotEqual(offenderAddress.ZoneID, offenderAddressAfterUpdate.ZoneID);

        }
    }
}
