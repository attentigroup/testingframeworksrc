﻿using System;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Zones;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    /// <summary>
    /// This block only check if the offender you searched for in the GetOffendersResponse is active or not
    /// If the offender isn't active it thorws an exeption
    /// </summary>
    public class IsActiveAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool Expected { get; set; }


        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender OffenderEnt { get; set; }

        protected override void ExecuteBlock()
        {
            OffenderEnt = null;
            if (GetOffendersResponse.OffendersList.Length > 0)
            {
                try
                {
                    OffenderEnt = GetOffendersResponse.OffendersList[0];
                }
                catch (Exception e)
                {
                    Log.WarnFormat($"{e}: The IsActive field of offender ID {OffenderEnt.ID} is null");
                }
                var status = OffenderEnt.IsActive == false ? "Not Active" : "Active";
                Assert.IsTrue(OffenderEnt.IsActive == Expected, $"OffenderID: {OffenderEnt.ID} -> Status: {status}; Not as expected");
            }                         
            else
            {
                throw new Exception("GetOffendersResponse.OffendersList is empty - Activation process failed");
            }
        }
    }
}

   