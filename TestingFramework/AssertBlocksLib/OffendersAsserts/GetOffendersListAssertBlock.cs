﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffendersListAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.FirstNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter FirstName { get; set; }

        [PropertyTest(EnumPropertyName.LastNameQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter LastName { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramType [] ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramStatus[] ProgramStatus { get; set; }

        [PropertyTest(EnumPropertyName.ProgramConcept, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EnmProgramConcept? ProgramConcept { get; set; }

        [PropertyTest(EnumPropertyName.SocialSecurity, EnumPropertyType.SocialSecurity, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter SocialSecurity { get; set; }

        [PropertyTest(EnumPropertyName.Receiver, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter Receiver { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntNumericParameter OfficerID { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.IncludeAddressList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeAddressList { get; set; }

        [PropertyTest(EnumPropertyName.EndOfServiceCodeQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter EndOfServiceCode { get; set; }

        [PropertyTest(EnumPropertyName.ProgramPhoneNumberQueryParameter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntStringParameter ProgramPhoneNumberQueryParameter { get; set; }

        //[PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.None)]
        //public int? GroupID { get; set; }


        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            if (GetOffendersResponse == null || GetOffendersResponse.OffendersList.Length == 0)
                throw new Exception("Offenders list is empty!");

            if (AgencyID != 0)
            {
                foreach(var offender in GetOffendersResponse.OffendersList)
                {
                    Assert.AreEqual((int)AgencyID, offender.AgencyID);
                }
            }

            if (FirstName!=null)
            {
                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    Assert.AreEqual(FirstName.Value, offender.FirstName);
                }
            }

            if (LastName!=null)
            {
                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    Assert.AreEqual(LastName.Value, offender.LastName);
                }
            }

            if (Limit != 0)
            {
                Assert.IsTrue(GetOffendersResponse.OffendersList.Count()<= Limit);
            }
           
            if (ProgramConcept != null)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.ProgramConcept == ProgramConcept));


            if (SocialSecurity != null)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.SocialSecurity == SocialSecurity.Value));

            if (Receiver != null)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.EquipmentInfo.ReceiverSN == Receiver.Value));

            if (OffenderID != 0)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.ID == OffenderID));

            if (OfficerID != null)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.OfficerID == OfficerID.Value));

            if (OffenderRefID != null)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.RefID == OffenderRefID.Value));

            if (IncludeAddressList)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.ContactInfo.Addresses.Length > 1));

            if (!IncludeAddressList)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.ContactInfo.Addresses.Length == 1));

            if (EndOfServiceCode!=null)
                Assert.IsTrue(GetOffendersResponse.OffendersList.All(x => x.EosCode == EndOfServiceCode.Value));

           if (ProgramPhoneNumberQueryParameter != null)
            {
                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    bool cond = (offender.ContactInfo.CellData.VoicePrefixPhone + offender.ContactInfo.CellData.VoicePhoneNumber).Contains(ProgramPhoneNumberQueryParameter.Value);                    
                    Assert.IsTrue(cond);
                }
            }

            if (ProgramStatus != null)
            {
                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    Assert.IsTrue(ProgramStatus.Contains(offender.ProgramStatus));
                }
            }

            if (ProgramType != null)
            {
                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    Assert.IsTrue(ProgramType.Contains(offender.ProgramType.Value));
                }
            }


        }
    }
}
