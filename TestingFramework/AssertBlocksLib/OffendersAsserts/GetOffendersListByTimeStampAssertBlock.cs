﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffendersListByTimeStampAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponseByTimeStamp { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetOffendersResponse.OffendersList.Length >= GetOffendersResponseByTimeStamp.OffendersList.Length);
        }
    }
}
