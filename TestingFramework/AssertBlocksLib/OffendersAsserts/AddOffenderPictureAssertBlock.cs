﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.OffendersAsserts
{
    public class AddOffenderPictureAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderPictureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddPictureRequest AddOffenderPictureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetPicturesResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetPicturesResponse GetPicturesResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {            
            Assert.AreEqual(AddOffenderPictureResponse.NewPhotoID, GetPicturesResponse.PicturesList[0].PictureID);
            Assert.AreEqual(AddOffenderPictureRequest.OffenderID, GetPicturesResponse.PicturesList[0].OffenderID);

        }
    }
}
