﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{ 
    public class FilterTitleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.ExpectedTitle, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ExpectedTitle { get; set; }

        [PropertyTest(EnumPropertyName.ActualTitle, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ActualTitle { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(ExpectedTitle, ActualTitle,$"Actual Filter name that display:{ActualTitle} , Expected filter Name: {ExpectedTitle}");
        }
    }
}

