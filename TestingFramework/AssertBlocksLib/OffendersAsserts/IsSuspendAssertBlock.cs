﻿using System;
using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Zones;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    /// <summary>
    /// This block only check if the offender you searched for in the GetOffendersResponse is active or not
    /// If the offender isn't active it thorws an exeption
    /// </summary>
    public class IsSuspendAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmProgramStatus Expected { get; set; }


        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Offenders0.EntOffender OffenderEnt { get; set; }

        protected override void ExecuteBlock()
        {
            OffenderEnt = null;
            if (GetOffendersResponse.OffendersList.Length > 0)
            {
                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    if (offender.ProgramStatus == Expected)
                    {
                        OffenderEnt = offender;
                        break;
                    }
                }
                Assert.AreEqual(OffenderEnt.ProgramStatus, Expected, "Is not as expected");
            }
            else
            {
                throw new Exception("GetOffendersResponse.OffendersList is empty - Suspend program failed");
            }
        }
    }
}

