﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Groups0 =TestingFramework.Proxies.EM.Interfaces.Groups12_0;

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffendersListByGroupIdAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.Group, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntGroup Group { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetOffendersResponse.OffendersList.Length, Group.OffendersList.Length);
        }
    }
}
