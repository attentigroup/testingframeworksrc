﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class UpdateOffenderAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponseAfterUpdate { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            var getOffenderResponseAfterUpdate = GetOffendersResponseAfterUpdate.OffendersList[0];
            Assert.AreNotEqual(getOffenderResponse.FirstName, getOffenderResponseAfterUpdate.FirstName);
            Assert.AreNotEqual(getOffenderResponse.LastName, getOffenderResponseAfterUpdate.LastName);
            Assert.AreNotEqual(getOffenderResponse.OfficerID, getOffenderResponseAfterUpdate.OfficerID);
            Assert.AreEqual(getOffenderResponse.ProgramType, getOffenderResponseAfterUpdate.ProgramType);
            Assert.AreEqual(getOffenderResponse.RefID, getOffenderResponseAfterUpdate.RefID);

            Assert.AreNotEqual(getOffenderResponse.AgencyID, getOffenderResponseAfterUpdate.AgencyID);
            Assert.AreNotEqual(getOffenderResponse.DateOfBirth, getOffenderResponseAfterUpdate.DateOfBirth);
            Assert.AreNotEqual(getOffenderResponse.DepartmentOfCorrection, getOffenderResponseAfterUpdate.DepartmentOfCorrection);
            //Assert.AreEqual(getOffenderResponse.EquipmentInfo.HomeUnitSN, AddOffenderRequest.HomeUnit);
            //Assert.AreNotEqual(getOffenderResponse.Language, getOffenderResponseAfterUpdate.Language);
            Assert.AreNotEqual(getOffenderResponse.MiddleName, getOffenderResponseAfterUpdate.MiddleName);
            //Assert.AreNotEqual(getOffenderResponse.ProgramConcept, getOffenderResponseAfterUpdate.ProgramConcept);
            //Assert.AreEqual(getOffenderResponse.EquipmentInfo.ReceiverSN, getOffenderResponseAfterUpdate.Receiver);
            Assert.AreNotEqual(getOffenderResponse.SocialSecurity, getOffenderResponseAfterUpdate.SocialSecurity);
            //Assert.AreEqual(getOffenderResponse.EquipmentInfo.TransmitterSN, getOffenderResponseAfterUpdate.Transmitter);


        }
    }
}
