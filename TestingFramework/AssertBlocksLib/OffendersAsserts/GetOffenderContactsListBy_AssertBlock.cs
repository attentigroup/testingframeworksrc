﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffenderContactsListBy_AssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.ExpectedContactID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ExpectedContactID { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedOffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ExpectedOffenderID { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            if (ExpectedContactID != 0 && ExpectedOffenderID != 0)
            {
                var offenderContact = GetOffenderContactsListResponse.ContactsList[0];
                CheckGetContactByContactID(offenderContact);
                Assert.AreEqual(ExpectedOffenderID, offenderContact.OffenderID);
            }
            if (ExpectedContactID != 0)
            {
                var offenderContact = GetOffenderContactsListResponse.ContactsList[0];
                CheckGetContactByContactID(offenderContact);
            }
            else if (ExpectedOffenderID != 0)
            {
                foreach(var contact in GetOffenderContactsListResponse.ContactsList)
                {
                    Assert.AreEqual(ExpectedOffenderID, contact.OffenderID);
                }
            }
           
        }

        private void CheckGetContactByContactID(Offenders0.EntOffenderContact offenderContact)
        {
            Assert.IsTrue(GetOffenderContactsListResponse.ContactsList.Count() == 1);
            Assert.AreEqual(ExpectedContactID, offenderContact.ContactID);
        }
    }
}
