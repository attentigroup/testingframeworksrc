﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.OffendersAsserts
{
    public class UpdateOffenderContactAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsListResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsListResponseAfterUpdate { get; set; }

        protected override void ExecuteBlock()
        {
            var offenderContactBeforeUpdate = GetOffenderContactsListResponse.ContactsList[0];
            var offenderContactAfterUpdate = GetOffenderContactsListResponseAfterUpdate.ContactsList[0];

            Assert.AreNotEqual(offenderContactBeforeUpdate.FirstName, offenderContactAfterUpdate.FirstName);
            Assert.AreNotEqual(offenderContactBeforeUpdate.LastName, offenderContactAfterUpdate.LastName); 
            Assert.AreNotEqual(offenderContactBeforeUpdate.Priority, offenderContactAfterUpdate.Priority);
            Assert.AreNotEqual(offenderContactBeforeUpdate.Relation, offenderContactAfterUpdate.Relation);
            Assert.AreNotEqual(offenderContactBeforeUpdate.MiddleName, offenderContactAfterUpdate.MiddleName);
            Assert.AreNotEqual(offenderContactBeforeUpdate.Phone, offenderContactAfterUpdate.Phone);
            Assert.AreNotEqual(offenderContactBeforeUpdate.PrefixPhone, offenderContactAfterUpdate.PrefixPhone);

        }
    }
}
