﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    public class ReallocateActiveOffenderAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders12_0.EntMsgAddOffenderRequest AddOffenderRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders12_0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getOffenderResponse = GetOffendersResponse.OffendersList[0];
            Assert.AreNotEqual(getOffenderResponse.OfficerID, AddOffenderRequest.OfficerID);
            Assert.AreNotEqual(getOffenderResponse.AgencyID, AddOffenderRequest.AgencyID);
        }
    }
}
