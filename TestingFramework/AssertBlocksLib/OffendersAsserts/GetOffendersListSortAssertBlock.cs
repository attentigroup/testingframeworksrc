﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffendersListSortAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponseSorted { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EnmOffendersSortOptions SortField { get; set; }
        protected override void ExecuteBlock()
        {
            Offenders0.EntOffender[] offenders = new Offenders0.EntOffender[GetOffendersResponse.OffendersList.Length];
            switch (SortField)
            {
                case Offenders0.EnmOffendersSortOptions.OffenderID:
                    if(SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.ID).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.ID).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.OffenderRefID:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.RefID).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.RefID).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.AgencyID:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.AgencyID).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.AgencyID).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.OfficerID:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.OfficerID).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.OfficerID).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.Receiver:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.EquipmentInfo.ReceiverSN).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.EquipmentInfo.ReceiverSN).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.Transmitter:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.EquipmentInfo.TransmitterSN).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.EquipmentInfo.TransmitterSN).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.FirstName:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.FirstName).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.FirstName).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.MiddleName:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.MiddleName).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.MiddleName).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.LastName:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.LastName).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.LastName).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.SocialSecurityNumber:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.SocialSecurity).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.SocialSecurity).ToArray();
                    break;
                //case Offenders0.EnmOffendersSortOptions.ProgramMainPhone:
                //    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                //        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.ID).ToArray();
                //    else
                //        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.ID).ToArray();
                //    break;
                case Offenders0.EnmOffendersSortOptions.EndOfServiceCode:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.EosCode).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.EosCode).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.ProgramConcept:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.ProgramConcept).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.ProgramConcept).ToArray();
                    break;
                case Offenders0.EnmOffendersSortOptions.LastTimeStamp:
                    if (SortDirection == Offenders0.ListSortDirection.Ascending)
                        offenders = GetOffendersResponse.OffendersList.OrderBy(x => x.Timestamp).ToArray();
                    else
                        offenders = GetOffendersResponse.OffendersList.OrderByDescending(x => x.Timestamp).ToArray();
                    break;
                default:
                    break;
            }

            for (int i = 0; i < offenders.Length; i++)
            {
                Assert.AreEqual(GetOffendersResponseSorted.OffendersList[i].ID, offenders[i].ID); 
            }

            


        }
    }
}
