﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffenderContactsListAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var offenderContact = GetOffenderContactsListResponse.ContactsList[0];

            Assert.AreEqual(offenderContact.FirstName, AddOffenderContactRequest.FirstName);
            Assert.AreEqual(offenderContact.LastName, AddOffenderContactRequest.LastName);
            Assert.AreEqual(offenderContact.OffenderID, AddOffenderContactRequest.OffenderID);
            Assert.AreEqual(offenderContact.Priority, AddOffenderContactRequest.Priority);
            Assert.AreEqual(offenderContact.Relation, AddOffenderContactRequest.Relation);
            Assert.AreEqual(offenderContact.MiddleName, AddOffenderContactRequest.MiddleName);
            Assert.AreEqual(offenderContact.Phone, AddOffenderContactRequest.Phone);
            Assert.AreEqual(offenderContact.PrefixPhone, AddOffenderContactRequest.PrefixPhone);

        }
    }


    public class GetOffenderContactsListAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffenderContactsListResponse GetOffenderContactsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var offenderContact = GetOffenderContactsListResponse.ContactsList[0];

            Assert.AreEqual(offenderContact.FirstName, AddOffenderContactRequest.FirstName);
            Assert.AreEqual(offenderContact.LastName, AddOffenderContactRequest.LastName);
            Assert.AreEqual(offenderContact.OffenderID, AddOffenderContactRequest.OffenderID);
            Assert.AreEqual(offenderContact.Priority, AddOffenderContactRequest.Priority);
            Assert.AreEqual(offenderContact.Relation, AddOffenderContactRequest.Relation);
        }
    }


    public class GetOffenderContactsListAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddOffenderContactRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderContactRequest AddOffenderContactRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffenderContactsListResponse GetOffenderContactsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var offenderContact = GetOffenderContactsListResponse.ContactsList[0];

            Assert.AreEqual(offenderContact.FirstName, AddOffenderContactRequest.FirstName);
            Assert.AreEqual(offenderContact.LastName, AddOffenderContactRequest.LastName);
            Assert.AreEqual(offenderContact.OffenderID, AddOffenderContactRequest.OffenderID);
            Assert.AreEqual(offenderContact.Priority, AddOffenderContactRequest.Priority);
            Assert.AreEqual(offenderContact.Relation, AddOffenderContactRequest.Relation);
        }
    }
}
