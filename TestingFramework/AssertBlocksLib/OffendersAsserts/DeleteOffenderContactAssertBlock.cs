﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.OffendersAsserts
{
    public class DeleteOffenderContactAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderContactsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetContactsListResponse GetOffenderContactsListResponse { get; set; }  
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetOffenderContactsListResponse.ContactsList.Length == 0);

        }
    }
}
