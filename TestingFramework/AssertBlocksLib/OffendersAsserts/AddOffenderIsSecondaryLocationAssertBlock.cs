﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.OffendersAsserts
{
    public class AddOffenderIsSecondaryLocationAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetOffendersResponse.OffendersList.Length == 2);
            Assert.AreEqual(GetOffendersResponse.OffendersList[0].RefID, GetOffendersResponse.OffendersList[1].RefID);
            Assert.AreNotEqual(GetOffendersResponse.OffendersList[0].IsPrimaryLocation, GetOffendersResponse.OffendersList[1].IsPrimaryLocation);



        }
    }
}
