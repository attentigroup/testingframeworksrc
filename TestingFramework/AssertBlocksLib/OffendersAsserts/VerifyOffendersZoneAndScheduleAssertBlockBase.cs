﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Zones;
using TestingFramework.Proxies.API.Schedule;


#region API refs

using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;

using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using System;
using System.Linq;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

#endregion

namespace AssertBlocksLib.GroupsBlocks
{
    public abstract class VerifyOffendersZoneAndScheduleAssertBlockBase : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime EndTime { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.MsgAddZoneResponse AddZoneResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZoneResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Schedule0.EntMsgAddTimeFrameResponse AddTimeFrameResponse { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeIDsList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<int> TimeFrameIdList { get; set; }
        
        [PropertyTest(EnumPropertyName.OffendersListIDs, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] OffendersListIDs { get; set; }

        public VerifyOffendersZoneAndScheduleAssertBlockBase()
        {

        }

        public virtual void ExecuteSpecific(bool exist)
        { }

        protected override void ExecuteBlock()
        {
            Assert.IsFalse(OffendersListIDs.IsNullOrEmpty(), "OffendersList is null or empty");

            int zoneID;
            if (AddZoneResponse != null)
                zoneID = AddZoneResponse.ZoneID;
            else if (GetZoneResponse != null)
                zoneID = (int)GetZoneResponse.ZoneList[0].ID;
            else
                throw new Exception("AddZoneResponse or GetZoneResponse is null!");

            bool zoneRes = FindZone(zoneID);

            int TFID;
            if (AddTimeFrameResponse != null)
                TFID = AddTimeFrameResponse.NewTimeFrameID;
            else if (!TimeFrameIdList.IsNullOrEmpty() && TimeFrameIdList.Capacity > 0)
                TFID = TimeFrameIdList.First();
            else
                throw new Exception("AddTimeFrameResponse or TimeFrameIdList is null!");

            bool TFRes = FindTF(TFID);

            bool result = zoneRes & TFRes;

            ExecuteSpecific(result);
        }

        public bool FindZone(int ZoneID)
        {
            bool zoneFound = false;
                    
            foreach (var of in OffendersListIDs)
            {
                ////////// Find Zone ID ////////////
                var getZone = new MsgGetZonesByEntityIDRequest();
                getZone.EntityID = of; // The offender's ID
                getZone.EntityType = Zones0.EnmEntityType.Offender;
                getZone.Version = -2; //-1 - Current version used by the device, -2 - Planned version to be downloaded to the device in next download
                Zones0.MsgGetZonesByEntityIDResponse zoneResponse = ZonesProxy.Instance.GetZonesByEntityID(getZone);

                var numOfZonesID = zoneResponse.ZoneList.Count(x => x.GroupZoneID.Equals(ZoneID));

                if (numOfZonesID != 0)
                    zoneFound = true;
            }
            return zoneFound;
        }

        public bool FindTF(int TFID)
        {
            bool TFFound = false;

            foreach (var of in OffendersListIDs)
            {
                ////////// Find TF ID //////////////
                var getSchedule = new Schedule0.EntMsgGetScheduleRequest();
                getSchedule.EntityID = of; // The offender's ID
                getSchedule.EntityType = Schedule0.EnmEntityType.Offender;
                getSchedule.StartDate = StartTime;
                getSchedule.EndDate = EndTime.AddMinutes(1);
                Schedule0.EntMsgGetScheduleResponse scheduleResponse = ScheduleProxy.Instance.GetSchedule(getSchedule);

                var numOfTFID = scheduleResponse.Schedule.WeeklySchedule.Count(x => x.InheritedTimeFrameID.Equals(TFID));

                if (numOfTFID != 0)
                    TFFound = true;
            }
            return TFFound;
        }


        //protected override void ExecuteBlock()
        //{

        //    foreach (var of in OffendersList)
        //    {
        //        ////////// Find Zone ID ////////////
        //        var getZone = new MsgGetZonesByEntityIDRequest();
        //        getZone.EntityID = of.ID; // The offender's ID
        //        getZone.EntityType = Zones0.EnmEntityType.Offender;
        //        getZone.Version = -2; //-1 - Current version used by the device, -2 - Planned version to be downloaded to the device in next download
        //        Zones0.MsgGetZonesByEntityIDResponse zoneResponse = ZonesProxy.Instance.GetZonesByEntityID(getZone);

        //        var numOfZonesID = zoneResponse.ZoneList.Count(x => x.GroupZoneID.Equals(ZoneID));

        //        if (numOfZonesID != 1)
        //            throw new Exception($"ZoneId from group = {ZoneID} doesn't match the ZoneId from the offender = {zoneResponse.ZoneList[0].ID}");
        //        ////////////////////////////////////

        //        ////////// Find TF ID //////////////
        //        var getSchedule = new Schedule0.EntMsgGetScheduleRequest();
        //        getSchedule.EntityID = of.ID; // The offender's ID
        //        getSchedule.EntityType = Schedule0.EnmEntityType.Offender;
        //        getSchedule.StartDate = StartTime;
        //        getSchedule.EndDate = EndTime.AddMinutes(1);
        //        Schedule0.EntMsgGetScheduleResponse scheduleResponse = ScheduleProxy.Instance.GetSchedule(getSchedule);

        //        var numOfTFID = scheduleResponse.Schedule.WeeklySchedule.Count(x => x.InheritedTimeFrameID.Equals(TFID));

        //        if (numOfTFID != 1)
        //            throw new Exception($"TimeFrameId from group = {TFID} doesn't match the TimeFrameId from the offender ");
        //        ////////////////////////////////////
        //    }
        //}

        //public override void CleanUp()
        //{

        //}
    }
}
