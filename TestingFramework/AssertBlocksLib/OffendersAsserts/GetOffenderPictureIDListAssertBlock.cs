﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffenderPictureIDListAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetPictureIDListResponse GetOffenderPictureIDListResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgAddPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {
            int offenderPictureID = GetOffenderPictureIDListResponse.PictureIDList[0];

            Assert.AreEqual(offenderPictureID, AddOffenderPictureResponse.NewPhotoID);
        }
    }


    public class GetOffenderPictureIDListAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgGetOffenderPictureIDListResponse GetOffenderPictureIDListResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders1.EntMsgAddOffenderPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {
            int offenderPictureID = GetOffenderPictureIDListResponse.PictureIDList[0];

            Assert.AreEqual(offenderPictureID, AddOffenderPictureResponse.NewPhotoID);
        }
    }


    public class GetOffenderPictureIDListAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderPictureIDListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgGetOffenderPictureIDListResponse GetOffenderPictureIDListResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddOffenderPictureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders2.EntMsgAddOffenderPictureResponse AddOffenderPictureResponse { get; set; }

        protected override void ExecuteBlock()
        {
            int offenderPictureID = GetOffenderPictureIDListResponse.PictureIDList[0];

            Assert.AreEqual(offenderPictureID, AddOffenderPictureResponse.NewPhotoID);
        }
    }
}
