﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;


namespace AssertBlocksLib.OffendersAsserts
{
    public class AddNewGroupAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GroupID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int GroupID { get; set; }

        [PropertyTest(EnumPropertyName.OffendersList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Groups0.EntGroupOffender[] OffendersList { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GroupID != 0, "Not Expected To Be GroupID: {GroupID}");
            Assert.IsFalse(OffendersList.IsNullOrEmpty(), "OffendersList is null or empty");
        }

    }
}
