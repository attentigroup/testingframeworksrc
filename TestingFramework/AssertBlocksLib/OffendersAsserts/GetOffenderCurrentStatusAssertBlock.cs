﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

#endregion

namespace AssertBlocksLib.OffendersAsserts
{
    public class GetOffenderCurrentStatusAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderCurrentStatusResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffenderCurrentStatusResponse GetOffenderCurrentStatusResponse { get; set; }


        protected override void ExecuteBlock()
        {
            var getOffenderCurrentStatusResponse = GetOffenderCurrentStatusResponse.Status;
            Assert.AreNotEqual(getOffenderCurrentStatusResponse, null);
        }
    }
}
