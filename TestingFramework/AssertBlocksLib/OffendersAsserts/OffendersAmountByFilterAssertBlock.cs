﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#endregion

namespace AssertBlocksLib.OffendersAsserts
{ 
    public class OffendersAmountByFilterAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int AmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.Title, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string filterTitle { get; set; }

        [PropertyTest(EnumPropertyName.GetDeletedOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersCountersResponse GetOffendersCountersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            object val;
            int sum = -1;
            if (GetOffendersCountersResponse != null)
            {                
                GetOffendersCountersResponse.CountersList.TryGetValue(filterTitle, out val);
                sum = (int)val;
            }
            if(GetOffendersResponse != null)
            {
                sum = GetOffendersResponse.TotalCount;
            }


            Assert.AreEqual(AmountOfRows, sum, $"Web amount of offenders: {AmountOfRows}, API amount of offenders: {sum}");
        }
    }
}

