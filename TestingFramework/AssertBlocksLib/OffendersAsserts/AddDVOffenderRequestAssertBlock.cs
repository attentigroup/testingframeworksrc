﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssertBlocksLib.OffendersAsserts
{
    public class AddDVOffenderRequestAssertBlock : AddOffenderRequestAssertBlock
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            if (AddOffenderRequest.RelatedOffenderID != -1)
                Assert.AreEqual(AddOffenderRequest.RelatedOffenderID, GetOffendersResponse.OffendersList[0].RelatedOffenderID, $"Expected Related Offender ID: {AddOffenderRequest.RelatedOffenderID}, Actual Related Offender ID: {GetOffendersResponse.OffendersList[0].RelatedOffenderID}");

        }
    }
}
