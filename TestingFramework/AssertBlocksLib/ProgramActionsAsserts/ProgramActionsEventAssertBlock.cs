﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.ProgramActionsAsserts
{
    public class ProgramActionsEventAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EventCode, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EventCode { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var downloadEvent = GetEventsResponse.EventsList.FirstOrDefault(x => x.Code == EventCode);

            if( downloadEvent == null)
            {
                Log.Debug("ProgramActionsEventAssertBlock - Event doesn't exist. going to log all events");
                foreach (var specificEvent in GetEventsResponse.EventsList)
                {
                    var eventData = specificEvent.ToLog();
                    Log.Debug($"Event Data {eventData}");
                }
            }

            Assert.IsTrue(downloadEvent != null, "Event doesn't exist");
        }
    }
}
