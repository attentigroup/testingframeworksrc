﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using System;
#endregion

namespace AssertBlocksLib.ProgramActionsAsserts
{
    public class GetActionsListCheckValuesAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.EntMsgGetActionsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgGetActionsListResponse EntMsgGetActionsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            List<string> values = new List<string>();

            // if the list is empty fail the test //
            foreach (ProgramActions0.EnmProgramType pt in EntMsgGetActionsListResponse.Actions.Keys)
            {
                values.Add(pt.ToString());
            }

            Assert.AreEqual(values.Count, EntMsgGetActionsListResponse.Actions.Keys.Count);

            Assert.IsTrue(values.Contains("RF_Landline"), "RF_Landline missing"); //
            Assert.IsTrue(values.Contains("RF_Cellular"), "RF_Cellular missing"); //
            Assert.IsTrue(values.Contains("RF_Dual"), "RF_Dual missing"); //
            Assert.IsTrue(values.Contains("VB_Landline"), "VB_Landline missing"); //
            Assert.IsTrue(values.Contains("VB_Cellular"), "VB_Cellular missing"); //
            Assert.IsTrue(values.Contains("VBR_Landline"), "VBR_Landline missing"); //
            Assert.IsTrue(values.Contains("VBR_Cellular"), "VBR_Cellular missing"); //
            Assert.IsTrue(values.Contains("E4_Landline"), "E4_Landline missing"); //
            Assert.IsTrue(values.Contains("E4_Cellular"), "E4_Cellular missing"); //
            Assert.IsTrue(values.Contains("E4_Dual"), "E4_Dual missing"); //
            Assert.IsTrue(values.Contains("One_Piece"), "One_Piece missing"); //
            Assert.IsTrue(values.Contains("One_Piece_RF"), "One_Piece_RF missing"); //
            Assert.IsTrue(values.Contains("Two_Piece"), "Two_Piece missing");//
            Assert.IsTrue(values.Contains("Voice_Verification"), "Voice_Verification missing"); //
            Assert.IsTrue(values.Contains("VB_Dual"), "VB_Dual missing"); //
            Assert.IsTrue(values.Contains("MTD"), "MTD missing"); //
            Assert.IsTrue(values.Contains("VBR_Dual"), "VBR_Dual missing");//
        }
    }
}
