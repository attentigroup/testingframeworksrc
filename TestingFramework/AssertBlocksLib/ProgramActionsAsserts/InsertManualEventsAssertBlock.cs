﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.ProgramActionsAsserts
{
    public class InsertManualEventsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.InsertManualEventRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public ProgramActions0.EntMsgInsertManualEventRequest InsertManualEventRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(InsertManualEventRequest.EventCode, GetEventsResponse.EventsList[0].Code);
            Assert.AreEqual(InsertManualEventRequest.HandlingStatus.ToString(), GetEventsResponse.EventsList[0].Status.ToString());
            Assert.AreEqual(InsertManualEventRequest.OffenderID, GetEventsResponse.EventsList[0].OffenderID);
        }
    }
}
