﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.Proxies.API.Queue;
using System.Threading;
#endregion

namespace AssertBlocksLib.ProgramActionsAsserts
{
    public class SendVibrationRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetQueueResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueResponse GetQueueResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendVibrationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendVibrationRequest EntMsgSendVibrationRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntQueueProgramTracker, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Queue0.EntQueueProgramTracker EntQueueProgramTracker { get; set; }

        protected override void ExecuteBlock()
        {
            bool reqFound = false;
            if (GetQueueResponse.RequestList.Length == 0)
            {
                Assert.AreEqual(reqFound, false);
            }
            else
            {
                for (int i = 0; i < GetQueueResponse.RequestList.Length; i++)
                {
                    EntQueueProgramTracker = (Queue0.EntQueueProgramTracker)GetQueueResponse.RequestList[i];

                    if (EntQueueProgramTracker.OffenderId == EntMsgSendVibrationRequest.OffenderID)
                    {
                        reqFound = true;
                        break;
                    }
                }

                try
                {
                    Assert.AreEqual(reqFound, false); // Success - if there's no requests on this offender in the queue, Fail - otherwise
                }
                catch (Exception e)
                {
                    throw new Exception(e + ", no matchup between Queue and DownloadRequest");
                }
            }
        }
    }
}
