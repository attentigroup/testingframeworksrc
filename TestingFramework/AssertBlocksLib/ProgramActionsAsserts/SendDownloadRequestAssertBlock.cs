﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.Proxies.API.Queue;
using System.Threading;
#endregion

namespace AssertBlocksLib.ProgramActionsAsserts
{
    /// <summary>
    /// This Assert Block Checks if there's any requests in the queue for your offender
    /// </summary>
    public class SendDownloadRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetQueueResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Queue0.EntMessageGetQueueResponse GetQueueResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgSendDownloadRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendDownloadRequest EntMsgSendDownloadRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntQueueProgramTracker, EnumPropertyType.None, EnumPropertyModifier.Output)]
        public Queue0.EntQueueProgramTracker EntQueueProgramTracker { get; set; }

        protected override void ExecuteBlock()
        {
            bool reqFound = false;
            for (int i=0; i< GetQueueResponse.RequestList.Length ; i++)
            {

                EntQueueProgramTracker = (Queue0.EntQueueProgramTracker) GetQueueResponse.RequestList[i];

                if (EntQueueProgramTracker.OffenderId == GetOffenderId())
                {
                    reqFound = true;
                    break;
                }
            }
      
            try
            {
                Assert.AreEqual(reqFound, false); // Success - if there's no requests on this offender in the queue, Fail - otherwise
            }
            catch (Exception e)
            {
                throw new Exception(e + ", no matchup between Queue and DownloadRequest");
            }
        }

        protected virtual int GetOffenderId()
        {
            return EntMsgSendDownloadRequest.OffenderID;
        }
    }
}
