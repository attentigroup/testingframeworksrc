﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;


#region API refs
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestingFramework.Proxies.API.Queue;
using System.Threading;
#endregion
namespace AssertBlocksLib.ProgramActionsAsserts
{
    public class GetProgramSuspensionsDataAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.EntMsgGetProgramSuspensionsDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgGetProgramSuspensionsDataResponse EntMsgGetProgramSuspensionsDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            // if the list is empty fail the test //
            Assert.AreNotEqual(EntMsgGetProgramSuspensionsDataResponse.SuspensionsList.Count(), 0);
        }
    }
}
