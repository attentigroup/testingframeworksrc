﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;



#region API refs
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
#endregion
namespace AssertBlocksLib.ProgramActionsAsserts
{
    /// <summary>
    /// if the list is empty fail the test
    /// </summary>
    public class GetActionsListIsNotEmptyAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.EntMsgGetActionsListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgGetActionsListResponse EntMsgGetActionsListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreNotEqual(EntMsgGetActionsListResponse.Actions.Count(), 0);
        }
    }
}
