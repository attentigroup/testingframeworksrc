﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;

using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;

namespace AssertBlocksLib.ProgramActionsAsserts
{
    public class SendStartRangeRequestAssertBlock : SendDownloadRequestAssertBlock
    {

        [PropertyTest(EnumPropertyName.EntMsgSendStartRangeTestRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ProgramActions0.EntMsgSendStartRangeTestRequest EntMsgSendStartRangeTestRequest { get; set; }

        protected override int GetOffenderId()
        {
            return EntMsgSendStartRangeTestRequest.OffenderID;
        }
    }
}
