﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace AssertBlocksLib.Trails
{
    public class GetTrailAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Log.Debug($"Trail response for {PropertiesLogging.ToLog(GetTrailRequest)} return {GetTrailResponse.Points.Length} points");
            foreach (var point in GetTrailResponse.Points)
            {
                Assert.AreEqual(point.OffenderID, GetTrailRequest.OffenderID);
                Assert.IsTrue(point.Time >= GetTrailRequest.StartTime);
                Assert.IsTrue(point.Time <= GetTrailRequest.EndTime);
            }
        }
    }

    public class GetTrailAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails1.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails1.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var point in GetTrailResponse.Points)
            {
                Assert.AreEqual(point.OffenderID, GetTrailRequest.OffenderID);
                Assert.IsTrue(point.Date >= GetTrailRequest.StartTime);
                Assert.IsTrue(point.Date <= GetTrailRequest.EndTime);

            }
        }
    }

    public class GetTrailAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var point in GetTrailResponse.Points)
            {
                Assert.AreEqual(point.OffenderID, GetTrailRequest.OffenderID);
                Assert.IsTrue(point.Date >= GetTrailRequest.StartTime);
                Assert.IsTrue(point.Date <= GetTrailRequest.EndTime);

            }
        }
    }
}
