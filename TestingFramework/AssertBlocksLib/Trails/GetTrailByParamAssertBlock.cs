﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;

namespace AssertBlocksLib.Trails
{
    public class GetTrailByParamAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetTrailResponse GetTrailResponse { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int OffenderID { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyViolation { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndTime { get; set; }
        protected override void ExecuteBlock()
        {
            if (OffenderID != 0)
            {
                foreach(var point in GetTrailResponse.Points)
                {
                    Assert.AreEqual(OffenderID, point.OffenderID);
                }
            }

            if (OnlyViolation)
            {
                foreach (var point in GetTrailResponse.Points)
                {
                    Assert.AreEqual(OnlyViolation, point.IsPointViolation);
                }
            }
            else if (!OnlyViolation)
            {
                var noViolationPoints = GetTrailResponse.Points.Where(x => x.IsPointViolation == false);
                Assert.IsTrue(noViolationPoints.Count() > 0);
            }

            if(StartTime != null && EndTime != null)
            {
                foreach (var point in GetTrailResponse.Points)
                {
                    Assert.IsTrue(point.Time >= StartTime);
                    Assert.IsTrue(point.Time <= EndTime);
                }
            }
            
        }
    }
}
