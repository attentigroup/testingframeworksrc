﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;

namespace AssertBlocksLib.Trails
{
    public class GetGeographicLocationByParamAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetGeographicLocationsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetGeographicLocationsResponse GetGeographicLocationsResponse { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime { get; set; }

        [PropertyTest(EnumPropertyName.Limit, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Limit { get; set; }

        [PropertyTest(EnumPropertyName.OffenderRefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefID { get; set; }

        [PropertyTest(EnumPropertyName.OffendersIDs, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] OffendersIDs { get; set; }

        [PropertyTest(EnumPropertyName.PositionsIDs, EnumPropertyType.None, EnumPropertyModifier.None)]
        public long[] PositionsIDs { get; set; }

        [PropertyTest(EnumPropertyName.IncludeHistory, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IncludeHistory { get; set; }

        [PropertyTest(EnumPropertyName.ProgramTypes, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmProgramType[] ProgramTypes { get; set; }
        protected override void ExecuteBlock()
        {
            int length = 10;
            if (GetGeographicLocationsResponse.Locations.Length < length)
                length = GetGeographicLocationsResponse.Locations.Length;

            if (AgencyID != 0)
            {
                for (int i = 0; i < length; i++)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntNumericParameter() { Operator = TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EnmNumericOperator.Equal, Value = GetGeographicLocationsResponse.Locations[i].OffenderID }
                    });
                    Assert.AreEqual(AgencyID, getOffender.OffendersList[0].AgencyID);
                }
            }

            if (StartTime != null && EndTime != null)
            {
                for (int i = 0; i < length; i++)
                {
                    Assert.IsTrue(GetGeographicLocationsResponse.Locations[i].Time >= StartTime);
                    Assert.IsTrue(GetGeographicLocationsResponse.Locations[i].Time <= EndTime);
                }
            }

            if (Limit != 0)
            {
                Assert.IsTrue(GetGeographicLocationsResponse.Locations.Length <= Limit);
            }

            if (!string.IsNullOrEmpty(OffenderRefID))
            {
                for (int i = 0; i < length; i++)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntNumericParameter() { Operator = TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EnmNumericOperator.Equal, Value = GetGeographicLocationsResponse.Locations[i].OffenderID }
                    });
                    Assert.AreEqual(OffenderRefID, getOffender.OffendersList[0].RefID);
                }
            }

            if (OffendersIDs != null)
            {
                for (int i = 0; i < length; i++)
                {
                    Assert.AreEqual(OffendersIDs[0], GetGeographicLocationsResponse.Locations[i].OffenderID);
                }
            }

            if (PositionsIDs != null)
            {
                Assert.AreEqual(1, GetGeographicLocationsResponse.Locations.Length);
                Assert.AreEqual(PositionsIDs[0], GetGeographicLocationsResponse.Locations[0].ID);
            }

            //if (!IncludeHistory)
            //{
            //    for (int i = 0; i < 20; i++)
            //    {
            //        Assert.AreEqual(OffendersIDs[0], GetGeographicLocationsResponse.Locations[i].);
            //    }
            //}

            if (ProgramTypes != null)
            {
                for (int i = 0; i < length; i++)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntNumericParameter() { Operator = TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EnmNumericOperator.Equal, Value = GetGeographicLocationsResponse.Locations[i].OffenderID }
                    });
                    Assert.AreEqual(ProgramTypes[0].ToString(), getOffender.OffendersList[0].ProgramType.ToString());
                }
            }


        }
    }
}
