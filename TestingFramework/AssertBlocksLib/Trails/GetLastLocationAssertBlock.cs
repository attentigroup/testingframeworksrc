﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace AssertBlocksLib.Trails
{
    public class GetLastLocationAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var location in GetLastLocationResponse.LastLocationList)
            {
                if (GetLastLocationRequest.OnlyInUnknownLocation == true)
                    Assert.IsTrue(location.IsUnknown == Trails0.EnmStatus.Unknown);
                else
                    Assert.IsFalse(location.IsUnknown == Trails0.EnmStatus.Unknown);

                var getOffender = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
                {
                    OffenderID = new Offenders0.EntNumericParameter { Value = location.OffenderID, Operator = Offenders0.EnmNumericOperator.Equal }
                });
                Assert.AreEqual(getOffender.OffendersList[0].AgencyID, GetLastLocationRequest.AgencyID, $"Expected Agency ID: {getOffender.OffendersList[0].AgencyID}, Actual Agency ID: {GetLastLocationRequest.AgencyID}");
                Assert.AreEqual(getOffender.OffendersList[0].OfficerID, GetLastLocationRequest.OfficerID, $"Expected Officer ID: {getOffender.OffendersList[0].AgencyID}, Actual Officer ID: {GetLastLocationRequest.AgencyID}");

            }

        }

    }

    public class GetLastLocationAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails1.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails1.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var location in GetLastLocationResponse.LastLocationList)
            {
                if (GetLastLocationRequest.OnlyInUnknownLocation == true)
                    Assert.IsTrue(location.IsUnknown == Trails1.EnmStatus.Unknown);
                else
                    Assert.IsFalse(location.IsUnknown == Trails1.EnmStatus.Unknown);

                var getOffender = OffendersProxy_1.Instance.GetOffenders(new Offenders1.EntMsgGetOffendersListRequest()
                {
                    OffenderID = new Offenders1.EntNumericQueryParameter { Value = location.OffenderID, Operator = Offenders1.EnmSqlOperatorNum.Equal }
                });
                Assert.AreEqual(getOffender.OffendersList[0].AgencyID, GetLastLocationRequest.AgencyID);
                Assert.AreEqual(getOffender.OffendersList[0].OfficerID, GetLastLocationRequest.OfficerID);

            }

        }

    }

    public class GetLastLocationAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetLastLocationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EntMsgGetLastLocationRequest GetLastLocationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails2.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var location in GetLastLocationResponse.LastLocationList)
            {
                if (GetLastLocationRequest.OnlyInUnknownLocation == true)
                    Assert.IsTrue(location.IsUnknown == Trails2.EnmStatus.Unknown);
                else
                    Assert.IsFalse(location.IsUnknown == Trails2.EnmStatus.Unknown);

                var getOffender = OffendersProxy_2.Instance.GetOffenders(new Offenders2.EntMsgGetOffendersListRequest()
                {
                    OffenderID = new Offenders2.EntNumericQueryParameter { Value = location.OffenderID, Operator = Offenders2.EnmSqlOperatorNum.Equal }
                });
                Assert.AreEqual(getOffender.OffendersList[0].AgencyID, GetLastLocationRequest.AgencyID);
                Assert.AreEqual(getOffender.OffendersList[0].OfficerID, GetLastLocationRequest.OfficerID);

            }

        }

    }
}
