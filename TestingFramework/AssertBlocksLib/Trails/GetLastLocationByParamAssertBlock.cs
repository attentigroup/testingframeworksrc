﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;

namespace AssertBlocksLib.Trails
{
    public class GetLastLocationByParamAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetLastLocationResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetLastLocationResponse GetLastLocationResponse { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInUnknownLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInUnknownLocation { get; set; }

        [PropertyTest(EnumPropertyName.OnlyInViolation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool OnlyInViolation { get; set; }

        [PropertyTest(EnumPropertyName.ProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmLastLocationProgramType ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.OfficerID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OfficerID { get; set; }

        protected override void ExecuteBlock()
        {
            int length = 10;
            if (GetLastLocationResponse.LastLocationList.Length < length)
                length = GetLastLocationResponse.LastLocationList.Length;

            if (OnlyInUnknownLocation)
            {
                for (int i = 0; i < length; i++)
                {
                    Assert.AreEqual(Trails0.EnmStatus.Alert, GetLastLocationResponse.LastLocationList[i].IsUnknown);
                }
            }

            if (ProgramType == Trails0.EnmLastLocationProgramType.OnePiece)
            {
                for (int i = 0; i < length; i++)
                {
                    Assert.AreEqual("One_Piece_RF", GetLastLocationResponse.LastLocationList[i].ProgramType.ToString());
                }
            }
            else if (ProgramType == Trails0.EnmLastLocationProgramType.OnePiece)
            {
                for (int i = 0; i < length; i++)
                {
                    Assert.AreEqual("Two_Piece", GetLastLocationResponse.LastLocationList[i].ProgramType.ToString());
                }
            }

            if (OnlyInViolation)
            {
                for (int i = 0; i < length; i++)
                {
                    Assert.AreEqual(Trails0.EnmStatus.Alert, GetLastLocationResponse.LastLocationList[i].IsViolation);
                }
            }

            if(AgencyID != 0)
            {
                for (int i = 0; i < length; i++)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntNumericParameter() { Operator = TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EnmNumericOperator.Equal, Value = GetLastLocationResponse.LastLocationList[i].OffenderID }
                    });
                    Assert.AreEqual(AgencyID, getOffender.OffendersList[0].AgencyID);
                }
            }

            if (OfficerID != 0)
            {
                for (int i = 0; i < length; i++)
                {
                    var getOffender = OffendersProxy.Instance.GetOffenders(new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntMsgGetOffendersListRequest()
                    {
                        OffenderID = new TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EntNumericParameter() { Operator = TestingFramework.Proxies.EM.Interfaces.Offenders12_0.EnmNumericOperator.Equal, Value = GetLastLocationResponse.LastLocationList[i].OffenderID }
                    });
                    Assert.AreEqual(OfficerID, getOffender.OffendersList[0].OfficerID);
                }
            }
        }
    }
}
