﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
#endregion

namespace AssertBlocksLib.Trails
{
    public class GetGeographicLocationsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetGeographicLocationsRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetGeographicLocationsRequest GetGeographicLocationsRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGeographicLocationsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetGeographicLocationsResponse GetGeographicLocationsResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Log.Debug($"Geographic Locations response for {PropertiesLogging.ToLog(GetGeographicLocationsRequest)} return {GetGeographicLocationsResponse.Locations.Length} Locations");
            foreach (var position in GetGeographicLocationsResponse.Locations)
            {
                Assert.AreEqual(position.OffenderID, GetGeographicLocationsRequest.OffendersIDs[0]);
                Assert.IsTrue(position.Time >= GetGeographicLocationsRequest.StartTime);
                Assert.IsTrue(position.Time <= GetGeographicLocationsRequest.EndTime);
            }
        }
    }
}
