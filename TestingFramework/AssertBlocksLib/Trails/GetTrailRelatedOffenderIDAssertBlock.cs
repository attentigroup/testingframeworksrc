﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;

namespace AssertBlocksLib.Trails
{
    public class GetTrailRelatedOffenderIDAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetTrailRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetTrailRequest GetTrailRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetTrailResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetTrailResponse GetTrailResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var aggressorPoints = GetTrailResponse.Points.FirstOrDefault(x => x.OffenderID == GetTrailRequest.OffenderID);
            Assert.IsTrue(aggressorPoints != null);
            var victimPoints = GetTrailResponse.Points.FirstOrDefault(x => x.OffenderID == GetTrailRequest.RelatedOffenderID);
            Assert.IsTrue(victimPoints != null);
        }
    }
}
