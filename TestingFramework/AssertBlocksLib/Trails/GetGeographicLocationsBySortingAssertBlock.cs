﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;

namespace AssertBlocksLib.Trails
{
    public class GetGeographicLocationsBySortingAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetGeographicLocationsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Trails0.EntMsgGetGeographicLocationsResponse GetGeographicLocationsResponse { get; set; }

        [PropertyTest(EnumPropertyName.SortDirection, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public ListSortDirection SortDirection { get; set; }

        [PropertyTest(EnumPropertyName.SortField, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Trails0.EnmPositionsSortOptions SortField { get; set; }
        protected override void ExecuteBlock()
        {
            Trails0.EntPosition[] locations = new Trails0.EntPosition[GetGeographicLocationsResponse.Locations.Length];
            switch (SortField)
            {
                case Trails0.EnmPositionsSortOptions.OffenderID:
                    if (SortDirection == ListSortDirection.Ascending)
                        locations = GetGeographicLocationsResponse.Locations.OrderBy(x => x.OffenderID).ToArray();
                    else
                        locations = GetGeographicLocationsResponse.Locations.OrderByDescending(x => x.OffenderID).ToArray();
                    break;
                case Trails0.EnmPositionsSortOptions.PositionID:
                    if (SortDirection == ListSortDirection.Ascending)
                        locations = GetGeographicLocationsResponse.Locations.OrderBy(x => x.ID).ToArray();
                    else
                        locations = GetGeographicLocationsResponse.Locations.OrderByDescending(x => x.ID).ToArray();
                    break;
                case Trails0.EnmPositionsSortOptions.Time:
                    if (SortDirection == ListSortDirection.Ascending)
                        locations = GetGeographicLocationsResponse.Locations.OrderBy(x => x.Time).ToArray();
                    else
                        locations = GetGeographicLocationsResponse.Locations.OrderByDescending(x => x.Time).ToArray();
                    break;
                case Trails0.EnmPositionsSortOptions.Timestamp:
                    if (SortDirection == ListSortDirection.Ascending)
                        locations = GetGeographicLocationsResponse.Locations.OrderBy(x => x.Timestamp).ToArray();
                    else
                        locations = GetGeographicLocationsResponse.Locations.OrderByDescending(x => x.Timestamp).ToArray();
                    break;
                default:
                    break;
            }                                

            for (int i = 0; i < locations.Length; i++)
            {
                Assert.AreEqual(GetGeographicLocationsResponse.Locations[i].ID, locations[i].ID);
            }
        }
    }
}
