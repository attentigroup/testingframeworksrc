﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using TestingFramework.TestsInfraStructure.Implementation;

using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;


namespace AssertBlocksLib.ConfigurationAsserts
{
  public class GetGPSRuleConfigurationListIsDefaultAssertBlock : AssertBaseBlock
    {


        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgResetGPSRuleConfigurationRequest ResetGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListDefaultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            try
            {
           
                var DefaultRuleIDParameters = GetGPSRuleConfigurationListDefaultResponse.RuleConfigList.Single(x => x.ID == ResetGPSRuleConfigurationRequest.RuleID);
                var OffenderGraceTimeRuleID = GetGPSRuleConfigurationListResponse.RuleConfigList.Single(x => x.ID == ResetGPSRuleConfigurationRequest.RuleID);
                
                Assert.AreEqual(DefaultRuleIDParameters.GraceTime, OffenderGraceTimeRuleID.GraceTime);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + " GetGPSRuleConfigurationListResponse issue");
            }

        }
    }

    public class GetGPSRuleConfigurationListIsDefaultAssertBlock_1 : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgResetGPSRuleConfigurationnRequest ResetGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListDefaultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            try
            {
               
                var DefaultRuleIDParameters = GetGPSRuleConfigurationListDefaultResponse.RuleConfigList.Single(x => x.ID == ResetGPSRuleConfigurationRequest.RuleID);
                var OffenderGraceTimeRuleID = GetGPSRuleConfigurationListResponse.RuleConfigList.Single(x => x.ID == ResetGPSRuleConfigurationRequest.RuleID);
               
                Assert.AreEqual(DefaultRuleIDParameters.GraceTime, OffenderGraceTimeRuleID.GraceTime);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + " GetGPSRuleConfigurationListResponse issue");
            }

        }
    }


    public class GetGPSRuleConfigurationListIsDefaultAssertBlock_2 : AssertBaseBlock
    {


        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.ResetGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgResetGPSRuleConfigurationnRequest ResetGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListDefaultResponse { get; set; }
        protected override void ExecuteBlock()
        {
            try
            {
              
                var DefaultRuleIDParameters = GetGPSRuleConfigurationListDefaultResponse.RuleConfigList.Single(x => x.ID == ResetGPSRuleConfigurationRequest.RuleID);
                var OffenderGraceTimeRuleID = GetGPSRuleConfigurationListResponse.RuleConfigList.Single(x => x.ID == ResetGPSRuleConfigurationRequest.RuleID);

                Assert.AreEqual(DefaultRuleIDParameters.GraceTime, OffenderGraceTimeRuleID.GraceTime);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + " GetGPSRuleConfigurationListResponse issue");
            }

        }
    }

}
