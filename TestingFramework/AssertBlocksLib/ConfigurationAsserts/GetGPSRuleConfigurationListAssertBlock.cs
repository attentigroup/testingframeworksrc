﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using System.Linq;
#endregion

namespace AssertBlocksLib.ConfigurationAsserts
{
  public class GetGPSRuleConfigurationListAssertBlock : AssertBaseBlock
    {

        

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        protected override void ExecuteBlock()
        {

                var Response = GetGPSRuleConfigurationListResponse.RuleConfigList[0];
                Assert.IsTrue(Response.CloseEventID > 0);
 
        }
    }


    public class GetGPSRuleConfigurationListAssertBlock_1 : AssertBaseBlock
    {



        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        protected override void ExecuteBlock()
        {
                var Response = GetGPSRuleConfigurationListResponse.RuleConfigList[0];
                Assert.IsTrue(Response.CloseEventID > 0);

        }
    }

    public class GetGPSRuleConfigurationListAssertBlock_2 : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        protected override void ExecuteBlock()
        {
                var Response = GetGPSRuleConfigurationListResponse.RuleConfigList[0];
                Assert.IsTrue(Response.CloseEventID > 0);

        }
    }
}
