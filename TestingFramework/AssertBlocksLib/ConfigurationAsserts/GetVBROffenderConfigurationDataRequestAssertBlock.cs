﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;


namespace AssertBlocksLib.ConfigurationAsserts
{
    public class GetVBROffenderConfigurationDataRequestAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }



        protected override void ExecuteBlock()
        {
            var getOffenderConfigurationDataResponse = GetOffenderConfigurationDataResponse.ConfigurationData;
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("CSDPriority").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.CSDPriority);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("GPRSPriority").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.GPRSPriority);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("GraceAfterTimeFrameEnd").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("GraceAfterTimeFrameStart").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("GraceBeforeTimeFrameEnd").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("GraceBeforeTimeFrameStart").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart); 
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("MinimumTimeBetweenAlcoholTests").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.MinimumTimeBetweenAlcoholTests);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("NumberOfRings").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.NumberOfRings);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("SupportCSD").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.SupportCSD);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("SupportGPRS").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.SupportGPRS);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("SupportSMSCallback").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("TimeBetweenUploads").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads);
            Assert.AreEqual(getOffenderConfigurationDataResponse.GetType().GetProperty("TransmitterDisappearTime").GetValue(getOffenderConfigurationDataResponse, null), UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime);

        }
    }
}