﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;

using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;


namespace AssertBlocksLib.ConfigurationAsserts
{
    public class GetRFOffenderConfigurationDataRequestAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }



        protected override void ExecuteBlock()
        {
            var configDataRf = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration0.EntConfigurationRF;
            
            Assert.AreEqual(configDataRf.CSDPriority, UpdateRFOffenderConfigurationDataRequest.CSDPriority);
            Assert.AreEqual(configDataRf.CallerIDRequired, UpdateRFOffenderConfigurationDataRequest.CallerIDRequired);
            Assert.AreEqual(configDataRf.CommunicationMethodForVoiceTests, UpdateRFOffenderConfigurationDataRequest.CommunicationMethodForVoiceTests);
            Assert.AreEqual(configDataRf.GPRSPriority, UpdateRFOffenderConfigurationDataRequest.GPRSPriority);
            Assert.AreEqual(configDataRf.GraceAfterTimeFrameEnd, UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd);
            Assert.AreEqual(configDataRf.GraceAfterTimeFrameStart, UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart);
            Assert.AreEqual(configDataRf.GraceAfterVoiceTestEnd, UpdateRFOffenderConfigurationDataRequest.GraceAfterVoiceTestEnd);
            Assert.AreEqual(configDataRf.GraceBeforeTimeFrameEnd, UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd);
            Assert.AreEqual(configDataRf.GraceBeforeTimeFrameStart, UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart);
            Assert.AreEqual(configDataRf.GraceBeforeVoiceTestStart, UpdateRFOffenderConfigurationDataRequest.GraceBeforeVoiceTestStart);
            Assert.AreEqual(configDataRf.HangupOutOfSchedule, UpdateRFOffenderConfigurationDataRequest.HangupOutOfSchedule);
            Assert.AreEqual(configDataRf.IsKosher, UpdateRFOffenderConfigurationDataRequest.IsKosher);
            Assert.AreEqual(configDataRf.LandlinePriority, UpdateRFOffenderConfigurationDataRequest.LandlinePriority);
            Assert.AreEqual(configDataRf.NumberOfPhrases, UpdateRFOffenderConfigurationDataRequest.NumberOfPhrases);
            Assert.AreEqual(configDataRf.NumberOfRings, UpdateRFOffenderConfigurationDataRequest.NumberOfRings);
            Assert.AreEqual(configDataRf.OfficerPassword, UpdateRFOffenderConfigurationDataRequest.OfficerPassword);
            Assert.AreEqual(configDataRf.OfficerScreenActive, UpdateRFOffenderConfigurationDataRequest.OfficerScreenActive);
            Assert.AreEqual(configDataRf.ReceiverRange, UpdateRFOffenderConfigurationDataRequest.ReceiverRange);
            Assert.AreEqual(configDataRf.RingVolume, UpdateRFOffenderConfigurationDataRequest.RingVolume);
            Assert.AreEqual(configDataRf.SupportCSD, UpdateRFOffenderConfigurationDataRequest.SupportCSD);
            Assert.AreEqual(configDataRf.SupportGPRS, UpdateRFOffenderConfigurationDataRequest.SupportGPRS);
            Assert.AreEqual(configDataRf.SupportLandline, UpdateRFOffenderConfigurationDataRequest.SupportLandline);
            Assert.AreEqual(configDataRf.SupportSMSCallback, UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback);
            Assert.AreEqual(configDataRf.TimeBetweenUploads, UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads);
            Assert.AreEqual(configDataRf.TransmitterDisappearTime, UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime);
            Assert.AreEqual(configDataRf.VerifyAfterEnroll, UpdateRFOffenderConfigurationDataRequest.VerifyAfterEnroll);
        }
    }

    public class GetRFOffenderConfigurationDataRequestAssertBlock_1 : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }



        protected override void ExecuteBlock()
        { 
            var configDataRf = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration1.EntConfigurationRF;

            Assert.AreEqual(configDataRf.CSDPriority, UpdateRFOffenderConfigurationDataRequest.CSDPriority);
            Assert.AreEqual(configDataRf.CallerIDRequired, UpdateRFOffenderConfigurationDataRequest.CallerIDRequired);
            Assert.AreEqual(configDataRf.CommunicationMethodForVoiceTests, UpdateRFOffenderConfigurationDataRequest.CommunicationMethodForVoiceTests);
            Assert.AreEqual(configDataRf.GPRSPriority, UpdateRFOffenderConfigurationDataRequest.GPRSPriority);
            Assert.AreEqual(configDataRf.GraceAfterTimeFrameEnd, UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd);
            Assert.AreEqual(configDataRf.GraceAfterTimeFrameStart, UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart);
            Assert.AreEqual(configDataRf.GraceAfterVoiceTestEnd, UpdateRFOffenderConfigurationDataRequest.GraceAfterVoiceTestEnd);
            Assert.AreEqual(configDataRf.GraceBeforeTimeFrameEnd, UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd);
            Assert.AreEqual(configDataRf.GraceBeforeTimeFrameStart, UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart);
            Assert.AreEqual(configDataRf.GraceBeforeVoiceTestStart, UpdateRFOffenderConfigurationDataRequest.GraceBeforeVoiceTestStart);
            Assert.AreEqual(configDataRf.HangupOutOfSchedule, UpdateRFOffenderConfigurationDataRequest.HangupOutOfSchedule);
            Assert.AreEqual(configDataRf.IsKosher, UpdateRFOffenderConfigurationDataRequest.IsKosher);
            Assert.AreEqual(configDataRf.LandlinePriority, UpdateRFOffenderConfigurationDataRequest.LandlinePriority);
            Assert.AreEqual(configDataRf.NumberOfPhrases, UpdateRFOffenderConfigurationDataRequest.NumberOfPhrases);
            Assert.AreEqual(configDataRf.NumberOfRings, UpdateRFOffenderConfigurationDataRequest.NumberOfRings);
            Assert.AreEqual(configDataRf.OfficerPassword, UpdateRFOffenderConfigurationDataRequest.OfficerPassword);
            Assert.AreEqual(configDataRf.OfficerScreenActive, UpdateRFOffenderConfigurationDataRequest.OfficerScreenActive);
            Assert.AreEqual(configDataRf.ReceiverRange, UpdateRFOffenderConfigurationDataRequest.ReceiverRange);
            Assert.AreEqual(configDataRf.RingVolume, UpdateRFOffenderConfigurationDataRequest.RingVolume);
            Assert.AreEqual(configDataRf.SupportCSD, UpdateRFOffenderConfigurationDataRequest.SupportCSD);
            Assert.AreEqual(configDataRf.SupportGPRS, UpdateRFOffenderConfigurationDataRequest.SupportGPRS);
            Assert.AreEqual(configDataRf.SupportLandline, UpdateRFOffenderConfigurationDataRequest.SupportLandline);
            Assert.AreEqual(configDataRf.SupportSMSCallback, UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback);
            Assert.AreEqual(configDataRf.TimeBetweenUploads, UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads);
            Assert.AreEqual(configDataRf.TransmitterDisappearTime, UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime);
            Assert.AreEqual(configDataRf.VerifyAfterEnroll, UpdateRFOffenderConfigurationDataRequest.VerifyAfterEnroll);
        }
    }

    public class GetRFOffenderConfigurationDataRequestAssertBlock_2 : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.UpdateRFOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateRFOffenderConfigurationDataRequest UpdateRFOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }



        protected override void ExecuteBlock()
        {
            var configDataRf = GetOffenderConfigurationDataResponse.ConfigurationData as Configuration2.EntConfigurationRF;

            Assert.AreEqual(configDataRf.CSDPriority, UpdateRFOffenderConfigurationDataRequest.CSDPriority);
            Assert.AreEqual(configDataRf.CallerIDRequired, UpdateRFOffenderConfigurationDataRequest.CallerIDRequired);
            Assert.AreEqual(configDataRf.CommunicationMethodForVoiceTests, UpdateRFOffenderConfigurationDataRequest.CommunicationMethodForVoiceTests);
            Assert.AreEqual(configDataRf.GPRSPriority, UpdateRFOffenderConfigurationDataRequest.GPRSPriority);
            Assert.AreEqual(configDataRf.GraceAfterTimeFrameEnd, UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameEnd);
            Assert.AreEqual(configDataRf.GraceAfterTimeFrameStart, UpdateRFOffenderConfigurationDataRequest.GraceAfterTimeFrameStart);
            Assert.AreEqual(configDataRf.GraceAfterVoiceTestEnd, UpdateRFOffenderConfigurationDataRequest.GraceAfterVoiceTestEnd);
            Assert.AreEqual(configDataRf.GraceBeforeTimeFrameEnd, UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameEnd);
            Assert.AreEqual(configDataRf.GraceBeforeTimeFrameStart, UpdateRFOffenderConfigurationDataRequest.GraceBeforeTimeFrameStart);
            Assert.AreEqual(configDataRf.GraceBeforeVoiceTestStart, UpdateRFOffenderConfigurationDataRequest.GraceBeforeVoiceTestStart);
            Assert.AreEqual(configDataRf.HangupOutOfSchedule, UpdateRFOffenderConfigurationDataRequest.HangupOutOfSchedule);
            Assert.AreEqual(configDataRf.IsKosher, UpdateRFOffenderConfigurationDataRequest.IsKosher);
            Assert.AreEqual(configDataRf.LandlinePriority, UpdateRFOffenderConfigurationDataRequest.LandlinePriority);
            Assert.AreEqual(configDataRf.NumberOfPhrases, UpdateRFOffenderConfigurationDataRequest.NumberOfPhrases);
            Assert.AreEqual(configDataRf.NumberOfRings, UpdateRFOffenderConfigurationDataRequest.NumberOfRings);
            Assert.AreEqual(configDataRf.OfficerPassword, UpdateRFOffenderConfigurationDataRequest.OfficerPassword);
            Assert.AreEqual(configDataRf.OfficerScreenActive, UpdateRFOffenderConfigurationDataRequest.OfficerScreenActive);
            Assert.AreEqual(configDataRf.ReceiverRange, UpdateRFOffenderConfigurationDataRequest.ReceiverRange);
            Assert.AreEqual(configDataRf.RingVolume, UpdateRFOffenderConfigurationDataRequest.RingVolume);
            Assert.AreEqual(configDataRf.SupportCSD, UpdateRFOffenderConfigurationDataRequest.SupportCSD);
            Assert.AreEqual(configDataRf.SupportGPRS, UpdateRFOffenderConfigurationDataRequest.SupportGPRS);
            Assert.AreEqual(configDataRf.SupportLandline, UpdateRFOffenderConfigurationDataRequest.SupportLandline);
            Assert.AreEqual(configDataRf.SupportSMSCallback, UpdateRFOffenderConfigurationDataRequest.SupportSMSCallback);
            Assert.AreEqual(configDataRf.TimeBetweenUploads, UpdateRFOffenderConfigurationDataRequest.TimeBetweenUploads);
            Assert.AreEqual(configDataRf.TransmitterDisappearTime, UpdateRFOffenderConfigurationDataRequest.TransmitterDisappearTime);
            Assert.AreEqual(configDataRf.VerifyAfterEnroll, UpdateRFOffenderConfigurationDataRequest.VerifyAfterEnroll);
        }
    }
}
