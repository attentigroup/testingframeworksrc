﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using System.Linq;
#endregion

namespace AssertBlocksLib.ConfigurationAsserts
{
    public class UpdateHandlingProcedureAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoEmailNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoEmailNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoFaxNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoFaxNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoPagerNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoPagerNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.Comments, GetHandlingProcedureResponse.HandlingProcedure.Comments);
            Assert.AreEqual(UpdateHandlingProcedureRequest.InheritAgency, GetHandlingProcedureResponse.HandlingProcedure.InheritAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.InheritOfficer, GetHandlingProcedureResponse.HandlingProcedure.InheritOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.ViolationHandling, GetHandlingProcedureResponse.HandlingProcedure.ViolationHandling);

        }
    }

    public class UpdateHandlingProcedureAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoEmailNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoEmailNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoFaxNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoFaxNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoPagerNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoPagerNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.Comments, GetHandlingProcedureResponse.HandlingProcedure.Comments);
            Assert.AreEqual(UpdateHandlingProcedureRequest.InheritAgency, GetHandlingProcedureResponse.HandlingProcedure.InheritAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.InheritOfficer, GetHandlingProcedureResponse.HandlingProcedure.InheritOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.ViolationHandling, GetHandlingProcedureResponse.HandlingProcedure.ViolationHandling);

        }
    }

    public class UpdateHandlingProcedureAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateHandlingProcedureRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateHandlingProcedureRequest UpdateHandlingProcedureRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetHandlingProcedureResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetHandlingProcedureResponse GetHandlingProcedureResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoEmailNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoEmailNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoFaxNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoFaxNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoPagerNotificationToAgency, GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.AutoPagerNotificationToOfficer, GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.Comments, GetHandlingProcedureResponse.HandlingProcedure.Comments);
            Assert.AreEqual(UpdateHandlingProcedureRequest.InheritAgency, GetHandlingProcedureResponse.HandlingProcedure.InheritAgency);
            Assert.AreEqual(UpdateHandlingProcedureRequest.InheritOfficer, GetHandlingProcedureResponse.HandlingProcedure.InheritOfficer);
            Assert.AreEqual(UpdateHandlingProcedureRequest.ViolationHandling, GetHandlingProcedureResponse.HandlingProcedure.ViolationHandling);

        }
    }
}
