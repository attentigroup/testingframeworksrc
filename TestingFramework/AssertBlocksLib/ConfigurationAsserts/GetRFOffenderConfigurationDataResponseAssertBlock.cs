﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using TestingFramework.Proxies.API.Configuration;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using TestingFramework.TestsInfraStructure.Implementation;

using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
namespace AssertBlocksLib.ConfigurationAsserts
{
    public class GetRFOffenderConfigurationDataResponseAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

       

        protected override void ExecuteBlock()
        {
            var getOffenderConfigurationDataResponse = GetOffenderConfigurationDataResponse.ConfigurationData;
            var NumberOfRings = getOffenderConfigurationDataResponse.GetType().GetProperty("NumberOfRings").GetValue(getOffenderConfigurationDataResponse);

            Assert.IsTrue(NumberOfRings != null);

           

        }
    }
}
