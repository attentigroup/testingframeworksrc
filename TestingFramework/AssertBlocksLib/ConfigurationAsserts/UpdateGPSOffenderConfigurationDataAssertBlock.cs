﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;

namespace AssertBlocksLib.ConfigurationAsserts
{
    public class UpdateGPSOffenderConfigurationDataAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateGPSOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateGPSOffenderConfigurationDataRequest UpdateGPSOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataDefaultResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            bool areEquals = true;
            var defaultConfigurationData = GetOffenderConfigurationDataDefaultResponse.ConfigurationData;
            var configurationData = GetOffenderConfigurationDataResponse.ConfigurationData;
            var defaultConfigurationDataProperties = defaultConfigurationData.GetType().GetProperties();
            var configurationDataProperties= configurationData.GetType().GetProperties();
            for(int i = 0; i < defaultConfigurationDataProperties.Length; i++)
            {
                if (defaultConfigurationDataProperties[i].GetValue(defaultConfigurationData, null) == configurationDataProperties[i].GetValue(configurationData, null))
                    continue;
                else
                {
                    areEquals = false;
                    break;
                }     
                    
            }
            Assert.IsFalse(areEquals);

        }
    }

    public class UpdateGPSOffenderConfigurationDataAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataDefaultResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            bool areEquals = true;
            var defaultConfigurationData = GetOffenderConfigurationDataDefaultResponse.ConfigurationData;
            var configurationData = GetOffenderConfigurationDataResponse.ConfigurationData;
            var defaultConfigurationDataProperties = defaultConfigurationData.GetType().GetProperties();
            var configurationDataProperties = configurationData.GetType().GetProperties();
            for (int i = 0; i < defaultConfigurationDataProperties.Length; i++)
            {
                if (defaultConfigurationDataProperties[i].GetValue(defaultConfigurationData, null) == configurationDataProperties[i].GetValue(configurationData, null))
                    continue;
                else
                {
                    areEquals = false;
                    break;
                }

            }
            Assert.IsFalse(areEquals);
        }
    }

    public class UpdateGPSOffenderConfigurationDataAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetOffenderConfigurationDataRequest GetOffenderConfigurationDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataDefaultResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataDefaultResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            bool areEquals = true;
            var defaultConfigurationData = GetOffenderConfigurationDataDefaultResponse.ConfigurationData;
            var configurationData = GetOffenderConfigurationDataResponse.ConfigurationData;
            var defaultConfigurationDataProperties = defaultConfigurationData.GetType().GetProperties();
            var configurationDataProperties = configurationData.GetType().GetProperties();
            for (int i = 0; i < defaultConfigurationDataProperties.Length; i++)
            {
                if (defaultConfigurationDataProperties[i].GetValue(defaultConfigurationData, null) == configurationDataProperties[i].GetValue(configurationData, null))
                    continue;
                else
                {
                    areEquals = false;
                    break;
                }

            }
            Assert.IsFalse(areEquals);
        }
    }
}