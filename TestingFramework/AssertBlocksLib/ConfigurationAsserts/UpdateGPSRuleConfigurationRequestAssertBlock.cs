﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using System.Linq;
#endregion

namespace AssertBlocksLib.ConfigurationAsserts
{
  public class UpdateGPSRuleConfigurationRequestAssertBlock : AssertBaseBlock
    {


        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }
      
        protected override void ExecuteBlock()
        {
            var GraceTime = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).GraceTime;
            var IsGenerateLED = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).IsGenerateLED;
            var IsGenerateVibration = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).IsGenerateVibration;
            var IsGenerateWarningMessage = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).IsGenerateWarningMessage;
            var IsImmediateUpload = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).IsImmediateUpload;
            var WarningMessage = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).WarningMessage;

            Assert.AreEqual(GraceTime, UpdateGPSRuleConfigurationRequest.GraceTime);
            Assert.AreEqual(IsGenerateLED, UpdateGPSRuleConfigurationRequest.IsGenerateLED);
            Assert.AreEqual(IsGenerateVibration, UpdateGPSRuleConfigurationRequest.IsGenerateVibration);
            Assert.AreEqual(IsGenerateWarningMessage, UpdateGPSRuleConfigurationRequest.IsGenerateWarningMessage);
            Assert.AreEqual(IsImmediateUpload, UpdateGPSRuleConfigurationRequest.IsImmediateUpload);
            Assert.AreEqual(WarningMessage, UpdateGPSRuleConfigurationRequest.WarningMessage);

        }

        }
    }


    public class UpdateGPSRuleConfigurationRequestAssertBlock_1 : AssertBaseBlock
    {


        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration1.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        protected override void ExecuteBlock()
        { 
             var GraceTime = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).GraceTime;

             Assert.AreEqual(GraceTime, UpdateGPSRuleConfigurationRequest.GraceTime);

        }
    }

    public class UpdateGPSRuleConfigurationRequestAssertBlock_2 : AssertBaseBlock
    {


        [PropertyTest(EnumPropertyName.UpdateGPSRuleConfigurationRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgUpdateGPSRuleConfigurationRequest UpdateGPSRuleConfigurationRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration2.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        protected override void ExecuteBlock()
        {
                var GraceTime = GetGPSRuleConfigurationListResponse.RuleConfigList.First(i => i.ID == UpdateGPSRuleConfigurationRequest.RuleID).GraceTime;

                Assert.AreEqual(GraceTime, UpdateGPSRuleConfigurationRequest.GraceTime);

    }
}
