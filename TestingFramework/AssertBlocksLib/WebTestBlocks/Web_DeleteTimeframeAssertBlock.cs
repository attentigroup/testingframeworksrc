﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;


using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.WebTestBlocks
{
    public abstract class Web_DeleteTimeframeAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.TimeframeID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int TimeFrameID { get; set; }

        protected Schedule0.EntTimeFrame[] TimeFrames4Validate { get; set; }


        protected override void ExecuteBlock()
        {
            Validate(TimeFrames4Validate);
        }

        public void Validate(Schedule0.EntTimeFrame[] timeFrames)
        {
            var timeframe = timeFrames.FirstOrDefault(x => x.ID == TimeFrameID);
            Assert.IsTrue(timeframe == null);
        }

    }
    
}
