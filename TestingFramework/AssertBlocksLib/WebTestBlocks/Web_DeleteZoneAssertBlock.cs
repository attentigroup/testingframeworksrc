﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_DeleteZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }

        [PropertyTest(EnumPropertyName.ZoneID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ZoneID { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetZonesByEntityIDResponse.ZoneList != null);
            var Zone = GetZonesByEntityIDResponse.ZoneList.FirstOrDefault(x => x.ID == ZoneID);
            Assert.IsTrue(Zone == null, $"Zone: {ZoneID} still exists!");

        }

        
    }
}
