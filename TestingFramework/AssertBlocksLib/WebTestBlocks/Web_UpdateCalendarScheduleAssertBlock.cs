﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_UpdateCalendarScheduleAssertBlock : Web_UpdateScheduleAssertBlock
    {
        protected override void ExecuteBlock()
        {
            TimeFrames4Validate = GetScheduleResponse.Schedule.CalendarSchedule;
            base.ExecuteBlock();
        }

    }
}
