﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_IsSecurityConfigElementShownAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.isElementShown, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<SecurityConfigObject> isElementShown { get; set; }


        [PropertyTest(EnumPropertyName.ModifyStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnmModify ModifyStatus { get; set; }

        protected override void ExecuteBlock()
        {
            if (isElementShown.Count() < 1)
                Assert.Fail("No results available!");
            bool ExpectedValue = (ModifyStatus == EnmModify.Visible) ? true : false;

            var failedList = new List<SecurityConfigObject>();

            foreach (var item in isElementShown)
            {
                if (item.ActualVisibleEnabledExposed != ExpectedValue || (item.Locator == By.Name("No selector fit") && ModifyStatus != EnmModify.Invisible))
                {
                    failedList.Add(item);
                }
                Logger.WriteLine($"ResouceID: {item.ResourceId}; Expected visibiity: {ModifyStatus.ToString()}; Actual is shown: {item.ActualVisibleEnabledExposed}; Locator: {item.Locator}");
            }

            Logger.WriteLine($"{failedList.Count} security elements failed out of {isElementShown.Count}");

            if (failedList.Any())
            {
                Logger.TakeScreenshot();
                StringBuilder sb = new StringBuilder();
                foreach (var failedItem in failedList)
                {
                    sb.Append($"{failedItem.ResourceId}, ");
                }
                //Logger.WriteLine($"{failedList.Count} security elements failed out of {isElementShown.Count}");
                Assert.Fail($"The followings Resource Ids failed to be shown {ModifyStatus.ToString()} as expected: {sb.ToString()}");
            }
        }
    }
}

