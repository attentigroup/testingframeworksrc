﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_EventsAddFilterAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EventTime { get; set; }

        [PropertyTest(EnumPropertyName.Severity, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Severity { get; set; }

        [PropertyTest(EnumPropertyName.ID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int? eventID { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string EventMessage { get; set; }

        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public CurrentStatusTableRow[] EventsArr { get; set; }

        protected override void ExecuteBlock()
        {
            if (EventsArr == null || EventsArr.Length == 0)
                throw new Exception("No Events From Current Status Table!");

            if (!string.IsNullOrEmpty(Severity))
            {
                foreach (var eventRow in EventsArr)
                {
                    Assert.AreEqual(eventRow.Severity, Severity);
                }
            }
            else if (eventID != null)
            {
                foreach (var eventRow in EventsArr)
                {
                    Assert.IsTrue(int.Parse(eventRow.EventId) <= eventID);
                }
            }
            else if (EventTime != null)
            {
                foreach (var eventRow in EventsArr)
                {
                    var rowTime = DateTime.ParseExact(eventRow.EventTime, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    Assert.IsTrue(rowTime < EventTime);
                }
            }
            else if (!string.IsNullOrEmpty(EventMessage))
            {
                var foundEvent = false;

                foreach (var eventRow in EventsArr)
                {
                    if (eventRow.Message.Equals(EventMessage))
                        {
                            foundEvent = true;
                            break;
                        }

                }
                
                Assert.IsTrue(foundEvent);
            }

        }
    }
}
