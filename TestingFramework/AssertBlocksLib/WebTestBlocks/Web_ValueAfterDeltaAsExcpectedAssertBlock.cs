﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.WebTestBlocks
{
    /// <summary>
    /// Generic compariosion between to integers
    /// </summary>
    public class Web_ValueAfterDeltaAsExcpectedAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.BeforeDelta, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int BeforeDelta { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int Expected { get; set; }

        [PropertyTest(EnumPropertyName.Delta, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int Delta { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public String Message { get; set; }


        protected override void ExecuteBlock()
        {
            var AfterDelta = BeforeDelta + Delta;
            Assert.AreEqual(AfterDelta, Expected, $"{Message}. Expected= {Expected} Actual= {AfterDelta}");
        }
    }
}
