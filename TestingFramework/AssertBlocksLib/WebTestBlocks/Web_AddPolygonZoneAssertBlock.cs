﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_AddPolygonZoneAssertBlock : Web_AddZoneAssertBlock
    {
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            Assert.IsTrue(IsZoneType(typeof(Zones0.EntPolygon)));
        }
    }
}
