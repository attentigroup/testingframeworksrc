﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_OffenderPictureAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.Displayed, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool Displayed { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(Displayed);
        }
    }
}
