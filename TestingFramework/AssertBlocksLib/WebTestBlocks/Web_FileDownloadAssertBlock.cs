﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
  public  class Web_FileDownloadAssertBlock : AssertBaseBlock
    {
      
        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool Expected { get; set; }

        [PropertyTest(EnumPropertyName.ReportName, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string ReportName { get; set; }

        [PropertyTest(EnumPropertyName.GenerateReportDateTime, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public DateTime GenerateReportDateTime { get; set; }

        protected override void ExecuteBlock()
        {
            bool exist = false;

            string userProfile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string downloadDirectory = Path.Combine(userProfile, "Downloads");
            var fileNameSearchPattern = $"*{ReportName }*";
            string[] filePaths = Directory.GetFiles(downloadDirectory, fileNameSearchPattern);

            if (filePaths.Count() > 0)
            {
                foreach (string file in filePaths)
                {
                    FileInfo thisFile = new FileInfo(file);
                    //Check the file that are downloaded in the last 4 minutes
                    if (thisFile.LastWriteTime >= GenerateReportDateTime)
                    {
                        exist = true;
                        File.Delete(file);
                    }

                }
                Assert.IsTrue(exist == Expected, "Check if the file was exist under the download path");
            }
	        else
            {
                throw new Exception("Download Folder is empty - Report"+ ReportName +" does not exist - process failed");
                Log.Info($" *** OpertationalReports Directory: {downloadDirectory} ***");
                Console.WriteLine($" *** OpertationalReports Directory: {downloadDirectory} : {downloadDirectory} ***");
            }
        }
    }
}
