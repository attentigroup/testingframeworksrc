﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_AddEquipmentAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.Equipment, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment Equipment { get; set; }
        protected override void ExecuteBlock()
        {
            var getEquipment = EquipmentProxy.Instance.GetEquipmentList(new EntMsgGetEquipmentListRequest()
            {
                SerialNumber = Equipment.SerialNumber,
            });
            var equipment = getEquipment.EquipmentList[0];

            Assert.IsTrue(getEquipment.EquipmentList.Length > 0);
            Assert.IsTrue(equipment.AgencyID != null);
            Assert.AreEqual(equipment.SerialNumber, Equipment.SerialNumber);
            Assert.IsTrue(equipment.OffenderID == null);

            var getCellularData = EquipmentProxy.Instance.GetCellularData(new EntMsgGetCellularDataRequest()
            {
                SerialNumber = Equipment.SerialNumber
            });
            if (getCellularData.CellularData.Count() > 0)
            {
                var cellularData = getCellularData.CellularData[0];

                Assert.IsTrue(getCellularData.CellularData.Count() > 0);
                Assert.AreEqual(Equipment.SIMDataNumber, cellularData.DataPhoneNumber);
                Assert.AreEqual(Equipment.SIMVoiceNumber, cellularData.VoicePhoneNumber);
            }

        }
    }
}


