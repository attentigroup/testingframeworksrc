﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_OffenderHeaderDetailsLabelAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.OffenderLabel, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string OffenderLabel { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedOffenderLabel, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ExpectedOffenderLabel { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(OffenderLabel.Contains(ExpectedOffenderLabel));
        }
    }
}