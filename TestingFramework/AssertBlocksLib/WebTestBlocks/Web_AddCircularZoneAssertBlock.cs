﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_AddCircularZoneAssertBlock : Web_AddZoneAssertBlock
    {
        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RealRadious { get; set; }

        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int BufferRadious { get; set; }
        protected override void ExecuteBlock()
        {
            base.ExecuteBlock();
            Assert.IsTrue(IsZoneType(typeof(Zones0.EntCircular)));
            var circularZone = (Zones0.EntCircular)Zone;
            Assert.AreEqual(circularZone.BufferRadius, BufferRadious, $"API buffer radious: {circularZone.BufferRadius}, Web buffer radious: {BufferRadious}");
            Assert.AreEqual(circularZone.RealRadius, RealRadious, $"API real radious: {circularZone.RealRadius}, Web real radious: {RealRadious}");
        }
    }
}
