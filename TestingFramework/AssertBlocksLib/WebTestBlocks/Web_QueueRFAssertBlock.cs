﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_QueueRFAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.FilterName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<QueueRow> QueueList { get; set; }

        [PropertyTest(EnumPropertyName.IsElementExist, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsExpectedInQueue { get; set; }

        //Used here with Receiver SN value
        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefId { get; set; }

        protected override void ExecuteBlock()
        {
            var queueRowRF = QueueList.FirstOrDefault(x => x.ReceiverSerialNumber == OffenderRefId);

            if (queueRowRF != null)
            {
                WebTests.SeleniumWrapper.Logger.WriteLine($"Receiver Serial Number in Queue is {queueRowRF.ReceiverSerialNumber}");
                Assert.AreEqual(IsExpectedInQueue, queueRowRF != null, $"Queue row with receiver s/n {OffenderRefId} not found");
            }
            else
                throw new Exception("No elements found in queue rows");
        }
    }
}
