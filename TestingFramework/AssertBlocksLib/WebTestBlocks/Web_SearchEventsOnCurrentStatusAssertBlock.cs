﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.WebTestBlocks
{
   public class Web_SearchEventsOnCurrentStatusAssertBlock : Web_SearchEventsOnMonitorScreenAssertBlock
    {
        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.None)]
        public CurrentStatusTableRow CurrentStatusTableRow { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetEventsResponse.EventsList[0].ID, int.Parse(CurrentStatusTableRow.EventId));
        }
    }
}

