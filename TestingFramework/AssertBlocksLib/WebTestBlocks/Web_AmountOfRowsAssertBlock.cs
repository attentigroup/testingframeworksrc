﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_AmountOfRowsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.WebAmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int WebAmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfEvents, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public int APIAmountOfRows { get; set; }

        [PropertyTest(EnumPropertyName.AmountOfEvents2, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int APIAmountOfRows2 { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.None, EnumPropertyModifier.None)]
        public String Message { get; set; }

        public Web_AmountOfRowsAssertBlock()
        {
            APIAmountOfRows2 = 0;
        }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(WebAmountOfRows, APIAmountOfRows+ APIAmountOfRows2, $"{Message}. Web= {WebAmountOfRows} API= {APIAmountOfRows + APIAmountOfRows2}");
        }
    }
}
