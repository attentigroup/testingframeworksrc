﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_VerifyDVLinkAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.isDVLinkWorks, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool isDVLinkWorks { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(isDVLinkWorks);
        }
    }
}
