﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_FilterEventAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? StartTime{ get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime? EndTime{ get; set; }

        [PropertyTest(EnumPropertyName.Recent, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Recent { get; set; }

        [PropertyTest(EnumPropertyName.Date, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string[] DatesArr { get; set; }

        protected override void ExecuteBlock()
        {
            if(StartTime!=null && EndTime != null)
            {
                foreach(var date in DatesArr)
                {
                    DateTime time = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    Assert.IsTrue(time >= StartTime);
                    Assert.IsTrue(time <= EndTime);
                }                
            }
            else if (!string.IsNullOrEmpty(Recent))
            {
                foreach (var date in DatesArr)
                {
                    DateTime time = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    StartTime = DateTime.Now.AddHours(-(int.Parse(Recent)));
                    EndTime = DateTime.Now.AddMinutes(5);
                    Assert.IsTrue(time >= StartTime);
                    Assert.IsTrue(time <= EndTime);
                }
            }

        }
    }
}
