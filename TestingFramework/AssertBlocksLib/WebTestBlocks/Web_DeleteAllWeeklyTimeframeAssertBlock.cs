﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_DeleteAllWeeklyTimeframeAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetScheduleResponse.Schedule.WeeklySchedule != null);
            Assert.IsTrue(GetScheduleResponse.Schedule.WeeklySchedule.Length == 0);
        }

    }
}
