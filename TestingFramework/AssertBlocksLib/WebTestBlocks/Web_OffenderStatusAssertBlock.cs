﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_OffenderStatusAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.OffenderStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string OffenderStatus { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedOffenderStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ExpectedOffenderStatus { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(OffenderStatus.Contains(ExpectedOffenderStatus));
        }
    }
}
