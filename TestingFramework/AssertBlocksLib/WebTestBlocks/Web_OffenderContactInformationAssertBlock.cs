﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_OffenderContactInformationAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.OffenderContact, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string OffenderContact { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedOffenderContactPhone, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ExpectedOffenderContactPhone { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedOffenderContactPrefixPhone, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string ExpectedOffenderContactPrefixPhone { get; set; }

        protected override void ExecuteBlock()
        {
            //var details = OffenderContact.Split("\r\n");
            var Contact = OffenderContact.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var ContactPhone = Contact[Contact.Length - 1].Replace(" ", String.Empty);
            var phone = ContactPhone.Replace("Contact", String.Empty);

            Assert.IsTrue(phone.Equals(ExpectedOffenderContactPrefixPhone + ExpectedOffenderContactPhone));
        }
    }
}
