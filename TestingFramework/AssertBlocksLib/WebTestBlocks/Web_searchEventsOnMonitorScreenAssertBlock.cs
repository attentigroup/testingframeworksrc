﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.WebTestBlocks
{
   public class Web_SearchEventsOnMonitorScreenAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EventsList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public MonitorTableRow MonitorTableRow { get; set; }

        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetEventsResponse.EventsList[0].ID, int.Parse(MonitorTableRow.EventId));
        }
    }
}
