﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_DeleteWeeklyTimeframeAssertBlock : Web_DeleteTimeframeAssertBlock
    {
        protected override void ExecuteBlock()
        {
            TimeFrames4Validate = GetScheduleResponse.Schedule.WeeklySchedule;
            base.ExecuteBlock();
        }
    }
}
