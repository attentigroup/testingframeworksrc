﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_GetFilterCountersAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.FilterCounter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int FilterCounterBefore { get; set; }

        [PropertyTest(EnumPropertyName.FilterCounter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int FilterCounterAfter { get; set; }

        [PropertyTest(EnumPropertyName.FilterName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string FilterTitle { get; set; }

        protected override void ExecuteBlock()
        {
            WebTests.SeleniumWrapper.Logger.WriteLine($"Filter: '{FilterTitle}', count before handle: {FilterCounterBefore}; After handle: {FilterCounterAfter}");

            if (FilterCounterBefore > 0)
                Assert.AreEqual(FilterCounterAfter, FilterCounterBefore - 1);
            else
                Assert.Fail("No New events available");
        }
    }
}
