﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_OffenderLocationAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.OffenderLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public OffenderLocation OffenderDetailsBefore { get; set; }

        [PropertyTest(EnumPropertyName.OffenderLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public OffenderLocation OffenderDetailsAfter { get; set; }

        [PropertyTest(EnumPropertyName.IsLastTrailPointDisplayedOnMap, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsLastTrailPointDisplayedOnMap { get; set; }

        protected override void ExecuteBlock()
        {
            OffenderDetailsBefore.LastTrailPointDateTime = (OffenderDetailsBefore.LastTrailPointDateTime == string.Empty) ? DateTime.MinValue.ToString() : OffenderDetailsBefore.LastTrailPointDateTime;

            var IsEarlierTrailPointDateTime = DateTime.TryParse(OffenderDetailsBefore.LastTrailPointDateTime, out DateTime earlierTrailPointDateTime);
            var IsLatestTrailPointDateTime = DateTime.TryParse(OffenderDetailsAfter.LastTrailPointDateTime, out DateTime latestTrailPointDateTime);

            if (IsEarlierTrailPointDateTime && IsLatestTrailPointDateTime)
                Assert.IsTrue(latestTrailPointDateTime.CompareTo(earlierTrailPointDateTime) > 0);
            else
                WebTests.SeleniumWrapper.Logger.WriteLine($"Can't parse earlier DateTime '{OffenderDetailsBefore.LastTrailPointDateTime}' or latest DateTime '{OffenderDetailsAfter.LastTrailPointDateTime}'");
                
            Assert.IsTrue(OffenderDetailsAfter.NonDataLabel == string.Empty, "Trail not found");
            Assert.AreNotEqual(OffenderDetailsBefore.LastTrailPointDateTime, OffenderDetailsAfter.LastTrailPointDateTime, "Last trail point before and after simulator are the same");
            Assert.AreEqual(IsLastTrailPointDisplayedOnMap, OffenderDetailsAfter.TrailLastPointOnMapExists, "Map visibility is not as expected");
        }
    }
}
