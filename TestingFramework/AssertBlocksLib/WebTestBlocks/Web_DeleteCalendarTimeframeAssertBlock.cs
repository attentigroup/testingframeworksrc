﻿namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_DeleteCalendarTimeframeAssertBlock : Web_DeleteTimeframeAssertBlock
    {
        protected override void ExecuteBlock()
        {
            TimeFrames4Validate = GetScheduleResponse.Schedule.CalendarSchedule;
            base.ExecuteBlock();
        }
    }
}
