﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_VerifyTransmitterForDVAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AggTX, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string AggTX { get; set; }

        [PropertyTest(EnumPropertyName.VicTX, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string VicTX { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(AggTX == VicTX);
        }
    }
}
