﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_AddNewOffenderAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public OffenderDetails OffenderDetails { get; set; }
        protected override void ExecuteBlock()
        {
            var getOffender = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
            {
                OffenderRefID = new Offenders0.EntStringParameter() { Value = OffenderDetails.OffenderRefId , Operator = Offenders0.EnmStringOperator.Equal}
            });
            var apiOffender = getOffender.OffendersList[0];

            Assert.IsTrue(getOffender.OffendersList.Count() > 0);
            Assert.AreEqual(apiOffender.RefID, OffenderDetails.OffenderRefId);
            Assert.AreEqual(apiOffender.FirstName, OffenderDetails.FirstName);
            Assert.AreEqual(apiOffender.LastName, OffenderDetails.LastName);
            Assert.AreEqual(apiOffender.AgencyName, OffenderDetails.Agency);
            Assert.AreEqual(apiOffender.OfficerName, OffenderDetails.Officer);
            Assert.AreEqual(apiOffender.EquipmentInfo.ReceiverSN, OffenderDetails.Receiver);

        }
    }
}
