﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_VoiceSimNumberAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.voiceSimNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string voiceSimNumber { get; set; }

        [PropertyTest(EnumPropertyName.VoicePrefixPhone, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string VoicePrefixPhone { get; set; }

        [PropertyTest(EnumPropertyName.VoicePhoneNumber, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string VoicePhoneNumber { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(voiceSimNumber.Equals(VoicePrefixPhone + VoicePhoneNumber));
        }
    }
}
