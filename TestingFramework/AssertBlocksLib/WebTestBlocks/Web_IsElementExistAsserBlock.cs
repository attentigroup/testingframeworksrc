﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_IsElementExistAsserBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.IsElementExist, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsElementExist { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(IsElementExist);
        }
    }
}
