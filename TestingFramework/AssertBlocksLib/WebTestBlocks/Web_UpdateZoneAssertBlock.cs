﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_UpdateZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgAddZoneResponse AddZoneResponse { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsFullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.RealRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int RealRadious { get; set; }

        [PropertyTest(EnumPropertyName.BufferRadius, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int BufferRadious { get; set; }
        protected override void ExecuteBlock()
        {
            var zone = GetZonesByEntityIDResponse.ZoneList.FirstOrDefault(x => x.ID == AddZoneResponse.ZoneID);
            Assert.AreEqual(zone.Name, Name);
            Assert.AreEqual(zone.GraceTime, GraceTime);
            Assert.AreEqual(zone.FullSchedule, IsFullSchedule);

    
        }
    }
}
