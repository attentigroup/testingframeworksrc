﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure;
using WebTests.SeleniumWrapper;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_PagerDisplayedRowsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AmountOfRows, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<ValueTuple<int, int>> PagerAmountOfDisplayedRowInPageList { get; set; }

        [PropertyTest(EnumPropertyName.ShowResults, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int ShowResultsNumber { get; set; }

        protected override void ExecuteBlock()
        {
            if (PagerAmountOfDisplayedRowInPageList == null)
                throw new WebTestingException("The pager counter is null");

            for (int i = 0; i < PagerAmountOfDisplayedRowInPageList.Count; i++)
            {
                Logger.WriteLine($"Page {i} displayes rows from: {PagerAmountOfDisplayedRowInPageList[i].Item1} to: {PagerAmountOfDisplayedRowInPageList[i].Item2}");

                Assert.IsTrue(PagerAmountOfDisplayedRowInPageList[i].Item2 - PagerAmountOfDisplayedRowInPageList[i].Item1 <= ShowResultsNumber - 1);

                if (i < PagerAmountOfDisplayedRowInPageList.Count - 1)
                    Assert.AreEqual(ShowResultsNumber - 1, PagerAmountOfDisplayedRowInPageList[i].Item2 - PagerAmountOfDisplayedRowInPageList[i].Item1);
            }
        }
        
    }
}
