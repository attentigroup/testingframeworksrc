﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_QueueTrackerAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.FilterCounter, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<QueueRow> QueueList { get; set; }

        [PropertyTest(EnumPropertyName.IsElementExist, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsExpectedInQueue { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefId { get; set; }

        protected override void ExecuteBlock()
        {
            var queueRowforTracker = QueueList.FirstOrDefault(x => x.OffenderName.Contains(OffenderRefId));

            if (queueRowforTracker != null)
            {
                WebTests.SeleniumWrapper.Logger.WriteLine($"Offender Name Number in Queue is {queueRowforTracker.OffenderName}");
                Assert.AreEqual(IsExpectedInQueue, queueRowforTracker != null, $"Queue row with Offender Name s/n {OffenderRefId} not found");
            }

            else
                throw new Exception("No elements found in queue rows");
        }

    }
}
