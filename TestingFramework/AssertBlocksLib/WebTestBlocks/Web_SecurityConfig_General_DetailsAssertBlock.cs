﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfig_General_DetailsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.PrimaryTabLocatorString, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TargetClassName { get; set; }

        [PropertyTest(EnumPropertyName.Refine, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string[] Refine { get; set; }

        protected override void ExecuteBlock()
        {
            
        }
    }
}
