﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_ElementVisibleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.IsElementVisible, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsElementVisible { get; set; }

        [PropertyTest(EnumPropertyName.IsExpectedVisible, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsExpectedVisible { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(IsExpectedVisible, IsElementVisible, "Element visibility is not as expected!");
        }
    }
}
