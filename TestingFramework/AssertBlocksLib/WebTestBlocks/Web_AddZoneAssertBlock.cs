﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.WebTestBlocks
{
    public abstract class Web_AddZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }

        [PropertyTest(EnumPropertyName.ZoneName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Name { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        [PropertyTest(EnumPropertyName.GraceTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int GraceTime { get; set; }

        [PropertyTest(EnumPropertyName.FullSchedule, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsFullSchedule { get; set; }

        [PropertyTest(EnumPropertyName.Zone, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.EntZone Zone{ get; set; }
        protected override void ExecuteBlock()
        {
            Zone = GetZonesByEntityIDResponse.ZoneList.FirstOrDefault(x => x.Name == Name);
            Assert.IsTrue(Zone != null);
            Assert.AreEqual(Zone.Limitation.ToString(), Limitation, $"API limitation: {Zone.Limitation}, Web limitation: {Limitation}");
            Assert.AreEqual(Zone.GraceTime, GraceTime, $"API grace time: {Zone.GraceTime}, Web grace time: {GraceTime}");
            Assert.AreEqual(Zone.FullSchedule, IsFullSchedule, $"API full schedule: {Zone.FullSchedule}, Web full schedule: {IsFullSchedule}");
        }

        protected bool IsZoneType(Type zoneType)
        {
            return Zone.GetType() == zoneType;
        }
    }
}
