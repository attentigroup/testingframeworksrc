﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using Common.CustomAttributes;
using WebTests.InfraStructure.Entities;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebTests.SeleniumWrapper;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_TrailReportAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.TrailReport, EnumPropertyType.None, EnumPropertyModifier.None)]
        public TrailReport TrailReport { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.OffenderLocation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public OffenderLocation OffenderLocation { get; set; }

        protected override void ExecuteBlock()
        {
            var actualFrom = OffenderLocation.FromDateTime;
            var reportTo = TrailReport.From;
            var fromTimeSpan = Math.Abs(actualFrom.Subtract(reportTo).Seconds);
            var reportTimeSpan = TrailReport.To.Subtract(TrailReport.From);
            var actualTimeSpan = OffenderLocation.ToDateTime.Subtract(OffenderLocation.FromDateTime);
            var ComparativePeriodTimeSpan = actualTimeSpan.Subtract(reportTimeSpan).Seconds;

            Logger.WriteLine($"Offender details: {TrailReport.OffenderDetails}");

            Assert.IsTrue(TrailReport.OffenderDetails.Contains(GetOffendersResponse.OffendersList.Single().FirstName),$"Offender details in the report are not as expected: {GetOffendersResponse.OffendersList.Single().FirstName}");
            Assert.IsTrue(TrailReport.OffenderDetails.Contains(GetOffendersResponse.OffendersList.Single().LastName), $"Offender details in the report are not as expected: {GetOffendersResponse.OffendersList.Single().LastName}");
            Assert.IsTrue(TrailReport.OffenderDetails.Contains(GetOffendersResponse.OffendersList.Single().RefID), $"Offender details in the report are not as expected: {GetOffendersResponse.OffendersList.Single().RefID}");
            Assert.AreEqual("Trail Report", TrailReport.Header, "The header 'Trail Reports' is not displayed");
            Assert.IsTrue(TrailReport.IsMapExists,"Map is not displayed");
            Assert.IsTrue(fromTimeSpan < 5, $"fromTimeSpan= {fromTimeSpan}");
            Assert.IsTrue(ComparativePeriodTimeSpan == 0, $"ComparativePeriodTimeSpan= {ComparativePeriodTimeSpan}");
        }
    }
}
