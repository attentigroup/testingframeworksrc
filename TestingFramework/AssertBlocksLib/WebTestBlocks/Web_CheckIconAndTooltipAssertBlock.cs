﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_CheckIconAndTooltipAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.MonitorTableRowArray, EnumPropertyType.None, EnumPropertyModifier.None)]
        public MonitorTableRow[] MonitorTableRowArr { get; set; }

        [PropertyTest(EnumPropertyName.CurrentStatusTableRowArray, EnumPropertyType.None, EnumPropertyModifier.None)]
        public CurrentStatusTableRow[] CurrentStatusTableRowArr { get; set; }

        [PropertyTest(EnumPropertyName.Status, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string Status { get; set; }

        protected override void ExecuteBlock()
        {
            if (MonitorTableRowArr != null && MonitorTableRowArr.Length > 0)
            {
                foreach (var eventDetails in MonitorTableRowArr)
                {
                    var icon = eventDetails.EventStatus;
                    Assert.IsTrue(Status.Equals(icon), $"icon {icon} not as expected ({Status})");
                }
            }
            else if (CurrentStatusTableRowArr != null && CurrentStatusTableRowArr.Length > 0)
            {
                foreach (var eventDetails in CurrentStatusTableRowArr)
                {
                    var icon = eventDetails.Status;
                    Assert.IsTrue(Status.Equals(icon), $"icon {icon} not as expected ({Status})");
                }
            }
            else
                throw new Exception("Table object not found");
            

        }
    }
}
