﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_ManualEventAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CurrentStatusTableRow, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public CurrentStatusTableRow CurrentStatusTableRow { get; set; }

        [PropertyTest(EnumPropertyName.expected, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string expectedStatus { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(expectedStatus, CurrentStatusTableRow.Status);
        }
    }
}
