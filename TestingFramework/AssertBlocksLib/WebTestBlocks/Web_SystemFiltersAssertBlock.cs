﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.SeleniumWrapper;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_SystemFiltersAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.SystemFilters, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string[] SystemFilters { get; set; }

        [PropertyTest(EnumPropertyName.expected, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] ExpectedSystemFilters { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var element in ExpectedSystemFilters)
            {
                Assert.IsTrue(SystemFilters.Contains(element));
            }
        }
    }
}
