﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.WebTestBlocks
{
    public abstract class Web_AddScheduleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.Type, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Type { get; set; }

        [PropertyTest(EnumPropertyName.Location, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Location { get; set; }

        [PropertyTest(EnumPropertyName.RecurseEveryWeek, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool RecurseEveryWeek { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime StartDate { get; set; }

        [PropertyTest(EnumPropertyName.EndTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime EndDate { get; set; }

        [PropertyTest(EnumPropertyName.Limitation, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string Limitation { get; set; }

        protected Schedule0.EntTimeFrame[] TimeFrames4Validate{get; set;}


        protected override void ExecuteBlock()
        {
            Validate(TimeFrames4Validate);
        }
       

        private Schedule0.EntTimeFrame Validate(Schedule0.EntTimeFrame[] timeFrames)
        {
            Schedule0.EntTimeFrame schedule = null;
            try
            {
                if (timeFrames == null || timeFrames.Length == 0)
                    throw new Exception("CalendarSchedule null or empty");
                schedule = timeFrames.First(x => x.FromTime == StartDate && x.ToTime == EndDate);
            }
            catch (Exception ex)
            {
                throw new Exception("CalendarSchedule  does not conaint schedula between time values", ex);
            }
            var typeWithoutQuote = Type.Replace("\'", "");
            var typeWithoutSpaces = typeWithoutQuote.Replace(" ", "");
            Assert.AreEqual(schedule.TimeFrameType.ToString(), typeWithoutSpaces);
            if(!string.IsNullOrEmpty(Location))
                Assert.AreEqual(schedule.LocationName, Location);
            Assert.AreEqual(schedule.IsWeekly, RecurseEveryWeek);
            if (!string.IsNullOrEmpty(Location))
                Assert.AreEqual(schedule.LimitationType.ToString(), Limitation);

            return schedule;
        }
    }
}
