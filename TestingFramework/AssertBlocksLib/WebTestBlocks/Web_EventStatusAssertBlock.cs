﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_EventStatusAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EventStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public string EventStatus { get; set; }

        [PropertyTest(EnumPropertyName.expected, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string expected { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(expected, EventStatus);
        }
    }
}
