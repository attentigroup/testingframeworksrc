﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

#endregion

namespace AssertBlocksLib.WebTestBlocks
{
  public class Web_UpdateGPSOffenderConfigurationRequestAssertBlock : AssertBaseBlock
    {

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse_BeforeUpdate { get; set; }

        [PropertyTest(EnumPropertyName.GetOffenderConfigurationDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetOffenderConfigurationDataResponse GetOffenderConfigurationDataResponse_AfterUpdate { get; set; }

        public static int ONE_PIECE_GEN_2 = 46;
        public static int ONE_PIECE_GEN_39 = 80;
        public static int ONE_PIECE_TD4I = 84;
        public static int TWO_PIECE_DV = 42;
        public static int TWO_PIECE = 82;

        protected override void ExecuteBlock()
        {
            var offender = GetOffendersResponse.OffendersList[0];
            TrackerAssert(offender);
            E4Assert(offender);
        }

        public void TrackerAssert(Offenders0.EntOffender offender)
        {
            if (offender.ProgramType == Offenders0.EnmProgramType.One_Piece_RF || offender.ProgramType == Offenders0.EnmProgramType.Two_Piece)
            {
                var responseBeforeUpdate = (Configuration0.EntConfigurationGPS)GetOffenderConfigurationDataResponse_BeforeUpdate.ConfigurationData;
                var responseAfterUpdate = (Configuration0.EntConfigurationGPS)GetOffenderConfigurationDataResponse_AfterUpdate.ConfigurationData;

                if (offender.EquipmentInfo.HomeUnitSN != null || offender.EquipmentInfo.ReceiverModel == TWO_PIECE)
                    Assert.AreNotEqual(responseBeforeUpdate.ReceiverRange, responseAfterUpdate.ReceiverRange, "Base Unit Range");

                Assert.AreNotEqual(responseBeforeUpdate.NormalLoggingRate, responseAfterUpdate.NormalLoggingRate, "Normal Logging Rate");
                Assert.AreNotEqual(responseBeforeUpdate.ViolationLoggingRate, responseAfterUpdate.ViolationLoggingRate, "Violation Logging Rate");
                Assert.AreNotEqual(responseBeforeUpdate.TimeBetweenUploads, responseAfterUpdate.TimeBetweenUploads, "Time Between Uploads");
                Assert.AreNotEqual(responseBeforeUpdate.ScheduleActive, responseAfterUpdate.ScheduleActive, "Schedule Active");
                Assert.AreNotEqual(responseBeforeUpdate.PassiveMode, responseAfterUpdate.PassiveMode, "Passive Mode");
                Assert.AreNotEqual(responseBeforeUpdate.DetectOtherTransmitters, responseAfterUpdate.DetectOtherTransmitters, "Detect Other Transmitters");

                if (offender.EquipmentInfo.ReceiverModel == ONE_PIECE_GEN_2 || offender.EquipmentInfo.ReceiverModel == ONE_PIECE_GEN_39 || offender.EquipmentInfo.ReceiverModel == TWO_PIECE_DV)
                {
                    Assert.AreNotEqual(responseBeforeUpdate.SupportGPRS, responseAfterUpdate.SupportGPRS, "Support GPRS");
                }

                Assert.AreNotEqual(responseBeforeUpdate.LBSActive, responseAfterUpdate.LBSActive, "LBS Active");

                if (responseAfterUpdate.LBSActive == true)
                {
                    Assert.AreNotEqual(responseBeforeUpdate.LBSForViolations, responseAfterUpdate.LBSForViolations, "LBS For Violations");
                    Assert.AreNotEqual(responseBeforeUpdate.GPSDisappearTime, responseAfterUpdate.GPSDisappearTime, "GPS Disappear Time");
                    Assert.AreNotEqual(responseBeforeUpdate.LBSSamplingRate, responseAfterUpdate.LBSSamplingRate, "LBS Sampling Rate");
                    Assert.AreNotEqual(responseBeforeUpdate.LBSRequestTimeout, responseAfterUpdate.LBSRequestTimeout, "LBS Request Timeout");
                    Assert.AreNotEqual(responseBeforeUpdate.LBSNoPositionTimeout, responseAfterUpdate.LBSNoPositionTimeout, "LBS No Position Timeout");
                }

                if (offender.EquipmentInfo.ReceiverModel == TWO_PIECE_DV)
                {
                    Assert.AreNotEqual(responseBeforeUpdate.DisplayOffenderName, responseAfterUpdate.DisplayOffenderName, "Display Offender Name");

                    Assert.AreNotEqual(responseBeforeUpdate.ReceptionIconsActive, responseAfterUpdate.ReceptionIconsActive, "Reception Icons Active");

                    if (offender.ProgramConcept == Offenders0.EnmProgramConcept.Victim)
                    {
                        Assert.AreNotEqual(responseBeforeUpdate.GPSProximity, responseAfterUpdate.GPSProximity, "GPS Proximity");
                        Assert.AreNotEqual(responseBeforeUpdate.GPSProximityBuffer, responseAfterUpdate.GPSProximityBuffer, "GPS Proximity Buffer");
                    }
                }
            }
        }

        public void E4Assert(Offenders0.EntOffender offender)
        {
            if (offender.ProgramType == Offenders0.EnmProgramType.E4_Dual)
            {
                var responseBeforeUpdate = (Configuration0.EntConfigurationRF)GetOffenderConfigurationDataResponse_BeforeUpdate.ConfigurationData;
                var responseAfterUpdate = (Configuration0.EntConfigurationRF)GetOffenderConfigurationDataResponse_AfterUpdate.ConfigurationData;

                Assert.AreNotEqual(responseBeforeUpdate.ReceiverRange, responseAfterUpdate.ReceiverRange, "Base Unit Range");
                Assert.AreNotEqual(responseBeforeUpdate.TransmitterDisappearTime, responseAfterUpdate.TransmitterDisappearTime, "Transmitter Disappear Time");
                Assert.AreNotEqual(responseBeforeUpdate.NumberOfRings, responseAfterUpdate.NumberOfRings, "Number Of Rings");
                Assert.AreNotEqual(responseBeforeUpdate.TimeBetweenUploads, responseAfterUpdate.TimeBetweenUploads, "Time Between Uploads");
                Assert.AreNotEqual(responseBeforeUpdate.RingVolume, responseAfterUpdate.RingVolume, "Ring Volume");
                Assert.AreNotEqual(responseBeforeUpdate.IsKosher, responseAfterUpdate.IsKosher, "Is Kosher");
                Assert.AreNotEqual(responseBeforeUpdate.GraceBeforeTimeFrameStart, responseAfterUpdate.GraceBeforeTimeFrameStart, "Grace Before Time Frame Start");
                Assert.AreNotEqual(responseBeforeUpdate.GraceAfterTimeFrameStart, responseAfterUpdate.GraceAfterTimeFrameStart, "Grace After Time Frame Start");
                Assert.AreNotEqual(responseBeforeUpdate.GPRSPriority, responseAfterUpdate.GPRSPriority, "Support GPRS Priority");
                Assert.AreNotEqual(responseBeforeUpdate.LandlinePriority, responseAfterUpdate.LandlinePriority, "Support Landline Priority");
                Assert.AreNotEqual(responseBeforeUpdate.OfficerScreenActive, responseAfterUpdate.OfficerScreenActive, "Officer Screen Active");
            }
        }
    }
}



