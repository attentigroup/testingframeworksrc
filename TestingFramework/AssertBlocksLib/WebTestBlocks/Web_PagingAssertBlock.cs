﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure;
using WebTests.SeleniumWrapper;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_PagingAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.IsPagingWorks, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsPagingWorks { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(IsPagingWorks);
        }
        
    }
}
