﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;
using WebTests.SeleniumWrapper;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_SecurityConfigAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.SecurityConfigResults, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<List<SecurityConfigObject>> SecurityConfigResults { get; set; }

        [PropertyTest(EnumPropertyName.ModifyStatus, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmModify ModifyStatus { get; set; }
        [PropertyTest(EnumPropertyName.ResourceID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string[] ResourceID { get; set; }

        protected override void ExecuteBlock()
        {
            var setVisibility = ModifyStatus == EnmModify.Visible ? true : false;

            var select = SecurityConfigResults.Select(x => x.FindAll(l => l.ActualVisibleEnabledExposed == setVisibility)).ToList();

            for (int i = 0; i < SecurityConfigResults.Count; i++)
            {
                Logger.WriteLine($"Method: {i + 1}");
                foreach (var item in SecurityConfigResults[i])
                {
                    Logger.WriteLine($"MethodName: {item.SecurityConfigMethodName}; ElementName: {item.ElementName}; ActualVisible: {item.ActualVisibleEnabledExposed}");
                }
            }
            foreach (var list in select)
            {
                foreach (var item in list)
                {
                    if (ResourceID.Any(r => item.ElementName == r))
                    {
                        Assert.AreEqual(item.ActualVisibleEnabledExposed, setVisibility, $"'{item.ElementName}' visibility is {item.ActualVisibleEnabledExposed}, while the expectd result is {setVisibility}");
                    }
                }
            }
        }

    }
   
}

