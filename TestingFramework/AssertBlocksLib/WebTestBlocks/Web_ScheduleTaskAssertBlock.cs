﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_ScheduleTaskAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.MonitorTableRow, EnumPropertyType.None, EnumPropertyModifier.None)]
        public MonitorTableRow MonitorTableRow { get; set; }

        [PropertyTest(EnumPropertyName.StartTime, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime time { get; set; }

        [PropertyTest(EnumPropertyName.ReceiverSN, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ReceiverSN { get; set; }

        [PropertyTest(EnumPropertyName.RefID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string OffenderRefId { get; set; }
        protected override void ExecuteBlock()
        {
            var date = time.ToString("dd/MM/yyyy");

            Assert.IsTrue(MonitorTableRow != null);
            Assert.IsTrue(MonitorTableRow.EventTime.Contains(date), $"Date is not as expected; Actual is: {MonitorTableRow.EventTime}; Expected is: {date}");
            Assert.AreEqual(MonitorTableRow.ReceiverSN, ReceiverSN);
        }
    }
}
