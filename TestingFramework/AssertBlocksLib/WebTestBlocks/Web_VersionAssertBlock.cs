﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace TestingFramework.AssertBlocksLib.WebTestBlock
{
    public class Web_VersionAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.Version, EnumPropertyType.None, EnumPropertyModifier.None)]
        public AppVersions ActualVersions { get; set; }

        [PropertyTest(EnumPropertyName.ExpectedVersions, EnumPropertyType.None, EnumPropertyModifier.None)]
        public AppVersions ExpectedVersions { get; set; }

        protected override void ExecuteBlock()
        {
            Console.WriteLine($"Expected    : {ExpectedVersions.WebVersion}, {ExpectedVersions.DatabaseVersion}, {ExpectedVersions.DatabaseServer}");
            Console.WriteLine($"Actual      : {ActualVersions.WebVersion}, {ActualVersions.DatabaseVersion}, {ActualVersions.DatabaseServer}");
            Assert.AreEqual(ExpectedVersions.WebVersion, ActualVersions.WebVersion, "Web vesion not ase expected");
            Assert.AreEqual(ExpectedVersions.DatabaseVersion, ActualVersions.DatabaseVersion, "DB vesion not ase expected");
            Assert.AreEqual(ExpectedVersions.DatabaseServer, ActualVersions.DatabaseServer, "DB server not ase expected");
        }
    }
}
