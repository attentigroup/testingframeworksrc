﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

namespace AssertBlocksLib.WebTestBlocks
{
   public class Web_IsEventIDExistInMonitorTableRowAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EventsList, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public MonitorTableRow[] MonitorTableRows { get; set; }

 //       [PropertyTest(EnumPropertyName.ArrayLength, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
  //      public int ArrayLength { get; set; }

        [PropertyTest(EnumPropertyName.EventID, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]

        public int EventID { get; set; }

        protected override void ExecuteBlock()
        {
            if (MonitorTableRows != null)
            {
                bool result = false;
                for (int i=0; (i < (MonitorTableRows.Length - 1)) ; i++)
                {
                    if (MonitorTableRows[i].EventId == EventID.ToString())
                    {
                        result = true;
                        break;
                    }
                }
                Assert.IsTrue(result == true, $"Required Linked event ID ={EventID} not exist");
            }
            else
                throw new Exception("Monitor Table row is null");
        }
    }
}
