﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;

namespace AssertBlocksLib.WebTestBlocks
{
    public class Web_FilterByEndOfProgramForActiveOffenderAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.Offender, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public OffenderDetails[] OffenderDetailsArr { get; set; }

        [PropertyTest(EnumPropertyName.Time, EnumPropertyType.None, EnumPropertyModifier.None)]
        public DateTime Time { get; set; }
        protected override void ExecuteBlock()
        {
            if (OffenderDetailsArr == null || OffenderDetailsArr.Length == 0)
                Assert.Fail("Offender Details Are Empty!!");
            
            foreach (var offender in OffenderDetailsArr)
            {
                var rowTime = DateTime.ParseExact(offender.ProgramEnd, "dd/MM/yyyy HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
                Assert.IsTrue(rowTime > Time);
                Assert.AreEqual("Active", offender.Status);
            }
        }
    }
}
