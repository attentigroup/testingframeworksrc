﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;


namespace AssertBlocksLib.EquipmentAsserts
{
    public class DeleteEquipmentAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetEquipmentListResponse.EquipmentList.Length == 0);
        }
    }
}
