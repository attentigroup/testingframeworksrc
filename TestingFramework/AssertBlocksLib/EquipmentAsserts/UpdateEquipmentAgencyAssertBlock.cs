﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class UpdateEquipmentAgencyAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(UpdateEquipmentRequest.AgencyID, GetEquipmentListResponse.EquipmentList[0].AgencyID);
        }
    }
}
