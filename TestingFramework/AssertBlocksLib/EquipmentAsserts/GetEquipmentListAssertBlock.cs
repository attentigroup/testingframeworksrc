﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetEquipmentListAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        protected override void ExecuteBlock()
        {

            var Equipment = GetEquipmentListResponse.EquipmentList[0];

            Assert.AreEqual(AddEquipmentRequest.Model, Equipment.Model);
            Assert.AreEqual(AddEquipmentRequest.ProtocolType, Equipment.ProtocolType);
        }
    }


    public class GetEquipmentListAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        protected override void ExecuteBlock()
        {

            var Equipment = GetEquipmentListResponse.EquipmentList[0];

            Assert.AreEqual(AddEquipmentRequest.Model, Equipment.Model);
            Assert.AreEqual(AddEquipmentRequest.ProtocolType, Equipment.ProtocolType);
        }
    }
}
