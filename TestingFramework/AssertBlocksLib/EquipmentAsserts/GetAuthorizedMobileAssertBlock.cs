﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetAuthorizedMobileAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileListResponse { get; set; }

        [PropertyTest(EnumPropertyName.ApplicationID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmMobileApplication ApplicationID { get; set; }

        [PropertyTest(EnumPropertyName.MobileID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string MobileID { get; set; }
        protected override void ExecuteBlock()
        {
            if(ApplicationID != null)
            {
                foreach(var mobile in GetAuthorizedMobileListResponse.AuthorizedMobilesList)
                {
                    Assert.AreEqual(ApplicationID, mobile.ApplicationID);
                }
            }

            if (!string.IsNullOrEmpty(MobileID))
            {
                    Assert.AreEqual(MobileID, GetAuthorizedMobileListResponse.AuthorizedMobilesList[0].ID);
            }
        }
    }
}
