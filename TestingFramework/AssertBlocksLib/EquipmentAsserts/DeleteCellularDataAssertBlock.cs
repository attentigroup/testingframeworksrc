﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class DeleteCellularDataAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetCellularDataResponse.CellularData.Length == 0, 
                $"CellularData.Length need to be 0 and it is {GetCellularDataResponse.CellularData.Length}.");
        }
    }
}
