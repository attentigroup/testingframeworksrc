﻿using System;
using System.ServiceModel;
using AssertBlocksLib.TestingFrameworkAsserts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;

public class ValidationFaultExceptionEquipmentAssertBlock : ValidationFaultExceptionAssertBlock
{
    public ValidationFaultExceptionEquipmentAssertBlock(string[] field, string rule)
        : base(field, rule, typeof(FaultException<Equipment0.ValidationFault>)) { }

    protected override void AssertMessage()
    {
        var blockException = BlockException as FaultException<Equipment0.ValidationFault>;

        if (blockException == null)//it might be the inner exception
            blockException = BlockException.InnerException as FaultException<Equipment0.ValidationFault>;

        var validationError = blockException.Detail.Errors[0];
        Assert.AreEqual(Field.Length, validationError.Field.Length);

        Array.ForEach(Field, e => Array.Exists(validationError.Field, a => a == e));

        Assert.AreEqual(Rule, validationError.Rule);
    }
}