﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Equipment_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace AssertBlocksLib.EquipmentAsserts
{
    public class CheckSimIsActiveAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_0.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {

            if (GetCellularDataResponse.CellularData.Length > 0)
            {
                var CellularData = GetCellularDataResponse.CellularData[0];

                Assert.IsTrue(CellularData.IsPrimarySIM);
            }
            else
            {
                throw new Exception("GetCellularDataResponse.CellularData is empty - No cellular data was found");
            }
        }
    }
}

