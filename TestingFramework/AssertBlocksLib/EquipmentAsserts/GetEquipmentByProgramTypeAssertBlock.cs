﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetEquipmentByProgramTypeAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment.EnmProgramType ProgramType { get; set; }
        protected override void ExecuteBlock()
        {
            
            switch (ProgramType)
            {
                case Equipment.EnmProgramType.One_Piece_RF:
                    foreach(var equip in GetEquipmentListResponse.EquipmentList)
                    {         
                        Assert.IsTrue(equip.Model.ToString().Contains("One_Piece"));                     
                    }
                    break;
                case Equipment.EnmProgramType.Two_Piece:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Model.ToString().Contains("Two_Piece"));
                    }
                    break;
                case Equipment.EnmProgramType.RF_Dual:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Model.ToString().Contains("E4"));
                    }
                    break;
                case Equipment.EnmProgramType.RF_Cellular:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Model.ToString().Contains("E3"));
                    }
                    break;
                case Equipment.EnmProgramType.VB_Dual:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Model.ToString().Contains("Alcohol"));
                    }
                    break;
                default:
                    bool defaultCond = false;
                    Assert.IsTrue(defaultCond);
                    break;
            }
            
        }
    }
}
