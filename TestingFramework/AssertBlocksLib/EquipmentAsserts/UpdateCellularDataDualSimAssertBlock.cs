﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Equipment_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace AssertBlocksLib.EquipmentAsserts
{

    public class UpdateCellularDataDualSimAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_0.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_0.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {

            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreEqual(UpdateCellularDataRequest.DataPhoneNumber, CellularData.DataPhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePrefixPhone, CellularData.VoicePrefixPhone);
            Assert.AreEqual(UpdateCellularDataRequest.DataPrefixPhone, CellularData.DataPrefixPhone);

        }
    }
}

