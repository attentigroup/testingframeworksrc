﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetEquipmentByTypeAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment.EnmEquipmentType EquipmentType { get; set; }
        protected override void ExecuteBlock()
        {
            switch (EquipmentType)
            {
                case Equipment.EnmEquipmentType.All:
                    break;
                case Equipment.EnmEquipmentType.Receiver:
                    foreach(var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Description.Equals("<RCVR>"));
                    }
                    break;
                case Equipment.EnmEquipmentType.Transmitter:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Description.Equals("<TRX>"));
                    }
                    break;
                case Equipment.EnmEquipmentType.Transceiver:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Description.Equals("<TCVR>"));
                    }
                    break;
                case Equipment.EnmEquipmentType.ManualResetDevice:
                    foreach (var equip in GetEquipmentListResponse.EquipmentList)
                    {
                        Assert.IsTrue(equip.Description.Equals("<MRD>"));
                    }
                    break;
                case Equipment.EnmEquipmentType.Mobile:
                    break;
                default:
                    break;
            }
        }
    }
}
