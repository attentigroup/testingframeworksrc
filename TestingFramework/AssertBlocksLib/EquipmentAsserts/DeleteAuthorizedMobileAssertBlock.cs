﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class DeleteAuthorizedMobileAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileListResponse { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetAuthorizedMobileListResponse.AuthorizedMobilesList.Length, 0);
        }
    }
}
