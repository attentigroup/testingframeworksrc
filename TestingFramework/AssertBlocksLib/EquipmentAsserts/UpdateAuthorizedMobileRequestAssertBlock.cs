﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class UpdateAuthorizedMobileRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgAddAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgAddAuthorizedMobileRequest AddAuthorizedMobileRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]

        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(1, GetAuthorizedMobileListResponse.AuthorizedMobilesList.Length);

            var AuthorizedMobile = GetAuthorizedMobileListResponse.AuthorizedMobilesList[0];

            Assert.IsTrue(AuthorizedMobile.IsActive);
        }
    }
}
