﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class AddEquipmentAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgAddEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgAddEquipmentRequest AddEquipmentRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        protected override void ExecuteBlock()
        {

            var Equipment = GetEquipmentListResponse.EquipmentList[0];

            Assert.AreEqual(AddEquipmentRequest.AgencyID, Equipment.AgencyID);
            //Assert.AreEqual(AddEquipmentRequest.Description, Equipment.Description);
            Assert.AreEqual(AddEquipmentRequest.EquipmentEncryptionGSM, Equipment.EquipmentEncryptionGSM);
            Assert.AreEqual(AddEquipmentRequest.EquipmentEncryptionRF, Equipment.EquipmentEncryptionRF);
            Assert.AreEqual(AddEquipmentRequest.Manufacturer, Equipment.Manufacturer);
            Assert.AreEqual(AddEquipmentRequest.Model, Equipment.Model);
            Assert.AreEqual(AddEquipmentRequest.ProtocolType, Equipment.ProtocolType);
            Assert.AreEqual(AddEquipmentRequest.SerialNumber, Equipment.SerialNumber);
            Assert.AreEqual(AddEquipmentRequest.SupportEncryption, Equipment.SupportEncryption);
        }
    }
}
