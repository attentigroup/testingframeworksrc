﻿using AssertBlocksLib.TestingFrameworkAsserts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;


namespace AssertBlocksLib.EquipmentAsserts
{
    public static class EquipmentAssertsFactory
    {
        public static ValidationFaultExceptionAssertBlock ValidateEquipment_SerialNumber_WS001()
        {
            ValidationFaultExceptionAssertBlock ValidateEquipment_SerialNumber_WS001 = new ValidationFaultExceptionEquipmentAssertBlock(new[] { "SerialNumber" }, "WS001");
            return ValidateEquipment_SerialNumber_WS001;
        }
    }
}
