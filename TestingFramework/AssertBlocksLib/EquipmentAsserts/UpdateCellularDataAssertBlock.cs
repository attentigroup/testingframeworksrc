﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Equipment_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace AssertBlocksLib.EquipmentAsserts
{
    public class UpdateCellularDataAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_0.EntMsgGetCellularDataResponse GetCellularDataResponseAfterUpdate { get; set; }

        protected override void ExecuteBlock()
        {

            var CellularDataAfterUpdate = GetCellularDataResponseAfterUpdate.CellularData[0];

            Assert.AreEqual(UpdateCellularDataRequest.DataPhoneNumber, CellularDataAfterUpdate.DataPhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.DataPrefixPhone, CellularDataAfterUpdate.DataPrefixPhone);
            Assert.AreEqual(UpdateCellularDataRequest.ProviderID, CellularDataAfterUpdate.ProviderID);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularDataAfterUpdate.VoicePhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePrefixPhone, CellularDataAfterUpdate.VoicePrefixPhone);

        }
    }
    public class UpdateCellularDataAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {

            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreNotEqual(UpdateCellularDataRequest.CSDPhoneNumber, CellularData.CSDPhoneNumber);
            Assert.AreNotEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreNotEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);

        }
    }


    public class UpdateCellularDataAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }


        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {

            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreNotEqual(UpdateCellularDataRequest.CSDPhoneNumber, CellularData.CSDPhoneNumber);
            Assert.AreNotEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreNotEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);

        }
    }
}

