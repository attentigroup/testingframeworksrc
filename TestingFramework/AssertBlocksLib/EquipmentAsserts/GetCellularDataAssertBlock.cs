﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetCellularDataAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.DeviceIDList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int[] DeviceIDList { get; set; }

        [PropertyTest(EnumPropertyName.SerialNumber, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string SerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.SIMID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int SIMID { get; set; }
        protected override void ExecuteBlock()
        {
            if (DeviceIDList?.Length > 0)
            {
                foreach(var cellularData in GetCellularDataResponse.CellularData)
                {
                    Assert.IsTrue(DeviceIDList.Contains(cellularData.EquipmentID));
                }
            }
            if (!string.IsNullOrEmpty(SerialNumber))
            {
                var getEquipment = EquipmentProxy.Instance.GetEquipmentList(new EntMsgGetEquipmentListRequest()
                {
                    SerialNumber = SerialNumber
                });
                foreach (var cellularData in GetCellularDataResponse.CellularData)
                {
                    Assert.AreEqual(getEquipment.EquipmentList[0].ID, cellularData.EquipmentID);
                }
            }
            if (SIMID != 0)
            {
                foreach (var cellularData in GetCellularDataResponse.CellularData)
                {
                    Assert.AreEqual(SIMID, cellularData.ID);
                }
            }
        }
    }
}
