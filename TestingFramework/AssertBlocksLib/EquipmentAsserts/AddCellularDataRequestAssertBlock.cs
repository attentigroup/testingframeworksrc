﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace AssertBlocksLib.EquipmentAsserts
{
    public class AddCellularDataRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgAddCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgAddCellularDataRequest AddCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgAddCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgAddCellularDataResponse AddCellularDataResponse { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            for (int i = 0; i < GetCellularDataResponse.CellularData.Length; i++)
            {
                int id = GetCellularDataResponse.CellularData[i].ID;
                if (id.CompareTo(AddCellularDataResponse.NewSIMID) == 0)
                {
                    var CellularData = GetCellularDataResponse.CellularData[i];

                    Assert.AreEqual(AddCellularDataRequest.DataPhoneNumber, CellularData.DataPhoneNumber, "DataPhoneNumber");
                    Assert.AreEqual(AddCellularDataRequest.ProviderID, CellularData.ProviderID, "ProviderID");
                    Assert.AreEqual(AddCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber, "VoicePhoneNumber");
                    Assert.AreEqual(AddCellularDataRequest.DataPrefixPhone, CellularData.DataPrefixPhone, "DataPrefixPhone");
                    Assert.AreEqual(AddCellularDataRequest.EquipmentID, CellularData.EquipmentID, "EquipmentID");
                    Assert.AreEqual(AddCellularDataRequest.IsPrimarySIM, CellularData.IsPrimarySIM, "IsPrimarySIM");
                    Assert.AreEqual(AddCellularDataRequest.VoicePrefixPhone, CellularData.VoicePrefixPhone, "VoicePrefixPhone");


                }
            }
        }
    }


    public class AddCellularDataRequestAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreEqual(UpdateCellularDataRequest.CSDPhoneNumber, CellularData.CSDPhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);
        }
    }


    public class AddCellularDataRequestAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreEqual(UpdateCellularDataRequest.CSDPhoneNumber, CellularData.CSDPhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);
        }
    }
}
