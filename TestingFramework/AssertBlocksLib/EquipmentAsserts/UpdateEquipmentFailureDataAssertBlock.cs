﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class UpdateEquipmentFailureDataAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateEquipmentRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgUpdateEquipmentRequest UpdateEquipmentRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var damageData = GetEquipmentListResponse.EquipmentList[0].DamageData;
            Assert.AreEqual(UpdateEquipmentRequest.EquipmentLocation, damageData.EquipmentLocation);
            Assert.IsTrue(UpdateEquipmentRequest.FailureDate.Value.Date - damageData.FailureDate.Value.Date == TimeSpan.Zero);
            Assert.AreEqual(UpdateEquipmentRequest.FailureDescription, damageData.FailureDescription);
            Assert.AreEqual(UpdateEquipmentRequest.FailureType, damageData.FailureType);
            Assert.IsTrue(UpdateEquipmentRequest.SentToRepairDate.Value.Date - damageData.SentToRepairDate.Value.Date == TimeSpan.Zero);
            if(UpdateEquipmentRequest.ReturnFromRepairDate.HasValue)
                Assert.IsTrue(UpdateEquipmentRequest.ReturnFromRepairDate.Value.Date - damageData.ReturnFromRepairDate.Value.Date == TimeSpan.Zero);
            Assert.AreEqual(UpdateEquipmentRequest.IsDamaged, GetEquipmentListResponse.EquipmentList[0].IsDamaged);
            Assert.AreEqual(UpdateEquipmentRequest.EquipmentID, GetEquipmentListResponse.EquipmentList[0].ID);
        }
    }
}
