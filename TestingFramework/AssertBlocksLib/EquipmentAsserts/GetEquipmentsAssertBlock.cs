﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetEquipmentsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        [PropertyTest(EnumPropertyName.AgencyID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int AgencyID { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentModelArray, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment.EnmEquipmentModel[] EquipmentModels { get; set; }

        [PropertyTest(EnumPropertyName.EnmProgramType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment.EnmProgramType [] ProgramType { get; set; }

        [PropertyTest(EnumPropertyName.ReturnDamageData, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnDamageData { get; set; }

        [PropertyTest(EnumPropertyName.ReturnOnlyNotAllocated, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ReturnOnlyNotAllocated { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentSerialNumber, EnumPropertyType.String, EnumPropertyModifier.None)]
        public string EquipmentSerialNumber { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentType, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Equipment.EnmEquipmentType EquipmentType { get; set; }

        [PropertyTest(EnumPropertyName.OffenderID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int OffenderID { get; set; }
        protected override void ExecuteBlock()
        {
            if(AgencyID != -1)
            {
                foreach(var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.AreEqual(AgencyID, equip.AgencyID, $"Agency ID: Expected: {AgencyID}, Actual: {equip.AgencyID}");
                }
            }
            if(EquipmentID != 0)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.AreEqual(EquipmentID, equip.ID, $"Equipment ID: Expected: {EquipmentID}, Actual: {equip.ID}");
                }
            }

            if (IsDamaged)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsTrue(equip.IsDamaged, $"Is Damaged: {equip.IsDamaged}");
                }
            }

            if (!IsDamaged)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsFalse(equip.IsDamaged, $"Is Not Damaged: {equip.IsDamaged}");
                }
            }

            if(EquipmentModels != null && EquipmentModels.Length > 0)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsTrue(EquipmentModels.Contains(equip.Model), $"Equipment model: {equip.Model}");
                }
            }

            if (ReturnDamageData)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsTrue(equip.DamageData != null, "ReturnDamageData: Damage data is empty");
                }
            }

            if (!ReturnDamageData)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsTrue(equip.DamageData == null, "Not ReturnDamageData: Damage data is not empty");
                }
            }
            
           
            if (ReturnOnlyNotAllocated)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsTrue(equip.OffenderID == null, "ReturnOnlyNotAllocated");
                }
            }

            if (!string.IsNullOrEmpty(EquipmentSerialNumber))
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.AreEqual(EquipmentSerialNumber, equip.SerialNumber, $"EquipmentSerialNumber: Expected: {EquipmentSerialNumber}, Actual: {equip.SerialNumber}");
                }
            }

            if(OffenderID != 0)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.AreEqual(OffenderID, equip.OffenderID, $"OffenderID: Expected: {OffenderID}, Actual: {equip.OffenderID}");
                }
            }
           
        }
    }
}
