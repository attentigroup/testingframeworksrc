﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.EquipmentAsserts
{
    /// <summary>
    /// this assert will check that only one authorized mobile return from the get request
    /// </summary>
    public class AddAuthorizedMobileRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgAddAuthorizedMobileRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgAddAuthorizedMobileRequest AddAuthorizedMobileRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetAuthorizedMobileListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]

        public EntMsgGetAuthorizedMobileListResponse GetAuthorizedMobileListResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(1, GetAuthorizedMobileListResponse.AuthorizedMobilesList.Length);

            var AuthorizedMobile = GetAuthorizedMobileListResponse.AuthorizedMobilesList[0];

            Assert.AreEqual(AddAuthorizedMobileRequest.ApplicationID, AuthorizedMobile.ApplicationID);
            Assert.AreEqual(AddAuthorizedMobileRequest.CellularNumber, AuthorizedMobile.CellularNumber);
            Assert.AreEqual(AddAuthorizedMobileRequest.ID, AuthorizedMobile.ID);
            Assert.AreEqual(AddAuthorizedMobileRequest.Model, AuthorizedMobile.Model);
            Assert.AreEqual(AddAuthorizedMobileRequest.OperatingSystem, AuthorizedMobile.OperatingSystem);
        }
    }
}
