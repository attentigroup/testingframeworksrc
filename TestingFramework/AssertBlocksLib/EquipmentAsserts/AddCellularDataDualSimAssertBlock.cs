﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;

#endregion

namespace AssertBlocksLib.EquipmentAsserts
{
    public class AddCellularDataDualSimAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
                Assert.AreEqual(GetCellularDataResponse.CellularData.Length, 2);    
        }
    }


    public class AddCellularDataDualSimAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_1.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreEqual(UpdateCellularDataRequest.CSDPhoneNumber, CellularData.CSDPhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);
        }
    }


    public class AddCellularDataDualSimAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgUpdateCellularDataRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgUpdateCellularDataRequest UpdateCellularDataRequest { get; set; }

        [PropertyTest(EnumPropertyName.EntMsgGetCellularDataResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment_2.EntMsgGetCellularDataResponse GetCellularDataResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var CellularData = GetCellularDataResponse.CellularData[0];

            Assert.AreEqual(UpdateCellularDataRequest.CSDPhoneNumber, CellularData.CSDPhoneNumber);
            Assert.AreEqual(UpdateCellularDataRequest.ProviderID, CellularData.ProviderID);
            Assert.AreEqual(UpdateCellularDataRequest.VoicePhoneNumber, CellularData.VoicePhoneNumber);
        }
    }
}
