﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace AssertBlocksLib.EquipmentAsserts
{
    public class GetEquipmentAssertIsDamageDataBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EntMsgGetEquipmentListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Equipment.EntMsgGetEquipmentListResponse GetEquipmentListResponse { get; set; }

        [PropertyTest(EnumPropertyName.IsDamaged, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool IsDamaged { get; set; }
      
        protected override void ExecuteBlock()
        {         

            if (IsDamaged)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsTrue(equip.IsDamaged, $"Is Damaged: {equip.IsDamaged}");
                }
            }

            if (!IsDamaged)
            {
                foreach (var equip in GetEquipmentListResponse.EquipmentList)
                {
                    Assert.IsFalse(equip.IsDamaged, $"Is Not Damaged: {equip.IsDamaged}");
                }
            }
            
        }
    }
}

