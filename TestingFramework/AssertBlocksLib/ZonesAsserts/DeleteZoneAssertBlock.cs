﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.ZonesAsserts
{
    public class DeleteZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddZoneResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgAddZoneResponse AddZoneResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var zone = GetZonesByEntityIDResponse.ZoneList.FirstOrDefault(x => x.ID == AddZoneResponse.ZoneID);
            Assert.IsTrue(zone == null);
        }
    }
}
