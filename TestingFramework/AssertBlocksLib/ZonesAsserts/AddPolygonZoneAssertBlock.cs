﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.ZonesAsserts
{
    public class AddPolygonZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddPolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgAddPolygonZoneRequest AddPolygonZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var zone = (Zones0.EntPolygon)GetZonesByEntityIDResponse.ZoneList[0];
            if (AddPolygonZoneRequest.BLP != null)
            {
                Assert.IsTrue(zone.BLP.LAT - AddPolygonZoneRequest.BLP.LAT < 0.005);
                Assert.IsTrue(zone.BLP.LON - AddPolygonZoneRequest.BLP.LON < 0.005);
            }
            Assert.AreEqual(AddPolygonZoneRequest.EntityID, zone.EntityID);
            Assert.AreEqual(AddPolygonZoneRequest.EntityType, zone.EntityType);
            Assert.AreEqual(AddPolygonZoneRequest.FullSchedule, zone.FullSchedule);
            Assert.AreEqual(AddPolygonZoneRequest.GraceTime, zone.GraceTime);
            Assert.AreEqual(AddPolygonZoneRequest.IsCurfewZone, zone.IsCurfewZone);
            Assert.AreEqual(AddPolygonZoneRequest.Limitation, zone.Limitation);
            Assert.AreEqual(AddPolygonZoneRequest.Name, zone.Name);
            for(int i=0;i<AddPolygonZoneRequest.Points.Length-1;i++)
            {
                Assert.IsTrue(zone.Points[i].LAT - AddPolygonZoneRequest.Points[i].LAT < 0.005);
                Assert.IsTrue(zone.Points[i].LON - AddPolygonZoneRequest.Points[i].LON < 0.005);
            }
        }
    }
}
