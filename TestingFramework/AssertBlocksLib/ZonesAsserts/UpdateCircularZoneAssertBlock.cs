﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.ZonesAsserts
{
    public class UpdateCircularZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var zone = (Zones0.EntCircular)GetZonesByEntityIDResponse.ZoneList[0];
            if (UpdateCircularZoneRequest.BLP != null)
            {
                Assert.IsTrue(zone.BLP.LAT - UpdateCircularZoneRequest.BLP.LAT < 0.005);
                Assert.IsTrue(zone.BLP.LON - UpdateCircularZoneRequest.BLP.LON < 0.005);
            }
            Assert.AreEqual(UpdateCircularZoneRequest.BufferRadius, zone.BufferRadius);
            Assert.AreEqual(UpdateCircularZoneRequest.EntityID, zone.EntityID);
            Assert.AreEqual(UpdateCircularZoneRequest.EntityType, zone.EntityType);
            Assert.AreEqual(UpdateCircularZoneRequest.FullSchedule, zone.FullSchedule);
            Assert.AreEqual(UpdateCircularZoneRequest.GraceTime, zone.GraceTime);
            Assert.AreEqual(UpdateCircularZoneRequest.IsCurfewZone, zone.IsCurfewZone);
            Assert.AreEqual(UpdateCircularZoneRequest.Name, zone.Name);
            Assert.IsTrue(zone.Point.LAT - UpdateCircularZoneRequest.Point.LAT < 0.005);
            Assert.IsTrue(zone.Point.LON - UpdateCircularZoneRequest.Point.LON < 0.005);
            Assert.AreEqual(UpdateCircularZoneRequest.RealRadius, zone.RealRadius);
            Assert.AreEqual(UpdateCircularZoneRequest.ZoneID, zone.ID);
        }
    }
}
