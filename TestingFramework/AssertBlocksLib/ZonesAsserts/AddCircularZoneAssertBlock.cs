﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace AssertBlocksLib.ZonesAsserts
{
    public class AddCircularZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.AddCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgAddCircularZoneRequest AddCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var zone = (Zones0.EntCircular)GetZonesByEntityIDResponse.ZoneList[0];
            if (AddCircularZoneRequest.BLP != null)
            {
                Assert.IsTrue(zone.BLP.LAT - AddCircularZoneRequest.BLP.LAT < 0.005 );
                Assert.IsTrue(zone.BLP.LON - AddCircularZoneRequest.BLP.LON < 0.005);
            }
            Assert.AreEqual(AddCircularZoneRequest.BufferRadius, zone.BufferRadius, "expected buffer radius {0}, actual buffer radius {1}", AddCircularZoneRequest.BufferRadius, zone.BufferRadius);
            Assert.AreEqual(AddCircularZoneRequest.EntityID, zone.EntityID, "expected entity id " + AddCircularZoneRequest.EntityID + ", actual entity id " + zone.EntityID);
            Assert.AreEqual(AddCircularZoneRequest.EntityType, zone.EntityType);
            Assert.AreEqual(AddCircularZoneRequest.FullSchedule, zone.FullSchedule);
            Assert.AreEqual(AddCircularZoneRequest.GraceTime, zone.GraceTime, "expected grace time {0}, actual grace time {1}", AddCircularZoneRequest.BufferRadius, zone.BufferRadius);
            Assert.AreEqual(AddCircularZoneRequest.IsCurfewZone, zone.IsCurfewZone);
            Assert.AreEqual(AddCircularZoneRequest.Limitation, zone.Limitation);
            Assert.AreEqual(AddCircularZoneRequest.Name, zone.Name);
            Assert.IsTrue(zone.Point.LAT - AddCircularZoneRequest.Point.LAT < 0.005);
            Assert.IsTrue(zone.Point.LON - AddCircularZoneRequest.Point.LON < 0.005);
            Assert.AreEqual(AddCircularZoneRequest.RealRadius, zone.RealRadius, "expected real radius {0}, actual real radius {1}", AddCircularZoneRequest.BufferRadius, zone.BufferRadius);

        }
    }
}
