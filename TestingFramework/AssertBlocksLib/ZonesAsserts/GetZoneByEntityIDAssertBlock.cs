﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.ZonesAsserts
{
    public class GetZoneByEntityIDAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetZonesByEntityIDRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDRequest GetZonesByEntityIDRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }
        protected override void ExecuteBlock()
        {
            if (GetZonesByEntityIDResponse.ZoneList.Length < 1)
            throw new Exception("GetZonesByEntityIDResponse: No zones found in the list!");
            Assert.AreEqual(GetZonesByEntityIDRequest.EntityID, GetZonesByEntityIDResponse.ZoneList[0].EntityID);
            Assert.AreEqual(GetZonesByEntityIDRequest.EntityType, GetZonesByEntityIDResponse.ZoneList[0].EntityType);
            //if(GetZonesByEntityIDRequest.Version == -2)
                Assert.AreEqual(0, GetZonesByEntityIDResponse.ZoneList[0].VersionNo);
            //else if(GetZonesByEntityIDRequest.Version == -1)
            //    Assert.AreEqual(-1, GetZonesByEntityIDResponse.ZoneList[0].VersionNo);
        }
    }
}
