﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion


namespace AssertBlocksLib.OffendersAsserts
{
    public class UpdateZoneRequestAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }



        protected override void ExecuteBlock()
        {

            string ZoneName;
            int EntityID, ZoneIndex = -1;

            if(UpdateCircularZoneRequest != null)
            {
                ZoneName = UpdateCircularZoneRequest.Name;
                EntityID = UpdateCircularZoneRequest.EntityID;
            }
            else if(UpdatePolygonZoneRequest != null)
            {
                ZoneName = UpdatePolygonZoneRequest.Name;
                EntityID = UpdatePolygonZoneRequest.EntityID;
            }
            else
            {
                throw new Exception("UpdateZoneRequestAssertBlock missing one of UpdateCircularZoneRequest / UpdatePolygonZoneRequest");
            }

            for(int i=0; i< GetZonesByEntityIDResponse.ZoneList.Length; i++)
            {
                if(EntityID == GetZonesByEntityIDResponse.ZoneList[i].EntityID)
                {
                    ZoneIndex = i;
                    break;
                }
            }
            var myZone = GetZonesByEntityIDResponse.ZoneList[ZoneIndex];

            Assert.AreEqual(myZone.Name, ZoneName);
        }
    }


    public class UpdateZoneRequestAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones1.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones1.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }



        protected override void ExecuteBlock()
        {

            string ZoneName;
            int EntityID, ZoneIndex = -1;

            if (UpdateCircularZoneRequest != null)
            {
                ZoneName = UpdateCircularZoneRequest.Name;
                EntityID = UpdateCircularZoneRequest.EntityID;
            }
            else if (UpdatePolygonZoneRequest != null)
            {
                ZoneName = UpdatePolygonZoneRequest.Name;
                EntityID = UpdatePolygonZoneRequest.EntityID;
            }
            else
            {
                throw new Exception("UpdateZoneRequestAssertBlock missing one of UpdateCircularZoneRequest / UpdatePolygonZoneRequest");
            }

            for (int i = 0; i < GetZonesByEntityIDResponse.ZoneList.Length; i++)
            {
                if (EntityID == GetZonesByEntityIDResponse.ZoneList[i].EntityID)
                {
                    ZoneIndex = i;
                    break;
                }
            }
            var myZone = GetZonesByEntityIDResponse.ZoneList[ZoneIndex];

            Assert.AreEqual(myZone.Name, ZoneName);
        }
    }


    public class UpdateZoneRequestAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateCircularZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.MsgUpdateCircularZoneRequest UpdateCircularZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones2.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones2.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }



        protected override void ExecuteBlock()
        {

            string ZoneName;
            int EntityID, ZoneIndex = -1;

            if (UpdateCircularZoneRequest != null)
            {
                ZoneName = UpdateCircularZoneRequest.Name;
                EntityID = UpdateCircularZoneRequest.EntityID;
            }
            else if (UpdatePolygonZoneRequest != null)
            {
                ZoneName = UpdatePolygonZoneRequest.Name;
                EntityID = UpdatePolygonZoneRequest.EntityID;
            }
            else
            {
                throw new Exception("UpdateZoneRequestAssertBlock missing one of UpdateCircularZoneRequest / UpdatePolygonZoneRequest");
            }

            for (int i = 0; i < GetZonesByEntityIDResponse.ZoneList.Length; i++)
            {
                if (EntityID == GetZonesByEntityIDResponse.ZoneList[i].EntityID)
                {
                    ZoneIndex = i;
                    break;
                }
            }
            var myZone = GetZonesByEntityIDResponse.ZoneList[ZoneIndex];

            Assert.AreEqual(myZone.Name, ZoneName);
        }
    }
}
