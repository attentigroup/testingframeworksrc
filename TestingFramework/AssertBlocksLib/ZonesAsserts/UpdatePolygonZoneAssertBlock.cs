﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestingFramework.TestsInfraStructure.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.ZonesAsserts
{
    public class UpdatePolygonZoneAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdatePolygonZoneRequest, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Zones0.MsgUpdatePolygonZoneRequest UpdatePolygonZoneRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetZonesByEntityIDResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Zones0.MsgGetZonesByEntityIDResponse GetZonesByEntityIDResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var zone = (Zones0.EntPolygon)GetZonesByEntityIDResponse.ZoneList[0];
            if (UpdatePolygonZoneRequest.BLP != null)
            {
                Assert.IsTrue(zone.BLP.LAT - UpdatePolygonZoneRequest.BLP.LAT < 0.005);
                Assert.IsTrue(zone.BLP.LON - UpdatePolygonZoneRequest.BLP.LON < 0.005);
            }
            Assert.AreEqual(UpdatePolygonZoneRequest.EntityID, zone.EntityID);
            Assert.AreEqual(UpdatePolygonZoneRequest.EntityType, zone.EntityType);
            Assert.AreEqual(UpdatePolygonZoneRequest.FullSchedule, zone.FullSchedule);
            Assert.AreEqual(UpdatePolygonZoneRequest.GraceTime, zone.GraceTime);
            Assert.AreEqual(UpdatePolygonZoneRequest.IsCurfewZone, zone.IsCurfewZone);
            Assert.AreEqual(UpdatePolygonZoneRequest.Name, zone.Name);
            for (int i = 0; i < UpdatePolygonZoneRequest.Points.Length - 1; i++)
            {
                Assert.IsTrue(zone.Points[i].LAT - UpdatePolygonZoneRequest.Points[i].LAT < 0.005);
                Assert.IsTrue(zone.Points[i].LON - UpdatePolygonZoneRequest.Points[i].LON < 0.005);
            }
        }
    }
}
