﻿using CommBox.Infrastructure;
using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;


#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
#endregion


namespace AssertBlocksLib.DevicesAsserts
{
    public class VerifyScheduleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandRule, CommandBase.EMPTY_SUB_COMMAND);
            var commandRule = GpsDeviceProxy.Commands[cmdKey] as CommandRule;

            Assert.AreEqual(GetScheduleResponse.Schedule.CalendarSchedule.Length, commandRule.AddedOneTimeSchedules.Count, 
                "Calander schedual count and command Rule OTS count different");

            //var timeFramesFromApi = GetScheduleResponse.Schedule.CalendarSchedule.
            //    ToDictionary(item => item.ID, item => item);
            //var missingTimeFrmaesInApi = new List<string>();
            //foreach (var ots in commandRule.AddedOneTimeSchedules)
            //{
            //    Schedule0.EntTimeFrame timeFrameFromApi = null;
            //    bool found = timeFramesFromApi.TryGetValue(ots.OneTimeSchedID, out timeFrameFromApi);
            //    if(found == true)
            //    {//verify all data correct
            //        Assert.AreEqual(timeFrameFromApi.FromTime.Value, ots.StartDateTime);
            //        Assert.AreEqual(timeFrameFromApi.ToTime.Value, ots.EndDateTime);

            //    }
            //    else
            //    {
            //        var ostString = ots.ToLog();
            //        missingTimeFrmaesInApi.Add(ostString);
            //        Log.WarnFormat("can't find ots with parameters {0}", ostString);                    
            //    }
            //}
            //if( missingTimeFrmaesInApi.Count > 0 )
            //{
            //    var errorMessage = string.Join(",", missingTimeFrmaesInApi);
            //    throw new System.Exception(string.Format("Can't find the following OTS in the schedual recived from API: {0}", errorMessage));
            //}
        }
    }
}
