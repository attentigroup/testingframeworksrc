﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandTamperThresholdAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandTamperThreshold, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandTamperThreshold;

            Assert.AreEqual(expected.TamperThreshold, cmd.TamperThreshold, "Tamper Threshold not equal");
        }
    }
}
