﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandNoLBSTimerAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicId = GetOffendersResponse.OffendersList[0].ID;
            var expected = dbProxy.GetProtechEOffenderMtdParams(demographicId);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandNoLBSTimer, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandNoLBSTimer;

            Assert.AreEqual(expected.NoLBSTimer, cmd.NoLBSTimer, "No LBS Timer not equal");
        }
    }
}
