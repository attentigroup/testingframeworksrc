﻿using CommBox.Infrastructure;
using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
#endregion
namespace AssertBlocksLib.DevicesAsserts
{
    public class VerifyRulesAssertBlock : CommandBaseAssertBlock
    {

        public static string HWRuleTypeString = "Hardware";

        [PropertyTest(EnumPropertyName.GetGPSRuleConfigurationListResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Configuration0.EntMsgGetGPSRuleConfigurationListResponse GetGPSRuleConfigurationListResponse { get; set; }

        [PropertyTest(EnumPropertyName.MetadataForGPSRules, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntRuleFieldsSetup> MetadataForGPSRules { get; set; }

        protected override void ExecuteBlock()
        {
            var foundRulesData = new List<string>();

            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandRule, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandRule;

            //for each HW that recived by the device from comm-box 
            //get the rule id from the meta-data and compare it to the ID from EntMsgGetGPSRuleConfigurationListResponse.EntGPSRule.ID
            foreach (var commBoxHwRule in cmd.HwRules)
            {
                Log.Debug(string.Format("rule received by commbox - {0}", PropertiesLogging.ToLog(commBoxHwRule)));
                //get the meta-data that fit the HW rule number from the comm-box
                var entGpsRuleMetaDataList = MetadataForGPSRules.FindAll(r => r.IDInProtechDB == commBoxHwRule.HWRuleNumber);
                //if(entGpsRuleMetaDataList.Count == 1 )
                //{
                //    Log.ErrorFormat("Can't find rule meta data that fit HW Rule Id{0} or find more then 1 (entGpsRuleMetaDataList.Count = {1} )", 
                //        commBoxHwRule.HWRuleNumber, entGpsRuleMetaDataList.Count);
                //    continue;
                //}
                
                foreach (var entGpsRuleMetaData in entGpsRuleMetaDataList)
                {
                    //find the coresponding rule from the web
                    var arEntRuleConfig = Array.FindAll(GetGPSRuleConfigurationListResponse.RuleConfigList,
                            delegate (Configuration0.EntGPSRule egr) { return egr.ID == entGpsRuleMetaData.RuleID; });

                    if (arEntRuleConfig.Length == 0)
                    {
                        Log.ErrorFormat("Can't find GPS Rule Configuration that fit Gps Rule Meta Data (RuleID {0} ) or find more then 1 (arEntRuleConfig.Length  = {1} )",
                            entGpsRuleMetaData.RuleID, arEntRuleConfig.Length);
                        continue;
                    }
                    var entRuleConfig = arEntRuleConfig[0];

                    //only then we find the rule from the commbox in the rules from the web- add it to the list
                    foundRulesData.Add(string.Format("commBoxHwRule.HWRuleNumber {0} found in entGpsRuleMetaData as entGpsRuleMetaData.RuleID {1} and found in entRuleConfig.ID {2}.",
                        commBoxHwRule.HWRuleNumber, entGpsRuleMetaData.RuleID, entRuleConfig.ID));
                    //found the rule continue to next HW rule from commBox
                    break;
                }                
            }
            //compare hw rules from device to founded rules list
            Assert.AreEqual(cmd.HwRules.Count, foundRulesData.Count, 
                string.Format("HW rules count {0} and founded rules count {1}", cmd.HwRules.Count, foundRulesData.Count));
        }
    }
}
