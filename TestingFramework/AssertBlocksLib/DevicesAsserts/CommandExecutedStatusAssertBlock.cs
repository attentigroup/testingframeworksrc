﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.DevicesAsserts
{
    /// <summary>
    /// Assert to verify the command execution status based on Network dissues description class
    /// </summary>
    public class CommandExecutedStatusAssertBlock : AssertBaseBlock
    {
        /// <summary>
        /// the object that implement the connection to the communication server.
        /// </summary>
        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        /// <summary>
        /// the requested status of the command
        /// </summary>
        [PropertyTest(EnumPropertyName.CommandExecutedStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnumCommandExecuted CommandExecutedStatus { get; set; }

        /// <summary>
        /// description of the network issue the assert need to verify.
        /// </summary>
        [PropertyTest(EnumPropertyName.GpsDeviceProxyNetworkIssuesDescription, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public NetworkIssuesDescription NetworkIssuesDescription { get; set; }


        protected override void ExecuteBlock()
        {
            //if Command Hangup executed the call completed and pass
            var cmdKey = CommandBase.GenerateKeyFromCommands(NetworkIssuesDescription.Command, NetworkIssuesDescription.SubCommand);
            var command = GpsDeviceProxy.Commands[cmdKey] as ICommand;

            //compare
            Assert.AreEqual(CommandExecutedStatus, command.CommandExecuted ,$" for command key {cmdKey}");
        }
    }
}
