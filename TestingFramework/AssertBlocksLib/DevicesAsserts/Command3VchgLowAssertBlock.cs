﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class Command3VchgLowAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            EntChargeLow expected = dbProxy.GetEntityByArgs<EntChargeLow>(typeof(EntChargeLow), EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.Command3VchgLow, CommandBase.EMPTY_SUB_COMMAND);
            var command3VchgLow = GpsDeviceProxy.Commands[cmdKey] as Command3VchgLow;

            Assert.AreEqual(expected.ChargeLow, command3VchgLow.ChargeLow, "Charge Low not equal");

        }
    }
}
