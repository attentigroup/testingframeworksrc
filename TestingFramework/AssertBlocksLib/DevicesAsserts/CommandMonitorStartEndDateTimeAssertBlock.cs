﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandMonitorStartEndDateTimeAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;
            var expected = dbProxy.GetProtechEOffenderParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandMonitorStartEndDateTime, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandMonitorStartEndDateTime;

            Assert.AreEqual(expected.MonitorStartTimestamp, cmd.MonitorStartTimestamp, "Monitor Start Timestamp not equal");
            Assert.AreEqual(expected.MonitorEndTimestamp, cmd.MonitorEndTimestamp, "Monitor End Timestamp not equal");
        }
    }
}
