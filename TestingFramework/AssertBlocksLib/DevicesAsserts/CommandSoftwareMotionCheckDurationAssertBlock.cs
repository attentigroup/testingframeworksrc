﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandSoftwareMotionCheckDurationAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetEntityByArgs<EntProtechEMTDParameters>(typeof(EntProtechEMTDParameters), EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandSoftwareMotionCheckDuration, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandSoftwareMotionCheckDuration;

            Assert.AreEqual(expected.SoftwareMotionCheckDuration, cmd.SoftwareMotionCheckDuration, "Software Motion Check Duration not equal");
        }
    }
}
