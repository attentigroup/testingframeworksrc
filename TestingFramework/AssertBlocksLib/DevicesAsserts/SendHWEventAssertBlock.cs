﻿using CommBox.Infrastructure;
using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
#endregion


namespace AssertBlocksLib.DevicesAsserts
{
    /// <summary>
    /// compare between the events send to the communication server and the events retrive from the API
    /// </summary>
    public class SendHWEventAssertBlock : CommandBaseAssertBlock
    {
        [PropertyTest(EnumPropertyName.GetEventsResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Events0.EntMsgGetEventsResponse GetEventsResponse { get; set; }

        public Dictionary<EnumRules, string> EnumRules2EventCodeDictionary { get; set; }

        public SendHWEventAssertBlock()
        {
            Init();
        }

        private void Init()
        {
            EnumRules2EventCodeDictionary = new Dictionary<EnumRules, string>();

            EnumRules2EventCodeDictionary.Add(EnumRules.MotionNoGPS, "F01");
        }

        protected override void ExecuteBlock()
        {
            var errorMessages = new List<string>();

            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandSendAllPoints, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandSendAllPoints;

            Assert.IsFalse(GetEventsResponse == null, "GetEventsResponse is null");
            Assert.IsFalse(GetEventsResponse.EventsList == null, "GetEventsResponse.EventsList is null");
            Assert.IsFalse(GetEventsResponse.EventsList.Length == 0, "GetEventsResponse.EventsList is empty");

            Log.DebugFormat("SendHWEventAssertBlock - HW count from command = {0} events from API = {1}", 
                cmd.PointsSent.Count, GetEventsResponse.EventsList.Length);

            foreach (var point in cmd.PointsSent)
            {
                foreach (var hwRuleFromDevice in point.HwRules)
                {
                    EnumRules hwRuleType = (EnumRules)hwRuleFromDevice.HWRuleNumber;

                    var eventCode2Find = EnumRules2EventCodeDictionary[hwRuleType];

                    var eventsFromResponse = Array.FindAll(GetEventsResponse.EventsList,
                        delegate (Events0.EntEvent ev) { return ev.Code.Equals(eventCode2Find, StringComparison.OrdinalIgnoreCase); });

                    if( eventsFromResponse.Length == 0)
                    {
                        errorMessages.Add(string.Format("SendHWEventAssertBlock - didnt find message {0}", eventCode2Find));
                    }
                }
            }
            if( errorMessages.Count > 0 )
            {
                throw new Exception(string.Format("SendHWEventAssertBlock - errors : {0}", string.Join(", ", errorMessages)));
            }
        }
    }
}
