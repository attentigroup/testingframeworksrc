﻿using CommBox.Infrastructure;
using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.DevicesAsserts
{
    /// <summary>
    /// compare the string arived from the communication layer and from the message that was send to the API
    /// </summary>
    public class VerifyActionsSendMessageAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        [PropertyTest(EnumPropertyName.Message, EnumPropertyType.String, EnumPropertyModifier.Mandatory)]
        public string Message { get; set; }

        /// <summary>
        /// use StringAssert.Contains to verify if the message in the received message.
        /// </summary>
        protected override void ExecuteBlock()
        {
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandDisplayMessage, CommandBase.EMPTY_SUB_COMMAND);
            var commandDisplayMessage = GpsDeviceProxy.Commands[cmdKey] as CommandDisplayMessage;

            StringAssert.Contains(commandDisplayMessage.DisplayMessage, Message);
        }
    }
}
