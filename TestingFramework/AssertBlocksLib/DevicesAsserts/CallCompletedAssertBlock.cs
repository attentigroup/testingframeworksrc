﻿using CommBox.Infrastructure;
using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.Device.CommandsHandler;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.DevicesAsserts
{
    /// <summary>
    /// assert to check if the call completed as need
    /// </summary>
    public class CallCompletedAssertBlock : AssertBaseBlock
    {
        /// <summary>
        /// the object that implement the connection to the communication server.
        /// </summary>
        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        /// <summary>
        /// the requested status of the command
        /// </summary>
        [PropertyTest(EnumPropertyName.CommandExecutedStatus, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public EnumCommandExecuted CommandExecutedStatus { get; set; }

        public CallCompletedAssertBlock()
        {
            CommandExecutedStatus = EnumCommandExecuted.CommandExecutedPass;
        }

        /// <summary>
        /// assert to check if the call completed as need
        /// </summary>
        protected override void ExecuteBlock()
        {
            //if Command Hangup executed the call completed and pass
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandHangup, CommandBase.EMPTY_SUB_COMMAND);
            var commandHangup = GpsDeviceProxy.Commands[cmdKey] as CommandHangup;

            //compare
            Assert.AreEqual(CommandExecutedStatus, commandHangup.CommandExecuted);
        }
    }
}
