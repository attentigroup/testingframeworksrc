﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandGPSNormalSaveAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandGPSNormalSave, CommandBase.EMPTY_SUB_COMMAND);
            var commandGPSNormalSave = GpsDeviceProxy.Commands[cmdKey] as CommandNormalGPSSaveInterval;

            Assert.AreEqual(expected.NormalGPSSaveInterval, commandGPSNormalSave.NormalGPSSaveInterval, "Normal GPSSave Interval not equal");
        }
    }

    public class CommandGPSAlarmSaveAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandGPSAlarmSave, CommandBase.EMPTY_SUB_COMMAND);
            var commandGPSAlarmSave = GpsDeviceProxy.Commands[cmdKey] as CommandGPSAlarmSaveInterval;

            Assert.AreEqual(expected.AlarmGPSSaveInterval, commandGPSAlarmSave.AlarmGPSSaveInterval, "Alarm GPSSave Interval not equal");
        }
    }
}
