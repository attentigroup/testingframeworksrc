﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandSetOffenderDemographicIDAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetAssignedDemoID(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandSetOffenderDemographicID, CommandBase.EMPTY_SUB_COMMAND);
            var commandSetOffenderDemographicID = GpsDeviceProxy.Commands[cmdKey] as CommandSetOffenderDemographicID;

            Assert.AreEqual(expected.AssignedDemoID, commandSetOffenderDemographicID.AssignedDemoID, "AssignedDemoID not equal");
        }
    }
}
