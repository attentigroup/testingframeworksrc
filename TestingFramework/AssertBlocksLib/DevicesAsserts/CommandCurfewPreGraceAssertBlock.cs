﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandCurfewPreGraceAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;
            var expected = dbProxy.GetEntityByArgs<EntCurfewPreGrace>(typeof(EntCurfewPreGrace), demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandCurfewPreGrace, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandCurfewPreGrace;

            Assert.AreEqual(expected.CurfewPreGrace, cmd.CurfewPreGrace, "Curfew Pre Grace not equal");
        }
    }
}
