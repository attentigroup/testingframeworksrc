﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.Device;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace AssertBlocksLib.DevicesAsserts
{
    public abstract class CommandBaseAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.EquipmentID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public int EquipmentID { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

    }
}
