﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandPiezoTypeAssertBlock : CommandBaseAssertBlock
    {
        public const string PiezoTypes = "AB";
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPiezoType, CommandBase.EMPTY_SUB_COMMAND);
            var commandPiezoType = GpsDeviceProxy.Commands[cmdKey] as CommandPiezoType;

            StringAssert.Contains(PiezoTypes, expected.PiezoType, "PiezoType({0} is not one of the options ({1})", expected.PiezoType, PiezoTypes);
            Assert.AreEqual(expected.PiezoType[0], commandPiezoType.PiezoType, "Piezo Type not equal");
        }
    }
}
