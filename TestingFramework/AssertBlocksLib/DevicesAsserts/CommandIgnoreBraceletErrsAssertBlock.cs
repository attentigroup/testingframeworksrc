﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandIgnoreBraceletErrsAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetEntityByArgs<EntProtechEMTDParameters>(typeof(EntProtechEMTDParameters), EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandIgnoreBraceletErrors, CommandBase.EMPTY_SUB_COMMAND);
            var commandIgnoreBraceletErrs = GpsDeviceProxy.Commands[cmdKey] as CommandIgnoreBraceletErrs;

            Assert.AreEqual(expected.IgnoreBraceletErrors[0], commandIgnoreBraceletErrs.IgnoreBraceletErrors, "IgnoreBraceletErrors not equal");
        }
    }
}
