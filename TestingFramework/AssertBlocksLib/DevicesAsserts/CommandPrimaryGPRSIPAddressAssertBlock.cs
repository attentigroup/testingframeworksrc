﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandPrimaryGPRSIPAddressAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expectedMTDParameters = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPrimaryGPRSIPAddress, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandPrimaryGPRSIPAddress;

            Assert.AreEqual(expectedMTDParameters.PrimarySDCIPAddress, cmd.PrimarySDCIPAddress, "Primary SDCIP Address not equal");
        }
    }
}
