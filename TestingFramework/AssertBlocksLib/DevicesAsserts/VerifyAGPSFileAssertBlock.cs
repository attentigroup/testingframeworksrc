﻿using AssertBlocksLib.DevicesAsserts;
using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    /// <summary>
    /// the block will get the chuck
    /// </summary>
    public class VerifyAGPSFileAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get received crcs
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPartialCodeLoad, (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisInit);
            var commandPartialCodeLoadInit = GpsDeviceProxy.Commands[cmdKey] as CommandPartialCodeLoadInit;
            var receivedCrc1 = commandPartialCodeLoadInit.FileCrc;

            Assert.IsFalse(0 == receivedCrc1, "CRC received by message SIRFEphemerisInit equals 0");
            //get the file
            cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPartialCodeLoad, (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisCrc);
            var commandPartialCodeLoadCrc = GpsDeviceProxy.Commands[cmdKey] as CommandPartialCodeLoadCrc;
            var receivedCrc2 = commandPartialCodeLoadInit.FileCrc;

            Assert.IsFalse(0 == receivedCrc2, "CRC received by message SIRFEphemerisCrc equals 0");

            Assert.AreEqual(receivedCrc1, receivedCrc2, "CRC received from SIRFEphemerisInit message and from SIRFEphemerisCrc message not the same");
            //build the file
            cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPartialCodeLoad, (EnumProtocolCommands)EnumCodeLoadCommands.SIRFEphemerisCode);
            var commandPartialCodeLoadCode = GpsDeviceProxy.Commands[cmdKey] as CommandPartialCodeLoadCode;

            byte[] EntireAGPSBuffer = BuildAGPSFileBufferFromChunks(commandPartialCodeLoadCode.ChunksList);
            //calc CRC
            var calcCrc = Crc32.GetCrc32(EntireAGPSBuffer);
            //compare calc to received CRC
            Assert.AreEqual(receivedCrc1, calcCrc, "caculated CRC for AGPS file not the same as CRC received");
        }

        private byte[] BuildAGPSFileBufferFromChunks(List<ChunkData> ChunksList)
        {
            byte[]  fileBuffer = null;
            int     fileLength = 0;

            //calc ther size of the file
            ChunksList.ForEach(x => fileLength += x.ChunksBytes.Length);
            //sort the list as the address
            ChunksList.Sort((x, y) => x.ChunkAddress.CompareTo(y.ChunkAddress));

            fileBuffer = new byte[fileLength];

            foreach (var chunk in ChunksList)
            {
                Log.DebugFormat("Adding chunk to address {0}.chunk size {1} total size {2}", 
                    chunk.ChunkAddress, chunk.ChunksBytes.Length, fileLength);
                Buffer.BlockCopy(chunk.ChunksBytes, 0, fileBuffer, chunk.ChunkAddress, chunk.ChunksBytes.Length);
            }
            return fileBuffer;
        }
    }


}
