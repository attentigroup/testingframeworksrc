﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandBraceletRangeAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandBraceletRange, CommandBase.EMPTY_SUB_COMMAND);
            var commandBraceletRange = GpsDeviceProxy.Commands[cmdKey] as CommandBraceletRange;

            Assert.AreEqual(expected.MinValidBraceletRSSI, commandBraceletRange.MinValidBraceletRssi, "MinValidBraceletRssi not equal");
        }
    }
}
