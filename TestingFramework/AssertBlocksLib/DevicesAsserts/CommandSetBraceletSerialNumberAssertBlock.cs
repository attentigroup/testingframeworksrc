﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandSetBraceletSerialNumberAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;
            var expected = dbProxy.GetProtechEOffenderParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandSetBraceletSerialNumber, CommandBase.EMPTY_SUB_COMMAND);
            var commandSetBraceletSerialNumber = GpsDeviceProxy.Commands[cmdKey] as CommandSetBraceletSerialNumber;

            Assert.AreEqual(expected.BraceletID, commandSetBraceletSerialNumber.BraceletID, "Bracelet ID not equal");
        }
    }
}
