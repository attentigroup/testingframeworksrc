﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandSecondayPLGIPAddressAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;

            var expected = dbProxy.GetProtechEOffenderMtdParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandSecondayPLGIPAddress, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandSecondayPLGIPAddress;

            if(string.IsNullOrEmpty(expected.LbsSecondaryIP) == false)
            {
                Assert.AreEqual(expected.LbsSecondaryIP, cmd.SecondayPLGIPAddress, "Seconday PLG IP Address not equal");
            }
            else
            {
                Assert.AreEqual(string.IsNullOrEmpty(expected.LbsSecondaryIP), string.IsNullOrEmpty(cmd.SecondayPLGIPAddress), "Seconday PLG IP Address not equal");
            }
            
        }
    }
}
