﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandBacklightOnTimeAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandBacklightOnTime, CommandBase.EMPTY_SUB_COMMAND);
            var commandBacklightOnTime = GpsDeviceProxy.Commands[cmdKey] as CommandBacklightOnTime;

            Assert.AreEqual(expected.BacklightOnTime, commandBacklightOnTime.BacklightOnTime, "Back light On Time not equal");
        }
    }
}
