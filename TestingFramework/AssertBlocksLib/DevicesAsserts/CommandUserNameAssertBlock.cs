﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AssertBlocksLib.DevicesAsserts
{
    /// <summary>
    /// check that the format the name as done in commbox - \CommBox\Software\CommBox.Data\EntDeviceConfigurationsExtention.cs - method AssembleUsername()
    /// </summary>
    public class CommandUserNameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        protected override void ExecuteBlock()
        {
            //get the user name from the command
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandUserName, CommandBase.EMPTY_SUB_COMMAND);
            var cmdUserName = GpsDeviceProxy.Commands[cmdKey] as CommandUserName;
            if (string.IsNullOrEmpty(cmdUserName.UserName))
            {
                throw new Exception("User name in command null or empty.can't continue to verify user name.");
            }

            //get the user name from the equipment
            string expectedFirstMiddleName = GetOffendersResponse.OffendersList[0].FirstName;
            if( !string.IsNullOrWhiteSpace(GetOffendersResponse.OffendersList[0].MiddleName))
            {
                var fullMiddleName = GetOffendersResponse.OffendersList[0].MiddleName;
                string middleName = !String.IsNullOrWhiteSpace(fullMiddleName) && fullMiddleName.Length >= 6
                    ? fullMiddleName.Substring(0, 6) : fullMiddleName;
                expectedFirstMiddleName = string.Format("{0} {1}",
                    GetOffendersResponse.OffendersList[0].FirstName, middleName);
            }
            //add \0 at the end as this is the way it arrived from the server in case unicode.
            string expectedLastName = string.Format("{0}\0", GetOffendersResponse.OffendersList[0].LastName);
            if(cmdUserName.Encoding == Encoding.ASCII)
            {
                expectedLastName = GetOffendersResponse.OffendersList[0].LastName;
            }


            var subStrings = cmdUserName.UserName.Split('\r');
            //first substring first name
            var actualFirstMiddleName = subStrings[0];
            //second substring last name
            var actualLastName = subStrings[1];
            //compare
            Assert.AreEqual(expectedFirstMiddleName, actualFirstMiddleName, "First Name");
            Assert.AreEqual(expectedLastName, actualLastName, "Last Name");
        }
    }
}
