﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandCallBackIntervalAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expectedMTDParameters = dbProxy.GetProtechEMTDParameters(EquipmentID);
            var demographicID = GetOffendersResponse.OffendersList[0].ID;
            var expectedOffenderMtdParams = dbProxy.GetProtechEOffenderMtdParams(demographicID);

            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandCallBackInterval, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandCallBackInterval;

            Assert.AreEqual(expectedMTDParameters.CallBackIntervalSeconds, cmd.CallBackIntervalSeconds, "CallBack Interval not equal");

            Assert.AreEqual(expectedOffenderMtdParams.InChargerCallBackIntervalSecs, cmd.InChargerCallBackIntervalSecs, "InCharger CallBack Interval Secs not equal");
        }
    }
}
