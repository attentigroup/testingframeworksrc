﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandEnableFeaturesAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandEnableFeatures, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandEnableFeatures;

            //based on \CommBox\Software\CommBox.Data\APIExtensions\EntDeviceConfigurations.cs line 49-50
            Int32 expectedFeaturesMap = 0;
            PacketBuilder.SetBit(ref expectedFeaturesMap, EnumBits.Bit0);
            PacketBuilder.SetBit(ref expectedFeaturesMap, EnumBits.Bit2);

            Assert.AreEqual(expectedFeaturesMap, cmd.FeaturesMap, "FeaturesMap not equal");
        }
    }
}
