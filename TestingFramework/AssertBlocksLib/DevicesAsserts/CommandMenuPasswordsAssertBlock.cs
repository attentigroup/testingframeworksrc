﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandMenuPasswordsAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expectedMTDParameters = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandFactoryMenuPassword, CommandBase.EMPTY_SUB_COMMAND);
            var cmdFactoryMenuPassword = GpsDeviceProxy.Commands[cmdKey] as CommandFactoryMenuPassword;

            Assert.AreEqual(expectedMTDParameters.FactoryMenuPassword, cmdFactoryMenuPassword.FactoryMenuPassword, "Factory Menu Password not equal");

            cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandOfficerMenuPassword, CommandBase.EMPTY_SUB_COMMAND);
            var cmdOfficerMenuPassword = GpsDeviceProxy.Commands[cmdKey] as CommandOfficerMenuPassword;

            Assert.AreEqual(expectedMTDParameters.OfficerMenuPassword, cmdOfficerMenuPassword.OfficerMenuPassword, "Officer Menu Password not equal");
        }
    }
}
