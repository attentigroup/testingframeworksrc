﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandBraceletGoneTimeAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;

            var expected = dbProxy.GetEntityByArgs<EntProtechEMTDParameters>(typeof(EntProtechEMTDParameters), EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandBraceletGoneTime, CommandBase.EMPTY_SUB_COMMAND);
            var commandBraceletGoneTime = GpsDeviceProxy.Commands[cmdKey] as CommandBraceletGoneTime;

            Assert.AreEqual(expected.BraceletNoRxGone, commandBraceletGoneTime.BraceletNoRxGone, "Bracelet No Rx Gone time not equal");
        }
    }
}
