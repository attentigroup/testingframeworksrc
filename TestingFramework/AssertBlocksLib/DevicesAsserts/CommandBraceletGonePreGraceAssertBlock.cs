﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandBraceletGonePreGraceAssertBlock : CommandBaseAssertBlock
    {

        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;
            
            var expected = dbProxy.GetProtechEOffenderMtdParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandBraceletGonePreGrace, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandBraceletGonePreGrace;

            Assert.AreEqual(expected.BraceletGonePreGrace, cmd.BraceletGonePreGrace, "Bracelet Gone Pre Grace not equal");
        }
    }
}
