﻿using CommBox.Infrastructure;
using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandSetUTCOffsetsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        [PropertyTest(EnumPropertyName.GpsDeviceProxy, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public GpsDeviceProxy GpsDeviceProxy { get; set; }

        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;
            var expected = dbProxy.GetDemographicsParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandSetUTCOffsets, CommandBase.EMPTY_SUB_COMMAND);
            var commandSetUTCOffsets = GpsDeviceProxy.Commands[cmdKey] as CommandSetUTCOffsets;

            Assert.AreEqual(expected.DaylightTimeOffset, commandSetUTCOffsets.DaylightTimeOffset, "DaylightTimeOffset not equal");
            Assert.AreEqual(expected.StandardTimeOffset, commandSetUTCOffsets.StandardTimeOffset, "StandardTimeOffset not equal");
        }
    }
}
