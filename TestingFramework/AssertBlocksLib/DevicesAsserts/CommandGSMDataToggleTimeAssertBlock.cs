﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandGSMDataToggleTimeAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var expected = dbProxy.GetProtechEMTDParameters(EquipmentID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandGSMDataToggleTime, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandGSMDataToggleTime;

            var expectedDevicePrimaryNetworkToggleMins = expected.DevicePrimaryNetworkToggleMins * 60;
            Assert.AreEqual(expectedDevicePrimaryNetworkToggleMins, cmd.DevicePrimaryNetworkToggleMins, "Device Primary Network Toggle Mins not equal");
        }
    }
}
