﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.DAL.Entities.CommboxConfiguration;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandPhoneNumbersAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;

            var expected = dbProxy.GetProtechEOffenderMtdParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPhoneNumbers, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandPhoneNumbers;

            var expectedPhoneCount = CalcPhoneCountFromEntProtechEOffenderMtdParams(expected);
            //ignore empty descriptions
            var tempDescriptionList = cmd.MtdDialoutDescriptionList.FindAll((x) => string.IsNullOrEmpty(x) == false);

            Assert.AreEqual(tempDescriptionList.Count, cmd.PhoneCount, "MTD Dialout Description\\Phones List.Count not equal");           
            Assert.AreEqual(expectedPhoneCount, cmd.PhoneCount, "Phone count not equal");

        }

        private int CalcPhoneCountFromEntProtechEOffenderMtdParams(EntProtechEOffenderMtdParams expected)
        {
            int phoneCount              = 0;
            var propertyName            = "PhoneNumber";

            var phoneNumberProeprties = expected.GetType().GetProperties();

            foreach (var property in phoneNumberProeprties)
            {
                if( property.Name.Contains(propertyName))
                {
                    var phoneNumberValue = property.GetValue(expected) as string;
                    if( string.IsNullOrWhiteSpace(phoneNumberValue) == false)
                    {
                        phoneCount++;
                    }
                }
            }
            return phoneCount;
        }
    }
}
