﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandContactMethodAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandContactMethod, CommandBase.EMPTY_SUB_COMMAND);
            var commandContactMethod = GpsDeviceProxy.Commands[cmdKey] as CommandContactMethod;

            Assert.AreEqual(0, commandContactMethod.ContactMethod, "Contact Method not equal");
        }
    }
}
