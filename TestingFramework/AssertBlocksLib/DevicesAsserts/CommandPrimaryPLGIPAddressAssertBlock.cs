﻿using CommBox.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.DAL;
using TestingFramework.Proxies.Device.CommandsHandler.Commands;

namespace AssertBlocksLib.DevicesAsserts
{
    public class CommandPrimaryPLGIPAddressAssertBlock : CommandBaseAssertBlock
    {
        protected override void ExecuteBlock()
        {
            //get the expected from DB
            CommboxConfigurationDBProxy dbProxy = CommboxConfigurationDBProxy.Instance;
            var demographicID = GetOffendersResponse.OffendersList[0].ID;

            var expected = dbProxy.GetProtechEOffenderMtdParams(demographicID);
            //get the actual from device
            var cmdKey = CommandBase.GenerateKeyFromCommands(EnumProtocolCommands.CommandPrimaryPLGIPAddress, CommandBase.EMPTY_SUB_COMMAND);
            var cmd = GpsDeviceProxy.Commands[cmdKey] as CommandPrimaryPLGIPAddress;

            if( string.IsNullOrEmpty(expected.LbsPrimaryIP) == false)
            {
                Assert.AreEqual(expected.LbsPrimaryIP, cmd.PrimaryPLGIPAddress, "Primary PLG IP Address not equal");
            }
            else
            {
                Assert.AreEqual(string.IsNullOrEmpty(expected.LbsPrimaryIP), string.IsNullOrEmpty(cmd.PrimaryPLGIPAddress), "Primary PLG IP Address not equal");
            }
            
        }
    }
}
