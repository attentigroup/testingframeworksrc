﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;


using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;


namespace AssertBlocksLib.ScheduleAsserts
{
    public class DeleteTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }


        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length == 0);

        }
    }

    public class DeleteTimeFrameAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            //var IdList = GetScheduleResponse.Schedule.CalendarSchedule.Where(x => x.ID == TimeframeId);
            //Assert.IsFalse(IdList.Any());
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length == 0);
        }
    }

    public class DeleteTimeFrameAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length == 0);
        }
    }
}
