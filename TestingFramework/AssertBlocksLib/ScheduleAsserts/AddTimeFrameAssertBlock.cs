﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;


namespace AssertBlocksLib.ScheduleAsserts
{
    public class AddTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameResponse AddTimeFrameResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetScheduleResponse.Schedule.CalendarSchedule[0].ID, AddTimeFrameResponse.NewTimeFrameID);

            int index = -1;
            for (int i=0; i< GetScheduleResponse.Schedule.CalendarSchedule.Length && index == -1; i++)
            {
                if (GetScheduleResponse.Schedule.CalendarSchedule[i].ID == AddTimeFrameResponse.NewTimeFrameID)
                    index = i;
            }

            Assert.AreEqual(AddTimeFrameRequest.StartTime, GetScheduleResponse.Schedule.CalendarSchedule[index].FromTime);
            Assert.AreEqual(AddTimeFrameRequest.EndTime, GetScheduleResponse.Schedule.CalendarSchedule[index].ToTime);
        }
    }

    public class AddTimeFrameAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length > 0);

        }
    }

    public class AddTimeFrameAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }
        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length > 0);
        }
    }
}

