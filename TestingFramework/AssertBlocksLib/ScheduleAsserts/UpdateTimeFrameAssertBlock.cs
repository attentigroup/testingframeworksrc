﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Schedule;
using Common.Enum;
using Common.CustomAttributes;

#region API refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endregion

namespace AssertBlocksLib.ScheduleAsserts
{
    public class UpdateTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }


        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            int index = -1;
            for(int i=0; i< GetScheduleResponse.Schedule.CalendarSchedule.Length && index == -1; i++)
            {
                if (GetScheduleResponse.Schedule.CalendarSchedule[i].ID == UpdateTimeFrameRequest.TimeframeID)
                    index = i;
            }

            var getTimeFrame = GetScheduleResponse.Schedule.CalendarSchedule[index];

            Assert.AreEqual(UpdateTimeFrameRequest.StartTime, getTimeFrame.FromTime);
            Assert.AreEqual(UpdateTimeFrameRequest.EndTime, getTimeFrame.ToTime);
            Assert.AreEqual(UpdateTimeFrameRequest.LocationID, getTimeFrame.LocationID);

        }
    }


    public class UpdateTimeFrameAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }


        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getTimeFrame = GetScheduleResponse.Schedule.CalendarSchedule[0];

            Assert.AreEqual(getTimeFrame.FromTime, UpdateTimeFrameRequest.StartTime);
            Assert.AreEqual(getTimeFrame.ToTime, UpdateTimeFrameRequest.EndTime);
            Assert.AreEqual(getTimeFrame.LocationID, UpdateTimeFrameRequest.LocationID);
            Assert.AreEqual(getTimeFrame.ID, UpdateTimeFrameRequest.TimeframeID);
        }
    }


    public class UpdateTimeFrameAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }


        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getTimeFrame = GetScheduleResponse.Schedule.CalendarSchedule[0];

            Assert.AreEqual(getTimeFrame.FromTime, UpdateTimeFrameRequest.StartTime);
            Assert.AreEqual(getTimeFrame.ToTime, UpdateTimeFrameRequest.EndTime);
            Assert.AreEqual(getTimeFrame.LocationID, UpdateTimeFrameRequest.LocationID);
            Assert.AreEqual(getTimeFrame.ID, UpdateTimeFrameRequest.TimeframeID);
        }
    }
}
