﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.ScheduleAsserts
{
    public class UpdateAlcoholTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgUpdateTimeFrameRequest UpdateTimeFrameRequest { get; set; }


        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            int index = -1;
            for (int i = 0; i < GetScheduleResponse.Schedule.MEMSSchedule.Length && index == -1; i++)
            {
                if (GetScheduleResponse.Schedule.MEMSSchedule[i].ID == UpdateTimeFrameRequest.TimeframeID)
                    index = i;
            }

            var getTimeFrame = GetScheduleResponse.Schedule.MEMSSchedule[index];

            Assert.AreEqual(UpdateTimeFrameRequest.StartTime, getTimeFrame.FromTime);
            Assert.AreEqual(UpdateTimeFrameRequest.EndTime, getTimeFrame.ToTime);
            if(UpdateTimeFrameRequest.LocationID!=null)
             Assert.AreEqual(UpdateTimeFrameRequest.LocationID, getTimeFrame.LocationID);
            Assert.AreEqual(UpdateTimeFrameRequest.NumberOfTests, getTimeFrame.NumberOfTests);
            Assert.AreEqual(UpdateTimeFrameRequest.TimeFrameType, getTimeFrame.TimeFrameType);
        }
    }
}
