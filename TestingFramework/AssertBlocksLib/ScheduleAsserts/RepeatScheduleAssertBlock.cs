﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;


namespace AssertBlocksLib.ScheduleAsserts
{
    public class RepeatScheduleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }
        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length > 1);
            Assert.AreEqual(RepeatScheduleRequest.Date, GetScheduleResponse.Schedule.CalendarSchedule[0].FromTime);
            Assert.AreEqual(RepeatScheduleRequest.Date.AddDays(7), GetScheduleResponse.Schedule.CalendarSchedule[1].FromTime);
            
        }
    }

    public class RepeatScheduleAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }
        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length > 1);
            Assert.AreEqual(RepeatScheduleRequest.Date, GetScheduleResponse.Schedule.CalendarSchedule[0].FromTime);
            Assert.AreEqual(RepeatScheduleRequest.Date.AddDays(7), GetScheduleResponse.Schedule.CalendarSchedule[1].FromTime);
        }
    }

    public class RepeatScheduleAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.RepeatScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgRepeatScheduleRequest RepeatScheduleRequest { get; set; }
        protected override void ExecuteBlock()
        {
            var calenderSchedule = GetScheduleResponse.Schedule.CalendarSchedule;
            Assert.IsTrue(calenderSchedule.Length > 1);
            Assert.AreEqual(RepeatScheduleRequest.Date, GetScheduleResponse.Schedule.CalendarSchedule[0].FromTime);
            Assert.AreEqual(RepeatScheduleRequest.Date.AddDays(7), GetScheduleResponse.Schedule.CalendarSchedule[1].FromTime);
        }
    }
}
