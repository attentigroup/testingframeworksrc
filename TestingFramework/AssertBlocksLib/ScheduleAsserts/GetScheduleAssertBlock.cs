﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.ScheduleAsserts
{
    public class GetScheduleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleRequest GetScheduleRequest { get; set; }

        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetScheduleRequest.EntityID, GetScheduleResponse.Schedule.EntityID);
            Assert.AreEqual(GetScheduleRequest.EntityType, GetScheduleResponse.Schedule.EntityType);
            foreach(var timeframe in GetScheduleResponse.Schedule.CalendarSchedule)
            {
                Assert.IsTrue(timeframe.FromTime >= GetScheduleRequest.StartDate);
                Assert.IsTrue(timeframe.ToTime <= GetScheduleRequest.EndDate);
            }
            

        }
    }
}
