﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.ScheduleAsserts
{
    public class AddVoiceTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameResponse AddTimeFrameResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetScheduleResponse.Schedule.VoiceSchedule[0].ID, AddTimeFrameResponse.NewTimeFrameID);

            int index = -1;
            for (int i = 0; i < GetScheduleResponse.Schedule.VoiceSchedule.Length && index == -1; i++)
            {
                if (GetScheduleResponse.Schedule.VoiceSchedule[i].ID == AddTimeFrameResponse.NewTimeFrameID)
                    index = i;
            }

            Assert.AreEqual(AddTimeFrameRequest.StartTime, GetScheduleResponse.Schedule.VoiceSchedule[index].FromTime);
            Assert.AreEqual(AddTimeFrameRequest.EndTime, GetScheduleResponse.Schedule.VoiceSchedule[index].ToTime);
            Assert.AreEqual(AddTimeFrameRequest.EntityID, GetScheduleResponse.Schedule.EntityID);
            Assert.AreEqual(AddTimeFrameRequest.EntityType, GetScheduleResponse.Schedule.EntityType);
            //Assert.AreEqual(AddTimeFrameRequest.LocationID, GetScheduleResponse.Schedule.VoiceSchedule[index].LocationID);
            Assert.AreEqual(AddTimeFrameRequest.TimeFrameType, GetScheduleResponse.Schedule.VoiceSchedule[index].TimeFrameType);
            Assert.AreEqual(AddTimeFrameRequest.NumberOfTests, GetScheduleResponse.Schedule.VoiceSchedule[index].NumberOfTests);
            Assert.AreEqual(AddTimeFrameRequest.VoiceTestCallType, GetScheduleResponse.Schedule.VoiceSchedule[index].CallType);
        }
    }
}
