﻿using AssertBlocksLib.TestingFrameworkAsserts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssertBlocksLib.ScheduleAsserts
{
    public static class ScheduleAssertsFactory
    {
        /// <summary>
        /// "IsWeekly", "TimeFrameType" , "WS760"
        /// </summary>
        /// <returns></returns>
        public static ValidationFaultExceptionAssertBlock ValidateSchedule_IsWeekly_TimeFrameType_WS760()
        {
            return new ValidationFaultExceptionScheduleAssertBlock(new[] { "IsWeekly", "TimeFrameType" }, "WS760");
        }
    }
}
