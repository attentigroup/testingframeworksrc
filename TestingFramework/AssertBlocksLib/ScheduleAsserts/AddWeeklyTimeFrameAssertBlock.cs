﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.ScheduleAsserts
{
    public class AddWeeklyTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameResponse AddTimeFrameResponse { get; set; }

        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgAddTimeFrameRequest AddTimeFrameRequest { get; set; }
        protected override void ExecuteBlock()
        {
            Assert.AreEqual(GetScheduleResponse.Schedule.WeeklySchedule[0].ID, AddTimeFrameResponse.NewTimeFrameID);

            int index = -1;
            for (int i = 0; i < GetScheduleResponse.Schedule.WeeklySchedule.Length && index == -1; i++)
            {
                if (GetScheduleResponse.Schedule.WeeklySchedule[i].ID == AddTimeFrameResponse.NewTimeFrameID)
                    index = i;
            }

            Assert.AreEqual(AddTimeFrameRequest.StartTime, GetScheduleResponse.Schedule.WeeklySchedule[index].FromTime);
            Assert.AreEqual(AddTimeFrameRequest.EndTime, GetScheduleResponse.Schedule.WeeklySchedule[index].ToTime);
            Assert.AreEqual(AddTimeFrameRequest.EntityID, GetScheduleResponse.Schedule.EntityID);
            Assert.AreEqual(AddTimeFrameRequest.EntityType, GetScheduleResponse.Schedule.EntityType);
            Assert.AreEqual(AddTimeFrameRequest.LocationID, GetScheduleResponse.Schedule.WeeklySchedule[index].LocationID);
            Assert.AreEqual(AddTimeFrameRequest.TimeFrameType, GetScheduleResponse.Schedule.WeeklySchedule[index].TimeFrameType);
        }
    }
}
