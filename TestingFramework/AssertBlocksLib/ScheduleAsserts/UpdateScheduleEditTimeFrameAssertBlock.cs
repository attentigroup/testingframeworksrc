﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
namespace AssertBlocksLib.ScheduleAsserts
{
    public class UpdateScheduleEditTimeFrameAssertBlock : AssertBaseBlock
    {
        private dynamic getScheduleResponse;
        private dynamic addTimeFrameRequest;

        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public dynamic GetScheduleResponse
        {
            get { return Convert.ChangeType(getScheduleResponse, GetGetScheduleResponse()); }
            set { getScheduleResponse = Convert.ChangeType(value, GetGetScheduleResponse()); }
        }
        
        [PropertyTest(EnumPropertyName.AddTimeFrameRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]

        public dynamic AddTimeFrameRequest
        {
            get { return Convert.ChangeType(addTimeFrameRequest, GetAddTimeFrameRequest()); }
            set { addTimeFrameRequest = Convert.ChangeType(value, GetAddTimeFrameRequest()); }
        }
        
        protected override void ExecuteBlock()
        {
            var fromTime = GetScheduleResponse.Schedule.CalendarSchedule[0].FromTime;
            var toTime = GetScheduleResponse.Schedule.CalendarSchedule[0].ToTime;
            var originalFromTime = AddTimeFrameRequest.StartTime;
            var originalToTime = AddTimeFrameRequest.EndTime;
            Assert.IsTrue((fromTime != originalFromTime || toTime != originalToTime), $"Schedule was not updated!; Original From time: {originalFromTime}; Original To time: {originalToTime}; Updated From time: {fromTime}; Updated To time: {toTime}");
        }

        //Methods to oveeride
        protected virtual Type GetGetScheduleResponse()
        {
            return typeof(Schedule0.EntMsgGetScheduleResponse);
        }

        protected virtual Type GetAddTimeFrameRequest()
        {
            return typeof(Schedule0.EntMsgAddTimeFrameRequest);
        }
    }

    public class UpdateScheduleEditTimeFrameAssertBlock_1 : UpdateScheduleEditTimeFrameAssertBlock
    {
        protected override Type GetGetScheduleResponse()
        {
            return typeof(Schedule1.EntMsgGetScheduleResponse);
        }

        protected override Type GetAddTimeFrameRequest()
        {
            return typeof(Schedule1.EntMsgAddTimeFrameRequest);
        }
    }

    public class UpdateScheduleEditTimeFrameAssertBlock_2 : UpdateScheduleEditTimeFrameAssertBlock
    {
        protected override Type GetGetScheduleResponse()
        {
            return typeof(Schedule2.EntMsgGetScheduleResponse);
        }

        protected override Type GetAddTimeFrameRequest()
        {
            return typeof(Schedule2.EntMsgAddTimeFrameRequest);
        }
    }

}
