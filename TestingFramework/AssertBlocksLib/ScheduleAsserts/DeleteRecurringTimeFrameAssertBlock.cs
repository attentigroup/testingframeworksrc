﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TestingFramework.TestsInfraStructure.Implementation;


using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;


namespace AssertBlocksLib.ScheduleAsserts
{
    public class DeleteRecurringTimeFrameAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetScheduleResponse.Schedule.SkipDates.Length > 0);
            Assert.AreEqual(GetScheduleResponse.Schedule.SkipDates[0].TimeFrameID, GetScheduleResponse.Schedule.WeeklySchedule[0].ID);
            Assert.IsTrue(GetScheduleResponse.Schedule.SkipDates[0].SkipDate >= GetScheduleResponse.Schedule.WeeklySchedule[0].FromTime);
        }
    }

    public class DeleteRecurringTimeFrameAssertBlock_1 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule1.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetScheduleResponse.Schedule.SkipDates.Length > 0);
            Assert.AreEqual(GetScheduleResponse.Schedule.SkipDates[0].TimeFrameID, GetScheduleResponse.Schedule.WeeklySchedule[0].ID);
            Assert.IsTrue(GetScheduleResponse.Schedule.SkipDates[0].SkipDate >= GetScheduleResponse.Schedule.WeeklySchedule[0].FromTime);
        }
    }

    public class DeleteRecurringTimeFrameAssertBlock_2 : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule2.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            Assert.IsTrue(GetScheduleResponse.Schedule.SkipDates.Length > 0);
            Assert.AreEqual(GetScheduleResponse.Schedule.SkipDates[0].TimeFrameID, GetScheduleResponse.Schedule.WeeklySchedule[0].ID);
            Assert.IsTrue(GetScheduleResponse.Schedule.SkipDates[0].SkipDate >= GetScheduleResponse.Schedule.WeeklySchedule[0].FromTime);
        }
    }
}
