﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

namespace AssertBlocksLib.ScheduleAsserts
{
    public class UpdateScheduleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.UpdateScheduleRequest, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgUpdateScheduleRequest UpdateScheduleRequest { get; set; }


        [PropertyTest(EnumPropertyName.GetScheduleResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Schedule0.EntMsgGetScheduleResponse GetScheduleResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var getTimeFrame = GetScheduleResponse.Schedule.CalendarSchedule[0];

            Assert.AreEqual(UpdateScheduleRequest.EntityID, GetScheduleResponse.Schedule.EntityID);
            Assert.AreEqual(UpdateScheduleRequest.EntityType, GetScheduleResponse.Schedule.EntityType);
            Assert.AreEqual(UpdateScheduleRequest.Timeframes[0].EndTime, getTimeFrame.ToTime);
            Assert.AreEqual(UpdateScheduleRequest.Timeframes[0].StartTime, getTimeFrame.FromTime);
            Assert.AreEqual(UpdateScheduleRequest.Timeframes[0].IsWeekly, getTimeFrame.IsWeekly);
            Assert.AreEqual(UpdateScheduleRequest.Timeframes[0].LocationID, getTimeFrame.LocationID);
            Assert.AreEqual(UpdateScheduleRequest.Timeframes[0].TimeframeID, getTimeFrame.ID);
            Assert.AreEqual(UpdateScheduleRequest.Timeframes[0].TimeFrameType, getTimeFrame.TimeFrameType);

        }
    }
}
