﻿using System;
using System.ServiceModel;
using AssertBlocksLib.TestingFrameworkAsserts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;

public class ValidationFaultExceptionScheduleAssertBlock : ValidationFaultExceptionAssertBlock
{
    public ValidationFaultExceptionScheduleAssertBlock(string[] field, string rule)
        : base(field, rule, typeof(FaultException<Schedule0.ValidationFault>)) { }

    protected override void AssertMessage()
    {
        var blockException = BlockException as FaultException<Schedule0.ValidationFault>;

        if (blockException == null)//it might be the inner exception
            blockException = BlockException.InnerException as FaultException<Schedule0.ValidationFault>;

        var validationError = blockException.Detail.Errors[0];
        Assert.AreEqual(Field.Length, validationError.Field.Length);

        Array.ForEach(Field, e => Array.Exists(validationError.Field, a => a == e));

        Assert.AreEqual(Rule, validationError.Rule);
    }

    public static ValidationFaultExceptionScheduleAssertBlock ValidateSchedule_IsWeekly_TimeFrameType_WS760()
    {       
        return new ValidationFaultExceptionScheduleAssertBlock(new[] { "IsWeekly", "TimeFrameType" }, "WS760");
    }
}