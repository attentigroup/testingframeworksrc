﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_AreCustomFieldsVisibleAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsVisibility, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Dictionary<int, bool> CustomFieldsVisibility { get; set; }

        [PropertyTest(EnumPropertyName.Expected, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ExpectedVisibilityStatus { get; set; }

        protected override void ExecuteBlock()
        {
            foreach (var customField in CustomFields)
            {
                if(CustomFieldsVisibility.Any(x => x.Key == customField.ID) == true)
                {
                    var customFieldElement = CustomFieldsVisibility.First(x => x.Key == customField.ID);
                    Assert.AreEqual(ExpectedVisibilityStatus, customFieldElement.Value, $"Custom field element: {customFieldElement.Key} is not visible");
                }
                else
                {
                    throw new Exception($"Custom field: {customField.ID} not found in CustomFieldsVisibility dictionary!");
                }
                
            }
        }
    }
}
