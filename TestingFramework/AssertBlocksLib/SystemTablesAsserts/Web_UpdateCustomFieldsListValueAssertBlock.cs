﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_UpdateCustomFieldsListValueAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFields, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntCustomFiledListItem> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.TableIdName, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableIdName { get; set; }

        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ValueDescription { get; set; }

        protected override void ExecuteBlock()
        {
            var listValue = CustomFields.FirstOrDefault(x => x.ValueDescription == ValueDescription);
            Assert.IsTrue(listValue != null, $"Value: {ValueDescription} not found in systable");

            //TODO: Noa - Add assert for table ID name
            //Assert.AreEqual(TableIdName, listValue., $"Table name: {} not found in systable");
        }
    }
}
