﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;


namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_VerifyCustomFieldListValuesAreDisplayedInOffenderDetailsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFieldsListValuesVisibility, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Dictionary<string, bool> CustomFieldsListValuesVisibility { get; set; }
        protected override void ExecuteBlock()
        {
            foreach(var customField in CustomFieldsListValuesVisibility)
            {
                Assert.IsTrue(customField.Value, $"Custom field with resource ID: {customField.Key} is not displayed");
            }
            
        }
    }
}
