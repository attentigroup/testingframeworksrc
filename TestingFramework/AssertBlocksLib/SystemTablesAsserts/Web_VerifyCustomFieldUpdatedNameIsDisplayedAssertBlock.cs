﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_VerifyCustomFieldUpdatedNameIsDisplayedAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.IsUpdatedCustomFieldNameExist, EnumPropertyType.None, EnumPropertyModifier.None)]
        public Dictionary<string, bool> IsUpdatedCustomFieldNameExist { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var customField in IsUpdatedCustomFieldNameExist)
            {
                Assert.IsTrue(customField.Value, $"Custom field: {customField.Key} wasn't found");
            }

        }
    }
}
