﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_UpdateCustomFieldsNamesAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsSystemDefinitions { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTypeList { get; set; }
        protected override void ExecuteBlock()
        {
            foreach (var field in CustomFieldsByTypeList)
            {
                var customField = CustomFieldsSystemDefinitions.FirstOrDefault(x => x.ID == field.ID);
                Assert.IsTrue(customField != null);
                Assert.AreEqual(field.FieldType.ToString(), customField.Name);
            }
        }
    }
}
