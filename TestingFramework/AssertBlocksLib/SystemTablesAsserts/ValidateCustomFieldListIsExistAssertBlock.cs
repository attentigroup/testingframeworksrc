﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class ValidateCustomFieldListIsExistAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.EmsSystableDictionary, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Dictionary<EnmDBAction, List<EntSysTableItem>> CustomFieldsListValue { get; set; }

        [PropertyTest(EnumPropertyName.CustomFields, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFiledListItem> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.EnmDBAction, EnumPropertyType.None, EnumPropertyModifier.None)]
        public EnmDBAction ActionType { get; set; }

        protected override void ExecuteBlock()
        {

            var inputCustomFields = CustomFieldsListValue[ActionType];
            foreach(EntSysTableItem row in inputCustomFields)
            {
                var existingRow = CustomFields.FirstOrDefault(x => x.ValueCode == row.ValueCode);
                Assert.IsTrue(existingRow != null, $"Value code: {row.ValueCode} not found in systable");
                Assert.AreEqual(existingRow.ValueDescription, row.Description, $"Description: {row.Description} not found in systable");
                Assert.AreEqual(existingRow.TableID, row.TableID, $"Table ID: {row.TableID} not found in systable");
                Assert.AreEqual(existingRow.Language, row.LanguageCode, $"Language: {row.LanguageCode} not found in systable");
            }
        }
    }
}
