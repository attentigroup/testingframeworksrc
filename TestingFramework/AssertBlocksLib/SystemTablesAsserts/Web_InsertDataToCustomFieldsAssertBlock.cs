﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_InsertDataToCustomFieldsAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.GetOffendersResponse, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public Offenders0.EntMsgGetOffendersResponse GetOffendersResponse { get; set; }

        protected override void ExecuteBlock()
        {
            var offenderCustomFields = GetOffendersResponse.OffendersList[0].CustomFields;
            Assert.AreEqual(CustomFields.Count(), offenderCustomFields.Length);
            foreach(var field in CustomFields)
            {
                var offenderField = offenderCustomFields.FirstOrDefault(x => x.ID == field.ID);
                Assert.IsTrue(offenderField != null, $"Field ID: {field.ID} doesn't exist");
                Assert.AreEqual(field.Name, offenderField.FieldName);
                Assert.IsTrue(offenderField.Value != null, $"Field ID: {field.ID} doesn't have value");
            }            
        }
    }
}
