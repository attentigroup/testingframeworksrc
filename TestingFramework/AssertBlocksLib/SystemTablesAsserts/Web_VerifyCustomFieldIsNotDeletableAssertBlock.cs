﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_VerifyCustomFieldIsNotDeletableAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.Description, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string ValueDescription { get; set; }

        [PropertyTest(EnumPropertyName.IsNotDeletable, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public bool IsNotDeletable { get; set; }
        protected override void ExecuteBlock()
        {
                Assert.IsTrue(IsNotDeletable, $"Is not deletable not true for {ValueDescription}");
        }
    }
}
