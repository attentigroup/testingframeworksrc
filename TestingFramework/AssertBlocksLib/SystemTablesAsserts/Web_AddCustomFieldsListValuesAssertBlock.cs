﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_AddCustomFieldsListValuesAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFields, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<EntCustomFiledListItem> CustomFields { get; set; }

        [PropertyTest(EnumPropertyName.TableID, EnumPropertyType.None, EnumPropertyModifier.None)]
        public string TableID { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.None)]
        public List<string> CustomFieldsListValues { get; set; }

        protected override void ExecuteBlock()
        {
            foreach (var value in CustomFieldsListValues)
            {
                var listValue = CustomFields.FirstOrDefault(x => x.ValueDescription == value);
                Assert.IsTrue(listValue != null, $"Value: {value} not found in systable");
                Assert.AreEqual(TableID, listValue.TableID, $"Expected table ID: {TableID} not equals to actual table ID: {listValue.TableID}");
            }
        }
    }
}
