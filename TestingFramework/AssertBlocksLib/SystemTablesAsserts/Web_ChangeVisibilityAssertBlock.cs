﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using TestingFramework.TestsInfraStructure.Implementation;

namespace AssertBlocksLib.SystemTablesAsserts
{
    public class Web_ChangeVisibilityAssertBlock : AssertBaseBlock
    {
        [PropertyTest(EnumPropertyName.CustomFieldsSystemDefinitions, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsSystemDefinitions { get; set; }

        [PropertyTest(EnumPropertyName.CustomFieldsByTypeLst, EnumPropertyType.None, EnumPropertyModifier.Mandatory)]
        public List<EntCustomFieldSystemDefinition> CustomFieldsByTypeList { get; set; }

        [PropertyTest(EnumPropertyName.ShowInOffenderDetails, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowInOffenderDetails { get; set; }

        [PropertyTest(EnumPropertyName.ShowInOffendersList, EnumPropertyType.None, EnumPropertyModifier.None)]
        public bool ShowInOffendersList { get; set; }
        protected override void ExecuteBlock()
        {
            var visibleCustomFieldsLst = CustomFieldsSystemDefinitions.FindAll(x => x.ShowInOffenderDetails == ShowInOffenderDetails && x.ShowInOffendersList == ShowInOffendersList);
            Assert.AreEqual(visibleCustomFieldsLst.Count(), CustomFieldsByTypeList.Count());
            foreach(var field in CustomFieldsByTypeList)
            {
                Assert.IsTrue(visibleCustomFieldsLst.FirstOrDefault(x => x.ID == field.ID) != null);
            }
        }
    }
}
