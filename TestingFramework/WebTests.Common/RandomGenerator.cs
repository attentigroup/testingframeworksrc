﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.Common
{
    public class RandomGenerator
    {
        static Random random = new Random();
        public static string GenerateString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static int GenerateNumber()
        {
            return random.Next();
        }

        public static int GenerateNumber(int maxValue)
        {
            return random.Next(maxValue);
        }

        public static int GenerateNumber(int min, int max)
        {
            return random.Next(min, max);
        }
    }
}
