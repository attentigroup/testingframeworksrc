﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;
using WebTests.SeleniumWrapper;

namespace WebTests.Common
{
    public static class UIAutomationExtensions
    {
        public static AutomationElement GetUIAutomationElementByName(this AutomationElement aeParentElement, string aeName, TreeScope treeScope, int seconds = 5)
        {
            var condition = new PropertyCondition(AutomationElement.NameProperty, aeName);
            var aElement = WaitForUIAutomationElement(aeParentElement, condition, treeScope, seconds);
            return aElement;
        }

        public static AutomationElement GetUIAutomationElementByAutomationId(this AutomationElement aeParentElement, string automationId, TreeScope treeScope, int seconds = 5)
        {
            var condition = new PropertyCondition(AutomationElement.AutomationIdProperty, automationId);
            var aElement = WaitForUIAutomationElement(aeParentElement, condition, treeScope, seconds);
            return aElement;
        }

        public static AutomationElement GetUIAutomationWindowByPartialName(this AutomationElement parentElement, string partialName, int seconds = 30)
        {
            var condition = new PropertyCondition(AutomationElement.IsEnabledProperty, true);
            var windows = WaitForUIAutomationElements(parentElement, condition, TreeScope.Children);
            List<AutomationElement> WindowsList = new List<AutomationElement>(windows.Cast<AutomationElement>());
            var window = WindowsList.FirstOrDefault(l => l.Current.Name.Contains(partialName));
            return window;
        }

        /// <summary>
        /// To add multiple conditions use: new AndCondition(condition1, condition2)
        /// </summary>
        /// <param name="windowName"></param>
        /// <returns></returns>
        public static AutomationElement GetUIAutomationElementByPropertyCondition(this AutomationElement parentElement, PropertyCondition condition)
        {
            var aElement = WaitForUIAutomationElement(parentElement, condition);

            return aElement;
        }

        /// <summary>
        /// Gets all first level desktop UIAutomation elements
        /// </summary>
        /// <returns></returns>
        public static AutomationElementCollection GetAllWindows(TreeScope treeScope = TreeScope.Children)
        {
            var condition = new PropertyCondition(AutomationElement.IsControlElementProperty, true);
            var allElements = AutomationElement.RootElement.FindAll(treeScope, condition);
            return allElements;
        }

        public static List<AutomationElement> GetAllWindows(this AutomationElement parentElement, TreeScope treeScope = TreeScope.Children)
        {
            var condition = new PropertyCondition(AutomationElement.IsControlElementProperty, true);
            var allElements = AutomationElement.RootElement.FindAll(treeScope, condition);
            return new List<AutomationElement>(allElements.Cast<AutomationElement>());
        }

        /// <summary>
        /// Retrieves the direct children buttons of a parent element
        /// </summary>
        /// <param name="aElement"></param>
        /// <returns></returns>
        public static AutomationElementCollection GetAllButtonsFromUIAutomationElement(this AutomationElement aElement, TreeScope treeScope = TreeScope.Descendants)
        {
            var condition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button);
            var elements = aElement.FindAll(treeScope, condition);
            return elements;
        }

        public static AutomationElement GetUIAutomationButtonByPartialName(this AutomationElement parentElement, string partialName, TreeScope treeScope = TreeScope.Descendants, int seconds = 10)
        {
            var condition = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button);
            var elements = WaitForUIAutomationElements(parentElement, condition, treeScope, seconds);
            var elementList = new List<AutomationElement>(elements.Cast<AutomationElement>());
            var element = elementList.FirstOrDefault(l => l.Current.Name.Contains(partialName));
            return element;
        }

        public static void UIAutomationClick(this AutomationElement UIAutomationButton)
        {
            var pattern = UIAutomationButton.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            pattern.Invoke();
        }

        public static string GetTextUsingUIAutomation(this AutomationElement textBox)
        {
            var textpatternPattern = textBox.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;

            var text = textpatternPattern.Current.Value;

            return text;
        }

        public static void InsertTextUsingUIAutomation(this AutomationElement aElement, string text)
        {
            if (!aElement.Current.IsKeyboardFocusable)
            {
                throw new InvalidOperationException(
                "The control with an AutomationID of "
                + aElement.Current.AutomationId.ToString()
                + "is read-only.\n\n");
            }
            // Once you have an instance of an AutomationElement,  
            // check if it supports the ValuePattern pattern.
            object valuePattern = null;

            // Control does not support the ValuePattern pattern 
            // so use keyboard input to insert content.
            //
            // NOTE: Elements that support TextPattern 
            //       do not support ValuePattern and TextPattern
            //       does not support setting the text of 
            //       multi-line edit or document controls.
            //       For this reason, text input must be simulated
            //       using one of the following methods.
            //       
            StringBuilder feedbackText = new StringBuilder();
            if (!aElement.TryGetCurrentPattern(
                ValuePattern.Pattern, out valuePattern))
            {
                feedbackText.Append("The control with an AutomationID of ")
                    .Append(aElement.Current.AutomationId.ToString())
                    .Append(" does not support ValuePattern.")
                    .AppendLine(" Using keyboard input.\n");

                // Set focus for input functionality and begin.
                aElement.SetFocus();

                // Pause before sending keyboard input.
                Thread.Sleep(100);

                // Delete existing content in the control and insert new content.
                System.Windows.Forms.SendKeys.SendWait("^{HOME}");   // Move to start of control
                System.Windows.Forms.SendKeys.SendWait("^+{END}");   // Select everything
                System.Windows.Forms.SendKeys.SendWait("{DEL}");     // Delete selection
                System.Windows.Forms.SendKeys.SendWait(text);
            }
            // Control supports the ValuePattern pattern so we can 
            // use the SetValue method to insert content.
            else
            {
                feedbackText.Append("The control with an AutomationID of ")
                    .Append(aElement.Current.AutomationId.ToString())
                    .Append((" supports ValuePattern."))
                    .AppendLine(" Using ValuePattern.SetValue().\n");

                // Set focus for input functionality and begin.
                aElement.SetFocus();

                ((ValuePattern)valuePattern).SetValue(text);

            }
            Logger.WriteLine("UIAutomation message: {0}", feedbackText.ToString());
            Thread.Sleep(500);
        }

        private static AutomationElement WaitForUIAutomationElement(AutomationElement aeParentElement, PropertyCondition condition, TreeScope treeScope = TreeScope.Descendants, int seconds = 10)
        {
            var toTime = DateTime.Now.AddSeconds(seconds);
            AutomationElement aElement = null;
            do
            {
                Logger.WriteLine($"UIAutomation message: {DateTime.Now} Searching for element value: {condition.Value}");
                aElement = aeParentElement.FindFirst(treeScope, condition);
                Thread.Sleep(250);
            } while (aElement == null && DateTime.Now < toTime);

            if (DateTime.Now > toTime)
                throw new Exception($"UIAutomation: Failed to find {condition.Value} element");

            return aElement;
        }

        private static AutomationElementCollection WaitForUIAutomationElements(AutomationElement aeParentElement, PropertyCondition condition, TreeScope treeScope = TreeScope.Descendants, int seconds = 5)
        {
            var toTime = DateTime.Now.AddSeconds(seconds);
            AutomationElementCollection aElements = null;
            do
            {
                Logger.WriteLine("UIAutomation message: {0} Searching for element value: {1}", DateTime.Now, condition.Value);
                aElements = aeParentElement.FindAll(treeScope, condition);
                Thread.Sleep(250);
            } while ((aElements == null || aElements.Count == 0) && DateTime.Now < toTime);

            return aElements;
        }
    }
}
