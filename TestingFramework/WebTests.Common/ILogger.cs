﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP.AutomationTests.Infra.Common
{
    interface ILogger
    {
        void Info(string line);

        void Info(object line);

        void Failed(string line);

        void TakeScreeShot(IWebDriver driver);
    }
}
