﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.Events;
using RelevantCodes.ExtentReports;

namespace TP.AutomationTests.Infra.Common
{
    public class ExtentReportsLogger : LoggerBase
    {
        private ExtentReports extent;
        private ExtentTest test;
        public static string FilePathInstance { get; set; }
        Dictionary<string, string> sysInfo = new Dictionary<string, string>();

        public ExtentReportsLogger(string testName):base(testName)
        {
        }
        
        protected override void Initialize()
        {
            string filePath = Path.Combine(GetReportFolder(), "report.html");
            FilePathInstance = filePath;

            extent = new ExtentReports(filePath, false);

            Console.WriteLine($"Web report location: {filePath}");

            SysInfo("Selenium Version", Assembly.GetAssembly(typeof(OpenQA.Selenium.IWebDriver)).GetName().Version.ToString());

            test = extent.StartTest(TestName, "Windows 10 Mobile Rules! ಠ_ಠ");
        }

        public void SysInfo(string sysParameter, string value)
        {
            sysInfo.Add(sysParameter, value);
            extent.AddSystemInfo(sysInfo);
        }
        
        public void Warning(object exception)
        {
            Warning(exception.ToString());
        }

        public void Warning(string exception)
        {
            test.Log(LogStatus.Warning, exception);
        }

        public override void Failed(object exception)
        {
            Failed(exception.ToString());
        }

        public override void Failed(string exception)
        {
            test.Log(LogStatus.Fail, exception);
        }

        public override void Info(object line)
        {
            Info(line.ToString());
        }

        public override void Info(string line)
        {
            test.Log(LogStatus.Info, string.Format(line));
        }

        public override void RegisterEvents(EventFiringWebDriver driver)
        {
            driver.ElementClicked += Driver_ElementClicked;
            //driver.ExceptionThrown += Driver_ExceptionThrown;
        }

        private void Driver_ExceptionThrown(object sender, WebDriverExceptionEventArgs e)
        {
            TakeScreeShot(e.Driver);
        }

        private void Driver_ElementClicked(object sender, WebElementEventArgs e)
        {
            TakeScreeShot(e.Driver);
        }

        protected override void WriteScreenShotLog(string fullPath)
        {
            test.Log(LogStatus.Info, test.AddScreenCapture(fullPath.ToString()));
        }

        public override void Dispose()
        {
            extent.EndTest(test);
            extent.Flush();
            extent.Close();
        }
    }
}
