﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using System;
using WebTests.SeleniumWrapper;

namespace WebTests.Common
{
    public abstract class BrowserInitializer : IBrowser
    {
        protected readonly EventFiringWebDriver _driver;

        public BrowserInitializer(IWebDriver driver)
        {
            _driver = new EventFiringWebDriver(driver);
        }
        public abstract BrowserName Name { get; }

        public void DeleteAllCookies()
        {
            _driver.Manage().Cookies.DeleteAllCookies();
        }

        public void Dispose()
        {
            _driver.Quit();
        }

        public IWebDriver GetWebDriver()
        {
            return _driver;
        }

        public void MaximizeWindow()
        {
            _driver.Manage().Window.Maximize();
        }

        public void NavigateToUrl(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        public void SetPageloadTimeout(int seconds)
        {
            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(seconds);
        }

        public void RegisterToEvents()
        {
            _driver.Navigated += _driver_Navigated;

            //_driver.ExceptionThrown += _driver_ExceptionThrown;
            _driver.ElementClicked += _driver_ElementClicked;
        }

        private void _driver_ElementClicked(object sender, WebElementEventArgs e)
        {
            Logger.TakeScreenshot();
        }

        private void _driver_ExceptionThrown(object sender, WebDriverExceptionEventArgs e)
        {
            Logger.Fail(e.ThrownException.ToString());
            Logger.TakeScreenshot();
        }

        private void _driver_Navigated(object sender, WebDriverNavigationEventArgs e)
        {
            Logger.WriteLine($"Navigating to {_driver.Url}");
            Logger.TakeScreenshot();
        }


    }
}
