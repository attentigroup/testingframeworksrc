﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.Common.Browsers
{
    public class ChromeBrowser : BrowserInitializer
    {
        public ChromeBrowser(IWebDriver driver):base(driver)
        {
            
        }
        public override BrowserName Name => BrowserName.Chrome;
    }
}
