﻿using OpenQA.Selenium;
using WebTests.SeleniumWrapper;

namespace WebTests.Common.Browsers
{
    public class InternetExplorerBrowser : BrowserInitializer
    {
        public InternetExplorerBrowser(IWebDriver driver) : base(driver)
        {

        }
        public override BrowserName Name => BrowserName.IE;
    }
}
