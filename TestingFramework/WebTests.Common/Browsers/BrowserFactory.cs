﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using WebTests.SeleniumWrapper;

namespace WebTests.Common.Browsers
{
    public class BrowserFactory
    {
        BrowserName _browserName;
        public BrowserFactory(BrowserName browserName)
        {
            _browserName = browserName;
        }
        public BrowserInitializer GetInitializeBrowser()
        {
            BrowserInitializer browserInitializer;

            switch (_browserName)
            {
                case BrowserName.IE:
                    browserInitializer = GetIEBrowserInitializer();
                    break;

                case BrowserName.Firefox:
                    browserInitializer = GetFirefoxBrowserInitializer();
                    break;

                case BrowserName.Chrome:
                    browserInitializer = GetChromeBrowserInitializer();
                    break;

                default:
                    throw new Exception($"'{_browserName}' Browser is not supported");

            }
            
            browserInitializer.MaximizeWindow();
            browserInitializer.SetPageloadTimeout(30);
            browserInitializer.RegisterToEvents();
            browserInitializer.DeleteAllCookies();
            return browserInitializer;
        }
        
        private BrowserInitializer GetFirefoxBrowserInitializer()
        {
            FirefoxOptions options = new FirefoxOptions();
            
            options.AcceptInsecureCertificates = true;
            
            var ffDriver = new FirefoxDriver(options);

            Logger.WriteLine($"Firefox version: {ffDriver.Capabilities.Version}");

            var firefoxInitializer = new FirefoxBrowser(ffDriver);
            
            return firefoxInitializer;
        }

        private BrowserInitializer GetIEBrowserInitializer()
        {
            var IEDriver = new InternetExplorerDriver(GetIEOptions());

            Logger.WriteLine($"IE Version: {IEDriver.Capabilities.Version}");

            var IEInitializer = new InternetExplorerBrowser(IEDriver);

            return IEInitializer;
        }

        private InternetExplorerOptions GetIEOptions()
        {
            //IE configurations: https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver#required-configuration
            //Registry.SetValue(
            //    @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BFCACHE",
            //    "iexplore.exe", 0);

            var internetExplorerOptions = new InternetExplorerOptions
            {
                EnsureCleanSession = false,
                EnableNativeEvents = true,//try
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                IgnoreZoomLevel = true,
            };

            return internetExplorerOptions;
        }

        private BrowserInitializer GetChromeBrowserInitializer()
        {
            var chromeDriver = new ChromeDriver();

            Logger.WriteLine($"Chrome Version: {chromeDriver.Capabilities.Version}");

            var chromeInitializer = new ChromeBrowser(chromeDriver);

            return chromeInitializer;
        }
    }
}
