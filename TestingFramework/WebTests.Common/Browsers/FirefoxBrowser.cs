﻿using OpenQA.Selenium;
using WebTests.SeleniumWrapper;

namespace WebTests.Common.Browsers
{
    public class FirefoxBrowser : BrowserInitializer
    {
        public FirefoxBrowser(IWebDriver driver) : base(driver)
        {

        }
        public override BrowserName Name => BrowserName.Firefox;

        
    }
}
