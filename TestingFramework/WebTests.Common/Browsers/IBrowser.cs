﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.Common
{
    internal interface IBrowser : IDisposable
    {
        IWebDriver GetWebDriver();

        void NavigateToUrl(string url);

        void DeleteAllCookies();

        void MaximizeWindow();

        BrowserName Name { get; }

        void RegisterToEvents();
    }
}

