﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebTests.SeleniumWrapper;

namespace WebTests.Common
{
    public static class SeleniumExtensions
    {
        public static BrowserElement SelectFromComboboxByValue(this BrowserElement combobox, string selectionValue)
        {
            SelectElement select = new SelectElement(combobox.GetWebElement());

            if (selectionValue != select.SelectedOption.GetAttribute("value"))
            {
                select.SelectByValue(selectionValue);
            }
            var selected = select.SelectedOption;

            Logger.WriteLine($"Combobox slection is : {selected.Text}");

            return new BrowserElement(combobox.GetWebDriverFromElement(combobox), selected, $"Combobox selection: {selected.Text}", combobox.BrowserName);
        }

        public static void SelectFromComboboxByValueWithStale(this BrowserElement combobox, string selectionValue)
        {
            SelectElement select = new SelectElement(combobox.GetWebElement());

            if (selectionValue != select.SelectedOption.GetAttribute("value"))
            {
                select.SelectByValue(selectionValue);
            }

        }

        public static BrowserElement SelectFromComboboxByIndex(this BrowserElement combobox, int selectionIndex)
        {
            SelectElement select = new SelectElement(combobox.GetWebElement());

            select.SelectByIndex(selectionIndex);

            var selected = select.SelectedOption;

            Logger.WriteLine($"Combobox slection is : {selected.Text}");

            return new BrowserElement(combobox.GetWebDriverFromElement(combobox), selected, $"Combobox selection: {selected.Text}", combobox.BrowserName);
        }

        public static List<BrowserElement> GetOptionList(this BrowserElement combobox)
        {
            var driver = combobox.GetWebDriverFromElement(combobox);
            List<BrowserElement> optionList = new List<BrowserElement>();
            SelectElement select = new SelectElement(combobox.GetWebElement());
            foreach (var item in select.Options.ToList())
            {
                optionList.Add(new BrowserElement(driver, combobox, "combobox", BrowserName.Chrome));
            }
            return optionList;
        }

        public static BrowserElement SelectFromComboboxByText(this BrowserElement combobox, string text)
        {
            SelectElement select = new SelectElement(combobox.GetWebElement());

            select.SelectByText(text);

            var selected = select.SelectedOption;

            Logger.WriteLine($"Combobox slection is : {selected.Text}");

            return new BrowserElement(combobox.GetWebDriverFromElement(combobox), selected, $"Combobox selection: {selected.Text}", combobox.BrowserName);
        }

        /// <summary>
        /// This methods works only with 'select - option' combobox type
        /// </summary>
        /// <param name="combobox"></param>
        /// <param name="partialText"></param>
        /// <returns></returns>
        public static BrowserElement SelectFromComboboxByPartialText(this BrowserElement combobox, string partialText)
        {
            var options = combobox.WaitForElements(By.CssSelector("option"), "options");
            var option = options.FirstOrDefault(o => o.Text.Contains(partialText));
            return option;
        }

        /// <summary>
        /// Use for custom comboboxes
        /// </summary>
        /// <param name="combobox"></param>
        /// <param name="optionsSelector"></param>
        /// <param name="partialText"></param>
        /// <returns></returns>
        public static BrowserElement SelectFromComboboxByPartialText(this BrowserElement combobox, By optionsSelector, string partialText)
        {
            var browser = new Browser(combobox.GetWebDriverFromElement(combobox), combobox.BrowserName, 30);
            var options = browser.WaitForElements(optionsSelector, "options");
            var option = options.FirstOrDefault(o => o.Text.Contains(partialText));
            return option;
        }

        public static void DragAndDropFromToOffset(this BrowserElement element, int offsetX, int offsetY)
        {
            Actions actions = new Actions(element.GetWebDriverFromElement(element.GetWebElement()));

            actions.MoveToElement(element.GetWebElement()).MoveByOffset(offsetX, offsetY).ClickAndHold().Build().Perform();

            actions.Release().Perform();
        }

        public static void DrawPolygon(this BrowserElement element, Point[] Points)
        {
            Actions actions = new Actions(element.GetWebDriverFromElement(element.GetWebElement()));
            

            foreach (var point in Points)
            {
                actions.MoveToElement(element.GetWebElement()).MoveByOffset(point.X, point.Y).Build().Perform();
                actions.Click()/*.Perform()*/;

                //actions.MoveToElement(element.GetWebElement()).MoveByOffset(point.X, point.Y).ClickAndHold().Build().Perform();

                //actions.Release().Perform();
            }
            actions.DoubleClick().Perform();
        }


        public static string SelectFromCustomComboboxByIndex(this BrowserElement parentElement, By comboboxLocator, By optionsLocator, int index)
        {
            var combobox = parentElement.WaitForElement(comboboxLocator, "Combobox");

            combobox.MoveCursorToElement();
            combobox.DoubleClick();
            Thread.Sleep(200);
            var options = parentElement.WaitForElements(optionsLocator, "Option");
            var option = options[index];
            option.MoveCursorToElement(true);
            return option.Text;
        }

        public static string SelectFromCustomComboboxByText(this BrowserElement parentElement, string comboxId, string optionText)
        {
            var combobox = parentElement.WaitForElement(By.XPath($"//input[@id='{comboxId}']/.."), "");
            var input = combobox.WaitForElement(By.TagName("input"), "Input element");
            Wait.Until(() => input.Displayed);
            input.MoveCursorToElement();
            input.DoubleClick();
            input.Text=optionText;
            //Thread.Sleep(200);//Slepp only for animation of combobox!!!
            //var options = combobox.WaitForElements(By.CssSelector("ul>li"), "options");
            // var selectOption = options.FirstOrDefault(x => x.Text == optionText);
            // selectOption.MoveCursorToElement(true);
            // return selectOption.Text;
            return optionText;


            /////////////////
            //var selectOptionCombobox = _container.WaitForElement(By.CssSelector(".ui-multiselect.ui-widget.ui-state-default.ui-corner-all"), "Select option");
            //selectOptionCombobox.Click();
            //var dropDownList = _container.WaitForElement(By.CssSelector(".ui-multiselect-menu.ui-widget.ui-widget-content.ui-corner-all"), "Dropdown list");
            //var selectOptions = dropDownList.WaitForElements(By.TagName("label"), "");
            //foreach (var selection in selections)
            //{
            //    var option = selectOptions.FirstOrDefault(x => x.GetAttribute("title") == selection);
            //    option.Click();
            //}

            //var comboboxCloseBtn = _container.WaitForElement(By.CssSelector(".ui-icon.ui-icon-circle-close"), "Select option");
            //comboboxCloseBtn.Click();
            ///////////





        }

        /// <summary>
        /// Accepts comma delimited locators and returns the first related element as a ValueTuple<List<BrowserElement>, By>
        /// </summary>
        /// <param name="elementWrapper"></param>
        /// <param name="locators"></param>
        /// <returns>ValueTuple<List<BrowserElement>, By></returns>
        public static ValueTuple<List<BrowserElement>, By> WaitForElementsOR(this ElementWrapper elementWrapper, int seconds = 1, params By[] locators)
        {
            var elements = new List<BrowserElement>();
            By selectedLocator = null;

            string description = string.Empty;

            foreach (var locator in locators)
            {
                description = $"Element with locator: '{locator.ToString()}' using WaitForElementOR";

                elements = elementWrapper.WaitForElements(locator, description, minElementsToGet: 0, explicitWaitForSingleElement: seconds);
                selectedLocator = locator;
                if (elements.Any())
                    break;
            }
            if (elements.Any())
                Logger.WriteLine($"{description} found");
            else
            {
                selectedLocator = By.Name("No selector fit");
                Logger.WriteLine("WaitForElementsOR: no element found");
            }

            return (elements, selectedLocator);
        }

        public static bool IsElementVisible(this Browser browser, By elementLocator, string description, int seconds = 1)
        {
            var elements = new List<BrowserElement>();

            elements = browser.WaitForElements(elementLocator, description, minElementsToGet: 0, explicitWaitForSingleElement: seconds);

            return IsElementVisibleInternal((elements, elementLocator));
        }

        public static bool IsElementVisible(this Browser browser, int seconds = 1, params By[] elementLocators)
        {
            var elements = browser.WaitForElementsOR(seconds, elementLocators);

            var isElementsVisible = IsElementVisibleInternal(elements);

            if (isElementsVisible)
                Logger.WriteLine($"Element '{elements.Item1[0].ElementDescription}' visiblity: {isElementsVisible}");
            else
                Logger.WriteLine($"Element with locator {elements.Item2} visibility: False");

            return isElementsVisible;
        }

        private static bool DoSkipRefresh(By[] elementLocators, params string[] exceptionalLocatorSubstring)
        {
            bool isContainingExceptional = false;
            foreach (var item in exceptionalLocatorSubstring)
            {
                if (elementLocators.Any(x => x.ToString().Contains(item)))
                {
                    isContainingExceptional = true;
                    break;
                }
            }

            return isContainingExceptional;
        }

        public static ValueTuple<bool, By> AreAllElementsVisible(this Browser browser, EnmModifyOption enmModifyOption, int seconds = 1, bool isMoveCursorToElement = true, params By[] elementLocators)
        {
            browser.WaitForNotBusyCursor();

            var elements = browser.WaitForElementsOR(seconds, elementLocators);

            var areElementsVisible = AreAllElementsVisibleInternal(elements, enmModifyOption, isMoveCursorToElement);

            //TODO experimental: refresh resets the previous over-scroll
            var doSkipRefresh = DoSkipRefresh(elementLocators, "_LatestEvents_", "OffenderDetails_FilesList_", "GroupsList_MemebersList_", "GroupsList_List_");
            
            if (elementLocators.Any(x => x.ToString().Contains("GridHeader")) && !doSkipRefresh)
                browser.RefreshPage();

            if (areElementsVisible)
                Logger.WriteLine($"Element '{elements.Item1[0].ElementDescription}' visiblity: {areElementsVisible}");
            else
                Logger.WriteLine($"Element with locator {elements.Item2} visibility: False");

            return (areElementsVisible, elements.Item2);
        }

        private static bool IsElementVisibleInternal(ValueTuple<List<BrowserElement>, By> TupleElementsAndLocator)
        {
            if (TupleElementsAndLocator.Item1.Any())
            {
                return TupleElementsAndLocator.Item1.Single().Displayed;
            }
            else
                return false;
        }

        private static bool AreAllElementsVisibleInternal(ValueTuple<List<BrowserElement>, By> TupleElementsAndLocator, EnmModifyOption modifyOption, bool isMoveCursorToElement = true)
        {
            if (TupleElementsAndLocator.Item1.Any())
            {
                var visibleEnabledExposedElements = new List<bool>();

                foreach (var item in TupleElementsAndLocator.Item1)
                {
                    if (isMoveCursorToElement)
                        item.MoveCursorToElement();
                    if (modifyOption == EnmModifyOption.ShowHideOnly)
                        visibleEnabledExposedElements.Add(item.Displayed);
                    else if (modifyOption == EnmModifyOption.EnableDisableOnly)
                        visibleEnabledExposedElements.Add(item.Displayed && item.Enabled);
                    else if (modifyOption == EnmModifyOption.EnableDisableHideOnly)
                        visibleEnabledExposedElements.Add(IsExposed(item));
                }
                //var areAllVisible = TupleElementsAndLocator.Item1.All(x => x.Displayed);
                var isAnyVisible = visibleEnabledExposedElements.Any(x => x == true);

                if (isAnyVisible == true && TupleElementsAndLocator.Item1.Any(x => !x.Displayed))
                    Logger.Warning($"The returned result for locator '{TupleElementsAndLocator.Item2}' in AreAllElementsVisibleInternal method is *true* but some elements are *invisible*");

                return isAnyVisible;
            }
            else
                return false;
        }

        private static bool IsExposed(BrowserElement item)
        {
            var value = item.GetAttribute("value");
            if (value.Contains("*"))
                return false;

            return true;
        }


    }
}
