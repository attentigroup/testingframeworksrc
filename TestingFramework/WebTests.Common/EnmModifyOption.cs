﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.Common
{
    public enum EnmModifyOption : int
    {
        All = 89,
        
        ShowHideOnly = 86,//stars
       
        EnableDisableOnly = 68,//hiden
        
        EnableDisableHideOnly = 72,// visible/invisble
        
        None = 78,
    }
}
