﻿using System;
using System.IO;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TP.AutomationTests.Infra.Common
{
    //Inherit from TargetWithLayout in NLog is used
    public abstract class LoggerBase : ILogger, IDisposable
    {
        protected string TestName { get; set; }
        //public static TestContext _testContext;
        protected LoggerBase(string testName)
        {
            TestName = testName;
            Initialize();
        }

        public abstract void Dispose();
        public abstract void Failed(object line);
        public abstract void Failed(string line);
        public abstract void Info(object line);
        public abstract void Info(string line);
        public abstract void RegisterEvents(EventFiringWebDriver driver);

        protected abstract void Initialize();

        public void TakeScreeShot(IWebDriver driver)
        {
            var fullPath = new StringBuilder();
            var path = GetReportFolder();
            var subFolder = @"\" + TestName;

            fullPath.Append(path).Append(subFolder).Append(@"\").Append("fig").Append(GetTimestamp(DateTime.Now)).Append(".jpg");

            if (!Directory.Exists(path + subFolder))
                Directory.CreateDirectory(path + subFolder);
            try
            {
                var file = ((ITakesScreenshot)driver).GetScreenshot();
                file.SaveAsFile(fullPath.ToString(), ScreenshotImageFormat.Jpeg);
                WriteScreenShotLog(fullPath.ToString());
            }
            catch (Exception ex)
            {
                WebTests.SeleniumWrapper.Logger.Warning($"Taking screenshot failed: {ex}");
            }
        }

        protected abstract void WriteScreenShotLog(string path);


        public static string GetReportFolder()
        {
            var fullPath = Path.GetFullPath(ReportPath);
            return fullPath;
        }

        public static string GetFullPath()
        {
            return Path.Combine(GetReportFolder(), "Report.html");
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssfff");
        }

        public static string ReportPath
        {
            get
            {
                var machineName = Environment.MachineName;
                var reportsBasePath = (machineName == "VMENV3-WEBAPP1" || machineName == "ILWL-RNDTEST1" || machineName == "ILWL-RNDTEST2") ? $@"\\10.10.8.145\Automation\Reports\{machineName}" : @"C:\Temp\ExtentReports";
                var reportPath = Path.Combine(reportsBasePath, DateTime.Now.ToString("yyyy.MM.dd"));

                if (!Directory.Exists(reportPath))
                {
                    try
                    {
                        Directory.CreateDirectory(reportPath);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"*** Creating report folder '{reportPath}' failed! ***");
                        Console.WriteLine(ex);
                        //replace with throw after shared folder assignment
                        reportPath = Path.Combine(@"C:\Temp\ExtentReports", DateTime.Now.ToString("yyyy.MM.dd"));
                    }
                }

                return reportPath;
            }
        }
    }
}
