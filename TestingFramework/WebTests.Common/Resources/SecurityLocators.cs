﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTests.Common.Resources
{
    public static class SecurityLocators
    {
        static SecurityLocators()
        {
            LoadDictionary();
        }
        public static Dictionary<string, By> SecurityApiToLocator { get; set; }

        public static void LoadDictionary()
        {
            //Offender Details
            SecurityApiToLocator = new Dictionary<string, By>();
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_Agency", By.Id("agency_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_DOC", By.Id("dep_of_correction_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_DateCreated", By.Id("create_date_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_DateOfBirth", By.Id("date_of_birth_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_FirstName", By.Id("first_name_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_Gender", By.Id("gender_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_ID", By.Id("ref_id_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_Language", By.Id("language_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_LastName", By.Id("last_name_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_MiddleName", By.Id("middle_name_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_Officer", By.Id("officer_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_ProgramEnd", By.CssSelector("input[id^='program_end_']"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_ProgramStart", By.CssSelector("input[id^='program_start_']"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_SocialSecurity", By.Id("social_security_txt"));
            SecurityApiToLocator.Add("OffenderDetails_Identification_Label_Title", By.Id("title_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_City", By.Id("city_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_Phone", By.Id("phone_number_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_PhoneName", By.Id("phone_description_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_PrefixPhone", By.Id("phone_prefix_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_State", By.Id("state_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_Street", By.Id("street_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_TimeZone", By.Id("timezone_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_Title", By.Id("address_name_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ContactInfoScreen_Label_ZipCode", By.Id("zip_code_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_BaseUnit", By.Id("base_unit_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_OutsideLine", By.Id("landline_ext_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_CourtOrder", By.Id("court_order_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_CurfewUnit", By.Id("base_unit_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_ProgramType", By.Id("program_type_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_Receiver", By.Id("receiver_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_SubType", By.Id("sub_type_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_Transmitter", By.Id("transmitter_txt"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_Victim", By.Id("victim_list_txt"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_DateTime1", By.Id("fieldID7"));//dateID1
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_ListItems1", By.Id("fieldID41"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Numeric1", By.Id("fieldID2"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Text100Characters1", By.Id("fieldID47"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Text12Characters1", By.Id("fieldID12"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Text255Characters1", By.Id("fieldID38"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Text255HyperLink1", By.Id("fieldID55"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Text25Characters1", By.Id("fieldID1"));
            SecurityApiToLocator.Add("OffenderDetails_CustomFields_Label_Text50Characters1", By.Id("fieldID34"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_OffenderID", By.CssSelector("[data-modify='OffenderDetails_ProgramDetails_Label_OffenderID'] input"));


            //GroupDetails
            SecurityApiToLocator.Add("GroupDetails_GridDetails_Header_GroupType", By.Id("GroupTypeField"));
            SecurityApiToLocator.Add("GroupDetails_GridDetails_Header_GroupName", By.Id("GroupNameField"));
            SecurityApiToLocator.Add("GroupDetails_GridDetails_Header_Description", By.Id("GroupDescriptionField"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_VoiceSIM", By.CssSelector("input[id^='cellular_']"));
            SecurityApiToLocator.Add("OffenderDetails_ProgramDetails_Label_Landline", By.CssSelector("input[id^='landline_']"));
            SecurityApiToLocator.Add("GroupDetails_Identification_Label_ProgramStart", By.Id("pStartDisabled"));
            SecurityApiToLocator.Add("GroupDetails_Identification_Label_ProgramEnd", By.Id("pEndDisabled"));


            //TrackerRules
            SecurityApiToLocator.Add("TrackerRules_GridDetails_Label_DisplayMessage", By.Id("isDisplayMessageBox"));
            SecurityApiToLocator.Add("TrackerRules_GridDetails_Label_GracePeriod", By.Id("gracePeriodInput"));
            SecurityApiToLocator.Add("TrackerRules_GridDetails_Label_ImmediateUpload", By.Id("immediateUpload"));
            SecurityApiToLocator.Add("TrackerRules_GridDetails_Label_Led", By.Id(""));
            SecurityApiToLocator.Add("TrackerRules_GridDetails_Label_Vibrate", By.Id("isVibrate"));

            //Offender Config
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_AgencyLevel", By.Id("agencyapplyto"));
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_OfficerLevel", By.Id("officerapplyto"));
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_AgencyNotification", By.Id("agencyFax"));
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_OfficerNotification", By.Id("officerFax"));
            SecurityApiToLocator.Add("OffenderConfig_LBS_Label_EnableLBS", By.Id("chkEnable"));
            SecurityApiToLocator.Add("OffenderConfig_LCD_Label_OfficerPassword", By.Id("officerPassword0"));
            SecurityApiToLocator.Add("OffenderConfig_LCD_Label_EnableOfficerScreen", By.Id("chkOfficerScreenEnable"));
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_Email", By.Id("agencyEmail"));
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_Fax", By.Id("agencyFax"));
            SecurityApiToLocator.Add("OffenderConfig_HandlingProcedures_Label_Pager", By.Id("agencyPager"));

        }
    }
}
