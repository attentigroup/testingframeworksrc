﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    /// <summary>
    /// data generator that expose the last value that was generated
    /// </summary>
    [DataGenerator(EnumPropertyType.ControlledDataGenerator)]
    public class ControlledDataGenerator : IDataGenerator
    {
        public static string LastValueGenerated { get; set; }
        public object GenerateData(string system)
        {
            LastValueGenerated = (string)DataGeneratorFactory.Instance.GenerateData(EnumPropertyType.String, string.Empty);
            return LastValueGenerated;
        }
    }
}
