﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.LBSNoPositionTimeout)]
    public class LBSNoPositionTimeoutDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int LBSNoPositionTimeout = (int)60 * DataGeneratorFactory.Instance.RNG.Next(1, 60);
            return LBSNoPositionTimeout;
        }
    }
}
