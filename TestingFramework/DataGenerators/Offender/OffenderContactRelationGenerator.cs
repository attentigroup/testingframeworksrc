﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.ContactRelation)]
    public class OffenderContactRelationGenerator : StringGeneratorBase, IDataGenerator
    {
        public const int MIN_VALUE = 1;
        public const int MAX_VALUE = 10;

        public OffenderContactRelationGenerator()
        {
            StringLength = MAX_VALUE;
        }
    }
}



   