﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{ 
    [DataGenerator(EnumPropertyType.AddressDescription)]

    public class OffenderAddressDescription : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var stackTrace = new StackTrace();
            string methodName = string.Empty;
            foreach (var stackFrame in stackTrace.GetFrames())
            {

                MethodBase methodBase = stackFrame.GetMethod();
                Object[] attributes = methodBase.GetCustomAttributes(
                                          typeof(TestMethodAttribute), false);

                if (attributes.Length >= 1)
                {
                    methodName = methodBase.Name;
                }
            }
            return methodName;
        
}
    }
}
