﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.LBSRequestTimeout)]
   public class LBSRequestTimeoutDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int LBSRequestTimeout = (int)60 * DataGeneratorFactory.Instance.RNG.Next(1, 5);
            return LBSRequestTimeout;
        }
    }
    
}
