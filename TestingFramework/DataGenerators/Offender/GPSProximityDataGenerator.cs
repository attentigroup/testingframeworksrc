﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.GPSProximity)]
    public class GPSProximityDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int GPSProximity = ( DataGeneratorFactory.Instance.RNG.Next(50, 100));
            return GPSProximity;
        }
    }
}
