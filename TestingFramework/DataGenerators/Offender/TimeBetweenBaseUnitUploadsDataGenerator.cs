﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.TimeBetweenBaseUnitUploads)]
    public class TimeBetweenBaseUnitUploadsDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int TimeBetweenBaseUnitUploads = (DataGeneratorFactory.Instance.RNG.Next(5, 24));
            return TimeBetweenBaseUnitUploads;
        }
    }
    
}
