﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.NumberOfRings)]
    class NumberOfRingsDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int NumberOfRings = (DataGeneratorFactory.Instance.RNG.Next(1, 30));
            return NumberOfRings;
        }
    }
}
