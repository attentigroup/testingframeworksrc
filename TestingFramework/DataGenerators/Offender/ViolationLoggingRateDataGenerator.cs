﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.ViolationLoggingRate)]
    class ViolationLoggingRateDataGenerator : IDataGenerator
    { 
            public object GenerateData(string system)
            {
                int ViolationLoggingRate = (int)5 * DataGeneratorFactory.Instance.RNG.Next(1, 360);
                return ViolationLoggingRate;
            }
        
    }
}
