﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.MiddleName)]
    public class OffenderMiddleNameGenerator : StringGeneratorBase, IDataGenerator
    {
        public const int MIN_VALUE = 1;
        public const int MAX_VALUE = 16;

        public OffenderMiddleNameGenerator()
        {
            StringLength = MAX_VALUE;
        }
    }
}




