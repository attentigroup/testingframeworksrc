﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion


namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.Title)]
    public class OffenderTitleGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (Offenders0.EnmTitle)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(Offenders0.EnmTitle)).Length-1);

        }
    }

    [DataGenerator(EnumPropertyType.Title_1)]
    public class OffenderTitleGenerator_1 : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (Offenders1.EnmTitle)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(Offenders1.EnmTitle)).Length-1);
        }
    }

    [DataGenerator(EnumPropertyType.Title_2)]
    public class OffenderTitleGenerator_2 : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (Offenders2.EnmTitle)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(Offenders0.EnmTitle)).Length-1);
        }
    }
}
