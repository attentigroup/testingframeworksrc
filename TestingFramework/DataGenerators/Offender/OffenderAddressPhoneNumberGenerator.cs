﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.AddressPhoneNumber)]
    public class OffenderAddressPhoneNumberGenerator : IDataGenerator
    {
        public static EntPhone[] GetPhones(int amountOfPhones = 1)
        {
            var phones = new EntPhone[amountOfPhones];
            
            for(int i=0; i< amountOfPhones; i++)
            {
                var phone = new EntPhone();
                phone.PhoneNumber = DataGeneratorFactory.Instance.RNG.Next(100, 999999).ToString();
                phones[i] = phone;
            }
  
            return phones;
            
        }
        public object GenerateData(string system)
        {
            var phones = new EntPhone[1];
            var phone = new EntPhone();
            var dateTimeString = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
            phone.PhoneNumber = dateTimeString;
            phones[0] = phone;
            return phones;

        }
    }
}

