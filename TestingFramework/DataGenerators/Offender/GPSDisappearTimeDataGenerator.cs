﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.GPSDisappearTime)]
    public class GPSDisappearTimeDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int GpsDisappearTime = (int)60 * DataGeneratorFactory.Instance.RNG.Next(1, 60);
            return GpsDisappearTime;
        }
    }
}
