﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.TimeBetweenUploads)]
    class TimeBetweenUploadsDataGenerator : IDataGenerator
    {
        
public object GenerateData(string system)
        {
            int TimeBetweenUploads = (int)5 * DataGeneratorFactory.Instance.RNG.Next(1, 4320);
            return TimeBetweenUploads;
        }
    }
}
