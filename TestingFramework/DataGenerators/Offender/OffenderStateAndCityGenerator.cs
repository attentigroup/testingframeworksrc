﻿using Common.CustomAttributes;
using Common.Enum;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;
using Resources0 = TestingFramework.Proxies.EM.Interfaces.Resources12_0;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.StateId)]
    public class OffenderStateAndCityGenerator : IDataGenerator
    {
        private static List<string> _stateIdList = null;
        private static readonly object Lockobject = new object();
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static List<string> StateIdList
        {
            get
            {
                if (_stateIdList == null)
                {
                    lock (Lockobject)
                    {
                        if (_stateIdList == null)
                        {
                            _stateIdList = GetStates();
      
                        }
                    }
                }
                return _stateIdList;
            }
        }
 
        private static List<string> GetStates()
        {
            var getStatesResponse = APIResourcesProxy.Instance.GetStatesList(new Resources0.EntMsgGetStateListRequest()
            {
                CountryID = null
            }
            );
            List<string> states = getStatesResponse.StateList.Select(o => o.StateID).Distinct().ToList();
            return states;
        }  

        public static string GetCityByStateId(string stateId)
        {
            var getCitiesRequest = new Resources0.EntMsgGetCitiesListRequest()
            {
                StateID = stateId
            };
            var getCitiesResponse = APIResourcesProxy.Instance.GetCitiesList(getCitiesRequest);
            Dictionary<string, Resources0.EntCity> dicCities = getCitiesResponse.CitiesList[stateId];//.Select(o => o.CityID).Distinct().ToList();
            List<string> cities = new List<string>();
            foreach (var val in dicCities)
            {
                cities.Add(val.Value.CityID);
            }
            int officerRandIndex = DataGeneratorFactory.Instance.RNG.Next(cities.Count - 1);
            var cityId = cities[officerRandIndex];
            return cityId;
        }
        public object GenerateData(string system)
        {
            int statesCount = StateIdList.Count;
            int randIndex = DataGeneratorFactory.Instance.RNG.Next(statesCount - 1);
            string stateId = StateIdList[randIndex];
            return stateId;
        }
    }
}
