﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
     
    [DataGenerator(EnumPropertyType.TransmitterDisappearTime)]
    public class TransmitterDisappearTimeDataGenerator : IDataGenerator
    {

        /// <summary>
        /// This Data generator relate to RF configurtion Tests
        /// </summary>
        /// <param name="system"></param>
        /// <returns></returns>
        public object GenerateData(string system)
        {
            int TransmitterDisappearTime = (DataGeneratorFactory.Instance.RNG.Next(1, 60));
            return TransmitterDisappearTime;
        }
    }
}
