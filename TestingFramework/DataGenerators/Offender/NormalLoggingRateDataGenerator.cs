﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.NormalLoggingRate)]
   public class NormalLoggingRateDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int NormalLoggingRate = (int)5 * DataGeneratorFactory.Instance.RNG.Next(360, 720);
            return NormalLoggingRate;
        }
    }
}
