﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Interfaces;


namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.DateOfBirth)]
    public class OffenderDateOfBirthGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            DateTime start = DateTime.Today.AddYears(-87);
            Random gen = new Random();
            int range = ((TimeSpan)(DateTime.Today.AddDays(-1) - start)).Days;
            var dateOfBirth = start.AddDays(gen.Next(range));
            return dateOfBirth;
        }
    }
}
