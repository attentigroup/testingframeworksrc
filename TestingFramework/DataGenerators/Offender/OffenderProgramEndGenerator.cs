﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.ProgramEnd)]
    public class OffenderProgramEndGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var programEnd = DateTime.Now.AddYears(2).AddDays(-1);
            return programEnd;
        }
    }
}
