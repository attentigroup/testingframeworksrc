﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Reflection;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.Gender)]
    public class OffenderGenderGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (Offenders0.EnmGender)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(Offenders0.EnmGender)).Length-1);
        }
    }

    [DataGenerator(EnumPropertyType.Gender_1)]
    public class OffenderGenderGenerator_1 : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (Offenders1.EnmGender)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(Offenders1.EnmGender)).Length-1);

        }
    }

    [DataGenerator(EnumPropertyType.Gender_2)]
    public class OffenderGenderGenerator_2 : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (Offenders2.EnmGender)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(Offenders2.EnmGender)).Length-1);

        }
    }
}
