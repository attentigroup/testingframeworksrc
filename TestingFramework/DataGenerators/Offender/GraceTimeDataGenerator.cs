﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
   
    [DataGenerator(EnumPropertyType.GraceTime)]
    public class GraceTimeDataGenerator : IDataGenerator
    {
        /// <summary>
        /// This Data generator relate to RF configurtion Tests
        /// </summary>
        /// <param name="system"></param>
        /// <returns></returns>
        public object GenerateData(string system)
        {
            int GraceTime = (DataGeneratorFactory.Instance.RNG.Next(1, 60));
            return GraceTime;
        }
    }
}
