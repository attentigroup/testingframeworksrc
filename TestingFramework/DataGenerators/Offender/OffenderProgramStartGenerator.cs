﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.ProgramStart)]
    public class OffenderProgramStartGenerator : IDataGenerator
    { 
        public object GenerateData(string system)
        {
            var programStart = DateTime.Now; 
            return programStart;
        }
        
    }
}
