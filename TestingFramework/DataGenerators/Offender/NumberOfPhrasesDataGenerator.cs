﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.NumberOfPhrases)]
    class NumberOfPhrasesDataGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            int NumberOfPhrases = (DataGeneratorFactory.Instance.RNG.Next(3, 8));
            return NumberOfPhrases;
        }
    }
    
}
