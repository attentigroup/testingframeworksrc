﻿using Common.CustomAttributes;
using Common.Enum;
using System.Text;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.ZipCode)]
    public class OffenderZipCodeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            string CHARS = "1234567890";
            var builder = new StringBuilder();
            for (var i = 0; i < 5; i++)
            {
                var currentIndex = DataGeneratorFactory.Instance.RNG.Next(1, CHARS.Length);
                var ch = CHARS[currentIndex];
                builder.Append(ch);
            }
            return builder.ToString();

        }
    }
}





