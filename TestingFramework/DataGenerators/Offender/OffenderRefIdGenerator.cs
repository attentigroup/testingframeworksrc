﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Offender
{
    [DataGenerator(EnumPropertyType.OffenderRefID)]
    public class OffenderRefIdGenerator : StringGeneratorBase, IDataGenerator
    {
        public const int MIN_VALUE = 1;
        public const int MAX_VALUE = 12;

        public OffenderRefIdGenerator()
        {
            StringLength = MAX_VALUE;
        }
    }
}
