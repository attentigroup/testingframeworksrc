﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.Proxies.API.Trails;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;

namespace DataGenerators.Offender
{
    public class OffenderWithTrailIDGenerator : IDataGenerator
    {
        private static List<int> _offenderWithTrailIdList = null;

        private static Dictionary<int, List<int>> _dicOffenderWithTrailAgencyAndOfficer = null;


        public static List<int> OffenderWithTrailIdList
        {
            get
            {
                _offenderWithTrailIdList = GetOffenderWithTrailIDList();
                return _offenderWithTrailIdList;
            }

        }

        public static Dictionary<int, List<int>> DicOffenderWithTrailAgencyAndOfficer
        {
            get
            {
                _dicOffenderWithTrailAgencyAndOfficer = GetAgenciesAndOfficersDictionary();
                return _dicOffenderWithTrailAgencyAndOfficer;
            }
        }


        public static int GetOffenderWithTrailAgencyID()
        {
            int agencyCount = DicOffenderWithTrailAgencyAndOfficer.Count;
            int randIndex = DataGeneratorFactory.Instance.RNG.Next(agencyCount - 1);
            var randAgencyAndOfficers = _dicOffenderWithTrailAgencyAndOfficer.ElementAt(randIndex);
            int agencyId = randAgencyAndOfficers.Key;
            return agencyId;

        }

        public static int GetOffenderWithTrailOfficerID(int agencyID)
        {
            int officerId = 0;
            if (_dicOffenderWithTrailAgencyAndOfficer.Any())
            {
                officerId = GetOOfficerID(_dicOffenderWithTrailAgencyAndOfficer, agencyID);
            }
            else
                officerId = GetOOfficerID(DicOffenderWithTrailAgencyAndOfficer, agencyID);
            return officerId;
        }


        private static int GetOOfficerID(Dictionary<int, List<int>> dic, int agencyID)
        {
            int officerId = 0;
            if (dic.ContainsKey(agencyID))
            {
                var officers = _dicOffenderWithTrailAgencyAndOfficer[agencyID];
                int officerRandIndex = DataGeneratorFactory.Instance.RNG.Next(officers.Count - 1);
                officerId = officers[officerRandIndex];
            }
            return officerId;
        }

        /// <summary>
        /// get offender IDs from the last 3 month geographic locations 
        /// </summary>
        /// <returns></returns>
        public static List<int> GetOffenderWithTrailIDList()
        {
            //intial request to get geographic location from API
            Trails0.EntMsgGetGeographicLocationsRequest getGeographicLocationsRequest = new Trails0.EntMsgGetGeographicLocationsRequest();
            getGeographicLocationsRequest.Limit = 200;
            getGeographicLocationsRequest.EndTime = DateTime.Now;
            getGeographicLocationsRequest.StartTime = getGeographicLocationsRequest.EndTime.Value.AddDays(-1);

            //intial location response 
            Trails0.EntMsgGetGeographicLocationsResponse locationsResponse;
            //defines number of iterations
            int counter = 30;

            do
            {
                locationsResponse = TrailsProxy.Instance.GetGeographicLocations(getGeographicLocationsRequest); //get locations

                //prepare for next iteration- will sample location every 10 days
                getGeographicLocationsRequest.EndTime = getGeographicLocationsRequest.EndTime.Value.AddDays(-10);
                getGeographicLocationsRequest.StartTime = getGeographicLocationsRequest.StartTime.Value.AddDays(-10);
                counter--;
            } while (!locationsResponse.Locations.Any() && counter > 0);

            if (!locationsResponse.Locations.Any())
                throw new Exception("No geographic locations were found in last 3 months");

            return locationsResponse.Locations.Select(x => x.OffenderID).Distinct().ToList();
        }

        //public static List<int> GetOffenderWithTrailIDList1()
        //{
        //    List<int> OffenderIdWithTrailIDList = new List<int>();
        //    var getGeographicLocationsRequest = new TestingFramework.Proxies.EM.Interfaces.Trails12_0.EntMsgGetGeographicLocationsRequest();
        //    var retrivalEndTime = DateTime.Now.AddDays(-30);
        //    getGeographicLocationsRequest.EndTime = DateTime.Now;
        //    getGeographicLocationsRequest.Limit = 200;


        //    while (OffenderIdWithTrailIDList.Count == 0 && getGeographicLocationsRequest.EndTime > retrivalEndTime)
        //    {
        //        getGeographicLocationsRequest.StartTime = getGeographicLocationsRequest.EndTime.Value.AddDays(-1);
        //        var getGeographicLocations = TrailsProxy.Instance.GetGeographicLocations(getGeographicLocationsRequest);

        //        if (getGeographicLocations.Locations.Length > 0)
        //        {
        //            foreach (var location in getGeographicLocations.Locations)
        //            {
        //                if (OffenderIdWithTrailIDList.Contains(location.OffenderID))
        //                    continue;
        //                OffenderIdWithTrailIDList.Add(location.OffenderID);

        //            }
        //            break;
        //        }
        //        getGeographicLocationsRequest.EndTime = getGeographicLocationsRequest.StartTime;
        //    }
        //    return OffenderIdWithTrailIDList;
        //}

        public static Dictionary<int, List<int>> GetAgenciesAndOfficersDictionary()
        {
            Dictionary<int, List<int>> OffenderWithTrailAgencyAndOfficerDictionary = new Dictionary<int, List<int>>();
            foreach (int offenderId in OffenderWithTrailIdList)
            {
                var getOffender = OffendersProxy.Instance.GetOffenders(new Offenders0.EntMsgGetOffendersListRequest()
                {
                    OffenderID = new Offenders0.EntNumericParameter { Operator = Offenders0.EnmNumericOperator.Equal, Value = offenderId }
                });

                if (OffenderWithTrailAgencyAndOfficerDictionary.ContainsKey(getOffender.OffendersList[0].AgencyID) == false)
                {
                    OffenderWithTrailAgencyAndOfficerDictionary.Add(getOffender.OffendersList[0].AgencyID, new List<int>());
                }
                OffenderWithTrailAgencyAndOfficerDictionary[getOffender.OffendersList[0].AgencyID].Add(getOffender.OffendersList[0].OfficerID);
            }
            return OffenderWithTrailAgencyAndOfficerDictionary;
        }

        public object GenerateData(string system)
        {
            int offendersCount = OffenderWithTrailIdList.Count;
            int randIndex = DataGeneratorFactory.Instance.RNG.Next(offendersCount - 1);
            int offenderId = _offenderWithTrailIdList[randIndex];
            return offenderId;
        }
    }


}
