﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    public abstract class BoolGeneratorBase
    {
        public int Probability { get; set; }

        public object GenerateData(string system)
        {
            return NextBool(DataGeneratorFactory.Instance.RNG, Probability);
        }
        protected bool NextBool(Random rng, int truePercentage = 50)
        {
            return rng.NextDouble() < truePercentage / 100.0;
        }
    }


    /// <summary>
    /// generate bool value with specified probability (in percentage)
    /// </summary>
    [DataGenerator(EnumPropertyType.Bool)]
    public class BoolGenerator : BoolGeneratorBase, IDataGenerator
    {
        public const int PROBABILITY = 50;

        public BoolGenerator()
        {
            Probability = PROBABILITY;
        }
    }
}
