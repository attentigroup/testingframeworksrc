﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    [DataGenerator(EnumPropertyType.NullableInt32)]
    public class NullableInt32Generator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return new Nullable<Int32>(DataGeneratorFactory.Instance.RNG.Next());
        }
    }
}
