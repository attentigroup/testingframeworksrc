﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    [DataGenerator(EnumPropertyType.Double)]
    public class DoubleGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return DataGeneratorFactory.Instance.RNG.NextDouble();
        }
    }
}
