﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using System.Configuration;

namespace DataGenerators.Equipment
{
    [DataGenerator(EnumPropertyType.ProviderID)]
    public class ProviderIDGenerator : IDataGenerator
    {
        private static List<int> _providerIdList = null;

        public static List<int> ProviderIDList
        {
            get
            {
                if (_providerIdList == null)
                {
                    _providerIdList = GetProviderIDList();                            

                }
                return _providerIdList;
            }
        }

        private static List<int> GetProviderIDList()
        {
            var providers = APIResourcesProxy.Instance.GetCellularCommunicationProviders().ProvidersList;
            var providerIDList = new List<int>();
            foreach(var provider in providers)
            {
                providerIDList.Add(provider.ID.Value);
            }
            return providerIDList;
        }

        private static int _providerIndex = 0;

        public object GenerateData(string system)
        {
            if (_providerIndex >= ProviderIDList.Count)
            {
                _providerIndex = 0;
            }
            var providerId = _providerIdList[_providerIndex];
            _providerIndex++;

            return providerId;

        }
    }
}
