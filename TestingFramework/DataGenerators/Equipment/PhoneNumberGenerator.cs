﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    /// <summary>
    /// Phone number generator
    /// This generator will retrive all cellular data and generate phone number 
    /// that not used by the system at the moment
    /// </summary>
    [DataGenerator(EnumPropertyType.PhoneNumber)]
    public class PhoneNumberDataGenerator : BasePhoneNumberDataGenerator, IDataGenerator
    {
        public object GenerateData(string system)
        {
            var phoneNumber = GetFreePhoneNumber();
            return phoneNumber;
        }
    }
}
