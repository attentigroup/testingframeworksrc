﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    [DataGenerator(EnumPropertyType.FailureDate)]
    public class FailureDateGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return DateTime.Now.AddDays(-2);
        }
    }
}
