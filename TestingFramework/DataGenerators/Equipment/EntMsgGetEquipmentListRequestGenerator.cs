﻿using Common.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    [DataGenerator(Common.Enum.EnumPropertyType.EntMsgGetEquipmentListRequest)]
    public class EntMsgGetEquipmentListRequestGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var entMsgGetEquipmentListRequest = new EntMsgGetEquipmentListRequest()
            {
                ReturnOnlyNotAllocated = false,
                ReturnDamageData = true,
                Models = new EnmEquipmentModel[] { EnmEquipmentModel.One_Piece_GPS, EnmEquipmentModel.RF_Curfew_Dual_E4 }

            };
            return entMsgGetEquipmentListRequest;
        }
    }
}
