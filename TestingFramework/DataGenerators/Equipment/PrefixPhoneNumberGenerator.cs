﻿using Common.CustomAttributes;
using Common.Enum;
using log4net;
using System.Collections.Generic;
using System.Reflection;
using TestingFramework.Proxies.API.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;

namespace DataGenerators.Equipment
{
    /// <summary>
    /// base class for phone number and prefix phone generators
    /// </summary>
    abstract public class BasePhoneNumberDataGenerator
    {
        /// <summary>
        /// Log interface
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// the number of digits for the phone number.
        /// </summary>
        public const int DIGIT_COUNT = 7;

        private Dictionary<string, string> _prefixPhones = new Dictionary<string, string>();
        private Dictionary<string, string> _phoneNumbers = new Dictionary<string, string>();

        private string _lastPrefixPhoneGenerated = string.Empty;

        private void UpdateCellularData()
        {
            var getCellularDataRequest = new Equipment0.EntMsgGetCellularDataRequest();
            var getCellularDataResponse = EquipmentProxy.Instance.GetCellularData(getCellularDataRequest);

            Log.Debug($"Retrived {getCellularDataResponse.CellularData.Length} CellularData records");
            _prefixPhones.Clear();
            _phoneNumbers.Clear();

            foreach (var cellularData in getCellularDataResponse.CellularData)
            {
                //prefix
                if( string.IsNullOrEmpty(cellularData.VoicePrefixPhone) == false && _prefixPhones.ContainsKey(cellularData.VoicePrefixPhone) == false)
                {
                    _prefixPhones.Add(cellularData.VoicePrefixPhone, cellularData.VoicePrefixPhone);
                }
                if(string.IsNullOrEmpty(cellularData.DataPrefixPhone) == false && _prefixPhones.ContainsKey(cellularData.DataPrefixPhone) == false)
                {
                    _prefixPhones.Add(cellularData.DataPrefixPhone, cellularData.DataPrefixPhone);
                }
                //phone number
                if(string.IsNullOrEmpty(cellularData.VoicePhoneNumber) == false && _phoneNumbers.ContainsKey(cellularData.VoicePhoneNumber) == false)
                {
                    _phoneNumbers.Add(cellularData.VoicePhoneNumber, cellularData.VoicePhoneNumber);
                }
                if (string.IsNullOrEmpty(cellularData.DataPhoneNumber) == false && _phoneNumbers.ContainsKey(cellularData.DataPhoneNumber) == false)
                {
                    _phoneNumbers.Add(cellularData.DataPhoneNumber, cellularData.DataPhoneNumber);
                }
            }
        }

        public string GetPrefixPhone()
        {
            UpdateCellularData();

            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var keyList = new List<string>(_prefixPhones.Keys);
            var randomPrefixIndex = intGenerator.GenerateData(0, keyList.Count - 1);

            var prefixPhone = _prefixPhones[keyList[randomPrefixIndex]];

            if (prefixPhone == _lastPrefixPhoneGenerated)
            {
                randomPrefixIndex = intGenerator.GenerateData(0, keyList.Count - 1);
                prefixPhone = _prefixPhones[keyList[randomPrefixIndex]];
            }
            _lastPrefixPhoneGenerated = prefixPhone;

            return prefixPhone;
        }

        public string GenerateNumber(int digitCount)
        {
            var random = DataGeneratorFactory.Instance.RNG;
            string r = "";
            int i;
            for (i = 1; i < digitCount; i++)
            {
                r += random.Next(1, 9).ToString();
            }
            return r;
        }

        public string GetFreePhoneNumber()
        {
            UpdateCellularData();
            var result = string.Empty;
            bool find = false;

            while (find == false)
            {
                var randomNumber = GenerateNumber(DIGIT_COUNT);
                if (_phoneNumbers.ContainsKey(randomNumber) == false)
                {//we find it
                    find = true;
                    result = randomNumber;
                }
            }
            return result;
        }
    }
    /// <summary>
    /// data generator for prefix.
    /// This data generator will retrive all cellular data and will select one from the exiting prefix.
    /// </summary>
    [DataGenerator(EnumPropertyType.PrefixPhoneNumber)]
    public class PrefixPhoneDataGenerator : BasePhoneNumberDataGenerator, IDataGenerator
    {
        public object GenerateData(string system)
        {
            var prefix = GetPrefixPhone();
            return prefix;
        }
    }

}
