﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    [DataGenerator(EnumPropertyType.FailureType)]
    public class FailureTypeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return (EnmFailureType)DataGeneratorFactory.Instance.RNG.Next(Enum.GetNames(typeof(EnmFailureType)).Length - 1);
        }
    }
}
