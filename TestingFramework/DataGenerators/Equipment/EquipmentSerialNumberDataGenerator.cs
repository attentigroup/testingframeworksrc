﻿using log4net;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.Proxies.API.Equipment;


using Equipment_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using System.Collections.Generic;
using Common.CustomAttributes;
using Common.Enum;
using System.Reflection;
using System;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.PropertiesHandeling;
using System.Linq;

namespace DataGenerators.Equipment
{
    //based on CommBox\Software\CommunicationGateway.Message\SMARTMessage.cs
    /*
     *         private EnumDeviceModule GetDeviceModel(Int32 deviceID)
        {
            EnumDeviceModule eDeviceModule = EnumDeviceModule.Unknown;
            if (deviceID >= 34000000L && deviceID <= 34099999L)
            {
                eDeviceModule = EnumDeviceModule.OnePiece_Generation_0;One_Piece_GPS
            }
            else if (deviceID >= 34200000L && deviceID <= 34299999L)
            {
                eDeviceModule = EnumDeviceModule.OnePiece_Generation_2;One_Piece_GPS_RF
            }
            else if (deviceID >= 34300000L && deviceID <= 34399999L)
            {
                eDeviceModule = EnumDeviceModule.OnePiece_Generation_3_9;One_Piece_GPS_RF_G39
            }
            else if (deviceID >= 34600000L && deviceID <= 34699999L)
            {
                eDeviceModule = EnumDeviceModule.OnePiece_Generation_4_i;One_Piece_GPS_RF_G4i
            }
            else if (deviceID >= 35000000L && deviceID <= 35099999L)
            {
                eDeviceModule = EnumDeviceModule.TwoPiece_Generation_0_6; - Two_Piece_GPS
            }
            else if (deviceID >= 35400000L && deviceID <= 35499999L)
            {
                eDeviceModule = EnumDeviceModule.TwoPiece_Generation_4;
            }
            else if (deviceID >= 35600000L && deviceID <= 35699999L)
            {
                eDeviceModule = EnumDeviceModule.TwoPiece_Generation_6;Two_Piece_3G
            }
            else if (deviceID >= 50400000L && deviceID < 50499999L)
            {
                eDeviceModule = EnumDeviceModule.BaseUnit;
            }

            return eDeviceModule;
        }


        and based on \E4App\Branches\Version-12.0.0\Application\Dmatek.BLL\Equipment\EquipmentBLL.cs, method ValidateImportReceiver

        ...
        ...
        bool updateProtocol = addEquipmentData.ProtocolType == EnmProtocolType.NotDefined;
                switch (model)
                {
                    case EnmEquipmentModel.RCVR_WMTD_Two_Way_RF:
                        allowedPrefix = new List<string>() { "342" };
                        allowedProtocol = new List<EnmProtocolType>() { EnmProtocolType.P0, EnmProtocolType.P5 };
                        if (updateProtocol)
                            addEquipmentData.ProtocolType = EnmProtocolType.P5;
                        break;
                    case EnmEquipmentModel.RCVR_WMTD_MAT:
                        allowedPrefix = new List<string>() { "343" };
                        allowedProtocol = new List<EnmProtocolType>() { EnmProtocolType.P0, EnmProtocolType.P5, EnmProtocolType.P8 };
                        if (updateProtocol)
                            addEquipmentData.ProtocolType = EnmProtocolType.P5;
                        break;
                    case EnmEquipmentModel.RCVR_WMTD_MAT_4_i:
                        allowedPrefix = new List<string>() { "346" };
                        allowedProtocol = new List<EnmProtocolType>() { EnmProtocolType.P5, EnmProtocolType.P8 };
                        if (updateProtocol)
                            addEquipmentData.ProtocolType = EnmProtocolType.P5;
                        break;
                        ...
                        ...
                        ...
                        }
                  }


     * */
    /// <summary>
    /// base class for all data generators for equipment SN.
    /// </summary>
    public abstract class EquipmentSerialNumberDataGeneratorBase
    {
        /// <summary>
        /// Log interface
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// the model type that the data generator will retrive equipment based on to find free SN
        /// </summary>
        public Equipment_0.EnmEquipmentModel EnmEquipmentModel { get; set; }

        public long MinValue { get; set; }

        public long MaxValue { get; set; }

        /// <summary>
        /// the implementation of the Generate data for all equipments
        /// </summary>
        /// <param name="system"></param>
        /// <returns></returns>
        public object GenerateData(string system)
        {
            return GetFreeSerialNumber();
        }

        private static string _lastValueUsed = string.Empty;

        /// <summary>
        /// find free SN from the list of equipments
        /// </summary>
        /// <returns></returns>
        protected virtual string GetFreeSerialNumber()
        {
        //    string result = string.Empty;
        //    var dicSn = new Dictionary<string, Equipment_0.EntEquipmentBase>();

        //    var EquipmentList = GetEquipmentList();
        //    foreach (var equipment in EquipmentList)
        //    {
        //        if (dicSn.ContainsKey(equipment.SerialNumber) == false)
        //            dicSn.Add(equipment.SerialNumber, equipment);
        //    }
        //    var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
        //    long randomStartingPoint = intGenerator.GenerateData((int)MinValue, (int)MaxValue);
        //    for (long iCurrentValue = randomStartingPoint; iCurrentValue <= MaxValue; iCurrentValue++)
        //    {
        //        var sCurrentValue = iCurrentValue.ToString();
        //        if (sCurrentValue == _lastValueUsed)
        //        {
        //            Log.Debug($"random generator generate the same value as last time({sCurrentValue}).continue to generate another value.");
        //            continue;
        //        }
        //        var ContainsKey = dicSn.ContainsKey(sCurrentValue);
        //        if (ContainsKey == false)
        //        {//find free SN
        //            result = sCurrentValue;
        //            _lastValueUsed = sCurrentValue;
        //            break;
        //        }
        //    }
            //return result;
            ///
            var equipmentRepository = new HashSet<string>();

            var equipmentList = GetEquipmentList();

            foreach (var equipment in equipmentList)
            {
                equipmentRepository.Add(equipment.SerialNumber);
            }

            var _intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            long _randomStartingPoint = _intGenerator.GenerateData((int)MinValue, (int)MaxValue);

            bool isAvailable = false;
            long currentValue;
            do
            {
                currentValue = _randomStartingPoint;

                isAvailable = equipmentRepository.Add(currentValue.ToString());
            }
            while (!isAvailable);

            return currentValue.ToString();
        }

        /// <summary>
        /// get the equipment list based on the filter defined by the inhereted class.
        /// </summary>
        /// <returns></returns>
        protected Equipment_0.EntEquipmentBase[] GetEquipmentList()
        {
            var entMsgGetEquipmentListRequest = new Equipment_0.EntMsgGetEquipmentListRequest();
            //entMsgGetEquipmentListRequest.Models = new Equipment_0.EnmEquipmentModel[] { EnmEquipmentModel };
            //Log.Info($"going to get equipments based on filter:{PropertiesLogging.ToLog(entMsgGetEquipmentListRequest)}");

            var entMsgGetEquipmentListResponse = EquipmentProxy.Instance.GetEquipmentList(entMsgGetEquipmentListRequest);
            var results = entMsgGetEquipmentListResponse.EquipmentList;
            Log.Info($"return {results.Length} equipments");
            return results;
        }
    }

    /// <summary>
    /// special implementation of dictionary for numeric serial numbers
    /// </summary>
    public class SerialNumbersDictionary : Dictionary<int, Equipment_0.EntEquipmentBase> {}

    /// <summary>
    /// data generator for serial  numbers that combined from letters and numbers
    /// </summary>
    public abstract class EquipmentCombinedSerialNumberDataGeneratorBase : EquipmentSerialNumberDataGeneratorBase
    {
        /// <summary>
        /// the length of the serial number
        /// </summary>
        public int ChararctersLength { get; set; }

        /// <summary>
        /// CTor for 
        /// </summary>
        public EquipmentCombinedSerialNumberDataGeneratorBase()
        {
            ChararctersLength = 6;//default
        }
        /// <summary>
        /// get the equipments and order them to dictionary of letters and numeric values
        /// </summary>
        /// <returns></returns>
        protected override string GetFreeSerialNumber()
        {
            var result = string.Empty;

            var dicSn = FillEquipmentDictionary();
            result = GenerateSN(dicSn);           

            return result;
        }

        //get random letter and find free number
        private string GenerateSN(Dictionary<char, SerialNumbersDictionary> dicSn)
        {
            var     result = string.Empty;
            var     intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;

            //in case dictionary is empty add one char
            if( dicSn.Keys.Count == 0)
            {
                Log.Debug("Equipments dictionary empty. adding default \'A\' as key");
                dicSn.Add('A', new SerialNumbersDictionary());
            }
            var     keyList = new List<char>(dicSn.Keys);
            var     randomCharIndex = intGenerator.GenerateData(0, keyList.Count - 1);

            //get random list based on the first char
            char randomChar = keyList[randomCharIndex];
            SerialNumbersDictionary snd = dicSn[randomChar];
            for (int i = (int)MinValue; i < MaxValue; i++)
            {
                if( snd.ContainsKey(i) == false)
                {//find free number.use it.
                    var numderWithTrillingZeros = i.ToString($"D{ChararctersLength - 1}");
                    result = $"{randomChar}{numderWithTrillingZeros}";
                    Log.Debug($"Find free number {result}.");
                    break;
                }
            }
            return result;
        }

        private Dictionary<char, SerialNumbersDictionary> FillEquipmentDictionary()
        {
            var     EquipmentList = GetEquipmentList();
            var     dicSn = new Dictionary<char, SerialNumbersDictionary>();

            foreach (var equipment in EquipmentList)
            {
                SerialNumbersDictionary snd = null;
                //if the first char is letter use it
                bool isLetter = !String.IsNullOrEmpty(equipment.SerialNumber) && Char.IsLetter(equipment.SerialNumber[0]);
                if( isLetter == true)
                {
                    if (!dicSn.ContainsKey(equipment.SerialNumber[0]))
                    {
                        dicSn.Add(equipment.SerialNumber[0], new SerialNumbersDictionary());
                    }
                    snd = dicSn[equipment.SerialNumber[0]];
                    //get the number from the serial number
                    var snNumeric = Int32.MinValue;
                    if (Int32.TryParse(equipment.SerialNumber.Substring(1, equipment.SerialNumber.Length - 1), out snNumeric) == false)
                    {
                        Log.Debug($"Unable to parse equipment serial number {equipment.SerialNumber}.continue to next equipment.");
                        continue;
                    }
                    if (snd.ContainsKey(snNumeric) == false)
                    {
                        snd.Add(snNumeric, equipment);
                    }
                }                
            }
            return dicSn;
        }
    }


    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6)]
    public class EquipmentSerialNumberDataGenerator_TwoPiece_Generation_6 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_TwoPiece_Generation_6()
        {
         //   EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Two_Piece_3G;
            MinValue = 35600000L;
            MaxValue = 35699999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_4)]
    public class EquipmentSerialNumberDataGenerator_TwoPiece_Generation_4 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_TwoPiece_Generation_4()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Two_Piece_GPS;
            MinValue = 35400000L;
            MaxValue = 35499999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6)]
    public class EquipmentSerialNumberDataGenerator_TwoPiece_Generation_0_6 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_TwoPiece_Generation_0_6()
        {
            //EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Two_Piece_GPS;
            MinValue = 35000000L;
            MaxValue = 35099999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_0)]
    public class EquipmentSerialNumberDataGenerator_OnePiece_Generation_0 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_OnePiece_Generation_0()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.One_Piece_GPS;
            MinValue = 34000000L;
            MaxValue = 34099999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2)]
    public class EquipmentSerialNumberDataGenerator_OnePiece_Generation_2 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_OnePiece_Generation_2()
        {
            //EnmEquipmentModel = Equipment_0.EnmEquipmentModel.One_Piece_GPS_RF;
            MinValue = 34200000L;
            MaxValue = 34299999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9)]
    public class EquipmentSerialNumberDataGenerator_OnePiece_Generation_3_9 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_OnePiece_Generation_3_9()
        {
            //EnmEquipmentModel = Equipment_0.EnmEquipmentModel.One_Piece_GPS_RF_G39;
            MinValue = 34300000L;
            MaxValue = 34399999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i)]
    public class EquipmentSerialNumberDataGenerator_OnePiece_Generation_4_i : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_OnePiece_Generation_4_i()
        {
            //EnmEquipmentModel = Equipment_0.EnmEquipmentModel.One_Piece_GPS_RF_G4i;
            MinValue = 34600000L;
            MaxValue = 34699999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_7)]
    public class EquipmentSerialNumberDataGenerator_TwoPiece_Generation_7 : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_TwoPiece_Generation_7()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Two_Piece_3G_V7;
            MinValue = 35800000L;
            MaxValue = 35899999L;
        }
    }


    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_BTX)]
    public class EquipmentSerialNumberDataGenerator_BTX : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_BTX()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.BTX;
            MinValue = 10000;
            MaxValue = UInt16.MaxValue;
        }
    }

    /// <summary>
    /// max DEU serial number length is 6 characters
    /// min char count = 5
    /// max value 65535
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit)]
    public class EquipmentSerialNumberDataGenerator_Beacon_Curfew_Unit : EquipmentSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_Beacon_Curfew_Unit()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Beacon_Curfew_Unit;
            MinValue = 10000L;//5 chars
            MaxValue = UInt16.MaxValue;//65535
        }
    }

    /// <summary>
    /// Transmitter DV serial number validation:
    /// max serial number length is 6 characters
    /// if has 6 characters and the first one is numeric
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV)]
    public class EquipmentSerialNumberDataGenerator_Transmitter_DV : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_Transmitter_DV()
        {
            //EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Transmitter_DV;
            MinValue = 10000;
            MaxValue = UInt16.MaxValue;
        }
    }

    /// <summary>
    /// RF_Curfew_Cell_E3 serial number validation:
    /// max serial number length is 6 characters
    /// if has 6 characters and the first one is numeric
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Cell_E3)]
    public class EquipmentSerialNumberDataGenerator_RF_Curfew_Cell_E3 : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_RF_Curfew_Cell_E3()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.RF_Curfew_Cell_E3;
            MinValue = 10000;
            MaxValue = UInt16.MaxValue;
        }
    }
    

    /// <summary>
    /// RF_Curfew_Dual_E4 serial number validation:
    /// max serial number length is 6 characters
    /// if has 6 characters and the first one is numeric
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4)]
    public class EquipmentSerialNumberDataGenerator_RF_Curfew_Dual_E4 : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_RF_Curfew_Dual_E4()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.RF_Curfew_Dual_E4;
            MinValue = 100000L;
            MaxValue = 650000L;//UInt16.MaxValue;
        }
    }

    /// <summary>
    /// Transmitter_860 serial number validation:
    /// max serial number length is 6 characters
    /// if has 6 characters and the first one is numeric
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_860)]
    public class EquipmentSerialNumberDataGenerator_Transmitter_860 : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_Transmitter_860()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Transmitter_860;
            MinValue = 10000;
            MaxValue = UInt16.MaxValue;
        }
    }

    /// <summary>
    /// Transmitter_890 serial number validation:
    /// max serial number length is 6 characters
    /// if has 6 characters and the first one is numeric
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_890_P6)]
    public class EquipmentSerialNumberDataGenerator_Transmitter_890_P6 : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_Transmitter_890_P6()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Transmitter_890;
            MinValue = 400000L;
            MaxValue = 649999L;
        }
    }

    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_890_P2_P0)]
    public class EquipmentSerialNumberDataGenerator_Transmitter_890_P2_P0 : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_Transmitter_890_P2_P0()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Transmitter_890;
            MinValue = 1L;
            MaxValue = 65000L;
        }
    }

    /// <summary>
    /// Transmitter_Triple_Tamper (TRX 700) serial number validation:
    /// max serial number length is 6 characters
    /// if has 6 characters and the first one is numeric
    /// </summary>
    [DataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper)]
    public class EquipmentSerialNumberDataGenerator_Transmitter_Triple_Tamper : EquipmentCombinedSerialNumberDataGeneratorBase, IDataGenerator
    {
        public EquipmentSerialNumberDataGenerator_Transmitter_Triple_Tamper()
        {
           // EnmEquipmentModel = Equipment_0.EnmEquipmentModel.Transmitter_Triple_Tamper;
            MinValue = 10000;
            MaxValue = UInt16.MaxValue;
        }
    }

    









}
