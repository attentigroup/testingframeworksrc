﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    [DataGenerator(EnumPropertyType.AppVersion)]
    class MobileAppVersionGenerator : IDataGenerator
    {
        #region Singleton
        private static MobileAppVersionGenerator _mobileAppVersionGenerator = null;
        private int v;

        public static MobileAppVersionGenerator Instance
        {
            get
            {
                if (_mobileAppVersionGenerator == null)
                {
                    _mobileAppVersionGenerator = new MobileAppVersionGenerator(42);
                }

                return _mobileAppVersionGenerator;
            }
        }

        public MobileAppVersionGenerator()
        {
            if(Instance.AppVersions == null)
            {
                Instance.AppVersions = new Dictionary<string, string>();
                //TODO: need to read available versions from central data source.
                Instance.AppVersions.Add("12.1.0.6", "12.1.0.6");
                Instance.AppVersions.Add("12.1.0.7", "12.1.0.7");
                Instance.AppVersions.Add("12.1.0.8", "12.1.0.8");
            }
        }

        public MobileAppVersionGenerator(int v)
        {
            this.v = v;
        }
        #endregion Singleton

        public Dictionary<string,string> AppVersions { get; set; }
        public object GenerateData(string system)
        {
            int randIndex = DataGeneratorFactory.Instance.RNG.Next(Instance.AppVersions.Count - 1);
            string randKey = Instance.AppVersions.Keys.ToList()[randIndex];
            return Instance.AppVersions[randKey];
        }
    }
}
