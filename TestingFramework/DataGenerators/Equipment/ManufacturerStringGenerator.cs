﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    [DataGenerator(EnumPropertyType.Manufacturer)]
    public class ManufacturerStringGenerator : StringGeneratorBase, IDataGenerator
    {
        public const int MaximumLengthOfManufacturer = 10;
        public ManufacturerStringGenerator()
        {
            StringLength = MaximumLengthOfManufacturer;
        }
    }
}
