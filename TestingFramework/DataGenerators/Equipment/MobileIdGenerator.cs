﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Equipment
{
    /// <summary>
    /// Unique ID of the mobile device.
    /// </summary>

    [DataGenerator(EnumPropertyType.MobileID)]
    public class MobileIdGenerator : IDataGenerator
    {
        /// <summary>
        /// Unique ID of the mobile device.
        /// </summary>
        /// <param name="system"></param>
        /// <returns></returns>
        public object GenerateData(string system)
        {
            var dateTimeSecString = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_FFF");
            return string.Format("Mobile_id_{0}", dateTimeSecString);
        }
    }
}
