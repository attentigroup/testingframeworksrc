﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    [DataGenerator(EnumPropertyType.GUID)]
    public class GuidGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return Guid.NewGuid();
        }
    }
}
