﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Zone
{
    [DataGenerator(EnumPropertyType.BufferRadius)]
    public class BufferRadiusGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            return (int)intGenerator.GenerateData(10, 64999);
        }
    }
}
