﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Zone
{
    [DataGenerator(EnumPropertyType.ZoneName)]
    public class ZoneNameGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return "AutomationZone_" + DateTime.Now.Ticks.ToString();
        }
    }
}
