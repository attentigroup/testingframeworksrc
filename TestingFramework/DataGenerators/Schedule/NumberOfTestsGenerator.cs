﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Schedule
{
    [DataGenerator(EnumPropertyType.NumberOfTests)]
    public class NumberOfTestsGenerator : IDataGenerator
    {
        public static int GetNumberOfTestsByTimeRange(DateTime startTime, DateTime endTime, EnmTimeFrameType TimeFrameType, EnmVoiceTestCallType? VoiceTestCallType)
        {
            if (TimeFrameType == EnmTimeFrameType.VoiceTest && (VoiceTestCallType == EnmVoiceTestCallType.Incoming || VoiceTestCallType == EnmVoiceTestCallType.Incoming24))
                return 1;
            else
            {
                var timeRange = endTime - startTime;
                var maxNumberOfTests = timeRange.TotalHours / 0.5;
                return DataGeneratorFactory.Instance.RNG.Next(1, (int)maxNumberOfTests);
            }

        }

        public object GenerateData(string system)
        {
            throw new NotImplementedException();
        }
    }
}
