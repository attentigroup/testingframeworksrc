﻿using Common.CustomAttributes;
using Common.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Reflection;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;
using System.Linq;
#endregion

namespace DataGenerators.Configuration
{
    [DataGenerator(EnumPropertyType.GraceTime_1)]
    public class ConfigurationGPSRuleConfigurationGenerator : IDataGenerator
    {
        //public const int MIN_VALUE = 600;
        //public const int MAX_VALUE = 1200;

        //public int GenerateData(int minValue, int maxValue)
        //{

        //    var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
        //    var x =  60*intGenerator.GenerateData(1, 10);
        //    return x;
        //    //var index2Set = intGenerator.GenerateData(600, 1200);
        //    //StringLength = MAX_VALUE;
        //}

        public object GenerateData(string system)
        {
            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            return (int)60 * intGenerator.GenerateData(1, 1440);
        }


    }
}
