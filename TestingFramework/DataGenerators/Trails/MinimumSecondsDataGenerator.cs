﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Trails
{
    [DataGenerator(EnumPropertyType.MinimumSeconds)]
    class MinimumSecondsDataGenerator :IDataGenerator
    {
        public object GenerateData(string system)
        {
            int MinimumSeconds = (int)60 * DataGeneratorFactory.Instance.RNG.Next(1, 60);
            return MinimumSeconds;
        }
    }
}
