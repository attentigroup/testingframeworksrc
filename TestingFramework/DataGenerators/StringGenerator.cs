﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    /// <summary>
    /// abstract class for common functionality fro string generators
    /// </summary>
    public abstract class StringGeneratorBase
    {
        /// <summary>
        /// const array of chars that a string generator should use.
        /// </summary>
        public const string CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        /// <summary>
        /// the numbers of chars that the string will be generated
        /// </summary>
        /// <example>
        /// <code>
        /// public class StringGenerator : StringGeneratorBase, IDataGenerator
        /// {
        ///     public const int MIN_VALUE = 5;
        ///     public const int MAX_VALUE = 20;
        /// 
        ///     public StringGenerator()
        ///     {
        ///         <b>StringLength</b> = MAX_VALUE;
        ///     }
        /// }
        /// </code>
        /// </example>
        public int StringLength { get; set; }

        public object GenerateData(string system)
        {
            return GenerateRandomStringWithLength(StringLength);
        }
        public string GenerateRandomStringWithLength(int stringLength)
        {
            var builder = new StringBuilder();
            for (var i = 0; i < stringLength; i++)
            {
                var currentIndex = DataGeneratorFactory.Instance.RNG.Next(1, CHARS.Length);
                var ch = CHARS[currentIndex];
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }


    [DataGenerator(EnumPropertyType.String)]
    public class StringGenerator : StringGeneratorBase, IDataGenerator
    {
        public const int         MIN_VALUE = 5;
        public const int         MAX_VALUE = 20;
        
        public StringGenerator()
        {
            StringLength = MAX_VALUE;
        }
    }


 
}
