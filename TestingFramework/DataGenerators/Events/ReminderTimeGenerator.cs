﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.ReminderTime)]
    public class ReminderTimeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var reminderTimes = APIResourcesProxy.Instance.GetLookup(new TestingFramework.Proxies.EM.Interfaces.Resources12_0.EntMsgGetLookupRequest()
            {
                LookupName = "TIME_BLOCK"
            }).Lookup;
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, reminderTimes.Count - 1);
            var reminderTime = int.Parse(reminderTimes.ElementAt(currentIndex).Key);
            return reminderTime;
            //return DataGeneratorFactory.Instance.RNG.Next(1, 300);
        }
    }
}
