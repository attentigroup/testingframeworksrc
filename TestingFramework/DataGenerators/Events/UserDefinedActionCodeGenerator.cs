﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.UserDefinedActionCode)]
    public class UserDefinedActionCodeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var userDefinedActionCodes = APIResourcesProxy.Instance.GetLookup(new TestingFramework.Proxies.EM.Interfaces.Resources12_0.EntMsgGetLookupRequest()
            {
               LookupName = "USERACTN"
            });
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, userDefinedActionCodes.Lookup.Count - 1);
            return userDefinedActionCodes.Lookup.ElementAt(currentIndex).Key;
        }
    }
}
