﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.ThirdPartyActionCode)]
    public class ThirdPartyActionCodeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var thirdPartyActionCodes = APIResourcesProxy.Instance.GetLookup(new TestingFramework.Proxies.EM.Interfaces.Resources12_0.EntMsgGetLookupRequest()
            {
                LookupName = "ASSIGN3RD"
            });
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, thirdPartyActionCodes.Lookup.Count - 1);
            return thirdPartyActionCodes.Lookup.ElementAt(currentIndex).Key;
        }
    }
}
