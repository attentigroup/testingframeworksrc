﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    /// <summary>
    /// generate schedual time in the future (adding minutes - between 1 to 30)
    /// </summary>
    [DataGenerator(EnumPropertyType.ScheduleTaskTime)]
    public class ScheduleTaskTimeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(1, 30);
            return DateTime.Now.AddMinutes(currentIndex);
        }
    }
}
