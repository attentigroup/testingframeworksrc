﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.PagerType)]
    public class PagerTypeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var pagerProviderIDs = APIResourcesProxy.Instance.GetPagerProvidersList();
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, pagerProviderIDs.PagerProvidersList.Count - 1);
            var providerID = int.Parse(pagerProviderIDs.PagerProvidersList.ElementAt(currentIndex).Key);
            var pagerTypes = APIResourcesProxy.Instance.GetPagerDeviceTypeList(new TestingFramework.Proxies.EM.Interfaces.Resources12_0.EntMsgGetPagerDeviceTypesRequest()
            {
                ProviderID = providerID
            });
            var currentIndexPagerType = DataGeneratorFactory.Instance.RNG.Next(0, pagerTypes.PagerDeviceList.Count - 1);
            return int.Parse(pagerTypes.PagerDeviceList.ElementAt(currentIndexPagerType).Key);
        }

        public static int GetPagerTypeByProviderID(int providerID)
        {
            var pagerTypes = APIResourcesProxy.Instance.GetPagerDeviceTypeList(new TestingFramework.Proxies.EM.Interfaces.Resources12_0.EntMsgGetPagerDeviceTypesRequest()
            {
                ProviderID = providerID
            });
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, pagerTypes.PagerDeviceList.Count - 1);
            return int.Parse(pagerTypes.PagerDeviceList.ElementAt(currentIndex).Key);
        }
    }
}
