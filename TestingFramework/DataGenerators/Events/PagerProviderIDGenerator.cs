﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.PagerProviderID)]
    public class PagerProviderIDGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var pagerProviderIDs = APIResourcesProxy.Instance.GetPagerProvidersList();
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, pagerProviderIDs.PagerProvidersList.Count - 1);
            return int.Parse(pagerProviderIDs.PagerProvidersList.ElementAt(currentIndex).Key);
        }
    }
}
