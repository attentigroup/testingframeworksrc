﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.PagerCode)]
    public class PagerCodeGenerator : IDataGenerator
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }


        public PagerCodeGenerator()
        {
            MinValue = 100;
            MaxValue = 1000000;
        }
        public object GenerateData(string system)
        {
            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            return intGenerator.GenerateData(MinValue, MaxValue).ToString();
        }
    }
}
