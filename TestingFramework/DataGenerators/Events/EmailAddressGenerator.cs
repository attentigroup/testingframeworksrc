﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.EmailAddress)]
    public class EmailAddressGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            string CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var builder = new StringBuilder();
            for (var i = 0; i < 3; i++)
            {
                var currentIndex = DataGeneratorFactory.Instance.RNG.Next(1, CHARS.Length);
                var ch = CHARS[currentIndex];
                builder.Append(ch);
            }
            var firstNameString = "Auto_" + builder.ToString() + "@" + builder.ToString() + ".com";
            return firstNameString;
        }
    }
}
