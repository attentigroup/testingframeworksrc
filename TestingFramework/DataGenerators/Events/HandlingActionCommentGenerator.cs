﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.Comment)]
    public class HandlingActionCommentGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            return "Handled by automation" + DateTime.Now.ToString();
        }
    }
}
