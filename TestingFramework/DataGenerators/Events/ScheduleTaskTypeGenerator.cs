﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.Proxies.API.Resources;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators.Events
{
    [DataGenerator(EnumPropertyType.ScheduleTaskType)]
    public class ScheduleTaskTypeGenerator : IDataGenerator
    {
        public object GenerateData(string system)
        {
            var scheduledTaskTypes = APIResourcesProxy.Instance.GetScheduledTaskTypes().ScheduledTaskTypes;
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, scheduledTaskTypes.Count - 1);
            return scheduledTaskTypes.ElementAt(currentIndex).Key;
        }

        public static string GetScheduleTaskSubTypeByScheduleTaskType(string scheduleTaskType)
        {
            var scheduledTaskSubTypes = APIResourcesProxy.Instance.GetScheduledTaskSubTypes(new TestingFramework.Proxies.EM.Interfaces.Resources12_0.EntMsgGetScheduledTaskSubTypesRequest()
            {
                ScheduledTaskType = scheduleTaskType
            }).ScheduledTaskSubTypes;
            var currentIndex = DataGeneratorFactory.Instance.RNG.Next(0, scheduledTaskSubTypes.Count - 1);
            return scheduledTaskSubTypes.ElementAt(currentIndex).Key;
        }

        
    }
}
