﻿using Common.CustomAttributes;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;

namespace DataGenerators
{
    /// <summary>
    /// generate random buffer of bytes
    /// </summary>
    [DataGenerator(EnumPropertyType.Bytes)]
    public class BytesGenerator : IDataGenerator
    {
        /// <summary>
        /// default size of buffer
        /// </summary>
        public const int DEFAULT_BUFFER_SIZE = 5;
        /// <summary>
        /// interface method of IDataGenerator for bytes buffer generator.
        /// </summary>
        /// <param name="system"></param>
        /// <returns></returns>
        public object GenerateData(string system)
        {
            return GenerateData(DEFAULT_BUFFER_SIZE);
        }

        /// <summary>
        /// generate random buffer for specific size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>An array of bytes to contain random numbers.</returns>
        public byte[] GenerateData(int size)
        {
            var buff = new byte[size];
            GenerateData(buff);
            return buff;
        }

        /// <summary>
        /// generate random buffer for specific buffer
        /// </summary>
        /// <param name="buffer"></param>
        public void GenerateData(byte[] buffer)
        {
            DataGeneratorFactory.Instance.RNG.NextBytes(buffer);
        }
    }
}
