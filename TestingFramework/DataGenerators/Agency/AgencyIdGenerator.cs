﻿using Common.CustomAttributes;
using Common.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Exceptions;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.Proxies.API.Events;
using log4net;
using System.Reflection;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events_0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Offenders_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

using Users = TestingFramework.Proxies.EM.Interfaces.Users;

using TestingFramework.Proxies.API.Users;

namespace DataGenerators.Agency
{
    [DataGenerator(EnumPropertyType.AgencyId)]
    public class AgencyIdGenerator : IDataGenerator
    {
        private static List<int> _agencyIdList = null;
        private static readonly object Lockobject = new object();

        private static string AgencyIdGenerator_GetMethodToken = "AgencyIdGenerator_GetMethod";

        private static int GetOffendersRequestLimit = 1000;
        private static string GetOffendersRequestLimitToken = "AgencyIdGenerator_GetOffendersRequestLimit";

        private static int GetEventsRequestLimit = 1000;
        private static string GetEventsRequestLimitToken = "AgencyIdGenerator_GetEventsRequestLimit";

        
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static List<int> AgencyIdList
        {
            get
            {
                if (_agencyIdList == null)
                {
                    lock (Lockobject)
                    {
                        if (_agencyIdList == null)
                        {
                            //read configuration
                            var value = ConfigurationManager.AppSettings.Get(GetOffendersRequestLimitToken);
                            if( value != null )
                            {
                                int.TryParse(value, out GetOffendersRequestLimit);
                            }

                            var GetMethod = ConfigurationManager.AppSettings.Get(AgencyIdGenerator_GetMethodToken);

                            switch(GetMethod)
                            {
                                case "Events":
                                    _agencyIdList = GetAgenciesByEvents();
                                    break;
                                case "Offenders":
                                    _agencyIdList = GetAgenciesFromOffendersList();
                                    break;
                                case "Officers":
                                    _agencyIdList = GetAgenciesFromOfficersList();
                                    break;
                                default:
                                    Log.DebugFormat("GetMethod config {0} and it it unsuported method. using events.", GetMethod);
                                    _agencyIdList = GetAgenciesByEvents();
                                    break;
                            }
                        }
                    }
                }
                return _agencyIdList;
            }
        }

        public static Dictionary<int, List<int>> _dicAgencies2Officers = new Dictionary<int, List<int>>();

        private static List<int> GetAgenciesFromOffendersList()
        {
            var entMsgGetOffendersRequest = new Offenders0.EntMsgGetOffendersListRequest();
            entMsgGetOffendersRequest.Limit = GetOffendersRequestLimit;
            var getOffendersResponse = OffendersProxy.Instance.GetOffenders(entMsgGetOffendersRequest);

            List<int> agencies = getOffendersResponse.OffendersList.Select(o => o.AgencyID).Distinct().ToList();
            foreach (var agencyId in agencies)
            {
                _dicAgencies2Officers.Add(agencyId, new List<int>());
            }

            foreach (var offender in getOffendersResponse.OffendersList)
            {
                var entMsgGetOfficerRequest = new Users.EntMsgGetOfficerRequest() { OfficerID = offender.OfficerID };
                try
                {
                    var getOfficersResponse = UsersProxy.Instance.GetOfficer(entMsgGetOfficerRequest);
                    if(getOfficersResponse.Officer.UserStatus == Users.EnmUserStatus.Active )
                    {
                        _dicAgencies2Officers[offender.AgencyID].Add(offender.OfficerID);
                    }
                    else
                    {
                        Log.InfoFormat("officer id {0} with status {1} and can't be used", offender.OfficerID, getOfficersResponse.Officer.UserStatus);
                    }

                    }
                catch (Exception ex)
                {
                    Log.ErrorFormat("during get officer id {0} error happend:{1}", offender.OfficerID, ex.Message);
                    throw;
                }
                
            }

            return agencies;
        }

        private static List<int> GetAgenciesFromOfficersList()
        {
            List<int> agencies = new List<int>();
            var entMsgGetOfficersRequest = new Users.EntMsgGetOfficersListRequest();
            var getOfficersResponse = UsersProxy.Instance.GetOfficers(entMsgGetOfficersRequest);

            Users.EntOfficer[] officers = getOfficersResponse.OfficersList;

            foreach (var officer in officers)
            {
                if (officer.UserStatus == Users.EnmUserStatus.Active)
                {
                    if (!(_dicAgencies2Officers.ContainsKey(officer.AgencyID.Value)))
                    {
                        agencies.Add(officer.AgencyID.Value);
                        _dicAgencies2Officers.Add(officer.AgencyID.Value, new List<int>());
                        _dicAgencies2Officers[officer.AgencyID.Value].Add(officer.ID);
                    }
                    else
                        _dicAgencies2Officers[officer.AgencyID.Value].Add(officer.ID);
                }
            }

            return agencies;
        }

        public static int GetOfficerByAgencyId(int agencyId)
        {
            int agenciesCount = AgencyIdList.Count;
            Log.DebugFormat("Going to get officer from agency {0} out of {1} agencies.", agencyId, agenciesCount);
            int officerId = -1;
            if( _dicAgencies2Officers.ContainsKey(agencyId))
            {
                var officers = _dicAgencies2Officers[agencyId];
                int officerRandIndex = DataGeneratorFactory.Instance.RNG.Next(officers.Count - 1);
                officerId = officers[officerRandIndex];
            }
            else
            {
                throw new DataGeneratorException(new AgencyIdGenerator(), 
                    string.Format($"Unable to get officer for agency {agencyId}"));
            }
            return officerId;
        }

        public static List<int> GetAgenciesByEvents()
        {
            //read configuration
            var value = ConfigurationManager.AppSettings.Get(GetEventsRequestLimitToken);
            if (value != null)
            {
                int.TryParse(value, out GetEventsRequestLimit);
            }
                       
            value = ConfigurationManager.AppSettings.Get(GetOffendersRequestLimitToken);
            if (value != null)
            {
                int.TryParse(value, out GetOffendersRequestLimit);
            }

            var getEventsRequest = new Events_0.EntMsgGetEventsRequest();
            getEventsRequest.IncludeHistory = false;
            getEventsRequest.SortField = Events_0.EnmEventsSortOptions.EventTime;
            getEventsRequest.SortDirection = System.ComponentModel.ListSortDirection.Descending;

            int totalEventRetrived = 0;
            var dicOffenders = new Dictionary<int, int>();
            var dicOfficers = new Dictionary<int, int>();
            var retrivaleEndTime = DateTime.Now.AddDays(-7);

            //set limit
            getEventsRequest.Limit = GetEventsRequestLimit;
            //set initialize time
            getEventsRequest.EndTime = DateTime.Now;

            while (dicOffenders.Count < GetOffendersRequestLimit && getEventsRequest.EndTime > retrivaleEndTime)
            {
                //set time range
                getEventsRequest.StartTime = getEventsRequest.EndTime.Value.AddHours(-1);
                Events_0.EntMsgGetEventsResponse response = EventsProxy.Instance.GetEvents(getEventsRequest);
                Log.DebugFormat("request events from {0} to {1} return {2} events.",
                    getEventsRequest.StartTime.Value, getEventsRequest.EndTime.Value, response.EventsList.Length);
                totalEventRetrived += response.EventsList.Length;

                foreach (var eventItem in response.EventsList)
                {
                    if (eventItem.OffenderID.HasValue)
                    {
                        int offenderId = eventItem.OffenderID.Value;
                        if (offenderId <= 0)
                            continue;//invalid offender id
                        
                        if (dicOffenders.ContainsKey(offenderId) == false)
                        {//new offender - add it to the dictionary
                            dicOffenders.Add(offenderId, offenderId);
                            if (dicOffenders.Count >= GetOffendersRequestLimit)
                                break;//no need more offenders
                        }
                        else
                        {
                            Log.DebugFormat("Event id {0} arrived with offender id {1} that allready added to the dicionary - skipping it", eventItem.ID, offenderId);
                        }
                    }
                    else
                    {
                        Log.DebugFormat("Event id {0} arrived without offender id - skipping it", eventItem.ID);
                    }
                }
                getEventsRequest.EndTime = getEventsRequest.StartTime;
            }

            Log.DebugFormat("After retrieve {0} events arrived to {1} offenders.", totalEventRetrived, dicOffenders.Count);
            var OffendersIdsList = dicOffenders.Keys.ToList();
            //log offendersref id
            Offenders_0.EntMsgGetOffendersListRequest entMsgGetOffendersRequest = new Offenders_0.EntMsgGetOffendersListRequest();
            foreach (var offenderId in OffendersIdsList)
            {
                entMsgGetOffendersRequest.OffenderID = new Offenders_0.EntNumericParameter() { Value = offenderId, Operator = Offenders_0.EnmNumericOperator.Equal };

                var EntMsgGetOffendersResponse = OffendersProxy.Instance.GetOffenders(entMsgGetOffendersRequest);
                var offender = EntMsgGetOffendersResponse.OffendersList[0];
                Log.InfoFormat("offender id = {0} ref id = {1}", offenderId, offender.RefID);
                
                //check the officer good to use it AKA not deleted
                try
                {
                    TestingFramework.Proxies.DataObjects.Officer officer = UsersProxy.Instance.GetOfficerById(offender.OfficerID);
                    if(officer.UserStatus == TestingFramework.Proxies.DataObjects.UserStatus.Active)
                    {
                        //add agency only when we have officer on that agency.
                        if (_dicAgencies2Officers.ContainsKey(offender.AgencyID) == false)
                        {
                            _dicAgencies2Officers.Add(offender.AgencyID, new List<int>());
                        }
                        _dicAgencies2Officers[offender.AgencyID].Add(offender.OfficerID);
                    }
                    else
                    {
                        Log.InfoFormat("officer id {0} with status {1} and can't be used", offender.OfficerID, officer.UserStatus);
                    }                    
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("during get officer id {0} error happend:{1}", offender.OfficerID, ex.Message);
                    throw;
                }
                
            }
            var agenciesIdList = _dicAgencies2Officers.Keys.ToList();
            Log.DebugFormat("After retrieve {0} offenders we have {1} agencies.", OffendersIdsList.Count, agenciesIdList.Count);
            return agenciesIdList;
        }

        public object GenerateData(string system)
        {
            int agenciesCount = AgencyIdList.Count;
            int randIndex = DataGeneratorFactory.Instance.RNG.Next(agenciesCount-1);
            int agencyId = AgencyIdList[randIndex];
            return agencyId;
        }

        public static int GenerateDiffenetAgencyId(int oldAgencyId)
        {
            int agenciesCount = AgencyIdList.Count;
            int agencyId = -1;
            int retry = 5;
            do
            {
                int randIndex = DataGeneratorFactory.Instance.RNG.Next(agenciesCount - 1);
                agencyId = AgencyIdList[randIndex];
                retry--;
            }
            while (oldAgencyId == agencyId & retry > 0);
       
            
            return agencyId;
        }
    }
}
