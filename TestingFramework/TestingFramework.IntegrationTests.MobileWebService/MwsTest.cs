﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.Mws;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using Common.Categories;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using LogicBlocksLib.MwsBlocks;
using TestBlocksLib.MwsBlocks;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;


namespace TestingFramework.IntegrationTests.MobileWebService
{
    [Ignore]//TODO:Omer set it to ignore.Remove this when mobile environment will be stable
    [TestClass]
    public class MwsTest : BaseUnitTest
    {

        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetMobileStatus_NotRegistered_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetMobileStatusRequestBlock>().
                    SetDescription("Create Get Translations Request").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.MobileID).WithValue("11111").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.6").

                AddBlock<GetMobileStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Translations Request").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetMobileStatus_PendingForActive_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.6").
                    SetProperty(x => x.CellularNumber).WithValue("0544111111").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgGetMobileStatusRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Translations Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<GetMobileStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Translations Request").

                AddBlock<CreateEntMsgDeleteAuthorizedMobileRequestBlock>().
                    SetDescription("Create delete authorized mobile request").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).

                AddBlock<DeleteAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete authorized mobile").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetMobileStatus_Active_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("14.1.0.9").
                    SetProperty(x => x.CellularNumber).WithValue("0544111112").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.Comment).WithValue("Yoooo!!").

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<CreateEntMsgGetMobileStatusRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Translations Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<GetMobileStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Translations Request").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetMobileStatus_OptionalUpgrade_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.0").
                    SetProperty(x => x.CellularNumber).WithValue("0544111113").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.Comment).WithValue("Yoooo!!").

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<CreateEntMsgGetMobileStatusRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Translations Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<GetMobileStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Translations Request").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void FTR_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.7").
                    SetProperty(x => x.CellularNumber).WithValue("0544111114").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("yuvaloflogin").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.Comment).WithValue("Yoooo!!").

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<CreateEntMsgValidateLoginRequestBlock>().
                    SetDescription("Create Validate Login mobile Request").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.Username).WithValue("yuvaloflogin").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<ValidateLoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Validate Login mobile Request").

                AddBlock<CreateEntMsgLogoutRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Logout mobile Request").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<LogoutTestBlock>().UsePreviousBlockData().
                    SetDescription("Logout mobile Request").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetTranslations_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.6").
                    SetProperty(x => x.CellularNumber).WithValue("0544111115").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.Comment).WithValue("Yoooo!!").

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<CreateEntMsgValidateLoginRequestBlock>().
                    SetDescription("Create Validate Login mobile Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<ValidateLoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Validate Login mobile Request").

                AddBlock<CreateEntMsgGetTranslationsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Translations Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<GetTranslationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Translations Request").

                AddBlock<CreateEntMsgLogoutRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Logout mobile Request").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<LogoutTestBlock>().UsePreviousBlockData().
                    SetDescription("Logout mobile Request").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetMetaData_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.6").
                    SetProperty(x => x.CellularNumber).WithValue("0544111116").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.Comment).WithValue("Yoooo!!").

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<CreateEntMsgValidateLoginRequestBlock>().
                    SetDescription("Create Validate Login mobile Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<ValidateLoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Validate Login mobile Request").

                AddBlock<CreateEntMsgGetMetaDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Meta Data Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<GetMetaDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get MetaData Request").

                AddBlock<CreateEntMsgLogoutRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Logout mobile Request").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<LogoutTestBlock>().UsePreviousBlockData().
                    SetDescription("Logout mobile Request").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MobileWebService)]
        [TestMethod]
        public void GetOffendersList_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgRegisterRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Register mobile").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.AppVersion).WithValue("12.1.0.6").
                    SetProperty(x => x.CellularNumber).WithValue("0544111117").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<RegisterTestBlock>().UsePreviousBlockData().
                    SetDescription("Register Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.Comment).WithValue("Yoooo!!").

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<CreateEntMsgValidateLoginRequestBlock>().
                    SetDescription("Create Validate Login mobile Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.Username).WithValue("eladof").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<ValidateLoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Validate Login mobile Request").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders List Request").
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.ApplicationID).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).
                    SetProperty(x => x.isOnlyActive).WithValue(false).
                    SetProperty(x => x.isOnlyViolation).WithValue(false).

                AddBlock<GetOffendersListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders List Request").

                AddBlock<CreateEntMsgLogoutRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Logout mobile Request").
                    SetProperty(x => x.ApplicationID).WithValue("3").
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.SessionToken).
                        WithValueFromBlockIndex("ValidateLoginTestBlock").
                            FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.SessionToken).
                    //SetProperty(x => x.STT).
                    //    WithValueFromBlockIndex("ValidateLoginTestBlock").
                    //        FromDeepProperty<ValidateLoginTestBlock>(x => x.ValidateLoginResponse.mwsSecurityData.STT).
                    SetProperty(x => x.AppVersion).
                        WithValueFromBlockIndex("CreateEntMsgRegisterRequestBlock").
                            FromPropertyName(EnumPropertyName.AppVersion).

                AddBlock<LogoutTestBlock>().UsePreviousBlockData().
                    SetDescription("Logout mobile Request").

            ExecuteFlow();
        }
    }
}
