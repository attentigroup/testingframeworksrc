using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("TestingFramework.IntegrationTests.MobileWebService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("3M")]
[assembly: AssemblyProduct("TestingFramework.IntegrationTests.MobileWebService")]
[assembly: AssemblyCopyright("Copyright © 3M 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("acb7f06c-e370-4e1e-aa95-68ff29f93726")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
