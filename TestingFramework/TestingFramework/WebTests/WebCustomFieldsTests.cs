﻿using AssertBlocksLib.SystemTablesAsserts;
using Common.Categories;
using LogicBlocksLib.Information;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.Resources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.InformationBlock;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.ResourcesBlock;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.Information;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.WebTestBlocks.CustomFields;
using TestBlocksLib.APIExtensions;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    public class WebCustomFieldsTests : WebBaseUnitTest
    {
        #region API tests
        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void CustomFieldsSvc_ChangeCustomFieldsVisibility()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                //Assert

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void CustomFieldsSvc_UpdateCustomFieldsNames()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars }).

                AddBlock<UpdateCustomFieldsNanesTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                //Assert

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void CustomFieldsListValueSvc_AddCustomFieldsListValues()
        {
            TestingFlow.
                
                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>().UsePreviousBlockData().

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<CreateUpdateEmsSystableLogicBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.Action).WithValue(EnmDBAction.I).
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).
                    SetProperty(x => x.Language).WithValue("ENG").

                AddBlock<UpdateEmsSystableTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.CustomFieldsListValue).
                WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.InputCustomFieldsDictionary).
                SetProperty(x => x.CleanUp_CustomFieldDictionary).
                WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                FromDeepProperty< CreateUpdateEmsSystableLogicBlock>(x => x.CleanUp_CustomFieldDictionary).

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<ValidateCustomFieldListIsExistAssertBlock>().
                    SetProperty(x=> x.ActionType).WithValue(EnmDBAction.I).
                    SetProperty(x => x.CustomFieldsListValue).
                WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.InputCustomFieldsDictionary).

            ExecuteFlow();
        }
        #endregion

        #region Web tests
        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_ChangeCustomFieldsVisibility_ShowInOffenderDetails()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().

                 AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ChangeCustomFieldsVisibilityTestBlock>().
                    SetProperty(x => x.CustomFieldsByTypeLst).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty< GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().
                    SetName("GetCustomFieldsSystemDefinitionsTestBlock_After").

                AddBlock<Web_ChangeVisibilityAssertBlock>().
                    SetProperty(x => x.CustomFieldsSystemDefinitions).
                        WithValueFromBlockIndex("GetCustomFieldsSystemDefinitionsTestBlock_After").
                            FromDeepProperty<GetCustomFieldsSystemDefinitionsTestBlock>(x => x.CustomFieldsSystemDefinitions).
                    SetProperty(x => x.ShowInOffenderDetails).
                        WithValueFromBlockIndex("Web_ChangeCustomFieldsVisibilityTestBlock").
                            FromDeepProperty<Web_ChangeCustomFieldsVisibilityTestBlock>(x => x.ShowInOffenderDetails).
                    SetProperty(x => x.ShowInOffendersList).
                        WithValueFromBlockIndex("Web_ChangeCustomFieldsVisibilityTestBlock").
                            FromDeepProperty<Web_ChangeCustomFieldsVisibilityTestBlock>(x => x.ShowInOffendersList).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_ChangeCustomFieldsVisibility_ShowInOffendersListAndOffenderDetails()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().

                 AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ChangeCustomFieldsVisibilityTestBlock>().
                    SetProperty(x => x.CustomFieldsByTypeLst).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().
                SetName("GetCustomFieldsSystemDefinitionsTestBlock_After").

                AddBlock<Web_ChangeVisibilityAssertBlock>().
                SetProperty(x => x.CustomFieldsSystemDefinitions).
                WithValueFromBlockIndex("GetCustomFieldsSystemDefinitionsTestBlock_After").
                FromDeepProperty<GetCustomFieldsSystemDefinitionsTestBlock>(x => x.CustomFieldsSystemDefinitions).
                SetProperty(x => x.ShowInOffenderDetails).
                        WithValueFromBlockIndex("Web_ChangeCustomFieldsVisibilityTestBlock").
                            FromDeepProperty<Web_ChangeCustomFieldsVisibilityTestBlock>(x => x.ShowInOffenderDetails).
                    SetProperty(x => x.ShowInOffendersList).
                        WithValueFromBlockIndex("Web_ChangeCustomFieldsVisibilityTestBlock").
                            FromDeepProperty<Web_ChangeCustomFieldsVisibilityTestBlock>(x => x.ShowInOffendersList).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_UpdateCustomFieldsName()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().

                 AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_UpdateCustomFieldsNamesTestBlock>().
                    SetProperty(x => x.CustomFieldsByTypeLst).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().
                SetName("GetCustomFieldsSystemDefinitionsTestBlock_After").

                AddBlock<Web_UpdateCustomFieldsNamesAssertBlock>().
                    SetProperty(x => x.CustomFieldsSystemDefinitions).
                    WithValueFromBlockIndex("GetCustomFieldsSystemDefinitionsTestBlock_After").
                    FromDeepProperty<GetCustomFieldsSystemDefinitionsTestBlock>(x => x.CustomFieldsSystemDefinitions).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_VerifyCustomFieldsDisplayedInOffenderDetails_Visible()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndGetCustomFieldsVisibilityFromOffenderDetailsTestBlock>().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).

                AddBlock<Web_AreCustomFieldsVisibleAssertBlock>().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    SetProperty(x => x.ExpectedVisibilityStatus).WithValue(true).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_VerifyCustomFieldsDisplayedInOffenderDetails_Invisible()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndGetCustomFieldsVisibilityFromOffenderDetailsTestBlock>().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).

                AddBlock<Web_AreCustomFieldsVisibleAssertBlock>().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    SetProperty(x => x.ExpectedVisibilityStatus).WithValue(false).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_VerifyCustomFieldsDisplayedInOffendersList_Visible()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndGetCustomFieldsVisibilityFromOffendersListTestBlock>().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).

                AddBlock<Web_AreCustomFieldsVisibleAssertBlock>().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    SetProperty(x => x.ExpectedVisibilityStatus).WithValue(true).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_VerifyCustomFieldsDisplayedInOffendersList_Invisible()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, /*EnmWebFieldType.DropDownList,*/ EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<GetAllResourcesDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<Web_SelectOffenderAndGetCustomFieldsVisibilityFromOffendersListTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).

                AddBlock<Web_AreCustomFieldsVisibleAssertBlock>().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    SetProperty(x => x.ExpectedVisibilityStatus).WithValue(false).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_InsertDataToAllFieldTypesInOffenderDetails()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndInsertDataToAllCustomFieldsTypeTestBlock>().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_WithCustomField").
                    SetProperty(x => x.OffenderRefIDString).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_CustomField").
                SetProperty(x => x.GetOffendersListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_WithCustomField").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

            AddBlock<Web_InsertDataToCustomFieldsAssertBlock>().
                SetProperty(x => x.CustomFields).
                WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                SetProperty(x => x.GetOffendersResponse).
                WithValueFromBlockIndex("GetOffendersTestBlock_CustomField").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFieldsListValue_AddCustomFieldsListValue()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddCustomFieldsListValuesTestBlock>().
                    SetProperty(x => x.TableIdName).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].Name).
                    SetProperty(x => x.Length).WithValue(3).

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<Web_AddCustomFieldsListValuesAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFieldsListValue_UpdateCustomFieldsListValue()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>().UsePreviousBlockData().

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<CreateUpdateEmsSystableLogicBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.Action).WithValue(EnmDBAction.I).
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).
                    SetProperty(x => x.Language).WithValue("ENG").

                AddBlock<UpdateEmsSystableTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFieldsListValue).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.InputCustomFieldsDictionary).
                    SetProperty(x => x.CleanUp_CustomFieldDictionary).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.CleanUp_CustomFieldDictionary).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_UpdateCustomFieldListValueTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.TableIdName).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].Name).
                    SetProperty(x => x.ValueDescription).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.Description).

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<Web_UpdateCustomFieldsListValueAssertBlock>().
                    SetProperty(x => x.ValueDescription).
                    WithValueFromBlockIndex("Web_UpdateCustomFieldListValueTestBlock").
                    FromDeepProperty<Web_UpdateCustomFieldListValueTestBlock>(x => x.UpdatedValueDescription).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFieldsListValue_DeleteCustomFieldsListValue()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>().UsePreviousBlockData().

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<CreateUpdateEmsSystableLogicBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.Action).WithValue(EnmDBAction.I).
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).
                    SetProperty(x => x.Language).WithValue("ENG").

                AddBlock<UpdateEmsSystableTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.CustomFieldsListValue).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.InputCustomFieldsDictionary).
                    SetProperty(x => x.CleanUp_CustomFieldDictionary).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.CleanUp_CustomFieldDictionary).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_DeleteCustomFieldListValueTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.TableIdName).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].Name).
                    SetProperty(x => x.ValueDescription).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.Description).

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<Web_DeleteCustomFieldListValueAssertBlock>().
                    SetProperty(x => x.ValueDescription).
                    WithValueFromBlockIndex("Web_DeleteCustomFieldListValueTestBlock").
                    FromDeepProperty<Web_DeleteCustomFieldListValueTestBlock>(x => x.ValueDescription).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_VerifyCustomFieldsListItemAndItsValuesDisplayedInOffenderDetails()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>().UsePreviousBlockData().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldByTableIdTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetName("CreateChangeVisibilityForCustomFieldsRequestBlock_Visible").
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldByTableIdTestBlock").
                    FromDeepProperty<GetCustomFieldByTableIdTestBlock>(x => x.CustomFieldsByTableID).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("CreateChangeVisibilityForCustomFieldsRequestBlock_Visible").
                    FromDeepProperty<CreateChangeVisibilityForCustomFieldsRequestBlock>(x => x.CustomFields).

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddCustomFieldsListValuesTestBlock>().
                    SetProperty(x => x.TableIdName).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].Name).
                    SetProperty(x => x.Length).WithValue(3).

                AddBlock<Web_SelectOffenderAndGetCustomFieldsListValuesVisibilityTestBlock>().UsePreviousBlockData().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    //SetProperty(x => x.InputCustomFieldsDictionary).
                    //WithValueFromBlockIndex("UpdateEmsSystableTestBlock").
                    //FromDeepProperty<UpdateEmsSystableTestBlock>(x => x.CustomFieldsListValue).

               AddBlock<Web_VerifyCustomFieldListValuesAreDisplayedInOffenderDetailsAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFieldsListValue_VerifyItIsImpossibleToDeleteDropDownListValueOnceItIsUsedInOffenderDetails()
        {
            TestingFlow.

                 AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>().UsePreviousBlockData().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldByTableIdTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                AddBlock<CreateUpdateEmsSystableLogicBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.Action).WithValue(EnmDBAction.I).
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).
                    SetProperty(x => x.Language).WithValue("ENG").

                AddBlock<UpdateEmsSystableTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.CustomFieldsListValue).
                WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.InputCustomFieldsDictionary).
                SetProperty(x => x.CleanUp_CustomFieldDictionary).
                WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.CleanUp_CustomFieldDictionary).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetName("CreateChangeVisibilityForCustomFieldsRequestBlock_Visible").
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldByTableIdTestBlock").
                    FromDeepProperty<GetCustomFieldByTableIdTestBlock>(x => x.CustomFieldsByTableID).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("CreateChangeVisibilityForCustomFieldsRequestBlock_Visible").
                    FromDeepProperty<CreateChangeVisibilityForCustomFieldsRequestBlock>(x => x.CustomFields).

                AddBlock<GetPredefinedListsFromSysTableTestBlock>().
                    SetProperty(x => x.TableID).
                    WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                    FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].TableID).

                 AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("AddOffenderTestBlock").
                     FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                 AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                 AddBlock<GetCustomFieldsTestBlock>().

                AddBlock<CreateEntMsgUpdateOffenderWithSpecificCustomFieldValueRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.OfficerID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].AgencyID).
                    SetProperty(x => x.Timestamp).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                            SetProperty(x => x.CityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].CityID).
                            SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].FirstName).
                            SetProperty(x => x.Language).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Language).
                            SetProperty(x => x.LastName).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].LastName).
                            SetProperty(x => x.ProgramStart).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramStart).
                            SetProperty(x => x.ProgramEnd).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramEnd).
                            SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                            SetProperty(x => x.StateID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].StateID).
                            SetProperty(x => x.CustomFieldID).
                        WithValueFromBlockIndex("GetPredefineCustomFieldsSystemDefinitionsRequestBlock").
                        FromDeepProperty<GetPredefineCustomFieldsSystemDefinitionsRequestBlock>(x => x.PredefinedCustomFields[0].ID).
                            SetProperty(x => x.CustomFieldValueCode).
                        WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                            FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.ValueCode).

               AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_CheckCustomFieldListValueIsNotDeletableTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.ValueDescription).
                    WithValueFromBlockIndex("CreateUpdateEmsSystableLogicBlock").
                    FromDeepProperty<CreateUpdateEmsSystableLogicBlock>(x => x.Description).

                AddBlock<Web_VerifyCustomFieldIsNotDeletableAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_VerifyCustomFieldDataDisplayedInOffendersList()
        {
            TestingFlow.

                 AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ShowInOffenderDetails).WithValue(false).
                    SetProperty(x => x.ShowInOffendersList).WithValue(false).

                 AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                 SetName("ChangeVisibilityForCustomFieldsTestBlock_Invisible").

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars/*, EnmWebFieldType.HyperLink*/}).

                AddBlock<CreateChangeVisibilityForCustomFieldsRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).
                    SetProperty(x => x.ShowInOffenderDetails).WithValue(true).
                    SetProperty(x => x.ShowInOffendersList).WithValue(true).

                AddBlock<ChangeVisibilityForCustomFieldsTestBlock>().UsePreviousBlockData().
                SetName("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                 SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                 AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("AddOffenderTestBlock").
                     FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                 AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                 AddBlock<GetCustomFieldsTestBlock>().

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.OfficerID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].AgencyID).
                    SetProperty(x => x.Timestamp).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                            SetProperty(x => x.CityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].CityID).
                            SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].FirstName).
                            SetProperty(x => x.Language).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Language).
                            SetProperty(x => x.LastName).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].LastName).
                            SetProperty(x => x.ProgramStart).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramStart).
                            SetProperty(x => x.ProgramEnd).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramEnd).
                            SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                            SetProperty(x => x.StateID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].StateID).

               AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<GetAllResourcesDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<Web_SelectOffenderAndVerifyCustomFieldDataDisplayedInOffendersListTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                 AddBlock<Web_AreCustomFieldsVisibleAssertBlock>().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("ChangeVisibilityForCustomFieldsTestBlock_Visible").
                    FromDeepProperty<ChangeVisibilityForCustomFieldsTestBlock>(x => x.CustomFields).
                    SetProperty(x => x.ExpectedVisibilityStatus).WithValue(true).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_CustomFields_Sanity)]
        [TestMethod]
        public void Web_CustomFields_ChangeCustomFieldsNamesAndVerifyItsDisplayed()
        {
            TestingFlow.

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<GetCustomFieldsByFieldTypeTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FieldTypes).WithValue(new EnmWebFieldType[] { EnmWebFieldType.Integer, EnmWebFieldType.DateTime, EnmWebFieldType.String25Chars,
                    EnmWebFieldType.String12Chars, EnmWebFieldType.DropDownList, EnmWebFieldType.String100Chars, EnmWebFieldType.HyperLink}).

                AddBlock<UpdateCustomFieldsNanesTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.CustomFields).
                    WithValueFromBlockIndex("GetCustomFieldsByFieldTypeTestBlock").
                    FromDeepProperty<GetCustomFieldsByFieldTypeTestBlock>(x => x.CustomFieldsByTypeLst).

                AddBlock<GetCustomFieldsSystemDefinitionsTestBlock>().

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_GetUpdatedCustomFieldsNamesTestBlock>().
                    SetProperty(x => x.CustomFieldsByTypeLst).
                    WithValueFromBlockIndex("UpdateCustomFieldsNanesTestBlock").
                    FromDeepProperty<UpdateCustomFieldsNanesTestBlock>(x => x.CustomFields).

                AddBlock<Web_VerifyCustomFieldUpdatedNameIsDisplayedAssertBlock>().

            ExecuteFlow();
        }

        #endregion

    }
}
