﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.APIExtensions;
using LogicBlocksLib.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.EventsBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.API.APIExtensions.Security;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Pages;
using WebTests.InfraStructure.Pages.OffenderPages;
using WebTests.InfraStructure.TestLogicLayer;
using static QueueAndLogSecorityConfigData;
using static WebTests.InfraStructure.Pages.OffenderGroupsListPage;
using static WebTests.InfraStructure.Pages.OffenderPages.AddNewOffenderPage;

#region API refs

using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;

#endregion


using static WebTests.InfraStructure.TestLogicLayer.SecurityConfigLogic_Test;

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    public class WebSecurityConfigTest : BaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTestPOC()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexSE").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGUISecurityDataLogicBlock>().
                    SetDescription("Set GUI security logic block").
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ResourceID).WithValue(new string[] { QueueAndLogAPIElementName.Queue_ListSchedule_GridHeader_Status.ToString(), QueueAndLogAPIElementName.Log_ListSchedule_GridHeader_Status.ToString() }).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_SecurityConfigAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigQueueGeneral()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexMC").
                    SetProperty(x => x.Password).WithValue("W2e3r4t5").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Queue_").
                  SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Queue_List_Section_Filter", "Queue_ListAsyc_GridHeader_ActionType" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_OffendersList_Visible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexSE").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("OffendersList_").
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_OffendersList_Invisible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexSE").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("OffendersList_").
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                //Test the excluded filter section for invisibility

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("OffendersList_List_Section_Filter").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_Equipment_Visible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexSE").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Equipment_").
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_LastKnownLocation()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("newSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("LastKnownLocation_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                 AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                 AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("LastKnownLocation_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTest_LogScreen()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("VladimirSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Log_").
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Log_List_Section_Filter", "ListAsyc" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }



        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_GroupsList()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("YairSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).


                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("GroupsList_").
                    //SetProperty(x => x.ExcludeItems).WithValue(new string[] { "MemebersList" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "GroupsList_MemebersList_GridHeader_RefID", "GroupsList_MemebersList_GridHeader_LastName", "GroupsList_MemebersList_GridHeader_FirstName", "GroupsList_MemebersList_GridHeader_MiddleName", "GroupsList_MemebersList_GridHeader_OfficerName", "GroupsList_MemebersList_GridHeader_AgencyName", "GroupsList_MemebersList_GridHeader_StatusIcon", "GroupsList_MemebersList_GridHeader_Index" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_GroupsList_MemebersList()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("YairSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).


                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("GroupsList_").
                   //SetProperty(x => x.ExcludeItems).WithValue(new string[] { "_List_GridHeader_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "GroupsList_List_GridHeader_GroupName", "GroupsList_List_GridHeader_ProgramType", "GroupsList_List_GridHeader_Description", "GroupsList_List_GridHeader_GroupIcon" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTest_TrackerRules()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("VladimirSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("TrackerRules").
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Label" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigMonitorFilter()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexMC").
                    SetProperty(x => x.Password).WithValue("W2e3r4t5").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Monitor_EventsGrid_Section_Filter").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigMonitorCustomFilterByTime()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexMC").
                    SetProperty(x => x.Password).WithValue("W2e3r4t5").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Monitor_EventGrid_Button_CustomFilterByTime").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_SecurityConfigAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTestPOC2()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexMC").
                    SetProperty(x => x.Password).WithValue("W2e3r4t5").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Queue_").
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Queue_List_Section_Filter" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                //AddBlock<Web_SecurityConfigAssertBlock>().

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTest_Group()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("newSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Group_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("Group_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTest_UserGrid()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("newSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("UserGrid_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("UserGrid_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).



                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTest_UserOffenderGrid()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("newSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("UserOffendersGrid_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("UserOffendersGrid_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).



                ExecuteFlow();
        }
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigTest_UserAgenciesGrid()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("newSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("UserAgenciesGrid_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue("UserAgenciesGrid_").
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfigEventGridAgencyNameVisible()
        {
            TestingFlow.
                
                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("YairSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue("EventGrid_List_Button_QuickHandle").
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_MoveCursorToQuickHandleTestBlock>().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }
    }
}
