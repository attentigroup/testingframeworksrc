﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_GroupDetailsTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_GroupDetails_GridHeader_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_List_GridHeader_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_List_GridHeader_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_GroupDetails_GridHeader_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_List_GridHeader_" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_GroupDetails_HeadersAndLabels_Enabled_ReadOnly()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_GridDetails_Header_", "GroupDetails_Identification_Label_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "GroupDetails_GridDetails_Header_GroupType" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                 //ReadOnly test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_GridDetails_Header_", "GroupDetails_Identification_Label_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_GroupDetails_HeadersAndLabels_ReadOnly()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_GridDetails_Header_", "GroupDetails_Identification_Label_" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_GroupDetails_HeadersAndLabels_HideData()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "GroupDetails_GridDetails_Header_", "GroupDetails_Identification_Label_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }
    }
}
