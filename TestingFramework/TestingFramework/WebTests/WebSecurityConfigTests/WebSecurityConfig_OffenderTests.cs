﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure;


#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

#endregion


namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_OffenderTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_2Piece_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SendMessageNow", "Offender_Actions_Button_SendMessageNextCall" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().

                  //Invisible test
                  AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SendMessageNow", "Offender_Actions_Button_SendMessageNextCall" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").
                    
              ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_2Piece_SuspendProgram_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SuspendProgram", "Offender_Actions_Button_UnsuspendProgram" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigSuspendProgramTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().

                  //Invisible test
                  AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SuspendProgram", "Offender_Actions_Button_UnsuspendProgram" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigSuspendProgramTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                    ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1Piece_Main_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[]{
                       "Offender_Actions_Button_Upload",
                       "Offender_Actions_List_HeaderActions",
                       "Offender_General_Button_DeleteOffender",
                       "Offender_General_Button_Refresh",
                       "Offender_HeaderDetails_Button_OffenderPhoto",
                       "Offender_HeaderDetails_Label_OffenderAddress",
                       "Offender_HeaderDetails_Label_OffenderName"
                       }).

                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                    AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_Main>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().


                   //Invisible test
                  
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[]{
                       "Offender_Actions_Button_Upload",
                       "Offender_Actions_List_HeaderActions",
                       "Offender_General_Button_DeleteOffender",
                       "Offender_General_Button_Refresh",
                       "Offender_HeaderDetails_Button_OffenderPhoto",
                       "Offender_HeaderDetails_Label_OffenderAddress",
                       "Offender_HeaderDetails_Label_OffenderName"
                       }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_Main>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  ExecuteFlow();
        }

 //       [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
 //       [TestMethod]
 //       public void SecurityConfig_Offender_1Piece_Main_Invisible()
 //       {
 //           TestingFlow.
 //               AddBlock<GetGuiSecurityDataTestBlock>().
 //                   SetDescription("Get GUI security data of user").
 //                   SetProperty(x => x.FirstName).WithValue("orenAD").
 //                   SetProperty(x => x.Password).WithValue("Q1w2e3r4").
 //                   SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

 //               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
 //                  SetProperty(x => x.Refine).WithValue(new string[]{"Offender_Actions_Button_Upload",
 //                                                                       "Offender_Actions_List_HeaderActions",
 //                                                                       "Offender_General_Button_DeleteOffender",
 //                                                                       "Offender_General_Button_Refresh",
 //                                                                       "Offender_HeaderDetails_Button_OffenderPhoto",
 //                                                                       "Offender_HeaderDetails_Label_OffenderAddress",
 //                                                                       "Offender_HeaderDetails_Label_OffenderName"
 //}).
 //                  SetProperty(x => x.ExcludeItems).WithValue(new string[] {
 //                      "Offender_Actions_List_HeaderActions"
 //                  }).
 //                  SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
 //                  SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

 //               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
 //               SetDescription("Login page").

 //               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
 //                   SetDescription("Clear cache").

 //                   AddBlock<Web_SecurityConfigOffender_Main>().
 //               SetDescription("Navigation only SecurityConfigOffenderStatus test block").

 //               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
 //                 SetDescription("Security config test").

 //                 AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
 //                 SetDescription("Web security config assert block").
 //                  //Test excluded item

 //                  AddBlock<SetGuiSecurityDataForVisibilityTest>().
 //                  SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_List_HeaderActions" }).
 //                  SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
 //                  SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
 //                  SetProperty(x => x.ExcludeItems).WithValue(null).

 //                   AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
 //                   SetDescription("Clear cache").

 //                   AddBlock<Web_SecurityConfigOffender_Main>().
 //               SetDescription("Navigation only SecurityConfigOffenderStatus test block").
 //               SetProperty(x => x.ProgramType).WithValue("1 Piece").

 //               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
 //                 SetDescription("Security config test").

 //                 AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
 //                 SetDescription("Web security config assert block").

 //                   ExecuteFlow();
 //       }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1Piece_OffenderActions_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_ReallocateOffender", "Offender_Actions_Button_SendManualEvent" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF, Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout test block").

                  //  Invisible test
                 
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_ReallocateOffender", "Offender_Actions_Button_SendManualEvent" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                    ExecuteFlow();
        }

        [Ignore]//Not automated
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1PieceRF_Actions_Visible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_ClearTamper" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                  //SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<GetOffenderWithHomeunitTestBlock>().UsePreviousBlockData().
                  SetDescription("Get offender with Curefew home unit").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").
                    ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Offender_1Piece_OffenderActions_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_ReallocateOffender", "Offender_Actions_Button_SendManualEvent" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] {
        //               "Offender_Actions_List_HeaderActions"
        //           }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //            AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
        //        SetDescription("Navigation only SecurityConfigOffenderStatus test block").
        //        SetProperty(x => x.ProgramType).WithValue("1 Piece").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").
        //            ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1Piece_Actions_Visiblity()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SuspendProgram", "Offender_Actions_Button_SendVibration", "Offender_Actions_Button_SendEndOfService" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Offender_Actions_Button_SendEndOfServiceSolid" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").
                SetProperty(x => x.ProgramType).WithValue("1 Piece").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout").

                  //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SuspendProgram", "Offender_Actions_Button_SendVibration", "Offender_Actions_Button_SendEndOfService" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Offender_Actions_Button_SendEndOfServiceSolid" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").
                    SetProperty(x => x.ProgramType).WithValue("1 Piece").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Offender_1Piece_Actions_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { /*"Offender_Actions_Button_SuspendProgram",*/ "Offender_Actions_Button_ReallocateOffender", "Offender_Actions_Button_SendVibration" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] {
        //               "Offender_Actions_List_HeaderActions" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //            AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().
        //        SetDescription("Navigation only SecurityConfigOffenderStatus test block").
        //        SetProperty(x => x.ProgramType).WithValue("1 Piece").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").
        //            ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1Piece_Download_Visibility()
        {
            TestingFlow.
                
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                   
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_DownloadNextCall", "Offender_Actions_Button_DownloadNow" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF, Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_Download>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout").

                  //Invisible test1

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_DownloadNextCall"/*, "Offender_Actions_Button_DownloadNow"*/ }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_Download>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout").

                  //Invisible test2
               
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] {/* "Offender_Actions_Button_DownloadNextCall", */"Offender_Actions_Button_DownloadNow" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_Download>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout").

                  ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Offender_1Piece_Download_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_DownloadNextCall", "Offender_Actions_Button_DownloadNow" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Offender_Actions_List_HeaderActions" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //            AddBlock<Web_SecurityConfigOffender_Download>().
        //        SetDescription("Navigation only SecurityConfigOffenderStatus test block").
        //        SetProperty(x => x.ProgramType).WithValue("1 Piece").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").

        //            ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1Piece_PictureSaveAndClose_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_AddPicture_Button_SaveAndClose", "Offender_AddPicture_Button_Add", "Offender_AddPicture_Button_Delete" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF, Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_PicturePopup>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").
                    SetProperty(x => x.ProgramType).WithValue("1 Piece").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Web security config assert block").

                AddBlock<Web_RefreshPageBlock>().

                AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout test bock").

                //Invisible test
                
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_AddPicture_Button_SaveAndClose", "Offender_AddPicture_Button_Add", "Offender_AddPicture_Button_Delete" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_PicturePopup>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").
                    SetProperty(x => x.ProgramType).WithValue("1 Piece").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Web security config assert block").

                ExecuteFlow();
        }

        //[Ignore]//fix according to visible
        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Offender_1Piece_PictureSaveAndClose_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "Offender_AddPicture_Button_SaveAndClose" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //            AddBlock<Web_SecurityConfigOffender_PicturePopup>().
        //        SetDescription("Navigation only SecurityConfigOffenderStatus test block").
        //        SetProperty(x => x.ProgramType).WithValue("1 Piece").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").
        //            ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_1Piece_ReallocatePopup_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_ReallocatePopup_Label_Description", "Offender_ReallocatePopup_Label_Reason" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ReallocatePopup>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_RefreshPageBlock>().

                  AddBlock<Web_LogoutTestBlock>().

                  //Invisible test
                 
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_ReallocatePopup_Label_Description", "Offender_ReallocatePopup_Label_Reason" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_ReallocatePopup>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                    ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Offender_1Piece_ReallocatePopup_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "Offender_ReallocatePopup_Label_Description", "Offender_ReallocatePopup_Label_Reason" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Offender_Actions_List_HeaderActions" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //            AddBlock<Web_SecurityConfigOffender_ReallocatePopup>().
        //        SetDescription("Navigation only SecurityConfigOffenderStatus test block").
        //        SetProperty(x => x.ProgramType).WithValue("1 Piece").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").
        //            ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_E4_ReallocatePopup_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                   
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Get offender logic block").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                 AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "Offender_ReallocatePopup_Label_Description", "Offender_ReallocatePopup_Label_Reason" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_ReallocatePopup>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Web security config assert block").

                AddBlock<Web_RefreshPageBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "Offender_ReallocatePopup_Label_Description", "Offender_ReallocatePopup_Label_Reason" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_ReallocatePopup>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Web security config assert block").

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_E4_Actions_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SendEndOfServiceSolid" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

              AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

              AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

              AddBlock<Web_LogoutTestBlock>().

                  //Invisible tests
              AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
              AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SendEndOfServiceSolid" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

              AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

              AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

              AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

              AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

              AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                    ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_E4_OffenderActions_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_AddNewProgram" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().

                  //Invisible test
                  AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                   
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_AddNewProgram" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_E4_Web_OffenderActions_AddNewProgram_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                   
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_AddNewProgram" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                   SetDescription("Get offender logic block").
                   SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                   SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                   SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                   SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout test block").

                  //Invisible test
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
             
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_AddNewProgram" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                   SetDescription("Get offender logic block").
                   SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                   SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).
                   SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                   SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                   SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Web security config assert block").

            ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_Actions_Voice_Visible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SendEnrollment", "Offender_Actions_Button_SendSuspendTest", "Offender_Actions_Button_SendVoiceTest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Voice_Verification }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").
                
               ExecuteFlow();
        }

        //[Ignore]//Not applicable
        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Offender_Voice_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_SendEnrollment", "Offender_Actions_Button_SendSuspendTest", "Offender_Actions_Button_SendVoiceTest" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //            AddBlock<Web_SecurityConfigOffender_OffenderActionsTestBlock>().
        //        SetDescription("Navigation only SecurityConfigOffenderStatus test block").
        //        SetProperty(x => x.ProgramType).WithValue("voice").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").
        //            ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_RelatedOffender_Visibility()
        {
            TestingFlow.
                
                /* Visible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Get offender logic block").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_HeaderDetails_Label_RelatedOffenderName" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_RelatedOffender>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout test block").

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_HeaderDetails_Label_RelatedOffenderName" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigOffender_RelatedOffender>().UsePreviousBlockData().
                    SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                    ExecuteFlow();
        }
        [Ignore]
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_Alcohol_Visible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_Actions_Button_AFRSetup", "Offender_Actions_Button_SendAlcoholTest", "Offender_Actions_Button_SendEndRangeTest", "Offender_Actions_Button_SendStartRangeTest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.VBR_Cellular }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigOffender_ActionsTestBlock>().
                SetDescription("Navigation only SecurityConfigOffenderStatus test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").
                    ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_CurrentPlannedList_Visibility()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_General_Button_CurrentPlannedList" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfig_Offender_CurrentPlannedList>().
                SetDescription("Navigation only to Offender detailes - configuration test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().

                   //Invisible test
                   AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_General_Button_CurrentPlannedList" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfig_Offender_CurrentPlannedList>().
                SetDescription("Navigation only to Offender detailes - configuration test block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").
                    ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Offender_SummaryAdditionalDetails_Visible()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Offender_SummaryAdditionalDetails_Label_OffenderPhoto" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).
                   SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_OffendersListScreenExpandRow>().
                SetDescription("Navigation only to Offender list - expand row").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").
                    ExecuteFlow();
        }
    }
}
