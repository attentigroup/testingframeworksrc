﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_TrackerRulesTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_TrackerRules_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("VladimirSA").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "TrackerRules_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "TrackerRules_GridDetails_Label" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                 //Invisible test 1
                 AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("VladimirSA").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "TrackerRules_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "TrackerRules_GridDetails_Label" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_TrackerRules_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("VladimirSA").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "TrackerRules" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] { "Label" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_TrackerRules_ReadOnly_Enabled()
        {
            TestingFlow.
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "TrackerRules_GridDetails_Label_DisplayMessage", "TrackerRules_GridDetails_Label_GracePeriod", "TrackerRules_GridDetails_Label_ImmediateUpload", "TrackerRules_GridDetails_Label_Vibrate" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                  AddBlock<Web_LogoutTestBlock>().
                  SetDescription("Logout block").


                /// Enabled Test Starts Here ///
                
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "TrackerRules_GridDetails_Label_DisplayMessage", "TrackerRules_GridDetails_Label_GracePeriod", "TrackerRules_GridDetails_Label_ImmediateUpload", "TrackerRules_GridDetails_Label_Vibrate" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                  SetDescription("Web security config assert block").

                    ExecuteFlow();
        }


        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_TrackerRules_Enabled()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "TrackerRules_GridDetails_Label_DisplayMessage", "TrackerRules_GridDetails_Label_GracePeriod", "TrackerRules_GridDetails_Label_ImmediateUpload", "TrackerRules_GridDetails_Label_Vibrate" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //          SetDescription("Web security config assert block").
        //            ExecuteFlow();
        //}
    }
}
