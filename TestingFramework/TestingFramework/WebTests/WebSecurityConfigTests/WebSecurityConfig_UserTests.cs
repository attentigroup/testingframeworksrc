﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_UserTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_User_DisableAccount_Visibility()
        {
            TestingFlow.

               /* Visible Part Starts Here */

               AddBlock<GetGuiSecurityDataTestBlock>().
                   SetDescription("Get GUI security data of user").
                   //SetProperty(x => x.UserName).WithValue("orenAD").
                   //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                   //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "User_Actions_Button_DisableAccount" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                   SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                   SetDescription("Clear cache").

               AddBlock<Web_SecurityConfigUser_EnableDisableAccount>().
                   SetProperty(x => x.AccountStatus).WithValue("Disable").
                   SetDescription("Navigation only UserList test block").

               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                   SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                   SetDescription("Web security config assert block").

               AddBlock<Web_LogoutTestBlock>().
                   SetDescription("Security config test").

               /* Invisible Part Starts Here */

               AddBlock<GetGuiSecurityDataTestBlock>().
                   SetDescription("Get GUI security data of user").
                   //SetProperty(x => x.UserName).WithValue("orenAD").
                   //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                   //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "User_Actions_Button_DisableAccount" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                   SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                   SetDescription("Clear cache").

               AddBlock<Web_SecurityConfigUser_EnableDisableAccount>().
                   SetProperty(x => x.AccountStatus).WithValue("Disable").
                   SetDescription("Navigation only UserList test block").
                   
               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                   SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                   SetDescription("Web security config assert block").

               ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_User_DisableAccount_Invisible()
        //{

        //    TestingFlow.


        //              AddBlock<GetGuiSecurityDataTestBlock>().
        //                  SetDescription("Get GUI security data of user").
        //                  SetProperty(x => x.FirstName).WithValue("orenAD").
        //                  SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //                  SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //              AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //                 SetProperty(x => x.Refine).WithValue(new string[] { "User_Actions_Button_DisableAccount" }).
        //                 SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //                 SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).



        //              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //              SetDescription("Login page").

        //              AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //                  SetDescription("Clear cache").

        //                  AddBlock<Web_SecurityConfigUser_Disable>().
        //                  SetProperty(x => x.AccountStatus).WithValue("Disable").
        //              SetDescription("Navigation only UserList test block").


        //              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //                SetDescription("Security config test").

        //                    AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //                    SetDescription("Web security config assert block").
        //                  ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_User_EnableAccount_Visibility()
        {

            TestingFlow.

                    /* Visible Part Starts Here */

                    AddBlock<GetGuiSecurityDataTestBlock>().
                        SetDescription("Get GUI security data of user").
                        //SetProperty(x => x.UserName).WithValue("orenAD").
                        //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                        //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                    AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                        SetProperty(x => x.Refine).WithValue(new string[] { "User_Actions_Button_EnableAccount" }).
                        SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                        SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                    AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                        SetDescription("Login page").

                    AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                        SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigUser_EnableDisableAccount>().
                        SetProperty(x => x.AccountStatus).WithValue("Enable").
                        SetDescription("Navigation only UserList test block").

                    AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                        SetDescription("Security config test").

                    AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                        SetDescription("Web security config assert block").

                    AddBlock<Web_LogoutTestBlock>().
                        SetDescription("Security config test").

                    /* Invisible Part Starts Here */

                    AddBlock<GetGuiSecurityDataTestBlock>().
                        SetDescription("Get GUI security data of user").
                        //SetProperty(x => x.UserName).WithValue("orenAD").
                        //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                        //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                    AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                        SetProperty(x => x.Refine).WithValue(new string[] { "User_Actions_Button_EnableAccount" }).
                        SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                        SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                    AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                        SetDescription("Login page").

                    AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                        SetDescription("Clear cache").

                    AddBlock<Web_SecurityConfigUser_EnableDisableAccount>().
                        SetProperty(x => x.AccountStatus).WithValue("Enable").
                        SetDescription("Navigation only UserList test block").


                    AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                        SetDescription("Security config test").

                    AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                        SetDescription("Web security config assert block").

                    ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_User_EnableAccount_Invisible()
        //{
        //    TestingFlow.

        //              AddBlock<GetGuiSecurityDataTestBlock>().
        //                  SetDescription("Get GUI security data of user").
        //                  SetProperty(x => x.FirstName).WithValue("orenAD").
        //                  SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //                  SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //              AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //                 SetProperty(x => x.Refine).WithValue(new string[] { "User_Actions_Button_EnableAccount" }).
        //                 SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //                 SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //              SetDescription("Login page").

        //              AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //                  SetDescription("Clear cache").

        //                  AddBlock<Web_SecurityConfigUser_Enable>().
        //                  SetProperty(x => x.AccountStatus).WithValue("Enable").
        //              SetDescription("Navigation only UserList test block").


        //              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //                SetDescription("Security config test").

        //                    AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //                    SetDescription("Web security config assert block").
        //                  ExecuteFlow();
        //}
    }
}
