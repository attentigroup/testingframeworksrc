﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.APIExtensions;
using LogicBlocksLib.Events;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.EventsBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

#endregion

using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using TestBlocksLib.DevicesBlocks;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_OffenderLocationTests : WebBaseUnitTest
    {       

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderLocation_AddEditDeleteZone_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
        //        Offenders0.EnmProgramStatus.PreActive}).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<CreateMsgAddCircularZoneRequestBlock>().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
        //            SetProperty(x => x.Name).WithValue("Work").
        //            SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
        //            SetProperty(x => x.RealRadius).WithValue(50).
        //            SetProperty(x => x.BufferRadius).WithValue(10).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_AddNewZone",
        //               "OffenderLocation_Zones_Button_DeleteZone", "OffenderLocation_Zones_Button_EditZone" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndMoveCursorToZonesListRowTestBlock>().
        //            SetProperty(x => x.OffenderRefId).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
        //            SetProperty(x => x.receiverSN).
        //             WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderLocation_SaveZone_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
        //        Offenders0.EnmProgramStatus.PreActive}).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<CreateMsgAddCircularZoneRequestBlock>().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
        //            SetProperty(x => x.Name).WithValue("Work").
        //            SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
        //            SetProperty(x => x.RealRadius).WithValue(50).
        //            SetProperty(x => x.BufferRadius).WithValue(10).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_SaveZone" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndOpenEditCircularZonePopupTestBlock>().
        //            SetProperty(x => x.OffenderRefId).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
        //            SetProperty(x => x.ReceiverSN).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderLocation_AddPointsOfInterest_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
        //        Offenders0.EnmProgramStatus.PreActive}).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<CreateMsgAddCircularZoneRequestBlock>().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
        //            SetProperty(x => x.Name).WithValue("Work").
        //            SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
        //            SetProperty(x => x.RealRadius).WithValue(50).
        //            SetProperty(x => x.BufferRadius).WithValue(10).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_AddPointsOfInterest" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndOpenAddZoneMenuTestBlock>().
        //            SetProperty(x => x.OffenderRefId).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
        //            SetProperty(x => x.receiverSN).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderLocation_SavePointsOfInterest_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
        //        Offenders0.EnmProgramStatus.PreActive}).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<CreateMsgAddCircularZoneRequestBlock>().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
        //            SetProperty(x => x.Name).WithValue("Work").
        //            SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
        //            SetProperty(x => x.RealRadius).WithValue(50).
        //            SetProperty(x => x.BufferRadius).WithValue(10).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_SavePointsOfInterest" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndOpenAddPOIPopupTestBlock>().
        //            SetProperty(x => x.OffenderRefId).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderLocation_PointSortedTime_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //            SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //            SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<RunXTSimulatorTestBlock>().
        //            SetDescription("Run XT Simulator Test Block").
        //            SetProperty(x => x.EquipmentSN)./*WithValue("35013370").*/
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_PointToolTip_Label_PointStoredTime" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndOpenPointInformationTooltipTestBlock>().
        //            SetProperty(x => x.OffenderRefId).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_ZoneTooltip_EditDeleteZone_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.StateID).WithValue("IL").

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                   
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_ZoneTooltip_Button_EditZone",
                       "OffenderLocation_ZoneTooltip_Button_DeleteZone" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenZoneTooltipTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

                /* Invisible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_ZoneTooltip_Button_EditZone",
                       "OffenderLocation_ZoneTooltip_Button_DeleteZone" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenZoneTooltipTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_PointStoredTime()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    //SetProperty(x => x.Rule).WithValue("Panic_Activated-first_violation").
                    SetProperty(x => x.EquipmentSN)./*WithValue("35620587").*/
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_PointToolTip_Label_PointStoredTime" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenPointInformationTooltipTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_PointToolTip_Label_PointStoredTime" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenPointInformationTooltipTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_MapToolbar_Trail_Visibility()
        {
            TestingFlow.

            /* Visible Part Starts Here */

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
               
                SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
               

            AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

            AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

            AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_MapToolbar_", "OffenderLocation_Trail_" }).
                SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderLocation_Trail_Button_Events" }).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

            AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_SelectOffenderTestBlock>().
                SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.PrimaryTabLocatorString).WithValue("Location").

            AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().UsePreviousBlockData().
                SetDescription("Security Config Element Shown Assert Block").

            AddBlock<Web_LogoutTestBlock>().
                SetDescription("Web Logout Test Block").

            /* Invisible Part Starts Here */

            AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

            AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_MapToolbar_", "OffenderLocation_Trail_" }).
                SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderLocation_Trail_Button_Events" }).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

            AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

            AddBlock<Web_SelectOffenderTestBlock>().
                SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.PrimaryTabLocatorString).WithValue("Location").

            AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
              SetDescription("Security config test").

            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().UsePreviousBlockData().
                SetDescription("Security Config Element Shown Assert Block").

            ExecuteFlow();
        }

       

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_Events_Visibility()
        {
            TestingFlow.
                
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                   

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                SetDescription("Get GUI security data of user").

               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Trail_Button_Events" }).
            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

            AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
              SetProperty(x => x.PrimaryTabLocatorString).WithValue("Location").

            AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
              SetDescription("Security config test").

            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            AddBlock<Web_LogoutTestBlock>().

            //Invisible test
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Trail_Button_Events" }).
            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

            AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
              SetProperty(x => x.PrimaryTabLocatorString).WithValue("Location").

            AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
              SetDescription("Security config test").

            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_AddEditDeleteZone_Visible()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_AddNewZone",
                       "OffenderLocation_Zones_Button_DeleteZone", "OffenderLocation_Zones_Button_EditZone" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCursorToZonesListRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

                /* Invisible Part Starts Here */
               
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_AddNewZone",
                       "OffenderLocation_Zones_Button_DeleteZone", "OffenderLocation_Zones_Button_EditZone" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCursorToZonesListRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_SaveZone_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_SaveZone" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenEditCircularZonePopupTestBlock>().
                    SetProperty(x => x.ReceiverSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

               /* Invisible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_SaveZone" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenEditCircularZonePopupTestBlock>().
                    SetProperty(x => x.ReceiverSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_AddPointsOfInterest_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_AddPointsOfInterest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenAddZoneMenuTestBlock>().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.receiverSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

             /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_AddPointsOfInterest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenAddZoneMenuTestBlock>().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.receiverSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderLocation_SavePointsOfInterest_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_SavePointsOfInterest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenAddPOIPopupTestBlock>().
                    SetProperty(x => x.ReceiverSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Security config test").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

            /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_Zones_Button_SavePointsOfInterest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenAddPOIPopupTestBlock>().
                    SetProperty(x => x.ReceiverSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Security config test").

            ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderLocation_ZoneTooltip_EditDeleteZone_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetProperty(x => x.CityID).WithValue("TLV").
        //            SetProperty(x => x.Language).WithValue("ENG").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
        //            SetProperty(x => x.StateID).WithValue("IL").

        //        AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").

        //        AddBlock<CreateMsgAddCircularZoneRequestBlock>().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("AddOffenderTestBlock").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
        //            SetProperty(x => x.Name).WithValue("Work").
        //            SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
        //            SetProperty(x => x.RealRadius).WithValue(50).
        //            SetProperty(x => x.BufferRadius).WithValue(10).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //            SetProperty(x => x.OffenderID).
        //                WithValueFromBlockIndex("AddOffenderTestBlock").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderLocation_ZoneTooltip_Button_EditZone",
        //               "OffenderLocation_ZoneTooltip_Button_DeleteZone" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndOpenZoneTooltipTestBlock>().
        //            SetProperty(x => x.OffenderRefId).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}
    }
}
