﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_OffendersListTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffendersList_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffendersList_List_Section_Filter", "OffendersList_List_GridHeader_DateTime2",
                       "OffendersList_List_GridHeader_DateTime3", "OffendersList_List_GridHeader_DateTime4", "OffendersList_List_GridHeader_DateTime5",
                       "OffendersList_List_GridHeader_ListItems2", "OffendersList_List_GridHeader_ListItems10", "OffendersList_List_GridHeader_ListItems3",
                       "OffendersList_List_GridHeader_ListItems4", "OffendersList_List_GridHeader_ListItems5", "OffendersList_List_GridHeader_ListItems6",
                       "OffendersList_List_GridHeader_ListItems7", "OffendersList_List_GridHeader_ListItems8", "OffendersList_List_GridHeader_ListItems9",
                       "OffendersList_List_GridHeader_Numeric2", "OffendersList_List_GridHeader_Numeric3", "OffendersList_List_GridHeader_Numeric4",
                       "OffendersList_List_GridHeader_Numeric5","OffendersList_List_GridHeader_Text12Characters2","OffendersList_List_GridHeader_Text12Characters3",
                   "OffendersList_List_GridHeader_Text12Characters4","OffendersList_List_GridHeader_Text12Characters5","OffendersList_List_GridHeader_Text12Characters6",
                   "OffendersList_List_GridHeader_Text12Characters7","OffendersList_List_GridHeader_Text12Characters8","OffendersList_List_GridHeader_Text25Characters2",
                   "OffendersList_List_GridHeader_Text25Characters3","OffendersList_List_GridHeader_Text25Characters4","OffendersList_List_GridHeader_Text25Characters5",
                   "OffendersList_List_GridHeader_Text25Characters6","OffendersList_List_GridHeader_Text25Characters7","OffendersList_List_GridHeader_Text25Characters8",
                   "OffendersList_List_GridHeader_Text25Characters9","OffendersList_List_GridHeader_Text25Characters10","OffendersList_List_GridHeader_Text25Characters11",
                   "OffendersList_List_GridHeader_Text25Characters12","OffendersList_List_GridHeader_Text25Characters13","OffendersList_List_GridHeader_Text25Characters14",
                   "OffendersList_List_GridHeader_Text25Characters15", "OffendersList_List_GridHeader_Text50Characters2","OffendersList_List_GridHeader_Text50Characters3",
                   "OffendersList_List_GridHeader_Text50Characters4","OffendersList_List_GridHeader_Text100Characters2","OffendersList_List_GridHeader_Text100Characters3",
                   "OffendersList_List_GridHeader_Text100Characters4", "OffendersList_List_GridHeader_Text255Characters2","OffendersList_List_GridHeader_Text255Characters3"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout").

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffendersList_List_Section_Filter", "OffendersList_List_GridHeader_DateTime2",
                   "OffendersList_List_GridHeader_DateTime3", "OffendersList_List_GridHeader_DateTime4", "OffendersList_List_GridHeader_DateTime5",
                   "OffendersList_List_GridHeader_ListItems2", "OffendersList_List_GridHeader_ListItems10", "OffendersList_List_GridHeader_ListItems3",
                   "OffendersList_List_GridHeader_ListItems4", "OffendersList_List_GridHeader_ListItems5", "OffendersList_List_GridHeader_ListItems6",
                   "OffendersList_List_GridHeader_ListItems7", "OffendersList_List_GridHeader_ListItems8", "OffendersList_List_GridHeader_ListItems9",
                   "OffendersList_List_GridHeader_Numeric2", "OffendersList_List_GridHeader_Numeric3", "OffendersList_List_GridHeader_Numeric4",
                   "OffendersList_List_GridHeader_Numeric5","OffendersList_List_GridHeader_Text12Characters2","OffendersList_List_GridHeader_Text12Characters3",
                   "OffendersList_List_GridHeader_Text12Characters4","OffendersList_List_GridHeader_Text12Characters5","OffendersList_List_GridHeader_Text12Characters6",
                   "OffendersList_List_GridHeader_Text12Characters7","OffendersList_List_GridHeader_Text12Characters8","OffendersList_List_GridHeader_Text25Characters2",
                   "OffendersList_List_GridHeader_Text25Characters3","OffendersList_List_GridHeader_Text25Characters4","OffendersList_List_GridHeader_Text25Characters5",
                   "OffendersList_List_GridHeader_Text25Characters6","OffendersList_List_GridHeader_Text25Characters7","OffendersList_List_GridHeader_Text25Characters8",
                   "OffendersList_List_GridHeader_Text25Characters9","OffendersList_List_GridHeader_Text25Characters10","OffendersList_List_GridHeader_Text25Characters11",
                   "OffendersList_List_GridHeader_Text25Characters12","OffendersList_List_GridHeader_Text25Characters13","OffendersList_List_GridHeader_Text25Characters14",
                   "OffendersList_List_GridHeader_Text25Characters15", "OffendersList_List_GridHeader_Text50Characters2","OffendersList_List_GridHeader_Text50Characters3",
                   "OffendersList_List_GridHeader_Text50Characters4","OffendersList_List_GridHeader_Text100Characters2","OffendersList_List_GridHeader_Text100Characters3",
                   "OffendersList_List_GridHeader_Text100Characters4", "OffendersList_List_GridHeader_Text255Characters2","OffendersList_List_GridHeader_Text255Characters3"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffendersList_Filter_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.UserName).WithValue("YairOF").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.UserName).WithValue("YairOF").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffendersList_Invisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        AddBlock<Web_LogoutTestBlock>().
        //        SetDescription("Logout test block").
        //        //Test the excluded filter section for invisibility

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_List_Section_Filter" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(null).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //           AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffendersList_DownloadAllButton_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_Button_DownloadAll" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnFilterTestBlock>().
                    SetProperty(x => x.filterTitle).WithValue("Download Recommended").
                    SetProperty(x => x.sectionNumber).WithValue(-1).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout").

                     // Invisible test
                     AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_Button_DownloadAll" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnFilterTestBlock>().
                    SetProperty(x => x.filterTitle).WithValue("Download Recommended").
                    SetProperty(x => x.sectionNumber).WithValue(-1).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffendersList_LegenedButton_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_Button_Legend" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnFilterTestBlock>().
                    SetProperty(x => x.filterTitle).WithValue("Offender Summary").
                    SetProperty(x => x.sectionNumber).WithValue(-1).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().

                    //Invisible test
                    AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_Button_Legend" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnFilterTestBlock>().
                    SetProperty(x => x.filterTitle).WithValue("Offender Summary").
                    SetProperty(x => x.sectionNumber).WithValue(-1).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffendersList_RefreshButton_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_Button_Refresh" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnFilterTestBlock>().
                    SetProperty(x => x.filterTitle).WithValue("Offender Summary").
                    SetProperty(x => x.sectionNumber).WithValue(-1).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffendersList_General_Button_Refresh" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnFilterTestBlock>().
                    SetProperty(x => x.filterTitle).WithValue("Offender Summary").
                    SetProperty(x => x.sectionNumber).WithValue(-1).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }
    }
}
