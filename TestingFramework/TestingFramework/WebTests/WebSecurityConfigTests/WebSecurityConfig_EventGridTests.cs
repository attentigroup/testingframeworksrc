﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.Events;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.EventsBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;

#region API refs

using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

#endregion

using TestBlocksLib.DevicesBlocks;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_EventGridTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_AdditionalData_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_AdditionalData_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndExpandTestBlock>().
                SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_AdditionalData_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndExpandTestBlock>().
                    SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                    FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_LatestEvent_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_LatestEvents_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndPressOpenRecentEvent>().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                  AddBlock<Web_RefreshPageBlock>().

                  AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_LatestEvents_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndPressOpenRecentEvent>().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_List_GridHeader_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_GridHeader_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_GridHeader_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_SystemAdditionalData_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_UnHandle", "EventGrid_SystemAdditionalData_Button_LinkedEvents" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndOpenHandlingOptionMenu>().UsePreviousBlockData().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("GetEventsTestBlock").
                FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_UnHandle", "EventGrid_SystemAdditionalData_Button_LinkedEvents" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndOpenHandlingOptionMenu>().UsePreviousBlockData().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("GetEventsTestBlock").
                FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_SystemAdditionalData_Unhandle_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.Comment).WithValue("Handled by automation").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_UnHandle" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndExpandTestBlock>().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock").
                FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.EventID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                  AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_UnHandle" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndExpandTestBlock>().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock").
                FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.EventID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_OffenderAdditionalData_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                     SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_" }).
                     SetProperty(x => x.ExcludeItems).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_UnHandle",
                         "EventGrid_OffenderAdditionalData_Button_LinkedEvents", "EventGrid_OffenderAdditionalData_Button_AlcoholTestResults",
                     "EventGrid_OffenderAdditionalData_Button_BreathAlcoholTest", "EventGrid_OffenderAdditionalData_Button_ClearPanic",
                     "EventGrid_OffenderAdditionalData_Button_ClearTamper", "EventGrid_OffenderAdditionalData_Button_DVDistance",
                      "EventGrid_OffenderAdditionalData_Button_VoiceRetries",
                     "EventGrid_OffenderAdditionalData_Button_VoiceTest", "EventGrid_OffenderAdditionalData_Button_ShowLocation"}).
                     SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                     SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndOpenHandlingOptionMenu>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                     SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_" }).
                     SetProperty(x => x.ExcludeItems).WithValue(new string[] {"EventGrid_OffenderAdditionalData_Button_HandlingOptions", "EventGrid_OffenderAdditionalData_Button_UnHandle",
                         "EventGrid_OffenderAdditionalData_Button_LinkedEvents", "EventGrid_OffenderAdditionalData_Button_AlcoholTestResults",
                     "EventGrid_OffenderAdditionalData_Button_BreathAlcoholTest", "EventGrid_OffenderAdditionalData_Button_ClearPanic",
                     "EventGrid_OffenderAdditionalData_Button_ClearTamper", "EventGrid_OffenderAdditionalData_Button_DVDistance",
                      "EventGrid_OffenderAdditionalData_Button_VoiceRetries",
                     "EventGrid_OffenderAdditionalData_Button_VoiceTest", "EventGrid_OffenderAdditionalData_Button_ShowLocation"}).
                     SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                     SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndOpenHandlingOptionMenu>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_OffenderAdditionalData_Unhandle_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                 SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                 SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                 SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                 SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                 SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.Comment).WithValue("Handled by automation").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventId).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_UnHandle" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndExpandTestBlock>().
                SetProperty(x => x.eventID).
                WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock").
                FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.EventID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                  AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                //AddBlock<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>().

                //AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
                //    SetDescription("Create Add handling action request").
                //    SetProperty(x => x.Comment).WithValue("Handled by automation").
                //    SetProperty(x => x.EventID).
                //        WithValueFromBlockIndex("GetOffenderEvent_NewInProccessAndAutoProccessTestBlock").
                //        FromDeepProperty<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>(x => x.OffenderEvent.ID).
                //    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                //AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                //    SetDescription("Add handling action test").

                //AddBlock<GetGuiSecurityDataTestBlock>().
                //    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_UnHandle" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventAndExpandTestBlock>().
                   SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock").
                    FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.EventID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                  AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_OffenderAdditionalData_ShowLocation_Visibility()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetEventsRequestBlock>().
                 SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                 SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                 SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                 SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).

               AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

               AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                    FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.OffenderId).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ShowLocation" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                      SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                      SetDescription("Clear cache").

               AddBlock<Web_OffenderCurrentStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Offender current status test block").
                    SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

               AddBlock<Web_LogoutTestBlock>().

               //Invisible test
               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                     SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ShowLocation" }).
                     SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                     SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                      SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                      SetDescription("Clear cache").

               AddBlock<Web_OffenderCurrentStatusTestBlock>().UsePreviousBlockData().
               SetDescription("Offender current status test block").

               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_SystemAdditionalData_LinkedEvents_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.Comment).WithValue("Handled by automation").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
                    SetProperty(x => x.ScheduledTaskType).WithValue("13").
                    SetProperty(x => x.ScheduledTaskSubType).WithValue("USERDEF").
                    SetProperty(x => x.ScheduledTaskTime).WithValue(DateTime.Now.AddMinutes(2)).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_LinkedEvents" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectScheduleTaskEventAndExpandTestBlock>().
                SetProperty(x => x.Recent).WithValue("max").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_LinkedEvents" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectScheduleTaskEventAndExpandTestBlock>().
                SetProperty(x => x.Recent).WithValue("48").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_OffenderAdditionalData_LinkedEvents_Visibility()
        {
            TestingFlow.

                AddBlock<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>().

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.Comment).WithValue("Handled by automation").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetOffenderEvent_NewInProccessAndAutoProccessTestBlock").
                            FromDeepProperty<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>(x => x.OffenderEvent.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
                    SetProperty(x => x.ScheduledTaskType).WithValue("13").
                    SetProperty(x => x.ScheduledTaskSubType).WithValue("USERDEF").
                    SetProperty(x => x.ScheduledTaskTime).WithValue(DateTime.Now.AddMinutes(2)).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_LinkedEvents" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectScheduleTaskEventAndExpandTestBlock>().
                SetProperty(x => x.Recent).WithValue("max").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_LinkedEvents" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectScheduleTaskEventAndExpandTestBlock>().
                SetProperty(x => x.Recent).WithValue("max").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_OffenderAdditionalData_ClearPanic_Visibility()
        {
            TestingFlow.

                 /* Visible Part Starts Here */

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                //Get recent event before simulator
                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().SetName("EventsBefore").

                AddBlock<RunXTSimulatorTestBlock>().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_PANIC;HW_BTX_STRAP"). // Panic_Activated-first_violation
                    SetProperty(x => x.EquipmentSN)./*WithValue("35013370").*/
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                //Wait for the entrance of new event related to equipment
                AddBlock<WaitForNextEventTestBlock>().UsePreviousBlockData().SetName("EventsBefore").
                 SetProperty(x => x.GetEventsResponse).WithValueFromBlockIndex("EventsBefore").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ClearPanic" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock>().
                SetProperty(x => x.message).WithValue("Victim Panic Alert").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).
                  SetProperty(x => x.Prepration).WithValue(true).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Web Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ClearPanic" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock>().
                SetProperty(x => x.message).WithValue("Victim Panic Alert").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).
                  SetProperty(x => x.Prepration).WithValue(true).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_OffenderAdditionalData_ClearTamper_Visibility()
        {
            TestingFlow.

                 /* Visible Part Starts Here */

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<GetOffenderWithHomeunitTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender with Curefew home unit").
                //*** Wait for new event design starts here ***
                //Get recent event before simulator
                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventID).
                SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().SetName("EventsBefore").

                AddBlock<RunXTSimulatorTestBlock>().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_BU_TAMPER").//Home_Unit_Tamper-first_violation
                    SetProperty(x => x.EquipmentSN)./*WithValue("34258268").*/
                        WithValueFromBlockIndex("GetOffenderWithHomeunitTestBlock").
                            FromDeepProperty<GetOffenderWithHomeunitTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                //Wait for the entrance of new event related to equipment
                //AddBlock<WaitForNextEventTestBlock>().UsePreviousBlockData().SetName("EventsBefore").
                // SetProperty(x => x.GetEventsResponse).WithValueFromBlockIndex("EventsBefore").
                //    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).
                //    SetProperty(x => x.SecondsTimeOut).WithValue(30).
                //*** Wait for new event design ends here ***


                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ClearTamper" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock>().
                SetProperty(x => x.message).WithValue("Home Unit Tamper").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).
                  SetProperty(x => x.Prepration).WithValue(true).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Web Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ClearTamper" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock>().
                SetProperty(x => x.message).WithValue("Home Unit Tamper").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").
                  SetProperty(x => x.Type).WithValue(null).
                  SetProperty(x => x.Prepration).WithValue(true).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGrid_QuickHandle_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
               
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_Button_QuickHandle" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_MoveCursorToQuickHandleTestBlock>().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventId).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").
                    SetProperty(x => x.Prepration).WithValue(true).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Web Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_Button_QuickHandle" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_MoveCursorToQuickHandleTestBlock>().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventId).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").
                    SetProperty(x => x.Prepration).WithValue(true).

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }



        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_EventGridAgencyName_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
               
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_GridHeader_AgencyName" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_GridHeader_AgencyName" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGridQuickHandleInvisible()
        //{
        //    TestingFlow.


        //        AddBlock<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_Button_QuickHandle" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_MoveCursorToQuickHandleTestBlock>().
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetOffenderEvent_NewInProccessAndAutoProccessTestBlock").
        //                    FromDeepProperty<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>(x => x.OffenderEvent.ID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //            ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_AdditionalData_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetEventWithOffenderTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_AdditionalData_" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventAndExpandTestBlock>().
        //        SetProperty(x => x.EventID).
        //        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
        //        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_LatestEvent_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetEventWithOffenderTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_LatestEvents_" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventAndPressOpenRecentEvent>().
        //        SetProperty(x => x.EventID).
        //        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
        //        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_List_GridHeader_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_List_GridHeader_" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_SystemAdditionalData_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetEventWithOffenderTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_" }).
        //           SetProperty(x => x.ExcludeItems).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_UnHandle", "EventGrid_SystemAdditionalData_Button_LinkedEvents" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventAndOpenHandlingOptionMenu>().
        //        SetProperty(x => x.EventID).
        //        WithValueFromBlockIndex("GetEventsTestBlock").
        //        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_SystemAdditionalData_Unhandle_Invisible()
        //{
        //    TestingFlow.

        //         AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetDescription("Create get events request").
        //            SetName("GetEventsRequestBlock_BeforeHandling").
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Limit).WithValue(1).
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
        //            SetDescription("Get events test").

        //        AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
        //            SetDescription("Create Add handling action request").
        //            SetProperty(x => x.Comment).WithValue("Handled by automation").
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetEventsTestBlock").
        //                FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
        //            SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

        //        AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add handling action test").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_UnHandle" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventAndExpandTestBlock>().
        //        SetProperty(x => x.EventID).
        //        WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock").
        //        FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.EventID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").
        //          SetProperty(x => x.Type).WithValue(null).

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_OffenderAdditionalData_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetEventWithOffenderTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //             SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_" }).
        //             SetProperty(x => x.ExcludeItems).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_UnHandle",
        //                 "EventGrid_OffenderAdditionalData_Button_LinkedEvents", "EventGrid_OffenderAdditionalData_Button_AlcoholTestResults",
        //             "EventGrid_OffenderAdditionalData_Button_BreathAlcoholTest", "EventGrid_OffenderAdditionalData_Button_ClearPanic",
        //             "EventGrid_OffenderAdditionalData_Button_ClearTamper", "EventGrid_OffenderAdditionalData_Button_DVDistance",
        //              "EventGrid_OffenderAdditionalData_Button_VoiceRetries", "EventGrid_OffenderAdditionalData_Button_HandlingOptions",
        //             "EventGrid_OffenderAdditionalData_Button_VoiceTest", "EventGrid_OffenderAdditionalData_Button_ShowLocation"}).
        //             SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //             SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventAndOpenHandlingOptionMenu>().
        //            SetProperty(x => x.EventID).
        //            WithValueFromBlockIndex("GetEventsTestBlock").
        //            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_OffenderAdditionalData_Unhandle_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>().

        //        AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
        //            SetDescription("Create Add handling action request").
        //            SetProperty(x => x.Comment).WithValue("Handled by automation").
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetOffenderEvent_NewInProccessAndAutoProccessTestBlock").
        //                FromDeepProperty<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>(x => x.OffenderEvent.ID).
        //            SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

        //        AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add handling action test").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_UnHandle" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventAndExpandTestBlock>().
        //        SetProperty(x => x.EventID).
        //        WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock").
        //        FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.EventID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").
        //          SetProperty(x => x.Type).WithValue(null).

        //          AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_OffenderAdditionalData_ShowLocation_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
        //        SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetEventWithOffenderTestBlock>().

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //          AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //             SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ShowLocation",
        //                 "EventGrid_OffenderAdditionalData_Button_HandlingOptions" }).
        //             SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //             SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //          AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //              SetDescription("Login page").

        //          AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //              SetDescription("Clear cache").

        //          AddBlock<Web_FilterByTimeAndSelectEvent>().
        //          SetProperty(x => x.EventID).
        //          WithValueFromBlockIndex("GetEventsTestBlock").
        //          FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
        //          SetProperty(x => x.Recent).WithValue("72").

        //          AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //        ExecuteFlow();
        //}


        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_SystemAdditionalData_LinkedEvents_Invisible()
        //{
        //    TestingFlow.

        //         AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetDescription("Create get events request").
        //            SetName("GetEventsRequestBlock_BeforeHandling").
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Limit).WithValue(1).
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
        //            SetDescription("Get events test").

        //        AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
        //            SetDescription("Create Add handling action request").
        //            SetProperty(x => x.Comment).WithValue("Handled by automation").
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetEventsTestBlock").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
        //            SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
        //            SetProperty(x => x.ScheduledTaskType).WithValue("13").
        //            SetProperty(x => x.ScheduledTaskSubType).WithValue("USERDEF").
        //            SetProperty(x => x.ScheduledTaskTime).WithValue(DateTime.Now.AddMinutes(2)).

        //        AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add handling action test").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_SystemAdditionalData_Button_LinkedEvents" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectScheduleTaskEventAndExpandTestBlock>().
        //        SetProperty(x => x.Recent).WithValue("48").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").
        //          SetProperty(x => x.Type).WithValue(null).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_OffenderAdditionalData_LinkedEvents_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>().

        //        AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
        //            SetDescription("Create Add handling action request").
        //            SetProperty(x => x.Comment).WithValue("Handled by automation").
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetOffenderEvent_NewInProccessAndAutoProccessTestBlock").
        //                    FromDeepProperty<GetOffenderEvent_NewInProccessAndAutoProccessTestBlock>(x => x.OffenderEvent.ID).
        //            SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
        //            SetProperty(x => x.ScheduledTaskType).WithValue("13").
        //            SetProperty(x => x.ScheduledTaskSubType).WithValue("USERDEF").
        //            SetProperty(x => x.ScheduledTaskTime).WithValue(DateTime.Now.AddMinutes(2)).

        //        AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add handling action test").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_LinkedEvents" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectScheduleTaskEventAndExpandTestBlock>().
        //        SetProperty(x => x.Recent).WithValue("72").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").
        //          SetProperty(x => x.Type).WithValue(null).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_OffenderAdditionalData_ClearPanic_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
        //        SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
        //        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

        //        AddBlock<GetOffendersTestBlock>().

        //        AddBlock<RunXTSimulatorTestBlock>().
        //            SetDescription("Run XT Simulator Test Block").
        //            SetProperty(x => x.Rule).WithValue("Panic_Activated-first_violation").
        //            SetProperty(x => x.EquipmentSN)./*WithValue("35013370").*/
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ClearPanic" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock>().
        //        SetProperty(x => x.message).WithValue("Victim Panic Alert").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").
        //          SetProperty(x => x.Type).WithValue(null).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_EventGrid_OffenderAdditionalData_ClearTamper_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //               SetDescription("Get offender logic block").
        //               SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
        //               SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //               SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
        //          //SetProperty(x => x.Limit).WithValue(1).

        //          AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
        //               SetDescription("Get offender test block").

        //          AddBlock<GetOffenderWithHomeunitTestBlock>().UsePreviousBlockData().
        //          SetDescription("Get offender with Curefew home unit").

        //         AddBlock<RunXTSimulatorTestBlock>().
        //            SetDescription("Run XT Simulator Test Block").
        //            SetProperty(x => x.Rule).WithValue("Home_Unit_Tamper-first_violation").
        //            SetProperty(x => x.EquipmentSN)./*WithValue("34258268").*/
        //            WithValueFromBlockIndex("GetOffenderWithHomeunitTestBlock").
        //            FromDeepProperty<GetOffenderWithHomeunitTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "EventGrid_OffenderAdditionalData_Button_ClearTamper" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectEventByMessageAndOpenHandlingOptionMenuTestBlock>().
        //        SetProperty(x => x.message).WithValue("Home Unit Tamper").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").
        //          SetProperty(x => x.Type).WithValue(null).

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}
    }
}
