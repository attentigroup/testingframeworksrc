﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using LogicBlocksLib.ScheduleBlocks;
using TestBlocksLib.ScheduleBlocks;
using WebTests.InfraStructure;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

#endregion



namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_ScheduleTests : WebBaseUnitTest
    {
        Offenders0.EnmProgramType[] voice = { Offenders0.EnmProgramType.Voice_Verification };
        Offenders0.EnmProgramStatus[] preActive = { Offenders0.EnmProgramStatus.PreActive };
        #region Tests Parameters for Zones0

        Zones0.EntPoint Point_AddCircularZone = new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones0.EntPoint[] Points_AddPolygonZone = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones0.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones0.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones0.EntPoint[] Points_UpdatePolygonZone = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones0.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones0.EntPoint() { LAT = 31.105820, LON = 33.834568 } };


        #endregion

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Schedule_Actions_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_Actions_Button_AddTimeFrame", "Schedule_Actions_Button_DeleteAllTimeFrames", "Schedule_Actions_Button_SaveOffenderSchedule" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    
                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("Schedule").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                   SetDescription("Security config test").

                /* Invisible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                  
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_Actions_Button_AddTimeFrame", "Schedule_Actions_Button_DeleteAllTimeFrames", "Schedule_Actions_Button_SaveOffenderSchedule" }).
                   //SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_General_Button_SaveAdditionalData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    
                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("Schedule").
                
                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                
            ExecuteFlow();
        }

        

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Schedule_TimeFrameTooltip_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                   
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                  
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneName).WithValue("Oren's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.GraceTime).WithValue(5).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(5)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_TimeFrameTooltip_" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

               AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

               AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("Schedule").

               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

               AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

               /* Invisible Part Starts Here */

               AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                   
               AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_TimeFrameTooltip_" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

               AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

               AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

               AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

               AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("Schedule").

               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

               AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

       

        [Ignore]
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Schedule_Actions_RepeatVoiceTests_Visible()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(voice).
                    SetProperty(x => x.ProgramStatus).WithValue(preActive).
                AddBlock<GetOffendersTestBlock>().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_Actions_Button_RepeatVoiceTests" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("Schedule").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }


       

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_Schedule_Actions_SaveGroupSchedule_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_Actions_Button_SaveGroupSchedule" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                   SetDescription("Security config test").

                /* Invisible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_Actions_Button_SaveGroupSchedule" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_Schedule_Actions_SaveGroupSchedule_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "Schedule_Actions_Button_SaveGroupSchedule" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}
    }
}
