﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_GeneralTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void Security_General_Visibility()
        {
            TestingFlow.

            AddBlock<GetGuiSecurityDataTestBlock>().
              SetDescription("Get GUI security data of user").
                
              AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] { "General_" }).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

              AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

              AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout").

              AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

              ////////Invisible test1 =====================
              
              AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] { "General_" }).
                SetProperty(x => x.ExcludeItems).WithValue(new string[] {
                "General_Toolbar_Button_Offenders", "General_Toolbar_Button_System", "General_Toolbar_Button_Reports",
                "General_Screen_Link_Monitor","General_Screen_Link_Dashboard","General_Screen_Link_OffendersList",

                "General_Screen_Link_GroupDetails", "General_Toolbar_ListItem_AddNewUser", "General_Screen_Link_GroupParametes",
                "General_Screen_Link_UserAgencies","General_Screen_Link_UserConfiguration","General_Screen_Link_UserDetails",
                "General_Screen_Link_UserOffenders","General_Toolbar_Button_Logout", "General_Toolbar_ListItem_User",
                "General_Toolbar_ListItem_SystemConfiguration", "General_Toolbar_ListItem_ReceiverVersionsImport", "General_Screen_Link_GroupsList",
                "General_Toolbar_ListItem_QueueAndLog", "General_Toolbar_ListItem_AddNewOffender", "General_Screen_Link_UsersList",
                "General_Toolbar_Tooltip_About","General_Screen_Link_OffenderDetails"
                 }).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

              AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

              AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

              AddBlock<Web_LogoutTestBlock>().

              //Invisible test2 =====================
             
              AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Toolbar_ListItem_AddNewUser", "General_Screen_Link_GroupParametes",
                "General_Screen_Link_UserAgencies","General_Screen_Link_UserConfiguration",
                "General_Screen_Link_UserOffenders",/*"General_Toolbar_Button_Logout",*/
                "General_Toolbar_ListItem_SystemConfiguration", "General_Toolbar_ListItem_ReceiverVersionsImport",
                "General_Toolbar_Tooltip_About","General_Toolbar_ListItem_QueueAndLog",
                }).

                SetProperty(x => x.ExcludeItems).WithValue(null).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                SetProperty(x => x.IsExactEqual).WithValue(true).

              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

              AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

              AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

              AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

              AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout block").

              //Invisible test3 =====================
              
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Screen_Link_UsersList"
                }).
                SetProperty(x => x.ExcludeItems).WithValue(null).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

             AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout block").

             //Invisible test4 =====================
            
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Toolbar_ListItem_User"
                }).
                SetProperty(x => x.ExcludeItems).WithValue(null).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                SetProperty(x => x.IsExactEqual).WithValue(true).

             AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

             AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout block").

             //Invisible test5 =====================
             
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Screen_Link_GroupsList",
                }).
                SetProperty(x => x.ExcludeItems).WithValue(null).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                SetProperty(x => x.IsExactEqual).WithValue(true).

             AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

             AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout block").

             //Invisible test6 =====================
             
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Toolbar_Button_Offenders", "General_Toolbar_Button_System", "General_Toolbar_Button_Reports",
                "General_Screen_Link_Monitor","General_Screen_Link_Dashboard"
                }).
                SetProperty(x => x.ExcludeItems).WithValue(null).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                SetProperty(x => x.IsExactEqual).WithValue(true).

             AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

             AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout block").

             //Details Visible test =====================
            
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Screen_Link_UserDetails","General_Screen_Link_GroupDetails","General_Screen_Link_OffenderDetails"
                }).
                 SetProperty(x => x.ExcludeItems).WithValue(null).
                 SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                 SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).
                 SetProperty(x => x.IsExactEqual).WithValue(true).

             AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                 SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                 SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                 SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                 SetDescription("Web security config assert block").

             AddBlock<Web_LogoutTestBlock>().
                 SetDescription("Logout block").

             //Details Invisible test
             
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                 SetProperty(x => x.Refine).WithValue(new string[] {
                 "General_Screen_Link_UserDetails","General_Screen_Link_GroupDetails","General_Screen_Link_OffenderDetails"
                 }).
                 SetProperty(x => x.ExcludeItems).WithValue(null).
                 SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                 SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                 SetProperty(x => x.IsExactEqual).WithValue(true).

             AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                 SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                 SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                 SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                 SetDescription("Web security config assert block").

            AddBlock<Web_LogoutTestBlock>().
                SetDescription("Logout block").

             //Invisible Logout test =====================
             
             AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                SetProperty(x => x.Refine).WithValue(new string[] {
                "General_Toolbar_Button_Logout"
                }).
                SetProperty(x => x.ExcludeItems).WithValue(null).
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                SetProperty(x => x.IsExactEqual).WithValue(true).

             AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

             AddBlock<Web_ClearCacheTestBlock>().
                SetDescription("Clear cache").

             AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                SetDescription("Security config test").

             AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                SetDescription("Web security config assert block").

         ExecuteFlow();
        }
    }
}
