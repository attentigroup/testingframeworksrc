﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using Common.Enum;
using WebTests.InfraStructure;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
#endregion


namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_OffenderConfigurationTests : WebBaseUnitTest
    {

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_SaveButton_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveConfigData" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorAnyOffenderTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveConfigData" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorAnyOffenderTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }


        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_SaveButton_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("yairrsa").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveConfigData" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorAnyOffenderTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_HandlingProcedures_SaveButton_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF, Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get offender test block").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveHandlingProcedure" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigHandlingProcedureTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveHandlingProcedure" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigHandlingProcedureTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_HandlingProcedures_SaveButton_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveHandlingProcedure" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigHandlingProcedureTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_TrackerRules_SaveButton()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveTrackerRules" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTrackingRulesTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveTrackerRules" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTrackingRulesTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_TrackerRules_SaveButton_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_Button_SaveTrackerRules" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTrackingRulesTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_1PieceGPSRF_Visibility()
        {
            TestingFlow.

               /* Visible Part Starts Here */

            #region Add Offender and Equipment

               AddBlock<VerifyIfEquipmenExistsTestBlock>().SetName("VerifyOnePiece").
                    SetProperty(x => x.EquipmentSN).WithValue("34334313").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("One_Piece_GPS_RF").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("VerifyOnePiece").
                    FromDeepProperty<VerifyIfEquipmenExistsTestBlock>(x => x.EquipmentSN).//WithValue("34334313").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).
                    //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    //SetProperty(x => x.VoicePhoneNumber).WithValue("1134311").
                    //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    //SetProperty(x => x.DataPhoneNumber).WithValue("1134311").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<VerifyIfEquipmenExistsTestBlock>().SetName("VerifyBeacon").
                    SetProperty(x => x.EquipmentSN).WithValue("34311").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgAddEquipmentRequest type Beacon").
                    SetName("Beacon").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("VerifyBeacon").
                    FromDeepProperty<VerifyIfEquipmenExistsTestBlock>(x => x.EquipmentSN).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.FirstName).WithValue("Yahoooo").
                    SetProperty(x => x.OfficerID).WithValue(33).
                    //SetProperty(x => x.RefID).WithValue("Yahoooo").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("One_Piece_GPS_RF").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("Beacon").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

            #endregion

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
               
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_", "OffenderConfig_Communication_", "OffenderConfig_LBS_" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_General_Label_TrackerSanitySecond",
                                                                              "OffenderConfig_General_Label_TimeSensitivity",
                                                                              "OffenderConfig_General_Label_SanityOccursEvery",
                                                                              "OffenderConfig_General_Label_SanityCycleStartsAt",
                                                                              "OffenderConfig_General_Label_Range",
                                                                              "OffenderConfig_General_Label_NumberOfRings",
                                                                              "OffenderConfig_General_Label_Kosher",
                                                                              "OffenderConfig_General_Button_SaveTrackerRules",
                                                                              "OffenderConfig_General_Button_SaveHandlingProcedure",
                                                                              "OffenderConfig_LBS_Label_EnableLBS",
                                                                              "OffenderConfig_LBS_Header_Location",
                                                                              "OffenderConfig_Communication_Header_DefaultPhoneforVoiceTest",
                    "OffenderConfig_General_Label_RingVolume",
                    "OffenderConfig_General_Label_TrackerSanityMinutes",
                    "OffenderConfig_General_Label_ShieldingProfileSensitivity"}).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_", "OffenderConfig_Communication_", "OffenderConfig_LBS_" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_General_Label_TrackerSanitySecond",
                                                                              "OffenderConfig_General_Label_TimeSensitivity",
                                                                              "OffenderConfig_General_Label_SanityOccursEvery",
                                                                              "OffenderConfig_General_Label_SanityCycleStartsAt",
                                                                              "OffenderConfig_General_Label_Range",
                                                                              "OffenderConfig_General_Label_NumberOfRings",
                                                                              "OffenderConfig_General_Label_Kosher",
                                                                              "OffenderConfig_General_Button_SaveTrackerRules",
                                                                              "OffenderConfig_General_Button_SaveHandlingProcedure",
                                                                              "OffenderConfig_LBS_Label_EnableLBS",
                                                                              "OffenderConfig_LBS_Header_Location",
                                                                              "OffenderConfig_Communication_Header_DefaultPhoneforVoiceTest",
                    "OffenderConfig_General_Label_RingVolume",
                    "OffenderConfig_General_Label_TrackerSanityMinutes",
                    "OffenderConfig_General_Label_ShieldingProfileSensitivity"}).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_1PieceGPSRF_Invisible()
        //{
        //    TestingFlow.

        //    #region Add Offender and Equipment

        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("One_Piece_GPS_RF").
        //            SetProperty(x => x.AgencyID).WithValue(11).
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("34334312").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").

        //        AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
        //            SetDescription("Create EntMsgAddCellularDataRequest Type").
        //            SetProperty(x => x.EquipmentID).
        //                WithValueFromBlockIndex("AddEquipmentTestBlock").
        //                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
        //            SetProperty(x => x.ProviderID).WithValue(2).
        //            SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
        //            SetProperty(x => x.VoicePhoneNumber).WithValue("1134311").
        //            SetProperty(x => x.DataPrefixPhone).WithValue("97254").
        //            SetProperty(x => x.DataPhoneNumber).WithValue("1134311").

        //        AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Cellular Data test block").

        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create EntMsgAddEquipmentRequest type Beacon").
        //            SetName("Beacon").
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("34311").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").

        //        AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetProperty(x => x.FirstName).WithValue("Yahoooo").
        //            SetProperty(x => x.OfficerID).WithValue(33).
        //            SetProperty(x => x.RefID).WithValue("Yahoooo").
        //            SetProperty(x => x.CityID).WithValue("TLV").
        //            SetProperty(x => x.Language).WithValue("ENG").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
        //            SetProperty(x => x.StateID).WithValue("IL").
        //            SetProperty(x => x.Receiver).
        //                WithValueFromBlockIndex("One_Piece_GPS_RF").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
        //            SetProperty(x => x.HomeUnit).
        //                WithValueFromBlockIndex("Beacon").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

        //       AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").

        //    #endregion

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("yairrsa").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_General_", "OffenderConfig_Communication_", "OffenderConfig_LBS_" }).
        //            SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_General_Label_TrackerSanitySecond",
        //                                                                      "OffenderConfig_General_Label_TimeSensitivity",
        //                                                                      "OffenderConfig_General_Label_SanityOccursEvery",
        //                                                                      "OffenderConfig_General_Label_SanityCycleStartsAt",
        //                                                                      "OffenderConfig_General_Label_Range",
        //                                                                      "OffenderConfig_General_Label_NumberOfRings",
        //                                                                      "OffenderConfig_General_Label_Kosher",
        //                                                                      "OffenderConfig_General_Button_SaveTrackerRules",
        //                                                                      "OffenderConfig_General_Button_SaveHandlingProcedure",
        //                                                                      "OffenderConfig_LBS_Label_EnableLBS",
        //                                                                      "OffenderConfig_LBS_Header_Location",
        //                                                                      "OffenderConfig_Communication_Header_DefaultPhoneforVoiceTest"}).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").
        //            SetProperty(x => x.SearchTerm).
        //                WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
        //                    FromPropertyName(EnumPropertyName.RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_2PieceGPS_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

            #region Add Offender and Equipment
                AddBlock<VerifyIfEquipmenExistsTestBlock>().
                    SetProperty(x => x.EquipmentSN).WithValue("35633389").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Two_Piece_3G").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("VerifyIfEquipmenExistsTestBlock").
                        FromDeepProperty<VerifyIfEquipmenExistsTestBlock>(x => x.EquipmentSN).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).
                    //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    //SetProperty(x => x.VoicePhoneNumber).WithValue("5634311").
                    //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    //SetProperty(x => x.DataPhoneNumber).WithValue("5634311").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("BaseUnit").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.GPS_Base_Unit).
                    //SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("50438889").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.FirstName).WithValue("Yahoooo2").
                    SetProperty(x => x.OfficerID).WithValue(33).
                    //SetProperty(x => x.RefID).WithValue("Yahoooo2").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). //1Piece GPS/RF 
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("Two_Piece_3G").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("BaseUnit").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

            #endregion

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_BaseUnit_",
                        "OffenderConfig_General_Label_Range", "OffenderConfig_General_Label_TimeSensitivity",
                        "OffenderConfig_LCD_Label_ShowLCDIcons", "OffenderConfig_LCD_Header_TrackerScreenMessage",
                        "OffenderConfig_LCD_Header_TrackerScreen", "OffenderConfig_LBS_Header_Location",
                    "OffenderConfig_General_Label_TrackerSanitySeconds",
                    "OffenderConfig_General_Label_ShieldingProfileSensitivity"}).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

               AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_BaseUnit_",
                        "OffenderConfig_General_Label_Range", "OffenderConfig_General_Label_TimeSensitivity",
                        "OffenderConfig_LCD_Label_ShowLCDIcons", "OffenderConfig_LCD_Header_TrackerScreenMessage",
                        "OffenderConfig_LCD_Header_TrackerScreen", "OffenderConfig_LBS_Header_Location",
                    "OffenderConfig_General_Label_TrackerSanitySeconds",
                    "OffenderConfig_General_Label_ShieldingProfileSensitivity"}).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }
       
        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_2PieceGPS_Invisible()
        //{
        //    TestingFlow.

        //    #region Add Offender and Equipment

        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("Two_Piece_3G").
        //            SetProperty(x => x.AgencyID).WithValue(11).
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("35634311").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").

        //        AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
        //            SetDescription("Create EntMsgAddCellularDataRequest Type").
        //            SetProperty(x => x.EquipmentID).
        //                WithValueFromBlockIndex("AddEquipmentTestBlock").
        //                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
        //            SetProperty(x => x.ProviderID).WithValue(2).
        //            SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
        //            SetProperty(x => x.VoicePhoneNumber).WithValue("5634311").
        //            SetProperty(x => x.DataPrefixPhone).WithValue("97254").
        //            SetProperty(x => x.DataPhoneNumber).WithValue("5634311").

        //        AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Cellular Data test block").

        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("BaseUnit").
        //            //SetProperty(x => x.AgencyID).WithValue(11).
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.GPS_Base_Unit).
        //            //SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("50455555").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").

        //        AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetProperty(x => x.FirstName).WithValue("Yahoooo2").
        //            SetProperty(x => x.OfficerID).WithValue(33).
        //            SetProperty(x => x.RefID).WithValue("Yahoooo2").
        //            SetProperty(x => x.CityID).WithValue("TLV").
        //            SetProperty(x => x.Language).WithValue("ENG").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). //1Piece GPS/RF 
        //            SetProperty(x => x.StateID).WithValue("IL").
        //            SetProperty(x => x.Receiver).
        //                WithValueFromBlockIndex("Two_Piece_3G").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
        //            SetProperty(x => x.HomeUnit).
        //                WithValueFromBlockIndex("BaseUnit").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

        //       AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").

        //    #endregion

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("yairrsa").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_BaseUnit_", "OffenderConfig_General_Label_Range", "OffenderConfig_General_Label_TimeSensitivity", "OffenderConfig_LCD_Label_ShowLCDIcons", "OffenderConfig_LCD_Header_TrackerScreenMessage", "OffenderConfig_LCD_Header_TrackerScreen", "OffenderConfig_LBS_Header_Location" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").
        //            SetProperty(x => x.SearchTerm).
        //                WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
        //                    FromPropertyName(EnumPropertyName.RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_DV_Visibility()
        {
            TestingFlow.

                 /* Visible Part Starts Here */

            #region add equipment for victim
                 AddBlock<VerifyIfEquipmenExistsTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("35631116").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("VictimEquipmentRequest").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("VerifyIfEquipmenExistsTestBlock").
                        FromDeepProperty<VerifyIfEquipmenExistsTestBlock>(x => x.EquipmentSN).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("VictimEquipmentResponse").
            #endregion

            #region add cellular data for victim
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("VictimEquipmentResponse").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).
                    //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    //SetProperty(x => x.VoicePhoneNumber).WithValue("6311113").
                    //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    //SetProperty(x => x.DataPhoneNumber).WithValue("6311113").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
            #endregion

            #region add offender victim
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetName("Victim").
                    SetProperty(x => x.OfficerID).WithValue(33).
                    //SetProperty(x => x.FirstName).WithValue("AutoVic_Sec").
                    //SetProperty(x => x.RefID).WithValue("AutoVic_Sec").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("VictimEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                    SetName("AddVictim").
            #endregion

            #region add equipment for aggressor
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("AggressorEquipmentRequest").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    //SetProperty(x => x.EquipmentSerialNumber).WithValue("35641115").
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AggressorEquipmentResponse").
            #endregion

            #region add cellular data for aggressor
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AggressorEquipmentResponse").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).
                    //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    //SetProperty(x => x.VoicePhoneNumber).WithValue("6241112").
                    //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    //SetProperty(x => x.DataPhoneNumber).WithValue("6241112").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
            #endregion

            #region add offender aggressor
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetName("Aggressor").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    //SetProperty(x => x.FirstName).WithValue("AutoAgg_Sec").
                    //SetProperty(x => x.RefID).WithValue("AutoAgg_Sec").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("AggressorEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.RelatedOffenderID).
                        WithValueFromBlockIndex("AddVictim").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                    SetName("AddAggressor").
            #endregion

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_DV_Header_DVParameters", "OffenderConfig_DV_Label_GPSProximity", "OffenderConfig_DV_Label_ProximityBuffer", "OffenderConfig_DV_Label_RFProximity" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("Victim").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                  /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_DV_Header_DVParameters", "OffenderConfig_DV_Label_GPSProximity", "OffenderConfig_DV_Label_ProximityBuffer", "OffenderConfig_DV_Label_RFProximity" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("Victim").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_DV_Invisible()
        //{
        //    TestingFlow.

        //    #region add equipment for victim
        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("VictimEquipmentRequest").
        //            SetProperty(x => x.AgencyID).WithValue(11).
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("35631115").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").
        //            SetName("VictimEquipmentResponse").
        //    #endregion

        //    #region add cellular data for victim
        //        AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
        //            SetDescription("Create EntMsgAddCellularDataRequest Type").
        //            SetProperty(x => x.EquipmentID).
        //                WithValueFromBlockIndex("VictimEquipmentResponse").
        //                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
        //            SetProperty(x => x.ProviderID).WithValue(2).
        //            SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
        //            SetProperty(x => x.VoicePhoneNumber).WithValue("6311113").
        //            SetProperty(x => x.DataPrefixPhone).WithValue("97254").
        //            SetProperty(x => x.DataPhoneNumber).WithValue("6311113").

        //        AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Cellular Data test block").
        //    #endregion

        //    #region add offender victim
        //        AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetName("Victim").
        //            SetProperty(x => x.OfficerID).WithValue(33).
        //            SetProperty(x => x.FirstName).WithValue("AutoVic_Sec").
        //            SetProperty(x => x.RefID).WithValue("AutoVic_Sec").
        //            SetProperty(x => x.CityID).WithValue("TLV").
        //            SetProperty(x => x.Language).WithValue("ENG").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
        //            SetProperty(x => x.StateID).WithValue("IL").
        //            SetProperty(x => x.Receiver).
        //                WithValueFromBlockIndex("VictimEquipmentRequest").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
        //            SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).

        //       AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").
        //            SetName("AddVictim").
        //    #endregion

        //    #region add equipment for aggressor
        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("AggressorEquipmentRequest").
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("35641141").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").
        //            SetName("AggressorEquipmentResponse").
        //    #endregion

        //    #region add cellular data for aggressor
        //        AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
        //            SetDescription("Create EntMsgAddCellularDataRequest Type").
        //            SetProperty(x => x.EquipmentID).
        //                WithValueFromBlockIndex("AggressorEquipmentResponse").
        //                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
        //            SetProperty(x => x.ProviderID).WithValue(2).
        //            SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
        //            SetProperty(x => x.VoicePhoneNumber).WithValue("6241112").
        //            SetProperty(x => x.DataPrefixPhone).WithValue("97254").
        //            SetProperty(x => x.DataPhoneNumber).WithValue("6241112").

        //        AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Cellular Data test block").
        //    #endregion

        //    #region add offender aggressor
        //       AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetName("Aggressor").
        //            SetProperty(x => x.FirstName).WithValue("AutoAgg_Sec").
        //            SetProperty(x => x.RefID).WithValue("AutoAgg_Sec").
        //            SetProperty(x => x.CityID).WithValue("TLV").
        //            SetProperty(x => x.Language).WithValue("ENG").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
        //            SetProperty(x => x.StateID).WithValue("IL").
        //            SetProperty(x => x.Receiver).
        //                WithValueFromBlockIndex("AggressorEquipmentRequest").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
        //            SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
        //            SetProperty(x => x.RelatedOffenderID).
        //                WithValueFromBlockIndex("AddVictim").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

        //       AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").
        //            SetName("AddAggressor").
        //    #endregion

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("yairrsa").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_DV_Header_DVParameters", "OffenderConfig_DV_Label_GPSProximity", "OffenderConfig_DV_Label_ProximityBuffer", "OffenderConfig_DV_Label_RFProximity" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").
        //            SetProperty(x => x.SearchTerm).
        //                WithValueFromBlockIndex("Victim").
        //                    FromPropertyName(EnumPropertyName.RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}

        [Ignore]
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_Voice_Visible()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_Schedule_", "OffenderConfig_Voice_" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_Schedule_Label_MinimumTimeBetweenTests" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).WithValue("voice").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_Voice_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("yuvalsa").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_Schedule_", "OffenderConfig_Voice_" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").
        //            SetProperty(x => x.SearchTerm).WithValue("voice").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_E4_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */
            #region Add Offender and Wquipment
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Receiver").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("444444").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("ReceiverResponse").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Transmitter").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Transmitter_860).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_860.ToString()). // WithValue("E44444").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("TransmitterResponse").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("ReceiverResponse").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).
                    //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    //SetProperty(x => x.VoicePhoneNumber).WithValue("4411111").
                    //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    //SetProperty(x => x.DataPhoneNumber).WithValue("4411111").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AgencyID).WithValue(11).
                    SetProperty(x => x.OfficerID).WithValue(33).
                    //SetProperty(x => x.FirstName).WithValue("AutoSec_E4").
                    //SetProperty(x => x.RefID).WithValue("AutoSec_E4").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("Receiver").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Transmitter).
                        WithValueFromBlockIndex("Transmitter").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.ContactPhoneNumber).WithValue(null).
                    SetProperty(x => x.ContactPrefixPhone).WithValue(null).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
            #endregion

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_LCD_", "OffenderConfig_Communication_","OffenderConfig_General_Label_RingVolume" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_Communication_Header_DefaultPhoneforVoiceTest", "OffenderConfig_LCD_Header_TrackerScreen", "OffenderConfig_LCD_Header_TrackerScreenMessage", "OffenderConfig_LCD_Label_ShowLCDIcons", "OffenderConfig_General_Label_SanityCycleStartsAt", "OffenderConfig_General_Label_SanityOccursEvery", "OffenderConfig_General_Label_NumberOfRings" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_LCD_", "OffenderConfig_Communication_", "OffenderConfig_General_Label_RingVolume" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_Communication_Header_DefaultPhoneforVoiceTest", "OffenderConfig_LCD_Header_TrackerScreen", "OffenderConfig_LCD_Header_TrackerScreenMessage", "OffenderConfig_LCD_Label_ShowLCDIcons", "OffenderConfig_General_Label_SanityCycleStartsAt", "OffenderConfig_General_Label_SanityOccursEvery", "OffenderConfig_General_Label_NumberOfRings" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.RefID).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_E4_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("Receiver").
        //            SetProperty(x => x.AgencyID).WithValue(11).
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.RF_Curfew_Dual_E4).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("444444").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").
        //            SetName("ReceiverResponse").

        //        AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("create random EntMsgAddEquipmentRequest type").
        //            SetName("Transmitter").
        //            SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
        //            SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
        //            SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Transmitter_860).
        //            SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
        //            SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
        //            SetProperty(x => x.EquipmentSerialNumber).WithValue("E44444").

        //        AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add equipment test block").
        //            SetName("TransmitterResponse").

        //        AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
        //            SetDescription("Create EntMsgAddCellularDataRequest Type").
        //            SetProperty(x => x.EquipmentID).
        //                WithValueFromBlockIndex("ReceiverResponse").
        //                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
        //            SetProperty(x => x.ProviderID).WithValue(2).
        //            SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
        //            SetProperty(x => x.VoicePhoneNumber).WithValue("4411111").
        //            SetProperty(x => x.DataPrefixPhone).WithValue("97254").
        //            SetProperty(x => x.DataPhoneNumber).WithValue("4411111").

        //        AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Cellular Data test block").

        //        AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetProperty(x => x.OfficerID).WithValue(33).
        //            SetProperty(x => x.FirstName).WithValue("AutoSec_E4").
        //            SetProperty(x => x.RefID).WithValue("AutoSec_E4").
        //            SetProperty(x => x.CityID).WithValue("TLV").
        //            SetProperty(x => x.Language).WithValue("ENG").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
        //            SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
        //            SetProperty(x => x.StateID).WithValue("IL").
        //            SetProperty(x => x.Receiver).
        //                WithValueFromBlockIndex("Receiver").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
        //            SetProperty(x => x.Transmitter).
        //                WithValueFromBlockIndex("Transmitter").
        //                    FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
        //            SetProperty(x => x.ContactPhoneNumber).WithValue(null).
        //            SetProperty(x => x.ContactPrefixPhone).WithValue(null).

        //       AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("yairrsa").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_LCD_", "OffenderConfig_Communication_" }).
        //            SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderConfig_Communication_Header_DefaultPhoneforVoiceTest", "OffenderConfig_LCD_Header_TrackerScreen", "OffenderConfig_LCD_Header_TrackerScreenMessage", "OffenderConfig_LCD_Label_ShowLCDIcons", "OffenderConfig_General_Label_SanityCycleStartsAt", "OffenderConfig_General_Label_SanityOccursEvery", "OffenderConfig_General_Label_NumberOfRings" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").
        //            SetProperty(x => x.SearchTerm).
        //                WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
        //                    FromPropertyName(EnumPropertyName.RefID).

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_ProgramParameters_Alcohol_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                    //SetProperty(x => x.UserName).WithValue("orenAD").
                    //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_Schedule_Label_MinimumTimeBetweenTests" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).WithValue("Alcohol").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").
                    
                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_Schedule_Label_MinimumTimeBetweenTests" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).WithValue("Alcohol").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_ProgramParameters_Alcohol_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //            SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_Schedule_Label_MinimumTimeBetweenTests" }).
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_OffenderConfigTabSelectorTestBlock>().
        //            SetDescription("Offenders List Screen Click On Filter Test Block").
        //            SetProperty(x => x.SearchTerm).WithValue("Alcohol").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //            SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
        //            SetDescription("Is Security Config Element Shown Assert Block").

        //        ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_HandlingProcedures_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                  
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_HandlingProcedures_Header_AutoNotification",
                       "OffenderConfig_HandlingProcedures_Header_InheritProcedures" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("ProgConfig").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("handlingProTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Logout Test Block").

            /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_HandlingProcedures_Header_AutoNotification",
                       "OffenderConfig_HandlingProcedures_Header_InheritProcedures" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("ProgConfig").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("handlingProTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_HandlingProcedures_Invisible()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //            SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //            SetProperty(x => x.Limit).WithValue(1).

        //            AddBlock<GetOffendersTestBlock>().

        //        AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Address Request").
        //            SetProperty(x => x.OffenderID).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

        //       AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
        //            SetDescription("Add Offender Contact test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_HandlingProcedures_Header_AutoNotification",
        //               "OffenderConfig_HandlingProcedures_Header_InheritProcedures" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndNavigateToTabTestBlock>().
        //            SetProperty(x => x.FirstSearchTerm).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
        //            SetProperty(x => x.PrimaryTabLocatorString).WithValue("ProgConfig").
        //            SetProperty(x => x.SecondaryTabLocatorString).WithValue("handlingProTab").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderConfiguration_HandlingProcedures_ReadOnly()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                    SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_HandlingProcedures_Label_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("ProgConfig").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("handlingProTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        //TODO: removed from testing list due tp complexity
        //[TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        //[TestMethod]
        //public void SecurityConfig_OffenderConfiguration_HandlingProcedures_Enabled()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //            SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
        //            SetProperty(x => x.Limit).WithValue(1).

        //        AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

        //        AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Address Request").
        //            SetProperty(x => x.OffenderID).
        //                WithValueFromBlockIndex("GetOffendersTestBlock").
        //                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

        //       AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
        //            SetDescription("Add Offender Contact test block").

        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("orenAD").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

        //        AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
        //           SetProperty(x => x.Refine).WithValue(new string[] { "OffenderConfig_HandlingProcedures_Label_" }).
        //           SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
        //           SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Login page").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //            SetDescription("Clear cache").

        //        AddBlock<Web_SelectOffenderAndNavigateToTabTestBlock>().
        //            SetProperty(x => x.FirstSearchTerm).
        //            WithValueFromBlockIndex("GetOffendersTestBlock").
        //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
        //            SetProperty(x => x.PrimaryTabLocatorString).WithValue("ProgConfig").
        //            SetProperty(x => x.SecondaryTabLocatorString).WithValue("handlingProTab").

        //        AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
        //          SetDescription("Security config test").

        //        AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

        //    ExecuteFlow();
        //}


    }
}
