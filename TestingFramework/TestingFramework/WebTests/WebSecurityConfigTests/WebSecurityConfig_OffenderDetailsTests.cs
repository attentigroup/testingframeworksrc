﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestBlocksLib.EquipmentBlocks;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.Information;
using TestBlocksLib.InformationBlock;
using WebTests.InfraStructure;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;

#endregion



namespace TestingFramework.UnitTests.WebTests.WebSecurityConfigTests
{
    [TestClass]
    public class WebSecurityConfig_OffenderDetailsTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_AdditionalData_AddEditDeleteContact_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_AdditionalData_Button_AddContact",
                       "OffenderDetails_AdditionalData_Button_EditContact", "OffenderDetails_AdditionalData_Button_DeleteContact"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCursorToOffenderContactRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_AdditionalData_Button_AddContact",
                       "OffenderDetails_AdditionalData_Button_EditContact", "OffenderDetails_AdditionalData_Button_DeleteContact"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCursorToOffenderContactRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_AdditionalData_OffenderContactsAndPriorityStatus_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_AdditionalData_Section_", "OffenderDetails_AdditionalData_Label_PriorityStatus" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).WithValue("1 Piece GPS").

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_AdditionalData_Section_", "OffenderDetails_AdditionalData_Label_PriorityStatus" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).WithValue("1 Piece GPS").

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_FilesList_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_FilesList_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                 //Invisible test
                 AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_FilesList_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_General_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_General_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_General_Button_SaveAdditionalData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_General_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_General_Button_SaveAdditionalData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_General_SaveAdditionalData_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_General_Button_SaveAdditionalData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                    AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_General_Button_SaveAdditionalData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().

                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").


                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_EditCellularData_Visiblity()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Button_EditCellularNumbers" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndDesplayVoiceSimTooltipTestBlock>().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").


                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Button_EditCellularNumbers" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndDesplayVoiceSimTooltipTestBlock>().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").


                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_SaveCellularData_Visibility()
        {
            TestingFlow.

                AddBlock<GetOffenderWithVoiceSimDataTestBlock>().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Button_SaveCellularData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenCellularDataPopupTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffenderWithVoiceSimDataTestBlock").
                    FromDeepProperty<GetOffenderWithVoiceSimDataTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_RefreshPageBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Button_SaveCellularData" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndOpenCellularDataPopupTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffenderWithVoiceSimDataTestBlock").
                    FromDeepProperty<GetOffenderWithVoiceSimDataTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ContactInformation_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInformation_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ContactInformation_Button_AddPhone",
                       "OffenderDetails_ContactInformation_Button_DeletePhone", "OffenderDetails_ContactInformation_Button_UpdateAddress",
                   "OffenderDetails_ContactInformation_Button_DeleteAddress", "OffenderDetails_ContactInformation_Header_EditDeleteAddress"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInformation_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ContactInformation_Button_AddPhone",
                       "OffenderDetails_ContactInformation_Button_DeletePhone"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ContactInformation_EditAndDeleteAddress_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInformation_Button_DeleteAddress",
                       "OffenderDetails_ContactInformation_Header_EditDeleteAddress", "OffenderDetails_ContactInformation_Button_UpdateAddress" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCursorToContactInformationRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInformation_Button_DeleteAddress",
                       "OffenderDetails_ContactInformation_Header_EditDeleteAddress", "OffenderDetails_ContactInformation_Button_UpdateAddress" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCursorToContactInformationRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ContactInformation_AddDeletePhone_Visibility()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                SetProperty(x => x.Limit).WithValue(10).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.Phones).WithValue(new Offenders0.EntPhone[2]).
                    SetProperty(x => x.Phones[0]).WithValue(new Offenders0.EntPhone() { PhoneNumber = "123" }).
                    SetProperty(x => x.Phones[1]).WithValue(new Offenders0.EntPhone() { PhoneNumber = "456" }).

                AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInformation_Button_AddPhone",
                   "OffenderDetails_ContactInformation_Button_DeletePhone"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndPressEditAddressTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_RefreshPageBlock>().

                AddBlock<Web_LogoutTestBlock>().

                 //Invisible test
                 AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                //SetProperty(x => x.UserName).WithValue("orenAD").
                //SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                //SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInformation_Button_AddPhone",
                   "OffenderDetails_ContactInformation_Button_DeletePhone"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndPressEditAddressTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CaseManagment_AbsenceRequest_Visible()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddAuthorizedAbsenceDataRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.Code).WithValue("AAA").
                SetProperty(x => x.Description).WithValue("automation").
                SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddAuthorizedAbsenceDataTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CaseManagment_Button_AddAbsenceRequest",
                       "OffenderDetails_CaseManagment_Button_DeleteAbsenceRequest", "OffenderDetails_CaseManagment_Button_UpdateAbsenceRequest" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCuesorToCaseManagmentRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("caseManagmentLink").
                    SetProperty(x => x.FilterName).WithValue("absenceRequests").

                //AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                //  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CaseManagment_Warning_Visibility()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderWarningRequestBlock>().
                SetProperty(x => x.Description).WithValue("automation").
                SetProperty(x => x.OffenderID).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddOffenderWarningTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CaseManagment_Button_AddWarning",
                       "OffenderDetails_CaseManagment_Button_DeleteWarning"}).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_CaseManagment_ListItem_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCuesorToCaseManagmentRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("caseManagmentLink").
                    SetProperty(x => x.FilterName).WithValue("warnings").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CaseManagment_Button_AddWarning",
                       "OffenderDetails_CaseManagment_Button_DeleteWarning"}).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_CaseManagment_ListItem_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCuesorToCaseManagmentRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("caseManagmentLink").
                    SetProperty(x => x.FilterName).WithValue("warnings").


               AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CaseManagment_ScheduleChange_Visible()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddScheduleChangeRequestBlock>().
                SetProperty(x => x.Code).WithValue("AAA").
                SetProperty(x => x.Description).WithValue("automation").
                SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<AddScheduleChangeTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CaseManagment_Button_AddSceduleChangeReason",
                                                                       "OffenderDetails_CaseManagment_Button_DeleteScheduleChangeReason",
                                                                       "OffenderDetails_CaseManagment_Button_UpdateScheduleChangeReason" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndMoveCuesorToCaseManagmentRowTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("caseManagmentLink").
                    SetProperty(x => x.FilterName).WithValue("scheduleChangeReasons").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CaseManagment_ListItems_Visibility()
        {
            TestingFlow.

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CaseManagment_ListItem_" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("caseManagmentLink").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Security config test").

                /* Invisible Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CaseManagment_ListItem_" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("caseManagmentLink").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CustomFields_Visibility()
        {
            TestingFlow.
                 AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                /* Visible Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1", "OffenderDetails_CustomFields_Label_ListItems1",
                   "OffenderDetails_CustomFields_Label_Numeric1", "OffenderDetails_CustomFields_Label_Text100Characters1", "OffenderDetails_CustomFields_Label_Text12Characters1",
                   "OffenderDetails_CustomFields_Label_Text255Characters1", "OffenderDetails_CustomFields_Label_Text255HyperLink1",
                   "OffenderDetails_CustomFields_Label_Text25Characters1", "OffenderDetails_CustomFields_Label_Text50Characters1"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                   SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                   SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                 //Invisible test
                 AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1", "OffenderDetails_CustomFields_Label_ListItems1",
                   "OffenderDetails_CustomFields_Label_Numeric1", "OffenderDetails_CustomFields_Label_Text100Characters1", "OffenderDetails_CustomFields_Label_Text12Characters1",
                   "OffenderDetails_CustomFields_Label_Text255Characters1", "OffenderDetails_CustomFields_Label_Text255HyperLink1",
                   "OffenderDetails_CustomFields_Label_Text25Characters1", "OffenderDetails_CustomFields_Label_Text50Characters1"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                   SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                   SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                   SetDescription("Security config test").

/* Invisible Part Starts Here */
AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1", "OffenderDetails_CustomFields_Label_ListItems1",
                   "OffenderDetails_CustomFields_Label_Numeric1", "OffenderDetails_CustomFields_Label_Text100Characters1", "OffenderDetails_CustomFields_Label_Text12Characters1",
                   "OffenderDetails_CustomFields_Label_Text255Characters1", "OffenderDetails_CustomFields_Label_Text255HyperLink1",
                   "OffenderDetails_CustomFields_Label_Text25Characters1", "OffenderDetails_CustomFields_Label_Text50Characters1"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

 AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                   SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                   SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                   SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_Identification_Enabled_Readonly()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive, Offenders0.EnmProgramStatus.PostActive }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_Identification_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_Identification_Label_ID", "OffenderDetails_Identification_Label_DateCreated",
                   "OffenderDetails_Identification_Label_ProgramStart"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Readonly test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_Identification_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

       

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_Identification_HideData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_Identification_" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_DeleteCellularData_Visibility()
        {
            TestingFlow.

                 AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
               
                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(Equipment0.EnmProgramType.One_Piece_RF).

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Button_DeleteCellularData" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEquipmentAndOpenVoiceSimDataPopup>().UsePreviousBlockData().
                    SetProperty(x => x.equipmentSN).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.EquipmentSN).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                AddBlock<Web_RefreshPageBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Invisible test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Button_DeleteCellularData" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectEquipmentAndOpenVoiceSimDataPopup>().
                    SetProperty(x => x.equipmentSN).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().
                    SetDescription("Is Security Config Element Shown Assert Block").

                ExecuteFlow();
        }

        

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ContactInfoScreen_ReadOnlyEnabled()
        {
            TestingFlow.

                /* Read-Only Part Starts Here */

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_Only_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInfoScreen_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ContactInfoScreen_Label_Country", "OffenderDetails_ContactInfoScreen_Label_TimeZone", "OffenderDetails_ContactInfoScreen_Label_Title" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndPressEditAddressTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_RefreshPageBlock>().
                    SetDescription("Web Logout Test Block").

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Web Logout Test Block").

                /* Enable Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                    SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInfoScreen_" }).
                    SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ContactInfoScreen_Label_PhoneName",
                        "OffenderDetails_ContactInfoScreen_Label_TimeZone", "OffenderDetails_ContactInfoScreen_Label_Title",
                         "OffenderDetails_ContactInfoScreen_Label_Country" }).
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndPressEditAddressTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ContactInfoScreen_HideData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ContactInfoScreen_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ContactInfoScreen_Label_Country" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderAndPressEditAddressTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_Label_DV_ReadOnly_Enabled()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_CurfewUnit",
                       "OffenderDetails_ProgramDetails_Label_Landline", "OffenderDetails_ProgramDetails_Label_OutsideLine",
                       "OffenderDetails_ProgramDetails_Label_VoiceSIM", "OffenderDetails_ProgramDetails_Button_DeleteCellularData",
                       "OffenderDetails_ProgramDetails_Button_SaveCellularData", "OffenderDetails_ProgramDetails_Button_EditCellularNumbers",
                   "OffenderDetails_ProgramDetails_Label_OffenderID"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                 //Enabled test
                 AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_CurfewUnit",
                   "OffenderDetails_ProgramDetails_Label_Landline", "OffenderDetails_ProgramDetails_Label_OutsideLine",
                   "OffenderDetails_ProgramDetails_Label_VoiceSIM", "OffenderDetails_ProgramDetails_Button_DeleteCellularData",
                   "OffenderDetails_ProgramDetails_Button_SaveCellularData", "OffenderDetails_ProgramDetails_Button_EditCellularNumbers",
                   "OffenderDetails_ProgramDetails_Label_ProgramType", "OffenderDetails_ProgramDetails_Label_Receiver",
                   "OffenderDetails_ProgramDetails_Label_SubType", "OffenderDetails_ProgramDetails_Label_OffenderID"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                 AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_Label_DV_HideData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_" }).
                   SetProperty(x => x.ExcludeItems).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_CurfewUnit",
                       "OffenderDetails_ProgramDetails_Label_Landline", "OffenderDetails_ProgramDetails_Label_OutsideLine",
                       "OffenderDetails_ProgramDetails_Label_VoiceSIM", "OffenderDetails_ProgramDetails_Button_DeleteCellularData",
                       "OffenderDetails_ProgramDetails_Button_SaveCellularData", "OffenderDetails_ProgramDetails_Button_EditCellularNumbers",
                       "OffenderDetails_ProgramDetails_Label_OffenderID"}).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                ExecuteFlow();
        }

        

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_Label_E4_ReadOnly_Enabled()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<GetOffenderWithLandlineDataTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_Landline", "OffenderDetails_ProgramDetails_Label_OutsideLine" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderWithLandlineDataTestBlock").
                            FromDeepProperty<GetOffenderWithLandlineDataTestBlock>(x => x.Offender.RefID).

                AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Enabled test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_Landline", "OffenderDetails_ProgramDetails_Label_OutsideLine" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderWithLandlineDataTestBlock").
                            FromDeepProperty<GetOffenderWithLandlineDataTestBlock>(x => x.Offender.RefID).

                AddBlock<Web_NavigateToTabTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_Label_E4_HideData()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Cellular, Offenders0.EnmProgramType.E4_Dual, Offenders0.EnmProgramType.E4_Landline }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_Landline", "OffenderDetails_ProgramDetails_Label_OutsideLine" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_Label_1PieceGPS_RF_ReadOnlyEnabled()
        {
            TestingFlow.

                /* Read-Only Part Starts Here */

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_CurfewUnit" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).WithValue("1 Piece GPS/RF").

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                 AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                    SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Web Logout Test Block").

                /* Enable Part Starts Here */

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_CurfewUnit" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).WithValue("1 Piece GPS/RF").

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_ProgramDetails_Label_1PieceGPS_RF_HideData()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_ProgramDetails_Label_CurfewUnit" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                 AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_AdditionalData_AddEditDeleteFile_Visibility()
        {
            TestingFlow.

                 AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                //add file by api
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderRefIDQueryParameter).WithValue(new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = "34688811" }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                    AddBlock<CreateAdditionalDataAddFileRequestBlock>().UsePreviousBlockData().
                    SetDescription("Add File to offender").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.OwnerEntity).WithValue(1).
                            SetProperty(x => x.FileName).WithValue("AddOffenderPicture.jpg").
                            SetProperty(x => x.FileDisplayName).WithValue("AddOffenderPicture.jpg").
                            SetProperty(x => x.FilePath).WithValue(TestingFramework.UnitTests.OffendersSvcTests.OffendersSvcTests.PicPath).

             AddBlock<AddFileTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender File test block").

            AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
               SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_AdditionalData_Button_AddFile",
                   "OffenderDetails_AdditionalData_Button_EditFile", "OffenderDetails_AdditionalData_Button_DeleteFile" }).
               SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
               SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

            AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_SelectOffenderAndMoveCursorToOffenderDocumentRowTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.OffenderRefId).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            AddBlock<Web_LogoutTestBlock>().

            //Invisible test
            AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
               SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_AdditionalData_Button_AddFile",
                   "OffenderDetails_AdditionalData_Button_EditFile", "OffenderDetails_AdditionalData_Button_DeleteFile" }).
               SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
               SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

            AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login page").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_SelectOffenderAndMoveCursorToOffenderDocumentRowTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.OffenderRefId).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                SetProperty(x => x.SecondaryTabLocatorString).WithValue("additionalDataTab").

            AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

       

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CustomFields_ReadOnly_Enabled()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.One_Piece_Only_RF, Offenders0.EnmProgramType.One_Piece }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] {"OffenderDetails_CustomFields_Label_ListItems1",
                   "OffenderDetails_CustomFields_Label_Numeric1", "OffenderDetails_CustomFields_Label_Text100Characters1", "OffenderDetails_CustomFields_Label_Text12Characters1",
                   "OffenderDetails_CustomFields_Label_Text255Characters1", "OffenderDetails_CustomFields_Label_Text25Characters1",
                       "OffenderDetails_CustomFields_Label_Text50Characters1" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Enabled test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] {"OffenderDetails_CustomFields_Label_ListItems1",
                   "OffenderDetails_CustomFields_Label_Numeric1", "OffenderDetails_CustomFields_Label_Text100Characters1", "OffenderDetails_CustomFields_Label_Text12Characters1",
                   "OffenderDetails_CustomFields_Label_Text255Characters1", "OffenderDetails_CustomFields_Label_Text25Characters1",
                       "OffenderDetails_CustomFields_Label_Text50Characters1" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CustomFields_DateTime_ReadOnly_Enabled()
        {
            TestingFlow.

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").
                
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Readonly).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

                AddBlock<Web_LogoutTestBlock>().

                //Enabled test
                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CustomFields_DateTime_HideData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).
                SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).
                   SetProperty(x => x.IsExactEqual).WithValue(false).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_SecurityConfiguration)]
        [TestMethod]
        public void SecurityConfig_OffenderDetails_CustomFields_HideData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                Offenders0.EnmProgramStatus.PreActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RefIdStringMinLength).WithValue(3).

                AddBlock<GetGuiSecurityDataTestBlock>().
                    SetDescription("Get GUI security data of user").

                AddBlock<SetGuiSecurityDataForVisibilityTest>().UsePreviousBlockData().
                   SetProperty(x => x.Refine).WithValue(new string[] { "OffenderDetails_CustomFields_Label_DateTime1", "OffenderDetails_CustomFields_Label_ListItems1",
                   "OffenderDetails_CustomFields_Label_Numeric1", "OffenderDetails_CustomFields_Label_Text100Characters1", "OffenderDetails_CustomFields_Label_Text12Characters1",
                   "OffenderDetails_CustomFields_Label_Text255Characters1", "OffenderDetails_CustomFields_Label_Text25Characters1",
                       "OffenderDetails_CustomFields_Label_Text50Characters1" }).
                   SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.EnableDisableHideOnly).
                   SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Hidden).
                   SetProperty(x => x.IsExactEqual).WithValue(true).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_SelectOffenderTestBlock>().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<Web_NavigateToTabTestBlock>().
                    SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").
                    SetProperty(x => x.SecondaryTabLocatorString).WithValue("offenderDetailsTab").

                AddBlock<Web_SecurityConfigFastTestBlock>().UsePreviousBlockData().
                  SetDescription("Security config test").

                AddBlock<Web_IsSecurityConfigElementShownAssertBlock>().

            ExecuteFlow();
        }

    }
}
