﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.EquipmentBlocks;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.EquipmentBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.AssertBlocksLib.WebTestBlock;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.DevicesBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Enum;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using LogicBlocksLib.ScheduleBlocks;
using TestBlocksLib.ScheduleBlocks;
using AssertBlocksLib.ScheduleAsserts;
using TestBlocksLib.AgencyBlocks;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using DataGenerators.Equipment;
using TestBlocksLib.OfficerBlocks;
using WebTests.InfraStructure;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
#endregion


namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    [DeploymentItem("Resources", "Resources")]  
    public class ImportEquipmentAddOffenderActivateTests : WebBaseUnitTest
    {

        Offenders0.EnmProgramStatus[] status = { Offenders0.EnmProgramStatus.Active };
        Offenders0.EnmProgramType[] oneP_program = { Offenders0.EnmProgramType.One_Piece };
        Offenders0.EnmProgramType[] twoP_program = { Offenders0.EnmProgramType.Two_Piece };

        #region Import equipment and allocate to offender

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddEquipment_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddEquipmentTestBlock>().
                    SetProperty(x => x.FileName).WithValue("1Piece Gen3.9 Not Encrypted P0.txt").

                    SetProperty(x => x.EquipmentSN).WithValue("34322210").

                 AddBlock<Web_GetEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddOffender_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                    AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                    AddBlock<GetAgencyTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                    AddBlock<GetOfficerTestBlock>().
                     SetProperty(x => x.AgencyID).
                     WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").


                AddBlock<Web_AddNewOffenderExtendedTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).WithValue("1 Piece GPS/RF").
                SetProperty(x => x.AgencyName).
                WithValueFromBlockIndex("GetAgencyTestBlock").
                FromDeepProperty<GetAgencyTestBlock>(x => x.GetAgenciesResponse.AgenciesList[0].Name).
                SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                AddBlock<Web_GetOffenderTestBlock>().
                SetProperty(x => x.refId).
                    WithValueFromBlockIndex("Web_AddNewOffenderExtendedTestBlock").
                    FromDeepProperty<Web_AddNewOffenderExtendedTestBlock>(x => x.OffenderDetails.OffenderRefId).

                AddBlock<Web_AddNewOffenderAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddEquipment_E4()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddSeveralEquipmentTestBlock>().
                    SetProperty(x => x.FileName).WithValue("RCVR Import E4 P6 New_Format.txt").
                    SetProperty(x => x.receiver).WithValue(new string[] { "198520" }).
                    //SetProperty(x => x.receiver[0]).WithValue("198520").
                    SetProperty(x => x.Transmitter).WithValue(new string[1]).
                    SetProperty(x => x.Transmitter[0]).WithValue("A198520").

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("198520").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("A198520").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddOffender_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Receiver").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.SupportEncryption).WithValue(true).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("100111").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("ReceiverResponse").

                AddBlock<GetAgencyTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                    AddBlock<GetOfficerTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Transmitter").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Transmitter_860).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.SupportEncryption).WithValue(true).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("A100111").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("TransmitterResponse").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddNewOffenderExtendedTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).WithValue("RF Curfew Dual (E4)").
                SetProperty(x => x.Receiver).WithValue("100111").
                SetProperty(x => x.Transmitter).WithValue("A100111").
                SetProperty(x => x.AgencyName).
                WithValueFromBlockIndex("GetAgencyTestBlock").
                FromDeepProperty<GetAgencyTestBlock>(x => x.GetAgenciesResponse.AgenciesList[0].Name).
                SetProperty(x => x.OfficerName).
                 WithValueFromBlockIndex("GetOfficerTestBlock").
                FromDeepProperty<GetOfficerTestBlock>(x => x.OfficerName).

                AddBlock<Web_GetOffenderTestBlock>().
                SetProperty(x => x.refId).
                    WithValueFromBlockIndex("Web_AddNewOffenderExtendedTestBlock").
                    FromDeepProperty<Web_AddNewOffenderExtendedTestBlock>(x => x.OffenderDetails.OffenderRefId).

                AddBlock<Web_AddNewOffenderAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddEquipment_2Piece_GPS()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddSeveralEquipmentTestBlock>().
                    SetProperty(x => x.FileName).WithValue("2Piece V6 Not Encypted_35602111.txt").
                    SetProperty(x => x.receiver).WithValue(new string[1]).
                    SetProperty(x => x.receiver[0]).WithValue("35602111").
                    SetProperty(x => x.Transmitter).WithValue(new string[1]).
                    SetProperty(x => x.Transmitter[0]).WithValue("A41111").

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("35602111").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("A41111").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddOffender_2Piece_GPS()
        {
            TestingFlow.

                  AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                 SetName("AddEquipmentTestBlock_2Piece").
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_2Piece").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddCellularDataRequest Type").
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Cellular Data test block").

                 AddBlock<GetAgencyTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                 AddBlock<GetOfficerTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                    AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper.ToString()).

              AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
               SetName("AddEquipmentTestBlock_Transsmiter").
                    SetDescription("Add equipment test block").
                SetProperty(x => x.AddEquipmentRequest).
                WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                FromDeepProperty<CreateEntMsgAddEquipmentRequestBlock>(x => x.AddEquipmentRequest).

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_Transsmiter").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_Transsmiter").


                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddNewOffenderExtendedTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).WithValue("2 Piece GPS").
                SetProperty(x => x.Receiver).WithValueFromBlockIndex("GetEquipmentListTestBlock_2Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.Transmitter).WithValueFromBlockIndex("GetEquipmentListTestBlock_Transsmiter").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyName).
                 WithValueFromBlockIndex("GetAgencyTestBlock").
                FromDeepProperty<GetAgencyTestBlock>(x => x.GetAgenciesResponse.AgenciesList[0].Name).
                SetProperty(x => x.OfficerName).
                 WithValueFromBlockIndex("GetOfficerTestBlock").
                FromDeepProperty<GetOfficerTestBlock>(x => x.OfficerName).

                AddBlock<Web_GetOffenderTestBlock>().
                SetProperty(x => x.refId).
                    WithValueFromBlockIndex("Web_AddNewOffenderExtendedTestBlock").
                    FromDeepProperty<Web_AddNewOffenderExtendedTestBlock>(x => x.OffenderDetails.OffenderRefId).

                AddBlock<Web_AddNewOffenderAssertBlock>().UsePreviousBlockData().

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddEquipment_DV()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddSeveralEquipmentTestBlock>().
                    SetProperty(x => x.FileName).WithValue("2Piece V6 Not Encypted_DV.txt").
                    SetProperty(x => x.receiver).WithValue(new string[2]).
                    SetProperty(x => x.receiver[0]).WithValue("35602222").
                    SetProperty(x => x.receiver[1]).WithValue("35602224").
                    SetProperty(x => x.Transmitter).WithValue(new string[1]).
                    SetProperty(x => x.Transmitter[0]).WithValue("A41113").

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("35602222").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("35602224").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

                AddBlock<Web_GetEquipmentTestBlock>().
                SetProperty(x => x.EquipmentSN).WithValue("A41113").

                AddBlock<Web_AddEquipmentAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddOffender_DV()
        {
            TestingFlow.

            #region add equipment for victim
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                 SetName("AddEquipmentTestBlock_2Piece").
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_2Piece").

            #endregion

            #region add cellular data for victim


                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddCellularDataRequest Type").
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Cellular Data test block").

            #endregion

            #region Select Agency
                 AddBlock<GetAgencyTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                 AddBlock<GetOfficerTestBlock>().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                    FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

            #endregion

            #region add equipment for aggressor

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_aggressor").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
            #endregion

            #region add cellular data for aggressor

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentTestBlock_aggressor").
            #endregion

            #region add equipment -TX for aggressor 

            AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("create random EntMsgAddEquipmentRequest type").
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("AddEquipmentTestBlock").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                SetProperty(x => x.EquipmentSerialNumber).
                    WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).


            AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetDescription("Add equipment test block").
                SetName("AddEquipmentTestBlock_TRX_840").

            AddBlock<CreateEntMsgGetEquipmentListRequest>().
            SetName("CreateEntMsgGetEquipmentListRequest_TRX_840").
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_TRX_840").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
            SetName("GetEquipmentListTestBlock_TRX_840").
            SetProperty(x => x.GetEquipmentListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TRX_840").
                        FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).
            #endregion



                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddNewOffenderExtendedTestBlock>().UsePreviousBlockData().
                SetName("Web_AddNewVictimOffenderExtendedTestBlock").
                SetProperty(x => x.ProgramType).WithValue("2 Piece GPS").
                SetProperty(x => x.SubType).WithValue("DV Victim").
                SetProperty(x => x.Receiver).WithValueFromBlockIndex("GetEquipmentListTestBlock_2Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyName).
                 WithValueFromBlockIndex("GetAgencyTestBlock").
                FromDeepProperty<GetAgencyTestBlock>(x => x.GetAgenciesResponse.AgenciesList[0].Name).
                SetProperty(x => x.OfficerName).
                 WithValueFromBlockIndex("GetOfficerTestBlock").
                FromDeepProperty<GetOfficerTestBlock>(x => x.OfficerName).

                AddBlock<Web_GetOffenderTestBlock>().UsePreviousBlockData().
                SetName("Web_GetVictimOffenderTestBlock").
                SetProperty(x => x.refId).
                    WithValueFromBlockIndex("Web_AddNewVictimOffenderExtendedTestBlock").
                    FromDeepProperty<Web_AddNewOffenderExtendedTestBlock>(x => x.OffenderDetails.OffenderRefId).

                    AddBlock<Web_AddNewOffenderAssertBlock>().

                AddBlock<Web_AddNewOffenderExtendedTestBlock>().UsePreviousBlockData().
                SetName("Web_AddNewOffenderTestBlock_Aggressor").
                SetProperty(x => x.ProgramType).WithValue("2 Piece GPS").
                SetProperty(x => x.SubType).WithValue("DV Aggressor").
                SetProperty(x => x.AgencyName).
                       WithValueFromBlockIndex("GetAgencyTestBlock").
                           FromDeepProperty<GetAgencyTestBlock>(x => x.GetAgenciesResponse.AgenciesList[0].Name).
                SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                           FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.Transmitter).WithValueFromBlockIndex("GetEquipmentListTestBlock_TRX_840").
                                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                SetProperty(x => x.RelatedOffenderDetails).
                    WithValueFromBlockIndex("Web_AddNewVictimOffenderExtendedTestBlock").
                        FromDeepProperty<Web_AddNewOffenderExtendedTestBlock>(x => x.OffenderDetails).

                AddBlock<Web_GetOffenderTestBlock>().
                SetProperty(x => x.refId).
                    WithValueFromBlockIndex("Web_AddNewOffenderTestBlock_Aggressor").
                    FromDeepProperty<Web_AddNewOffenderExtendedTestBlock>(x => x.OffenderDetails.OffenderRefId).

             AddBlock<Web_AddNewOffenderAssertBlock>().

            ExecuteFlow();
        }

        #endregion

        #region Add schedule

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddScheduleToOffender_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                       AddBlock<Web_AddScheduleTestBlock>().
                        SetProperty(x => x.SearchTerm).
                             WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                                  FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).AddMinutes(1)).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

                 AddBlock<AddTimeFrameExistAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddScheduleToOffender_2Piece_GPS()
        {
            TestingFlow.

                   AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                 SetName("AddEquipmentTestBlock_2Piece").
                    SetDescription("Add equipment test block").

                    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddCellularDataRequest Type").
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Cellular Data test block").

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                     SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_2Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper.ToString()).

              AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
               SetName("AddEquipmentTestBlock_Transsmiter").
                    SetDescription("Add equipment test block").
                SetProperty(x => x.AddEquipmentRequest).
                WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                FromDeepProperty<CreateEntMsgAddEquipmentRequestBlock>(x => x.AddEquipmentRequest).

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_Transsmiter").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_Transsmiter").

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_2Piece").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_Transsmiter").
                         FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

            AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                       AddBlock<Web_AddScheduleTestBlock>().
                        SetProperty(x => x.SearchTerm).
                             WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                                  FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).AddMinutes(1)).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

                 AddBlock<AddTimeFrameExistAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddScheduleToOffender_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Receiver").
                   
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.SupportEncryption).WithValue(true).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("100211").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("ReceiverResponse").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Transmitter").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Transmitter_860).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.SupportEncryption).WithValue(true).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E10021").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("TransmitterResponse").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.Receiver).WithValue("100211").
                    SetProperty(x => x.Transmitter).WithValue("E10021").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                     AddBlock<Web_AddScheduleTestBlock>().UsePreviousBlockData().
                        SetProperty(x => x.SearchTerm).
                             WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                                  FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).
                    SetProperty(x => x.Type).WithValue("Must Be Out").
                    SetProperty(x => x.Location).WithValue("Undefined").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).AddMinutes(1)).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

                 AddBlock<AddTimeFrameExistAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddScheduleToOffender_DV()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_victim").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_victim").

                ///aggressor

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_aggressor").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentTestBlock_aggressor").

            //////aggressor TX

            AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("create random EntMsgAddEquipmentRequest type").
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                SetProperty(x => x.EquipmentSerialNumber).
                    WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).

            AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetDescription("Add equipment test block").
                SetName("AddEquipmentTestBlock_TRX_840").

            AddBlock<CreateEntMsgGetEquipmentListRequest>().
            SetName("CreateEntMsgGetEquipmentListRequest_TRX_840").
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_TRX_840").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
            SetName("GetEquipmentListTestBlock_TRX_840").
            SetProperty(x => x.GetEquipmentListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TRX_840").
                        FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

            /// add victim offender 

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetVictimOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").

            /// add aggressor offender 
            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_TRX_840").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                         SetProperty(x => x.RelatedOffenderID).
                    WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
               SetName("AddAggressorOffendertestblock").
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("GetAggressorOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                       AddBlock<Web_AddScheduleTestBlock>().
                        SetProperty(x => x.SearchTerm).
                             WithValueFromBlockIndex("GetAggressorOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                         WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).AddMinutes(1)).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

                 AddBlock<AddTimeFrameExistAssertBlock>().

            ExecuteFlow();
        }

        #endregion

        #region Add location
        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddLocationToOffender_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").


                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddCircularZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetProperty(x => x.SearchTerm).
                WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).




                AddBlock<Web_IsMapContainsZones>().

                AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddLocationToOffender_2Piece_GPS()
        {
            TestingFlow.

                             AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                 SetName("AddEquipmentTestBlock_2Piece").
                    SetDescription("Add equipment test block").

                    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddCellularDataRequest Type").
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Cellular Data test block").

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                     SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_2Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper.ToString()).

              AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
               SetName("AddEquipmentTestBlock_Transsmiter").
                    SetDescription("Add equipment test block").
                SetProperty(x => x.AddEquipmentRequest).
                WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                FromDeepProperty<CreateEntMsgAddEquipmentRequestBlock>(x => x.AddEquipmentRequest).

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_Transsmiter").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_Transsmiter").

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_2Piece").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_Transsmiter").
                         FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

            AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

          AddBlock<Web_AddCircularZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetProperty(x => x.SearchTerm).
                WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<Web_IsMapContainsZones>().

            AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddLocationToOffender_DV()
        {
            TestingFlow.
            #region add equipment for victim
               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_victim").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_victim").
            #endregion

            #region add equipment for Aggressor

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_aggressor").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentTestBlock_aggressor").
            #endregion

            #region add TX equipment for Aggressor

            AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("create random EntMsgAddEquipmentRequest type").
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                SetProperty(x => x.EquipmentSerialNumber).
                    WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).

            AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetDescription("Add equipment test block").
                SetName("AddEquipmentTestBlock_TRX_840").

            AddBlock<CreateEntMsgGetEquipmentListRequest>().
            SetName("CreateEntMsgGetEquipmentListRequest_TRX_840").
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_TRX_840").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
            SetName("GetEquipmentListTestBlock_TRX_840").
            SetProperty(x => x.GetEquipmentListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TRX_840").
                        FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).
            #endregion

            #region add victim offender 

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetVictimOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

            #region  add aggressor offender 
            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_TRX_840").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                         SetProperty(x => x.RelatedOffenderID).
                    WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
               SetName("AddAggressorOffendertestblock").
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("GetAggressorOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_AddCircularZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetProperty(x => x.SearchTerm).
                WithValueFromBlockIndex("GetAggressorOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

            AddBlock<Web_IsMapContainsZones>().

            AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        #endregion

        #region Activation tests

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void ActivateOffender_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ActivateOffender>().
                    SetProperty(x => x.OffenderRefId).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.Receiver).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void ActivateOffender_2Piece_GPS()
        {
            TestingFlow.

                 AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                 SetName("AddEquipmentTestBlock_2Piece").
                    SetDescription("Add equipment test block").

                    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddCellularDataRequest Type").
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Cellular Data test block").

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                     SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_2Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper.ToString()).

              AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
               SetName("AddEquipmentTestBlock_Transsmiter").
                    SetDescription("Add equipment test block").
                SetProperty(x => x.AddEquipmentRequest).
                WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_Transmitter").
                FromDeepProperty<CreateEntMsgAddEquipmentRequestBlock>(x => x.AddEquipmentRequest).

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_Transsmiter").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_Transsmiter").

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_2Piece").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_2Piece").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_Transsmiter").
                         FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

            AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                SetDescription("Add Offender test block").

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ActivateOffender>().
            SetProperty(x => x.OffenderRefId).
            WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                SetDescription("Run XT Simulator Test Block").
                SetProperty(x => x.EquipmentSN).
                    WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.Receiver).

            AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                 SetDescription("Create Get Offenders Request").
                 SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("AddOffenderTestBlock").
                         FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

            AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                 SetDescription("Get Offenders Test Block").

            AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                 SetDescription("Is Active Assert Block").
                 SetProperty(x => x.Expected).WithValue(true).

            ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void ActivateOffender_E4()
        {
            TestingFlow.

                AddBlock<VerifyIfEquipmenExistsTestBlock>().ShallGeneratePropertiesWithRandomValues().

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(78).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.E4_RF_Cellular).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("VerifyIfEquipmenExistsTestBlock").
                             FromDeepProperty<VerifyIfEquipmenExistsTestBlock>(x => x.EquipmentSN).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(1).
                    SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    SetProperty(x => x.VoicePhoneNumber).WithValue("4720068").
                    SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    SetProperty(x => x.DataPhoneNumber).WithValue("4720068").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(78).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("A10020").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Cellular).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AgencyID).WithValue(78).
                    SetProperty(x => x.Receiver).WithValueFromBlockIndex("VerifyIfEquipmenExistsTestBlock").
                             FromDeepProperty<VerifyIfEquipmenExistsTestBlock>(x => x.EquipmentSN).
                    SetProperty(x => x.Transmitter).WithValue("A10020").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ActivateOffender>().
                SetProperty(x => x.OffenderRefId).
                WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.Receiver).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void ActivateOffender_DV()
        {
            TestingFlow.
            #region add equipment for victim
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_victim").
                   SetDescription("Add equipment test block").
            #endregion

            #region add cellular data for victim

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_victim").
            #endregion

            #region add equipment for aggressor

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_aggressor").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
            #endregion

            #region add cellular data for aggressor

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentTestBlock_aggressor").
            #endregion

            #region add equipment -TX for aggressor 

            AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("create random EntMsgAddEquipmentRequest type").
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                SetProperty(x => x.EquipmentSerialNumber).
                    WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).

            AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetDescription("Add equipment test block").
                SetName("AddEquipmentTestBlock_TRX_840").

            AddBlock<CreateEntMsgGetEquipmentListRequest>().
            SetName("CreateEntMsgGetEquipmentListRequest_TRX_840").
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_TRX_840").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
            SetName("GetEquipmentListTestBlock_TRX_840").
            SetProperty(x => x.GetEquipmentListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TRX_840").
                        FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).
            #endregion

            #region add offender victim

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetVictimOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

            #region add offender aggressor

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_TRX_840").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                         SetProperty(x => x.RelatedOffenderID).
                    WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
               SetName("AddAggressorOffendertestblock").
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("GetAggressorOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

              AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ActivateOffender>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                        WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
 
              AddBlock<Web_ActivateOffender>().UsePreviousBlockData().
                SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetAggressorOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                SetDescription("Run XT Simulator Test Block").
                SetProperty(x => x.EquipmentSN).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("GetAggressorOffendersActiveTestBlock").
                    SetDescription("Get Offenders Test Block").

             AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                SetDescription("Is Active Assert Block").
                SetProperty(x => x.Expected).WithValue(true).
                SetProperty(x => x.GetOffendersResponse).
                    WithValueFromBlockIndex("GetAggressorOffendersActiveTestBlock").
                        FromPropertyName(EnumPropertyName.GetOffendersResponse).

            ExecuteFlow();
        }

        #endregion

            #region Add polygon zone

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void AddPolygonZoneToOffender_1Piece_GPS_RF()
        {
            TestingFlow.


               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_AddPolygonZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
            SetProperty(x => x.SearchTerm).
            WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

            AddBlock<Web_IsMapContainsZones>().

            AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        #endregion

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyDownloadRecommended()
        {
            TestingFlow.
          AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").


                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyDownloadRecommendedTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<Web_OffenderHeaderDetailsLabelAssertBlock>().
                    SetDescription("Offender Header Details Label Assert Block").
                    SetProperty(x => x.ExpectedOffenderLabel).WithValue("Download Recommended").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyOffenderStatusPreActive()
        {
            TestingFlow.

          AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<Web_OffenderStatusAssertBlock>().UsePreviousBlockData().
                    SetDescription("Offender Header Details Label Assert Block").
                    SetProperty(x => x.ExpectedOffenderStatus).WithValue("Pre Active").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyOffenderStatusActive()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(status).
                    SetProperty(x => x.ProgramType).WithValue(twoP_program).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                    SetDescription("Gets one active offender").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<Web_OffenderStatusAssertBlock>().UsePreviousBlockData().
                    SetDescription("Offender Header Details Label Assert Block").
                    SetProperty(x => x.ExpectedOffenderStatus).WithValue("Active").

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyOffenderContactPhone()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.ContactPhoneNumber).WithValue("111111111").
                    SetProperty(x => x.ContactPrefixPhone).WithValue("972111").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyOffenderContactInformationTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.FirstName).

                AddBlock<Web_OffenderContactInformationAssertBlock>().UsePreviousBlockData().
                    SetDescription("Offender Header Details Label Assert Block").
                    SetProperty(x => x.ExpectedOffenderContactPrefixPhone).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.ContactPrefixPhone).
                    SetProperty(x => x.ExpectedOffenderContactPhone).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.ContactPhoneNumber).

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyOffenderVoiceSimNumber()
        {
            TestingFlow.

          AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyVoiceSimNumberTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<Web_VoiceSimNumberAssertBlock>().UsePreviousBlockData().
                    SetDescription("Offender Header Details Label Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyPictureDisplayed()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.StateID).WithValue("IL").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(OffendersSvcTests.OffendersSvcTests.PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update Offender Request").
                    SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock").
                             FromDeepProperty<AddOffenderPictureTestBlock>(x => x.AddOffenderPictureResponse.NewPhotoID).
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender test block").

               AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyPictureDisplayedTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<Web_OffenderPictureAssertBlock>().
                    SetDescription("Offender Picture Assert Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyDVLink()
        {
            TestingFlow.
            #region add equipment for victim
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_victim").
                   SetDescription("Add equipment test block").
            #endregion

            #region add cellular data for victim

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_victim").
            #endregion

            #region add equipment for aggressor

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_aggressor").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
            #endregion

            #region add cellular data for aggressor

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentTestBlock_aggressor").
            #endregion

            #region add equipment -TX for aggressor 

            AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("create random EntMsgAddEquipmentRequest type").
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                SetProperty(x => x.EquipmentSerialNumber).
                    WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).

            AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetDescription("Add equipment test block").
                SetName("AddEquipmentTestBlock_TRX_840").

            AddBlock<CreateEntMsgGetEquipmentListRequest>().
            SetName("CreateEntMsgGetEquipmentListRequest_TRX_840").
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_TRX_840").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
            SetName("GetEquipmentListTestBlock_TRX_840").
            SetProperty(x => x.GetEquipmentListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TRX_840").
                        FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).
            #endregion

            #region add offender victim

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetVictimOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

            #region add offender aggressor

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_TRX_840").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                         SetProperty(x => x.RelatedOffenderID).
                    WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
               SetName("AddAggressorOffendertestblock").
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("GetAggressorOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndVerifyDVLinkTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].FirstName).

                AddBlock<Web_VerifyDVLinkAssertBlock>().
                    SetDescription("Offender Header Details Label Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyTransmitterForDV()
        {
            TestingFlow.

            #region add equipment for victim
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_victim").
                   SetDescription("Add equipment test block").
            #endregion

            #region add cellular data for victim

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_victim").
            #endregion

            #region add equipment for aggressor

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                     SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_aggressor").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
            #endregion

            #region add cellular data for aggressor

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_aggressor").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentTestBlock_aggressor").
            #endregion

            #region add equipment -TX for aggressor 

            AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("create random EntMsgAddEquipmentRequest type").
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("AddEquipmentTestBlock_victim").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                SetProperty(x => x.EquipmentSerialNumber).
                    WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).

            AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetDescription("Add equipment test block").
                SetName("AddEquipmentTestBlock_TRX_840").

            AddBlock<CreateEntMsgGetEquipmentListRequest>().
            SetName("CreateEntMsgGetEquipmentListRequest_TRX_840").
                SetProperty(x => x.EquipmentID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_TRX_840").
                        FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

            AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
            SetName("GetEquipmentListTestBlock_TRX_840").
            SetProperty(x => x.GetEquipmentListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TRX_840").
                        FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).
            #endregion

            #region add offender victim

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_victim").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetVictimOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion

            #region add offender aggressor

            AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetDescription("Create Add Offender Request").
                SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentTestBlock_aggressor").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].AgencyID).
                SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("GetEquipmentListTestBlock_TRX_840").
                        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                         SetProperty(x => x.RelatedOffenderID).
                    WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
               SetName("AddAggressorOffendertestblock").
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressorOffendertestblock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("GetAggressorOffendersTestBlock").
                    SetDescription("Get Offenders Test Block").
            #endregion


                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndGetTxFromDVPairTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Pre Active Offenders").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetVictimOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].FirstName).

                AddBlock<Web_VerifyTransmitterForDVAssertBlock>().
                    SetDescription("Offender Header Details Label Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyDownloadEvent_1Piece_GPS_RF()
        {
            TestingFlow.
             AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ActivateOffender>().
                    SetProperty(x => x.OffenderRefId).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.Receiver).
                    //SetProperty(x => x.RuleState).WithValue("off").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_MessageEventAssertBlock>().
                    SetProperty(x => x.Expected).WithValue("Download Successful").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ImportEquipmentAddOffender_Sanity)]
        [TestMethod]
        public void VerifyDownloadRequestInQueue()
        {
            TestingFlow.

             AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_ActivateOffender>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefId).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).

                AddBlock<Web_QueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Queue test block").
                    SetProperty(x => x.FilterSectionNumber).WithValue(1).
                    SetProperty(x => x.FilterTitle).WithValue("1 Piece GPS").

                AddBlock<Web_QueueTrackerAssertBlock>().UsePreviousBlockData().
                SetDescription("Web Queue assert block").
                SetProperty(x => x.IsExpectedInQueue).WithValue(true).
                SetProperty(x => x.OffenderRefId).WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.RefID).
            ExecuteFlow();
        }
    }
}
