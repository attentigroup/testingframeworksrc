﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.AssertBlocksLib.WebTestBlock;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using AssertBlocksLib.ProgramActionsAsserts;
using TestBlocksLib.DevicesBlocks;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using LogicBlocksLib.UsersBlocks;
using TestBlocksLib.UsersBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Enum;
using AssertBlocksLib.WebTestBlocks;
using TestBlocksLib.WebTestBlocks.AddNewOffender_Location;
using LogicBlocksLib.Events;
using TestBlocksLib.EventsBlocks;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using System.Linq;
using System;
using AssertBlocksLib.Events;
using LogicBlocksLib.ConfigurationBlocks;
using TestBlocksLib.ConfigurationBlocks;
using AssertBlocksLib.ConfigurationAsserts;
#endregion

namespace TestingFramework.UnitTests.WebTests
{

    [TestClass]
    public class WebUpdateOffendersProgramTests : BaseUnitTest
    {
        Offenders0.EnmProgramStatus[] PreActiveStatus = { Offenders0.EnmProgramStatus.PreActive };
        Offenders0.EnmProgramStatus[] PostActiveStatus = { Offenders0.EnmProgramStatus.PostActive };
        Offenders0.EnmProgramStatus[] ArchivedStatus = { Offenders0.EnmProgramStatus.Archived };
        Offenders0.EnmProgramStatus[] SuspendedStatus = { Offenders0.EnmProgramStatus.Suspended };
        Offenders0.EnmProgramStatus[] EnrollmentStatus = { Offenders0.EnmProgramStatus.Enrollment };

        public static int CURRENT_VERSION_NUMBER = -1;
        public static int PLANNED_VERSION_NUMBER = -2;
        Offenders0.EnmProgramStatus[] status = { Offenders0.EnmProgramStatus.Active };
        Offenders0.EnmProgramType[] oneP_program = { Offenders0.EnmProgramType.One_Piece };
        Offenders0.EnmProgramType[] twoP_program = { Offenders0.EnmProgramType.Two_Piece };

        public int OffenderId4Tests { get; set; }
        public int OffenderId4RFTests { get; set; }
        public int RuleID4Tests { get; set; }
        public WebUpdateOffendersProgramTests()
        {
            OffenderId4RFTests = 6160;
            RuleID4Tests = 100460;

        }


        [TestCategory(CategoriesNames.Web_UpdateConfiguration_Sanity)]
        [TestMethod]
        public void UpdateConfiguration_OnePieceGPSRF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("BeforeUpdateConfiguration").

                AddBlock<CreateConfigurationListToUpdatedListTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_OffenderConfigVerifyElementsExistTestBlock>().UsePreviousBlockData().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("AfterUpdateConfiguration").

                AddBlock<Web_UpdateGPSOffenderConfigurationRequestAssertBlock>().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_BeforeUpdate).
                        WithValueFromBlockIndex("BeforeUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_AfterUpdate).
                        WithValueFromBlockIndex("AfterUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).
                                                                
            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_UpdateConfiguration_Sanity)]
        [TestMethod]
        public void UpdateConfiguration_TwoPiece_DV_Aggressor()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue( Offenders0.EnmProgramConcept.Aggressor ).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("BeforeUpdateConfiguration").

                AddBlock<CreateConfigurationListToUpdatedListTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_OffenderConfigVerifyElementsExistTestBlock>().UsePreviousBlockData().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("AfterUpdateConfiguration").

                AddBlock<Web_UpdateGPSOffenderConfigurationRequestAssertBlock>().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_BeforeUpdate).
                        WithValueFromBlockIndex("BeforeUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_AfterUpdate).
                        WithValueFromBlockIndex("AfterUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_UpdateConfiguration_Sanity)]
        [TestMethod]
        public void UpdateConfiguration_TwoPiece_DV_Victim()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("BeforeUpdateConfiguration").

                AddBlock<CreateConfigurationListToUpdatedListTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_OffenderConfigVerifyElementsExistTestBlock>().UsePreviousBlockData().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("AfterUpdateConfiguration").

                AddBlock<Web_UpdateGPSOffenderConfigurationRequestAssertBlock>().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_BeforeUpdate).
                        WithValueFromBlockIndex("BeforeUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_AfterUpdate).
                        WithValueFromBlockIndex("AfterUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_UpdateConfiguration_Sanity)]
        [TestMethod]
        public void UpdateConfiguration_TwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("BeforeUpdateConfiguration").

                AddBlock<CreateConfigurationListToUpdatedListTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_OffenderConfigVerifyElementsExistTestBlock>().UsePreviousBlockData().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("AfterUpdateConfiguration").

                AddBlock<Web_UpdateGPSOffenderConfigurationRequestAssertBlock>().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_BeforeUpdate).
                        WithValueFromBlockIndex("BeforeUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_AfterUpdate).
                        WithValueFromBlockIndex("AfterUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_UpdateConfiguration_Sanity)]
        [TestMethod]
        public void UpdateConfiguration_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("BeforeUpdateConfiguration").

                AddBlock<CreateConfigurationListToUpdatedListTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login page").

                AddBlock<Web_OffenderConfigVerifyElementsExistTestBlock>().UsePreviousBlockData().
                    SetDescription("Offenders List Screen Click On Filter Test Block").
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetName("AfterUpdateConfiguration").

                AddBlock<Web_UpdateGPSOffenderConfigurationRequestAssertBlock>().
                    SetDescription("Get Configuration GPS Rule for offender").
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_BeforeUpdate).
                        WithValueFromBlockIndex("BeforeUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).
                    SetProperty(x => x.GetOffenderConfigurationDataResponse_AfterUpdate).
                        WithValueFromBlockIndex("AfterUpdateConfiguration").
                            FromPropertyName(EnumPropertyName.GetOffenderConfigurationDataResponse).

            ExecuteFlow();
        }

    }
}
