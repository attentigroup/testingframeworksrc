﻿using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.Events;
using System;
using TestBlocksLib.EventsBlocks;
using AssertBlocksLib.WebTestBlocks;

#region API Offenders refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using WebTests.InfraStructure;

#endregion

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
   public class MonitorOffenderTests: WebBaseUnitTest
    {

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SearchEventOnMonitorScreen()
       {

            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                    AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMinutes(-30)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                 AddBlock<Web_MonitorScreenSearchEventTestBlock>().UsePreviousBlockData().
                    SetDescription("Web_Search Event Test Block").
                    SetProperty(x => x.GetEventsResponse).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                    AddBlock<Web_SearchEventsOnMonitorScreenAssertBlock>().
                    SetDescription("Web_Search Event Assert Block").
 
                     ExecuteFlow();
        }
    }
}
