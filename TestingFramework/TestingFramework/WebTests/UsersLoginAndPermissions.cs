﻿using Common.Categories;
using LogicBlocksLib.APIExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.TestingFrameworkBlocks.TestBlocksLib.WebTestBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.AssertBlocksLib.WebTestBlock;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    public class UsersLoginAndPermissions : WebBaseUnitTest
    {
        //[TestCategory(CategoriesNames.Web_Sanity)]
        //[TestMethod]
        //public void GUISecurityConfigurationChangePasswordIsInvisible()
        //{
        //    TestingFlow.
        //        AddBlock<GetGuiSecurityDataTestBlock>().
        //            SetDescription("Get GUI security data of user").
        //            SetProperty(x => x.FirstName).WithValue("AlexSE").
        //            SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //            SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<SetGUISecurityDataLogicBlock>().
        //            SetDescription("Set GUI security logic block").
        //            SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //            SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.ResourceID).WithValue(new string[] { "General_AboutToolTip_Button_ChangePassword" }).


        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //            SetDescription("Web Login TestBlock").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //        SetDescription("Web Clear Cache TestBlock").

        //        AddBlock<Web_AboutToolTipTestBlock>().UsePreviousBlockData().
        //        SetDescription("Web About Tool Tip Test Block ").

        //        AddBlock<Web_ElementVisibleAssertBlock>().
        //        SetDescription("Verify if About Tool Tip change password is invisible").
        //        SetProperty(x => x.IsExpectedVisible).WithValue(false).

        //        AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
        //        SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //        SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //        SetDescription("Clear cache before logout").

        //        AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
        //        SetDescription("Clear cache and Log out").

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login").

        //        AddBlock<Web_AboutToolTipTestBlock>().UsePreviousBlockData().
        //        SetDescription("Web About Tool Tip Test Block ").

        //        AddBlock<Web_ElementVisibleAssertBlock>().
        //        SetDescription("Verify if About Tool Tip change password is visible").
        //        SetProperty(x => x.IsExpectedVisible).WithValue(true).

        //        ExecuteFlow();
        //}


        //[TestCategory(CategoriesNames.Web_Sanity)]
        //[TestMethod]
        //public void GUISecurityConfigurationDatabaseVersionIsInvisible()
        //{
        //    TestingFlow.

        //        AddBlock<GetGUISecurityDataLogicBlock>().
        //        SetDescription("Get GUI security data of user").
        //        SetProperty(x => x.FirstName).WithValue("AlexSE").
        //        SetProperty(x => x.Password).WithValue("Q1w2e3r4").
        //        SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

        //        AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
        //        SetDescription("Test block").

        //        AddBlock<SetGUISecurityDataLogicBlock>().
        //        SetDescription("Set GUI security logic block").
        //        SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //        SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

        //        AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
        //        SetProperty(x => x.ResourceID).WithValue(new string[] { "General_AboutToolTip_Label_DatabaseVersion" }).

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login").

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //        SetDescription("Clear cache").

        //        AddBlock<Web_AboutToolTipTestBlockDBVersion>().UsePreviousBlockData().
        //        SetDescription("Web About Tool Tip Test Block ").

        //        AddBlock<Web_ElementVisibleAssertBlock>().
        //        SetDescription("Verify if About Tool Tip change Data base version is invisible").
        //        SetProperty(x => x.IsExpectedVisible).WithValue(false).

        //        AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
        //        SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
        //        SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

        //        AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
        //        SetDescription("Clear cache before logout").

        //        AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
        //        SetDescription("Clear cache and Log out").

        //        AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
        //        SetDescription("Login").

        //        AddBlock<Web_AboutToolTipTestBlockDBVersion>().UsePreviousBlockData().
        //        SetDescription("Web About Tool Tip Test Block ").

        //        AddBlock<Web_ElementVisibleAssertBlock>().
        //        SetDescription("Verify if Data base version is visible").
        //        SetProperty(x => x.IsExpectedVisible).WithValue(true).

        //        ExecuteFlow();
        //}



        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void GUIChangePassword()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("AlexSE").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                SetProperty(x => x.NewPassword).WithValue("W2e3r4t5").

                AddBlock<Web_UserMenuChangePasswordTestBlock>().UsePreviousBlockData().

              

                ExecuteFlow();
        }
       
        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void GUIForgotPassword()
        {
            TestingFlow.

                AddBlock<Web_ForgotPasswordTestBlock>().
                SetDescription("ForGotPassword").
                SetProperty(x => x.FirstName).WithValue("AliceOF").
                SetProperty(x => x.Email).WithValue("Oshalev.cw@mmm.com").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SecurityConfigurationGeneral_Screen()
        {
            TestingFlow.
                AddBlock<GetGUISecurityDataLogicBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("newSA").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Test block").

                AddBlock<SetGUISecurityDataLogicBlock>().
                    SetDescription("Set GUI security logic block").
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ResourceID).WithValue(new string[] { "General_Screen_Link_DVPairsList" }).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_DVPairsVisibleTestBlock>().UsePreviousBlockData().

                AddBlock<Web_ElementVisibleAssertBlock>().
                    SetDescription("Verify if Filter section is visible").
                    SetProperty(x => x.IsExpectedVisible).WithValue(false).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache before logout").

                AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache and Log out").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_DVPairsVisibleTestBlock>().UsePreviousBlockData().

                AddBlock<Web_ElementVisibleAssertBlock>().
                    SetDescription("Verify if Filter section is visible").
                    SetProperty(x => x.IsExpectedVisible).WithValue(true).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SecurityConfigurationGeneral_Toolbar_AddNewOffender()
        {
            TestingFlow.
                AddBlock<GetGUISecurityDataLogicBlock>().
                SetDescription("Get GUI security data of user").
                SetProperty(x => x.FirstName).WithValue("newSA").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                SetDescription("Test block").

                AddBlock<SetGUISecurityDataLogicBlock>().
                SetDescription("Set GUI security logic block").
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ResourceID).WithValue(new string[] { "General_Toolbar_ListItem_AddNewOffender" }).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

                AddBlock<Web_AddNewOffenderVisibleTest>().UsePreviousBlockData().

                AddBlock<Web_ElementVisibleAssertBlock>().
                SetDescription("Verify if Filter section is visible").
                SetProperty(x => x.IsExpectedVisible).WithValue(false).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache before logout").

                AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache and Log out").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache before logout").

                AddBlock<Web_AddNewOffenderVisibleTest>().UsePreviousBlockData().

                AddBlock<Web_ElementVisibleAssertBlock>().
                SetDescription("Verify if Filter section is visible").
                SetProperty(x => x.IsExpectedVisible).WithValue(true).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SecurityConfigurationGeneral_Toolbar_About()
        {
            TestingFlow.
                AddBlock<GetGUISecurityDataLogicBlock>().
                SetDescription("Get GUI security data of user").
                SetProperty(x => x.FirstName).WithValue("newSA").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                SetDescription("Test block").

                AddBlock<SetGUISecurityDataLogicBlock>().
                SetDescription("Set GUI security logic block").
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ResourceID).WithValue(new string[] { "General_Toolbar_Tooltip_About" }).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

                AddBlock<Web_InfoToolTipTestBlock>().UsePreviousBlockData().

                AddBlock<Web_ElementVisibleAssertBlock>().
                SetDescription("Verify if Filter section is visible").
                SetProperty(x => x.IsExpectedVisible).WithValue(false).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache before logout").

                AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache and Log out").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login").

                 AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache before logout").

                AddBlock<Web_InfoToolTipTestBlock>().UsePreviousBlockData().

                AddBlock<Web_ElementVisibleAssertBlock>().
                SetDescription("Verify if Filter section is visible").
                SetProperty(x => x.IsExpectedVisible).WithValue(true).

                ExecuteFlow();
        }
    }
}