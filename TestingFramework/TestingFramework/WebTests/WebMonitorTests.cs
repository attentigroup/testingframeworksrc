﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using Common.Enum;
using LogicBlocksLib.Events;
using LogicBlocksLib.Group;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.ZonesBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.EventsBlocks;
using TestBlocksLib.GroupsBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using TestBlocksLib.WebTestBlocks.Paging;
using TestBlocksLib.ZonesBlocks;
using TestingFramework.AssertBlocksLib.WebTestBlock;
using LogicBlocksLib.ScheduleBlocks;
using TestBlocksLib.ScheduleBlocks;
using static WebTests.InfraStructure.Pages.GroupsDetailsPage.DetailsTabPage;
using AssertBlocksLib.GroupsBlocks;
using AssertBlocksLib.OffendersAsserts;
using TestingFramework.Proxies.API.Schedule;
using WebTests.InfraStructure;

#region API refs

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestBlocksLib.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using System.Collections.Generic;
using System.Collections;


#endregion

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    public class WebMonitorTests : WebBaseUnitTest
    {
        Offenders0.EnmProgramStatus[] status_Active = { Offenders0.EnmProgramStatus.Active };
        Offenders0.EnmProgramType[] trackers = { Offenders0.EnmProgramType.One_Piece_RF/*, Offenders0.EnmProgramType.Two_Piece*/ };

        #region Handle Event- 1Piece_GPS_RF 

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Remark_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Remark").

                AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Warning_1Piece_GPS_RF()
        {
            TestingFlow.

              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(40).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEvents_NewInProcessAutoProcess_TestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetEventsResponse).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEvents_NewInProcessAutoProcess_TestBlock").
                            FromDeepProperty<GetEvents_NewInProcessAutoProcess_TestBlock>(x => x.GetEventsResponseCopy.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Warning").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Email_1Piece_GPS_RF()
        {
            TestingFlow.

              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).
                //SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                 SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                 SetProperty(x => x.HandlingOption).WithValue("Email").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                 SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_TextMessage_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Text Message").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Handle_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Handle").

                 AddBlock<Web_GetEventStatus>().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("Handled").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_QuickHandling_1Piece_GPS_RF()
        {
            TestingFlow.

              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventQuickHandlingTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("Quick").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_ScheduleTask_1Piece_GPS_RF()
        {
            TestingFlow.
              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddHours(-22)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
              AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").
              AddBlock<GetRelatedEventsTestBlock>().UsePreviousBlockData().
                    SetName("API before"). 
                    SetDescription("Get linked events counts from API for the tested event before create schedule task").
                    SetProperty(x => x.EventLogID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.RowCount).WithValue(20).
              AddBlock<Web_LoginTestBlock>().
              //need to ask omer about the case that EventList return with amountof row=0
              AddBlock<Web_HandleScheduleTaskEvent>().UsePreviousBlockData().
                SetProperty(x => x.eventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                SetProperty(x => x.Time).WithValue(DateTime.Now.AddMinutes(2)).
                AddBlock<WaitForRelatedEventTestBlock>().UsePreviousBlockData().
                SetDescription("block that wait to get the new schedule task releated event").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                                SetProperty(x => x.APIAmountOfRowsBefore).WithValueFromBlockIndex("API before").
                     FromDeepProperty<GetRelatedEventsTestBlock>(x => x.APIAmountOfRows).
                AddBlock<GetRelatedEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get linked events from API for the tested event").
                    SetProperty(x => x.EventLogID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.RowCount).WithValue(20).
                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").
                AddBlock<Web_GetLinkedEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("WaitForRelatedEventTestBlock").
                            FromDeepProperty<WaitForRelatedEventTestBlock>(x => x.EventID).
                   SetProperty(x => x.ArrayLength).
                        WithValueFromBlockIndex("WaitForRelatedEventTestBlock").
                            FromDeepProperty<WaitForRelatedEventTestBlock>(x => x.APIAmountOfRowsAfter).
            AddBlock<Web_IsEventIDExistInMonitorTableRowAssertBlock>().UsePreviousBlockData().
                   SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("WaitForRelatedEventTestBlock").
                            FromDeepProperty<WaitForRelatedEventTestBlock>(x => x.LinkedEventID).
                  SetProperty(x => x.MonitorTableRows).
                    WithValueFromBlockIndex("Web_GetLinkedEventTestBlock").
                            FromDeepProperty<Web_GetLinkedEventTestBlock>(x => x.LinkedEventsArr).
            ExecuteFlow();
        }

        #endregion

        #region Handle Event - E4_RF

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Remark_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Remark").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Warning_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Warning").

                AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Email_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-15)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Email").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_TextMessage_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Text Message").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("InProcess").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_Handle_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.HandlingOption).WithValue("Handle").

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("Handled").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_QuickHandling_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleEventQuickHandlingTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                 AddBlock<Web_GetEventStatus>().UsePreviousBlockData().

                 AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("Quick").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void HandleEvent_ScheduleTask_E4_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).
                //SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_HandleScheduleTaskEvent>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Time).WithValue(DateTime.Now.AddMinutes(2)).

                AddBlock<Web_GetScheduleTaskEvent>().UsePreviousBlockData().
                    SetProperty(x => x.time).
                        WithValueFromBlockIndex("Web_HandleScheduleTaskEvent").
                            FromDeepProperty<Web_HandleScheduleTaskEvent>(x => x.Time).

                 AddBlock<Web_ScheduleTaskAssertBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].DeviceSN).

            ExecuteFlow();
        }

        #endregion

        #region Filter Events

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void EventByDate_FilterEventByPeriod_MonitorScreen()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_FilterEventsByPeriod>().
                    SetProperty(x => x.Recent).WithValue("48").

                AddBlock<Web_GetFilterEventTestBlock>().UsePreviousBlockData().

                AddBlock<Web_FilterEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.Recent).
                        WithValueFromBlockIndex("Web_FilterEventsByPeriod").
                            FromDeepProperty<Web_FilterEventsByPeriod>(x => x.Recent).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void EventByDate_FilterEventByRangeOfDates_MonitorScreen()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_FilterEventsByRangeOfDates>().
                    SetProperty(x => x.startTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.endTime).WithValue(DateTime.Now).

                AddBlock<Web_GetFilterEventTestBlock>().UsePreviousBlockData().

                AddBlock<Web_FilterEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.StartTime).
                        WithValueFromBlockIndex("Web_FilterEventsByRangeOfDates").
                            FromDeepProperty<Web_FilterEventsByRangeOfDates>(x => x.startTime).
                    SetProperty(x => x.EndTime).
                        WithValueFromBlockIndex("Web_FilterEventsByRangeOfDates").
                            FromDeepProperty<Web_FilterEventsByRangeOfDates>(x => x.endTime).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void EventByDate_FilterEventByPeriod_CurrentStatusScreen()
        {
            TestingFlow.

              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(-1).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_FilterOffenderEventsByPeriod>().UsePreviousBlockData().
                    SetProperty(x => x.Recent).WithValue("48").
                    SetProperty(x => x.SecondarySearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_GetOffenderFilterEventTestBlock>().UsePreviousBlockData().

                AddBlock<Web_FilterEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.StartTime).WithValue(null).
                    SetProperty(x => x.EndTime).WithValue(null).
                    SetProperty(x => x.Recent).
                        WithValueFromBlockIndex("Web_FilterOffenderEventsByPeriod").
                            FromDeepProperty<Web_FilterOffenderEventsByPeriod>(x => x.Recent).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void EventByDate_FilterEventByRangeOfDates_CurrentStatusScreen()
        {
            TestingFlow.

              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(-1).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_FilterOffenderEventsByRangeOfDates>().UsePreviousBlockData().
                    SetProperty(x => x.startTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.endTime).WithValue(DateTime.Now).
                    SetProperty(x => x.SecondarySearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                 AddBlock<Web_GetOffenderFilterEventTestBlock>().UsePreviousBlockData().

                AddBlock<Web_FilterEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.StartTime).
                        WithValueFromBlockIndex("Web_FilterOffenderEventsByRangeOfDates").
                            FromDeepProperty<Web_FilterOffenderEventsByRangeOfDates>(x => x.startTime).
                    SetProperty(x => x.EndTime).
                        WithValueFromBlockIndex("Web_FilterOffenderEventsByRangeOfDates").
                            FromDeepProperty<Web_FilterOffenderEventsByRangeOfDates>(x => x.endTime).

            ExecuteFlow();
        }

        #endregion

        #region Manual Event

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void ManualEvent_CreateManualEvent()
        {
            TestingFlow.

              AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(-1).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateManualEvent>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_GetManualEvent>().UsePreviousBlockData().

                AddBlock<Web_ManualEventAssertBlock>().
                    SetProperty(x => x.expectedStatus).WithValue("New").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void ManualEvent_HandleManualEvent()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(-1).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateManualEvent>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_GetManualEvent>().UsePreviousBlockData().

                AddBlock<Web_HandleEventFromCurrentStatusTestBlock>().UsePreviousBlockData().
                  SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("Web_GetManualEvent").
                        FromDeepProperty<Web_GetManualEvent>(x => x.CurrentStatusTableRow.EventId).
                  SetProperty(x => x.HandlingOption).WithValue("Handle").

                AddBlock<Web_GetEventStatusFromCurrentStatus>().UsePreviousBlockData().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("Web_GetManualEvent").
                            FromDeepProperty<Web_GetManualEvent>(x => x.CurrentStatusTableRow.EventId).

                AddBlock<Web_EventStatusAssertBlock>().
                    SetProperty(x => x.expected).WithValue("Handled").

            ExecuteFlow();
        }

        #endregion

        #region User Filter
        // Not deleted becuase need to change the test to count the number of rows diffrently
        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void UserFilter_FilterEventsByProgramType_Monitor()
        //{
        //    TestingFlow.

        //        AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_CreateProgramTypeUserFilterTestBlock>().
        //            SetProperty(x => x.ProgramTypes).WithValue(new string[] { "RF Curfew Dual (E4)" }).

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<Web_AmountOfRowsAssertBlock>().UsePreviousBlockData().
        //            SetDescription("Checks Filter section in Offenders screen").
        //            SetProperty(x => x.WebAmountOfRows).
        //                WithValueFromBlockIndex("Web_CreateProgramTypeUserFilterTestBlock").
        //                    FromPropertyName(EnumPropertyName.AmountOfRows).
        //            SetProperty(x => x.APIAmountOfRows).
        //                WithValueFromBlockIndex("GetEventsTestBlock").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void UserFilter_FilterEventsByAgencyId_Monitor()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.GetAgenciesByEvents()[0]).
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_CreateAgencyIdUserFilterTestBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.AgencyId).
        //                WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock").
        //                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.AgencyID).

        //         AddBlock<Web_AmountOfRowsAssertBlock>().
        //            SetDescription("Checks Filter section in Offenders screen").
        //            SetProperty(x => x.WebAmountOfRows).
        //                WithValueFromBlockIndex("Web_CreateAgencyIdUserFilterTestBlock").
        //                    FromPropertyName(EnumPropertyName.AmountOfRows).
        //            SetProperty(x => x.APIAmountOfRows).
        //                WithValueFromBlockIndex("GetEventsTestBlock").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

        //    ExecuteFlow();
        //}

        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void UserFilter_FilterEventsByOffenderName_Monitor()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
        //            SetName("CreateEntMsgGetEventsRequestBlock_ForSprcificOffender").
        //            SetProperty(x => x.OffenderID).
        //                WithValueFromBlockIndex("GetOffenderFromEvent").
        //                    FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].ID).
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
        //            SetName("GetEventsTestBlock_ForSprcificOffender").
        //            SetProperty(x => x.GetEventsRequest).
        //                WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ForSprcificOffender").
        //                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

        //        AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_CreateOffenderNameUserFilterTestBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.OffenderName).
        //                WithValueFromBlockIndex("GetOffenderFromEvent").
        //                    FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].FirstName).

        //        AddBlock<Web_AmountOfRowsAssertBlock>().UsePreviousBlockData().
        //            SetDescription("Checks Filter section in Offenders screen").
        //            SetProperty(x => x.WebAmountOfRows).
        //                WithValueFromBlockIndex("Web_CreateOffenderNameUserFilterTestBlock").
        //                    FromPropertyName(EnumPropertyName.AmountOfRows).
        //            SetProperty(x => x.APIAmountOfRows).
        //                WithValueFromBlockIndex("GetEventsTestBlock_ForSprcificOffender").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void PagingMonitor()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login web block").

                AddBlock<Web_MonitorPaging>().
                    SetDescription("Web Monitor Paging").

                AddBlock<Web_PagingAssertBlock>().
                    SetDescription("Pager displayed rows assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void PagingOffenderList()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login web block").

                AddBlock<Web_OffenderListPaging>().
                    SetDescription("Offender List Paging").

                AddBlock<Web_PagingAssertBlock>().
                    SetDescription("Pager displayed rows assert block").

            ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void UserFilter_FilterEventsByOfficerName_Monitor()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.SortDirection).WithValue(Events0.ListSortDirection.Descending).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
        //            SetName("CreateEntMsgGetEventsRequestBlock_ForSprcificOffender").
        //            SetProperty(x => x.EventID).WithValue(0).
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-15)).
        //            SetProperty(x => x.OfficerID).
        //                WithValueFromBlockIndex("GetOffenderFromEvent").
        //                    FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
        //            SetName("GetEventsTestBlock_ForSprcificOffender").
        //            SetProperty(x => x.GetEventsRequest).
        //                WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ForSprcificOffender").
        //                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

        //        AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_CreateOfficerNameUserFilterTestBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.OfficerName).
        //                WithValueFromBlockIndex("GetOffenderFromEvent").
        //                    FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].OfficerName).

        //        AddBlock<Web_AmountOfRowsAssertBlock>().
        //            SetDescription("Checks Filter section in Offenders screen").
        //            SetProperty(x => x.WebAmountOfRows).
        //                WithValueFromBlockIndex("Web_CreateOfficerNameUserFilterTestBlock").
        //                    FromPropertyName(EnumPropertyName.AmountOfRows).
        //            SetProperty(x => x.APIAmountOfRows).
        //                WithValueFromBlockIndex("GetEventsTestBlock_ForSprcificOffender").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

        //    ExecuteFlow();
        //}


        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterBySeverity_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.OffenderID).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("GetEventsTestBlock_ForSprcificOffender").
                    SetProperty(x => x.GetEventsRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                            FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateSeverityUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.Severity).WithValue("Violation").
                    SetProperty(x => x.SecondarySearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_GetCurrentStatusRowsTestBlock>().UsePreviousBlockData().

                AddBlock<Web_EventsAddFilterAssertBlock>().
                    SetProperty(x => x.Severity).WithValue("Violation").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void PagingCurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create event request logic block").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(100).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test block").

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().
                    SetDescription("Get Offender From Event").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get offender request logic block").

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.SHUTDOWNTIMER).WithValue("3").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login web block").

                AddBlock<Web_CurrentStatusAllPages>().UsePreviousBlockData().
                    SetDescription("Web current status paging block").
                    SetProperty(x => x.ShowResultsNumber).WithValue(100).
                    SetProperty(x => x.SearchTerm)./*WithValue("34366213").*/
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_PagingAssertBlock>().
                    SetDescription("Pager displayed rows assert block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByEventID_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventID).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("GetEventsTestBlock_ForSprcificOffender").
                    SetProperty(x => x.GetEventsRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                            FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateEventIDUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SecondarySearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock_ForSprcificOffender").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_GetCurrentStatusRowsTestBlock>().UsePreviousBlockData().

                AddBlock<Web_EventsAddFilterAssertBlock>().
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock_ForSprcificOffender").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByEventTime_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("GetEventsTestBlock_ForSpecificOffender").
                    SetProperty(x => x.GetEventsRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                            FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateEventTimeUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SecondarySearchTerm).
                    WithValueFromBlockIndex("GetOffenderFromEvent").
                        FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffenderFromEvent").
                        FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.EventTime).
                        WithValueFromBlockIndex("GetEventsTestBlock_ForSpecificOffender").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.EndTime.Value).

                AddBlock<Web_GetCurrentStatusRowsTestBlock>().UsePreviousBlockData().

                AddBlock<Web_EventsAddFilterAssertBlock>().
                    SetProperty(x => x.EventTime).
                        WithValueFromBlockIndex("GetEventsTestBlock_ForSpecificOffender").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.EndTime.Value).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByEventMessage_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetOffenderFromEvent>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.OffenderID).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("GetEventsTestBlock_ForSpecificOffender").
                    SetProperty(x => x.GetEventsRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ForSpecificOffender").
                            FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateEventMessageUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.SecondarySearchTerm).
                        WithValueFromBlockIndex("GetOffenderFromEvent").
                            FromDeepProperty<GetOffenderFromEvent>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.EventMessage).
                        WithValueFromBlockIndex("GetEventsTestBlock_ForSpecificOffender").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].Message).

                AddBlock<Web_GetCurrentStatusRowsTestBlock>().UsePreviousBlockData().

                AddBlock<Web_EventsAddFilterAssertBlock>().
                    SetProperty(x => x.EventMessage).
                        WithValueFromBlockIndex("GetEventsTestBlock_ForSpecificOffender").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].Message).
                    SetProperty(x => x.Severity).WithValue(null).
                    SetProperty(x => x.EventTime).WithValue(null).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByEndOfProgramForActiveOffender_OffendersList()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateFilterByEndOfProgramForActiveOffenderTestBlock>().
                    SetProperty(x => x.Time).WithValue(DateTime.Now).

                AddBlock<Web_GetOffenderDetailsFromTableTestBlock>().

                AddBlock<Web_FilterByEndOfProgramForActiveOffenderAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.Time).
                        WithValueFromBlockIndex("Web_CreateFilterByEndOfProgramForActiveOffenderTestBlock").
                            FromDeepProperty<Web_CreateFilterByEndOfProgramForActiveOffenderTestBlock>(x => x.Time).

            ExecuteFlow();
        }

        #endregion


        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void SystemFilter_InProcess_CheckAmountOfEvents_Monitor()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetDescription("Create get events request").
        //            SetName("GetEventsRequestBlock_BeforeHandling").
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Limit).WithValue(1).
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
        //            SetDescription("Get events test").

        //        AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Add handling action request").
        //            SetProperty(x => x.Comment).WithValue("Handled by automation").
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetEventsTestBlock").
        //                FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
        //            SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateRemark).

        //            AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add handling action test").

        //            AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_GetAmountOfRowsForSpecificSystemFilterMonitorScreen>().
        //            SetProperty(x => x.Title).WithValue("In Process").
        //            SetProperty(x => x.sectionNumber).WithValue(1).

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetName("CreateEntMsgGetEventsRequestBlock_InProcess").
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.InProcess).
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Limit).WithValue(-1).
        //            SetProperty(x => x.EventID).WithValue(null).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
        //        SetName("GetEventsTestBlock_InProcess").

        //         AddBlock<Web_AmountOfRowsAssertBlock>().
        //            SetDescription("Checks Filter section in Offenders screen").
        //            SetProperty(x => x.WebAmountOfRows).
        //                WithValueFromBlockIndex("Web_GetAmountOfRowsForSpecificSystemFilterMonitorScreen").
        //                    FromPropertyName(EnumPropertyName.AmountOfRows).
        //            SetProperty(x => x.APIAmountOfRows).
        //                WithValueFromBlockIndex("GetEventsTestBlock_InProcess").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_VerifyAllFilterExist_Monitor()
        {
            TestingFlow.

                    AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetSystemFiltersMonitorScreenTestBlock>().

                AddBlock<Web_SystemFiltersAssertBlock>().
                SetProperty(x => x.ExpectedSystemFilters).WithValue(new string[] { "New", "In 3rd Party Process", "In Process", "New, In Process", "New, In Process, Auto Process", "All" }).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_New_CheckAmountOfEvents_Monitor()
        {
            TestingFlow.

                    AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetAmountOfRowsForSpecificSystemFilterMonitorScreen>().
                    SetProperty(x => x.Title).WithValue("New").
                    SetProperty(x => x.sectionNumber).WithValue(1).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetName("CreateEntMsgGetEventsRequestBlock_InProcess").
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.EventID).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_InProcess").

                 AddBlock<Web_AmountOfRowsAssertBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_GetAmountOfRowsForSpecificSystemFilterMonitorScreen").
                            FromPropertyName(EnumPropertyName.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetEventsTestBlock_InProcess").
                            FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

            ExecuteFlow();
        }


        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void SystemFilter_New_CheckAmountOfEvents_CurrentStatus()
        //{
        //    TestingFlow.

        //         AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).

        //        AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

        //        AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

        //        AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen>().UsePreviousBlockData().
        //            SetProperty(x => x.Title).WithValue("New").
        //            SetProperty(x => x.sectionNumber).WithValue(1).
        //            SetProperty(x => x.EventID).
        //                WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
        //                    FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

        //        AddBlock<CreateEntMsgGetEventsRequestBlock>().
        //            SetName("CreateEntMsgGetEventsRequestBlock_InProcess").
        //            SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
        //            SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-14)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now).
        //            SetProperty(x => x.Limit).WithValue(-1).
        //            SetProperty(x => x.EventID).WithValue(null).
        //            SetProperty(x => x.OffenderID).
        //                WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
        //                    FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.OffenderID).

        //        AddBlock<GetEventsTestBlock>().
        //            SetName("GetEventsTestBlock_New").
        //            SetProperty(x => x.GetEventsRequest).
        //                WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_InProcess").
        //                    FromPropertyName(EnumPropertyName.GetEventsRequest).

        //        AddBlock<Web_AmountOfRowsAssertBlock>().UsePreviousBlockData().
        //            SetDescription("Checks Filter section in Offenders screen").
        //            SetProperty(x => x.WebAmountOfRows).
        //                WithValueFromBlockIndex("Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen").
        //                    FromPropertyName(EnumPropertyName.WebAmountOfRows).
        //            SetProperty(x => x.APIAmountOfRows).
        //                WithValueFromBlockIndex("GetEventsTestBlock_New").
        //                    FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

        //    ExecuteFlow();
        //}


        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_New_CheckIconAndTooltip_Monitor()
        {
            TestingFlow.

                    AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen>().
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("New").
                    SetProperty(x => x.EventsDetailsArrLength).WithValue(2).

                 AddBlock<Web_CheckIconAndTooltipAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.Status).
                        WithValueFromBlockIndex("Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen").
                            FromDeepProperty<Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen>(x => x.Title).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_InProcess_CheckIconAndTooltip_Monitor()
        {
            TestingFlow.

                    AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen>().
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("In Process").
                    SetProperty(x => x.EventsDetailsArrLength).WithValue(1).

                 AddBlock<Web_CheckIconAndTooltipAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.Status).
                        WithValueFromBlockIndex("Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen").
                            FromDeepProperty<Web_GetEventsDetailsFromSpecificSystemFilterMonitorScreen>(x => x.Title).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_New_CheckIconAndTooltip_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                 AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetEventsDetailsFromSpecificSystemFilterCurrentStatusScreen>().UsePreviousBlockData().
                    SetProperty(x => x.Title).WithValue("New").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<Web_CheckIconAndTooltipAssertBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.Status).
                        WithValueFromBlockIndex("Web_GetEventsDetailsFromSpecificSystemFilterCurrentStatusScreen").
                            FromDeepProperty<Web_GetEventsDetailsFromSpecificSystemFilterCurrentStatusScreen>(x => x.Title).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_InProcess_CheckIconAndTooltip_CurrentStatus()
        {
            TestingFlow.
                 AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").
                 AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateWarning).
                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Warning handling action to set event status to In Process").            
                AddBlock<Web_LoginTestBlock>().   
                AddBlock<Web_GetEventsDetailsFromSpecificSystemFilterCurrentStatusScreen>().UsePreviousBlockData().
                    SetProperty(x => x.Title).WithValue("In Process").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.EventsDetailsArrLength).WithValue(1).
                    SetProperty(x => x.eventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                AddBlock<Web_CheckIconAndTooltipAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.Status).
                        WithValueFromBlockIndex("Web_GetEventsDetailsFromSpecificSystemFilterCurrentStatusScreen").
                            FromDeepProperty<Web_GetEventsDetailsFromSpecificSystemFilterCurrentStatusScreen>(x => x.Title).
            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_InProcess_CheckAmountOfEvents_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-14)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.InProcess).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).// instead of: Events0.ListSortDirection.Descending

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen>().UsePreviousBlockData().
                    SetProperty(x => x.Title).WithValue("In Process").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_InProcess").
                    SetProperty(x => x.EventID).WithValue(0).
                    SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.OffenderID).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("GetEventsTestBlock_InProcess").

                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen").
                            FromPropertyName(EnumPropertyName.WebAmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetEventsTestBlock_InProcess").
                            FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_NewAndInProcess_CheckAmountOfEvents_CurrentStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-14)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("GetNewEventToTestOnItsOffenderNewAndInProcessFilter").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().

                    AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.OffenderID).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_New").


                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                   // SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-14)).
                   // SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.InProcess).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.OffenderID).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetEventsRequestBlock_InProcess").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen>().UsePreviousBlockData().
                    SetProperty(x => x.Title).WithValue("New, In Process").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetName("Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen").

            AddBlock<Web_AmountOfRowsAssertBlock>().
                SetDescription("Checks Filter section in Offenders screen").
                SetProperty(x => x.WebAmountOfRows).
                    WithValueFromBlockIndex("Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen").
                        FromPropertyName(EnumPropertyName.WebAmountOfRows).
                SetProperty(x => x.APIAmountOfRows).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_New").
                        FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).
                SetProperty(x => x.APIAmountOfRows2).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_InProcess").
                        FromDeepProperty<GetEventsTestBlock>(x => x.AmountOfEvents).
                SetProperty(x => x.Message).
                    WithValue("Fail to compare between amount of Events in WEB against to the API").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_HandleEventChangeAmountOfNewEvents_CurrentStatus()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Active Offenders Request").
                    // SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] 
                        { Offenders0.EnmProgramStatus.Active }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Active Offenders Test Block").
                AddBlock<CreateEntMsgGetEventsRequestBlockNewEvent2Weeks>().
                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get List of Events with status=New").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlockNewEvent2Weeks").
                    FromDeepProperty< CreateEntMsgGetEventsRequestBlockNewEvent2Weeks>(x => x.GetEventsRequest).
                AddBlock<GetNewEventIDFromListOfOffendersTestBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetEventsResponse).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).
                AddBlock<Web_LoginTestBlock>().
                AddBlock<Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen>().UsePreviousBlockData().
                    SetName("GetAmountOfRowsOfNewEventsBeforeHandleOneEvent").
                    SetProperty(x => x.Title).WithValue("New").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetNewEventIDFromListOfOffendersTestBlock").
                            FromDeepProperty<GetNewEventIDFromListOfOffendersTestBlock>(x => x.EventID).
                AddBlock<Web_HandleEventFromCurrentStatusTestBlock>().
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetNewEventIDFromListOfOffendersTestBlock").
                            FromDeepProperty<GetNewEventIDFromListOfOffendersTestBlock>(x => x.EventIdString).
                    SetProperty(x => x.HandlingOption).WithValue("Remark").
                AddBlock<Web_RefreshPageBlock>().
                AddBlock<Web_CurrentStatusGetFilterCounters>().
                    SetProperty(x => x.FilterTitle).WithValue("New").
                    SetName("GetAmountOfRowsOfNewEventsAfterHandleOneEvent").
                AddBlock<Web_ValueAfterDeltaAsExcpectedAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.Expected).
                        WithValueFromBlockIndex("GetAmountOfRowsOfNewEventsAfterHandleOneEvent").
                            FromDeepProperty<Web_CurrentStatusGetFilterCounters>(x => x.FilterCounter).
                    SetProperty(x => x.BeforeDelta).
                        WithValueFromBlockIndex("GetAmountOfRowsOfNewEventsBeforeHandleOneEvent").
                            FromDeepProperty<Web_GetAmountOfRowsForSpecificSystemFilterCurrentStatusScreen>(x => x.AmountOfRows).
                    SetProperty(x => x.Delta).
                        WithValue(-1).
                    SetProperty(x => x.Message).
                        WithValue("Failed comapre events.").
            ExecuteFlow();
        }








        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_VerifyAllFilterExist_CurrentStatusScreen()
        {
            TestingFlow.

                    AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetSystemFiltersCurrentStatusScreenTestBlock>().

                AddBlock<Web_SystemFiltersAssertBlock>().
                SetProperty(x => x.ExpectedSystemFilters).WithValue(new string[] { "New", "In 3rd Party Process", "In Process", "New, In Process", "New, In Process, Auto Process", "All" }).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void SystemFilter_VerifyAllFilterExist_OffendersListScreen()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_GetSystemFiltersOffendersListScreenTestBlock>().

                AddBlock<Web_SystemFiltersAssertBlock>().
                    SetProperty(x => x.ExpectedSystemFilters).WithValue(new string[] { "Offender Summary", "Active Offenders",
                        "Pre Active Offenders", "Post Active Offenders", "Suspended Offenders", "Archived Offenders",
                        "Download Recommended", "Missed Call", "Open Violation", "Enrollment Recommended", "All" }).

            ExecuteFlow();
        }
        
        //[TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        //[TestMethod]
        //public void Grouping_GPS_FullFlow()
        //{
        //    TestingFlow.

        //        AddBlock<Web_LoginTestBlock>().

        //        AddBlock<Web_NavigateToGroupsListAddNewGroupTestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Web Navigate To Groups List And Add New Group Test Block").
        //            SetProperty(x => x.GroupType).WithValue(GroupTypeEnum.Tracker).
        //            SetProperty(x => x.NumOfOffenders).WithValue(2).

        //        AddBlock<CreateEntMsgGetGroupRequestBlock>().
        //            SetDescription("Create EntMsgGetGroupRequest Block").

        //        AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
        //            SetDescription("Get Groups Data Test Block").

        //        AddBlock<FindGroupIDByNameTestBlock>().UsePreviousBlockData().
        //            SetDescription("Find Group ID By Name Test Block").

        //        AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("FindGroupIDByNameTestBlock").
        //                    FromDeepProperty<FindGroupIDByNameTestBlock>(x => x.GroupID).
        //            SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Add Time Frame Request").
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
        //             SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("FindGroupIDByNameTestBlock").
        //                    FromDeepProperty<FindGroupIDByNameTestBlock>(x => x.GroupID).
        //            SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
        //            SetProperty(x => x.IsWeekly).WithValue(true).
        //            SetProperty(x => x.LocationID).
        //                WithValueFromBlockIndex("AddCircularZoneTestBlock").
        //                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
        //            SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

        //        AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
        //            SetDescription("Add Time Frame test block").

        //        AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
        //            SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
        //            SetProperty(x => x.OffendersListIDs).
        //                WithValueFromBlockIndex("FindGroupIDByNameTestBlock").
        //                    FromDeepProperty<FindGroupIDByNameTestBlock>(x => x.OffendersListIDs).

        //        AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Delete Time Frame Request Block").
        //            SetProperty(x => x.TimeframeID).
        //                WithValueFromBlockIndex("AddTimeFrameTestBlock").
        //                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

        //        AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Time Frame test block").

        //        AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Delete Zone Request").
        //            SetProperty(x => x.ZoneID).
        //                WithValueFromBlockIndex("AddCircularZoneTestBlock").
        //                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

        //        AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Delete Zone test block").

        //        AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
        //            SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
        //            SetProperty(x => x.OffendersListIDs).
        //                WithValueFromBlockIndex("FindGroupIDByNameTestBlock").
        //                    FromDeepProperty<FindGroupIDByNameTestBlock>(x => x.OffendersListIDs).

        //        AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Ent Msg Delete Group Request Block").

        //        AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Ent Msg Delete Group Request Block").

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_FullFlow_API()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").
                    
                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_AddNewGroup()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_NavigateToGroupsListAddNewGroupTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Web Navigate To Groups List And Add New Group Test Block").
                    SetProperty(x => x.GroupType).WithValue(GroupTypeEnum.Tracker).
                    SetProperty(x => x.NumOfOffenders).WithValue(2).

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetDescription("Create EntMsgGetGroupRequest Block").

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<FindGroupIDByNameTestBlock>().UsePreviousBlockData().
                    SetDescription("Find Group ID By Name Test Block").

                AddBlock<AddNewGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Find Group ID By Name Test Block").

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").


            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_AddZoneToGroup()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").
                    
                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_SelectGroupAndAddZoneTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Web Select Group By Name Test Block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Msg Get Zones By Entity ID Request Block").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    
                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("GetZonesByEntityIDTestBlock").
                            FromDeepProperty<GetZonesByEntityIDTestBlock>(x => x.GetZonesByEntityIDResponse.ZoneList[0].ID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("GetZonesByEntityIDTestBlock").
                            FromDeepProperty<GetZonesByEntityIDTestBlock>(x => x.GetZonesByEntityIDResponse.ZoneList[0].ID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_AddScheduleToGroup()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").


                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_SelectGroupAndAddScheduleTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Web Select Group By Name Test Block").
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDateText).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12).ToString()).
                    SetProperty(x => x.EndDateText).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).ToString()).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule test block").
                        
                AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).
                    SetProperty(x => x.TimeFrameIdList).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.TimeFrameIdList).


                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeFrameIdList).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.TimeFrameIdList).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).
                    SetProperty(x => x.TimeFrameIdList).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.TimeFrameIdList).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").


            ExecuteFlow();


        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_DeleteScheduleFromGroup()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").
                    

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_SelectGroupAndDeleteRecurringTimeframesTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_DeleteZoneFromGroup()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").


                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_SelectGroupAndDeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Web Select Group And Delete Zone Test Block").

                AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_DeleteGroup()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").


                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<VerifyExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<VerifyNotExistOffendersZoneAndScheduleFromGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Verify Offenders Zone And Schedule From Group Test Block").
                    SetProperty(x => x.OffendersListIDs).
                        WithValueFromBlockIndex("GetUnallocatedOffenderListBlock").
                            FromDeepProperty<GetUnallocatedOffenderListBlock>(x => x.OffendersListIDs).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Web Delete Group Test Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_VerifyZoneAddedToEachOffender()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").
                    

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_VerifyGroupZoneAtOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Web Select Group And Select Offender Test Block").
                    SetProperty(x => x.NumOfOffenders).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.Limit).
                    SetProperty(x => x.Expected).WithValue(true).

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<Web_VerifyGroupZoneAtOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Web Select Group And Select Offender Test Block").
                    SetProperty(x => x.NumOfOffenders).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.Limit).
                    SetProperty(x => x.Expected).WithValue(false).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void Grouping_GPS_VerifyScheduleAddedToEachOffender()
        {
            TestingFlow.

                AddBlock<GetUnallocatedOffenderListBlock>().
                    SetDescription("Create EntMsgAddGroupRequest Block").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.Limit).WithValue(2).

                AddBlock<AddGroupWithUnallocatedOffendersTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add Group With Unallocated Offenders Test Block").
                    

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddHours(9)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(11)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").
                     
                AddBlock<Web_ClearCacheTestBlock>().
                    SetDescription("Web Clear Cache Test Block").

                AddBlock<Web_VerifyGroupScheduleAtOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Web Select Group And Select Offender Test Block").
                    SetProperty(x => x.TimeFrameName).
                        WithValueFromBlockIndex("CreateMsgAddInclusionCircularZoneRequestBlock").
                            FromDeepProperty<CreateMsgAddInclusionCircularZoneRequestBlock>(x => x.ZoneName).
                    SetProperty(x => x.NumOfOffenders).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.Limit).
                    SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_LogoutTestBlock>().
                    SetDescription("Web Logout Test Block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login Test Block").

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Time Frame Request Block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

                AddBlock<Web_VerifyGroupScheduleAtOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Web Select Group And Select Offender Test Block").
                    SetProperty(x => x.TimeFrameName).
                        WithValueFromBlockIndex("CreateMsgAddInclusionCircularZoneRequestBlock").
                            FromDeepProperty<CreateMsgAddInclusionCircularZoneRequestBlock>(x => x.ZoneName).
                    SetProperty(x => x.NumOfOffenders).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.Limit).
                    SetProperty(x => x.Expected).WithValue(false).

                AddBlock<CreateEntMsgDeleteGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<DeleteGroupTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Delete Group Request Block").

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupWithUnallocatedOffendersTestBlock").
                            FromDeepProperty<AddGroupWithUnallocatedOffendersTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<DeleteGroupAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByProgramType_Monitor_1Piece()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateProgramTypeUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ProgramTypes).WithValue(new string[]{ "1 Piece GPS", "1 Piece GPS/RF" }).
                  SetProperty(x => x.FilterTitleNameToSet).WithValue("Program Type Filter").

                 AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ActualTitle).
                        WithValueFromBlockIndex("Web_CreateProgramTypeUserFilterTestBlock").
                            FromDeepProperty<Web_CreateProgramTypeUserFilterTestBlock>(x => x.FilterTitleNameThatDisplayInWeb).
                    SetProperty(x => x.ExpectedTitle).
                        WithValueFromBlockIndex("Web_CreateProgramTypeUserFilterTestBlock").
                            FromDeepProperty<Web_CreateProgramTypeUserFilterTestBlock>(x => x.FilterTitleNameToSet).
            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByOfficerName_Monitor() => TestingFlow.

                 AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get 1Piece Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get 1Piece Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get all active 1Piece offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get all active 1Piece offenders list").


                AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                    SetDescription("Gets one active 1Piece offender to get valid officer name").

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateOfficerNameUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OfficerName).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.OfficerName).
                    SetProperty(x => x.FilterTitleNameToSet).WithValue("Officer Name Filter").

                 AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ActualTitle).
                        WithValueFromBlockIndex("Web_CreateOfficerNameUserFilterTestBlock").
                            FromDeepProperty<Web_CreateOfficerNameUserFilterTestBlock>(x => x.FilterTitleNameThatDisplayInWeb).
                    SetProperty(x => x.ExpectedTitle).
                        WithValueFromBlockIndex("Web_CreateOfficerNameUserFilterTestBlock").
                            FromDeepProperty<Web_CreateOfficerNameUserFilterTestBlock>(x => x.FilterTitleNameToSet).

            ExecuteFlow();

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByOffenderName_Monitor_1Piece() => TestingFlow.

         AddBlock<CreateEntMessageGetQueueRequestBlock>().
            SetDescription("Create Get 1Piece Queue Request Block").
            SetProperty(x => x.StartRowIndex).WithValue(0).
            SetProperty(x => x.MaximumRows).WithValue(100).
            SetProperty(x => x.filterID).WithValue(46).
            SetProperty(x => x.resultCode).WithValue(EnmResultCode.REQ_PRG_1TRACK).

        AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
            SetDescription("Get 1Piece Queue Test Block").

        AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
            SetDescription("Create request to get all active 1Piece offenders list").
            SetProperty(x => x.Limit).WithValue(100).
            SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
            SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

        AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
            SetDescription("Request to get all active 1Piece offenders list").


        AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
            SetDescription("Gets one active 1Piece offender to get valid officer name").

        AddBlock<Web_LoginTestBlock>().

        AddBlock<Web_CreateOffenderNameUserFilterTestBlock>().UsePreviousBlockData().
            SetProperty(x => x.OffenderName).
                WithValueFromBlockIndex("GetOneActiveOffender").
                    FromDeepProperty<GetOneActiveOffender>(x => x.Offender.FirstName).
            SetProperty(x => x.FilterTitleNameToSet).WithValue("Offender Name Filter").

         AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
            SetProperty(x => x.ActualTitle).
                WithValueFromBlockIndex("Web_CreateOffenderNameUserFilterTestBlock").
                    FromDeepProperty<Web_CreateOffenderNameUserFilterTestBlock>(x => x.FilterTitleNameThatDisplayInWeb).
            SetProperty(x => x.ExpectedTitle).
                WithValueFromBlockIndex("Web_CreateOffenderNameUserFilterTestBlock").
                    FromDeepProperty<Web_CreateOffenderNameUserFilterTestBlock>(x => x.FilterTitleNameToSet).

    ExecuteFlow();

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByProgramType_Monitor_2Piece()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateProgramTypeUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ProgramTypes).WithValue(new string[] { "2 Piece GPS", "2 Piece GPS Passive" }).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByProgramType_Monitor_E4()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateProgramTypeUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ProgramTypes).WithValue(new string[] { "RF Curfew Dual (E4)" }).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Monitor_Sanity)]
        [TestMethod]
        public void UserFilter_FilterByProgramType_Monitor_Alcohol()
        {
            TestingFlow.

                AddBlock<Web_LoginTestBlock>().

                AddBlock<Web_CreateProgramTypeUserFilterTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ProgramTypes).WithValue(new string[] { "Alcohol VB", "Alcohol Cell VB", "Alcohol VBR", "Alcohol Cell VBR" }).

            ExecuteFlow();
        }
    }
}
