﻿using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.Interfaces;
using AssertBlocksLib.WebTestBlocks;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests
{  
    /// implelemntation of all regression tests for Opertional Reports service.
   /// </summary>
   /// 
    [TestClass]
    public class OperationalReportsTest : WebBaseUnitTest
    {
        public const string OpertationalReports_CSV_File = @"OpertationalReports.csv";
        public const string OpertationalReports_COL_NAME = "OpertationalReports";
        private static void GenerateCsvFileForOpertationalReports(string filePath)
        {
            var sbData = new System.Text.StringBuilder();

            //header
            sbData.AppendLine(OpertationalReports_COL_NAME);

            sbData.AppendLine("Offender List");
            sbData.AppendLine("Battery Charge Status");
            sbData.AppendLine("Events Details");
            sbData.AppendLine("End of Service");
            sbData.AppendLine("Open Violation");
            sbData.AppendLine("Equipment Inventory");
            sbData.AppendLine("Billing");

            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            System.IO.File.WriteAllText(filePath, sbData.ToString());
        }

        [ClassInitialize]
        public static void OpertationalReportsSvcRegressionTestsInit(TestContext testContext)
        {
            GenerateCsvFileForOpertationalReports(OpertationalReports_CSV_File);
        }


        public ITestingFlow OpertationalReportss_GetOpertationalReportsData(ITestingFlow testingFlow,string OpertationalReports)
        {
            testingFlow.
                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("yairrmc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_NavigateToOperationalReportsTestBlock>().
                          SetProperty(x => x.ReportName).WithValue(OpertationalReports).

                AddBlock<Web_FileDownloadAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.Expected).WithValue(true);


            return testingFlow;
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + OpertationalReports_CSV_File,
            "OpertationalReports#csv",
            DataAccessMethod.Sequential)]

        [TestCategory(CategoriesNames.Web_Sanity)]
        public void OperationalReportsWithoutfiltersOffendersListReportMCUser()
        {
            DataRow currentDataRow = TestContext.DataRow;
            Log.Info($" *** OpertationalReports code: {TestContext.DataRow.RowState} {currentDataRow[OpertationalReports_COL_NAME].ToString()}***");
            Console.WriteLine($" *** OpertationalReports code: {TestContext.DataRow} : {TestContext} ***");

            string OpertationalReports = currentDataRow[OpertationalReports_COL_NAME].ToString();

            OpertationalReportss_GetOpertationalReportsData(TestingFlow,
               OpertationalReports);

            TestingFlow.
                ExecuteFlow();
     
    }

}
}
