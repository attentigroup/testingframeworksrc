﻿using AssertBlocksLib.WebTestBlocks;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.WebTestBlocks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using LogicBlocksLib.ScheduleBlocks;
using TestBlocksLib.ScheduleBlocks;
using AssertBlocksLib.ScheduleAsserts;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.ProgramActions;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Common.Enum;
using WebTests.InfraStructure;

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    public class WebZonesAndScheduleTests : WebBaseUnitTest
    {
        #region Add zone
        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddPolygonInclusionZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddPolygonZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    //SetProperty(x => x.Name).WithValue("Zone1").
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.GraceTime).WithValue(5).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_AddPolygonZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddCircularExclusionZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddCircularZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    //SetProperty(x => x.Name).WithValue("Zone2").
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.GraceTime).WithValue(5).
                    SetProperty(x => x.RealRadious).WithValue(50).
                    SetProperty(x => x.BufferRadious).WithValue(10).

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddPolygonInclusionZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddPolygonZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    //SetProperty(x => x.Name).WithValue("Zone1").
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.GraceTime).WithValue(5).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_AddPolygonZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddCircularExclusionZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddCircularZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    //SetProperty(x => x.Name).WithValue("Zone2").
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.GraceTime).WithValue(5).
                    SetProperty(x => x.RealRadious).WithValue(50).
                    SetProperty(x => x.BufferRadious).WithValue(10).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddPolygonInclusionZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddPolygonZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    //SetProperty(x => x.Name).WithValue("Zone1").
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.GraceTime).WithValue(5).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_AddPolygonZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddCircularExclusionZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddCircularZoneTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderRefId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    //SetProperty(x => x.Name).WithValue("Zone2").
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.GraceTime).WithValue(5).
                    SetProperty(x => x.RealRadious).WithValue(50).
                    SetProperty(x => x.BufferRadious).WithValue(10).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Update zone
        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_UpdatePolygonInclusionZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
 
                AddBlock<Web_SelectOffenderAndEditPolygonZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Name).WithValue("Zone3").
                    SetProperty(x => x.GraceTime).WithValue(10).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_UpdateZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_UpdateCircularExclusionZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndEditCircularZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Name).WithValue("Zone3").
                    SetProperty(x => x.GraceTime).WithValue(10).
                    SetProperty(x => x.RealRadious).WithValue(20).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_UpdateZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_UpdatePolygonInclusionZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndEditPolygonZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.Name).WithValue("Zone3").
                    SetProperty(x => x.GraceTime).WithValue(10).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_UpdateZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_UpdateCircularExclusionZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndEditCircularZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Name).WithValue("Zone3").
                    SetProperty(x => x.GraceTime).WithValue(10).
                    SetProperty(x => x.RealRadious).WithValue(20).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_UpdateZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_UpdatePolygonInclusionZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndEditPolygonZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.Name).WithValue("Zone3").
                    SetProperty(x => x.GraceTime).WithValue(10).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                       SetProperty(x => x.DV).WithValue("true").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_UpdateZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_UpdateCircularExclusionZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndEditCircularZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Name).WithValue("Zone3").
                    SetProperty(x => x.GraceTime).WithValue(10).
                    SetProperty(x => x.RealRadious).WithValue(20).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_UpdateZoneAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Delete zone
        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_DeletePolygonZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                    FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock< Web_DeleteZoneAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_DeletePolygonZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                    FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_DeleteZoneAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_DeletePolygonZone_DV_Aggresor()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                    FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_DeleteZoneAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_DeleteCircularZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_DeleteZoneAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_DeleteCircularZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                 AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                    FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_DeleteZoneAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_DeleteCircularZone_DV_Aggresor()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteZoneTestBlock>().
                SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                    FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<Web_DeleteZoneAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

            ExecuteFlow();
        }
        #endregion

        #region Add Schedule
        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddDCExclusionCalendarForCircularZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddDCExclusionCalendarForCircularZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddDCExclusionCalendarForCircularZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddDCExclusionWeeklyForCircularZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddDCExclusionWeeklyForCircularZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddDCExclusionWeeklyForCircularZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionCalendarForCircularZoneFullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                //
                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(2).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<VerifyNoOverlapingTimeframeTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                //

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionCalendarForPolygonZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionCalendarForPolygonZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionCalendarForPolygonZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionWeeklyForCircularZoneFullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionWeeklyForPolygonZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionWeeklyForPolygonZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddMBIInclusionWeeklyForPolygonZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddCalenderDCOverMBOWeekly_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                     AddBlock<Web_AddScheduleTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be Out").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(10)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(10)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(14)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.RecurseEveryWeek).
                        SetProperty(x => x.StartDate).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.StartDate).
                        SetProperty(x => x.EndDate).
                        WithValueFromBlockIndex("Web_AddScheduleTestBlock").
                        FromDeepProperty<Web_AddScheduleTestBlock>(x => x.EndDate).

                AddBlock<Web_AddTimeframeTestBlock>().
                    SetProperty(x => x.OffenderId).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.StartDateText).WithValue(DateTime.Now.Date.AddDays(3).AddHours(11).ToString()).
                    SetProperty(x => x.EndDateText).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).ToString()).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_second").
                    SetProperty(x => x.EntityID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(10)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(14)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_second").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_AddCalendarScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.RecurseEveryWeek).
                        WithValueFromBlockIndex("Web_AddTimeframeTestBlock").
                        FromDeepProperty<Web_AddTimeframeTestBlock>(x => x.RecurseEveryWeek).
                        SetProperty(x => x.StartDate).
                        WithValueFromBlockIndex("Web_AddTimeframeTestBlock").
                        FromDeepProperty<Web_AddTimeframeTestBlock>(x => x.StartDate).
                        SetProperty(x => x.EndDate).
                        WithValueFromBlockIndex("Web_AddTimeframeTestBlock").
                        FromDeepProperty<Web_AddTimeframeTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }
        #endregion

        #region Update Schedule
        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateDCExclusionWeeklyForCircularZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(2)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                    //SetProperty(x => x.GetScheduleRequest).
                    //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                    //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateDCExclusionWeeklyForCircularZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(5).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateDCExclusionWeeklyForCircularZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue("Exclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(5).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateMBIInclusionCalendarForCircularZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(5).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateCalendarScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateMBIInclusionCalendarForPolygonZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(5).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateCalendarScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateMBIInclusionCalendarForPolygonZone_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                //SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                //SetProperty(x => x.RealRadius).WithValue(50).
                //SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(5).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateCalendarScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateMBIInclusionCalendarForPolygonZone_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.Name).WithValue("Work").
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                //SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                //SetProperty(x => x.RealRadius).WithValue(50).
                //SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Must Be In").
                    SetProperty(x => x.Location).WithValue("Work").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue("Inclusion").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(5).AddHours(13)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateCalendarScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateWeeklyTimeframe_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").
              
                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(true).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(11)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.ProgramType).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(2)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateWeeklyScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_UpdateCalendarTimeframe_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndUpdateTimeFrameTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Type).WithValue("Don't Care").
                    SetProperty(x => x.RecurseEveryWeek).WithValue(false).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(11)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13)).
                    SetProperty(x => x.ProgramType).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(2)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_UpdateCalendarScheduleAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.StartDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.StartDate).
                SetProperty(x => x.EndDate).
                WithValueFromBlockIndex("Web_SelectOffenderAndUpdateTimeFrameTestBlock").
                FromDeepProperty<Web_SelectOffenderAndUpdateTimeFrameTestBlock>(x => x.EndDate).

            ExecuteFlow();
        }
        #endregion

        #region Delete timeframe
        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteWeeklyTimeframe_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteWeeklyTimeframeAssertBlock>().
                SetProperty(x => x.TimeFrameID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteWeeklyTimeframe_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteWeeklyTimeframeAssertBlock>().
                SetProperty(x => x.TimeFrameID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteCalendarTimeframe_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteCalendarTimeframeAssertBlock>().
                SetProperty(x => x.TimeFrameID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteCalendarTimeframe_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteCalendarTimeframeAssertBlock>().
                SetProperty(x => x.TimeFrameID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteWeeklyTimeframe_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").
 
                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.IsE4WeeklyTimeframe).WithValue(true).
                
                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteWeeklyTimeframeAssertBlock>().
                SetProperty(x => x.TimeFrameID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteCalendarTimeframe_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteSpecificTimeframeTestBlock>().
                    SetProperty(x => x.TimeFrameID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteWeeklyTimeframeAssertBlock>().
                SetProperty(x => x.TimeFrameID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteAllWeeklyTimeframes_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(46).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteAllTimeFrameTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock< Web_DeleteAllWeeklyTimeframeAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteAllWeeklyTimeframes_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetActiveOffenderWithoutRequestInQueueTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.FilterID).WithValue(47).
                    SetProperty(x => x.ResultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteAllTimeFrameTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                    FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                            FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetActiveOffenderWithoutRequestInQueueTestBlock").
                        FromDeepProperty<GetActiveOffenderWithoutRequestInQueueTestBlock>(x => x.Offender.ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteAllWeeklyTimeframeAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_DeleteAllWeeklyTimeframes_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddHours(1)).
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndDeleteAllTimeFrameTestBlock>().
                    SetProperty(x => x.SearchTerm).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(4)).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").
                //SetProperty(x => x.GetScheduleRequest).
                //    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                //    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_DeleteAllWeeklyTimeframeAssertBlock>().

            ExecuteFlow();
        }
        #endregion


        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddZoneAndMakeSureItsDisplayedInTheLeftPanel()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                //add zone
                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                 //download
                 AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndVerifyZonesListContainsZoneTestBlock>().
                    SetProperty(x => x.SearchTerm).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                       WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Zone_AddZoneAndMakeSureItsDisplayedInTheMap()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                //add zone
                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                 //download
                 AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_SelectOffenderAndVerifyZoneIsDisplayedInMapTestBlock>().
                    SetProperty(x => x.SearchTerm).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.ZoneID).
                       WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_ZonesAndSchedule_Sanity)]
        [TestMethod]
        public void Schedule_AddTimeframeAndVerifyItsDisplayedInTheCalendar()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                     SetName("GetEquipmentListTestBlock_Beacon").
                     SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                //add zone
                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                //Add schedule
                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

            AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                 //download
                 AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(12)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(3).AddHours(13).AddMinutes(1)).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<Web_SelectOffenderAndVerifyTimeframeIsDisplayedInScheduleTestBlock>().
                    SetProperty(x => x.SearchTerm).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.TimeFrameID).
                       WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<Web_IsElementExistAsserBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }
    }
}
