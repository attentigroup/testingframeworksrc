﻿using TestingFramework.TestsInfraStructure.Implementation;
using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.AssertBlocksLib.WebTestBlock;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using AssertBlocksLib.ProgramActionsAsserts;
using TestBlocksLib.DevicesBlocks;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using LogicBlocksLib.UsersBlocks;
using TestBlocksLib.UsersBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Enum;
using AssertBlocksLib.WebTestBlocks;
using TestBlocksLib.WebTestBlocks.AddNewOffender_Location;
using LogicBlocksLib.Events;
using TestBlocksLib.EventsBlocks;
using WebTests.InfraStructure;
using TestBlocksLib.FilterssBlocks;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using System.Linq;
using System;
using AssertBlocksLib.Events;
using LogicBlocksLib.APIExtensions;
using LogicBlocksLib.Filters;
#endregion

namespace TestingFramework.UnitTests.WebTests
{

    [TestClass]
    public class WebOffendersTests : WebBaseUnitTest
    {
        Offenders0.EnmProgramStatus[] PreActiveStatus = { Offenders0.EnmProgramStatus.PreActive };
        Offenders0.EnmProgramStatus[] PostActiveStatus = { Offenders0.EnmProgramStatus.PostActive };
        Offenders0.EnmProgramStatus[] ArchivedStatus = { Offenders0.EnmProgramStatus.Archived };
        Offenders0.EnmProgramStatus[] SuspendedStatus = { Offenders0.EnmProgramStatus.Suspended };
        Offenders0.EnmProgramStatus[] EnrollmentStatus = { Offenders0.EnmProgramStatus.Enrollment };

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void LoginWithMCUser()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                    SetDescription("Create EntMsgGetUsersListRequest Block").
                    SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).
                    SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.MonitorCenter).
                    SetProperty(x => x.SortDirection).WithValue(Users0.ListSortDirection.Descending).
                    SetProperty(x => x.IncludeDeletedUsers).WithValue(false).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Users List Test Block").

                AddBlock<GetAgenciesListForMCTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Agencies List For MC Test Block").
                    SetProperty(x => x.UserID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].ID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetOffendersList Request").

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("GetOffendersList").
                    SetProperty(x => x.GetAgenciesListForMCResponse).
                        WithValueFromBlockIndex("GetAgenciesListForMCTestBlock").
                            FromPropertyName(EnumPropertyName.GetAgenciesListForMCResponse).

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Test block").
                    SetProperty(x => x.UserName).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Username).
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnOffenderFiltersTestBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("All").

                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnOffenderFiltersTestBlock").
                            FromPropertyName(EnumPropertyName.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.amountOfOffenders).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void LoginWithAMUser()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                    SetDescription("Create EntMsgGetUsersListRequest Block").
                    SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).
                    SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.AgencyManager).
                    SetProperty(x => x.SortDirection).WithValue(Users0.ListSortDirection.Descending).
                    SetProperty(x => x.IncludeDeletedUsers).WithValue(false).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Users List Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetOffendersList Request").
                    SetProperty(x => x.AgencyIDValue).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].AgencyID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("GetOffendersList").

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Test block").
                    SetProperty(x => x.UserName).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Username).
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.AgencyManager).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnOffenderFiltersTestBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("All").

                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnOffenderFiltersTestBlock").
                            FromPropertyName(EnumPropertyName.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.amountOfOffenders).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void LoginWithSAUser()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                    SetDescription("Create EntMsgGetUsersListRequest Block").
                    SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).
                    SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                    SetProperty(x => x.SortDirection).WithValue(Users0.ListSortDirection.Descending).
                    SetProperty(x => x.IncludeDeletedUsers).WithValue(false).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Users List Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetOffendersList Request").

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("GetOffendersList").

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Test block").
                    SetProperty(x => x.UserName).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Username).
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.SystemAdministrator).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnOffenderFiltersTestBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("All").

                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnOffenderFiltersTestBlock").
                            FromPropertyName(EnumPropertyName.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.amountOfOffenders).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void LoginWithOFUser()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                    SetDescription("Create EntMsgGetUsersListRequest Block").
                    SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).
                    SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.Officer).
                    SetProperty(x => x.SortDirection).WithValue(Users0.ListSortDirection.Ascending).
                    SetProperty(x => x.IncludeDeletedUsers).WithValue(false).
                    SetProperty(x => x.SortField).WithValue(Users0.EnmUsersSortOptions.ID).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Users List Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetOffendersList Request").
                    SetProperty(x => x.OfficerIDValue).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].ID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("GetOffendersList").

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Test block").
                    SetProperty(x => x.UserName).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Username).
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenClickOnOffenderFiltersTestBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("All").

                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks Filter section in Offenders screen").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnOffenderFiltersTestBlock").
                            FromPropertyName(EnumPropertyName.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.amountOfOffenders).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendEOSOnePieceRF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).
                
                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).


                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(false).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

            //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
            //     SetDescription("Is Active Assert Block").
            //     SetProperty(x => x.Expected).WithValue(false).

            AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

            AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

            AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                 SetDescription("Is Active Assert Block").
                 SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void AddTrailPoint1PieceGPS_RF()
        {
            TestingFlow.

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<IsActiveAssertBlock>().
                      SetDescription("Is active assert block").
                      SetProperty(x => x.Expected).WithValue(true).

                  AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                  AddBlock<Web_OffenderLocationTestBlock>().SetName("LocatinBefore").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetOffendersTestBlock").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                  AddBlock<Web_OffenderLocationTestBlock>().SetName("LocationAfter").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).


                  AddBlock<Web_OffenderLocationAssertBlock>().UsePreviousBlockData().
                       SetDescription("Offender details assert block").
                       SetProperty(x => x.OffenderDetailsBefore).WithValueFromBlockIndex("LocatinBefore").
                       FromDeepProperty<Web_OffenderLocationTestBlock>(x => x.OffenderLocation).
                       SetProperty(x => x.OffenderDetailsAfter).WithValueFromBlockIndex("LocationAfter").
                       FromDeepProperty<Web_OffenderLocationTestBlock>(x => x.OffenderLocation).
                       SetProperty(x => x.IsLastTrailPointDisplayedOnMap).WithValue(true).

                   ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void PlayTrail1PieceGPS_RF()
        {
            TestingFlow.

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<IsActiveAssertBlock>().
                  SetDescription("Is active assert block").
                  SetProperty(x => x.Expected).WithValue(true).

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetOffendersTestBlock").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                 AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                 AddBlock<Web_PlayTrailTestBlock>().SetName("LocatinBefore").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                   ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void PlayTrail2Piece()
        {
            TestingFlow.

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<IsActiveAssertBlock>().
                  SetDescription("Is active assert block").
                  SetProperty(x => x.Expected).WithValue(true).

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetOffendersTestBlock").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").


                 AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                 AddBlock<Web_PlayTrailTestBlock>().SetName("LocatinBefore").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                   ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void AddTrailPoint2Piece()
        {
            TestingFlow.

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<IsActiveAssertBlock>().
                  SetDescription("Is active assert block").
                  SetProperty(x => x.Expected).WithValue(true).

                  AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                  AddBlock<Web_OffenderLocationTestBlock>().SetName("LocatinBefore").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetOffendersTestBlock").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                  AddBlock<Web_OffenderLocationTestBlock>().SetName("LocationAfter").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                  AddBlock<Web_OffenderLocationAssertBlock>().
                       SetDescription("Offender details assert block").
                       SetProperty(x => x.OffenderDetailsBefore).WithValueFromBlockIndex("LocatinBefore").
                       FromDeepProperty<Web_OffenderLocationTestBlock>(x => x.OffenderLocation).
                       SetProperty(x => x.OffenderDetailsAfter).WithValueFromBlockIndex("LocationAfter").
                       FromDeepProperty<Web_OffenderLocationTestBlock>(x => x.OffenderLocation).
                       SetProperty(x => x.IsLastTrailPointDisplayedOnMap).WithValue(true).

                   ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void TrailReport1PieceGPS_RF()
        {
            TestingFlow.

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                       SetProperty(x => x.Limit).WithValue(1).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                       SetDescription("Get offender test block").

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetOffendersTestBlock").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                 AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                       AddBlock<Web_TrailReportsTestBlock>().
                       SetDescription("Web get trail report test block").
                       SetProperty(x => x.RefID).WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                 AddBlock<Web_TrailReportAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void PrintTrail1PieceGPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                   SetDescription("Get offender logic block").
                   SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                   SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                   SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender test block").

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetOffendersTestBlock").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Web Login test block").

                AddBlock<Web_LocationPrintButtonTestBlock>().
                    SetDescription("Web get trail report test block").
                    SetProperty(x => x.RefID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                 AddBlock<Web_TrailReportAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void PlayTrailDVPairs()
        {
            TestingFlow.

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.LastTimeStamp).WithValue(null).
                       SetProperty(x => x.Limit).WithValue(50).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().SetName("Aggressor").
                       SetDescription("Get offender test block").

                AddBlock<GetActiveDVPairOffendersTestBlock>().UsePreviousBlockData().

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock").
                               FromDeepProperty<GetActiveDVPairOffendersTestBlock>(x => x.Aggressor.EquipmentInfo.ReceiverSN).
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                       SetProperty(x => x.DV).WithValue("true").

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(null).
                       SetProperty(x => x.ProgramConcept).WithValue(null).
                       SetProperty(x => x.LastTimeStamp).WithValue(0ul).
                       SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock").
                       FromDeepProperty<GetActiveDVPairOffendersTestBlock>(x => x.Aggressor.RelatedOffenderID).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().SetName("Victim").
                       SetDescription("Get offender test block").

                  AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("Victim").
                               FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                  AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                  AddBlock<Web_PlayTrailTestBlock>().SetName("Web_PlayTrailTestBlock_Aggressor").
                       SetDescription("Web Get offender location").
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock").
                       FromDeepProperty<GetActiveDVPairOffendersTestBlock>(x => x.Aggressor.EquipmentInfo.ReceiverSN).

                   ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void AddTrailPointDVPairs()
        {
            TestingFlow.
                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                       SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                       SetProperty(x => x.LastTimeStamp).WithValue(null).
                       SetProperty(x => x.Limit).WithValue(50).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().SetName("Aggressor").
                       SetDescription("Get offender test block").

                  AddBlock<GetActiveDVPairOffendersTestBlock>().UsePreviousBlockData().

                      AddBlock<IsActiveAssertBlock>().
                  SetDescription("Is active assert block").
                  SetProperty(x => x.Expected).WithValue(true).

                   AddBlock<Web_LoginTestBlock>().
                       SetDescription("Web Login test block").

                  AddBlock<Web_OffenderLocationTestBlock>().SetName("Web_Aggressor").
                       SetDescription("Web Get Aggressor location").
                       //SetProperty(x => x.RefID).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock"/*"Aggressor"*/).
                       //        FromDeepProperty<GetActiveDVPairOffendersTestBlock/*GetOffendersTestBlock*/>(x => x.Aggressor.RefID).
                        SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock"/*"Aggressor"*/).
                               FromDeepProperty<GetActiveDVPairOffendersTestBlock/*GetOffendersTestBlock*/>(x => x.Aggressor.EquipmentInfo.ReceiverSN).
                        SetProperty(x => x.FilterTitle).WithValue("Active Offenders").

                    AddBlock<RunXTSimulatorTestBlock>().
                       SetDescription("Run XT simulator for trail").
                       SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock"/*"Aggressor"*/).
                               FromDeepProperty<GetActiveDVPairOffendersTestBlock/*GetOffendersTestBlock*/>(x => x.Aggressor.EquipmentInfo.ReceiverSN/*GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN*/).
                               SetProperty(x => x.DV).WithValue("true").
                               SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                       SetDescription("Get offender logic block").
                       SetProperty(x => x.ProgramStatus).WithValue(null).
                       SetProperty(x => x.ProgramConcept).WithValue(null).
                       SetProperty(x => x.LastTimeStamp).WithValue(0ul).
                       SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock"/*"Aggressor"*/).
                               FromDeepProperty<GetActiveDVPairOffendersTestBlock/*GetOffendersTestBlock*/>(x => x.Aggressor.RelatedOffenderID).

                  AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().SetName("Victim").
                       SetDescription("Get offender test block").

                  //AddBlock<RunXTSimulatorTestBlock>().
                  //     SetDescription("Run XT simulator for trail").
                  //     SetProperty(x => x.EquipmentSN).WithValueFromBlockIndex("Victim").
                  //             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                  AddBlock<Web_OffenderLocationTestBlock>().SetName("Web_Victim").
                       SetDescription("Web Get Victim location").
                       //SetProperty(x => x.RefID).WithValueFromBlockIndex("Victim").
                       //FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                       SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetActiveDVPairOffendersTestBlock"/*"Aggressor"*/).
                               FromDeepProperty<GetActiveDVPairOffendersTestBlock/*GetOffendersTestBlock*/>(x => x.Victim.EquipmentInfo.ReceiverSN).

                        AddBlock<Web_OffenderLocationAssertBlock>().
                       SetDescription("Offender details assert block").
                       SetProperty(x => x.OffenderDetailsBefore).WithValueFromBlockIndex("Web_Aggressor").
                       FromDeepProperty<Web_OffenderLocationTestBlock>(x => x.OffenderLocation).
                       SetProperty(x => x.OffenderDetailsAfter).WithValueFromBlockIndex("Web_Victim").
                       FromDeepProperty<Web_OffenderLocationTestBlock>(x => x.OffenderLocation).
                       SetProperty(x => x.IsLastTrailPointDisplayedOnMap).WithValue(true).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendEOSOnePieceRFNoReceiver()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211112").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                            SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                             SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(true).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendEOSTwoPieceNoReceiver()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>()./*UsePreviousBlockData().*/ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                            SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                             SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(true).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendEOSTwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(false).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [Ignore]
        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendEOSE4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Receiver").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("333333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("ReceiverResponse").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("Transmitter").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Transmitter_860).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E33333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("TransmitterResponse").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("ReceiverResponse").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).
                    SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    SetProperty(x => x.VoicePhoneNumber).WithValue("4111111").
                    SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    SetProperty(x => x.DataPhoneNumber).WithValue("4111111").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.FirstName).WithValue("AutoEOS_E4").
                    SetProperty(x => x.RefID).WithValue("AutoEOS_E4").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("Receiver").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Transmitter).
                        WithValueFromBlockIndex("Transmitter").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.ContactPhoneNumber).WithValue(null).
                    SetProperty(x => x.ContactPrefixPhone).WithValue(null).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("Transmitter").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(false).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("Transmitter").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendEOSDVNoReceiver()
        {
            TestingFlow.

            #region add equipment for victim
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("VictimEquipmentRequest").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("VictimEquipmentResponse").
            #endregion

            #region add cellular data for victim
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("VictimEquipmentResponse").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
            #endregion

            #region add offender victim
                AddBlock<CreateEntMsgAddOffenderRequestBlock>()./*UsePreviousBlockData().*/ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetName("Victim").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("VictimEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                    SetName("AddVictim").
            #endregion

            #region send download victim
                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddVictim").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("VictimEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                             SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
            #endregion

            #region verify victim is active
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetName("CreateGetVictim").
                     SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                     SetProperty(x => x.LastTimeStamp).WithValue((ulong)0).
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddVictim").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().
                     SetDescription("Get Offenders Test Block").
                     SetName("GetVictim").
                     SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateGetVictim").
                            FromPropertyName(EnumPropertyName.GetOffendersListRequest).

                //AddBlock<IsActiveAssertBlock>().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).
                //     SetProperty(x => x.GetOffendersResponse).
                //        WithValueFromBlockIndex("GetVictim").
                //            FromPropertyName(EnumPropertyName.GetOffendersResponse).

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).
            #endregion

            #region add equipment for aggressor
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("AggressorEquipmentRequest").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AggressorEquipmentResponse").
            #endregion

            #region add cellular data for aggressor
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AggressorEquipmentResponse").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
            #endregion

            #region add offender aggressor
               AddBlock<CreateEntMsgAddOffenderRequestBlock>()./*UsePreviousBlockData().*/ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetName("Aggressor").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("AggressorEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.RelatedOffenderID).
                        WithValueFromBlockIndex("AddVictim").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                    SetName("AddAggressor").
            #endregion

            #region send download aggressor
                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressor").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("AggressorEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                             SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
            #endregion

            #region verify aggressor is active
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateGetAggressor").
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.LastTimeStamp).WithValue((ulong)0).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddAggressor").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").
                     SetName("GetAggressor").
                     SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateGetAggressor").
                            FromPropertyName(EnumPropertyName.GetOffendersListRequest).

                //AddBlock<IsActiveAssertBlock>().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).
                //     SetProperty(x => x.GetOffendersResponse).
                //        WithValueFromBlockIndex("GetAggressor").
                //            FromPropertyName(EnumPropertyName.GetOffendersResponse).

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).
            #endregion

            #region web login
                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").
            #endregion

            #region web EOS victim
                AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("Victim").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(true).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

              AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("VictimEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
            #endregion

            #region web EOS aggressor
               AddBlock<Web_OffendersListScreenSearchOffenderAndClickEOSTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("Aggressor").
                            FromPropertyName(EnumPropertyName.FirstName).
                    SetProperty(x => x.noReceiver).WithValue(true).
                    SetProperty(x => x.isAutoEOS).WithValue(true).
                    SetProperty(x => x.isCurfewViolations).WithValue(false).
                    SetProperty(x => x.isSuccessful).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("AggressorEquipmentRequest").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
            #endregion

            #region verify victim is not active
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetName("CreateGetVictimFin").
                     SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                     SetProperty(x => x.LastTimeStamp).WithValue((ulong)0).
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddVictim").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().
                     SetDescription("Get Offenders Test Block").
                     SetName("GetVictimFin").
                     SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateGetVictimFin").
                            FromPropertyName(EnumPropertyName.GetOffendersListRequest).

                AddBlock<IsActiveAssertBlock>().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).
                     SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetVictimFin").
                            FromPropertyName(EnumPropertyName.GetOffendersResponse).
            #endregion

            #region verify aggressor is not active
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetName("CreateGetAggressorFin").
                     SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                     SetProperty(x => x.LastTimeStamp).WithValue((ulong)0).
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddAggressor").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().
                     SetDescription("Get Offenders Test Block").
                     SetName("GetAggressorFin").
                     SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateGetAggressorFin").
                            FromPropertyName(EnumPropertyName.GetOffendersListRequest).

                AddBlock<IsActiveAssertBlock>().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).
                     SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetAggressorFin").
                            FromPropertyName(EnumPropertyName.GetOffendersResponse).
            #endregion

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SuspendProgramOnePieceRF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>()./*UsePreviousBlockData().*/ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickSuspendProgramTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsSuspendAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Suspend Assert Block").
                     SetProperty(x => x.Expected).WithValue(Offenders0.EnmProgramStatus.Suspended).

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickUnsuspendProgramTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Suspended Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SuspendProgramTwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>()./*UsePreviousBlockData().*/ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                            SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().
                SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickSuspendProgramTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsSuspendAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Suspend Assert Block").
                     SetProperty(x => x.Expected).WithValue(Offenders0.EnmProgramStatus.Suspended).

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickUnsuspendProgramTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Suspended Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(false).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                            SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendUploadOnePiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickUploadTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendUploadRequestAssertBlock>().
                    SetDescription("Send Upload Request Assert Block").
                    SetProperty(x => x.Expected).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).
                    //SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.LastTimeStamp).WithValue(null).
                    //SetProperty(x => x.LastEventID).WithValue(null).
                    //SetProperty(x => x.AgencyID).WithValue(null).
                    //SetProperty(x => x.OfficerID).WithValue(null).
                    //SetProperty(x => x.OffenderRefID).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_CurrentStatusSearchEventTestBlock>().UsePreviousBlockData().
                    SetDescription("Web_Search Event Test Block").
                    SetProperty(x => x.DoNavigateToCurrentStatus).WithValue(true).
                    SetProperty(x => x.GetEventsResponse).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<Web_SearchEventsOnCurrentStatusAssertBlock>().UsePreviousBlockData().
                    SetDescription("Web_Search Event Assert Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendUploadRequestAssertBlock>().
                    SetDescription("Send Upload Request Assert Block").
                    SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SendUploadTwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                            SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_OffendersListScreenSearchOffenderAndClickUploadTestBlock>().UsePreviousBlockData().
                    SetDescription("Search for offender and click on him in Offenders screen").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.Title).WithValue("Active Offenders").
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromPropertyName(EnumPropertyName.FirstName).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK 46-> 1TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendUploadRequestAssertBlock>().
                    SetDescription("Send Upload Request Assert Block").
                    SetProperty(x => x.Expected).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<Web_CurrentStatusSearchEventTestBlock>().UsePreviousBlockData().
                    SetDescription("Web_Search Event Test Block").
                    SetProperty(x => x.DoNavigateToCurrentStatus).WithValue(true).
                    SetProperty(x => x.GetEventsResponse).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<Web_SearchEventsOnCurrentStatusAssertBlock>().
                    SetDescription("Web_Search Event Assert Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendUploadRequestAssertBlock>().
                    SetDescription("Send Upload Request Assert Block").
                    SetProperty(x => x.Expected).WithValue(false).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyPreActiveScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Pre Active Offenders").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Pre Active Offenders").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SystemFilter_VerifyOffenderSummaryScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Offender Summary").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Offender Summary").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyActiveOffendersScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Active Offenders").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Active Offenders").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyPostActiveOffendersScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Post Active Offenders").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Post Active Offenders").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifySuspendedOffendersScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Suspended Offenders").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Suspended Offenders").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyArchivedOffendersScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Archived Offenders").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Archived Offenders").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyDownloadRecommendedScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Download Recommended").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Download Recommended").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyMissedCallScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Missed Call").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Missed Call").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyOpenViolationScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Open Violation").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Open Violation").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyEnrollmentRecommendedScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("Enrollment Recommended").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("Enrollment Recommended").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void VerifyAllScreenTitle()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndVerifyScreenNameTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.Title).WithValue("All").

            AddBlock<FilterTitleAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.ExpectedTitle).WithValue("All").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckAllScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("All").

            AddBlock<GetOffendersCountersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckActiveScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Active Offenders").

            AddBlock<GetOffendersCountersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.filterTitle).WithValue("Active").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckOpenViolationScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Open Violation").

            AddBlock<GetOffendersCountersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.filterTitle).WithValue("In Violation").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckMissedCallScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Missed Call").

            AddBlock<GetOffendersCountersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.filterTitle).WithValue("Missed Call").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckDownloadRecommendedScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Download Recommended").

            AddBlock<GetOffendersCountersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                SetProperty(x => x.filterTitle).WithValue("Download Recommended").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckPreActiveScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Pre Active Offenders").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(PreActiveStatus).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
                
            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckPostActiveScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Post Active Offenders").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(PostActiveStatus).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").
               
            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckArchivedScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Archived Offenders").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(ArchivedStatus).
                    SetProperty(x => x.IncludeArchived).WithValue(true).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void CheckSuspendedScreenOffendersAmount()
        {
            TestingFlow.

            AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

            AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

            AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                SetDescription("Click On Filter And Verify Screen Name").
                SetProperty(x => x.sectionNumber).WithValue(1).
                SetProperty(x => x.filterTitle).WithValue("Suspended Offenders").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(SuspendedStatus).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

            AddBlock<OffendersAmountByFilterAssertBlock>().UsePreviousBlockData().
                SetDescription("Filter Title Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
          public void CheckEnrollmentScreenOffendersAmount()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AllowVoiceTests).WithValue(true).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Voice_Verification).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().
                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                    SetDescription("Click On Filter And Verify Screen Name").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.filterTitle).WithValue("Enrollment Recommended").
                AddBlock<CreateEntMsgGetDataRequestBlock>().
                SetDescription("Create GetData Request").
                SetProperty(x => x.ScreenName).
                    WithValue(EnumFiltersScreenNameOptions.offender).  
                AddBlock<FiltersSvcGetDataTestBlock>().UsePreviousBlockData().
                    SetDescription("GetData Test Block").
                AddBlock<GetOffenderSystemFilterIDAccordingToFilterName>().UsePreviousBlockData().
                    SetDescription("GetData Test Block").
                    SetProperty(x => x.FilterName).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock").
                            FromDeepProperty<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>(x => x.filterTitle).
                AddBlock<GetOffendersByFilterIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders with Enrollment Recommended Test Block").
                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks number of offender with enrollment recommended in WEB is eqaul to API").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock").
                            FromDeepProperty<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>(x => x.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetOffendersByFilterIDTestBlock").
                            FromDeepProperty<GetOffendersByFilterIDTestBlock>(x => x.AmountOfRows).
                    SetProperty(x => x.Message).
                        WithValue("Fail to compare between amount of offenders with enrollment recommended in WEB against to the API").
            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void SystemFilter_CheckOffenderSummaryScreenOffendersAmount()
        {
            TestingFlow.
                AddBlock<Web_LoginTestBlock>().
                    SetDescription("Login").
                    SetProperty(x => x.UserName).WithValue("emsprc").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                AddBlock<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>().UsePreviousBlockData().
                    SetDescription("Click On Filter And Verify Screen Name").
                    SetProperty(x => x.sectionNumber).WithValue(1).
                    SetProperty(x => x.filterTitle).WithValue("Offender Summary").
                AddBlock<CreateEntMsgGetDataRequestBlock>().
                    SetDescription("Create GetData Request").
                    SetProperty(x => x.ScreenName).
                        WithValue(EnumFiltersScreenNameOptions.offender).
                AddBlock<FiltersSvcGetDataTestBlock>().UsePreviousBlockData().
                    SetDescription("GetData Test Block").
                AddBlock<GetOffenderSystemFilterIDAccordingToFilterName>().UsePreviousBlockData().
                    SetDescription("GetData Test Block").
                    SetProperty(x => x.FilterName).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock").
                            FromDeepProperty<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>(x => x.filterTitle).
                AddBlock<GetOffendersByFilterIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders with Enrollment Recommended Test Block").
                AddBlock<Web_AmountOfRowsAssertBlock>().
                    SetDescription("Checks number of offender with enrollment recommended in WEB is eqaul to API").
                    SetProperty(x => x.WebAmountOfRows).
                        WithValueFromBlockIndex("Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock").
                            FromDeepProperty<Web_OffendersListScreenClickOnFilterAndGetAmountOfRowsTestBlock>(x => x.AmountOfRows).
                    SetProperty(x => x.APIAmountOfRows).
                        WithValueFromBlockIndex("GetOffendersByFilterIDTestBlock").
                            FromDeepProperty<GetOffendersByFilterIDTestBlock>(x => x.AmountOfRows).
                    SetProperty(x => x.Message).
                        WithValue("Fail to compare between amount of offenders with enrollment recommended in WEB against to the API").
            ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.Web_Offenders_Sanity)]
        [TestMethod]
        public void UploadEventsByRequestE4RF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetDescription("Create Add Offender request logic block").
                SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).//Which program type here? Is ReceiverSN guarenteed?
                SetProperty(x => x.Limit).WithValue(1).

                AddBlock<GetOffendersTestBlock>().
                SetDescription("Get Offender test block").

                AddBlock<Web_LoginTestBlock>().
                SetDescription("Login test block").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                AddBlock<Web_NavigateToTabTestBlock>().
                SetProperty(x => x.PrimaryTabLocatorString).WithValue("OffenderDetails").

                AddBlock<Web_SendUploadTestBlock>().
                SetDescription("Send Upload test block").

                AddBlock<Web_QueueTestBlock>().
                SetDescription("Queue test block").
                SetProperty(x => x.FilterSectionNumber).WithValue(1).
                SetProperty(x => x.FilterTitle).WithValue("RF Curfew Dual (E4)").

                AddBlock<Web_QueueRFAssertBlock>().
                SetDescription("Web Queue assert block").
                SetProperty(x => x.IsExpectedInQueue).WithValue(true).

                SetProperty(x => x.OffenderRefId).WithValueFromBlockIndex("Web_SelectOffenderTestBlock").
                FromDeepProperty<Web_SelectOffenderTestBlock>(x => x.SearchTerm).

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetDescription("Get equipment list request logic block").
                SetProperty(x => x.EquipmentType).WithValue(Equipment0.EnmEquipmentType.Receiver).
                SetProperty(x => x.ProgramType).WithValue(Equipment0.EnmProgramType.E4_Dual).
                SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList.First().ID).
                SetProperty(x => x.EquipmentModels).WithValue(new Equipment0.EnmEquipmentModel[] { Equipment0.EnmEquipmentModel.RF_Curfew_Dual_E4 }).
                //GetOffendersResponse

                AddBlock<GetEquipmentListTestBlock>().
                SetDescription("Get equipment test block").

                //Add E4 Simulator block

                AddBlock<Web_QueueTestBlock>().
                SetDescription("Queue test block").
                SetProperty(x => x.FilterSectionNumber).WithValue(1).
                SetProperty(x => x.FilterTitle).WithValue("RF Curfew Dual (E4)").

                AddBlock<Web_QueueRFAssertBlock>().
                SetDescription("Web Queue assert block").
                SetProperty(x => x.IsExpectedInQueue).WithValue(false).
                SetProperty(x => x.OffenderRefId).WithValueFromBlockIndex("Web_SelectOffenderTestBlock").
                FromDeepProperty<Web_SelectOffenderTestBlock>(x => x.SearchTerm).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetDescription("Create Get Event Request logic block").
                SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).
                SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMinutes(-5)).
                

                AddBlock<GetEventsTestBlock>().
                SetDescription("Get events test block").

                AddBlock<GetEventsAssertBlock>().
            //compare ReceiverSN with QueueList from Web_QueueTestBlock

            ExecuteFlow();
        }
    }
}
