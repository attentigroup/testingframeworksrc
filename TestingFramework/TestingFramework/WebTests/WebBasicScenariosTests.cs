﻿using Common.Categories;
using LogicBlocksLib.APIExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.APIExtensions;
using TestBlocksLib.TestingFrameworkBlocks.TestBlocksLib.WebTestBlocks;
using TestBlocksLib.WebTestBlocks;
using TestingFramework.AssertBlocksLib.WebTestBlock;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions.Security;
using TestingFramework.TestsInfraStructure.Implementation;
using WebTests.InfraStructure.Entities;


#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using WebTests.InfraStructure;
using WebTests.SeleniumWrapper;
using TestBlocksLib.WebTestBlocks.AddNewOffender_Location;
using TestBlocksLib.EventsBlocks;
using LogicBlocksLib.Events;
using events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using System.Linq;
using System;
using AssertBlocksLib.WebTestBlocks;
#endregion

namespace TestingFramework.UnitTests
{
    /// <summary>
    /// Summary description for WebTestBasic
    /// </summary>
    [TestClass]
    public class WebBasicScenariosTests : WebBaseUnitTest
    {
        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void WebUIDemo()
        {
            TestingFlow.
                AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                SetProperty(x => x.UserName).WithValue("emsprc").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                AddBlock<GetVersionTestBlock>().
                SetProperty(x => x.ExpectedVersions).WithValue(new AppVersions() { WebVersion = "12.0.0.81", DatabaseVersion = "12.0.0.43", DatabaseServer = "vmSolaries_Env_13" }).

                AddBlock<Web_VersionAssertBlock>().
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SecurityConfigToggleOfficerFilterSection()
        {
            TestingFlow.

                AddBlock<GetGUISecurityDataLogicBlock>().
                    SetDescription("Get GUI security data of user").
                    SetProperty(x => x.FirstName).WithValue("AlexSE").
                    SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                    SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Test block").

                AddBlock<SetGUISecurityDataLogicBlock>().
                    SetDescription("Set GUI security logic block").
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ResourceID).WithValue(new string[] { "Monitor_EventsGrid_Section_Filter" }).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache").

                AddBlock<Web_MonitorScreenFilterSectionVisibleTestBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Monitor screen").

                AddBlock<Web_ElementVisibleAssertBlock>().
                    SetDescription("Verify if Filter section is visible").
                    SetProperty(x => x.IsExpectedVisible).WithValue(false).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                    SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache before logout").

                AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear cache and Log out").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                    SetDescription("Login").

                AddBlock<Web_MonitorScreenFilterSectionVisibleTestBlock>().UsePreviousBlockData().
                    SetDescription("Checks Filter section in Monitor screen").

                AddBlock<Web_ElementVisibleAssertBlock>().
                    SetDescription("Verify if Filter section is visible").
                    SetProperty(x => x.IsExpectedVisible).WithValue(true).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SecurityConfigToggleOfficerEventGridHeaders()
        {
            TestingFlow.
                AddBlock<GetGUISecurityDataLogicBlock>().
                SetDescription("Get GUI security data of user").
                SetProperty(x => x.FirstName).WithValue("AlexSE").
                SetProperty(x => x.Password).WithValue("Q1w2e3r4").
                SetProperty(x => x.UserType).WithValue(EnmUserType.Officer).

                AddBlock<GetGuiSecurityDataTestBlock>().UsePreviousBlockData().
                SetDescription("Test block").

                AddBlock<SetGUISecurityDataLogicBlock>().
                SetDescription("Set GUI security logic block").
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Invisible).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ResourceID).WithValue(new string[] { "EventGrid_List_GridHeader" }).

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login").

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache").

                AddBlock<Web_MonitorScreenEventListHeaderVisibleTestBlock>().UsePreviousBlockData().
                SetDescription("Checks Filter section in Monitor screen").

                AddBlock<Web_ElementVisibleAssertBlock>().
                SetDescription("Verify if Filter section is visible").
                SetProperty(x => x.IsExpectedVisible).WithValue(false).

                AddBlock<SetGUISecurityDataTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ModifyOption).WithValue(EnmModifyOption.ShowHideOnly).
                SetProperty(x => x.ModifyStatus).WithValue(EnmModify.Visible).

                AddBlock<Web_ClearCacheTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache before logout").

                AddBlock<Web_LogoutTestBlock>().UsePreviousBlockData().
                SetDescription("Clear cache and Log out").

                AddBlock<Web_LoginTestBlock>().UsePreviousBlockData().
                SetDescription("Login").

                AddBlock<Web_MonitorScreenFilterSectionVisibleTestBlock>().UsePreviousBlockData().
                SetDescription("Checks Filter section in Monitor screen").

                AddBlock<Web_ElementVisibleAssertBlock>().
                SetDescription("Verify if Filter section is visible").
                SetProperty(x => x.IsExpectedVisible).WithValue(true).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void EquipmentFlow()
        {
            TestingFlow.
                AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").


                AddBlock<Web_OffenderLocationTestBlock>().
                SetProperty(x => x.RefID).WithValue("8231").

                //AddBlock<Web_AddEquipmentTestBlock>().
                //SetProperty(x => x.FileName).WithValue("2Piece V6 Not Encypted.txt").
                //SetDescription("Add equipment").
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SystemFlow()
        {
            TestingFlow.
                AddBlock<Web_LoginTestBlock>().
                SetDescription("Login").
                AddBlock<Web_FlowDebugger>().
                SetDescription("Add new offender").

                //AddBlock<Web_AddEquipmentTestBlock>().
                //SetProperty(x => x.FileName).WithValue("2Piece V6 Not Encypted.txt").
                //SetDescription("Add equipment").
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void HandleNewEvent()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetDescription("Create Get events request logic block").
                SetProperty(x => x.Status).WithValue(events0.EnmHandlingStatusType.New).
                SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-14)).
                SetProperty(x => x.Limit).WithValue(200).
                AddBlock<GetEventsTestBlock>().
                SetDescription("Get events test block").

                AddBlock<GetEventWithOffenderTestBlock>().
                SetDescription("Get Offender-related event").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetDescription("Get offender request logic block").
                SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.OffenderId).
                
                AddBlock<GetOffendersTestBlock>().
                SetDescription("Get offender test block").

                AddBlock<Web_LoginTestBlock>().
                SetDescription("Login to web").

                AddBlock<Web_SelectOffenderTestBlock>().
                SetDescription("Web Get offender test block").
                SetProperty(x => x.SearchTerm).WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList.First().RefID).
                
                AddBlock<Web_CurrentStatusGetFilterCounters>().SetName("CounterBefore").
                SetProperty(x => x.FilterTitle).WithValue("New").

                AddBlock<Web_HandleEventFromCurrentStatusTestBlock>().
                SetProperty(x => x.EventID).WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<Web_CurrentStatusGetFilterCounters>().SetName("CounterAfter").
                
                AddBlock<Web_GetFilterCountersAssertBlock>().
                SetProperty(x => x.FilterCounterBefore).WithValueFromBlockIndex("CounterBefore").
                FromDeepProperty<Web_CurrentStatusGetFilterCounters>(x => x.FilterCounter).
                SetProperty(x => x.FilterCounterAfter).WithValueFromBlockIndex("CounterAfter").
                FromDeepProperty<Web_CurrentStatusGetFilterCounters>(x => x.FilterCounter).

            ExecuteFlow();
        }


    }
}
