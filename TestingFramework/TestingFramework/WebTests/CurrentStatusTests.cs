﻿using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestBlocksLib.WebTestBlocks;
using LogicBlocksLib.Events;
using TestBlocksLib.EventsBlocks;
using AssertBlocksLib.WebTestBlocks;

#region API Offenders refs
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.DevicesBlocks;
using Common.Enum;
using WebTests.InfraStructure;


#endregion

namespace TestingFramework.UnitTests.WebTests
{
    [TestClass]
    public class CurrentStatusTests : WebBaseUnitTest
    {

        [TestCategory(CategoriesNames.Web_Sanity)]
        [TestMethod]
        public void SearchEventFromCurrentStatusScreen()
        {
            TestingFlow.

                 AddBlock<Web_LoginTestBlock>().
                          SetDescription("Login").
                            SetProperty(x => x.UserName).WithValue("emsprc").
                            SetProperty(x => x.Password).WithValue("Q1w2e3r4").

                           AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                               SetDescription("create random EntMsgAddEquipmentRequest type").
                               SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                               SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                               SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                               SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                               SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                               SetProperty(x => x.EquipmentSerialNumber).WithValue("34215758").

                           AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                               SetDescription("Add equipment test block").

                           AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                               SetDescription("Create EntMsgAddCellularDataRequest Type").
                               SetProperty(x => x.EquipmentID).
                                   WithValueFromBlockIndex("AddEquipmentTestBlock").
                                       FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                               SetProperty(x => x.ProviderID).WithValue(2).
                               SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                               SetProperty(x => x.VoicePhoneNumber).WithValue("114132").
                               SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                               SetProperty(x => x.DataPhoneNumber).WithValue("2224234").

                           AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                               SetDescription("Add Cellular Data test block").

                           AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                               SetDescription("Create Add Offender Request").
                               SetProperty(x => x.FirstName).WithValue("Search").
                               SetProperty(x => x.RefID).WithValue("Auto_Search").
                               SetProperty(x => x.CityID).WithValue("TLV").
                               SetProperty(x => x.Language).WithValue("ENG").
                               SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                               SetProperty(x => x.StateID).WithValue("IL").
                               SetProperty(x => x.Receiver).
                                   WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                                       FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                           AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                               SetDescription("Add Offender test block").

                           AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                                SetDescription("Create Send Download Request").
                                SetProperty(x => x.Immediate).WithValue(true).
                                SetProperty(x => x.OffenderID).
                                    WithValueFromBlockIndex("AddOffenderTestBlock").
                                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                            AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                                SetDescription("Send Download Request Test Block").

                            AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                                SetDescription("Run XT Simulator Test Block").
                                SetProperty(x => x.EquipmentSN).
                                    WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                                        FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                             AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                                   SetDescription("Create Get Offenders Request").
                             SetProperty(x => x.OffenderID).
                                 WithValueFromBlockIndex("AddOffenderTestBlock").
                                     FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                             AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                                  SetDescription("Get Offenders Test Block").

                             AddBlock<CreateEntMsgGetEventsRequestBlock>().
                                    SetDescription("Create get events request").
                                    SetProperty(x => x.Limit).WithValue(1).
                                    SetProperty(x => x.ProgramType).WithValue(null).
                                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                                    SetProperty(x => x.OffenderID).
                                WithValueFromBlockIndex("AddOffenderTestBlock").
                                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                               AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                                    SetDescription("Get events test").

                               AddBlock<Web_CurrentStatusSearchEventTestBlock>().UsePreviousBlockData().
                                    SetDescription("Web_Search Event Test Block").
                                    SetProperty(x => x.DoNavigateToCurrentStatus).WithValue(false).
                                    SetProperty(x => x.GetEventsResponse).
                                        WithValueFromBlockIndex("GetEventsTestBlock").
                                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                                AddBlock<Web_SearchEventsOnCurrentStatusAssertBlock>().
                                SetDescription("Web_Search Event Assert Block").

                    ExecuteFlow();
        }
    }
}
