﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.Proxies.API.APIExtensions;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using System.ServiceModel;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.APIExtensions;
using LogicBlocksLib.APIExtensions;

namespace TestingFramework.UnitTests.APIExtensionsTests
{
    [TestClass]
    public class APIExtensionsSvcTests : BaseUnitTest
    {
        [TestMethod]
        public void GetEventsByFilterIDTest_MonitorScreen_LastDay()
        {
            TestingFlow.
                AddBlock<GetEventsByFilterIdTestingBlock>().
                    SetDescription("GetEventsByFilterID like first time user open monitor screen").
                    SetProperty(x => x.StartRowIndex).WithValue(1).
                    SetProperty(x => x.MaximumRows).WithValue(30).
                    SetProperty(x => x.FilterID).WithValue(1).
                    SetProperty(x => x.ScreenName).WithValue("Monitor").
                    SetProperty(x => x.PreviousTimeStamp).WithValue(new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 }).
                    SetProperty(x => x.PageUpperLimitEventId).WithValue(0).
                    SetProperty(x => x.FromTime).WithValue(DateTime.UtcNow.AddDays(-1)).
                    SetProperty(x => x.ToTime).WithValue(DateTime.UtcNow).
                    SetProperty(x => x.ForceRefresh).WithValue(false).
                AddBlock<GetEventsByFilterId_UpdatePrametersFromResponseLogicBlock>().UsePreviousBlockData().
                    SetDescription("Update parameters for the next GetEventsByFilterRequest from previous call").
                //need to add 30 seconds delay between requests as browser doing
                AddBlock<GetEventsByFilterIdTestingBlock>().UsePreviousBlockData().
                    SetDescription("GetEventsByFilterID like second time with parameters updated").
                ExecuteFlow();
        }
    }
}
