﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using LogicBlocksLib.ScheduleBlocks;
using TestBlocksLib.ScheduleBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Categories;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

#region API Zones refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

#region API Schedule refs
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using Schedule1 = TestingFramework.Proxies.EM.Interfaces.Schedule3_10;
using Schedule2 = TestingFramework.Proxies.EM.Interfaces.Schedule3_9;
using AssertBlocksLib.ScheduleAsserts;
using System.Collections.Generic;
using System.Linq;
using TestingFramework.Proxies.API.Schedule;



#endregion

namespace TestingFramework.UnitTests.ScheduleSvcTests
{
    [TestClass]
    public class ScheduleSvcTests : BaseUnitTest
    {
        #region Tests Parameters for Zones0

        Zones0.EntPoint Point_AddCircularZone = new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones0.EntPoint[] Points_AddPolygonZone = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones0.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones0.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones0.EntPoint[] Points_UpdatePolygonZone = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones0.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones0.EntPoint() { LAT = 31.105820, LON = 33.834568 } };


        #endregion

        #region Tests Parameters for Zones1

        Zones1.EntPoint Point_AddCircularZone_1 = new Zones1.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones1.EntPoint[] Points_AddPolygonZone_1 = new Zones1.EntPoint[3] {
                            new Zones1.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones1.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones1.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones1.EntPoint[] Points_UpdatePolygonZone_1 = new Zones1.EntPoint[3] {
                            new Zones1.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones1.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones1.EntPoint() { LAT = 31.105820, LON = 33.834568 } };

        #endregion

        #region Tests Parameters for Zones2

        Zones2.EntPoint Point_AddCircularZone_2 = new Zones2.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones2.EntPoint[] Points_AddPolygonZone_2 = new Zones2.EntPoint[3] {
                            new Zones2.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones2.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones2.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones2.EntPoint[] Points_UpdatePolygonZone_2 = new Zones2.EntPoint[3] {
                            new Zones2.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones2.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones2.EntPoint() { LAT = 31.105820, LON = 33.834568 } };

        #endregion


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_AddTimeFrame_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)). 
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                     AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                    AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule test block").
                        
                    AddBlock<AddTimeFrameAssertBlock>().
                    
            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_AddTimeFrame_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    
                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).
                    SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_1").
                            FromDeepProperty<AddCircularZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create get schedule request").

                AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get schedule test").

                    AddBlock<AddTimeFrameAssertBlock_1>().



            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_AddTimeFrame_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF). //2Piece GPS/RF 
                    
                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                    SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_2").
                            FromDeepProperty<AddCircularZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create get schedule request").

                AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get schedule test").

                AddBlock<AddTimeFrameAssertBlock_2>().


            ExecuteFlow();
        }

        
        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_DeleteTimeFrame_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.RF_Cellular).//E3 program type

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

            AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                   SetDescription("Get schedule request").
                   SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                   SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                   SetProperty(x => x.EntityID).
                   WithValueFromBlockIndex("AddOffenderTestBlock").
                       FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

               AddBlock<GetScheduleTestBlock>().
                   SetDescription("Get schedule test block").
                   SetProperty(x => x.GetScheduleRequest).
                       WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                       FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_DeleteTimeFrame_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.RF_Cellular).//E3 program type

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                 AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock_1>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_1").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_1>(x => x.GetScheduleRequest).


                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("GetScheduleTestBlock_1").
                    FromDeepProperty<GetScheduleTestBlock_1>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).

            AddBlock<DeleteTimeFrameTestBlock_1>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

                 AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock_1>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_1").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_1>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock_1>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_DeleteTimeFrame_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.RF_Cellular).//E3 program type
                    SetProperty(x => x.IsPrimaryLocation).WithValue(true).

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                 AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock_2>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_2").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_2>(x => x.GetScheduleRequest).


                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("GetScheduleTestBlock_2").
                    FromDeepProperty<GetScheduleTestBlock_2>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).

            AddBlock<DeleteTimeFrameTestBlock_2>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

                 AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock_2>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_2").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_2>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock_2>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_RepeatTimeFrame_Success()
        {

            //The RepeatSchedule API is relevant only for E3 program type 
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.RF_Cellular).//E3 program type

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgRepeatScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                AddBlock<RepeatScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).


                    AddBlock<RepeatScheduleAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_RepeatTimeFrame_1_Success()
        {
            //The RepeatSchedule API is relevant only for E3 program type 
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.RF_Cellular).//E3 program type

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgRepeatScheduleRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                AddBlock<RepeatScheduleTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock_1>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_1").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_1>(x => x.GetScheduleRequest).


                    AddBlock<RepeatScheduleAssertBlock_1>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_RepeatTimeFrame_2_Success()
        {
            //The RepeatSchedule API is relevant only for E3 program type 
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.RF_Cellular).//E3 program type
                    SetProperty(x => x.IsPrimaryLocation).WithValue(true).

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgRepeatScheduleRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                AddBlock<RepeatScheduleTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock_2>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_2").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_2>(x => x.GetScheduleRequest).
                        
                    AddBlock<RepeatScheduleAssertBlock_2>().

            ExecuteFlow();
        }

       
        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_DeleteRecurringTimeFrame_Success()
        {
            TestingFlow.

                        AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                            SetDescription("Create Add Offender Request").
                            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                        AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                            SetDescription("Add Offender test block").

                        AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                            SetDescription("Create Add Time Frame Request").
                            SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).//(DateTime.Parse("May 01 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                            SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).//(DateTime.Parse("May 01 2018 11:00AM", System.Globalization.CultureInfo.InvariantCulture)).
                            SetProperty(x => x.EntityID).
                                WithValueFromBlockIndex("AddOffenderTestBlock").
                                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                            SetProperty(x => x.IsWeekly).WithValue(true).
                            SetProperty(x => x.LocationID).WithValue(null).
                            SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                        AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                            SetDescription("Add Time Frame test block").

                            AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                                SetDescription("Get schedule request").
                                SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                            SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                                SetProperty(x => x.EntityID).
                                WithValueFromBlockIndex("AddOffenderTestBlock").
                                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

                            AddBlock<GetScheduleTestBlock>().
                                SetDescription("Get schedule test block").
                                SetProperty(x => x.GetScheduleRequest).
                                    WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                                    FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                        AddBlock<CreateEntMsgDeleteRecurringTimeFrameRequestBlock>().UsePreviousBlockData().
                            SetDescription("Create delete Time Frame request").
                            SetProperty(x => x.Date).WithValue(DateTime.Now.Date.AddDays(8)).//(DateTime.Parse("May 08 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                        SetProperty(x => x.EntityID).
                            WithValueFromBlockIndex("AddOffenderTestBlock").
                                FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                        SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                        AddBlock<DeleteRecurringTimeFrameTestBlock>().UsePreviousBlockData().
                            SetDescription("Delete recurring time frame test block").

                        AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                            SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).//(DateTime.Parse("May 01 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                            SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(30).AddHours(1)).//(DateTime.Parse("May 30 2018 11:00AM", System.Globalization.CultureInfo.InvariantCulture)).

                        AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                            SetDescription("Get schedule test block").

                        AddBlock<DeleteRecurringTimeFrameAssertBlock>().UsePreviousBlockData().
                            SetDescription("Delete recurring assert block").

                    ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_DeleteRecurringTimeFrame_1_Success()
        {
            TestingFlow.

                            AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                                SetDescription("Create Add Offender Request").
                                SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.E4_Dual).

                            AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                                SetDescription("Add Offender test block").

                            AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                                SetDescription("Create Add Time Frame Request").
                                SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                                SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                                SetProperty(x => x.EntityID).
                                    WithValueFromBlockIndex("AddOffenderTestBlock_1").
                                        FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                                SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                                SetProperty(x => x.IsWeekly).WithValue(true).
                                SetProperty(x => x.LocationID).WithValue(null).
                                SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.DontCare).

                            AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                                SetDescription("Add Time Frame test block").

                                AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                                    SetDescription("Get schedule request").
                                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                                SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                                    SetProperty(x => x.EntityID).
                                    WithValueFromBlockIndex("AddOffenderTestBlock_1").
                                        FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                                    SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).

                                AddBlock<GetScheduleTestBlock_1>().
                                    SetDescription("Get schedule test block").
                                    SetProperty(x => x.GetScheduleRequest).
                                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_1").
                                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_1>(x => x.GetScheduleRequest).

                            AddBlock<CreateEntMsgDeleteRecurringTimeFrameRequestBlock_1>().UsePreviousBlockData().
                                SetDescription("Create delete Time Frame request").
                                SetProperty(x => x.Date).WithValue(DateTime.Now.Date.AddDays(8)).
                            SetProperty(x => x.EntityID).
                                WithValueFromBlockIndex("AddOffenderTestBlock_1").
                                    FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                             SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                            SetProperty(x => x.TimeframeID).
                            WithValueFromBlockIndex("GetScheduleTestBlock_1").
                    FromDeepProperty<GetScheduleTestBlock_1>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).

                            AddBlock<DeleteRecurringTimeFrameTestBlock_1>().UsePreviousBlockData().
                                SetDescription("Delete recurring time frame test block").

                            AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                            SetDescription("Get schedule request").
                             SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                                SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(30).AddHours(1)).

                            AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
                                SetDescription("Get schedule test block").

                            AddBlock<DeleteRecurringTimeFrameAssertBlock_1>().UsePreviousBlockData().
                                SetDescription("Delete recurring assert block").

                        ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_DeleteRecurringTimeFrame_2_Success()
        {
            TestingFlow.

                            AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                                SetDescription("Create Add Offender Request").
                                SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.E4_Dual).
                                SetProperty(x => x.IsPrimaryLocation).WithValue(true).

                            AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                                SetDescription("Add Offender test block").

                            AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                                SetDescription("Create Add Time Frame Request").
                                SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                                SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                                SetProperty(x => x.EntityID).
                                    WithValueFromBlockIndex("AddOffenderTestBlock_2").
                                        FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                                SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                                SetProperty(x => x.IsWeekly).WithValue(true).
                                SetProperty(x => x.LocationID).WithValue(null).
                                SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.DontCare).

                            AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                                SetDescription("Add Time Frame test block").

                                AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                                    SetDescription("Get schedule request").
                                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                                SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                                    SetProperty(x => x.EntityID).
                                    WithValueFromBlockIndex("AddOffenderTestBlock_2").
                                        FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                                    SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).

                                AddBlock<GetScheduleTestBlock_2>().
                                    SetDescription("Get schedule test block").
                                    SetProperty(x => x.GetScheduleRequest).
                                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_2").
                                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock_2>(x => x.GetScheduleRequest).

                            AddBlock<CreateEntMsgDeleteRecurringTimeFrameRequestBlock_2>().UsePreviousBlockData().
                                SetDescription("Create delete Time Frame request").
                                SetProperty(x => x.Date).WithValue(DateTime.Now.Date.AddDays(8)).
                            SetProperty(x => x.EntityID).
                                WithValueFromBlockIndex("AddOffenderTestBlock_2").
                                    FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                             SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                            SetProperty(x => x.TimeframeID).
                            WithValueFromBlockIndex("GetScheduleTestBlock_2").
                    FromDeepProperty<GetScheduleTestBlock_2>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).

                            AddBlock<DeleteRecurringTimeFrameTestBlock_2>().UsePreviousBlockData().
                                SetDescription("Delete recurring time frame test block").

                            AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                            SetDescription("Get schedule request").
                             SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                                SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(30).AddHours(1)).

                            AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
                                SetDescription("Get schedule test block").

                            AddBlock<DeleteRecurringTimeFrameAssertBlock_2>().UsePreviousBlockData().
                                SetDescription("Delete recurring assert block").

                        ExecuteFlow();
        }

      
        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_UpdateTimeFrame_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(6)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(6).AddHours(2)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(5)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(5).AddMinutes(30)).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTestsUpdateScheduleEdit()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Offender Request").
                      SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                      SetDescription("Add Offender test block").

               AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Circular Zone Request").
                      SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock").
                              FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                       SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

               AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                       SetDescription("Add Circular Zone test block").

               AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                       SetDescription("Create Add Time Frame Request").
                       SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                       SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                       SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock").
                              FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                       SetProperty(x => x.IsWeekly).WithValue(false).
                       SetProperty(x => x.LocationID).
                          WithValueFromBlockIndex("AddCircularZoneTestBlock").
                              FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                       SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

               AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                       SetDescription("Add Time Frame test block").
               
               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
               SetProperty(x => x.IsWeekly).WithValue(false).
               SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
               SetProperty(x => x.TimeframeID).WithValueFromBlockIndex("AddTimeFrameTestBlock").FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).


               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").
               
               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
               SetDescription("Create get Schedule Request block").
               
               AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Get Schedule Test Block").

               AddBlock<UpdateScheduleEditTimeFrameAssertBlock>().
               SetDescription("Assert Schedule Test Block").
               SetProperty(x => x.AddTimeFrameRequest).WithValueFromBlockIndex("AddTimeFrameTestBlock").FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTestsUpdateScheduleEdit_1()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Offender Request").
                      SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                      SetDescription("Add Offender test block").

               AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Circular Zone Request").
                      SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_1").
                              FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).
                       SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).

               AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().
                       SetDescription("Add Circular Zone test block").

               AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                       SetDescription("Create Add Time Frame Request").
                       SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                       SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                       SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_1").
                              FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                       SetProperty(x => x.IsWeekly).WithValue(false).
                       SetProperty(x => x.LocationID).
                          WithValueFromBlockIndex("AddCircularZoneTestBlock_1").
                              FromDeepProperty<AddCircularZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                       SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

               AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                       SetDescription("Add Time Frame test block").

               AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                       SetDescription("Create get Schedule Request block").

               AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
                       SetDescription("Get schedule request block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock_1>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock_1").
                               FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
               SetProperty(x => x.IsWeekly).WithValue(false).
               SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.DontCare).
               SetProperty(x => x.TimeframeID).WithValueFromBlockIndex("GetScheduleTestBlock_1").FromDeepProperty<GetScheduleTestBlock_1>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).
               
               AddBlock<UpdateScheduleTestBlock_1>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").
               
               AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                       SetDescription("Create get Schedule Request block").
               AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
               SetDescription("Get Schedule Test Block").

               AddBlock<UpdateScheduleEditTimeFrameAssertBlock_1>().
               SetDescription("Verify Schedule Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTestsUpdateScheduleEdit_2()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Offender Request").
                      SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).  

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                      SetDescription("Add Offender test block").

               AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Circular Zone Request").
                      SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_2").
                              FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                       SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).

               AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().
                       SetDescription("Add Circular Zone test block").

               AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                       SetDescription("Create Add Time Frame Request").
                       SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                       SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                       SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_2").
                              FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                       SetProperty(x => x.IsWeekly).WithValue(false).
                       SetProperty(x => x.LocationID).
                          WithValueFromBlockIndex("AddCircularZoneTestBlock_2").
                              FromDeepProperty<AddCircularZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                       SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.MustBeOut).

               AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                       SetDescription("Add Time Frame test block").

               AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                       SetDescription("Create get Schedule Request block").

               AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
                       SetDescription("Get schedule request block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock_2>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock_2").
                               FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
               SetProperty(x => x.IsWeekly).WithValue(false).
               SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.DontCare).
               SetProperty(x => x.TimeframeID).WithValueFromBlockIndex("GetScheduleTestBlock_2").FromDeepProperty<GetScheduleTestBlock_2>(x => x.GetScheduleResponse.Schedule.CalendarSchedule.First().ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).


               AddBlock<UpdateScheduleTestBlock_2>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").


               AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
               SetDescription("Get Schedule Test Block").

               AddBlock<UpdateScheduleEditTimeFrameAssertBlock_2>().
               SetDescription("Verify Schedule Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTestsUpdateScheduleDelete()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Offender Request").
                      SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                      SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Circular Zone Request").
                      SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock").
                              FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                       SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

               AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                       SetDescription("Add Circular Zone test block").
                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                       SetDescription("Create Add Time Frame Request").
                       SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                       SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                       SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock").
                              FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                       SetProperty(x => x.IsWeekly).WithValue(false).
                       SetProperty(x => x.LocationID).
                          WithValueFromBlockIndex("AddCircularZoneTestBlock").
                              FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                       SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

               AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                       SetDescription("Add Time Frame test block").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                       SetDescription("Create get Schedule Request block").

               AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                       SetDescription("Get schedule request block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Get Schedule Test Block").

               AddBlock<UpdateScheduleDeleteAllTimeFrameAssertBlock>().
               SetDescription("Get Schedule Test Block").
               
               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTestsUpdateScheduleDelete_1()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Offender Request").
                      SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                      SetDescription("Add Offender test block").

                      AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Circular Zone Request").
                      SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_1").
                              FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).
                       SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).

               AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().
                       SetDescription("Add Circular Zone test block").

                       AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                       SetDescription("Create Add Time Frame Request").
                       SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                       SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                       SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_1").
                              FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                       SetProperty(x => x.IsWeekly).WithValue(false).
                       SetProperty(x => x.LocationID).
                          WithValueFromBlockIndex("AddCircularZoneTestBlock_1").
                              FromDeepProperty<AddCircularZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                       SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

               AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                       SetDescription("Add Time Frame test block").

               AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                       SetDescription("Create get Schedule Request block").

               AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
                       SetDescription("Get schedule request block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock_1>().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock_1").
                               FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).

               AddBlock<UpdateScheduleTestBlock_1>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
               SetDescription("Get Schedule Test Block").

               AddBlock<UpdateScheduleDeleteAllTimeFrameAssertBlock_1>().
               SetDescription("Get Schedule Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTestsUpdateScheduleDelete_2()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Offender Request").
                      SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                      SetDescription("Add Offender test block").

                      AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Add Circular Zone Request").
                      SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_2").
                              FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                       SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).

               AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().
                       SetDescription("Add Circular Zone test block").

                       AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                       SetDescription("Create Add Time Frame Request").
                       SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                       SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                       SetProperty(x => x.EntityID).
                          WithValueFromBlockIndex("AddOffenderTestBlock_2").
                              FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                       SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                       SetProperty(x => x.IsWeekly).WithValue(false).
                       SetProperty(x => x.LocationID).
                          WithValueFromBlockIndex("AddCircularZoneTestBlock_2").
                              FromDeepProperty<AddCircularZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                       SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

               AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                       SetDescription("Add Time Frame test block").

               AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                       SetDescription("Create get Schedule Request block").

               AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
                       SetDescription("Get schedule request block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock_2>().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock_2").
                               FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).

               AddBlock<UpdateScheduleTestBlock_2>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
               SetDescription("Get Schedule Test Block").

               AddBlock<UpdateScheduleDeleteAllTimeFrameAssertBlock_2>().
               SetDescription("Get Schedule Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_UpdateTimeFrame_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).
                    SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(6)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(6).AddHours(2)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule1.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_1").
                            FromDeepProperty<AddCircularZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule1.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").

                AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock_1").
                            FromDeepProperty<GetScheduleTestBlock_1>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(5)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(5).AddMinutes(30)).

                AddBlock<UpdateTimeFrameTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").

                AddBlock<GetScheduleTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<UpdateTimeFrameAssertBlock_1>().
                    SetDescription("Update Schedule Assert block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Service)]
        [TestMethod]
        public void ScheduleSvcTests_UpdateTimeFrame_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                    SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(6)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(6).AddHours(2)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule2.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_2").
                            FromDeepProperty<AddCircularZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule2.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").

                AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock_2").
                            FromDeepProperty<GetScheduleTestBlock_2>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(5)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(5).AddMinutes(30)).

                AddBlock<UpdateTimeFrameTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").

                AddBlock<GetScheduleTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<UpdateTimeFrameAssertBlock_2>().
                    SetDescription("Update Schedule Assert block").

               ExecuteFlow();
        }


        
    }
}
