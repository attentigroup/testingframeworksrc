﻿using AssertBlocksLib.ScheduleAsserts;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.ScheduleBlocks;
using LogicBlocksLib.ZonesBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.ScheduleBlocks;
using TestBlocksLib.ZonesBlocks;
using TestingFramework.Proxies.API.Schedule;
using TestingFramework.TestsInfraStructure.Implementation;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Schedule0 = TestingFramework.Proxies.EM.Interfaces.Schedule12_0;
using AssertBlocksLib.ScheduleAsserts;
using TestingFramework.TestsInfraStructure.Interfaces;
using LogicBlocksLib.Group;
using TestBlocksLib.GroupsBlocks;
using TestingFramework.Proxies.EM.Interfaces.Groups12_0;
using AssertBlocksLib.TestingFrameworkAsserts;
using AssertBlocksLib.EquipmentAsserts;

namespace TestingFramework.UnitTests.ScheduleSvcTests
{
    [TestClass]
    public class ScheduleRegressionTests : BaseUnitTest
    {
        #region Add time frame
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_AlcoholTest()
        {
            Schedule_AddTimeFrame_Offender_AlcoholTest(
                TestingFlow, 1, true).ExecuteFlow();

        }


        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_MustBeIn_1Piece()
        {
            Schedule_AddTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();
        }

        private ITestingFlow Schedule_AddTimeFrame_Offender_Isweekly(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        {
            testingFlow = AddOffender(TestingFlow, ProgramType);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = AddInclusionZone(testingFlow);
            else
                testingFlow = AddExclusionZone(testingFlow);

            testingFlow = AddTimeFrame(testingFlow, TimeFrameType, true);

            testingFlow = AddWeeklyTimeFrameAssert(testingFlow);

            return testingFlow;
        }


        private ITestingFlow AddOffender(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block");

            return testingFlow;
        }

        private ITestingFlow AddExclusionZone(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block");

            return testingFlow;
        }

        private ITestingFlow AddInclusionZone(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block");

            return testingFlow;
        }

        private ITestingFlow AddTimeFrame(ITestingFlow testingFlow, Schedule0.EnmTimeFrameType TimeFrameType, bool IsWeekly)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(TimeFrameType).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block");

            return testingFlow;
        }



        private ITestingFlow AddWeeklyTimeFrameAssert(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData();

            return testingFlow;
        }

        private ITestingFlow AddCalenderTimeFrameAssert(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData();

            return testingFlow;
        }


        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_DontCareOnMustBeIn()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(6)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgAddTimeFrameRequestBlock_2").
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                SetName("AddTimeFrameTestBlock_2").
                    SetDescription("Add Time Frame test block").
                    SetProperty(x => x.AddTimeFrameRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddTimeFrameRequestBlock_2").
                    FromDeepProperty<CreateEntMsgAddTimeFrameRequestBlock>(x => x.AddTimeFrameRequest).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.AddTimeFrameRequest).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).
                    SetProperty(x => x.AddTimeFrameResponse).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse).

                AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.AddTimeFrameRequest).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock_2").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).
                    SetProperty(x => x.AddTimeFrameResponse).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock_2").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse).

            ExecuteFlow();
        }



        //[TestCategory(CategoriesNames.Schedule_Regression)]
        //[TestMethod]
        //public void Schedule_AddTimeFrame_Offender_DontCareOnMustBeOut()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

        //        AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").

        //        AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Circular Zone Request").
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("AddOffenderTestBlock").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
        //            SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

        //        AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Circular Zone test block").

        //        AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Create Add Time Frame Request").
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(6)).
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("AddOffenderTestBlock").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
        //            SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
        //            SetProperty(x => x.IsWeekly).WithValue(true).
        //            SetProperty(x => x.LocationID).
        //                WithValueFromBlockIndex("AddCircularZoneTestBlock").
        //                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
        //            SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

        //        AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Time Frame test block").

        //            AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
        //            SetName("CreateEntMsgAddTimeFrameRequestBlock_2").
        //            SetDescription("Create Add Time Frame Request").
        //            SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
        //            SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).
        //            SetProperty(x => x.EntityID).
        //                WithValueFromBlockIndex("AddOffenderTestBlock").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
        //            SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
        //            SetProperty(x => x.IsWeekly).WithValue(false).
        //            SetProperty(x => x.LocationID).
        //                WithValueFromBlockIndex("AddCircularZoneTestBlock").
        //                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
        //            SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

        //        AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
        //        SetName("AddTimeFrameTestBlock_2").
        //            SetDescription("Add Time Frame test block").
        //            SetProperty(x => x.AddTimeFrameRequest).
        //            WithValueFromBlockIndex("CreateEntMsgAddTimeFrameRequestBlock_2").
        //            FromDeepProperty<CreateEntMsgAddTimeFrameRequestBlock>(x => x.AddTimeFrameRequest).

        //        AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
        //            SetDescription("Get schedule request").
        //            SetProperty(x => x.EntityID).
        //            WithValueFromBlockIndex("AddOffenderTestBlock").
        //                FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
        //            SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
        //            SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

        //        AddBlock<GetScheduleTestBlock>().
        //            SetDescription("Get schedule test block").
        //            SetProperty(x => x.GetScheduleRequest).
        //                WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
        //                FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

        //        AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.AddTimeFrameRequest).
        //                WithValueFromBlockIndex("AddTimeFrameTestBlock").
        //                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).
        //            SetProperty(x => x.AddTimeFrameResponse).
        //                WithValueFromBlockIndex("AddTimeFrameTestBlock").
        //                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse).

        //        AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData().
        //            SetProperty(x => x.AddTimeFrameRequest).
        //                WithValueFromBlockIndex("AddTimeFrameTestBlock_2").
        //                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).
        //            SetProperty(x => x.AddTimeFrameResponse).
        //                WithValueFromBlockIndex("AddTimeFrameTestBlock_2").
        //                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse).

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_MustBeOutOnDontCare()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(6)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgAddTimeFrameRequestBlock_2").
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(2)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                SetName("AddTimeFrameTestBlock_2").
                    SetDescription("Add Time Frame test block").
                    SetProperty(x => x.AddTimeFrameRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddTimeFrameRequestBlock_2").
                    FromDeepProperty<CreateEntMsgAddTimeFrameRequestBlock>(x => x.AddTimeFrameRequest).

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.AddTimeFrameRequest).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).
                    SetProperty(x => x.AddTimeFrameResponse).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse).

                AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.AddTimeFrameRequest).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock_2").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest).
                    SetProperty(x => x.AddTimeFrameResponse).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock_2").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_MustBeIn_2Piece()
        {
            Schedule_AddTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_MustBeOut_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_DontCare_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    //    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    //        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_DontCare_Exclusion_1Piece()
        {
            Schedule_AddTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_DontCare_Exclusion_2Piece()
        {
            Schedule_AddTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_DontCare_Inclusion_1Piece_Fail()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                    AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<ValidationFaultExceptionAssertBlock>(
                             ValidationFaultExceptionScheduleAssertBlock.ValidateSchedule_IsWeekly_TimeFrameType_WS760()).

                    ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_DontCare_Inclusion_2Piece_Fail()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                    AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<ValidationFaultExceptionAssertBlock>(
                      ValidationFaultExceptionScheduleAssertBlock.ValidateSchedule_IsWeekly_TimeFrameType_WS760()).

                    ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_VoiceTest_Incoming()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date, DateTime.Now.Date.AddDays(2)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_VoiceTest_Outgoing()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Outgoing, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_VoiceTest_OutAndIn()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.OutAndIn, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_IsWeekly_VoiceTest_Incoming24()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming24, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59)).ExecuteFlow();
        }

        private ITestingFlow Schedule_AddTimeFrame_Offender_VoiceTest(ITestingFlow testingFlow, Schedule0.EnmVoiceTestCallType VoiceTestCallType, int NumberOfTests, bool IsWeekly, DateTime StartTime, DateTime EndTime, DateTime GetScheduleStartTime, DateTime GetScheduleEndTime)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Voice_Verification).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(StartTime).
                    SetProperty(x => x.EndTime).WithValue(EndTime).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.VoiceTest).
                    SetProperty(x => x.VoiceTestCallType).WithValue(VoiceTestCallType).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).WithValue(GetScheduleStartTime).
                    SetProperty(x => x.EndDate).WithValue(GetScheduleEndTime).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetName("GetScheduleTestBlock_After").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddVoiceTimeFrameAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse);

            return testingFlow;
        }

        private ITestingFlow Schedule_AddTimeFrame_Offender_NotIsweekly(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        {
            testingFlow = AddOffender(TestingFlow, ProgramType);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = AddInclusionZone(testingFlow);
            else
                testingFlow = AddExclusionZone(testingFlow);

            testingFlow = AddTimeFrame(testingFlow, TimeFrameType, false);

            testingFlow = AddCalenderTimeFrameAssert(testingFlow);

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_MustBeIn_1Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_MustBeIn_2Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_DontCare_Exclusion_1Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_DontCare_Exclusion_2Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_DontCare_Inclusion_1Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_DontCare_Inclusion_2Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_MustBeOut_Exclusion_1Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.MustBeOut).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_MustBeOut_Exclusion_2Piece()
        {
            Schedule_AddTimeFrame_Offender_NotIsweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.MustBeOut).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_MustBeOut_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    //    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    //        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_DontCare_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    //    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    //        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_VoiceTest_Incoming()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date, DateTime.Now.Date.AddDays(2)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_VoiceTest_Outgoing()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Outgoing, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_VoiceTest_OutAndIn()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.OutAndIn, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1)).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_VoiceTest_Incoming24()
        {
            Schedule_AddTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming24, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59)).ExecuteFlow();

        }

        private ITestingFlow Schedule_AddTimeFrame_Offender_AlcoholTest(ITestingFlow testingFlow, int NumberOfTests, bool IsWeekly)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VB_Cellular).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.AlcoholTest).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

            AddBlock<AddAlcoholTimeFrameAssertBlock>().UsePreviousBlockData();

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Offender_NotIsWeekly_AlcoholTest()
        {
            Schedule_AddTimeFrame_Offender_AlcoholTest(
                TestingFlow, 1, false).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_IsWeekly_MustBeIn()
        {
            Schedule_AddTimeFrame_Group_Isweekly(
                TestingFlow, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();
        }


        private ITestingFlow Schedule_AddTimeFrame_Group_Isweekly(ITestingFlow testingFlow, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        {
            testingFlow = AddGroupWithOffender(TestingFlow);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = AddInclusionZoneToAGroup(testingFlow);
            else
                testingFlow = AddExclusionZoneToAGroup(testingFlow);

            testingFlow = AddTimeFrameToAGroup(testingFlow, TimeFrameType, true);

            testingFlow = AddWeeklyTimeFrameToAGroupAssert(testingFlow);

            return testingFlow;
        }


        private ITestingFlow AddGroupWithOffender(ITestingFlow testingFlow)
        {
            TestingFlow.

                 AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    //SetProperty(x => x.Name).WithValue("New group").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData();

            return testingFlow;
        }

        private ITestingFlow AddExclusionZoneToAGroup(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block");

            return testingFlow;
        }

        private ITestingFlow AddInclusionZoneToAGroup(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block");

            return testingFlow;
        }

        private ITestingFlow AddTimeFrameToAGroup(ITestingFlow testingFlow, Schedule0.EnmTimeFrameType TimeFrameType, bool IsWeekly)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(TimeFrameType).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block");

            return testingFlow;
        }

        private ITestingFlow AddWeeklyTimeFrameToAGroupAssert(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddWeeklyTimeFrameAssertBlock>().UsePreviousBlockData();

            return testingFlow;
        }

        private ITestingFlow AddCalenderTimeFrameToAGroupAssert(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                     SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<AddCalenderTimeFrameAssertBlock>().UsePreviousBlockData();

            return testingFlow;
        }


        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_IsWeekly_DontCare_Exclusion()
        {
            Schedule_AddTimeFrame_Group_Isweekly(
                TestingFlow, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_IsWeekly_DontCare_Inclusion_Fail()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetProperty(x => x.Description).WithValue("Group description").
                   SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                   SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                   SetProperty(x => x.OffendersListIDs[0]).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create Add Circular Zone Request").
                   SetProperty(x => x.EntityID).
                       WithValueFromBlockIndex("AddGroupTestBlock").
                           FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                   SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Circular Zone test block").
                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Add Time Frame Request").
                   SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                   SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                       WithValueFromBlockIndex("AddGroupTestBlock").
                           FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                   SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                   SetProperty(x => x.IsWeekly).WithValue(true).
                   SetProperty(x => x.LocationID).
                       WithValueFromBlockIndex("AddCircularZoneTestBlock").
                           FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                   SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Time Frame test block").

                 AddBlock<ValidationFaultExceptionAssertBlock>(
                             ValidationFaultExceptionScheduleAssertBlock.ValidateSchedule_IsWeekly_TimeFrameType_WS760()).

            ExecuteFlow();
        }

        private ITestingFlow Schedule_AddTimeFrame_Group_NotIsweekly(ITestingFlow testingFlow, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        {
            testingFlow = AddGroupWithOffender(TestingFlow);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = AddInclusionZoneToAGroup(testingFlow);
            else
                testingFlow = AddExclusionZoneToAGroup(testingFlow);

            testingFlow = AddTimeFrameToAGroup(testingFlow, TimeFrameType, false);

            testingFlow = AddCalenderTimeFrameToAGroupAssert(testingFlow);

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_NotIsWeekly_MustBeIn()
        {
            Schedule_AddTimeFrame_Group_NotIsweekly(
                TestingFlow, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_NotIsWeekly_DontCare_Exclusion()
        {
            Schedule_AddTimeFrame_Group_NotIsweekly(
                TestingFlow, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_NotIsWeekly_DontCare_Inclusion()
        {
            Schedule_AddTimeFrame_Group_NotIsweekly(
                TestingFlow, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_AddTimeFrame_Group_NotIsWeekly_MustBeOut()
        {
            Schedule_AddTimeFrame_Group_NotIsweekly(
                TestingFlow, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.MustBeOut).ExecuteFlow();
        }
        #endregion

        #region Delete recuring time frame
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_DeleteRecurringTimeFrame_Offender()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).//(DateTime.Parse("May 01 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).//(DateTime.Parse("May 01 2018 11:00AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<CreateEntMsgDeleteRecurringTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.Date).WithValue(DateTime.Now.Date.AddDays(8)).//(DateTime.Parse("May 08 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteRecurringTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete recurring time frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                SetDescription("Get schedule request").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).//(DateTime.Parse("May 01 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(30).AddHours(1)).//(DateTime.Parse("May 30 2018 11:00AM", System.Globalization.CultureInfo.InvariantCulture)).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").

                AddBlock<DeleteRecurringTimeFrameAssertBlock>().UsePreviousBlockData().
                    SetDescription("Delete recurring assert block").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_DeleteRecurringTimeFrame_Group()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    //SetProperty(x => x.Name).WithValue("New group").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.E4).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).//(DateTime.Parse("May 01 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).//(DateTime.Parse("May 01 2018 11:00AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).

                AddBlock<GetScheduleTestBlock>().
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<CreateEntMsgDeleteRecurringTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.Date).WithValue(DateTime.Now.Date.AddDays(8)).//(DateTime.Parse("May 08 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("AddTimeFrameTestBlock").
                            FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

                AddBlock<DeleteRecurringTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete recurring time frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date.AddDays(1)).//(DateTime.Parse("May 01 2018 10:30AM", System.Globalization.CultureInfo.InvariantCulture)).
                    SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(30).AddHours(1)).//(DateTime.Parse("May 30 2018 11:00AM", System.Globalization.CultureInfo.InvariantCulture)).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule test block").

                AddBlock<DeleteRecurringTimeFrameAssertBlock>().UsePreviousBlockData().
                    SetDescription("Delete recurring assert block").

                ExecuteFlow();
        }
        #endregion

        #region Delete time frame
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_DeleteTimeFrame_Offender_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).//E3 program type

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

            AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                   SetDescription("Get schedule request").
                   SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                   SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                   SetProperty(x => x.EntityID).
                   WithValueFromBlockIndex("AddOffenderTestBlock").
                       FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

               AddBlock<GetScheduleTestBlock>().
                   SetDescription("Get schedule test block").
                   SetProperty(x => x.GetScheduleRequest).
                       WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                       FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_DeleteTimeFrame_Offender_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).//E3 program type

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

            AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                   SetDescription("Get schedule request").
                   SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                   SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                   SetProperty(x => x.EntityID).
                   WithValueFromBlockIndex("AddOffenderTestBlock").
                       FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

               AddBlock<GetScheduleTestBlock>().
                   SetDescription("Get schedule test block").
                   SetProperty(x => x.GetScheduleRequest).
                       WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                       FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_DeleteTimeFrame_Offender_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).//E3 program type

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

            AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                   SetDescription("Get schedule request").
                   SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                   SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                   SetProperty(x => x.EntityID).
                   WithValueFromBlockIndex("AddOffenderTestBlock").
                       FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

               AddBlock<GetScheduleTestBlock>().
                   SetDescription("Get schedule test block").
                   SetProperty(x => x.GetScheduleRequest).
                       WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                       FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_DeleteTimeFrame_Group()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    //SetProperty(x => x.Name).WithValue("New group").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgDeleteTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Time Frame request").
                    SetProperty(x => x.TimeframeID).
                WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).

            AddBlock<DeleteTimeFrameTestBlock>().UsePreviousBlockData().
                SetDescription("Delete Time Frame test block").

            AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                   SetDescription("Get schedule request").
                   SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                   SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                   SetProperty(x => x.EntityID).
                   WithValueFromBlockIndex("AddGroupTestBlock").
                       FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                   SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).

               AddBlock<GetScheduleTestBlock>().
                   SetDescription("Get schedule test block").
                   SetProperty(x => x.GetScheduleRequest).
                       WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                       FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

            AddBlock<DeleteTimeFrameAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Get schedule
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_GetSchedule_Offender()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                     AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest.StartTime).
                    SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest.EndTime).

                    AddBlock<GetScheduleTestBlock>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                    AddBlock<GetScheduleAssertBlock>().



            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_GetSchedule_Group()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    //SetProperty(x => x.Name).WithValue("New group").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                     AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                        SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest.StartTime).
                    SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                    FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameRequest.EndTime).

                    AddBlock<GetScheduleTestBlock>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                    AddBlock<GetScheduleAssertBlock>().



            ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_RepeatTimeFrame_E4()
        {

            //The RepeatSchedule API is relevant only for E3 program type 
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.RF_Cellular).//E3 program type

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                AddBlock<CreateEntMsgRepeatScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                AddBlock<RepeatScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Repeat schedule request").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.StartDate).WithValue(DateTime.Now.Date).
                        SetProperty(x => x.EndDate).WithValue(DateTime.Now.Date.AddDays(10)).
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).

                    AddBlock<GetScheduleTestBlock>().
                        SetDescription("Get schedule test block").
                        SetProperty(x => x.GetScheduleRequest).
                            WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock").
                            FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                    AddBlock<RepeatScheduleAssertBlock>().

            ExecuteFlow();
        }

        #region Update time frame
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_AlcoholTest()
        {
            Schedule_UpdateTimeFrame_Offender_AlcoholTest(
                TestingFlow, 1, true).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_AlcoholTest()
        {
            Schedule_UpdateTimeFrame_Offender_AlcoholTest(
                TestingFlow, 1, false).ExecuteFlow();

        }

        private ITestingFlow Schedule_UpdateTimeFrame_Offender_AlcoholTest(ITestingFlow testingFlow, int NumberOfTests, bool IsWeekly)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VB_Cellular).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.AlcoholTest).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.MEMSSchedule[0].ID).
                     //SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(5)).
                     //SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(5).AddHours(1)).
                     SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.NumberOfTests).WithValue(NumberOfTests).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateAlcoholTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse);

            return testingFlow;
        }



        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_MustBeInToDontCare_Fail()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<ValidationFaultExceptionAssertBlock>(
                             ValidationFaultExceptionScheduleAssertBlock.ValidateSchedule_IsWeekly_TimeFrameType_WS760()).
                ExecuteFlow();

        }



        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_MustBeOutToDontCare()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                //SetProperty(x => x.BufferRadius).WithValue(10).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").


                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCareToMustBeOut()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                //SetProperty(x => x.BufferRadius).WithValue(10).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").


                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_MustBeInToDontCare()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").
                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCareToMustBeIn()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").


                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_DontCare_Exclusion_1Piece()
        {
            Schedule_UpdateTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();

        }



        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_DontCare_Exclusion_2Piece()
        {
            Schedule_UpdateTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_MustBeIn_1Piece()
        {
            Schedule_UpdateTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_MustBeIn_2Piece()
        {
            Schedule_UpdateTimeFrame_Offender_Isweekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();

        }


        private ITestingFlow Schedule_UpdateTimeFrame_Offender_Isweekly(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        {
            testingFlow = AddOffender(TestingFlow, ProgramType);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = AddInclusionZone(testingFlow);
            else
                testingFlow = AddExclusionZone(testingFlow);

            testingFlow = AddTimeFrame(testingFlow, TimeFrameType, true);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = UpdateWeeklyTimeFrameForInclusionZone(testingFlow);
            else
                testingFlow = UpdateWeeklyTimeFrameForExclusionZone(testingFlow);

            testingFlow = UpdateWeeklyTimeFrameAssert(testingFlow);

            return testingFlow;
        }

        private ITestingFlow Schedule_UpdateTimeFrame_Offender_NotIsWeekly(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        {
            testingFlow = AddOffender(TestingFlow, ProgramType);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = AddInclusionZone(testingFlow);
            else
                testingFlow = AddExclusionZone(testingFlow);

            testingFlow = AddTimeFrame(testingFlow, TimeFrameType, false);

            if (LimitationType == Zones0.EnmLimitationType.Inclusion)
                testingFlow = UpdateCalenderTimeFrameForInclusionZone(testingFlow);
            else
                testingFlow = UpdateCalenderTimeFrameForExclusionZone(testingFlow);

            testingFlow = UpdateCalenderTimeFrameAssert(testingFlow);

            return testingFlow;
        }

        private ITestingFlow UpdateWeeklyTimeFrameForInclusionZone(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateMsgAddCircularZoneRequestBlock_Update").
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 31, LON = 33 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                SetName("AddCircularZoneTestBlock_Update").
                    SetDescription("Add Circular Zone test block").
                    SetProperty(x => x.AddCircularZoneRequest).
                    WithValueFromBlockIndex("CreateMsgAddCircularZoneRequestBlock_Update").
                    FromDeepProperty<CreateMsgAddCircularZoneRequestBlock>(x => x.AddCircularZoneRequest).

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).
                     WithValueFromBlockIndex("AddCircularZoneTestBlock_Update").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block");

            return testingFlow;
        }

        private ITestingFlow UpdateWeeklyTimeFrameForExclusionZone(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateMsgAddCircularZoneRequestBlock_Update").
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 31, LON = 33 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                SetName("AddCircularZoneTestBlock_Update").
                    SetDescription("Add Circular Zone test block").
                    SetProperty(x => x.AddCircularZoneRequest).
                    WithValueFromBlockIndex("CreateMsgAddCircularZoneRequestBlock_Update").
                    FromDeepProperty<CreateMsgAddCircularZoneRequestBlock>(x => x.AddCircularZoneRequest).

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).
                     WithValueFromBlockIndex("AddCircularZoneTestBlock_Update").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block");

            return testingFlow;
        }

        private ITestingFlow UpdateCalenderTimeFrameForInclusionZone(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateMsgAddCircularZoneRequestBlock_Update").
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 31, LON = 33 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                SetName("AddCircularZoneTestBlock_Update").
                    SetDescription("Add Circular Zone test block").
                    SetProperty(x => x.AddCircularZoneRequest).
                    WithValueFromBlockIndex("CreateMsgAddCircularZoneRequestBlock_Update").
                    FromDeepProperty<CreateMsgAddCircularZoneRequestBlock>(x => x.AddCircularZoneRequest).

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).
                     WithValueFromBlockIndex("AddCircularZoneTestBlock_Update").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block");

            return testingFlow;
        }

        private ITestingFlow UpdateCalenderTimeFrameForExclusionZone(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateMsgAddCircularZoneRequestBlock_Update").
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 31, LON = 33 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                SetName("AddCircularZoneTestBlock_Update").
                    SetDescription("Add Circular Zone test block").
                    SetProperty(x => x.AddCircularZoneRequest).
                    WithValueFromBlockIndex("CreateMsgAddCircularZoneRequestBlock_Update").
                    FromDeepProperty<CreateMsgAddCircularZoneRequestBlock>(x => x.AddCircularZoneRequest).

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).
                     WithValueFromBlockIndex("AddCircularZoneTestBlock_Update").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block");

            return testingFlow;
        }

        private ITestingFlow UpdateWeeklyTimeFrameAssert(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateWeeklyTimeframeAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse);

            return testingFlow;
        }

        private ITestingFlow UpdateCalenderTimeFrameAssert(ITestingFlow testingFlow)
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
        SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse);

            return testingFlow;
        }


        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_DontCare_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateWeeklyTimeframeAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_MustBeOut_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateWeeklyTimeframeAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_DontCareToMustBeOut_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateWeeklyTimeframeAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_MustBeOutToDontCare_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateWeeklyTimeframeAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCare_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_MustBeOut_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCareToMustBeOut_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_MustBeOutToDontCare_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).
                    SetProperty(x => x.LocationID).WithValue(null).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddMonths(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddMonths(1).AddHours(1)).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.StartTime).
                        SetProperty(x => x.EndDate).
                    WithValueFromBlockIndex("UpdateTimeFrameTestBlock").
                        FromDeepProperty<UpdateTimeFrameTestBlock>(x => x.UpdateTimeFrameRequest.EndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateTimeFrameAssertBlock>().
                    SetDescription("Update Schedule Assert block").
                    SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).
                ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_VoiceTest_Incoming()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2), 1, DateTime.Now.Date, DateTime.Now.Date.AddDays(3)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_VoiceTest_Incoming24()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming24, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2).AddHours(23).AddMinutes(59), 1, DateTime.Now.Date, DateTime.Now.Date.AddDays(3).AddHours(23).AddMinutes(59)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_VoiceTest_OutAndIn()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.OutAndIn, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2).AddHours(1), 2, DateTime.Now.Date, DateTime.Now.Date.AddDays(3).AddHours(1)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_IsWeekly_VoiceTest_Outgoing()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Outgoing, 1, true, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2).AddHours(1), 2, DateTime.Now.Date, DateTime.Now.Date.AddDays(3).AddHours(1)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_VoiceTest_Incoming()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2), 1, DateTime.Now.Date, DateTime.Now.Date.AddDays(3)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_VoiceTest_Incoming24()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Incoming24, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2).AddHours(23).AddMinutes(59), 1, DateTime.Now.Date, DateTime.Now.Date.AddDays(3).AddHours(23).AddMinutes(59)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_VoiceTest_OutAndIn()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.OutAndIn, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2).AddHours(1), 2, DateTime.Now.Date, DateTime.Now.Date.AddDays(3).AddHours(1)).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_VoiceTest_Outgoing()
        {
            Schedule_UpdateTimeFrame_Offender_VoiceTest(
                TestingFlow, Schedule0.EnmVoiceTestCallType.Outgoing, 1, false, DateTime.Now.Date.AddDays(1), DateTime.Now.Date.AddDays(1).AddHours(1), DateTime.Now.Date.AddDays(2), DateTime.Now.Date.AddDays(2).AddHours(1), 2, DateTime.Now.Date, DateTime.Now.Date.AddDays(3).AddHours(1)).ExecuteFlow();
        }

        private ITestingFlow Schedule_UpdateTimeFrame_Offender_VoiceTest(ITestingFlow testingFlow, Schedule0.EnmVoiceTestCallType VoiceTestCallType, int NumberOfTests, bool IsWeekly, DateTime StartTime, DateTime EndTime, DateTime UpdateStartTime, DateTime UpdateEndTime, int UpdateNumberOfTests, DateTime GetScheduleStartTime, DateTime GetScheduleEndTime)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Voice_Verification).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(StartTime).
                    SetProperty(x => x.EndTime).WithValue(EndTime).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).WithValue(null).
                    //    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    //        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.VoiceTest).
                    SetProperty(x => x.VoiceTestCallType).WithValue(VoiceTestCallType).
                //SetProperty(x => x.NumberOfTests).WithValue(NumberOfTests).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).WithValue(GetScheduleStartTime).
                    SetProperty(x => x.EndDate).WithValue(GetScheduleEndTime).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

                AddBlock<CreateEntMsgUpdateTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").
                    SetProperty(x => x.TimeframeID).
                        WithValueFromBlockIndex("GetScheduleTestBlock").
                            FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.VoiceSchedule[0].ID).
                    SetProperty(x => x.StartTime).WithValue(UpdateStartTime).
                    SetProperty(x => x.EndTime).WithValue(UpdateEndTime).
                    SetProperty(x => x.NumberOfTests).WithValue(UpdateNumberOfTests).

                AddBlock<UpdateTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Time Frame test block").

                AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).
                    SetProperty(x => x.StartDate).WithValue(GetScheduleStartTime).
                    SetProperty(x => x.EndDate).WithValue(GetScheduleEndTime).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

                AddBlock<UpdateVoiceTimeframeAssertBlock>().
                    SetDescription("Update Schedule Assert block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse);

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCare_Inclusion_1Piece()
        {
            Schedule_UpdateTimeFrame_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCare_Inclusion_2Piece()
        {
            Schedule_UpdateTimeFrame_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCare_Exclusion_1Piece()
        {
            Schedule_UpdateTimeFrame_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_DontCare_Exclusion_2Piece()
        {
            Schedule_UpdateTimeFrame_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_MustBeIn_1Piece()
        {
            Schedule_UpdateTimeFrame_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateTimeFrame_Offender_NotIsWeekly_MustBeIn_2Piece()
        {
            Schedule_UpdateTimeFrame_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn).ExecuteFlow();

        }


        #endregion

        #region Update schedule
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_MustBeIn_1Piece()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_MustBeIn_2Piece()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_Inclusion_1Piece()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_Inclusion_2Piece()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_Exclusion_1Piece()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare, 10).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_E4()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(true).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.E4TimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.E4ScheduleTimeStamp).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateWeeklyScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_MustBeOut_E4()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(true).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.E4TimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.E4ScheduleTimeStamp).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateWeeklyScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_Exclusion_2Piece()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare, 10).ExecuteFlow();
        }

        private ITestingFlow Schedule_UpdateSchedule_Offender_IsWeekly(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType, int BufferRadius)
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               //AddBlock<TimeFrameToIgnoreTimeFrameObjectTestBlock>().
               //SetProperty(x => x.TimeframeID).
               //WithValueFromBlockIndex("GetScheduleTestBlock").
               //        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
               //        SetProperty(x => x.SkipDate).WithValue(DateTime.Now.Date.AddDays(9)).

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(true).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.SkipDates).WithValue(new Schedule0.EntIgnoreTimeFrame[1]).
               //SetProperty(x => x.SkipDates[0]).
               //WithValueFromBlockIndex("TimeFrameToIgnoreTimeFrameObjectTestBlock").
               //FromDeepProperty<TimeFrameToIgnoreTimeFrameObjectTestBlock>(x => x.IgnoreTimeFrame).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateWeeklyScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_MustBeIn_IgnoreTimeFrame_1Piece()
        {
            Schedule_UpdateSchedule_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_MustBeIn_IgnoreTimeFrame_2Piece()
        {
            Schedule_UpdateSchedule_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.MustBeIn, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_DontCare_Inclusion_IgnoreTimeFrame_1Piece()
        {
            Schedule_UpdateSchedule_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_DontCare_Inclusion_IgnoreTimeFrame_2Piece()
        {
            Schedule_UpdateSchedule_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_DontCare_Exclusion_IgnoreTimeFrame_1Piece()
        {
            Schedule_UpdateSchedule_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare, 10).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_DontCare_Exclusion_IgnoreTimeFrame_2Piece()
        {
            Schedule_UpdateSchedule_Offender_NotIsWeekly(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare, 10).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_DontCare_E4()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(false).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.E4TimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.E4ScheduleTimeStamp).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block").
                SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_MustBeOut_E4()
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).WithValue(null).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(false).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.E4TimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.E4ScheduleTimeStamp).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block").
                SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse).

                ExecuteFlow();
        }

        private ITestingFlow Schedule_UpdateSchedule_Offender_NotIsWeekly(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType, int BufferRadius)
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               //AddBlock<TimeFrameToIgnoreTimeFrameObjectTestBlock>().
               //SetProperty(x => x.TimeframeID).
               //WithValueFromBlockIndex("GetScheduleTestBlock").
               //        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
               //        SetProperty(x => x.SkipDate).WithValue(DateTime.Now.Date.AddDays(9)).

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(false).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.CalendarSchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.SkipDates).WithValue(new Schedule0.EntIgnoreTimeFrame[1]).
               //SetProperty(x => x.SkipDates[0]).
               //WithValueFromBlockIndex("TimeFrameToIgnoreTimeFrameObjectTestBlock").
               //FromDeepProperty<TimeFrameToIgnoreTimeFrameObjectTestBlock>(x => x.IgnoreTimeFrame).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block").
            SetProperty(x => x.GetScheduleResponse).
                        WithValueFromBlockIndex("GetScheduleTestBlock_After").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse);

            return testingFlow;
        }

        private ITestingFlow Schedule_UpdateSchedule_Offender_AlcoholTest(ITestingFlow testingFlow, int NumberOfTests, bool IsWeekly)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VB_Cellular).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                    SetProperty(x => x.LocationID).WithValue(null).
                    //    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    //        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.AlcoholTest).
                //SetProperty(x => x.NumberOfTests).WithValue(NumberOfTests).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(IsWeekly).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.AlcoholTest).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("AddTimeFrameTestBlock").
                        FromDeepProperty<AddTimeFrameTestBlock>(x => x.AddTimeFrameResponse.NewTimeFrameID).
               //SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               //SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.NumberOfTests).WithValue(2).
               SetProperty(x => x.AlcoholTestTimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.MEMSScheduleTimeStamp).
               SetProperty(x => x.RFTimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.RFScheduleTimeStamp).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateAlcoholScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_AlcoholTest()
        {
            Schedule_UpdateSchedule_Offender_AlcoholTest(
                TestingFlow, 1, true).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_NotIsWeekly_AlcoholTest()
        {
            Schedule_UpdateSchedule_Offender_AlcoholTest(
                TestingFlow, 1, false).ExecuteFlow();

        }


        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_Inclusion_IgnoreTimeFrame()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly_IgnoreTimeFrame(
                TestingFlow, Zones0.EnmLimitationType.Inclusion, Schedule0.EnmTimeFrameType.DontCare, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_UpdateSchedule_Offender_IsWeekly_DontCare_Exclusion_IgnoreTimeFrame()
        {
            Schedule_UpdateSchedule_Offender_IsWeekly_IgnoreTimeFrame(
                TestingFlow, Zones0.EnmLimitationType.Exclusion, Schedule0.EnmTimeFrameType.DontCare, 10).ExecuteFlow();
        }

        private ITestingFlow Schedule_UpdateSchedule_Offender_IsWeekly_IgnoreTimeFrame(ITestingFlow testingFlow, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType, int BufferRadius)
        {
            TestingFlow.
               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(true).
                    SetProperty(x => x.LocationID).WithValue(null).
                    //WithValueFromBlockIndex("AddCircularZoneTestBlock").
                    //    FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Schedule test block").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Schedule test block").

               AddBlock<TimeFrameToIgnoreTimeFrameObjectTestBlock>().
               SetProperty(x => x.TimeframeID).
               WithValueFromBlockIndex("GetScheduleTestBlock").
                       FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
                       SetProperty(x => x.SkipDate).WithValue(DateTime.Now.Date.AddDays(9)).

               AddBlock<CreateEntMsgUpdateScheduleRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntityID).
                           WithValueFromBlockIndex("AddOffenderTestBlock").
                               FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                SetProperty(x => x.IsWeekly).WithValue(true).
                SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.DontCare).
                SetProperty(x => x.TimeframeID).
                    WithValueFromBlockIndex("GetScheduleTestBlock").
                        FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.WeeklySchedule[0].ID).
               SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(2)).
               SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(2).AddHours(2)).
               SetProperty(x => x.SkipDates).WithValue(new Schedule0.EntIgnoreTimeFrame[1]).
               SetProperty(x => x.SkipDates[0]).
               WithValueFromBlockIndex("TimeFrameToIgnoreTimeFrameObjectTestBlock").
               FromDeepProperty<TimeFrameToIgnoreTimeFrameObjectTestBlock>(x => x.IgnoreTimeFrame).
               SetProperty(x => x.E4TimeStamp).
               WithValueFromBlockIndex("GetScheduleTestBlock").
               FromDeepProperty<GetScheduleTestBlock>(x => x.GetScheduleResponse.Schedule.E4ScheduleTimeStamp).

               AddBlock<UpdateScheduleTestBlock>().UsePreviousBlockData().
               SetDescription("Delete TimeFrame by update request").

               AddBlock<CreateEntMsgGetScheduleRequestBlock>().UsePreviousBlockData().
                    SetDescription("Get schedule request").
                    SetName("CreateEntMsgGetScheduleRequestBlock_After").
                    SetProperty(x => x.EntityID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                AddBlock<GetScheduleTestBlock>().
                SetName("GetScheduleTestBlock_After").
                    SetDescription("Get schedule test block").
                    SetProperty(x => x.GetScheduleRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetScheduleRequestBlock_After").
                        FromDeepProperty<CreateEntMsgGetScheduleRequestBlock>(x => x.GetScheduleRequest).

               AddBlock<UpdateWeeklyScheduleAssertBlock>().
               SetDescription("Assert Schedule Test Block");

            return testingFlow;
        }
        #endregion

        #region Get schedule parameter
        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_GetScheduleParameter_Offender()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeOut).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                     AddBlock<CreateEntMsgGetScheduleParameterRequest>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                        SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                    AddBlock<GetScheduleParametersTestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule test block").

            //AddBlock<GetScheduleAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Schedule_Regression)]
        [TestMethod]
        public void Schedule_GetScheduleParameter_Group()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    //SetProperty(x => x.Name).WithValue("New group").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateEntMsgAddTimeFrameRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Time Frame Request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.Date.AddDays(1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.Date.AddDays(1).AddHours(1)).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.IsWeekly).WithValue(false).
                    SetProperty(x => x.LocationID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.TimeFrameType).WithValue(Schedule0.EnmTimeFrameType.MustBeIn).

                AddBlock<AddTimeFrameTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Time Frame test block").

                    AddBlock<CreateEntMsgGetScheduleParameterRequest>().UsePreviousBlockData().
                        SetDescription("Get schedule request").
                        SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                        SetProperty(x => x.EntityType).WithValue(Schedule0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(ScheduleProxy.PlannedVersion).

                    AddBlock<GetScheduleParametersTestBlock>().UsePreviousBlockData().
                        SetDescription("Get schedule test block").

            //AddBlock<GetScheduleAssertBlock>().

            ExecuteFlow();
        }
        #endregion
    }
}
