﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Common.Categories;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using TestBlocksLib.DevicesBlocks;
using AssertBlocksLib.DevicesAsserts;

using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.Proxies.API.Agency;
using TestingFramework.Proxies.API.Users;
using Users = TestingFramework.Proxies.EM.Interfaces.Users;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Agency0 = TestingFramework.Proxies.EM.Interfaces.Agency;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;

namespace TestingFramework.UnitTests.EquipmentSvcTests
{
    [TestClass]
    [DeploymentItem("ConfigurationFiles", "ConfigurationFiles")]
    [DeploymentItem("EncryptionP8.dll")]
    public class EquipmentSvcLoadTests : BaseUnitTest
    {
        public const string CreateEntMsgAddOffenderRequestBlock_CsvFileName = @"DataSource\\EquipmentSvc_LoadEquipment.csv";
        public const string Offenders_List_Pre_Active_Offenders_CsvFileName = @"DataSource\\Offenders_List_Pre_Active_Offenders.csv";

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + CreateEntMsgAddOffenderRequestBlock_CsvFileName, "EquipmentSvc_LoadEquipment#csv", DataAccessMethod.Sequential),
            DeploymentItem(CreateEntMsgAddOffenderRequestBlock_CsvFileName)]
        [TestCategory(CategoriesNames.Equipment_Service_Load)]
        [Description("test for Task 23813: Add Devices to the system using API")]
        public void EquipmentSvc_LoadEquipmentFromFile()
        {
            DataRow currentDataRow = TestContext.DataRow;

            Offenders0.EnmProgramType Offender_ProgramType;
            Enum.TryParse(currentDataRow["Offender_ProgramType"].ToString(), out Offender_ProgramType);

            Equipment0.EnmEquipmentModel RX_Type;
            Enum.TryParse(currentDataRow["RX_Type"].ToString(), out RX_Type);

            Equipment0.EnmEquipmentModel TX_Type;
            Enum.TryParse(currentDataRow["TX_Type"].ToString(), out TX_Type);

            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.OfficerID).WithValue(currentDataRow["OfficerID"]).
                    SetProperty(x => x.RefID).WithValue(currentDataRow["RefID"].ToString()).
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offender_ProgramType).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.FirstName).WithValue(currentDataRow["FirstNamePrefix"].ToString()).
                    SetProperty(x => x.MiddleName).WithValue(currentDataRow["MiddleNamePrefix"].ToString()).
                    SetProperty(x => x.LastName).WithValue(currentDataRow["LastNamePrefix"].ToString()).
                    SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male).
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get the timeStamp for update").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block to get the timeStamp for update").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add transmitter Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(TX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["TX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("add transmitter Request test block").
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add reciver Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(RX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["RX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("AddEquipmentTestBlock_RX").
                    SetDescription("add reciver Request test block").
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_RX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(currentDataRow["ProviderID"]).
                    SetProperty(x => x.VoicePrefixPhone).WithValue(currentDataRow["VoicePrefixPhone"].ToString()).
                    SetProperty(x => x.VoicePhoneNumber).WithValue(currentDataRow["VoicePhoneNumber"].ToString()).
                    SetProperty(x => x.DataPrefixPhone).WithValue(currentDataRow["DataPrefixPhone"].ToString()).
                    SetProperty(x => x.DataPhoneNumber).WithValue(currentDataRow["DataPhoneNumber"].ToString()).
                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).//
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Transmitter).WithValue(currentDataRow["TX_SerialNumber"].ToString()).
                    SetProperty(x => x.Receiver).WithValue(currentDataRow["RX_SerialNumber"].ToString()).
                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                        SetDescription("Update Offender Test Block").
                    ExecuteFlow();
        }

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + CreateEntMsgAddOffenderRequestBlock_CsvFileName, "EquipmentSvc_LoadEquipment#csv", DataAccessMethod.Sequential),
            DeploymentItem(CreateEntMsgAddOffenderRequestBlock_CsvFileName)]
        [TestCategory(CategoriesNames.Equipment_Service_Load)]
        [Description("test for Task 23813: Add Home unit to offender using API")]
        public void EquipmentSvc_AddBaseUnitFromFile()
        {
            DataRow currentDataRow = TestContext.DataRow;

            if(string.IsNullOrEmpty(currentDataRow["BaseUnit_SerialNumber"].ToString()) == true)
            {
                Log.Info("No BaseUnit_SerialNumber column data. skipping the test.");
                    return;
            }
            var enmEquipmentModel = new Equipment0.EnmEquipmentModel();
            Enum.TryParse(currentDataRow["BaseUnit_Model"].ToString(), out enmEquipmentModel);

            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgAddEquipmentRequest type").
                    SetName("BaseUnit").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(enmEquipmentModel).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["BaseUnit_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add BaseUnit test block").
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by RefId").
                    SetProperty(x => x.OffenderRefIDQueryParameter).WithValue(
                        new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = currentDataRow["RefID"].ToString() }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Offender Request for the Base Unit").
                    SetProperty(x=>x.HomeUnit).
                        WithValueFromBlockIndex("BaseUnit").FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.Transmitter).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.TransmitterSN).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x=> x.Timestamp).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].AgencyID).
                    SetProperty(x => x.OfficerID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.FirstName).WithValue(currentDataRow["FirstNamePrefix"].ToString()).
                    SetProperty(x => x.MiddleName).WithValue(currentDataRow["MiddleNamePrefix"].ToString()).
                    SetProperty(x => x.LastName).WithValue(currentDataRow["LastNamePrefix"].ToString()).
                    SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male).
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                ExecuteFlow();
            }



        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + Offenders_List_Pre_Active_Offenders_CsvFileName, "Offenders_List_Pre_Active_Offenders#csv", DataAccessMethod.Sequential),
            DeploymentItem(Offenders_List_Pre_Active_Offenders_CsvFileName)]
        [TestCategory(CategoriesNames.Devices_Tests_Load)]
        [Description("Use the SN from the file for the call to commbox")]
        public void CommBox_ConfigurationChanges_FirstMiddleLastName_FromFile()
        {
            DataRow currentDataRow = TestContext.DataRow;

            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by RefId").
                    SetProperty(x => x.OffenderRefIDQueryParameter).WithValue(
                        new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = currentDataRow["RefID"].ToString() }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
                    SetProperty( x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    BeginTransaction("Call").EndTransaction("Call").
                AddBlock<CommandUserNameAssertBlock>().
                ExecuteFlow();
        }

        public const string Chille_Users_CsvFileName = @"DataSource\\Chille_Users.csv";

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", 
            "|DataDirectory|\\" + Chille_Users_CsvFileName, 
            "Chille_Users#csv", 
            DataAccessMethod.Sequential),
            DeploymentItem(Chille_Users_CsvFileName)]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Users_Tests)]
        [Description("add users to environment based on the AD users.")]
        public void Environment_Preparation_Users()
        {
            DataRow currentDataRow = TestContext.DataRow;

            var UserName = currentDataRow["Username"].ToString();

            //check if the user exist in the DB
            var entMsgGetUsersListRequest = new Proxies.EM.Interfaces.Users.EntMsgGetUsersListRequest();
            entMsgGetUsersListRequest.UserName = UserName;
            entMsgGetUsersListRequest.IncludeDeletedUsers = true;
            var getUsersResponse = Proxies.API.Users.UsersProxy.Instance.GetUsersList(entMsgGetUsersListRequest);
            if( getUsersResponse.UsersList.Length == 1)
            {
                Log.Info($"User {UserName} exist in DB.");
                return;
            }
            //check if the user exist in the AD
            var adh = new Proxies.Tools.ActiveDirectiryHelper();
            try
            {
                var adUser = adh.GetUserByUserName(UserName);
                if( adUser == null)
                {
                    Log.Info($"User {UserName} not exist in AD");
                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Error($"faild to get user {UserName} from AD . error : {ex.Message}.");
                return;
            }

            //add the user
            var theUser = new Proxies.EM.Interfaces.Users.EntUser();
            var entMsgAddUserRequest = new Proxies.EM.Interfaces.Users.EntMsgAddUserRequest();
            entMsgAddUserRequest.NewUser = new Proxies.EM.Interfaces.Users.EntUser();
            var entMsgGetUsersListRequestAfterAdd = new Proxies.EM.Interfaces.Users.EntMsgGetUsersListRequest();
            entMsgGetUsersListRequestAfterAdd.UserID = Proxies.API.Users.UsersProxy.Instance.AddUser(entMsgAddUserRequest);
            getUsersResponse = Proxies.API.Users.UsersProxy.Instance.GetUsersList(entMsgGetUsersListRequestAfterAdd);

            theUser = getUsersResponse.UsersList[0];//only 1 expected
            
            //update to active
            theUser.UserStatus = Proxies.EM.Interfaces.Users.EnmUserStatus.Active;
            var entMsgUpdateUserRequest = new Proxies.EM.Interfaces.Users.EntMsgUpdateUserRequest() { UpdatedUser = theUser };
            Proxies.API.Users.UsersProxy.Instance.UpdateUser(entMsgUpdateUserRequest);
            
            //var entMsgGetUsersListRequest = new EntMsgGetUsersListRequest();
            //EntMsgGetUsersListRequest entMsgGetUsersListRequestAfterAdd = new EntMsgGetUsersListRequest();

            //entMsgGetUsersListRequest.UserName = "bla";
            //entMsgGetUsersListRequest.IncludeDeletedUsers = true;
            //var getUsersResponse = UsersProxy.Instance.GetUsersList(entMsgGetUsersListRequest);

            //var theUser = new EntUser();
            //if (getUsersResponse.UsersList.Length == 0)
            //{//need to add the user
            //    var entMsgAddUserRequest = new EntMsgAddUserRequest();
            //    entMsgAddUserRequest.NewUser = new EntUser();

            //    entMsgGetUsersListRequestAfterAdd.UserID = UsersProxy.Instance.AddUser(entMsgAddUserRequest);
            //    getUsersResponse = UsersProxy.Instance.GetUsersList(entMsgGetUsersListRequestAfterAdd);

            //    theUser = getUsersResponse.UsersList[0];//only 1 expected
            //}
            //else
            //{
            //    theUser = getUsersResponse.UsersList[0];
            //}
            ////activate the user
            //theUser.UserStatus = Proxies.EM.Interfaces.Users.EnmUserStatus.Active;
            //EntMsgUpdateUserRequest entMsgUpdateUserRequest = new EntMsgUpdateUserRequest() { UpdatedUser = theUser };
            //UsersProxy.Instance.UpdateUser(entMsgUpdateUserRequest);
        }


        public const string EquipmentSvc_ChilleData_Offenders__Agg_Vic_Equipment_CsvFileName = @"DataSource\\EquipmentSvc_ChilleData_Offenders__Agg_Vic_Equipment.csv";
        public const string EquipmentSvc_Argentina_Offenders_TwoPeace_Regular_Equipment = @"DataSource\\EquipmentSvc_Argentina_Offenders_TwoPeace_Regular_Equipment.csv";
        public const string EquipmentSvc_Argentina_Offenders_RF_Curfew_Dual_Equipment = @"DataSource\\EquipmentSvc_Argentina_Offenders_RF_Curfew_Dual_Equipment.csv";

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", 
                "|DataDirectory|\\" + EquipmentSvc_ChilleData_Offenders__Agg_Vic_Equipment_CsvFileName,
                "EquipmentSvc_ChilleData_Offenders__Agg_Vic_Equipment#csv", DataAccessMethod.Sequential),
            DeploymentItem(EquipmentSvc_ChilleData_Offenders__Agg_Vic_Equipment_CsvFileName)]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Load DV Pairs the system using API")]
        public void Environment_Preparation_Load_DV_Pairs()
        {
            DataRow currentDataRow = TestContext.DataRow;

            Offenders0.EnmProgramType Agg_Offender_ProgramType;
            Enum.TryParse(currentDataRow["Agg_Offender_ProgramType"].ToString(), out Agg_Offender_ProgramType);

            Equipment0.EnmEquipmentModel Agg_RX_Type;
            Enum.TryParse(currentDataRow["Agg_RX_Type"].ToString(), out Agg_RX_Type);

            Equipment0.EnmEquipmentModel Agg_TX_Type;
            Enum.TryParse(currentDataRow["Agg_TX_Type"].ToString(), out Agg_TX_Type);

            Offenders0.EnmProgramType Vic_Offender_ProgramType;
            Enum.TryParse(currentDataRow["Vic_Offender_ProgramType"].ToString(), out Vic_Offender_ProgramType);

            Equipment0.EnmEquipmentModel Vic_RX_Type;
            Enum.TryParse(currentDataRow["Vic_RX_Type"].ToString(), out Vic_RX_Type);

            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Vic Offender Request").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.OfficerID).WithValue(currentDataRow["OfficerID"]).
                    SetProperty(x => x.RefID).WithValue(currentDataRow["Vic_RefID"].ToString()).
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Vic_Offender_ProgramType).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.FirstName).WithValue(currentDataRow["Vic_FirstNamePrefix"].ToString()).
                    SetProperty(x => x.MiddleName).WithValue(currentDataRow["Vic_MiddleNamePrefix"].ToString()).
                    SetProperty(x => x.LastName).WithValue(currentDataRow["Vic_LastNamePrefix"].ToString()).
                    SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Female).
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Ms).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("Add Vic Offender Test Block").
                    SetDescription("Add Vic Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get the timeStamp for update").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Vic Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("Get Vic Offenders Test Block").
                    SetDescription("Get Offenders Test Block to get the timeStamp for update").
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add Vic reciver Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Vic_RX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Vic_RX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("Add Vic Equipment Test Block RX").
                    SetDescription("add Vic reciver Request test block").
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create Vic Ent Msg Add Cellular Data Request Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("Add Vic Equipment Test Block RX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(currentDataRow["Vic_ProviderID"]).
                    SetProperty(x => x.VoicePrefixPhone).WithValue(currentDataRow["Vic_VoicePrefixPhone"].ToString()).
                    SetProperty(x => x.VoicePhoneNumber).WithValue(currentDataRow["Vic_VoicePhoneNumber"].ToString()).
                    SetProperty(x => x.DataPrefixPhone).WithValue(currentDataRow["Vic_DataPrefixPhone"].ToString()).
                    SetProperty(x => x.DataPhoneNumber).WithValue(currentDataRow["Vic_DataPhoneNumber"].ToString()).
                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Vic Cellular Data test block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Vic Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Vic Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).//
                         WithValueFromBlockIndex("Get Vic Offenders Test Block").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Receiver).WithValue(currentDataRow["Vic_RX_SerialNumber"].ToString()).
                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                        SetDescription("Update Vic Offender Test Block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Agg Add Offender Request").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.OfficerID).WithValue(currentDataRow["OfficerID"]).
                    SetProperty(x => x.RefID).WithValue(currentDataRow["Agg_RefID"].ToString()).
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Agg_Offender_ProgramType).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.FirstName).WithValue(currentDataRow["Agg_FirstNamePrefix"].ToString()).
                    SetProperty(x => x.MiddleName).WithValue(currentDataRow["Agg_MiddleNamePrefix"].ToString()).
                    SetProperty(x => x.LastName).WithValue(currentDataRow["Agg_LastNamePrefix"].ToString()).
                    SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male).
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.Receiver).WithValue(null).//later we will set new RX(to avoid use the rcv from the vic).

                    SetProperty(x => x.RelatedOffenderID).
                        WithValueFromBlockIndex("Add Vic Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("Add Agg Offender Test Block").
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get the timeStamp for update").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Agg Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("Get Agg Offenders Test Block").
                    SetDescription("Get Offenders Test Block to get the timeStamp for update").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add transmitter Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Agg_TX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Agg_TX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("add transmitter Request test block").
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add reciver Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Agg_RX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Agg_RX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("AddEquipmentTestBlock_RX").
                    SetDescription("add reciver Request test block").
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_RX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(currentDataRow["Agg_ProviderID"]).
                    SetProperty(x => x.VoicePrefixPhone).WithValue(currentDataRow["Agg_VoicePrefixPhone"].ToString()).
                    SetProperty(x => x.VoicePhoneNumber).WithValue(currentDataRow["Agg_VoicePhoneNumber"].ToString()).
                    SetProperty(x => x.DataPrefixPhone).WithValue(currentDataRow["Agg_DataPrefixPhone"].ToString()).
                    SetProperty(x => x.DataPhoneNumber).WithValue(currentDataRow["Agg_DataPhoneNumber"].ToString()).
                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Agg Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).//
                         WithValueFromBlockIndex("Get Agg Offenders Test Block").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Transmitter).WithValue(currentDataRow["Agg_TX_SerialNumber"].ToString()).
                    SetProperty(x => x.Receiver).WithValue(currentDataRow["Agg_RX_SerialNumber"].ToString()).
                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                        SetDescription("Update Offender Test Block").
                    ExecuteFlow();
        }

        [TestMethod,
   DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
       "|DataDirectory|\\" + EquipmentSvc_Argentina_Offenders_TwoPeace_Regular_Equipment,
       "EquipmentSvc_Argentina_Offenders_TwoPeace_Regular_Equipment#csv", DataAccessMethod.Sequential),
   DeploymentItem(EquipmentSvc_Argentina_Offenders_TwoPeace_Regular_Equipment)]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Load 2P Regular Pairs the system using API")]


        public void Environment_Preparation_Load_2P_Regular()
        {
            DataRow currentDataRow = TestContext.DataRow;

            //Two_Piece
            Offenders0.EnmProgramType Agg_Offender_ProgramType;
            Enum.TryParse(currentDataRow["Agg_Offender_ProgramType"].ToString(), out Agg_Offender_ProgramType);

            Equipment0.EnmEquipmentModel Agg_RX_Type;
            Enum.TryParse(currentDataRow["Agg_RX_Type"].ToString(), out Agg_RX_Type);
            //Change to Transmitter_Triple_Tamper
            Equipment0.EnmEquipmentModel Agg_TX_Type;
            Enum.TryParse(currentDataRow["Agg_TX_Type"].ToString(), out Agg_TX_Type);

            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Agg Add Offender Request").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.OfficerID).WithValue(currentDataRow["OfficerID"]).
                    SetProperty(x => x.RefID).WithValue(currentDataRow["Agg_RefID"].ToString()).
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Agg_Offender_ProgramType).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.FirstName).WithValue(currentDataRow["Agg_FirstNamePrefix"].ToString()).
                    SetProperty(x => x.MiddleName).WithValue(currentDataRow["Agg_MiddleNamePrefix"].ToString()).
                    SetProperty(x => x.LastName).WithValue(currentDataRow["Agg_LastNamePrefix"].ToString()).
                    SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male).
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.Receiver).WithValue(null).//later we will set new RX(to avoid use the rcv from the vic).
                                                                 //???
                                                                 //SetProperty(x => x.RelatedOffenderID).
                                                                 //    WithValueFromBlockIndex("Add Vic Offender Test Block").
                                                                 //        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("Add Agg Offender Test Block").
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get the timeStamp for update").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Agg Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("Get Agg Offenders Test Block").
                    SetDescription("Get Offenders Test Block to get the timeStamp for update").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add transmitter Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Agg_TX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Agg_TX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("add transmitter Request test block").
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add reciver Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Agg_RX_Type).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Agg_RX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("AddEquipmentTestBlock_RX").
                    SetDescription("add reciver Request test block").
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_RX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(currentDataRow["Agg_ProviderID"]).
                    SetProperty(x => x.VoicePrefixPhone).WithValue(currentDataRow["Agg_VoicePrefixPhone"].ToString()).
                    SetProperty(x => x.VoicePhoneNumber).WithValue(currentDataRow["Agg_VoicePhoneNumber"].ToString()).
                    SetProperty(x => x.DataPrefixPhone).WithValue(currentDataRow["Agg_DataPrefixPhone"].ToString()).
                    SetProperty(x => x.DataPhoneNumber).WithValue(currentDataRow["Agg_DataPhoneNumber"].ToString()).
                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Agg Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).//
                         WithValueFromBlockIndex("Get Agg Offenders Test Block").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Transmitter).WithValue(currentDataRow["Agg_TX_SerialNumber"].ToString()).
                    SetProperty(x => x.Receiver).WithValue(currentDataRow["Agg_RX_SerialNumber"].ToString()).
                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                        SetDescription("Update Offender Test Block").
                    ExecuteFlow();
        }


        [TestMethod,
           DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
               "|DataDirectory|\\" + EquipmentSvc_Argentina_Offenders_RF_Curfew_Dual_Equipment,
               "EquipmentSvc_Argentina_Offenders_RF_Curfew_Dual_Equipment#csv", DataAccessMethod.Sequential),
           DeploymentItem(EquipmentSvc_Argentina_Offenders_RF_Curfew_Dual_Equipment)]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Load DV Pairs the system using API")]
        public void Environment_Preparation_Load_E4()
        {
            DataRow currentDataRow = TestContext.DataRow;

            //E4_Dual
            Offenders0.EnmProgramType Agg_Offender_ProgramType;
            Enum.TryParse(currentDataRow["Agg_Offender_ProgramType"].ToString(), out Agg_Offender_ProgramType);
            //RF_Curfew_Dual_E4
            Equipment0.EnmEquipmentModel Agg_RX_Type;
            Enum.TryParse(currentDataRow["Agg_RX_Type"].ToString(), out Agg_RX_Type);
            //Transmitter_860
            Equipment0.EnmEquipmentModel Agg_TX_Type;
            Enum.TryParse(currentDataRow["Agg_TX_Type"].ToString(), out Agg_TX_Type);

            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Agg Add Offender Request").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.OfficerID).WithValue(currentDataRow["OfficerID"]).
                    SetProperty(x => x.RefID).WithValue(currentDataRow["Agg_RefID"].ToString()).
                    SetProperty(x => x.CityID).WithValue("CHU").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Agg_Offender_ProgramType).//***E4_Dual
                    SetProperty(x => x.StateID).WithValue("ARG").
                    SetProperty(x => x.FirstName).WithValue(currentDataRow["Agg_FirstNamePrefix"].ToString()).
                    SetProperty(x => x.MiddleName).WithValue(currentDataRow["Agg_MiddleNamePrefix"].ToString()).
                    SetProperty(x => x.LastName).WithValue(currentDataRow["Agg_LastNamePrefix"].ToString()).
                    SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male).
                    SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr).
                    //SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.Receiver).WithValue(null).//later we will set new RX(to avoid use the rcv from the vic).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("Add Agg Offender Test Block").
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get the timeStamp for update").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Agg Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetName("Get Agg Offenders Test Block").
                    SetDescription("Get Offenders Test Block to get the timeStamp for update").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add transmitter Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Agg_TX_Type).//Transmitter_860
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Agg_TX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("add transmitter Request test block").
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("add reciver Request logic block").
                    SetProperty(x => x.AgencyID).WithValue(currentDataRow["AgencyId"]).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Agg_RX_Type).//RF_Curfew_Dual_E4
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(currentDataRow["Agg_RX_SerialNumber"].ToString()).
                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("AddEquipmentTestBlock_RX").
                    SetDescription("add reciver Request test block").
                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_RX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(currentDataRow["Agg_ProviderID"]).
                    SetProperty(x => x.VoicePrefixPhone).WithValue(currentDataRow["Agg_VoicePrefixPhone"].ToString()).
                    SetProperty(x => x.VoicePhoneNumber).WithValue(currentDataRow["Agg_VoicePhoneNumber"].ToString()).
                    SetProperty(x => x.DataPrefixPhone).WithValue(currentDataRow["Agg_DataPrefixPhone"].ToString()).
                    SetProperty(x => x.DataPhoneNumber).WithValue(currentDataRow["Agg_DataPhoneNumber"].ToString()).
                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("Add Agg Offender Test Block").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).//
                         WithValueFromBlockIndex("Get Agg Offenders Test Block").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Transmitter).WithValue(currentDataRow["Agg_TX_SerialNumber"].ToString()).
                    SetProperty(x => x.Receiver).WithValue(currentDataRow["Agg_RX_SerialNumber"].ToString()).
                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                        SetDescription("Update Offender Test Block").
                    ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Send Download to all pre active offenders.")]
        public void Environment_Preparation_Send_Download_Immediate_2_All_PreActive()
        {
            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            var errorMessage = string.Empty;

            GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive };
            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);

            var offendersCount = GetOffendersResponse.OffendersList.Length;

            for (int i = 0; i < offendersCount; i++)
            {
                var offender = GetOffendersResponse.OffendersList[i];
                var entMsgSendDownloadRequest = new Proxies.EM.Interfaces.ProgramActions12.EntMsgSendDownloadRequest();
                entMsgSendDownloadRequest.OffenderID = offender.ID;
                entMsgSendDownloadRequest.Immediate = true;
                try
                {
                    Log.Debug($"start download to offender ({i+1}/{offendersCount}) offenders) RefID:{offender.RefID}, ID:{offender.ID}, Name:{offender.FirstName} {offender.LastName}");
                    var sw = System.Diagnostics.Stopwatch.StartNew();
                    Proxies.API.ProgramActions.ProgramActionsProxy.Instance.SendDownloadRequest(entMsgSendDownloadRequest);
                    sw.Stop();
                    Log.Debug($"download to offender RefID:{offender.RefID}, " +
                        $"ID:{offender.ID}, Name:{offender.FirstName} {offender.LastName} took {sw.Elapsed}");
                }
                catch (Exception ex)
                {
                    Log.Error($"Error while try to send download for offender {entMsgSendDownloadRequest.OffenderID}. error: {ex.Message}");
                }
            }
        }


        [TestMethod]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Place active Offenders Across Agencies.")]
        public void Environment_Preparation_Place_Offenders_Across_Agencies()
        {
            var dicAgency2Officer = new Dictionary<int, int>();
            //get all agencies
            Agency0.EntMsgGetAgenciesRequest entMsgGetAgenciesRequest = new Agency0.EntMsgGetAgenciesRequest();
            var GetAgenciesResponse = AgencyProxy.Instance.GetAgencies(entMsgGetAgenciesRequest);

            string[] agenciesNames = { "PERF1", "PERF2", "PERF3", "PERF4", "PERF5", "PERF6", "PERF7", "PERF8", "PERF9", "PERF10" };
            foreach (var agencyName in agenciesNames)
            {
                //var agency = from a in GetAgenciesResponse.AgenciesList where a.Name == agencyName select a;
                var agency = GetAgenciesResponse.AgenciesList.Single(a => a.Name == agencyName);
                //get officer
                var agencyId = (agency as Agency0.EntAgency).ID;
                var entMsgGetOfficersRequest = new Users.EntMsgGetOfficersListRequest() { AgencyID = agencyId };
                var getOfficersResponse = UsersProxy.Instance.GetOfficers(entMsgGetOfficersRequest);

                dicAgency2Officer.Add(agencyId, getOfficersResponse.OfficersList[0].ID);
            }

            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();

            GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active };
            GetOffendersListRequest.ProgramType = new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece };
            GetOffendersListRequest.ProgramConcept = Offenders0.EnmProgramConcept.Regular;

            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);

            var OffendersCount = GetOffendersResponse.OffendersList.Length;
            for (int i = 0, j=0; i < OffendersCount; i++, j++)
            {
                var agencyOfficerIndex = i % dicAgency2Officer.Count;

                var newAgency = dicAgency2Officer.ElementAt(agencyOfficerIndex).Key;
                var newOfficer = dicAgency2Officer.ElementAt(agencyOfficerIndex).Value;

                var reallocateActiveOffenderRequest = new Offenders0.EntMsgReallocateActiveOffenderRequest()
                {
                    OffenderID = GetOffendersResponse.OffendersList[i].ID,
                    NewAgencyID = newAgency,
                    NewOfficerID = newOfficer,
                    Comment = "Automation reallocate offender",
                    Reason = "4"//4= "Other"
                }; 
                try
                {
                    var offender = GetOffendersResponse.OffendersList[i];
                    Log.Debug($"({i+1}/{OffendersCount}) Going to reallocate active offender {offender.ID} " +
                        $"{offender.RefID} {offender.FirstName} {offender.LastName} from agency id {offender.AgencyID} to {newAgency}." +
                        $"from officer id {offender.OfficerID} to {newOfficer}");

                    Proxies.API.Offenders.OffendersProxy.Instance.ReallocateActiveOffender(reallocateActiveOffenderRequest);
                }
                catch (Exception ex)
                {
                    Log.Error($"While reallocate active offender.error:{ex.Message}");
                }
            }
        }



        [TestMethod]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Add Zones to all offenders..")]
        public void Environment_Preparation_AddZone2Offenders()
        {
            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();

            GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active };
            GetOffendersListRequest.ProgramType = new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece };
            GetOffendersListRequest.ProgramConcept = Offenders0.EnmProgramConcept.Aggressor;

            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);

            var OffendersCount = GetOffendersResponse.OffendersList.Length;
            for (int i = 0, j = 0; i < OffendersCount; i++, j++)
            {
                try
                {
                    var offender = GetOffendersResponse.OffendersList[i];

                    var AddCircularZoneRequest = new Zones0.MsgAddCircularZoneRequest();
                    AddCircularZoneRequest.BLP = null;
                    AddCircularZoneRequest.BufferRadius = 0;
                    AddCircularZoneRequest.EntityID = offender.ID;
                    AddCircularZoneRequest.EntityType = Zones0.EnmEntityType.Offender;
                    AddCircularZoneRequest.FullSchedule = false;
                    AddCircularZoneRequest.GraceTime = 42;
                    AddCircularZoneRequest.IsCurfewZone = false;
                    AddCircularZoneRequest.Limitation = Zones0.EnmLimitationType.Inclusion;
                    AddCircularZoneRequest.Name = "IncCircular1";
                    //AddCircularZoneRequest.Point = new Zones0.EntPoint() { LAT = -33.347997, LON = -70.516660 };//Camino Club de Golf 2501
                    AddCircularZoneRequest.Point = new Zones0.EntPoint() { LAT = 40.416972, LON = -3.703550 };//Plaza de la Puerta del Sol, s/n, 28013 Madrid

                    AddCircularZoneRequest.RealRadius = 50;

                    var AddZoneResponse = Proxies.API.Zones.ZonesProxy.Instance.AddCircularZone(AddCircularZoneRequest);


                    Log.Debug($"({i + 1}/{OffendersCount}) Going to add zone to offender{offender.ID} " +
                        $"{offender.RefID} {offender.FirstName} {offender.LastName} ");
                }
                catch (Exception ex)
                {
                    Log.Error($"While reallocate active offender.error:{ex.Message}");
                }
            }
        }

        [TestMethod]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Send EOS No RCV 2 All Offenders Without UploadTime")]
        public void Environment_Preparation_SendEosNr2AllOffendersWithoutUploadTime()
        {
            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();

            GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active };
            GetOffendersListRequest.SortField = Offenders0.EnmOffendersSortOptions.FirstName;
            GetOffendersListRequest.SortDirection = Offenders0.ListSortDirection.Ascending;
            GetOffendersListRequest.Limit = 1000;

            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);

            var OffendersCount = GetOffendersResponse.OffendersList.Length;
            for (int i = 0, j = 0; i < OffendersCount; i++, j++)
            {
                try
                {
                    var offender = GetOffendersResponse.OffendersList[i];

                    if(!offender.LastUploadDate.HasValue)
                    {
                        var EntMsgSendEndOfServiceRequest = new Proxies.EM.Interfaces.ProgramActions12.EntMsgSendEndOfServiceRequest();
                        EntMsgSendEndOfServiceRequest.Comment = "Automation need to remove this offender";
                        EntMsgSendEndOfServiceRequest.EndOfServiceCode = "MEOS";
                        EntMsgSendEndOfServiceRequest.EndOfServiceType = Proxies.EM.Interfaces.ProgramActions12.EnmEOSType.Default;
                        EntMsgSendEndOfServiceRequest.OffenderID = offender.ID;
                        //EntMsgSendEndOfServiceRequest.PendingRequestID = PendingRequestID;
                        EntMsgSendEndOfServiceRequest.StopProgramWithoutDeactivation = true;

                        Proxies.API.ProgramActions.ProgramActionsProxy.Instance.SendEndOfServiceRequest(EntMsgSendEndOfServiceRequest);
                        Log.Info($"going to send SendEndOfServiceRequest to offender ref_id {offender.RefID} " +
                            $"{offender.FirstName} {offender.LastName} with EOS code: {EntMsgSendEndOfServiceRequest.EndOfServiceCode}");
                    }
                }
                catch (Exception ex)
                {
                    Log.Error($"While reallocate active offender.error:{ex.Message}");
                }
            }
        }

        [TestMethod]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Update all victims last name with a sequential number")]
        public void Environment_Preparation_Update_All_Victims_Last_Name_with_sequential_umber()
        {
            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            var errorMessage = string.Empty;

            GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active };
            GetOffendersListRequest.ProgramConcept = Offenders0.EnmProgramConcept.Victim;

            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);

            var offendersCount = GetOffendersResponse.OffendersList.Length;

            for (int i = 0; i < offendersCount; i++)
            {
                try
                {
                    Offenders0.EntMsgUpdateOffenderRequest UpdateOffenderRequest = new Offenders0.EntMsgUpdateOffenderRequest();
                    UpdateOffenderRequest.ProgramStart = (DateTime)GetOffendersResponse.OffendersList[i].ProgramStart;
                    UpdateOffenderRequest.ProgramEnd = DateTime.Now.AddYears(1); //Extand the program end date by 3 years
                    UpdateOffenderRequest.OffenderID = GetOffendersResponse.OffendersList[i].ID;

                    UpdateOffenderRequest.AgencyID = GetOffendersResponse.OffendersList[i].AgencyID;
                    UpdateOffenderRequest.AllowVoiceTests = (bool)GetOffendersResponse.OffendersList[i].AllowVoiceTests;
                    UpdateOffenderRequest.CityID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].CityID;
                    UpdateOffenderRequest.ContactPhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePhoneNumber;
                    UpdateOffenderRequest.ContactPrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePrefixPhone;
                    UpdateOffenderRequest.CourtOrderNumber = GetOffendersResponse.OffendersList[i].CourtOrderNumber;
                    UpdateOffenderRequest.DateOfBirth = GetOffendersResponse.OffendersList[i].DateOfBirth;
                    UpdateOffenderRequest.DepartmentOfCorrection = GetOffendersResponse.OffendersList[i].DepartmentOfCorrection;
                    UpdateOffenderRequest.FirstName = GetOffendersResponse.OffendersList[i].FirstName;
                    UpdateOffenderRequest.Gender = GetOffendersResponse.OffendersList[i].Gender;
                    UpdateOffenderRequest.HomeUnit = GetOffendersResponse.OffendersList[i].EquipmentInfo.HomeUnitSN;
                    if (GetOffendersResponse.OffendersList[i].ContactInfo.LandLine != null)
                    {
                        UpdateOffenderRequest.LandlineDialOutPrefix = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.DialOutPrefix;
                        UpdateOffenderRequest.LandlinePhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.Phone;
                        UpdateOffenderRequest.LandlinePrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.PrefixPhone;
                    }
                    UpdateOffenderRequest.Language = GetOffendersResponse.OffendersList[i].Language;
                    UpdateOffenderRequest.LastName = $"Vic_{i+1}";
                    UpdateOffenderRequest.MiddleName = GetOffendersResponse.OffendersList[i].MiddleName;

                    UpdateOffenderRequest.OfficerID = GetOffendersResponse.OffendersList[i].OfficerID;
                    UpdateOffenderRequest.PhotoID = GetOffendersResponse.OffendersList[i].PhotoID;
                    UpdateOffenderRequest.ProgramType = (Offenders0.EnmProgramType)GetOffendersResponse.OffendersList[i].ProgramType;
                    UpdateOffenderRequest.Receiver = GetOffendersResponse.OffendersList[i].EquipmentInfo.ReceiverSN;
                    UpdateOffenderRequest.RelatedOffenderID = GetOffendersResponse.OffendersList[i].RelatedOffenderID;
                    UpdateOffenderRequest.SocialSecurity = GetOffendersResponse.OffendersList[i].SocialSecurity;
                    UpdateOffenderRequest.StateID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].StateID;
                    UpdateOffenderRequest.Street = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].Street;
                    UpdateOffenderRequest.Timestamp = GetOffendersResponse.OffendersList[i].Timestamp;
                    UpdateOffenderRequest.Title = GetOffendersResponse.OffendersList[i].Title;
                    UpdateOffenderRequest.Transmitter = GetOffendersResponse.OffendersList[i].EquipmentInfo.TransmitterSN;
                    UpdateOffenderRequest.ZipCode = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].ZipCode;

                
                    Proxies.API.Offenders.OffendersProxy.Instance.UpdateOffender(UpdateOffenderRequest);
                }
                catch (Exception ex)
                {
                    Log.Error($"Error while try to update offender {GetOffendersResponse.OffendersList[i].ID}. error: {ex.Message}");
                }
            }
        }


        [TestMethod]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Offenders_Tests)]
        [Description("Update all Aggressor last name with a sequential number")]
        public void Environment_Preparation_Update_All_Aggressors_Last_Name_with_sequential_umber()
        {
            var GetOffendersListRequest = new Offenders0.EntMsgGetOffendersListRequest();
            var errorMessage = string.Empty;

            GetOffendersListRequest.ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active };
            GetOffendersListRequest.ProgramConcept = Offenders0.EnmProgramConcept.Aggressor;

            var GetOffendersResponse = Proxies.API.Offenders.OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);

            var offendersCount = GetOffendersResponse.OffendersList.Length;

            for (int i = 0; i < offendersCount; i++)
            {
                try
                {
                    Offenders0.EntMsgUpdateOffenderRequest UpdateOffenderRequest = new Offenders0.EntMsgUpdateOffenderRequest();
                    UpdateOffenderRequest.ProgramStart = (DateTime)GetOffendersResponse.OffendersList[i].ProgramStart;
                    UpdateOffenderRequest.ProgramEnd = DateTime.Now.AddYears(1); //Extand the program end date by 3 years
                    UpdateOffenderRequest.OffenderID = GetOffendersResponse.OffendersList[i].ID;

                    UpdateOffenderRequest.AgencyID = GetOffendersResponse.OffendersList[i].AgencyID;
                    UpdateOffenderRequest.AllowVoiceTests = (bool)GetOffendersResponse.OffendersList[i].AllowVoiceTests;
                    UpdateOffenderRequest.CityID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].CityID;
                    UpdateOffenderRequest.ContactPhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePhoneNumber;
                    UpdateOffenderRequest.ContactPrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.CellData.VoicePrefixPhone;
                    UpdateOffenderRequest.CourtOrderNumber = GetOffendersResponse.OffendersList[i].CourtOrderNumber;
                    UpdateOffenderRequest.DateOfBirth = GetOffendersResponse.OffendersList[i].DateOfBirth;
                    UpdateOffenderRequest.DepartmentOfCorrection = GetOffendersResponse.OffendersList[i].DepartmentOfCorrection;
                    UpdateOffenderRequest.FirstName = GetOffendersResponse.OffendersList[i].FirstName;
                    UpdateOffenderRequest.Gender = GetOffendersResponse.OffendersList[i].Gender;
                    UpdateOffenderRequest.HomeUnit = GetOffendersResponse.OffendersList[i].EquipmentInfo.HomeUnitSN;
                    if (GetOffendersResponse.OffendersList[i].ContactInfo.LandLine != null)
                    {
                        UpdateOffenderRequest.LandlineDialOutPrefix = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.DialOutPrefix;
                        UpdateOffenderRequest.LandlinePhoneNumber = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.Phone;
                        UpdateOffenderRequest.LandlinePrefixPhone = GetOffendersResponse.OffendersList[i].ContactInfo.LandLine.PrefixPhone;
                    }
                    UpdateOffenderRequest.Language = GetOffendersResponse.OffendersList[i].Language;
                    UpdateOffenderRequest.LastName = $"Agg_{i + 1}";
                    UpdateOffenderRequest.MiddleName = GetOffendersResponse.OffendersList[i].MiddleName;

                    UpdateOffenderRequest.OfficerID = GetOffendersResponse.OffendersList[i].OfficerID;
                    UpdateOffenderRequest.PhotoID = GetOffendersResponse.OffendersList[i].PhotoID;
                    UpdateOffenderRequest.ProgramType = (Offenders0.EnmProgramType)GetOffendersResponse.OffendersList[i].ProgramType;
                    UpdateOffenderRequest.Receiver = GetOffendersResponse.OffendersList[i].EquipmentInfo.ReceiverSN;
                    UpdateOffenderRequest.RelatedOffenderID = GetOffendersResponse.OffendersList[i].RelatedOffenderID;
                    UpdateOffenderRequest.SocialSecurity = GetOffendersResponse.OffendersList[i].SocialSecurity;
                    UpdateOffenderRequest.StateID = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].StateID;
                    UpdateOffenderRequest.Street = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].Street;
                    UpdateOffenderRequest.Timestamp = GetOffendersResponse.OffendersList[i].Timestamp;
                    UpdateOffenderRequest.Title = GetOffendersResponse.OffendersList[i].Title;
                    UpdateOffenderRequest.Transmitter = GetOffendersResponse.OffendersList[i].EquipmentInfo.TransmitterSN;
                    UpdateOffenderRequest.ZipCode = GetOffendersResponse.OffendersList[i].ContactInfo.Addresses[0].ZipCode;


                    Proxies.API.Offenders.OffendersProxy.Instance.UpdateOffender(UpdateOffenderRequest);
                }
                catch (Exception ex)
                {
                    Log.Error($"Error while try to update offender {GetOffendersResponse.OffendersList[i].ID}. error: {ex.Message}");
                }
            }
        }

    }
}
