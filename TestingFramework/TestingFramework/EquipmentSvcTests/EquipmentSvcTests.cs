﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;

#region API refs
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment_1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment_2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;
using Common.Categories;
using System.ServiceModel;

#endregion

namespace TestingFramework.UnitTests.EquipmentSvcTests
{
    [TestClass]
    public class EquipmentSvcTests : BaseUnitTest
    {

        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_GetEquipmentList_Success()
        {
            TestingFlow.
                AddBlock<GetEquipmentListTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Get Equipment List Test Block").
            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_GetEquipmentList_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock_1>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetEquipmentListAssertBlock_1>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_GetEquipmentList_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock_2>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetEquipmentListAssertBlock_2>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddEquipment_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddEquipment_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment_1.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment_1.EnmModemType._2G).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment_1.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddEquipment_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment_2.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment_2.EnmModemType._2G).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment_2.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteEquipment_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgDeleteEquipmentRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<DeleteEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete equipment test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteEquipment_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyId).WithValue(null).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock_1>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgDeleteEquipmentRequestBlock_1>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1").
                            FromDeepProperty<GetEquipmentListTestBlock_1>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

                AddBlock<DeleteEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Delete equipment test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteEquipment_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyId).WithValue(null).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock_2>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgDeleteEquipmentRequestBlock_2>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_2").
                            FromDeepProperty<GetEquipmentListTestBlock_2>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

                AddBlock<DeleteEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Delete equipment test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateEquipment_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).
                    SetProperty(x => x.AgencyId).WithValue(null).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateEquipment_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment_1.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment_1.EnmModemType._2G).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment_1.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock_1>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1").
                            FromDeepProperty<GetEquipmentListTestBlock_1>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

                AddBlock<UpdateEquipmentTestBlock_1>().UsePreviousBlockData().

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1").
                            FromDeepProperty<GetEquipmentListTestBlock_1>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).
                    SetProperty(x => x.AgencyId).WithValue(null).

                AddBlock<UpdateEquipmentTestBlock_1>().UsePreviousBlockData().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateEquipment_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock_2>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_2").
                            FromDeepProperty<GetEquipmentListTestBlock_2>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

                AddBlock<UpdateEquipmentTestBlock_2>().UsePreviousBlockData().

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock_2>().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_2").
                            FromDeepProperty<GetEquipmentListTestBlock_2>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).
                    SetProperty(x => x.AgencyId).WithValue(null).

                AddBlock<UpdateEquipmentTestBlock_2>().UsePreviousBlockData().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddGetCellularData_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).//because it is the only SIM card it must be Primery

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<AddCellularDataRequestAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteCellularData_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<DeleteCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteCellularData_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Update Cellular Data test block").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock_1>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<DeleteCellularDataTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteCellularData_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Update Cellular Data test block").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock_2>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<DeleteCellularDataTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddCellularDataDualSim_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreatePrimarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddPrimarySim").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddSecondarySim").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<AddCellularDataDualSimAssertBlock>().
                    SetDescription("Add Cellular Data Dual Sim Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateCellularDataDualSim_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreatePrimarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddPrimarySim").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddSecondarySim").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<AddCellularDataDualSimAssertBlock>().
                    SetDescription("Add Cellular Data Dual Sim Assert Block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddSecondarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<UpdateCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SIMID).
                        WithValueFromBlockIndex("AddSecondarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<UpdateCellularDataDualSimAssertBlock>().
                    SetDescription("Update Cellular Data Assert Block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddPrimarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<UpdateCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SIMID).
                        WithValueFromBlockIndex("AddPrimarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<UpdateCellularDataDualSimAssertBlock>().
                    SetDescription("Update Cellular Data Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DualSimDeletePrimarySim_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreatePrimarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddPrimarySim").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddSecondarySim").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddPrimarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<DeleteCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<CheckSimIsActiveAssertBlock>().
                    SetDescription("Get Cellular Data Test Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateCellularData_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddCellularDataTestBlock").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<UpdateCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data Test Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddUpdateGetCellularData_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyId).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock_1>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<AddCellularDataRequestAssertBlock_1>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddUpdateGetCellularData_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyId).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment_2.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment_2.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment_2.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment_2.EnmModemType._2G).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment_2.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock_2>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<AddCellularDataRequestAssertBlock_2>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateCellularData_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyId).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create first EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data test block").


                AddBlock<CreateEntMsgGetCellularDataRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data test block").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create second EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_1").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Update Cellular Data test block").

                AddBlock<UpdateCellularDataAssertBlock_1>().


            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateCellularData_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyId).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create first EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Update Cellular Data test block").


                AddBlock<CreateEntMsgGetCellularDataRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data test block").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data test block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create second EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.ReceiverSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_2").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateCellularDataTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data test block").

                AddBlock<UpdateCellularDataAssertBlock_2>().


            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_UpdateAuthorizedMobile_Success()
        {
            TestingFlow.

                AddBlock<UpdateActiveStatus2AuthorizedMobilesTestBlock>().
                    SetDescription("Update all active=false mobiles to active=true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.IsActive).WithValue(true).

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.CellularNumber).WithValue("0544720164").
                    SetProperty(x => x.Model).WithValue("iPhone x").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").
                    SetProperty(x => x.AppVersion).WithValue("12.0.0.16").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Set authorized mobile to isActive = true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromPropertyName(EnumPropertyName.MobileID).
                    SetProperty(x => x.IsActive).WithValue(true).

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Update Authorized Mobile test block").

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get authorized mobile to verify values as intended").

                AddBlock<UpdateAuthorizedMobileRequestAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_AddGetAuthorizedMobile_Success()
        {
            TestingFlow.
                AddBlock<UpdateActiveStatus2AuthorizedMobilesTestBlock>().
                    SetDescription("Update all active=false mobiles to active=true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.IsActive).WithValue(true).

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get authorized mobile to verify values as intended").

                AddBlock<AddAuthorizedMobileRequestAssertBlock>().

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Service)]
        [TestMethod]
        public void EquipmentSvcTests_DeleteAuthorizedMobile_Success()
        {
            TestingFlow.
                AddBlock<UpdateActiveStatus2AuthorizedMobilesTestBlock>().
                    SetDescription("Update all active=false mobiles to active=true").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.IsActive).WithValue(true).

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544722264").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    ShallDoCleanUp(false).
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgDeleteAuthorizedMobileRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete authorized mobile request").

                AddBlock<DeleteAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete authorized mobile").

            ExecuteFlow();
        }

       
        [TestMethod]
        public void EquipmentSvcTests_AddEquipmentSerialNumberNullOrEmpty_Fail()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(2).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(string.Empty).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<ValidationFaultExceptionAssertBlock>(EquipmentAssertsFactory.ValidateEquipment_SerialNumber_WS001()).
                    SetDescription("Validation Fault Exception Assert Block").
            ExecuteFlow();
        }

    }
}
