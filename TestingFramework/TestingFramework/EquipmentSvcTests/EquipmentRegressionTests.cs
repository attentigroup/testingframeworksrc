﻿using AssertBlocksLib.EquipmentAsserts;
using Common.Categories;
using Common.Enum;
using DataGenerators.Agency;
using LogicBlocksLib.EquipmentBlocks;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.EquipmentBlocks;
using TestBlocksLib.OffendersBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestingFramework.Proxies.EM.Interfaces.Resources12_0;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.Interfaces;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;
using DataGenerators.Equipment;

namespace TestingFramework.UnitTests.EquipmentSvcTests
{
    [TestClass]
    public class EquipmentRegressionTests : BaseUnitTest
    {
        #region Add Authorized mobile
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddAuthorizedMobile_MobileInstaller()
        {
            TestingFlow.
                
                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get authorized mobile to verify values as intended").

                AddBlock<AddAuthorizedMobileRequestAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddAuthorizedMobile_MobileOfficerTool()
        {
            TestingFlow.
                
                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get authorized mobile to verify values as intended").

                AddBlock<AddAuthorizedMobileRequestAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Add equipment
        #region 1Piece
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_One_Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_One_Piece_GPS_RF_G39_P0()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_One_Piece_GPS_RF_G39_P5()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_One_Piece_GPS_RF_G39_P8()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_One_Piece_GPS_G4i_P5()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G4i).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_One_Piece_GPS_G4i_P8()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G4i).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region E3
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_RF_Curfew_Landline_E3()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_LL_E3).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Cell_E3.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_RF_Curfew_Cell_E3()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Cell_E3).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Cell_E3.ToString()).//WithValue("E40011").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Alcohol
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment__Alcohol_VBR()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Alcohol_VBR).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E40011").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Alcohol_Cell_VBR()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Alcohol_Cell_VBR).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E40011").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment__Alcohol_VB()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Alcohol_VB).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E40011").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProgramType).WithValue(null).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Alcohol_Cell_VB()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Alcohol_Cell_VB).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E40011").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region 2Piece
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Two_Piece_GPS_06()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_0_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Two_Piece_GPS_4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_GPS).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_4.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Two_Piece_3G_NotEncrypted()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Two_Piece_3G_Encrypted()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Two_Piece_v7_NotEncrypted()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G_V7).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_7.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Two_Piece_v7_Encrypted()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_256).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G_V7).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_8).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_7.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region E4
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_RF_Curfew_Dual_E4_NotEncrypted()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E300010").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_RF_Curfew_Dual_E4_Encrypted()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E300010").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Base unit
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Beacon_Curfew_Unit()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).//WithValue("40021").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_GPS_BaseUnit()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.GPS_Base_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("50421000").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Transmitter
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_Triple_Tamper()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_Triple_Tamper).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper.ToString()).//WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).//WithValue("E40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_860_P0()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_860).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_860.ToString()).//WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_860_P6()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_860).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_860.ToString()).//WithValue("E12345").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_890_P0()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_890).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_890_P2_P0.ToString()).//WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_890_P2()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_890).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_890_P2_P0.ToString()).//WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_890_P6()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_890).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_890_P6.ToString()).//WithValue("E400001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_BTX_P0()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.BTX).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_BTX.ToString()).//WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_BTX_P2()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.BTX).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_BTX.ToString()).//WithValue("A40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddEquipment_Transmitter_BTX_P6()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.Encryption_192).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.BTX).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_6).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_BTX.ToString()).//WithValue("40001").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<AddEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion
        #endregion

        #region Add cellular data
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddCellularData_IsPrimarySim()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E300212").

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_E3").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

            AddBlock<AddCellularDataRequestAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddCellularData_IsNotPrimarySim_2Piece_v6()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_E3").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                        SetName("CreateEntMsgAddCellularDataRequestBlock_Secondary").
                        SetDescription("Create EntMsgAddCellularDataRequest Type").
                        SetProperty(x => x.EquipmentID).
                            WithValueFromBlockIndex("AddEquipmentTestBlock").
                                FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                        SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetName("AddCellularDataTestBlock_Secondary").
                    SetDescription("Add Cellular Data test block").
                    SetProperty(x => x.AddCellularDataRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddCellularDataRequestBlock_Secondary").
                    FromDeepProperty<CreateEntMsgAddCellularDataRequestBlock>(x => x.AddCellularDataRequest).

                   AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SIMID).
                    WithValueFromBlockIndex("AddCellularDataTestBlock_Secondary").
                    FromDeepProperty< AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

            AddBlock<AddCellularDataRequestAssertBlock>().
            SetProperty(x => x.AddCellularDataRequest).
            WithValueFromBlockIndex("AddCellularDataTestBlock_Secondary").
            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataRequest).
            SetProperty(x => x.AddCellularDataResponse).
            WithValueFromBlockIndex("AddCellularDataTestBlock_Secondary").
            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_AddCellularData_IsNotPrimarySim_2Piece_v7()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G_V7).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_7.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_E3").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                        SetName("CreateEntMsgAddCellularDataRequestBlock_Secondary").
                        SetDescription("Create EntMsgAddCellularDataRequest Type").
                        SetProperty(x => x.EquipmentID).
                            WithValueFromBlockIndex("AddEquipmentTestBlock").
                                FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                        SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetName("AddCellularDataTestBlock_Secondary").
                    SetDescription("Add Cellular Data test block").
                    SetProperty(x => x.AddCellularDataRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddCellularDataRequestBlock_Secondary").
                    FromDeepProperty<CreateEntMsgAddCellularDataRequestBlock>(x => x.AddCellularDataRequest).

                   AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SIMID).
                    WithValueFromBlockIndex("AddCellularDataTestBlock_Secondary").
                    FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

            AddBlock<AddCellularDataRequestAssertBlock>().
            SetProperty(x => x.AddCellularDataRequest).
            WithValueFromBlockIndex("AddCellularDataTestBlock_Secondary").
            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataRequest).
            SetProperty(x => x.AddCellularDataResponse).
            WithValueFromBlockIndex("AddCellularDataTestBlock_Secondary").
            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse).

            ExecuteFlow();
        }
        #endregion

        #region Delete authorized mobile
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteAuthorizedMobile_MobileInstaller()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544722264").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    ShallDoCleanUp(false).
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgDeleteAuthorizedMobileRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete authorized mobile request").

                AddBlock<DeleteAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete authorized mobile").

                AddBlock<CreateEntMsgGetAuthorizedMobileListRequestBlock>().
                SetProperty(x => x.MobileID).
                WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().

                AddBlock<DeleteAuthorizedMobileAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteAuthorizedMobile_MobileOfficerTool()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.CellularNumber).WithValue("0544722264").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    ShallDoCleanUp(false).
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgDeleteAuthorizedMobileRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete authorized mobile request").

                AddBlock<DeleteAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete authorized mobile").

                AddBlock<CreateEntMsgGetAuthorizedMobileListRequestBlock>().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().

                AddBlock<DeleteAuthorizedMobileAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }
        #endregion

        #region Delete cellular data
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteCellularData_EquipmentID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<DeleteCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.DeviceIDList).WithValue(new int[1]).
                    SetProperty(x => x.DeviceIDList[0]).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

            AddBlock<DeleteCellularDataAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteCellularData_ID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock>().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddCellularDataTestBlock").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<DeleteCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.DeviceIDList).WithValue(new int[1]).
                    SetProperty(x => x.DeviceIDList[0]).
                    WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.SIMID).WithValue(null).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

            AddBlock<DeleteCellularDataAssertBlock>().

            ExecuteFlow();
        }
               
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DualSimDeletePrimarySim()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).//WithValue("35662620").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreatePrimarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddPrimarySim").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddSecondarySim").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddPrimarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<DeleteCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<CheckSimIsActiveAssertBlock>().
                    SetDescription("Get Cellular Data Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DualSimDeleteSecondarySim()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).//WithValue("35662620").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreatePrimarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddPrimarySim").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddSecondarySim").

                AddBlock<CreateEntMsgDeleteCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddSecondarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<DeleteCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<CheckSimIsActiveAssertBlock>().
                    SetDescription("Get Cellular Data Test Block").

            ExecuteFlow();
        }
        #endregion

        #region Delete equipment
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteEquipment_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgDeleteEquipmentRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<DeleteEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                AddBlock<DeleteEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteEquipment_2Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgDeleteEquipmentRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<DeleteEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                AddBlock<DeleteEquipmentAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_DeleteEquipment_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E357246").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgDeleteEquipmentRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgDeleteCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<DeleteEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete equipment test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                AddBlock<DeleteEquipmentAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Get authorized mobile
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetAuthorizedMobile_MobileInstaller()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetAuthorizedMobileListRequestBlock>().
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileInstaller).

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().

                AddBlock<GetAuthorizedMobileAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgGetAuthorizedMobileListRequestBlock").
                            FromDeepProperty<CreateEntMsgGetAuthorizedMobileListRequestBlock>(x => x.ApplicationID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetAuthorizedMobile_MobileOfficerTool()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetAuthorizedMobileListRequestBlock>().
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileOfficerTool).

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().

                AddBlock<GetAuthorizedMobileAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgGetAuthorizedMobileListRequestBlock").
                            FromDeepProperty<CreateEntMsgGetAuthorizedMobileListRequestBlock>(x => x.ApplicationID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetAuthorizedMobile_MobileID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgGetAuthorizedMobileListRequestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).

                AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().

                AddBlock<GetAuthorizedMobileAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).

            ExecuteFlow();
        }
        #endregion

        #region Get cellular data
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetCellularData_ByDeviceID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E300212").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).                    

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.DeviceIDList).WithValue(new int[1]).
                    SetProperty(x => x.DeviceIDList[0]).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<GetCellularDataAssertBlock>().
                SetProperty(x => x.DeviceIDList).
                WithValueFromBlockIndex("GetCellularDataTestBlock").
                FromDeepProperty<GetCellularDataTestBlock>(x => x.GetCellularDataRequest.DeviceIDList).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetCellularData_BySerialNumber()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E300212").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                   SetProperty(x => x.EquipmentID).
                   WithValueFromBlockIndex("AddEquipmentTestBlock").
                   FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                   AddBlock<GetEquipmentListTestBlock>().

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<GetCellularDataAssertBlock>().
                SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetCellularData_BySimID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E300212").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SIMID).
                        WithValueFromBlockIndex("AddCellularDataTestBlock").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<GetCellularDataAssertBlock>().
                SetProperty(x => x.SIMID).
                        WithValueFromBlockIndex("AddCellularDataTestBlock").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

            ExecuteFlow();
        }
        #endregion

        #region Get equipment
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByAgencyID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentsAssertBlock>().
                SetProperty(x => x.AgencyID).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.AgencyID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.One_Piece_RF }).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_EquipmentID").
                SetProperty(x => x.EquipmentID).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").
                    SetProperty(x => x.GetEquipmentListRequest).
                WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_EquipmentID").
                FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<GetEquipmentsAssertBlock>().
                SetProperty(x => x.EquipmentID).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByIsDamage()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.IsDamaged).WithValue(true).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentAssertIsDamageDataBlock>().
                SetProperty(x => x.IsDamaged).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.IsDamaged).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByIsNotDamage()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.IsDamaged).WithValue(false).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentsAssertBlock>().
                SetProperty(x => x.IsDamaged).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.IsDamaged).

            ExecuteFlow();
        }

        private ITestingFlow Equipment_GetEquipment_ByEquipmentModel(ITestingFlow testingFlow, Equipment.EnmEquipmentModel[] equipmentModels)
        {
            testingFlow.
                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentModels).WithValue(equipmentModels).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentsAssertBlock>().
                    SetProperty(x => x.EquipmentModels).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.Models);

            return testingFlow;
        }


        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_1Piece_GPS_RF_G4i()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G4i }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_1Piece_GPS_RF_G39()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G39 }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_1Piece_GPS_RF()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.One_Piece_GPS_RF }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_2Piece_3G_V7()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Two_Piece_3G_V7 }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_2Piece_3G()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Two_Piece_3G }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_2Piece_GPS()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Two_Piece_GPS }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_E4()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.RF_Curfew_Dual_E4 }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_E3()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.RF_Curfew_Cell_E3 }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_Alcohol()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Alcohol_Cell_VB,
                        Equipment.EnmEquipmentModel.Alcohol_Cell_VBR, Equipment.EnmEquipmentModel.Alcohol_Dual_VB,
                        Equipment.EnmEquipmentModel.Alcohol_Dual_VBR, Equipment.EnmEquipmentModel.Alcohol_VB,
                        Equipment.EnmEquipmentModel.Alcohol_VBR }).ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_Transmitter_Triple_Tamper()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Transmitter_Triple_Tamper }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_Transmitter_860()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Transmitter_860 }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_Transmitter_890()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Transmitter_890 }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_Transmitter_DV()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.Transmitter_DV }).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByEquipmentModel_BTX()
        {
            Equipment_GetEquipment_ByEquipmentModel(
                TestingFlow, new Equipment.EnmEquipmentModel[] { Equipment.EnmEquipmentModel.BTX }).ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByProgramType_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.One_Piece_RF } ). 
                    SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Receiver).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByProgramTypeAssertBlock>().
                    SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.ProgramType[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByProgramType_2Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Receiver).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByProgramTypeAssertBlock>().
                    SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.ProgramType[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByProgramType_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.RF_Dual }).
                    SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Receiver).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByProgramTypeAssertBlock>().
                    SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.ProgramType[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByProgramType_E3()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.RF_Cellular }).
                    SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Receiver).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByProgramTypeAssertBlock>().
                    SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.ProgramType[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByProgramType_Alcohol()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.VB_Dual }).
                    SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Receiver).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByProgramTypeAssertBlock>().
                    SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.ProgramType[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByReturnDamageData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.IsDamaged).WithValue(true).
                SetProperty(x => x.ReturnDamageData).WithValue(true).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentsAssertBlock>().
                SetProperty(x => x.ReturnDamageData).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.ReturnDamageData).

            ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Equipment_Regression)]
        //[TestMethod]
        //public void Equipment_GetEquipment_ByNotReturnDamageData()
        //{
        //    TestingFlow.

        //        AddBlock<CreateEntMsgGetEquipmentListRequest>().
        //        SetProperty(x => x.IsDamaged).WithValue(true).
        //        SetProperty(x => x.ReturnDamageData).WithValue(false).

        //        AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
        //            SetDescription("Get Equipment List Test Block").

        //        AddBlock<GetEquipmentsAssertBlock>().
        //        SetProperty(x => x.ReturnDamageData).
        //        WithValueFromBlockIndex("GetEquipmentListTestBlock").
        //        FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.ReturnDamageData).

        //    ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByReturnOnlyNotAllocated()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.IsDamaged).WithValue(true).
                SetProperty(x => x.ReturnDamageData).WithValue(true).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentsAssertBlock>().
                SetProperty(x => x.ReturnDamageData).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.ReturnDamageData).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_BySerialNumber()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.ProgramType).WithValue(new Equipment.EnmProgramType[] { Equipment.EnmProgramType.One_Piece_RF }).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetName("CreateEntMsgGetEquipmentListRequest_EquipmentID").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").
                    SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_EquipmentID").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<GetEquipmentsAssertBlock>().
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByType_Receiver()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Receiver).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByTypeAssertBlock>().
                SetProperty(x => x.EquipmentType).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.Type).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByType_Transmitter()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Transmitter).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByTypeAssertBlock>().
                SetProperty(x => x.EquipmentType).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.Type).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByType_Transceiver()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Transceiver).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByTypeAssertBlock>().
                SetProperty(x => x.EquipmentType).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.Type).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByType_ManualResetDevice()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.ManualResetDevice).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByTypeAssertBlock>().
                SetProperty(x => x.EquipmentType).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.Type).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByType_Mobile()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.EquipmentType).WithValue(Equipment.EnmEquipmentType.Mobile).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentByTypeAssertBlock>().
                SetProperty(x => x.EquipmentType).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.Type).

            ExecuteFlow();
        }

        
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByReturnOnlyDeletable()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.ReturnOnlyDeletable).WithValue(true).

                AddBlock<GetEquipmentListTestBlock>().
                    SetDescription("Get Equipment List Test Block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_GetEquipment_ByOffenderID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetProperty(x => x.OffenderID).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                AddBlock<GetEquipmentsAssertBlock>().
                SetProperty(x => x.OffenderID).
                WithValueFromBlockIndex("GetEquipmentListTestBlock").
                FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.OffenderID).

            ExecuteFlow();
        }
        #endregion

        #region Update authorized mobile
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateAuthorizedMobile_MobileInstaller_IsActive()
        {
            TestingFlow.
                
                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.ApplicationID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.AppVersion).//WithRandomGenerateValue().
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.AppVersion).
                    SetProperty(x => x.DeviceFCMToken).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().

            AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                SetDescription("Get authorized mobile to verify values as intended").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateAuthorizedMobile_MobileOfficerTool_IsActive()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.ApplicationID).
                    SetProperty(x => x.IsActive).WithValue(true).
                    SetProperty(x => x.AppVersion).//WithRandomGenerateValue().
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.AppVersion).

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().

            AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                SetDescription("Get authorized mobile to verify values as intended").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateAuthorizedMobile_MobileInstaller_IsNotActive()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileInstaller).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.ApplicationID).
                    SetProperty(x => x.IsActive).WithValue(false).
                    SetProperty(x => x.AppVersion).//WithRandomGenerateValue().
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.AppVersion).

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().

            AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                SetDescription("Get authorized mobile to verify values as intended").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateAuthorizedMobile_MobileOfficerTool_IsNotActive()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddAuthorizedMobileRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Add authorized mobile").
                    SetProperty(x => x.ApplicationID).WithValue(Equipment.EnmMobileApplication.MobileOfficerTool).
                    SetProperty(x => x.CellularNumber).WithValue("0544721164").
                    SetProperty(x => x.Model).WithValue("iPhone 8").
                    SetProperty(x => x.OperatingSystem).WithValue("Android 7.0.0").
                    SetProperty(x => x.Comment).WithValue("bla").

                AddBlock<AddAuthorizedMobileTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Authorized Mobile test block").

                AddBlock<CreateEntMsgUpdateAuthorizedMobileRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.MobileID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.MobileID).
                    SetProperty(x => x.ApplicationID).
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.ApplicationID).
                    SetProperty(x => x.IsActive).WithValue(false).
                    SetProperty(x => x.AppVersion).//WithRandomGenerateValue().
                        WithValueFromBlockIndex("CreateEntMsgAddAuthorizedMobileRequestBlock").
                            FromDeepProperty<CreateEntMsgAddAuthorizedMobileRequestBlock>(x => x.AppVersion).

                AddBlock<UpdateAuthorizedMobileTestBlock>().UsePreviousBlockData().

            AddBlock<GetAuthorizedMobileListTestBlock>().UsePreviousBlockData().
                SetDescription("Get authorized mobile to verify values as intended").

            ExecuteFlow();
        }
        #endregion

        #region Update cellular data
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateCellularData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID). 
                    SetProperty(x => x.ProviderID).WithValue(ProviderIDGenerator.ProviderIDList[0]).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddCellularDataTestBlock").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).
                    SetProperty(x => x.ProviderID).WithValue(ProviderIDGenerator.ProviderIDList[1]).

                AddBlock<UpdateCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                SetName("CreateEntMsgGetCellularDataRequestBlock_After").
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                SetName("GetCellularDataTestBlock_After").
                    SetDescription("Get Cellular Data Test Block").
                    SetProperty(x => x.GetCellularDataRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetCellularDataRequestBlock_After").
                    FromDeepProperty<CreateEntMsgGetCellularDataRequestBlock>(x => x.GetCellularDataRequest).

                AddBlock<UpdateCellularDataAssertBlock>().
                    SetProperty(x => x.GetCellularDataResponseAfterUpdate).
                    WithValueFromBlockIndex("GetCellularDataTestBlock_After").
                    FromDeepProperty<GetCellularDataTestBlock>(x => x.GetCellularDataResponse).

            ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateCellularData_IsNotPrimarySim()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).//WithValue("35661611").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreatePrimarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).    
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddPrimarySim").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetName("CreateSecondarySim").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").
                    SetName("AddSecondarySim").       
                    SetProperty(x => x.AddCellularDataRequest).
                        WithValueFromBlockIndex("CreateSecondarySim").
                            FromDeepProperty<CreateEntMsgAddCellularDataRequestBlock>(x => x.AddCellularDataRequest).

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetProperty(x => x.SerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").

                AddBlock<AddCellularDataDualSimAssertBlock>().
                    SetDescription("Add Cellular Data Dual Sim Assert Block").

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock>().
                    SetDescription("Create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.IsPrimarySIM).WithValue(true).
                    SetProperty(x => x.ProviderID).WithRandomGenerateValue().//WithValue(2).
                    SetProperty(x => x.VoicePrefixPhone).WithRandomGenerateValue().//WithValue("97255").
                    SetProperty(x => x.VoicePhoneNumber).WithRandomGenerateValue().//WithValue("1111111").
                    SetProperty(x => x.DataPrefixPhone).WithRandomGenerateValue().//WithValue("97255").
                    SetProperty(x => x.DataPhoneNumber).WithRandomGenerateValue().//WithValue("1111111").
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddSecondarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<UpdateCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data Test Block").

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetName("CreateEntMsgGetCellularDataRequestBlock_secondary").
                    SetProperty(x => x.SIMID).
                        WithValueFromBlockIndex("AddSecondarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                SetName("GetCellularDataTestBlock_Secondary").
                    SetDescription("Get Cellular Data Test Block").
                    SetProperty(x => x.GetCellularDataRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetCellularDataRequestBlock_secondary").
                            FromDeepProperty<CreateEntMsgGetCellularDataRequestBlock>(x => x.GetCellularDataRequest).


                AddBlock<UpdateCellularDataDualSimAssertBlock>().
                    SetDescription("Update Cellular Data Assert Block").
                    SetProperty(x => x.GetCellularDataResponse).
                        WithValueFromBlockIndex("GetCellularDataTestBlock_Secondary").
                            FromDeepProperty<GetCellularDataTestBlock>(x => x.GetCellularDataResponse).

                AddBlock<CreateEntMsgUpdateCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgUpdateCellularDataRequestBlock_Primary").
                    SetDescription("Create EntMsgUpdateCellularDataRequest type").
                    SetProperty(x => x.IsPrimarySIM).WithValue(false).
                    SetProperty(x => x.ID).
                        WithValueFromBlockIndex("AddPrimarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<UpdateCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Cellular Data Test Block").
                    SetProperty(x => x.UpdateCellularDataRequest).
                        WithValueFromBlockIndex("CreateEntMsgUpdateCellularDataRequestBlock_Primary").
                            FromDeepProperty<CreateEntMsgUpdateCellularDataRequestBlock>(x => x.UpdateCellularDataRequest).

                AddBlock<CreateEntMsgGetCellularDataRequestBlock>().
                    SetDescription("Create EntMsgGetCellularDataRequest Type").
                    SetName("CreateEntMsgGetCellularDataRequestBlock_Primary").
                    SetProperty(x => x.SIMID).
                        WithValueFromBlockIndex("AddPrimarySim").
                            FromDeepProperty<AddCellularDataTestBlock>(x => x.AddCellularDataResponse.NewSIMID).

                AddBlock<GetCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Cellular Data Test Block").
                    SetName("GetCellularDataTestBlock_Primary").
                    SetProperty(x => x.GetCellularDataRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetCellularDataRequestBlock_Primary").
                            FromDeepProperty<CreateEntMsgGetCellularDataRequestBlock>(x => x.GetCellularDataRequest).

                AddBlock<UpdateCellularDataDualSimAssertBlock>().
                    SetDescription("Update Cellular Data Assert Block").
                    SetProperty(x => x.GetCellularDataResponse).
                        WithValueFromBlockIndex("GetCellularDataTestBlock_Primary").
                            FromDeepProperty<GetCellularDataTestBlock>(x => x.GetCellularDataResponse).

            ExecuteFlow();
        }
        #endregion

        #region Update equipment
        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_MoveToAgency_1Piece_GPS_RF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34233333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").   

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateEquipmentAgencyAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_MoveToAgency_2Piece_GPS()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Two_Piece_GPS).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_4.ToString()).//WithValue("34233333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateEquipmentAgencyAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_MoveToAgency_E4()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E342333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateEquipmentAgencyAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_MoveToAgency_Alcohol()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Alcohol_VB).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("E342333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<UpdateEquipmentAgencyAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_SetFailureData_1Piece_GPS_RF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    //SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34233333").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).
                    SetProperty(x => x.IsDamaged).WithValue(true).
                    SetProperty(x => x.EquipmentLocation).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).
                    SetProperty(x => x.FailureDate).WithValueFromDifferentDataGenerator(EnumPropertyType.FailureDate.ToString()).
                    SetProperty(x => x.FailureDescription).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).
                    SetProperty(x => x.SentToRepairDate).WithValueFromDifferentDataGenerator(EnumPropertyType.SentToRepairDate.ToString()).
                    SetProperty(x => x.FailureType).WithValueFromDifferentDataGenerator(EnumPropertyType.FailureType.ToString()).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                SetProperty(x => x.ReturnDamageData).WithValue(true).
                SetProperty(x => x.AgencyID).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("EquipmentWithDamageData").

                AddBlock<UpdateEquipmentFailureDataAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetEquipmentListResponse).
                    WithValueFromBlockIndex("EquipmentWithDamageData").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_SetFailureData_2Piece_GPS()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Two_Piece_GPS).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_4.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).
                    SetProperty(x => x.IsDamaged).WithValue(true).
                    SetProperty(x => x.EquipmentLocation).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).
                    SetProperty(x => x.FailureDate).WithValueFromDifferentDataGenerator(EnumPropertyType.FailureDate.ToString()).
                    SetProperty(x => x.FailureDescription).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).
                    SetProperty(x => x.SentToRepairDate).WithValueFromDifferentDataGenerator(EnumPropertyType.SentToRepairDate.ToString()).
                    SetProperty(x => x.FailureType).WithValueFromDifferentDataGenerator(EnumPropertyType.FailureType.ToString()).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                SetProperty(x => x.ReturnDamageData).WithValue(true).
                SetProperty(x => x.AgencyID).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("EquipmentWithDamageData").

                AddBlock<UpdateEquipmentFailureDataAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetEquipmentListResponse).
                    WithValueFromBlockIndex("EquipmentWithDamageData").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Equipment_Regression)]
        [TestMethod]
        public void Equipment_UpdateEquipment_SetFailureData_E4()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create random EntMsgAddEquipmentRequest type").
                    //SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).//WithValue("E342331").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("Get Equipment Block").
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgUpdateEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgUpdateEquipmentRequest").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].ID).
                    SetProperty(x => x.AgencyId).WithValue(AgencyIdGenerator.AgencyIdList[0]).
                    SetProperty(x => x.IsDamaged).WithValue(true).
                    SetProperty(x => x.EquipmentLocation).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).
                    SetProperty(x => x.FailureDate).WithValueFromDifferentDataGenerator(EnumPropertyType.FailureDate.ToString()).
                    SetProperty(x => x.FailureDescription).WithValueFromDifferentDataGenerator(EnumPropertyType.String.ToString()).
                    SetProperty(x => x.SentToRepairDate).WithValueFromDifferentDataGenerator(EnumPropertyType.SentToRepairDate.ToString()).
                    SetProperty(x => x.FailureType).WithValueFromDifferentDataGenerator(EnumPropertyType.FailureType.ToString()).

                AddBlock<UpdateEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                SetProperty(x => x.ReturnDamageData).WithValue(true).
                SetProperty(x => x.AgencyID).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("EquipmentWithDamageData").

                AddBlock<UpdateEquipmentFailureDataAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetEquipmentListResponse).
                    WithValueFromBlockIndex("EquipmentWithDamageData").
                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse).

            ExecuteFlow();
        }
        #endregion

    }
}
