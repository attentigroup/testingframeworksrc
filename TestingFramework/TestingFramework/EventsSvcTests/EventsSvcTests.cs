﻿using Common.Categories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.EventsBlocks;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.Events;
using AssertBlocksLib.Events;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.DevicesBlocks;

#region API refs
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Events1 = TestingFramework.Proxies.EM.Interfaces.Events3_10;
using Events2 = TestingFramework.Proxies.EM.Interfaces.Events3_9;

#endregion

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment1 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment2 = TestingFramework.Proxies.EM.Interfaces.Equipment;
#endregion

namespace TestingFramework.UnitTests.EventsSvcTests
{
    [TestClass]
    public class EventsSvcTests : BaseUnitTest
    {        

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetEvents_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock< GetEventsAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").

            ExecuteFlow();
        }

       
        [TestMethod]
        public void EventsSvcTests_GetEvents_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events1.EnmHandlingStatusType.New).

                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsAssertBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events assert block").

            ExecuteFlow();
        }

        
        [TestMethod]
        public void EventsSvcTests_GetEvents_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_2>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.MaximumRows).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events2.EnmHandlingStatusType.New).

                AddBlock<GetEventsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsAssertBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetEventsMetaData_Success()
        {
            TestingFlow.               

                AddBlock<GetEventsMetaDataTestBlock>().
                    SetDescription("Get events metadata test").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetEventsMetaData_1_Success()
        {
            TestingFlow.

                AddBlock<GetEventsMetaDataTestBlock_1>().
                    SetDescription("Get events metadata test").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetEventsMetaData_2_Success()
        {
            TestingFlow.

                AddBlock<GetEventsMetaDataTestBlock_2>().
                    SetDescription("Get events metadata test").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_AddHandlingAction_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.IncludeHistory).WithValue(true).
                    
                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).
                
                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_AfterHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Status).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("GetEventsRequestBlock_AfterHandling").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<AddHandlingActionAssertBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        
        [TestMethod]
        public void EventsSvcTests_AddHandlingAction_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_1_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events1.EnmHandlingStatusType.New).  
                    SetProperty(x => x.IncludeHistory).WithValue(true).
                    SetProperty(x=> x.SortDirection).WithValue(Events1.ListSortDirection.Descending).
                    
                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock_1").
                            FromDeepProperty<GetEventsTestBlock_1>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events1.EnmHandlingActionType.Handle).
                
                AddBlock<AddHandlingActionTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_1_AfterHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_1").
                        FromDeepProperty<GetEventsTestBlock_1>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Status).WithValue(null).

                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("GetEventsRequestBlock_1_AfterHandling").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock_1>(x => x.GetEventsRequest).

                AddBlock<AddHandlingActionAssertBlock_1>().UsePreviousBlockData().
                 SetDescription("Add handling action assert block").

            ExecuteFlow();
        }
        
       
        [TestMethod]
        public void EventsSvcTests_AddHandlingAction_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_2>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_2_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.MaximumRows).WithValue(1).
                    SetProperty(x => x.Status).WithValue(Events2.EnmHandlingStatusType.New).
                    SetProperty(x => x.IncludeHistory).WithValue(true).
                    SetProperty(x => x.SortDirection).WithValue(Events2.ListSortDirection.Descending).


                AddBlock<GetEventsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock_2").
                            FromDeepProperty<GetEventsTestBlock_2>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events2.EnmHandlingActionType.Handle).
                
                AddBlock<AddHandlingActionTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetEventsRequestBlock_2>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_2_AfterHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.MaximumRows).WithValue(1).
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_2").
                        FromDeepProperty<GetEventsTestBlock_2>(x => x.GetEventsResponse.EventsList[0].ID).  
                    SetProperty(x => x.Status).WithValue(null).

                AddBlock<GetEventsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("GetEventsRequestBlock_2_AfterHandling").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock_2>(x => x.GetEventsRequest).

                AddBlock<AddHandlingActionAssertBlock_2>().UsePreviousBlockData().
                    SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetOffenderEventStatus_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.EventCodes).WithValue(new string[] { "P69", "P51", "P79", "P55", "P57", "P59", "D01", "F25" }).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffenderEventStatusRequestBlock>().
                    SetDescription("Create get offender event status request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetOffenderEventStatusTestBlock>().UsePreviousBlockData().  
                    SetDescription("Get offender event status test").

            ExecuteFlow();
        }

       
        [TestMethod]
        public void EventsSvcTests_GetOffenderEventStatus_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_1_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events1.EnmEventSeverity.Violation).
                   // SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.EventCodes).WithValue(new string[] { "P69", "P51", "P79", "P55",/* "MMC",*/ "P57", "P59", "D01","F11","F25","F15","F05"}).

                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffenderEventStatusRequestBlock_1>().
                    SetDescription("Create get offender event status request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_1").
                        FromDeepProperty<GetEventsTestBlock_1>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetOffenderEventStatusTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get offender event status test").

            ExecuteFlow();
        }

       
        [TestMethod]
        public void EventsSvcTests_GetOffenderEventStatus_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_2>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_2_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.MaximumRows).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events2.EnmEventSeverity.Violation).
                    //SetProperty(x => x.ProgramType).WithValue(Events2.EnmProgramType.One_Piece).
                    SetProperty(x => x.EventCode).WithValue(new Events2.EntStringQueryListParameter() { Operator = Events2.EnmSqlListOperatorStr.In, Value = new string[] { "P69", "P51", "P79", "P55", "MMC", "P57", "P59", "D01" } } ).

                AddBlock<GetEventsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffenderEventStatusRequestBlock_2>().
                    SetDescription("Create get offender event status request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_2").
                        FromDeepProperty<GetEventsTestBlock_2>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetOffenderEventStatusTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get offender event status test").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetHandlingActionByQuery_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.GetEventsResponse).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

            ExecuteFlow();
        }

        
        [TestMethod]
        public void EventsSvcTests_GetHandlingActionByQuery_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events1.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events1.EnmHandlingStatusType.Handled).

                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock_1>().
                    SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_1").
                        FromDeepProperty<GetEventsTestBlock_1>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryAssertBlock_1>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.GetEventsResponse).
                    WithValueFromBlockIndex("GetEventsTestBlock_1").
                        FromDeepProperty<GetEventsTestBlock_1>(x => x.GetEventsResponse).

            ExecuteFlow();
        }

       
        [TestMethod]
        public void EventsSvcTests_GetHandlingActionByQuery_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_2>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.MaximumRows).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events2.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events2.EnmHandlingStatusType.Handled).

                AddBlock<GetEventsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock_2>().
                    SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_2").
                        FromDeepProperty<GetEventsTestBlock_2>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryAssertBlock_2>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.GetEventsResponse).
                    WithValueFromBlockIndex("GetEventsTestBlock_2").
                        FromDeepProperty<GetEventsTestBlock_2>(x => x.GetEventsResponse).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetOpenPanicCalls_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOpenPanicCallsRequestBlock>().
                    SetDescription("Create get open panic calls request").
                    SetProperty(x => x.MaximumRows).WithValue(-1).
                    SetProperty(x => x.StartRowIndex).WithValue(0).

                AddBlock<GetOpenPanicCallsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get open panic calls test").

                AddBlock<GetOpenPanicCallsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetOpenPanicCalls_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOpenPanicCallsRequestBlock_1>().
                    SetDescription("Create get open panic calls request").
                    SetProperty(x => x.MaximumRows).WithValue(-1).
                    SetProperty(x => x.StartRowIndex).WithValue(0).

                AddBlock<GetOpenPanicCallsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get open panic calls test").

                AddBlock<GetOpenPanicCallsAssertBlock_1>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetOpenPanicCalls_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOpenPanicCallsRequestBlock_2>().
                    SetDescription("Create get open panic calls request").
                    SetProperty(x => x.MaximumRows).WithValue(-1).
                    SetProperty(x => x.StartRowIndex).WithValue(0).

                AddBlock<GetOpenPanicCallsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get open panic calls test").

                AddBlock<GetOpenPanicCallsAssertBlock_2>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetAlcoholTestResult_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("").
                    SetProperty(x => x.Message).WithValue("BAT Passed").

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("").

                AddBlock<CreateEntMsgGetAlcoholTestResultRequestBlock>().
                    SetDescription("Create get alcohol test result request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty< GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetAlcoholTestResultTestBlock>().UsePreviousBlockData().
                    SetDescription("Get alcohol test result test").

                AddBlock< GetAlcoholTestResultAssertBlock >().
  

            ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_GetAlcoholTestResult_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("").
                    SetProperty(x => x.Message).WithValue("BAT Passed").

                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("").

                AddBlock<CreateEntMsgGetAlcoholTestResultRequestBlock_1>().
                    SetDescription("Create get alcohol test result request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_1").
                    FromDeepProperty<GetEventsTestBlock_1>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetAlcoholTestResultTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get alcohol test result test").

                AddBlock<GetAlcoholTestResultAssertBlock_1>().


            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Service)]
        [TestMethod]
        public void EventsSvcTests_HandleAllEvents_Success()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(Equipment0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Model).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER;HW_DEVICE_BAT_LOW;HW_BTX_STRAP;HW_HW_RULE_SHUTDOWN").
                    // SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Bracelet_Gone-first_violation;Device_Tamper-violation;Bracelet_Battery_Low-violation;Device_Battery_Low-first_violation;Bracelet_Strap-first_violation;Bracelet_Body_Tamper-violation;Tracker_Shutdown-violation").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).

                AddBlock<CreateEntMsgHandleAllEventsRequestBlock>().
                    SetDescription("Create handle all events request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<HandleAllEventsTestBlock>().UsePreviousBlockData().
                        SetDescription("Handle all events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_AfterHandling").
                    SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Status).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<HandleAllEventsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

      
        [TestMethod]
        public void EventsSvcTests_HandleAllEvents_1_Success()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender_1>().
                    SetProperty(x => x.ProgramType).WithValue(Equipment1.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Model).WithValue(Equipment1.EnmEquipmentModel.One_Piece_GPS_RF_G39).

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentWithoutOffender_1").
                            FromDeepProperty<GetEquipmentWithoutOffender_1>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender_1").
                            FromDeepProperty<GetEquipmentWithoutOffender_1>(x => x.Equipment.SerialNumber).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER;HW_DEVICE_BAT_LOW;HW_BTX_STRAP;HW_HW_RULE_SHUTDOWN").
                    //SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Bracelet_Gone-first_violation;Device_Tamper-violation;Bracelet_Battery_Low-violation;Device_Battery_Low-first_violation;Bracelet_Strap-first_violation;Bracelet_Body_Tamper-violation;Tracker_Shutdown-violation").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender_1").
                            FromDeepProperty<GetEquipmentWithoutOffender_1>(x => x.Equipment.SerialNumber).

                AddBlock<CreateEntMsgHandleAllEventsRequestBlock_1>().
                    SetDescription("Create handle all events request").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<HandleAllEventsTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Handle all events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock_1>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_1_AfterHandling").
                    SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Status).WithValue(Events1.EnmHandlingStatusType.Batch).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddHours(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Events1.EnmProgramType.One_Piece_RF).


                AddBlock<GetEventsTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<HandleAllEventsAssertBlock_1>().UsePreviousBlockData().

            ExecuteFlow();
        }

        
        [TestMethod]
        public void EventsSvcTests_HandleAllEvents_2_Success()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender_2>().
                SetProperty(x => x.ProgramType).WithValue(Equipment2.EnmProgramType.One_Piece_RF).
                SetProperty(x => x.Model).WithValue(Equipment2.EnmEquipmentModel.One_Piece_GPS_RF_G39).

                AddBlock<CreateEntMsgGetCellularDataRequestBlock_2>().UsePreviousBlockData().
                SetProperty(x => x.ReceiverSerialNumber).
                WithValueFromBlockIndex("GetEquipmentWithoutOffender_2").
                            FromDeepProperty<GetEquipmentWithoutOffender_2>(x => x.Equipment.SerialNumber).

                AddBlock<GetCellularDataTestBlock_2>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentWithoutOffender_2").
                            FromDeepProperty<GetEquipmentWithoutOffender_2>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender_2").
                            FromDeepProperty<GetEquipmentWithoutOffender_2>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.VoicePhoneNumber).
                        WithValueFromBlockIndex("GetCellularDataTestBlock_2").
                            FromDeepProperty<GetCellularDataTestBlock_2>(x => x.GetCellularDataResponse.CellularData[0].VoicePhoneNumber).
                    SetProperty(x => x.VoicePrefixPhone).
                        WithValueFromBlockIndex("GetCellularDataTestBlock_2").
                            FromDeepProperty<GetCellularDataTestBlock_2>(x => x.GetCellularDataResponse.CellularData[0].VoicePrefixPhone).
                   SetProperty(x => x.ProviderID).
                       WithValueFromBlockIndex("GetCellularDataTestBlock_2").
                            FromDeepProperty<GetCellularDataTestBlock_2>(x => x.GetCellularDataResponse.CellularData[0].ProviderID).
                   SetProperty(x => x.CSDPhoneNumber).
                       WithValueFromBlockIndex("GetCellularDataTestBlock_2").
                            FromDeepProperty<GetCellularDataTestBlock_2>(x => x.GetCellularDataResponse.CellularData[0].CSDPhoneNumber).
                   SetProperty(x => x.CSDPrefixPhone).
                       WithValueFromBlockIndex("GetCellularDataTestBlock_2").
                            FromDeepProperty<GetCellularDataTestBlock_2>(x => x.GetCellularDataResponse.CellularData[0].CSDPrefixPhone).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER;HW_DEVICE_BAT_LOW;HW_BTX_STRAP;HW_HW_RULE_SHUTDOWN").
                    //SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Bracelet_Gone-first_violation;Device_Tamper-violation;Bracelet_Battery_Low-violation;Device_Battery_Low-first_violation;Bracelet_Strap-first_violation;Bracelet_Body_Tamper-violation;Tracker_Shutdown-violation").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender_2").
                            FromDeepProperty<GetEquipmentWithoutOffender_2>(x => x.Equipment.SerialNumber).

                AddBlock<CreateEntMsgHandleAllEventsRequestBlock_2>().
                    SetDescription("Create handle all events request").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<HandleAllEventsTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Handle all events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock_2>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_1_AfterHandling").
                    SetProperty(x => x.MaximumRows).WithValue(-1).
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Status).WithValue(Events2.EnmHandlingStatusType.Batch).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddHours(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Events2.EnmProgramType.One_Piece_RF).


                AddBlock<GetEventsTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<HandleAllEventsAssertBlock_2>().UsePreviousBlockData().

            ExecuteFlow();
        }

    }
}
