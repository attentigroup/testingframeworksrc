﻿using AssertBlocksLib.Events;
using Common.Categories;
using Common.Enum;
using LogicBlocksLib.Events;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.UsersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.EventsBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.UsersBlocks;
using TestingFramework.TestsInfraStructure.Implementation;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using TestBlocksLib.EquipmentBlocks;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.EquipmentBlocks;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using TestBlocksLib.LogBlocks;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;

namespace TestingFramework.UnitTests.EventsSvcTests
{
    [TestClass]
    public class EventsRegressionTests : BaseUnitTest
    {
        #region Add handling action
        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_QuickHandle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(50).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender-related event").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.QuickHandle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().
                    SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_Handle_NewToHandle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                    SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").
 
                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_PhoneCall()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<GetUserWithPhoneTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntMsgGetUsersListResponse).
                WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetEventsResponse).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.PhoneCall).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.ID).
                    SetProperty(x => x.PrefixPhone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PrefixPhone).
                    SetProperty(x => x.Phone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PhoneNumber).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_SendFax()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                    SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                    SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<GetUserWithPhoneTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntMsgGetUsersListResponse).
                WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty <GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendFax).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.ID).
                    SetProperty(x => x.PrefixPhone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PrefixPhone).
                    SetProperty(x => x.Phone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PhoneNumber).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                    SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_SendEmail()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendEmail).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[3].ID).
                    SetProperty(x => x.EmailAddress).WithValueFromDifferentDataGenerator(EnumPropertyType.EmailAddress.ToString()).//WithValue("auto@auto.com").

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_CreateRemark_NewToInProcess()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateRemark).                  
                    

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_CreateRemark_InProcessToInProcess()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.InProcess).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateRemark).


                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_CreateRemark_AutoProcessToInProcess()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateRemark).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_CreateWarning()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.EventCodes).WithValue(new string[] { "P69", "P51", "P79", "P55", "MMC", "P57", "P59", "D01", "F25" }).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().
                SetProperty(x => x.GetEventsResponse).
                WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                       WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateWarning).
                    

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                     WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_BatchHandling()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.EventCodes).WithValue(new string[] { "P69", "P51", "P79", "P55", "MMC", "P57", "P59", "D01", "F25" }).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetEventsResponse).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.BatchHandling).
                    

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_ScheduleTask()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
                    SetProperty(x => x.ScheduledTaskType).WithValueFromDifferentDataGenerator(EnumPropertyType.ScheduleTaskType.ToString()).
                    SetProperty(x => x.ScheduledTaskTime).WithValueFromDifferentDataGenerator(EnumPropertyType.ScheduleTaskTime.ToString()).
                    SetProperty(x => x.ReminderTime).WithValueFromDifferentDataGenerator(EnumPropertyType.ReminderTime.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                 SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_CustomAction()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                     AddBlock<GetEventWithOffenderTestBlock>().
                SetProperty(x => x.GetEventsResponse).
                WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse).

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CustomAction).
                    SetProperty(x => x.UserDefinedActionCode).WithValueFromDifferentDataGenerator(EnumPropertyType.UserDefinedActionCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_SendToThirdParty()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendToThirdParty).
                    SetProperty(x => x.ThirdPartyActionCode).WithValueFromDifferentDataGenerator(EnumPropertyType.ThirdPartyActionCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_ReturnFromThirdParty()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendToThirdParty).
                    SetProperty(x => x.ThirdPartyActionCode).WithValueFromDifferentDataGenerator(EnumPropertyType.ThirdPartyActionCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().
                    SetDescription("Create Add handling action request").
                    SetName("CreateEntMsgAddHandlingActionRequestBlock_ReturnFromThirdParty").
                    SetProperty(x => x.Comment).WithValue("Handled by automation").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ReturnFromThirdParty).
                    SetProperty(x => x.ThirdPartyActionCode).WithValue(null).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                SetName("AddHandlingActionTestBlock_ReturnFromThirdParty").
                    SetDescription("Add handling action test").
                    SetProperty(x => x.AddHandlingActionRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddHandlingActionRequestBlock_ReturnFromThirdParty").
                    FromDeepProperty<CreateEntMsgAddHandlingActionRequestBlock>(x => x.AddHandlingActionRequest).

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                    SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action assert block").
                    SetProperty(x => x.AddHandlingActionRequest).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock_ReturnFromThirdParty").
                    FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_UnHandle_NewToHandle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Unhandled).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_UnHandle_InProcessToHandle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                    AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateRemark).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Unhandled).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_UnHandle_AutoProcessToHandle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Unhandled).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_Type_SendTextMessage()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendTextMessage).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].ID).
                    SetProperty(x => x.ProviderID).WithValueFromDifferentDataGenerator(EnumPropertyType.PagerProviderID.ToString()).
                    SetProperty(x => x.PagerCode).WithValueFromDifferentDataGenerator(EnumPropertyType.PagerCode.ToString()).
                    
                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_ClearPanic()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_PANIC;HW_NO_GPS").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ClearPanicAlert).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).


                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_AddHandlingAction_ClearTamper()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_Beacon").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                             
                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_AfterXtRunning").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_AfterXtRunning").
                    SetDescription("Request to get offenders list").
                    SetProperty(x => x.GetOffendersListRequest).
                     WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_AfterXtRunning").
                           FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                 AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_BU_TAMPER;HW_NO_GPS").
                    SetProperty(x => x.EquipmentSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock_AfterXtRunning").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock_AfterXtRunning").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ClearTamper).
                    SetProperty(x => x.ProviderID).WithValue(null).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).


                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<AddHandlingActionRegressionAssertBlock>().UsePreviousBlockData().
                SetDescription("Add handling action assert block").

            ExecuteFlow();
        }
        #endregion

        #region Get events
        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_NotIncludeHistory()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.IncludeHistory).WithValue(false).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.IncludeHistory).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.IncludeHistory).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_IncludeHistory()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.IncludeHistory).WithValue(true).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").


            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Limit()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.IncludeHistory).WithValue(false).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Limit).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Limit).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_ProgramType_OnePiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.ProgramType).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.ProgramType).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Agency()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].OffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetName("CreateEntMsgGetEventsRequestBlock_Agency").
                    SetDescription("Create get events request").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty< GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].AgencyID).                    
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_Agency").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_Agency").
                    FromDeepProperty< CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEventsTestBlock_Agency").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.AgencyID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_TimeRange()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.StartTime).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.StartTime).
                    SetProperty(x => x.EndTime).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.EndTime).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_EventID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetName("CreateEntMsgGetEventsRequestBlock_ID").
                    SetDescription("Create get events request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_ID").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ID").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock_ID").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.EventID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Officer()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].OffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetName("CreateEntMsgGetEventsRequestBlock_Agency").
                    SetDescription("Create get events request").
                    SetProperty(x => x.OfficerID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_Agency").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_Agency").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.OfficerID).
                    WithValueFromBlockIndex("GetEventsTestBlock_Agency").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.OfficerID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_OffenderID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetName("CreateEntMsgGetEventsRequestBlock_ID").
                    SetDescription("Create get events request").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].OffenderID).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_ID").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ID").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetEventsTestBlock_ID").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.OffenderID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_OffenderRefID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].OffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                SetName("CreateEntMsgGetEventsRequestBlock_Agency").
                    SetDescription("Create get events request").
                    SetProperty(x => x.OffenderRefID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_Agency").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_Agency").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.OffenderRefID).
                    WithValueFromBlockIndex("GetEventsTestBlock_Agency").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.OffenderRefID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Message()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                SetName("CreateEntMsgGetEventsRequestBlock_ID").
                    SetDescription("Create get events request").
                    SetProperty(x => x.Message).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].Message).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_ID").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ID").
                    FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Message).
                    WithValueFromBlockIndex("GetEventsTestBlock_ID").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Message).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_EventCode()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                SetName("CreateEntMsgGetEventsRequestBlock_ID").
                    SetDescription("Create get events request").
                    SetProperty(x => x.EventCodes).WithValue(new string[1]).
                    SetProperty(x => x.EventCodes[0]).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].Code).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Limit).WithValue(20).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                SetName("GetEventsTestBlock_ID").
                    SetDescription("Get events test").
                    SetProperty(x => x.GetEventsRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEventsRequestBlock_ID").
                            FromDeepProperty<CreateEntMsgGetEventsRequestBlock>(x => x.GetEventsRequest).

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.EventCodes).
                        WithValueFromBlockIndex("GetEventsTestBlock_ID").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.EventCodes).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_ProgramType_TwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.ProgramType).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.ProgramType).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_ProgramType_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.ProgramType).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.ProgramType).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_ProgramType_Alcohol()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.VB_Dual).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.ProgramType).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.ProgramType).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_New()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_AutoProcess()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_Quick()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.Quick).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_Handled()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.Handled).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_InProcess()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.InProcess).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_Batch()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.Batch).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Status_ThirdParty()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.ThirdParty).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Status).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Status).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Severity_Alarm()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Alarm).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Severity).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Severity).
                    SetProperty(x => x.Status).WithValue(null).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Severity_Action()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Action).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Severity).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Severity).
                    SetProperty(x => x.Status).WithValue(null).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Severity_ScheduleTask()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.ScheduleTask).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Severity).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Severity).
                    SetProperty(x => x.Status).WithValue(null).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Severity_System()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.System).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Severity).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Severity).
                    SetProperty(x => x.Status).WithValue(null).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_Severity_Violation()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").
                    SetProperty(x => x.Severity).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsRequest.Severity).
                    SetProperty(x => x.Status).WithValue(null).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_SortDirection_Ascending()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Ascending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventID).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsBySortingAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEvents_SortDirection_Descending()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    //SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    //SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventCode).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventsBySortingAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get events assert block").

            ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetEventsMetaData()
        {
            TestingFlow.

                AddBlock<GetEventsMetaDataTestBlock>().
                    SetDescription("Get events metadata test").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetOffenderEventStatus()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    //SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece).
                    SetProperty(x => x.EventCodes).WithValue(new string[] { "P69", "P51", "P79", "P55", "P57", "P59", "D01" }).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetOffenderEventStatusRequestBlock>().
                    SetDescription("Create get offender event status request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                    FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetOffenderEventStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get offender event status test").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetOpenPanicCalls()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOpenPanicCallsRequestBlock>().
                    SetDescription("Create get open panic calls request").
                    SetProperty(x => x.MaximumRows).WithValue(-1).
                    SetProperty(x => x.StartRowIndex).WithValue(0).

                AddBlock<GetOpenPanicCallsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get open panic calls test").

                AddBlock<GetOpenPanicCallsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        #region Get handling action by query
        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_EventID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_TimeRange()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-10)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.AutoProcess).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                    SetProperty(x => x.EventID).
                    WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-10)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(false).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionTime()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-5)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.ActionFromTime).WithValue(DateTime.Now.AddDays(-5)).
                    SetProperty(x => x.ActionToTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                     SetProperty(x => x.StartTime).WithValue(null).
                    SetProperty(x => x.EndTime).WithValue(null).
                    SetProperty(x => x.ContainsActionType).WithValue(false).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_QuickHandle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(50).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender-related event").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                        FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.QuickHandle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.EventID).WithValue(null).
                    SetProperty(x => x.ContainsActionType).WithValue(true).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_Handle()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.Handle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.EventID).WithValue(null).
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_Remark()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateRemark).


                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.EventID).WithValue(null).
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_Warning()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CreateWarning).


                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.EventID).WithValue(null).
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("AddHandlingActionTestBlock").
                        FromDeepProperty<AddHandlingActionTestBlock>(x => x.AddHandlingActionRequest.Type).

            ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_BatchHandling()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).
                    SetProperty(x => x.EventCodes).WithValue(new string[] { "P69", "P51", "P79", "P55", "MMC", "P57", "P59", "D01" }).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.BatchHandling).


                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.BatchHandling).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        [Description("Waiting for XT Simulator for fixing")]
        public void Events_GetHandlingActionByQuery_ActionType_ClearPanicAlert()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.ClearPanicAlert).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_ClearTamper()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.ClearTamper).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_CustomAction()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.CustomAction).
                    SetProperty(x => x.UserDefinedActionCode).WithValueFromDifferentDataGenerator(EnumPropertyType.UserDefinedActionCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.CustomAction).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_PhoneCall()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<GetUserWithPhoneTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntMsgGetUsersListResponse).
                WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.PhoneCall).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.ID).
                    SetProperty(x => x.PrefixPhone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PrefixPhone).
                    SetProperty(x => x.Phone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PhoneNumber).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.PhoneCall).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_ReturnFromThirdParty()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendToThirdParty).
                    SetProperty(x => x.ThirdPartyActionCode).WithValueFromDifferentDataGenerator(EnumPropertyType.ThirdPartyActionCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.ReturnFromThirdParty).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_ScheduleTask()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                 AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
                    SetProperty(x => x.ScheduledTaskType).WithValueFromDifferentDataGenerator(EnumPropertyType.ScheduleTaskType.ToString()).
                    SetProperty(x => x.ScheduledTaskTime).WithValueFromDifferentDataGenerator(EnumPropertyType.ScheduleTaskTime.ToString()).
                    SetProperty(x => x.ReminderTime).WithValueFromDifferentDataGenerator(EnumPropertyType.ReminderTime.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_SendEmail()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendEmail).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[3].ID).
                    SetProperty(x => x.EmailAddress).WithValueFromDifferentDataGenerator(EnumPropertyType.EmailAddress.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.SendEmail).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_SendFax()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<GetUserWithPhoneTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.EntMsgGetUsersListResponse).
                WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendFax).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.ID).
                    SetProperty(x => x.PrefixPhone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PrefixPhone).
                    SetProperty(x => x.Phone).
                        WithValueFromBlockIndex("GetUserWithPhoneTestBlock").
                            FromDeepProperty<GetUserWithPhoneTestBlock>(x => x.User.Phone.PhoneNumber).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.SendFax).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_SendTextMessage()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendTextMessage).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].ID).
                    SetProperty(x => x.ProviderID).WithValueFromDifferentDataGenerator(EnumPropertyType.PagerProviderID.ToString()).
                    SetProperty(x => x.PagerCode).WithValueFromDifferentDataGenerator(EnumPropertyType.PagerCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.SendTextMessage).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_SendToThirdParty()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendToThirdParty).
                    SetProperty(x => x.ThirdPartyActionCode).WithValueFromDifferentDataGenerator(EnumPropertyType.ThirdPartyActionCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.SendToThirdParty).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_Unhandled()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.Unhandled).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<GetHandlingActionByQueryByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query assert").
                    SetProperty(x => x.ContainsActionType).WithValue(true).
                    SetProperty(x => x.ActionType).
                    WithValueFromBlockIndex("CreateEntMsgGetHandlingActionByQueryRequestBlock").
                        FromDeepProperty<CreateEntMsgGetHandlingActionByQueryRequestBlock>(x => x.ActionTypeList[0]).

            ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.Events_Regression)]
        [TestMethod]
        public void Events_HandleAllEvents()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(Equipment0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Model).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER;HW_DEVICE_BAT_LOW;HW_BTX_STRAP;HW_HW_RULE_SHUTDOWN").
                    //SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Bracelet_Gone-first_violation;Device_Tamper-violation;Bracelet_Battery_Low-violation;Device_Battery_Low-first_violation;Bracelet_Strap-first_violation;Bracelet_Body_Tamper-violation;Tracker_Shutdown-violation").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).

                AddBlock<CreateEntMsgHandleAllEventsRequestBlock>().
                    SetDescription("Create handle all events request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<HandleAllEventsTestBlock>().UsePreviousBlockData().
                        SetDescription("Handle all events test").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_AfterHandling").
                    SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Status).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(Events0.EnmProgramType.One_Piece).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<HandleAllEventsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }
    }
}
