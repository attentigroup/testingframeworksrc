﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingFramework.TestsInfraStructure.Implementation;

namespace TestingFramework.UnitTests
{
    /// <summary>
    /// Assembly initialization for testing framework.
    /// this code need to implemented in every testing project in order to initialize the framework properly.
    /// see this link for more information: https://connect.microsoft.com/VisualStudio/feedback/details/1834204/test-executiion-will-not-run-assemblyinitialize-or-assemblycleanup-in-a-base-class
    /// </summary>
    public class TestsAssemblyActions : BaseUnitTest
    {
        #region Assembly methods
        /// <summary>
        /// The Assembly level methods must be implemented in the same class of the tests.
        /// </summary>
        /// <param name="testContext"></param>
        [AssemblyInitialize]
        public static void TestingFrameworkAssemblyInit(TestContext testContext)
        {
            BaseUnitTest.AssemblyInit(testContext);
        }
        /// <summary>
        /// cleanup method for the testing framework assembly
        /// </summary>
        [AssemblyCleanup]
        public static void TestingFrameworkAssemblyCleanup()
        {
            BaseUnitTest.AssemblyCleanup();
        }
        #endregion Assembly methods
    }
}
