﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Categories;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.DevicesBlocks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using System.Collections.Generic;
using DataGenerators.Agency;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using TestBlocksLib.GroupsBlocks;
using LogicBlocksLib.Group;
using TestBlocksLib.LogBlocks;
using TestBlocksLib.QueueBlocks;
using LogicBlocksLib.QueueBlocks;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;

namespace TestingFramework.UnitTests.OffendersSvcTests
{
    [TestClass]
    public class OffendersRegressionTests : BaseUnitTest
    {
        #region Add contact
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffenderContact_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).
                        
               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).WithValue(null).
                        //WithValueFromBlockIndex("GetOffendersTestBlock").
                        //    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

                AddBlock<GetOffenderContactsListAssertBlock>().
                    SetDescription("Get Offender contact list assert block").

               ExecuteFlow();
        }
        #endregion

        #region Add address
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffenderAddress_WithoutZone_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                    AddBlock<AddOffenderAddressAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffenderAddress_WithZone_1Piece_GPS_RF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                        Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                    AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.106356, LON = 34.834755 }).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Street).WithValue("HaBarzel").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

               AddBlock<AddOffenderAddressAssertBlock>().
               SetProperty(x => x.GetOffendersResponse).
                   WithValueFromBlockIndex("GetOffendersTestBlock_After").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffenderAddress_1Piece_GPS_RF_WithSeveralPhones()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                    AddBlock<AddOffenderAddressAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffenderAddress_WithoutZone_2Piece_GPS()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

               AddBlock<AddOffenderAddressAssertBlock>().
               SetProperty(x => x.GetOffendersResponse).
                   WithValueFromBlockIndex("GetOffendersTestBlock_After").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffenderAddress_WithZone_2Piece_GPS()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                        Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                    AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneName).WithValue("Work").
                    SetProperty(x => x.Point).WithValue(new Zones0.EntPoint() { LAT = 32.106356, LON = 34.834755 }).
                    SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Street).WithValue("HaBarzel").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

               AddBlock<AddOffenderAddressAssertBlock>().
               SetProperty(x => x.GetOffendersResponse).
                   WithValueFromBlockIndex("GetOffendersTestBlock_After").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }
        #endregion

        #region Add offender
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_E4_AllowVoiceTest()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.AllowVoiceTests).WithValue(true).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Voice_Verification). 
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.LandlinePhoneNumber).WithValue("3455").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }
           
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_E4_NotAllowVoiceTest_IsSecondaryLocation()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().ShallGeneratePropertiesWithRandomValues().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.E4_Dual).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().
                SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).
                SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(1).
                    //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    //SetProperty(x => x.VoicePhoneNumber).WithValue("4720060").
                    //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    //SetProperty(x => x.DataPhoneNumber).WithValue("4720060").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x=> x.IsSecondaryLocation).WithValue(false).
                    SetProperty(x => x.LandlinePhoneNumber).WithValue(null).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.IsSecondaryLocation).WithValue(true).
                    SetProperty(x => x.RefID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.RefID).
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.FirstName).
                    SetProperty(x => x.LastName).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.LastName).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<StringToEntStringParameterConverter>().
                    SetProperty(x => x.ElementToConvert).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.RefID).

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderRefIDQueryParameter).
                    WithValueFromBlockIndex("StringToEntStringParameterConverter").
                            FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_RefID").
                    SetDescription("Get Offenders Test Block").

               AddBlock<AddOffenderIsSecondaryLocationAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_RefID").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_E4_AllowVoiceTest_IsSecondaryLocation()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(EnmProgramType.E4_Dual).
                    SetProperty(x => x.Model).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.IsSecondaryLocation).WithValue(false).
                    SetProperty(x => x.AllowVoiceTests).WithValue(true).
                    

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.IsSecondaryLocation).WithValue(true).
                    SetProperty(x => x.RefID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.RefID).
                    SetProperty(x => x.FirstName).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.FirstName).
                    SetProperty(x => x.LastName).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.LastName).
                    SetProperty(x => x.LandlinePhoneNumber).WithValue(null).
                    //SetProperty(x => x.LandlinePrefixPhone).WithValue("12").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<StringToEntStringParameterConverter>().
                    SetProperty(x => x.ElementToConvert).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.RefID).

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderRefIDQueryParameter).
                        WithValueFromBlockIndex("StringToEntStringParameterConverter").
                            FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.ProgramType).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_RefID").
                    SetDescription("Get Offenders Test Block").

               AddBlock<AddOffenderIsSecondaryLocationAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_RefID").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_E4_WithCustomFields()
        {

            TestingFlow.
     
                AddBlock<GetEquipmentWithoutOffender>().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.E4_Dual).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

                AddBlock<AddCustomFieldsAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_1Piece_GPS_RF_WithoutReceiver()
        {

            TestingFlow.
               
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    //SetProperty(x => x.Language).WithValue("ENG").  

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_1Piece_GPS_RF_WithoutHomeUnit_WithReceiver()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.One_Piece_RF).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }
      

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_1Piece_GPS_RF_WithReceiver_WithCustomFields()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.One_Piece_RF).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

                AddBlock<AddCustomFieldsAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_2Piece_GPS_Aggressor()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetName("CreateEntMsgAddEquipmentRequestBlock_for_first_equipment").
                    SetDescription("CreateEntMsgAddEquipmentRequestBlock_for_first_equipment").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                        
               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_2P").
                   SetDescription("Add equipment test block").

                    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_2P").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                 AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("create random EntMsgAddEquipmentRequest type").
                     SetProperty(x => x.AgencyID).
                     WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock_for_first_equipment").
                             FromDeepProperty<CreateEntMsgAddEquipmentRequestBlock>(x => x.AgencyID).
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Transmitter_DV).
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.NotDefined).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_2).
                     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_DV.ToString()).

                 AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                  SetName("AddEquipmentTestBlock_Transsmiter").
                     SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2P").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.SerialNumber).
                    SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock_2P").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.Transmitter).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_Transsmiter").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_2Piece_GPS_Victim()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.Two_Piece).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.Two_Piece_3G).//2Piece_v6

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_2Piece_GPS_WithCustomFields()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.Two_Piece).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.Two_Piece_3G).//2Piece_v6

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

                AddBlock<AddCustomFieldsAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_2Piece_DV_WithCustomFields()
        {

            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                SetProperty(x => x.ProgramType).WithValue(EnmProgramType.Two_Piece).
                SetProperty(x => x.Model).WithValue(EnmEquipmentModel.Two_Piece_3G).//2Piece_v6

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

                AddBlock<AddCustomFieldsAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_Voice_WithReceiver()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(EnmProgramType.E4_Dual).
                    SetProperty(x => x.Model).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                           SetProperty(x => x.AllowVoiceTests).WithValue(true).
                          // SetProperty(x => x.LandlinePhoneNumber).WithValue("3455").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddOffender_Alcohol_WithReceiver()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(EnmProgramType.VBR_Cellular).
                    SetProperty(x => x.Model).WithValue(EnmEquipmentModel.Alcohol_Cell_VBR).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VBR_Cellular).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                     

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        #endregion

        #region Add picture
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_AddPicture_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    
               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(@"OffendersSvcTests\AddOffenderPicture.jpg").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

              AddBlock<CreateEntMsgGetOffenderPicturesRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.PictureID).
                        WithValueFromBlockIndex("AddOffenderPictureTestBlock").
                            FromDeepProperty<AddOffenderPictureTestBlock>(x => x.AddOffenderPictureResponse.NewPhotoID).

              AddBlock<GetOffenderPicturesTestBlock>().UsePreviousBlockData().

              AddBlock<AddOffenderPictureAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Delete contact
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_DeleteOffenderContact_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<CreateEntMsgDeleteOffenderContactRequestBlock>().
                    SetDescription("Create delete Offender contact Request").
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<DeleteOffenderContactRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

                    AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).WithValue(null).
                        //WithValueFromBlockIndex("GetOffendersTestBlock").
                        //    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

                AddBlock<DeleteOffenderContactAssertBlock>().
                    SetDescription("Get Offender contact list assert block").

               ExecuteFlow();
        }
        #endregion

        #region Delete address
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_DeleteOffenderAddress_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgDeleteOffenderAddressRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create delete Offender contact Request").
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock").
                            FromDeepProperty<AddOffenderAddressTestBlock>(x => x.AddOffenderAddressResponse.NewAddressID).

               AddBlock<DeleteOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

                    AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

               ExecuteFlow();
        }
        #endregion

        #region Delete offender
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_DeleteOffender_1Piece_GPS_RF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.EnvironmentPreparation_Users_Tests)]
        [TestMethod]
        public void Offenders_DeleteAllOffenders()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<DeleteAllOffenders>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.GetOffendersResponse).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.LastTimestamp).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_DeleteOffender_2Piece_GPS()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).  

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_DeleteOffender_2Piece_DV()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_DeleteOffender_E4()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }
        #endregion

        #region Delete picture
        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void Offenders_DeleteOffenderPicture_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(@"OffendersSvcTests\AddOffenderPicture.jpg").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgDeleteOffenderPictureRequestBlock>().
                    SetDescription("Create Delete Offender picture Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock").
                             FromDeepProperty<AddOffenderPictureTestBlock>(x => x.AddOffenderPictureResponse.NewPhotoID).

               AddBlock<DeleteOffenderPictureTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Offender picture Test Block").


               ExecuteFlow();
        }
        #endregion

        #region Get contacts list
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetContactsList_ByContactID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).WithValue(null).
                        //WithValueFromBlockIndex("GetOffendersTestBlock").
                        //    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

                AddBlock<GetOffenderContactsListBy_AssertBlock>().
                    SetDescription("Get Offender contact list assert block").
                    SetProperty(x => x.ExpectedOffenderID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock").
                            FromDeepProperty<GetOffenderContactsListTestBlock>(x => x.GetOffenderContactsListRequest.OffenderID).
                    SetProperty(x => x.ExpectedContactID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock").
                            FromDeepProperty<GetOffenderContactsListTestBlock>(x => x.GetOffenderContactsListRequest.ContactID).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetContactsList_ByOffenderID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ContactID).WithValue(null).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

                AddBlock<GetOffenderContactsListBy_AssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list assert block").
                    SetProperty(x => x.ExpectedOffenderID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock").
                            FromDeepProperty<GetOffenderContactsListTestBlock>(x => x.GetOffenderContactsListRequest.OffenderID).

               ExecuteFlow();
        }
        #endregion

        #region Get offenders list
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_AgencyID()
        {
            TestingFlow.
     
               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.AgencyIDQueryParameter).WithValue(new Offenders0.EntNumericParameter()
                        { Operator = Offenders0.EnmNumericOperator.Equal, Value = AgencyIdGenerator.AgencyIdList[0] }).
                    SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.AgencyID.Value).

               ExecuteFlow();
        }

        
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_FirstName()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.FirstNameQueryParameter).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.FirstName).
                    SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.FirstName).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.FirstName).

               ExecuteFlow();
        }

        
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_LastName()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<StringToEntStringParameterConverter>().
                    SetProperty(x => x.ElementToConvert).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].LastName).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.LastNameQueryParameter).
                    WithValueFromBlockIndex("StringToEntStringParameterConverter").
                    FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.LastName).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.LastName).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_Limit()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
 
                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.Limit).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.Limit).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_1Piece_GPS_RF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_2Piece_GPS()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_E4()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_E3()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.RF_Cellular }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_Voice()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(EnmProgramType.E4_Dual).
                    SetProperty(x => x.Model).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Voice_Verification).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                           SetProperty(x => x.AllowVoiceTests).WithValue(true).
                           SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                           SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1).AddDays(-1)).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Voice_Verification }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).
                SetProperty(x => x.GetOffendersResponse).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                SetProperty(x => x.SocialSecurity).WithValue(null).
                SetProperty(x => x.Receiver).WithValue(null).
                SetProperty(x => x.OfficerID).WithValue(null).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_Alcohol()
        {
            TestingFlow.

                AddBlock<GetEquipmentWithoutOffender>().
                    SetProperty(x => x.ProgramType).WithValue(EnmProgramType.VBR_Cellular).
                    SetProperty(x => x.Model).WithValue(EnmEquipmentModel.Alcohol_Cell_VBR).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VBR_Cellular).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                            FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.VBR_Cellular }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).
                SetProperty(x => x.GetOffendersResponse).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                SetProperty(x => x.SocialSecurity).WithValue(null).
                SetProperty(x => x.Receiver).WithValue(null).
                SetProperty(x => x.OfficerID).WithValue(null).
                SetProperty(x => x.AgencyID).WithValue(null).
          
               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramType_WithSeveralProgramTypes()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF,
                    Offenders0.EnmProgramType.Two_Piece}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramType).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramType).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramStatus_PostActive()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PostActive }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramStatus).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramStatus_PreActive()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramStatus).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).

               ExecuteFlow();
        }

        //[TestCategory(CategoriesNames.Offenders_Regression)]
        //[TestMethod]
        //public void Offender_GetOffendersList_ProgramStatus_Rejected()
        //{
        //    TestingFlow.

        //       AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
        //            SetDescription("Create Get Offenders Request").
        //            //SetProperty(x => x.Limit).WithValue(20).
        //            SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Rejected }).

        //       AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
        //            SetDescription("Get Offenders Test Block").

        //        AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
        //        SetProperty(x => x.ProgramStatus).
        //        WithValueFromBlockIndex("GetOffendersTestBlock").
        //        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).

        //       ExecuteFlow();
        //}

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramStatus_Suspended()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211123").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    //SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Suspended }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ProgramStatus).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).
                    SetProperty(x => x.ProgramType).WithValue(null).
                    SetProperty(x => x.SocialSecurity).WithValue(null).
                    SetProperty(x => x.Receiver).WithValue(null).
                    SetProperty(x => x.OffenderID).WithValue(null).
                    SetProperty(x => x.OfficerID).WithValue(null).
                    SetProperty(x => x.AgencyID).WithValue(0).

            // AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
            //     SetDescription("Create Get Offenders Request").
            //     SetProperty(x => x.OffenderID).
            //         WithValueFromBlockIndex("AddOffenderTestBlock").
            //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

            //AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
            //SetName("GetOffendersTestBlock_Suspend").
            //     SetDescription("Get Offenders Test Block").

            //AddBlock<OffenderStatusAssertBlock>().
            //    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Suspended).
            //    SetProperty(x => x.GetOffendersResponse).
            //        WithValueFromBlockIndex("GetOffendersTestBlock_Suspend").
            //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

            //AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
            //    SetDescription("Create EntMsgSuspendProgramRequest Block").
            //    SetProperty(x => x.Comment).WithValue(null).
            //    SetProperty(x => x.Reason).WithValue(null).
            //    SetProperty(x => x.OffenderID).
            //        WithValueFromBlockIndex("AddOffenderTestBlock").
            //            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

            //AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
            //    SetDescription("Suspend Program Request Test Block").

            //AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
            //    SetDescription("Run XT Simulator Test Block").
            //    SetProperty(x => x.EquipmentSN).
            //        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
            //            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

            //AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
            //     SetDescription("Create Get Offenders Request").
            //     SetProperty(x => x.OffenderID).
            //         WithValueFromBlockIndex("AddOffenderTestBlock").
            //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

            //AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
            //     SetDescription("Get Offenders Test Block").

            //AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
            //    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

            //AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
            //    SetDescription("Create Send End Of Service Request").
            //    SetProperty(x => x.OffenderID).
            //         WithValueFromBlockIndex("AddOffenderTestBlock").
            //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
            //    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
            //    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
            //    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

            //AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
            //    SetDescription("Send End Of Service Test Block").

            //AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
            //    SetDescription("Run XT Simulator Test Block").
            //    SetProperty(x => x.EquipmentSN).
            //        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
            //            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
            //    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

            //AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
            //     SetDescription("Create Get Offenders Request").
            //     SetProperty(x => x.OffenderID).
            //         WithValueFromBlockIndex("AddOffenderTestBlock").
            //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

            //AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
            //     SetDescription("Get Offenders Test Block").

            //AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
            //    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).              

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramStatus_Active()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramStatus).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramStatus_Archieve()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.IncludeArchived).WithValue(true).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Archived }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramStatus).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramStatus_SeveralStatuses()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                    Offenders0.EnmProgramStatus.PreActive}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramStatus).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramStatus).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramConcept_Regular()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Regular).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramConcept).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramConcept).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramConcept_Aggressor()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramConcept).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramConcept).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_ProgramConcept_Victim()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Victim).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramConcept).
                WithValueFromBlockIndex("GetOffendersTestBlock").
                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.ProgramConcept).

               ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_Receiver()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                    AddBlock<StringToEntStringParameterConverter>().
                    SetProperty(x => x.ElementToConvert).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ReceiverQueryParameter).
                    WithValueFromBlockIndex("StringToEntStringParameterConverter").
                    FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetOffendersTestBlock_Receiver").
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.Receiver).
                    WithValueFromBlockIndex("GetOffendersTestBlock_Receiver").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.Receiver).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_SocialSecurityNumber()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Language).WithValue("ENG").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<StringToEntStringParameterConverter>().
                    SetProperty(x => x.ElementToConvert).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.SocialSecurity).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.SocialSecurityNumberQueryParameter).
                    WithValueFromBlockIndex("StringToEntStringParameterConverter").
                    FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.AgencyIDQueryParameter).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).
                    SetProperty(x => x.ProgramConcept).WithValue(null).                    
                    

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_SocialSecurity").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SocialSecurity).
                    WithValueFromBlockIndex("GetOffendersTestBlock_SocialSecurity").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.SocialSecurityNumber).
                    SetProperty(x => x.OfficerID).WithValue(null).
                    SetProperty(x => x.OffenderRefID).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_OffenderID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                        Offenders0.EnmProgramStatus.Active }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetOffendersTestBlock_OffenderID").
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock_OffenderID").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.OffenderID.Value).
                    SetProperty(x => x.ProgramStatus).WithValue(null).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_OfficerID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                        Offenders0.EnmProgramStatus.Active }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                  AddBlock<IntToEntNumericParameterConverter>().
                  SetProperty(x => x.ElementToConvert).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OfficerIDQueryParameter).
                    WithValueFromBlockIndex("IntToEntNumericParameterConverter").
                    FromDeepProperty<IntToEntNumericParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetOffendersTestBlock_OfficerID").
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OfficerID).
                    WithValueFromBlockIndex("GetOffendersTestBlock_OfficerID").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.OfficerID).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_OffenderRefID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                        Offenders0.EnmProgramStatus.Active }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                    AddBlock<StringToEntStringParameterConverter>().
                    SetProperty(x => x.ElementToConvert).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RefID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderRefIDQueryParameter).
                    WithValueFromBlockIndex("StringToEntStringParameterConverter").
                    FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               SetName("GetOffendersTestBlock_OffenderID").
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.OffenderRefID).
                    WithValueFromBlockIndex("GetOffendersTestBlock_OffenderID").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.OffenderRefID).
                    SetProperty(x => x.ProgramStatus).WithValue(null).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_IncludeAddressList()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<GetOffendersListAssertBlock>().
                    SetProperty(x => x.IncludeAddressList).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.IncludeAddressList).
                            SetProperty(x => x.ProgramStatus).WithValue(null).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_NotIncludeAddressList()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).
                            SetProperty(x => x.IncludeAddressList).WithValue(false).

               AddBlock<GetOffendersTestBlock>().//UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<GetOffendersListAssertBlock>().
                    SetProperty(x => x.IncludeAddressList).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.IncludeAddressList).
                            SetProperty(x => x.ProgramStatus).WithValue(null).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_EndOfServiceCode()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PostActive }).
                    SetProperty(x => x.EndOfServiceCodeQueryParameter).WithValue(new Offenders0.EntStringParameter() {Operator = Offenders0.EnmStringOperator.Equal, Value = "AEOS" }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EndOfServiceCode).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.EndOfServiceCode).
                    SetProperty(x => x.ProgramStatus).WithValue(null).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_GroupID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().

                AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    //SetProperty(x => x.Limit).WithValue(-1).
                    SetProperty(x => x.GroupID).
                    WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                    FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive, Offenders0.EnmProgramStatus.Active,
                    Offenders0.EnmProgramStatus.PostActive}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListByGroupIdAssertBlock>().
                    SetProperty(x => x.Group).
                    WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                    FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_PhoneNumber()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.Limit).WithValue(500).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                    AddBlock< GetOffenderWithPhoneNumberTestBlock>().UsePreviousBlockData().

                AddBlock<StringToEntStringParameterConverter>().UsePreviousBlockData().
                    SetProperty(x => x.ElementToConvert).
                    WithValueFromBlockIndex("GetOffenderWithPhoneNumberTestBlock").
                    FromDeepProperty<GetOffenderWithPhoneNumberTestBlock>(x => x.Offender.ContactInfo.CellData.VoicePhoneNumber).
                    SetProperty(x => x.Operator).WithValue(Offenders0.EnmStringOperator.Contains).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetName("CreateEntMsgGetOffendersListRequestBlock_Phone").
                    SetProperty(x => x.ProgramPhoneNumberQueryParameter).
                    WithValueFromBlockIndex("StringToEntStringParameterConverter").
                    FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    //SetProperty(x => x.Limit).WithValue(20).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Phone").
                    SetProperty(x => x.GetOffendersListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_Phone").
                    FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<GetOffendersListAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.ProgramPhoneNumberQueryParameter).
                WithValueFromBlockIndex("StringToEntStringParameterConverter").
                    FromDeepProperty<StringToEntStringParameterConverter>(x => x.ConvertedValue).
                    SetProperty(x => x.GetOffendersResponse).
                    WithValueFromBlockIndex("GetOffendersTestBlock_Phone").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_LastTimeStamp()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[]{ Offenders0.EnmProgramType.One_Piece_RF}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Language).WithValue("ENG").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_TimeStamp").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.LastTimeStamp).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.ResponseTimeStamp).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_TimeStamp").
                     SetProperty(x => x.GetOffendersListRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_TimeStamp").
                    FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<GetOffendersListByTimeStampAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffendersResponseByTimeStamp).
                    WithValueFromBlockIndex("GetOffendersTestBlock_TimeStamp").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_SortDirection_Ascending()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Ascending).
                    SetProperty(x => x.SortField).WithValue(Offenders0.EnmOffendersSortOptions.OffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_Sorted").
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListSortAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffendersResponseSorted).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Sorted").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                           SetProperty(x => x.SortDirection).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Sorted").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.SortDirection).
                    SetProperty(x => x.SortField).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Sorted").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.SortField).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffendersList_SortDirection_Descending()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).


               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Offenders0.EnmOffendersSortOptions.OffenderRefID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_Sorted").
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffendersListSortAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffendersResponseSorted).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Sorted").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                           SetProperty(x => x.SortDirection).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Sorted").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.SortDirection).
                    SetProperty(x => x.SortField).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Sorted").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersListRequest.SortField).


               ExecuteFlow();
        }


        #endregion

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetCustomFieldDefinitions()
        {
            TestingFlow.

               AddBlock<GetCustomFieldsDefinitionsTestBlock>().
                    
                AddBlock<GetCustomFieldsDefinitionsAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_GetOffenderCurrentStatus()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.Limit).WithValue(1).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffenderCurrentStatusRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<GetOffenderCurrentStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<GetOffenderCurrentStatusAssertBlock>().
                    SetDescription("Get Offender Current Status Assert Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_GetOffenderPictureIDList()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(@"OffendersSvcTests\AddOffenderPicture.jpg").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffenderPictureIDListRequestBlock>().
                    SetDescription("Create get Offender picture ID list Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderPictureIDListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender picture ID list Test Block").

               AddBlock<GetOffenderPictureIDListAssertBlock>().
                    SetDescription("Get Offender picture ID list Assert Block").


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_GetDeletedOffenders()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).
                    SetProperty(x => x.GetAgenciesListForMCResponse).WithValue(null).           

               AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                    SetDescription("Create Delete Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               AddBlock<CreateEntMsgGetDeletedOffendersRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Deleted Offenders Request").
               //SetProperty(x => x.LastTimestamp).
               //    WithValueFromBlockIndex("GetOffendersTestBlock").
               //        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<GetDeletedOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Deleted Offenders Test Block").

               AddBlock<GetDeletedOffendersAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffenderContact()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

                AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).WithValue(null).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").
                    SetName("GetOffenderContactsListTestBlock_Before").

               AddBlock<CreateEntMsgUpdateOffenderContactRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.Phone).WithRandomGenerateValue().
                    SetProperty(x => x.PrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.Priority).WithRandomGenerateValue().
                    SetProperty(x => x.Relation).WithRandomGenerateValue().
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<UpdateOffenderContactTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender contact test block").

                AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID)./*WithValue(2635).*/
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ContactID)./*WithValue(101).*/
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").
                    SetName("GetOffenderContactsListTestBlock_After").

                AddBlock<UpdateOffenderContactAssertBlock>().
                    SetDescription("Get Offender contact list assert block").
                    SetProperty(x => x.GetOffenderContactsListResponse).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock_Before").
                            FromDeepProperty<GetOffenderContactsListTestBlock>(x => x.GetOffenderContactsListResponse).
                    SetProperty(x => x.GetOffenderContactsListResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock_After").
                            FromDeepProperty<GetOffenderContactsListTestBlock>(x => x.GetOffenderContactsListResponse).

               ExecuteFlow();
        }
        
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffenderAddress()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

               AddBlock<CreateEntMsgUpdateOffenderAddressRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update Offender Address Request").
                    SetProperty(x => x.Description).WithValue("Update Address Test").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock").
                            FromDeepProperty<AddOffenderAddressTestBlock>(x => x.AddOffenderAddressResponse.NewAddressID).
                    SetProperty(x => x.ZipCode).WithRandomGenerateValue().
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Phones).WithValue(null).
                    //SetProperty(x => x.Description).WithValue("Automation").

                AddBlock<UpdateOffenderAddressTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                            SetProperty(x => x.IncludeAddressList).WithValue(true).
                            SetProperty(x => x.LastTimeStamp).WithValue(null).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

               AddBlock<UpdateOffenderAddressAssertBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        #region Update offender
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).  
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF //
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).
                    SetProperty(x => x.ContactPhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.ContactPrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.CourtOrderNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlineDialOutPrefix).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.ZipCode).WithRandomGenerateValue().

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffenderEquipment_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.AgencyID).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.OfficerID).WithValue(null).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF //
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    //SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).
                    SetProperty(x => x.ContactPhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.ContactPrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.CourtOrderNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlineDialOutPrefix).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.ZipCode).WithRandomGenerateValue().
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromDeepProperty<CreateEntMsgAddEquipmentRequestBlock>(x => x.EquipmentSerialNumber).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                //AddBlock<UpdateOffenderAssertBlock>().
                //    SetProperty(x => x.GetOffendersResponse).
                //        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                //            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                //        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                //            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_2Piece_GPS()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).//
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). 
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).
                    SetProperty(x => x.ContactPhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.ContactPrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.CourtOrderNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlineDialOutPrefix).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.ZipCode).WithRandomGenerateValue().

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_E4()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.RF_Dual). 
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).//
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.RF_Dual).
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).
                    SetProperty(x => x.ContactPhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.ContactPrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.CourtOrderNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlineDialOutPrefix).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePhoneNumber).WithRandomGenerateValue().
                    SetProperty(x => x.LandlinePrefixPhone).WithRandomGenerateValue().
                    SetProperty(x => x.ZipCode).WithRandomGenerateValue().

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_2Piece_DV()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). 
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                        //WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        //    FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). 
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_E4_AllowVoiceTest()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.RF_Dual).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AllowVoiceTests).WithValue(true).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).//
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                        //WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        //    FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.RF_Dual). 
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AllowVoiceTests).WithValue(false).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).


               ExecuteFlow();
        }

        
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_1Piece_GPS_RF_AddCustomField()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(null).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).//
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                    //    WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                    //        FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF //
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() {ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

                AddBlock<AddCustomFieldsAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestProperty("BUG", "33145")]
        [TestMethod]
        public void Offenders_UpdateOffender_1Piece_GPS_RF_DeleteCustomField()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).  
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                        //WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        //    FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF //
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(null).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

                AddBlock< DeleteCustomFieldsAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_1Piece_GPS_RF_EditCustomField()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 7, Name = "yud", Type = Offenders0.EnmFieldType.Date } }).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                        //WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        //    FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF //
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.VisibleCustomFieldsDefinitions).WithValue(new Offenders0.EntCustomFieldDefinition[] { new Offenders0.EntCustomFieldDefinition() { ID = 8, Name = "Birthday", Type = Offenders0.EnmFieldType.Date } }).
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                    SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_Alcohol()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VBR_Cellular).  
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[0]).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                        //WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        //    FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.VBR_Cellular). 
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).


               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_UpdateOffender_E4_IsSecondaryLocation()
        {
            TestingFlow.

                 AddBlock<GetEquipmentWithoutOffender>().
                     SetProperty(x => x.ProgramType).WithValue(EnmProgramType.E4_Dual).
                     SetProperty(x => x.Model).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).

                 AddBlock<CreateEntMsgAddEquipmentRequestBlock>().
                     SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                                 FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.RF_Curfew_Dual_E4).
                     SetProperty(x => x.EquipmentSerialNumber).WithValue("100122").
                     SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).

                 AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().

                 AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create EntMsgAddCellularDataRequest Type").
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                     SetProperty(x => x.ProviderID).WithValue(1).
                     //SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                     //SetProperty(x => x.VoicePhoneNumber).WithValue("4720060").
                     //SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                     //SetProperty(x => x.DataPhoneNumber).WithValue("4720060").

                 AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Cellular Data test block").

                 AddBlock<CreateEntMsgGetEquipmentListRequest>().
                     SetProperty(x => x.EquipmentID).
                         WithValueFromBlockIndex("AddEquipmentTestBlock").
                             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                 AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().

                 AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create Add Offender Request").
                     SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                     SetProperty(x => x.Language).WithValue("ENG").
                     SetProperty(x => x.Receiver).
                         WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                             FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.SerialNumber).
                     SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                             FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                     SetProperty(x => x.IsSecondaryLocation).WithValue(false).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Add Offender test block").

                 AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create Add Offender Request").
                     SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                     SetProperty(x => x.Language).WithValue("ENG").
                     SetProperty(x => x.Receiver).
                         WithValueFromBlockIndex("GetEquipmentListTestBlock").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                     SetProperty(x => x.AgencyID).
                         WithValueFromBlockIndex("GetEquipmentWithoutOffender").
                             FromDeepProperty<GetEquipmentWithoutOffender>(x => x.Equipment.AgencyID).
                     SetProperty(x => x.IsSecondaryLocation).WithValue(true).
                     SetProperty(x => x.RefID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.RefID).
                     SetProperty(x => x.FirstName).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.FirstName).
                             SetProperty(x => x.LastName).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderRequest.LastName).
                             SetProperty(x => x.LandlinePhoneNumber).WithValue(null).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                SetName("AddOffenderTestBlock_Secondary").
                     SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_Secondary").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_Before").

                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").//
                    SetProperty(x => x.DateOfBirth).WithRandomGenerateValue().
                    SetProperty(x => x.FirstName).WithRandomGenerateValue().
                    SetProperty(x => x.LastName).WithRandomGenerateValue().
                    SetProperty(x => x.Gender).WithRandomGenerateValue().
                    SetProperty(x => x.Language).WithValue("ENG").//
                    SetProperty(x => x.OffenderID).//
                        WithValueFromBlockIndex("AddOffenderTestBlock_Secondary").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).WithValue(null).
                        //WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                        //    FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.StateID).WithValue("IL").//
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.DepartmentOfCorrection).WithRandomGenerateValue().
                    SetProperty(x => x.SocialSecurity).WithRandomGenerateValue().
                    SetProperty(x => x.MiddleName).WithRandomGenerateValue().
                    SetProperty(x => x.AgencyID).WithValue(AgencyIdGenerator.AgencyIdList[1]).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                SetName("CreateEntMsgGetOffendersListRequestBlock_After").
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_Secondary").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    SetName("GetOffendersTestBlock_After").
                    SetProperty(x => x.GetOffendersListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetOffendersListRequestBlock_After").
                            FromDeepProperty<CreateEntMsgGetOffendersListRequestBlock>(x => x.GetOffendersListRequest).

                AddBlock<UpdateOffenderAssertBlock>().
                SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Before").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).
                            SetProperty(x => x.GetOffendersResponseAfterUpdate).
                        WithValueFromBlockIndex("GetOffendersTestBlock_After").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

               ExecuteFlow();
        }

        #endregion

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offender_GetOffenderCounters()
        {
            TestingFlow.

               AddBlock<GetOffenderCountersTestBlock>().

               AddBlock<GetOffenderCountersAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        #region Get pictures
        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_GetPictures_ByOffenderID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(@"OffendersSvcTests\AddOffenderPicture.jpg").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

              AddBlock<CreateEntMsgGetOffenderPicturesRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

              AddBlock<GetOffenderPicturesTestBlock>().UsePreviousBlockData().

              AddBlock<GetOffenderPicturesAssertBlock>().
              SetProperty(x => x.ExpectedOffenderID).
                        WithValueFromBlockIndex("GetOffenderPicturesTestBlock").
                            FromDeepProperty<GetOffenderPicturesTestBlock>(x => x.GetPicturesRequest.OffenderID).
                    SetProperty(x => x.ExpectedPictureID).
                        WithValueFromBlockIndex("GetOffenderPicturesTestBlock").
                            FromDeepProperty<GetOffenderPicturesTestBlock>(x => x.GetPicturesRequest.PictureID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Regression)]
        [TestMethod]
        public void Offenders_GetPictures_ByPictureID()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(@"OffendersSvcTests\AddOffenderPicture.jpg").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

              AddBlock<CreateEntMsgGetOffenderPicturesRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.PictureID).
                  WithValueFromBlockIndex("AddOffenderPictureTestBlock").
                      FromDeepProperty<AddOffenderPictureTestBlock>(x => x.AddOffenderPictureResponse.NewPhotoID).

              AddBlock<GetOffenderPicturesTestBlock>().UsePreviousBlockData().

              AddBlock<GetOffenderPicturesAssertBlock>().
                            SetProperty(x => x.ExpectedOffenderID).
                        WithValueFromBlockIndex("GetOffenderPicturesTestBlock").
                            FromDeepProperty<GetOffenderPicturesTestBlock>(x => x.GetPicturesRequest.OffenderID).
                    SetProperty(x => x.ExpectedPictureID).
                        WithValueFromBlockIndex("GetOffenderPicturesTestBlock").
                            FromDeepProperty<GetOffenderPicturesTestBlock>(x => x.GetPicturesRequest.PictureID).

            ExecuteFlow();
        }
        #endregion

    }
}
