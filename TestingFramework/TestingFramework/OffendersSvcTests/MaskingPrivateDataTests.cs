﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using Equipment = TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Categories;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.DevicesBlocks;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using System.Collections.Generic;
using DataGenerators.Agency;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using TestBlocksLib.LogBlocks;
using TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using TestBlocksLib;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using LogicBlocksLib.UsersBlocks;
using TestBlocksLib.EventsBlocks;
using AssertBlocksLib.Events;
using LogicBlocksLib.Events;
using TestBlocksLib.UsersBlocks;
using Users0 = TestingFramework.Proxies.EM.Interfaces.Users;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using LogicBlocksLib.Trails;
using TestBlocksLib.Trails;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using DataGenerators.Offender;
using AssertBlocksLib.MaskingPrivateData;
using LogicBlocksLib.Group;
//using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestBlocksLib.GroupsBlocks;
using Groups0 = TestingFramework.Proxies.EM.Interfaces.Groups12_0;
using TestBlocksLib.InformationBlock;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using LogicBlocksLib.ConfigurationBlocks;
using TestBlocksLib.ConfigurationBlocks;
using TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using LogicBlocksLib.APIExtensions;
using TestBlocksLib.APIExtensions;
using LogicBlocksLib.Filters;
using TestBlocksLib.FilterssBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;

namespace TestingFramework.UnitTests.OffendersSvcTests
{
    [TestClass]
    [DeploymentItem("MaskingPrivateData", "MaskingPrivateData")]
    public class MaskingPrivateDataTests : BaseUnitTest
    {
        #region Offender
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Offenders_GetOffender_1Piece_ValidateMasking()
        {
            //var offendersProxy = new Proxies.API.Offenders.OffendersProxy("renanse", "Q1w2e3r4");

            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G4i).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_Beacon").
                    SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                //SetProperty(x => x.OffendersProxyObject).WithValue(null).

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock1").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock2").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").
                    SetProperty(x => x.AddOffenderAddressRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddOffenderAddressRequestBlock2").
                    FromDeepProperty<CreateEntMsgAddOffenderAddressRequestBlock>(x => x.AddOffenderAddressRequest).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID)./*WithValue(2635).*/
               WithValueFromBlockIndex("AddOffenderTestBlock").
                   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
               SetProperty(x => x.ProgramType).WithValue(null).
               SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    //SetProperty(x => x.OffendersProxyObject).WithValue(offendersProxy).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Offenders_GetOffender_E4_ValidateMasking()
        {

            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.RF_Curfew_Dual_E4).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_RF_Curfew_Dual_E4.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_E4").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_E4").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_E4").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_E4").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Transmitter_Triple_Tamper).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Transmitter_Triple_Tamper.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_TX_700").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_TX_700").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_TX_700").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_TX_700").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_TX_700").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.E4_Dual).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_E4").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_E4").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.Transmitter).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_TX_700").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock1").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock2").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").
                    SetProperty(x => x.AddOffenderAddressRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddOffenderAddressRequestBlock2").
                    FromDeepProperty<CreateEntMsgAddOffenderAddressRequestBlock>(x => x.AddOffenderAddressRequest).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID)./*WithValue(2993).*/
                   WithValueFromBlockIndex("AddOffenderTestBlock").
                       FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
               SetProperty(x => x.ProgramType).WithValue(null).
               SetProperty(x => x.IncludeAddressList).WithValue(true).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<ValidateMaskingAssertBlock>().
                SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Offenders_GetOffenderContact_ValidateMasking()
        {

            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

                AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.ContactID).WithValue(null).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

                AddBlock<ValidateMaskingAssertBlock>().
                SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock").
                            FromDeepProperty<GetOffenderContactsListTestBlock>(x => x.GetOffenderContactsListResponse.ContactsList[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Offenders_GetDeletedOffenders_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                    SetDescription("Create Delete Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               AddBlock<CreateEntMsgGetDeletedOffendersRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Create Get Deleted Offenders Request").
               SetProperty(x => x.LastTimestamp).
                   WithValueFromBlockIndex("GetOffendersTestBlock").
                       FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<GetDeletedOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Deleted Offenders Test Block").

               AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetDeletedOffendersTestBlock").
                            FromDeepProperty<GetDeletedOffendersTestBlock>(x => x.GetDeletedOffendersResponse.DeletedOffendersList[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Offenders_GetPictures_ValidateMasking()
        {
            TestingFlow.

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(OffendersSvcTests.PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

              AddBlock<CreateEntMsgGetOffenderPicturesRequestBlock>().
                    SetProperty(x => x.OffenderID)./*WithValue(2635).*/
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

              AddBlock<GetOffenderPicturesTestBlock>().UsePreviousBlockData().

              AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffenderPicturesTestBlock").
                            FromDeepProperty<GetOffenderPicturesTestBlock>(x => x.GetPicturesResponse.PicturesList[0]).

            ExecuteFlow();
        }
        #endregion

        #region Events
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_SendEmail_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendEmail).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[3].ID).
                    SetProperty(x => x.EmailAddress).WithValueFromDifferentDataGenerator(EnumPropertyType.EmailAddress.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.SendEmail).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetHandlingActionByQueryTestBlock").
                            FromDeepProperty<GetHandlingActionByQueryTestBlock>(x => x.GetHandlingActionsByQueryResponse.ActionsList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_PhoneCall_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.PhoneCall).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].ID).
                    SetProperty(x => x.PrefixPhone).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Phone.PrefixPhone).
                    SetProperty(x => x.Phone).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Phone.PhoneNumber).
                            SetProperty(x => x.PrefixPhone).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].Phone.PrefixPhone).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.PhoneCall).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetHandlingActionByQueryTestBlock").
                            FromDeepProperty<GetHandlingActionByQueryTestBlock>(x => x.GetHandlingActionsByQueryResponse.ActionsList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Events_GetHandlingActionByQuery_ActionType_TextMessage_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().
                SetProperty(x => x.UserType).WithValue(Users0.EnmUserType.SystemAdministrator).
                SetProperty(x => x.UserStatus).WithValue(Users0.EnmUserStatus.Active).

                AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                          AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                        FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.SendTextMessage).
                    SetProperty(x => x.ContactType).WithValue(Events0.EnmContactType.SystemAdministrator).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0].ID).
                    SetProperty(x => x.ProviderID).WithValueFromDifferentDataGenerator(EnumPropertyType.PagerProviderID.ToString()).
                    SetProperty(x => x.PagerCode).WithValueFromDifferentDataGenerator(EnumPropertyType.PagerCode.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<CreateEntMsgGetHandlingActionByQueryRequestBlock>().UsePreviousBlockData().
                SetDescription("Create get handling action by query request").
                SetProperty(x => x.ActionTypeList).WithValue(new Events0.EnmHandlingActionType[1]).
                    SetProperty(x => x.ActionTypeList[0]).WithValue(Events0.EnmHandlingActionType.PhoneCall).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetHandlingActionByQueryTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling action by query test").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetHandlingActionByQueryTestBlock").
                            FromDeepProperty<GetHandlingActionByQueryTestBlock>(x => x.GetHandlingActionsByQueryResponse.ActionsList[0]).

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Events_GetOpenPanicCalls_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOpenPanicCallsRequestBlock>().
                    SetDescription("Create get open panic calls request").
                    SetProperty(x => x.MaximumRows).WithValue(-1).
                    SetProperty(x => x.StartRowIndex).WithValue(0).

                AddBlock<GetOpenPanicCallsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get open panic calls test").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOpenPanicCallsTestBlock").
                            FromDeepProperty<GetOpenPanicCallsTestBlock>(x => x.GetOpenPanicCallsResponse.PanicCallDataList[0]).

            ExecuteFlow();
        }
        #endregion

        #region Trail
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Trails_GetLastLocation_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.ProgramType).WithValue(Trails0.EnmLastLocationProgramType.OnePieceRF).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetLastLocationTestBlock").
                            FromDeepProperty<GetLastLocationTestBlock>(x => x.GetLastLocationResponse.LastLocationList[0]).

            ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Trails_GetTrail_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetTrailTestBlock").
                            FromDeepProperty<GetTrailTestBlock>(x => x.GetTrailResponse.Points[0]).

            ExecuteFlow();
        }
        #endregion

        #region Group
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Group_GetGroup_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    //SetProperty(x => x.Name).WithValue("New group").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(Groups0.EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetGroupRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetGroupRequest Block").
                    SetProperty(x => x.GroupID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Groups Data Test Block").

                AddBlock<ValidateMaskingAssertBlock>().UsePreviousBlockData().
               SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetGroupsDataTestBlock").
                            FromDeepProperty<GetGroupsDataTestBlock>(x => x.GetGroupDetailsResponse.GroupDetails[0]).


            ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Information_GetOffenderAdditionalData_ValidateMasking()
        {

            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G4i).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_Beacon").
                    SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock1").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock2").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").
                    SetProperty(x => x.AddOffenderAddressRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddOffenderAddressRequestBlock2").
                    FromDeepProperty<CreateEntMsgAddOffenderAddressRequestBlock>(x => x.AddOffenderAddressRequest).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<GetOffenderAdditionalDataTestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffenderAdditionalDataTestBlock").
                            FromDeepProperty<GetOffenderAdditionalDataTestBlock>(x => x.OffenderAdditionalData).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Queue_GetQueue_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueProgramTrackerTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetQueueProgramTrackerTestBlock").
                            FromDeepProperty<GetQueueProgramTrackerTestBlock>(x => x.QueueProgramTracker[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Log_GetLog_ValidateMasking()
        {
            TestingFlow.

               // AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
               //     SetDescription("Create EntMsgAddEquipmentRequest Type").
               //     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
               //     SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
               //     SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G39).
               //     SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
               //     SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
               //     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               //AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
               //    SetDescription("Add equipment test block").

               //AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
               //    SetDescription("Create EntMsgAddCellularDataRequest Type").
               //    SetProperty(x => x.EquipmentID).
               //        WithValueFromBlockIndex("AddEquipmentTestBlock").
               //            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               //AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
               //    SetDescription("Add Cellular Data test block").

               //AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
               //    SetDescription("Create Add Offender Request").
               //    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
               //    SetProperty(x => x.Receiver).
               //        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
               //            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               //AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
               //    SetDescription("Add Offender test block").

               // AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
               //     SetDescription("Create request to get offenders list").
               //     SetProperty(x => x.OffenderID).
               //        WithValueFromBlockIndex("AddOffenderTestBlock").
               //            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               // AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
               //     SetDescription("Request to get offenders list").

               //AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
               //    SetDescription("Create Send Download Request").
               //    SetProperty(x => x.Immediate).WithValue(true).
               //    SetProperty(x => x.OffenderID).
               //        WithValueFromBlockIndex("AddOffenderTestBlock").
               //            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               //AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
               //    SetDescription("Send Download Request Test Block").

               // AddBlock<CreateEntMessageGetQueueRequestBlock>().
               //     SetDescription("Create Get Queue Request Block").
               //     SetProperty(x => x.StartRowIndex).WithValue(0).
               //     SetProperty(x => x.MaximumRows).WithValue(100).
               //     SetProperty(x => x.filterID).WithValue(46).
               //     SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

               // AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
               //     SetDescription("Get Queue Test Block").

               // AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

               // AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
               //     SetDescription("Run XT Simulator Test Block").
               //     SetProperty(x => x.EquipmentSN).
               //         WithValueFromBlockIndex("GetOffendersTestBlock").
               //             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogProgramTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetLogProgramTestBlock").
                            FromDeepProperty<GetLogProgramTestBlock>(x => x.LogProgram).



            ExecuteFlow();
        }

        #region Users
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Users_GetOfficers_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOfficersListRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").

               AddBlock<GetOfficersListTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOfficersListTestBlock").
                            FromDeepProperty<GetOfficersListTestBlock>(x => x.GetOfficersListResponse.OfficersList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Users_GetUsers_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetUsersListRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").

               AddBlock<GetUsersListTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetUsersListTestBlock").
                            FromDeepProperty<GetUsersListTestBlock>(x => x.EntMsgGetUsersListResponse.UsersList[0]).

            ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void Configuration_GetOffenderConfigurationData_ValidateMasking()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(-2).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffenderConfigurationDataTestBlock").
                            FromDeepProperty<GetOffenderConfigurationDataTestBlock>(x => x.EntConfigurationGPS).

            ExecuteFlow();
        }

        #region APIExtensions
        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetEquipmentListForEquipmentScreen_ValidateMasking()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetEquipmentListForEquipmentScreenLogicBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.MaximumRows).WithValue(10).

                AddBlock<GetEquipmentListForEquipmentScreenTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").
 
                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetEquipmentListForEquipmentScreenTestBlock").
                            FromDeepProperty<GetEquipmentListForEquipmentScreenTestBlock>(x => x.EquipmentListData.EquipmentList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetVictimsAggressorsPairs_ValidateMasking()
        {
            TestingFlow.
                AddBlock<GetVictimsAggressorsPairsTestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.MaximumRows).WithValue(10).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetVictimsAggressorsPairsTestBlock").
                            FromDeepProperty<GetVictimsAggressorsPairsTestBlock>(x => x.GetVictimAggressorPairsResponse.Entities[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetOffenderByFilterID_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetDataRequestBlock>().
                SetDescription("Create GetData Request").
                SetProperty(x => x.ScreenName).
                    WithValue(EnumFiltersScreenNameOptions.offender).

                AddBlock<FiltersSvcGetDataTestBlock>().UsePreviousBlockData().
                    SetDescription("GetData Test Block").

                AddBlock<GetOffenderSystemFilterIDAccordingToFilterName>().UsePreviousBlockData().
                    SetDescription("GetData Test Block").
                    SetProperty(x => x.FilterName).WithValue("All").

                AddBlock<GetOffendersByFilterIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders with Enrollment Recommended Test Block").
                    SetProperty(x => x.FilterID).
                        WithValueFromBlockIndex("GetOffenderSystemFilterIDAccordingToFilterName").
                            FromDeepProperty<GetOffenderSystemFilterIDAccordingToFilterName>(x => x.FilterID).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffendersByFilterIDTestBlock").
                            FromDeepProperty<GetOffendersByFilterIDTestBlock>(x => x.GetOffendersByFilterIDRequest.response.Entities[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetEventAdditionalData_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(50).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender-related event").

                AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.QuickHandle).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetVictimsAggressorsPairsTestBlock").
                            FromDeepProperty<GetVictimsAggressorsPairsTestBlock>(x => x.GetVictimAggressorPairsResponse.Entities[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetLatestOffenderEvent_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetName("GetEventsRequestBlock_BeforeHandling").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(50).
                    //SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.Severity).WithValue(Events0.EnmEventSeverity.Violation).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender-related event").

                AddBlock<GetLatestOffenderEventsTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventLogID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetLastOffenderEventsTestBlock").
                            FromDeepProperty<GetLatestOffenderEventsTestBlock>(x => x.LastOffenderEvents[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetOffenderSummary_ValidateMasking()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active,
                    Offenders0.EnmProgramStatus.Active}).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<GetOffenderSummaryTestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffenderSummaryTestBlock").
                            FromDeepProperty<GetOffenderSummaryTestBlock>(x => x.OffenderSummaryData.Offender).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetContactList_ValidateMasking()
        {
            TestingFlow.

                AddBlock<GetContactListTestBlock>().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.ContactType).WithValue(EnmContactType.SystemAdministrator).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetContactListTestBlock").
                            FromDeepProperty<GetContactListTestBlock>(x => x.ContactList[0]).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetOffenderByID_ValidateMasking()
        {
            //var offendersProxy = new Proxies.API.Offenders.OffendersProxy("renanse", "Q1w2e3r4");

            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.One_Piece_GPS_RF_G4i).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_Beacon").
                    SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
                //SetProperty(x => x.OffendersProxyObject).WithValue(null).

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock1").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

                AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetName("CreateEntMsgAddOffenderAddressRequestBlock2").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                            SetProperty(x => x.amountOfPhonesToGenerate).WithValue(2).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").
                    SetProperty(x => x.AddOffenderAddressRequest).
                    WithValueFromBlockIndex("CreateEntMsgAddOffenderAddressRequestBlock2").
                    FromDeepProperty<CreateEntMsgAddOffenderAddressRequestBlock>(x => x.AddOffenderAddressRequest).

                AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<GetOffenderByIdTestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID)./*WithValue(2635).*/
               WithValueFromBlockIndex("AddOffenderTestBlock").
                   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetRelatedEvents_ValidateMasking()
        {

            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                 AddBlock<CreateEntMsgAddHandlingActionRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add handling action request").
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                    SetProperty(x => x.Type).WithValue(Events0.EnmHandlingActionType.ScheduleTask).
                    SetProperty(x => x.ScheduledTaskType).WithValueFromDifferentDataGenerator(EnumPropertyType.ScheduleTaskType.ToString()).
                    SetProperty(x => x.ScheduledTaskTime).WithValueFromDifferentDataGenerator(EnumPropertyType.ScheduleTaskTime.ToString()).
                    SetProperty(x => x.ReminderTime).WithValueFromDifferentDataGenerator(EnumPropertyType.ReminderTime.ToString()).

                AddBlock<AddHandlingActionTestBlock>().UsePreviousBlockData().
                    SetDescription("Add handling action test").

                AddBlock<GetRelatedEventsTestBlock>().
                SetProperty(x => x.EventLogID).
                        WithValueFromBlockIndex("GetEventsTestBlock").
                            FromDeepProperty<GetEventsTestBlock>(x => x.GetEventsResponse.EventsList[0].ID).
                            SetProperty(x => x.RowCount).WithValue(1).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetRelatedEventsTestBlock").
                            FromDeepProperty<GetRelatedEventsTestBlock>(x => x.GetRelatedEventsResponse[0]).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetOffenderEventAdditionalData_ValidateMasking()
        {

            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-1)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.Limit).WithValue(2).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender-related event").

                AddBlock<GetOffenderEventAdditionalDataTestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.OffenderID).
                    SetProperty(x => x.EventID).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.ID).
                    SetProperty(x => x.EventTime).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.Time).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetOffenderEventAdditionalDataTestBlock").
                            FromDeepProperty<GetOffenderEventAdditionalDataTestBlock>(x => x.EventAdditionalData).

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.MaskingPrivateData)]
        [TestMethod]
        public void APIExtensions_GetEventByFilterID_ValidateMasking()
        {

            TestingFlow.

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    //SetProperty(x => x.Limit).WithValue(100).
                    //SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<GetEventWithOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender-related event").

                AddBlock<GetEventsByFilterIdTestingBlock>().
                    SetDescription("GetEventsByFilterID like first time user open monitor screen").
                    SetProperty(x => x.StartRowIndex).WithValue(1).
                    SetProperty(x => x.MaximumRows).WithValue(30).
                    SetProperty(x => x.FilterID).WithValue(1).
                    SetProperty(x => x.ScreenName).WithValue("Monitor").
                    SetProperty(x => x.PreviousTimeStamp).WithValue(new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 }).
                    SetProperty(x => x.PageUpperLimitEventId).WithValue(0).
                    SetProperty(x => x.FromTime).WithValue(DateTime.UtcNow.AddDays(-1)).
                    SetProperty(x => x.ToTime).WithValue(DateTime.UtcNow).
                    SetProperty(x => x.ForceRefresh).WithValue(false).
                    SetProperty(x => x.OffenderId).
                        WithValueFromBlockIndex("GetEventWithOffenderTestBlock").
                            FromDeepProperty<GetEventWithOffenderTestBlock>(x => x.EventWithOffender.OffenderID).

                AddBlock<ValidateMaskingAssertBlock>().
                    SetProperty(x => x.Object).
                        WithValueFromBlockIndex("GetEventsByFilterIdTestingBlock").
                            FromDeepProperty<GetEventsByFilterIdTestingBlock>(x => x.EventDataResult.EventsList[0]).

               ExecuteFlow();
        }

        #endregion






    }
}
