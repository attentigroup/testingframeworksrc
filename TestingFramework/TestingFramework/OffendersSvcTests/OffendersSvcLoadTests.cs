﻿using AssertBlocksLib.OffendersAsserts;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Threading;
using TestBlocksLib.InformationBlock;
using TestBlocksLib.OffendersBlocks;
using TestingFramework.Proxies.API.Offenders;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

namespace TestingFramework.UnitTests.OffendersSvcTests
{
    [TestClass]
    [DeploymentItem("ConfigurationFiles", "ConfigurationFiles")]
    public class OffendersSvcLoadTests : BaseUnitTest
    {
        public const string OffendersSvc_CustomFields_CSV_File = @"OffendersSvc_CustomFields.csv";
        public const string OffenderID_COL_NAME = "OffenderID";

        public static double KeepConnectionSeconds { get; set; }

        public static int WaitTimeBetweenRetriesMiliseconds = 20000;

        [ClassInitialize]
        public static void OffendersSvcLoadTests_InitForLoadTest(TestContext testContext)
        {
            GenerateOffendersIdCsvFile(OffendersSvc_CustomFields_CSV_File);
        }

        [ClassCleanup]
        public static void OffendersSvcLoadTests_CleanupForLoadTest()
        {
            var dateTimeToStop = DateTime.Now.AddSeconds(KeepConnectionSeconds);
            Log.Info($"Going to keep the connection alive for {KeepConnectionSeconds} seconds. will stop at {dateTimeToStop}.");
            while (dateTimeToStop > DateTime.Now)
            {
                Log.Info($"Start call method to keep connection alive.");
                try
                {
                    Proxies.API.APIExtensions.APIExtensionsProxy.Instance.GetCommunicationServicesStatus();
                }
                catch (Exception exception)
                {
                    Log.Error($"while keep connection alive an exception was throwen: {exception.Message}.");
                }
                Log.Info($"Finish call method to keep connection alive.going to sleep for {WaitTimeBetweenRetriesMiliseconds} miliseconds");
                Thread.Sleep(WaitTimeBetweenRetriesMiliseconds);
            }
            Log.Info($"Finish wait for {KeepConnectionSeconds} seconds to keep the connection alive.");
        }

        /// <summary>
        /// method generate file from offender in DB.
        /// </summary>
        /// <param name="filePath"></param>
        public static void GenerateOffendersIdCsvFile(string filePath)
        {
            try
            {
                Log.Info($"going to retrive offenders for load test and save to file {filePath}.");
                var GetOffendersListRequest =
                        new Offenders0.EntMsgGetOffendersListRequest() { ProgramStatus = new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active } };
                var GetOffendersResponse = OffendersProxy.Instance.GetOffenders(GetOffendersListRequest);
                var offendersCount = GetOffendersResponse.OffendersList == null ? 0 : GetOffendersResponse.OffendersList.Length;
                Log.Info($"Retrived {offendersCount} offenders");
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

                var sbData = new System.Text.StringBuilder();
                sbData.AppendLine(OffenderID_COL_NAME);

                foreach (var offender in GetOffendersResponse.OffendersList)
                {
                    var newline = $"{offender.ID}";
                    sbData.AppendLine(newline);
                    Log.Debug($"Add offender id {offender.ID} ref id {offender.RefID} to the file");
                }

                System.IO.File.WriteAllText(filePath, sbData.ToString());
                Log.Info($"Finish write {GetOffendersResponse.OffendersList.Length} offenders to file {filePath}");
            }
            catch (System.Exception exception)
            {
                Log.ErrorFormat($"GenerateOffendersIdCsvFile for path {filePath} end with error:{exception.Message}");
                Log.DebugFormat($"GenerateOffendersIdCsvFile for path {filePath} end with error:{exception}");
            }

        }

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + OffendersSvc_CustomFields_CSV_File,
            "OffendersSvc_CustomFields#csv", 
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.Offenders_Service_Load)]
        public void OffendersSvc_CustomFields_GetFromFile()
        {
            DataRow currentDataRow = TestContext.DataRow;

            var offenderID = long.Parse(currentDataRow[OffenderID_COL_NAME].ToString());
            Log.Info($"Start test for offender id {offenderID}");

            TestingFlow.
                AddBlock<GetCustomFieldsTestBlock>().
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get offender").
                    SetProperty(x => x.OffenderID).WithValue(offenderID).
               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                    BeginTransaction("GetOffenders_With_CustomFields").
                    EndTransaction("GetOffenders_With_CustomFields").
                AddBlock<CustomFieldsVisibleAssertBlock>().
                ExecuteFlow();

            Log.Info($"Finish test for offender id {offenderID}");
            KeepConnectionSeconds = 70;
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Offenders_Service_Load)]
        public void OffendersSvc_CustomFields_Get()
        {
            TestingFlow.
                AddBlock<GetCustomFieldsTestBlock>().
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Requestto get active offender").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.Limit).WithValue(1).
               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<CustomFieldsVisibleAssertBlock>().
                ExecuteFlow();
        }

        [TestMethod]
        [TestCategory(CategoriesNames.Offenders_Service_Load)]
        public void OffendersSvc_CustomFields_Update()
        {
            TestingFlow.
                AddBlock<GetCustomFieldsTestBlock>().
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Requestto get active offender").
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.Limit).WithValue(1).
               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.CityID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].CityID).
                    SetProperty(x => x.DateOfBirth).WithValue(DateTime.Now.AddDays(-42)).
                    SetProperty(x => x.FirstName).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].FirstName).
                    SetProperty(x => x.LastName).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].LastName).
                    SetProperty(x => x.MiddleName).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].MiddleName).
                    SetProperty(x => x.Gender).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Gender).
                    SetProperty(x => x.Language).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Language).
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.OfficerID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramEnd).
                    SetProperty(x => x.ProgramStart).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramStart).
                    SetProperty(x => x.ProgramType).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                    SetProperty(x => x.StateID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].StateID).
                    SetProperty(x => x.Street).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].Street).
                    SetProperty(x => x.ContactPhoneNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.CellData.VoicePhoneNumber).
                    SetProperty(x => x.ContactPrefixPhone).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.CellData.VoicePrefixPhone).
                    SetProperty(x => x.CourtOrderNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].CourtOrderNumber).
                    SetProperty(x => x.Title).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Title).
                    SetProperty(x => x.Timestamp).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Transmitter).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.TransmitterSN).
                    SetProperty(x => x.Receiver).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.RelatedOffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RelatedOffenderID).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                ExecuteFlow();
        }

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + OffendersSvc_CustomFields_CSV_File,
            "OffendersSvc_CustomFields#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.Offenders_Service_Load)]
        public void OffendersSvc_CustomFields_Update_FromFile()
        {
            DataRow     currentDataRow = TestContext.DataRow;
            var         offenderID = long.Parse(currentDataRow[OffenderID_COL_NAME].ToString());

            const int MaximumProgramLength = 729;

            Log.Info($"Start test for offender id {offenderID}");

            TestingFlow.
                AddBlock<GetCustomFieldsTestBlock>().
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request to get specific offender").
                    SetProperty(x => x.OffenderID).WithValue(offenderID).
               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.CityID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].CityID).
                    SetProperty(x => x.DateOfBirth).WithValue(DateTime.Now.AddDays(-42)).
                    SetProperty(x => x.FirstName).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].FirstName).
                    SetProperty(x => x.LastName).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].LastName).
                    SetProperty(x => x.MiddleName).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].MiddleName).
                    SetProperty(x => x.Gender).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Gender).
                    SetProperty(x => x.Language).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Language).
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.OfficerID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramEnd).
                    SetProperty(x => x.ProgramStart).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramStart).
                    SetProperty(x => x.ProgramType).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                    SetProperty(x => x.StateID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].StateID).
                    SetProperty(x => x.Street).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.Addresses[0].Street).
                    SetProperty(x => x.ContactPhoneNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.CellData.VoicePhoneNumber).
                    SetProperty(x => x.ContactPrefixPhone).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ContactInfo.CellData.VoicePrefixPhone).
                    SetProperty(x => x.CourtOrderNumber).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].CourtOrderNumber).
                    SetProperty(x => x.Title).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Title).
                    SetProperty(x => x.Timestamp).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.Transmitter).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.TransmitterSN).
                    SetProperty(x => x.Receiver).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    SetProperty(x => x.RelatedOffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RelatedOffenderID).

                AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                ExecuteFlow();
        }
    }
}
