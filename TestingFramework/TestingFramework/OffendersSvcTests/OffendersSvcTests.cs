﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Categories;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.DevicesBlocks;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

#region API Equipment refs
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Equipment1 = TestingFramework.Proxies.EM.Interfaces.Equipment3_10;
using Equipment2 = TestingFramework.Proxies.EM.Interfaces.Equipment3_9;
#endregion

#region API ProgramActions refs
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using System.IO;
using System.Reflection;
#endregion

namespace TestingFramework.UnitTests.OffendersSvcTests
{
    [TestClass]
    public class OffendersSvcTests : BaseUnitTest
    {
        //Path to picture 
        public static string PicPath { get; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"OffendersSvcTests\AddOffenderPicture.jpg");

        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddGetOffender_Success()
        {            
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddGetOffender_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock_1>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddGetOffender_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).
               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<AddOffenderRequestAssertBlock_2>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetDeletedOffenders_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                    SetDescription("Create Delete Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               AddBlock<CreateEntMsgGetDeletedOffendersRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Deleted Offenders Request").
               //SetProperty(x => x.LastTimestamp).
               //    WithValueFromBlockIndex("GetOffendersTestBlock").
               //        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<GetDeletedOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Deleted Offenders Test Block").

               AddBlock<GetDeletedOffendersAssertBlock>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetDeletedOffenders_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).
               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgDeleteOffenderRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Delete Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                            FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.ResponseTimeStamp).

               AddBlock<DeleteOffenderTestBlock_1>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block_1").

               AddBlock<CreateEntMsgGetDeletedOffendersRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Get Deleted Offenders Request").
               //SetProperty(x => x.LastTimestamp).
               //    WithValueFromBlockIndex("GetOffendersTestBlock").
               //        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<GetDeletedOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Deleted Offenders Test Block").

               AddBlock<GetDeletedOffendersAssertBlock_1>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetDeletedOffenders_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgDeleteOffenderRequestBlock_2>().
                    SetDescription("Create Delete Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Timestamp).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                            FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.ResponseTimeStamp).

               AddBlock<DeleteOffenderTestBlock_2>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block_2").

               AddBlock<CreateEntMsgGetDeletedOffendersRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Get Deleted Offenders Request").
               //SetProperty(x => x.LastTimestamp).
               //    WithValueFromBlockIndex("GetOffendersTestBlock").
               //        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<GetDeletedOffendersTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Deleted Offenders Test Block").

               AddBlock<GetDeletedOffendersAssertBlock_2>().UsePreviousBlockData().

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffender_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

               AddBlock<DeleteOffenderTestBlock>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffender_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock_1>().UsePreviousBlockData().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.ResponseTimeStamp).

               AddBlock<DeleteOffenderTestBlock_1>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffender_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 
               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

                AddBlock<CreateEntMsgDeleteOffenderRequestBlock_2>().
                     SetDescription("Create Delete Offender Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock_2").
                             FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                     SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock_2").
                             FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.ResponseTimeStamp).

               AddBlock<DeleteOffenderTestBlock_2>().UsePreviousBlockData().
                     SetDescription("Delete Offender Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffender_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).

                    AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffender_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock_1").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock_1>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock_1").
                            FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.ResponseTimeStamp).

                    AddBlock<UpdateOffenderTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffender_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Update Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.OfficerID).
                        WithValueFromBlockIndex("CreateEntMsgAddOffenderRequestBlock_2").
                            FromDeepProperty<CreateEntMsgAddOffenderRequestBlock_2>(x => x.OfficerID).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Title).WithRandomGenerateValue().
                    SetProperty(x => x.Timestamp).//
                       WithValueFromBlockIndex("GetOffendersTestBlock_2").
                            FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.ResponseTimeStamp).

                    AddBlock<UpdateOffenderTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Update Offender Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddOffenderAddress_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddOffenderAddress_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddOffenderAddress_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffenderAddress_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Address test block").

               AddBlock<CreateEntMsgDeleteOffenderAddressRequestBlock>().
                    SetDescription("Create Delete Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock").
                            FromDeepProperty<AddOffenderAddressTestBlock>(x => x.AddOffenderAddressResponse.NewAddressID).

               AddBlock<DeleteOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffenderAddress_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").

                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Address test block").

               AddBlock<CreateEntMsgDeleteOffenderAddressRequestBlock_1>().
                    SetDescription("Create Delete Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock_1").
                            FromDeepProperty<AddOffenderAddressTestBlock_1>(x => x.AddOffenderAddressResponse.NewAddressID).

               AddBlock<DeleteOffenderAddressTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Delete Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffenderAddress_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").

                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Address test block").

               AddBlock<CreateEntMsgDeleteOffenderAddressRequestBlock_2>().
                    SetDescription("Create Delete Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock_2").
                            FromDeepProperty<AddOffenderAddressTestBlock_2>(x => x.AddOffenderAddressResponse.NewAddressID).

               AddBlock<DeleteOffenderAddressTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Delete Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffenderAddress_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
               
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 
                   

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").

                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

               AddBlock<CreateEntMsgUpdateOffenderAddressRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create update Offender Address Request").
                    SetProperty(x => x.Description).WithValue("Update Address Test").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock").
                            FromDeepProperty<AddOffenderAddressTestBlock>(x => x.AddOffenderAddressResponse.NewAddressID).
                   
                    SetProperty(x => x.Street).WithRandomGenerateValue().
                    SetProperty(x => x.Phones).WithValue(null).

               AddBlock<UpdateOffenderAddressTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffenderAddress_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).       

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

               AddBlock<CreateEntMsgUpdateOffenderAddressRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create update Offender Address Request").
                    SetProperty(x => x.Description).WithValue("Update Address Test").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock_1").
                            FromDeepProperty<AddOffenderAddressTestBlock_1>(x => x.AddOffenderAddressResponse.NewAddressID).
                   
                    SetProperty(x => x.Street).WithRandomGenerateValue().

               AddBlock<UpdateOffenderAddressTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffenderAddress_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF). 
                    

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderAddressRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.Description).WithValue("New Address Test").

                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderAddressTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender Address test block").

               AddBlock<CreateEntMsgUpdateOffenderAddressRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create update Offender Address Request").
                    SetProperty(x => x.Description).WithValue("Update Address Test").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.AddressID).
                        WithValueFromBlockIndex("AddOffenderAddressTestBlock_2").
                            FromDeepProperty<AddOffenderAddressTestBlock_2>(x => x.AddOffenderAddressResponse.NewAddressID).
                 
                    SetProperty(x => x.Street).WithRandomGenerateValue().

               AddBlock<UpdateOffenderAddressTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddDeleteOffenderContact_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 
             
               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgDeleteOffenderContactRequestBlock>().
                    SetDescription("Create delete Offender contact Request").
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<DeleteOffenderContactRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddDeleteOffenderContact_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock_1>().
                    SetDescription("Create Get Offender Contact list request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderContactsListTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offender Contact list test block").

               AddBlock<CreateEntMsgDeleteOffenderContactRequestBlock_1>().
                    SetDescription("Create delete Offender contact Request").
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock_1").
                            FromDeepProperty<GetOffenderContactsListTestBlock_1>(x => x.GetOffenderContactsListResponse.ContactsList[0].ContactID).

               AddBlock<DeleteOffenderContactRequestTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
 virtual         public void OffendersSvcTests_AddDeleteOffenderContact_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Address Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock_2>().
                    SetDescription("Create Get Offender Contact list request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderContactsListTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offender Contact list test block").

               AddBlock<CreateEntMsgDeleteOffenderContactRequestBlock_2>().
                    SetDescription("Create delete Offender contact Request").
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock_2").
                            FromDeepProperty<GetOffenderContactsListTestBlock_2>(x => x.GetOffenderContactsListResponse.ContactsList[0].ContactID).

               AddBlock<DeleteOffenderContactRequestTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Offender Address test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffenderContact_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgUpdateOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<UpdateOffenderContactTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender contact test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffenderContact_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).
               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock_1>().
                    SetDescription("Create Get Offender Contact list request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderContactsListTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offender Contact list test block").

               AddBlock<CreateEntMsgUpdateOffenderContactRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock_1").
                            FromDeepProperty<GetOffenderContactsListTestBlock_1>(x => x.GetOffenderContactsListResponse.ContactsList[0].ContactID).

               AddBlock<UpdateOffenderContactTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Offender contact test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffenderContact_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock_2>().
                    SetDescription("Create Get Offender Contact list request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderContactsListTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offender Contact list test block").

               AddBlock<CreateEntMsgUpdateOffenderContactRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create update Offender contact Request").

                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("GetOffenderContactsListTestBlock_2").
                            FromDeepProperty<GetOffenderContactsListTestBlock_2>(x => x.GetOffenderContactsListResponse.ContactsList[0].ContactID).

               AddBlock<UpdateOffenderContactTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Offender contact test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderContactList_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ContactID).
                        WithValueFromBlockIndex("AddOffenderContactTestBlock").
                            FromDeepProperty<AddOffenderContactTestBlock>(x => x.AddOffenderContactResponse.NewContactID).

               AddBlock<GetOffenderContactsListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

               AddBlock<GetOffenderContactsListAssertBlock>().
                    SetDescription("Get Offender contact list assert block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderContactList_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock_1>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderContactsListTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

               AddBlock<GetOffenderContactsListAssertBlock_1>().
                    SetDescription("Get Offender contact list assert block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderContactList_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderContactRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderContactTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender Contact test block").

               AddBlock<CreateEntMsgGetOffenderContactsListRequestBlock_2>().
                    SetDescription("Create update Offender contact Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderContactsListTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offender contact list test block").

               AddBlock<GetOffenderContactsListAssertBlock_2>().
                    SetDescription("Get Offender contact list assert block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddOffenderPicture_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update Offender Request").
                    SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].Timestamp).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock").
                             FromDeepProperty<AddOffenderPictureTestBlock>(x => x.AddOffenderPictureResponse.NewPhotoID).
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<UpdateOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offender test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddOffenderPicture_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF). 

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock_1>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create update Offender Request").
                    SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.ResponseTimeStamp).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock_1").
                             FromDeepProperty<AddOffenderPictureTestBlock_1>(x => x.AddOffenderPictureResponse.NewPhotoID).
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock_1").
                             FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<UpdateOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Offender test block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_AddOffenderPicture_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock_2>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<CreateEntMsgUpdateOffenderRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create update Offender Request").
                    SetProperty(x => x.Timestamp).
                         WithValueFromBlockIndex("GetOffendersTestBlock_2").
                             FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.ResponseTimeStamp).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock_2").
                             FromDeepProperty<AddOffenderPictureTestBlock_2>(x => x.AddOffenderPictureResponse.NewPhotoID).
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock_2").
                             FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<UpdateOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Update Offender test block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffenderPicture_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgDeleteOffenderPictureRequestBlock>().
                    SetDescription("Create Delete Offender picture Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock").
                             FromDeepProperty<AddOffenderPictureTestBlock>(x => x.AddOffenderPictureResponse.NewPhotoID).

               AddBlock<DeleteOffenderPictureTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Offender picture Test Block").


               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffenderPicture_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock_1>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgDeleteOffenderPictureRequestBlock_1>().
                    SetDescription("Create Delete Offender picture Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock_1").
                             FromDeepProperty<AddOffenderPictureTestBlock_1>(x => x.AddOffenderPictureResponse.NewPhotoID).

               AddBlock<DeleteOffenderPictureTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Delete Offender picture Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_DeleteOffenderPicture_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).
               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock_2>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgDeleteOffenderPictureRequestBlock_2>().
                    SetDescription("Create Delete Offender picture Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.PhotoID).
                         WithValueFromBlockIndex("AddOffenderPictureTestBlock_2").
                             FromDeepProperty<AddOffenderPictureTestBlock_2>(x => x.AddOffenderPictureResponse.NewPhotoID).

               AddBlock<DeleteOffenderPictureTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Delete Offender picture Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderPictureIDList_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffenderPictureIDListRequestBlock>().
                    SetDescription("Create get Offender picture ID list Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderPictureIDListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender picture ID list Test Block").

               AddBlock<GetOffenderPictureIDListAssertBlock>().
                    SetDescription("Get Offender picture ID list Assert Block").


               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderPictureIDList_1_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders1.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock_1>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffenderPictureIDListRequestBlock_1>().
                    SetDescription("Create get Offender picture ID list Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderPictureIDListTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offender picture ID list Test Block").

               AddBlock<GetOffenderPictureIDListAssertBlock_1>().
                    SetDescription("Get Offender picture ID list Assert Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderPictureIDList_2_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders2.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgAddOffenderPictureRequestBlock_2>().
                    SetDescription("Create Add Offender picture Request").
                    SetProperty(x => x.PhotoPath).WithValue(PicPath).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<AddOffenderPictureTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender picture test block").

               AddBlock<CreateEntMsgGetOffenderPictureIDListRequestBlock_2>().
                    SetDescription("Create get Offender picture ID list Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderPictureIDListTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Offender picture ID list Test Block").

               AddBlock<GetOffenderPictureIDListAssertBlock_2>().
                    SetDescription("Get Offender picture ID list Assert Block").

               ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        [DeploymentItem("DevicesBlocks\\xt_simulator", "DevicesBlocks\\xt_simulator")]
        [DeploymentItem("ConfigurationFiles", "ConfigurationFiles")]
        public void OffendersSvcTests_ReallocateActiveOffender_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34286133").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").

                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

               AddBlock<CreateEntMsgReallocateActiveOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Reallocate Active Offender Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Reason).WithValue("4").

               AddBlock<ReallocateActiveOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Create Reallocate Active Offender block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<ReallocateActiveOffenderAssertBlock>().
                    SetDescription("Get Offender picture ID list Assert Block").

               AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER").
                    //SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Offenders_Service)]
        [TestMethod]
        public void OffendersSvcTests_GetOffenderCurrentStatus_Success()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgGetOffenderCurrentStatusRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffenderCurrentStatusTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<GetOffenderCurrentStatusAssertBlock>().
                    SetDescription("Get Offender Current Status Assert Block").

               ExecuteFlow();
        }
    }
}
