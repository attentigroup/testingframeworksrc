﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.ConfigurationBlocks;
using TestBlocksLib.ConfigurationBlocks;
using AssertBlocksLib.ConfigurationAsserts;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;

using Common.Categories;


#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;

#endregion

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.OffendersAsserts;
using Common.Enum;
#endregion

namespace TestingFramework.UnitTests.ConfigurationRegression
{
    [TestClass]
    public class ConfigurationRegression : BaseUnitTest
    {
        public static int CURRENT_VERSION_NUMBER = -1;
        public static int PLANNED_VERSION_NUMBER = -2;
        Offenders0.EnmProgramStatus[] status = { Offenders0.EnmProgramStatus.Active };
        Offenders0.EnmProgramType[] oneP_program = { Offenders0.EnmProgramType.One_Piece };
        Offenders0.EnmProgramType[] twoP_program = { Offenders0.EnmProgramType.Two_Piece };

        public int OffenderId4Tests { get; set; }
        public int OffenderId4RFTests { get; set; }
        public int RuleID4onePieceTests { get; set; }

        public int RuleID4TwoPieceTests { get; set; }
        public ConfigurationRegression()
        {
         
            RuleID4onePieceTests = 100460;
            RuleID4TwoPieceTests = 100420;

        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetGPSRuleConfigurationList_OnePieceGPSRF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    SetProperty(x => x.ProgramType).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                    //WithValue(OffenderId4Tests).
                    SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
 
                AddBlock<GetGPSRuleConfigurationListAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetGPSRuleConfigurationList_TwoPiece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    SetProperty(x => x.ProgramType).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                    SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
              
                AddBlock<GetGPSRuleConfigurationListAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetOffenderConfigurationData_OnePieceGPSRF()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                AddBlock<GetOffenderConfigurationDataRequestAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }
        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetOffenderConfigurationData_TwoPiece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

            AddBlock<GetOffenderConfigurationDataRequestAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetOffenderConfigurationData_E4()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

            AddBlock<GetRFOffenderConfigurationDataResponseAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetOffenderConfigurationData_Alcohol()
        {
            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.VBR_Cellular}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

            AddBlock<GetRFOffenderConfigurationDataResponseAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }
        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetHandlingProcedure_OnePieceGPSRF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_BeforeUpdate").

                AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update handling procedure request").
                    SetProperty(x => x.AutoEmailNotificationToAgency).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<UpdateHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Update handling procedure test").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_AfterUpdate").

                AddBlock<UpdateHandlingProcedureAssertBlock>().
                    SetDescription("").


            ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetHandlingProcedure_TwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_BeforeUpdate").

                AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update handling procedure request").
                    SetProperty(x => x.AutoEmailNotificationToAgency).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<UpdateHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Update handling procedure test").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_AfterUpdate").

                AddBlock<UpdateHandlingProcedureAssertBlock>().
                    SetDescription("").


            ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_GetHandlingProcedure_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual}).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_BeforeUpdate").

                AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update handling procedure request").
                    SetProperty(x => x.AutoEmailNotificationToAgency).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<UpdateHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Update handling procedure test").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_AfterUpdate").

                AddBlock<UpdateHandlingProcedureAssertBlock>().
                    SetDescription("").


            ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_ResetGPSRuleConfiguration_OnePieceGPSRF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).WithValue(OffenderId4Tests).
                    SetProperty(x => x.ProgramType).WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                            SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update GPS Rule Configuration").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.RuleID).WithValue(RuleID4onePieceTests).
                    SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).//WithValue(1200).


                AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock>().UsePreviousBlockData().
                      SetDescription("Update GPS Rule Configuration test block").

                 AddBlock<CreateEntMsgResetGPSRuleConfigurationRequest>().UsePreviousBlockData().
                    SetDescription("Create Reset GPS Rule Configuration").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<ResetGPSRuleConfigurationRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Reset GPS Rule Configuration Request").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                      AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                         SetDescription("Get Configuration GPS Rule for offender").

                           AddBlock<GetGPSRuleConfigurationListIsDefaultAssertBlock>().
                           SetDescription("Assert for Get GPS Rule Configuration List Is Default Assert Block>").
                      ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_ResetGPSRuleConfiguration_TwoPiece()
        {
            TestingFlow.

            AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

            AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).WithValue(OffenderId4Tests).
                    SetProperty(x => x.ProgramType).WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                    SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest>().UsePreviousBlockData().
                    SetDescription("Create Update GPS Rule Configuration").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.RuleID).WithValue(RuleID4TwoPieceTests).
                    SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).//WithValue(1200).

                 AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock>().UsePreviousBlockData().
                      SetDescription("Update GPS Rule Configuration test block").


                 AddBlock<CreateEntMsgResetGPSRuleConfigurationRequest>().UsePreviousBlockData().
                    SetDescription("Create Reset GPS Rule Configuration").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<ResetGPSRuleConfigurationRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Reset GPS Rule Configuration Request").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<GetGPSRuleConfigurationListIsDefaultAssertBlock>().UsePreviousBlockData().
                    SetDescription("Assert for Get GPS Rule Configuration List Is Default Assert Block>"). 

            ExecuteFlow();

        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateRFOffenderConfigurationData_E4()
        {
            TestingFlow.

            AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                   SetDescription("Create request to get offenders list").
                   SetProperty(x => x.Limit).WithValue(1).
                   SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                   SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

           AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                   SetDescription("Request to get offenders list").

              AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Get offender Configuration Reuest").
                   SetProperty(x => x.OffenderID).
                   WithValueFromBlockIndex("GetOffendersTestBlock").
                   FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                   SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

               AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Get Configuration for offender").
                   SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                   AddBlock<CreateEntMsgUpdateRFOffenderConfigurationDataRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                      SetDescription("Create Update RF Offender Configuration Data Request").

                      SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                      SetProperty(x => x.ProgramType).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                         SetProperty(x => x.SupportGPRS).WithValue(true).
                         SetProperty(x => x.GPRSPriority).WithValue(1).
                         SetProperty(x => x.SupportSMSCallback).WithValue(true).
                         SetProperty(x => x.SupportCSD).WithValue(false).
                         SetProperty(x => x.SupportLandline).WithValue(true).
                         SetProperty(x => x.LandlinePriority).WithValue(2).
                         SetProperty(x => x.GraceAfterVoiceTestEnd).WithValueFromDifferentDataGenerator(EnumPropertyType.GraceTime.ToString()).
                         SetProperty(x => x.GraceBeforeVoiceTestStart).WithValueFromDifferentDataGenerator(EnumPropertyType.GraceTime.ToString()).

                   AddBlock<UpdateRFOffenderConfigurationDataRequestTestsBlock>().UsePreviousBlockData().
                       SetDescription("Update RF Offender Configuration DataRequest TestsBlock").

                   AddBlock<CreateEntMsgGetOffenderConfigurationDataRequest>().UsePreviousBlockData().
                    SetDescription("Create EntMsg Get Offender Configuration Data Request").
                      SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                          FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                       SetProperty(x => x.Version).WithValue(PLANNED_VERSION_NUMBER).

                   AddBlock<GetOffenderConfigurationDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender Configuration Data Request TestBlock").

                   AddBlock<GetRFOffenderConfigurationDataRequestAssertBlock>().
                      SetDescription("Get RF Offender Configuration Data Request AssertBlock").

                   ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateRFOffenderConfigurationData_Alcohol()
        {
            TestingFlow.

             AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.PreActive }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.VBR_Cellular }).

            AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

            AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

            AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

            AddBlock<CreateEntMsgUpdateRFOffenderConfigurationDataRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                       SetDescription("Create Update RF Offender Configuration Data Request").

                       SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                       SetProperty(x => x.ProgramType).
                          WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                       SetProperty(x => x.SupportGPRS).WithValue(true).
                       SetProperty(x => x.GPRSPriority).WithValue(0).
                       SetProperty(x => x.SupportSMSCallback).WithValue(true).
                       SetProperty(x => x.SupportCSD).WithValue(false).
                       SetProperty(x => x.MinimumTimeBetweenAlcoholTests).WithValueFromDifferentDataGenerator(EnumPropertyType.TimeBetweenBaseUnitUploads.ToString()).

            AddBlock<UpdateRFOffenderConfigurationDataRequestTestsBlock>().UsePreviousBlockData().
                       SetDescription("Update RF Offender Configuration DataRequest TestsBlock").

                   AddBlock<CreateEntMsgGetOffenderConfigurationDataRequest>().UsePreviousBlockData().
                    SetDescription("Create EntMsg Get Offender Configuration Data Request").
                      SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                          FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                       SetProperty(x => x.Version).WithValue(PLANNED_VERSION_NUMBER).

                   AddBlock<GetOffenderConfigurationDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offender Configuration Data Request TestBlock").

                   AddBlock<GetVBROffenderConfigurationDataRequestAssertBlock>().
                      SetDescription("Get RF Offender Configuration Data Request AssertBlock").

                    ExecuteFlow();

        }
        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateGPSRuleConfiguration_OnePieceGPSRF()
        {

            TestingFlow.
            AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

            AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                     SetProperty(x => x.ProgramType).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
              
                    SetDescription("Create Update GPS Rule Configuration").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.RuleID).WithValue(RuleID4onePieceTests).
                    SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).//WithValue(1200).

                AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock>().UsePreviousBlockData().
                      SetDescription("Update GPS Rule Configuration test block").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).
  
                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

            AddBlock<UpdateGPSRuleConfigurationRequestAssertBlock>().

            ExecuteFlow();
        }
        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateGPSRuleConfiguration_TwoPiece()
        {

            TestingFlow.
            AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece}).

            AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    SetProperty(x => x.ProgramType).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update GPS Rule Configuration").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                     SetProperty(x => x.ProgramType).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                    SetProperty(x => x.RuleID).WithValue(RuleID4TwoPieceTests).
                    SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).//WithValue(1200).

                AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock>().UsePreviousBlockData().
                      SetDescription("Update GPS Rule Configuration test block").

                AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                    SetDescription("Create Get GPS Rule Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                     WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").

            AddBlock<UpdateGPSRuleConfigurationRequestAssertBlock>().

            ExecuteFlow();
        }
        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateHandlingProcedure_InheritAgency()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_BeforeUpdate").

                AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update handling procedure request").
                    
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.AutoEmailNotificationToAgency).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToAgency).
                    SetProperty(x => x.AutoFaxNotificationToAgency).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToAgency).
                    SetProperty(x => x.AutoPagerNotificationToAgency).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToAgency).
                    SetProperty(x => x.Comments).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.Comments).
                    SetProperty(x => x.InheritAgency).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.InheritAgency).
                    SetProperty(x => x.InheritOfficer).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.InheritOfficer).
                    SetProperty(x => x.ViolationHandling).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.ViolationHandling).
                    

                AddBlock<UpdateHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Update handling procedure test").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_AfterUpdate").

                AddBlock<UpdateHandlingProcedureAssertBlock>().
                SetDescription("UpdateHandling Procedure AssertBlock").



            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateHandlingProcedure_InheritOfficer()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(1).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_BeforeUpdate").

                AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create update handling procedure request").
                    
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.AutoEmailNotificationToOfficer).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.AutoEmailNotificationToOfficer).
                    SetProperty(x => x.AutoPagerNotificationToOfficer).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.AutoPagerNotificationToOfficer).
                    SetProperty(x => x.AutoFaxNotificationToOfficer).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.AutoFaxNotificationToOfficer).
                    SetProperty(x => x.Comments).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.Comments).
                    SetProperty(x => x.InheritAgency).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.InheritAgency).
                    SetProperty(x => x.InheritOfficer).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.InheritOfficer).
                    SetProperty(x => x.ViolationHandling).WithValueFromBlockIndex("GetHandlingProcedureTestBlock").
                    FromDeepProperty<GetHandlingProcedureTestBlock>(x => x.GetHandlingProcedureResponse.HandlingProcedure.ViolationHandling).


                AddBlock<UpdateHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Update handling procedure test").

                AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get handling procedure Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                    SetDescription("Get handling procedure test").
                    SetName("GetHandlingProcedureTestBlock_AfterUpdate").

                AddBlock<UpdateHandlingProcedureAssertBlock>().
                SetDescription("UpdateHandling Procedure AssertBlock").



            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateGPSOffenderConfigurationData_OnePieceGPSRF()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                     AddBlock<CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create Update GPS offender Configuration").
                      SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                                            SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                        SetProperty(x => x.BaseUnitRange).WithValue(Configuration0.EnmRFRange.Long).
                        SetProperty(x => x.ReceiverRange).WithValue(Configuration0.EnmRFRange.Short).

                    AddBlock<UpdateGPSOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Update GPS Offender Configuration Data TestBlock").

                    AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_AfterUpdate").

                AddBlock<UpdateGPSOffenderConfigurationDataAssertBlock>().
                    SetProperty(x => x.GetOffenderConfigurationDataDefaultResponse).
                    WithValueFromBlockIndex("GetOffenderConfigurationDataTestBlock_BeforeUpdate").
                    FromDeepProperty<GetOffenderConfigurationDataTestBlock>(x => x.GetOffenderConfigurationDataResponse).


             ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Configuration_Regression)]
        [TestMethod]
        public void Configuration_UpdateGPSOffenderConfigurationData_TwoPiece()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                 AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get offender Configuration Reuest").
                    SetProperty(x => x.OffenderID).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Configuration for offender").
                    SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                     AddBlock<CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                     SetDescription("Create Update GPS offender Configuration").
                      SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                        SetProperty(x => x.ProgramType).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                         FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).
                        SetProperty(x => x.BaseUnitRange).WithValue(Configuration0.EnmRFRange.Long).
                        SetProperty(x => x.ReceiverRange).WithValue(Configuration0.EnmRFRange.Short).

                    AddBlock<UpdateGPSOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Update GPS Offender Configuration Data TestBlock").

                    AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_AfterUpdate").

                    AddBlock<UpdateGPSOffenderConfigurationDataAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.GetOffenderConfigurationDataDefaultResponse).
                    WithValueFromBlockIndex("GetOffenderConfigurationDataTestBlock_BeforeUpdate").
                    FromDeepProperty<GetOffenderConfigurationDataTestBlock>(x => x.GetOffenderConfigurationDataResponse).
  
            ExecuteFlow();
        }
    }
}
