﻿using AssertBlocksLib.Trails;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.Trails;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using TestBlocksLib.Trails;
using TestBlocksLib.OffendersBlocks;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;

using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;

#endregion

namespace TestingFramework.UnitTests.TrailsSvcTests
{
    [TestClass]
    public class TrailsSvcLoadTests : BaseUnitTest
    {
        public const string Offenders_List_For_GetTrails_CsvFileName = @"DataSource\\Offenders_List_For_GetTrails.csv";

        [TestCategory(CategoriesNames.Trails_Service_Load)]
        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", 
            "|DataDirectory|\\" + Offenders_List_For_GetTrails_CsvFileName, 
            "Offenders_List_For_GetTrails#csv", 
            DataAccessMethod.Sequential),
            DeploymentItem(Offenders_List_For_GetTrails_CsvFileName)]
        public void TrailsSvcLoadTests_GetTrail_FromFile()
        {
            DataRow currentDataRow = TestContext.DataRow;

            var offenderRefIDFromFile = currentDataRow["RefID"].ToString();
            var StartTimeDays = int.Parse(currentDataRow["StartTimeDays"].ToString());
            var EndTimeDays = int.Parse(currentDataRow["EndTimeDays"].ToString());
            var minimumSeconds = int.Parse(currentDataRow["MinimumSeconds"].ToString());
            var minimumMeters = int.Parse(currentDataRow["MinimumMeters"].ToString());
            var coordinatesFormat = Enum.Parse(typeof(Trails0.EnmCoordinatesFormat), currentDataRow["CoordinatesFormat"].ToString());

            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by RefId").
                    SetProperty(x => x.OffenderRefIDQueryParameter).WithValue(
                        new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = offenderRefIDFromFile }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<CreateEntMsgGetTrailRequestBlock>().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(StartTimeDays)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.AddDays(EndTimeDays)).
                    SetProperty(x => x.MinimumSeconds).WithValue(minimumSeconds).
                    SetProperty( x=> x.MinimumMeters).WithValue(minimumMeters).
                    SetProperty(x => x.CoordinatesFormat).WithValue(coordinatesFormat).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Service_Load)]
        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV",
            "|DataDirectory|\\" + Offenders_List_For_GetTrails_CsvFileName,
            "Offenders_List_For_GetTrails#csv",
            DataAccessMethod.Sequential),
            DeploymentItem(Offenders_List_For_GetTrails_CsvFileName)]
        public void TrailsSvcTests_GetGeographicLocation__FromFile()
        {
            DataRow currentDataRow = TestContext.DataRow;

            var offenderRefIDFromFile = currentDataRow["RefID"].ToString();
            var StartTimeDays = int.Parse(currentDataRow["StartTimeDays"].ToString());
            var EndTimeDays = int.Parse(currentDataRow["EndTimeDays"].ToString());

            Log.Info($"Start test for offender ref-id {offenderRefIDFromFile} start time day : {StartTimeDays} end time days: {EndTimeDays}");

            TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request by RefId").
                    SetProperty(x => x.OffenderRefIDQueryParameter).WithValue(
                        new Offenders0.EntStringParameter() { Operator = Offenders0.EnmStringOperator.Equal, Value = offenderRefIDFromFile }).
                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").
                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[1]).
                    SetProperty(x=>x.OffendersIDs[0]).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(StartTimeDays)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.AddDays(EndTimeDays)).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationsAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }
    }
}
