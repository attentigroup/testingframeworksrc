﻿using AssertBlocksLib.Trails;
using Common.Categories;
using DataGenerators.Offender;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.Trails;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.Trails;
using TestingFramework.TestsInfraStructure.Implementation;
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;

namespace TestingFramework.UnitTests.TrailsSvcTests
{
    [TestClass]
    public class TrailsRegressionTests : BaseUnitTest
    {
        #region Get trails
        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_OffenderID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailByParamAssertBlock>().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_CordinatesFormat_Degrees()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WGS84_Degrees).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock>().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_CordinatesFormat_Meters()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock>().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_OnlyViolation_False()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).
                    SetProperty(x => x.OnlyViolation).WithValue(false).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailByParamAssertBlock>().
                    SetDescription("Get trail assert").
                    SetProperty(x => x.OnlyViolation).
                    WithValueFromBlockIndex("GetTrailTestBlock").
                    FromDeepProperty<GetTrailTestBlock>(x => x.GetTrailRequest.OnlyViolation).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_OnlyViolation_True()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).
                    SetProperty(x => x.OnlyViolation).WithValue(true).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailByParamAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get trail assert").
                    SetProperty(x => x.OnlyViolation).
                    WithValueFromBlockIndex("GetTrailTestBlock").
                    FromDeepProperty<GetTrailTestBlock>(x => x.GetTrailRequest.OnlyViolation).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_IncludeNoLocation_False()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).
                    SetProperty(x => x.IncludeNoLocation).WithValue(false).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock>().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_IncludeNoLocation_True()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).
                    SetProperty(x => x.IncludeNoLocation).WithValue(true).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock>().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestProperty("BUG", "33145")]
        [TestMethod]
        public void Trails_GetTrail_RelatedOffenderID()
        {
            TestingFlow.
                    AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).
                        SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                    AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    ///Aggressor points

                    AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Send Download Request").
                        SetProperty(x => x.Immediate).WithValue(true).
                        SetProperty(x => x.OffenderID).
                                WithValueFromBlockIndex("GetOffendersTestBlock").
                                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                        SetDescription("Send Download Request Test Block").

                    AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                        SetDescription("Run XT Simulator Test Block").
                        SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER;HW_DEVICE_BAT_LOW;HW_BTX_STRAP;HW_HW_RULE_SHUTDOWN").
                        SetProperty(x => x.EquipmentSN).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).
                    ///victim points

                    AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Send Download Request").
                        SetProperty(x => x.Immediate).WithValue(true).
                        SetProperty(x => x.OffenderID).
                                WithValueFromBlockIndex("GetOffendersTestBlock").
                                    FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RelatedOffenderID).

                    AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                        SetDescription("Send Download Request Test Block").

                    AddBlock<CreateEntMsgGetEquipmentListRequest>().
                        SetProperty(x => x.OffenderID).
                          WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RelatedOffenderID).

                   AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Equipment List Test Block").

                   AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                        SetDescription("Run XT Simulator Test Block").
                        SetProperty(x => x.Rule).WithValue("HW_DEVICE_TAMPER;HW_DEVICE_BAT_LOW;HW_BTX_STRAP;HW_HW_RULE_SHUTDOWN").
                        SetProperty(x => x.EquipmentSN).
                             WithValueFromBlockIndex("GetEquipmentListTestBlock").
                                    FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).


                    AddBlock<CreateEntMsgGetTrailRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create get trail request").
                        SetProperty(x => x.OffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock").
                              FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMinutes(-30)).
                        SetProperty(x => x.EndTime).WithValue(DateTime.Now.AddMinutes(+1)).
                        SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).
                        SetProperty(x => x.RelatedOffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock").
                                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].RelatedOffenderID).

                    AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                        SetDescription("Get trail test").

                    AddBlock<GetTrailRelatedOffenderIDAssertBlock>().
                        SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetTrail_Time()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WGS84_Degrees).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailByParamAssertBlock>().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }
        #endregion

        #region Get geographic location
        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocation_AgencyID()
        {
            TestingFlow.

                AddBlock<GetOffenderWithTrailTestBlock>().

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetOffenderWithTrailTestBlock").
                    FromDeepProperty< GetOffenderWithTrailTestBlock>(x => x.Offender.AgencyID).
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).


                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_TimeRange()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).


                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_Limit()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.Limit).WithValue(20).
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).


                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocation_OffenderRefID()
        {
            TestingFlow.

                AddBlock<GetOffenderWithTrailTestBlock>().

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffenderRefID).
                    WithValueFromBlockIndex("GetOffenderWithTrailTestBlock").
                    FromDeepProperty<GetOffenderWithTrailTestBlock>(x => x.Offender.RefID).
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).


                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_OffenderID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").
                    SetProperty(x => x.OffendersIDs).
                        WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                            FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsRequest.OffendersIDs).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_PositionID()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().UsePreviousBlockData().
                    SetName("CreateEntMsgGetGeographicLocationsRequestBlock_Position").
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.PositionsIDs).WithValue(new long[1]).
                    SetProperty(x => x.PositionsIDs[0]).
                    WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                    FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsResponse.Locations[0].ID).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                SetName("GetGeographicLocationsTestBlock_Position").
                    SetDescription("Get geographic locations test").
                    SetProperty(x => x.GetGeographicLocationsRequest).
                    WithValueFromBlockIndex("CreateEntMsgGetGeographicLocationsRequestBlock_Position").
                    FromDeepProperty<CreateEntMsgGetGeographicLocationsRequestBlock>(x => x.GetGeographicLocationsRequest).

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").
                    SetProperty(x => x.PositionsIDs).WithValue(new long[1]).
                    SetProperty(x => x.PositionsIDs[0]).
                    WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                    FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsResponse.Locations[0].ID).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_IncludeHistory_False()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).
                    SetProperty(x => x.IncludeHistory).WithValue(false).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").
                    SetProperty(x => x.OffendersIDs).
                        WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                            FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsRequest.OffendersIDs).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_IncludeHistory_True()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).
                    SetProperty(x => x.IncludeHistory).WithValue(true).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMonths(-3)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").
                    SetProperty(x => x.OffendersIDs).
                        WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                            FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsRequest.OffendersIDs).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_ProgramType_1Piece_GPS_RF()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.ProgramTypes).WithValue(new Trails0.EnmProgramType[] { Trails0.EnmProgramType.One_Piece_RF }).
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).


                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").
                    SetProperty(x => x.OffendersIDs).
                        WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                            FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsRequest.OffendersIDs).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_ProgramType_2Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.ProgramTypes).WithValue(new Trails0.EnmProgramType[] { Trails0.EnmProgramType.Two_Piece }).
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).


                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationByParamAssertBlock>().
                    SetDescription("Get geographic locations assert").
                    SetProperty(x => x.OffendersIDs).
                        WithValueFromBlockIndex("GetGeographicLocationsTestBlock").
                            FromDeepProperty<GetGeographicLocationsTestBlock>(x => x.GetGeographicLocationsRequest.OffendersIDs).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_SortDirection_Ascending()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).
                    SetProperty(x => x.SortDirection).WithValue(Trails0.ListSortDirection.Ascending).
                    SetProperty(x => x.SortField).WithValue(Trails0.EnmPositionsSortOptions.OffenderID).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationsBySortingAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetGeographicLocations_SortDirection_Descending()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[] { OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0] }).
                    SetProperty(x => x.SortDirection).WithValue(Trails0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Trails0.EnmPositionsSortOptions.Time).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationsBySortingAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }
        #endregion

        #region Get last location
        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_CoordinatesFormat_Degrees()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WGS84_Degrees).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

            //AddBlock<GetLastLocationAssertBlock>().
            //    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_CoordinatesFormat_Meters()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                //AddBlock<GetLastLocationAssertBlock>().
                //    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_OnlyInUnknownLocation_True()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.OnlyInUnknownLocation).WithValue(true).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_OnlyInUnknownLocation_False()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.OnlyInUnknownLocation).WithValue(false).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_ProgramType_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.ProgramType).WithValue(Trails0.EnmLastLocationProgramType.OnePieceRF).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_ProgramType_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.ProgramType).WithValue(Trails0.EnmLastLocationProgramType.TwoPiece).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_OnlyInViolation_True()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.OnlyInViolation).WithValue(true).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_OnlyInViolation_False()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.OnlyInViolation).WithValue(false).
                    SetProperty(x => x.AgencyID).WithValue(null).
                    SetProperty(x => x.OfficerID).WithValue(null).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_AgencyID()
        {
            TestingFlow.

                AddBlock<GetOffenderWithTrailTestBlock>().

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("GetOffenderWithTrailTestBlock").
                    FromDeepProperty< GetOffenderWithTrailTestBlock>(x => x.Offender.AgencyID).
                     SetProperty(x => x.OfficerID).
                    WithValueFromBlockIndex("GetOffenderWithTrailTestBlock").
                    FromDeepProperty<GetOffenderWithTrailTestBlock>(x => x.Offender.OfficerID).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Regression)]
        [TestMethod]
        public void Trails_GetLastLocation_OfficerID()
        {
            TestingFlow.

                AddBlock<GetOffenderWithTrailTestBlock>().

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create get last location request").
                    SetProperty(x => x.OfficerID).
                    WithValueFromBlockIndex("GetOffenderWithTrailTestBlock").
                    FromDeepProperty<GetOffenderWithTrailTestBlock>(x => x.Offender.OfficerID).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationByParamAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }


        #endregion
    }
}
