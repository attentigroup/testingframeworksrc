﻿using AssertBlocksLib.Trails;
using Common.Categories;
using DataGenerators.Agency;
using DataGenerators.Offender;
using LogicBlocksLib.Trails;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.Trails;
using TestingFramework.TestsInfraStructure.Implementation;

#region API refs
using Trails0 = TestingFramework.Proxies.EM.Interfaces.Trails12_0;
using Trails1 = TestingFramework.Proxies.EM.Interfaces.Trails3_10;
using Trails2 = TestingFramework.Proxies.EM.Interfaces.Trails3_9;
#endregion

namespace TestingFramework.UnitTests.TrailsSvcTests
{
    [TestClass]
    public class TrailsSvcTests : BaseUnitTest
    {
        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetGeographicLocation_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGeographicLocationsRequestBlock>().
                    SetDescription("Create get geographic locations request").
                    SetProperty(x => x.OffendersIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersIDs[0]).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    //SetProperty(x => x.IncludeHistory).WithValue(true).

                AddBlock<GetGeographicLocationsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get geographic locations test").

                AddBlock<GetGeographicLocationsAssertBlock>().
                    SetDescription("Get geographic locations assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetLastLocation_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock>().
                    SetDescription("Create get last location request"). 
                    SetProperty(x => x.AgencyID).WithValue(OffenderWithTrailIDGenerator.GetOffenderWithTrailAgencyID()).

                AddBlock<GetLastLocationTestBlock>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationAssertBlock>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetLastLocation_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock_1>().
                    SetDescription("Create get last location request").
                                                                                                                                   
                AddBlock<GetLastLocationTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationAssertBlock_1>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetLastLocation_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetLastLocationRequestBlock_2>().
                    SetDescription("Create get last location request").                                                                                                                                  

                AddBlock<GetLastLocationTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get last location test").

                AddBlock<GetLastLocationAssertBlock_2>().
                    SetDescription("Get last location assert").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetTrail_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails0.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetTrailTestBlock>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetTrail_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails1.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetTrailTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get trail test").

            AddBlock<GetTrailAssertBlock_1>().UsePreviousBlockData().
                SetDescription("Get trail assert").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Trails_Service)]
        [TestMethod]
        public void TrailsSvcTests_GetTrail_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetTrailRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create get trail request").
                    SetProperty(x => x.OffenderID).WithValue(OffenderWithTrailIDGenerator.OffenderWithTrailIdList[0]).
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddDays(-7)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now).
                    SetProperty(x => x.CoordinatesFormat).WithValue(Trails2.EnmCoordinatesFormat.WebMercator_Meters).

                AddBlock<GetTrailTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get trail test").

                AddBlock<GetTrailAssertBlock_2>().UsePreviousBlockData().
                    SetDescription("Get trail assert").

            ExecuteFlow();
        }
    }
}
