E4Simulator.exe -DCC 10.10.11.45 -PORT 8000 -UNIT 864 -SHUTDOWNTIMER 60000 -OFFENDERID 42943


INSERT INTO ems_e4_tmp_reply 
                                (lReqId,
                                iReplyStatus,
                                task_id,
                                offender_id,
                                hmru_type,
                                dcc_id,
                                queue_id,
                                sw_version,
                                reply_time)
                SELECT    a.request_id,
                                                1,                            
                                                400, -- unit_id from ems_e4_unit_data
                                                b.offender_id ,
                                                a.hmru_type,
                                                2,
                                                a.queue_id,
                                                '1.16.17',
                                                datediff(ss,'01-01-1970',dateadd(hh,-2,getdate()))
                FROM   ems_e4_queue a ,
                                                ems_e4_unit_data b,
                                ems_smsgateway_queue sq
                where   a.request_id =b.request_id  
                and                        unit_id=177
                and                        a.request_id = sq.source_request_id
                and                        sq.task_status in( 'Y','F')
                and                        a.task_status in ('D') and  a.request_id not in (select lReqId from ems_e4_tmp_reply) and a.hmru_id=345
