﻿using Common.Categories;
using LogicBlocksLib.EquipmentBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using Common.Enum;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.OffendersBlocks;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.DevicesBlocks;
using AssertBlocksLib.OffendersAsserts;
using AssertBlocksLib.ProgramActionsAsserts;
using TestingFramework.TestsInfraStructure.Interfaces;
using System.Linq;
using System.Data;
using TestBlocksLib;
using System;
using TestBlocksLib.QueueBlocks;
using LogicBlocksLib.QueueBlocks;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using Log0 = TestingFramework.Proxies.EM.Interfaces.Log12_0;
using TestBlocksLib.LogBlocks;

#endregion

namespace TestingFramework.UnitTests.ProgramActionsSvcTests
{
    /// <summary>
    /// implelemntation of all regression tests for program actions service.
    /// </summary>
    [TestClass]
    public class SendEndOfServiceRegressionTests : BaseUnitTest
    {
        public const string EosCodes_CSV_File = @"EosCodes.csv";
        public const string EosCode_COL_NAME = "EosCode";
        private static void GenerateCsvFileForEosCode(string filePath)
        {
            var sbData = new System.Text.StringBuilder();

            //header
            sbData.AppendLine(EosCode_COL_NAME);

            //sbData.AppendLine("AEOS");
            //sbData.AppendLine("AFS");
            //sbData.AppendLine("CAWOL");
            //sbData.AppendLine("FAWOL");
            sbData.AppendLine("MEOS");
            //sbData.AppendLine("NAF");
            //sbData.AppendLine("RJCT");
            //sbData.AppendLine("TECH");

            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            System.IO.File.WriteAllText(filePath, sbData.ToString());
        }

        [ClassInitialize]
        public static void ProgramActionsSvcRegressionTestsInit(TestContext testContext)
        {
            GenerateCsvFileForEosCode(EosCodes_CSV_File);
        }

        public ITestingFlow ProgramActions_GetEndOfServiceData(ITestingFlow testingFlow, 
            Equipment0.EnmEquipmentModel EnmEquipmentModel, 
            Equipment0.EnmEncryptionType EquipmentEncryptionGSM, Equipment0.EnmEncryptionType EquipmentEncryptionRF,
            Equipment0.EnmModemType ModemType,
            Equipment0.EnmProtocolType ProtocolType,
            string EquipmentSerialNumberDataGenerator,
            //offender
            Offenders0.EnmProgramType ProgramType,
            //EOS Data
            ProgramActions0.EnmEOSType EndOfServiceType,
            string EndOfServiceCode, 
            int QueueFilterID,
            Queue0.EnmResultCode ResultCode,
            int LogFilterID
             )
        {
            var sEosCode4Logs = $" *** EOS code: {EndOfServiceCode} ***";
            Log.Info(sEosCode4Logs);
            Console.WriteLine(sEosCode4Logs);

            testingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("create random EntMsgAddEquipmentRequest type").
                   SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EquipmentEncryptionGSM).
                   SetProperty(x => x.EquipmentEncryptionRF).WithValue(EquipmentEncryptionRF).
                   SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel).
                   SetProperty(x => x.ModemType).WithValue(ModemType).
                   SetProperty(x => x.ProtocolType).WithValue(ProtocolType).
                   SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EquipmentSerialNumberDataGenerator).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(ProgramType).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(QueueFilterID).
                    SetProperty(x => x.resultCode).WithValue(ResultCode).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(20).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(LogFilterID).
                    SetProperty(x => x.resultCode).WithValue((Log0.EnmResultCode)ResultCode).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

               AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                   SetDescription("Create Send End Of Service Request").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EndOfServiceType).WithValue(EndOfServiceType).
                   SetProperty(x => x.EndOfServiceCode).WithValue(EndOfServiceCode).
                   SetProperty(x => x.Comment).WithValue("Send Manual EOS Request Test").

               AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                   SetDescription("Send End Of Service Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(20).

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                  AddBlock<SleepTestingBlock>().
                      SetProperty(x => x.SleepDurationSeconds).WithValue(20).

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

               AddBlock<CreateEntMsgGetEndOfServiceDataRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Ent Msg Get End Of Service Data Request Block").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetEndOfServiceDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get End Of Service Data Test Block").

               AddBlock<GetEndOfServiceDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get End Of Service Data Assert Block");

            return testingFlow;
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceRequest_1Piece_GPS_RF()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.One_Piece_GPS_RF,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_5,
                EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString(), 
                Offenders0.EnmProgramType.One_Piece_RF, 
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                46, Queue0.EnmResultCode.REQ_PRG_1TRACK, 76);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_1Piece_G39()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_5,
                EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString(),
                Offenders0.EnmProgramType.One_Piece_RF,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                46, Queue0.EnmResultCode.REQ_PRG_1TRACK, 76);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_1Piece_G4i()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G4i,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_5,
                EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_4_i.ToString(),
                Offenders0.EnmProgramType.One_Piece_RF,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                46, Queue0.EnmResultCode.REQ_PRG_1TRACK, 76);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_2Piece_3G()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.Two_Piece_3G,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_0,
                EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString(),
                Offenders0.EnmProgramType.Two_Piece,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                47, Queue0.EnmResultCode.REQ_PRG_2TRACK, 77);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_2Piece_3G_Encrypted()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.Two_Piece_3G,
                Equipment0.EnmEncryptionType.Encryption_256,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_8,
                EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString(),
                Offenders0.EnmProgramType.Two_Piece,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                47, Queue0.EnmResultCode.REQ_PRG_2TRACK, 77);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
            DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
           "EosCodes#csv",
           DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_2Piece_GPS()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.Two_Piece_GPS,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_0,
                EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_4.ToString(),
                Offenders0.EnmProgramType.Two_Piece,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                47, Queue0.EnmResultCode.REQ_PRG_2TRACK, 77);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_2Piece_3G_V7()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.Two_Piece_3G_V7,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_0,
                EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_7.ToString(),
                Offenders0.EnmProgramType.Two_Piece,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                47, Queue0.EnmResultCode.REQ_PRG_2TRACK, 77);

            TestingFlow.
                ExecuteFlow();
        }

        [TestMethod,
             DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\" + EosCodes_CSV_File,
            "EosCodes#csv",
            DataAccessMethod.Sequential)]
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        public void ProgramActions_SendEndOfServiceReques_2Piece_3G_V7_Encrypted()
        {
            DataRow currentDataRow = TestContext.DataRow;
            string EndOfServiceCode = currentDataRow[EosCode_COL_NAME].ToString();

            ProgramActions_GetEndOfServiceData(TestingFlow,
                Equipment0.EnmEquipmentModel.Two_Piece_3G_V7,
                Equipment0.EnmEncryptionType.Encryption_256,
                Equipment0.EnmEncryptionType.NoEncryption,
                Equipment0.EnmModemType.ThreeG,
                Equipment0.EnmProtocolType.Protocol_8,
                EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_7.ToString(),
                Offenders0.EnmProgramType.Two_Piece,
                ProgramActions0.EnmEOSType.Default, EndOfServiceCode,
                47, Queue0.EnmResultCode.REQ_PRG_2TRACK, 77);

            TestingFlow.
                ExecuteFlow();
        }

        
    }
}
