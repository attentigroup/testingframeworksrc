﻿using AssertBlocksLib.ProgramActionsAsserts;
using Common.Categories;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using LogicBlocksLib.QueueBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.QueueBlocks;
using TestingFramework.TestsInfraStructure.Implementation;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using LogicBlocksLib.Events;
using TestBlocksLib.EventsBlocks;
using Events0 = TestingFramework.Proxies.EM.Interfaces.Events12_0;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using Common.Enum;
using AssertBlocksLib.OffendersAsserts;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestBlocksLib;
using TestBlocksLib.LogBlocks;
using System.ComponentModel;

namespace TestingFramework.UnitTests.ProgramActionsSvcTests
{
    [TestClass]
    public class ProgramActionRegressionTests : BaseUnitTest
    {
        #region Insert manual event
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_InsertManualEvent_New()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgInsertManualEventRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Comment).WithValue("Manual event").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.HandlingStatus).WithValue(ProgramActions0.EnmHandlingStatusType.New).
                    SetProperty(x => x.EventCode).WithValue("LV1").

                AddBlock<InsertManualEventTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMinutes(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.AddMinutes(2)).
                    SetProperty(x => x.Limit).WithValue(10).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.New).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<InsertManualEventsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_InsertManualEvent_InProcess()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgInsertManualEventRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Comment).WithValue("Manual event").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.HandlingStatus).WithValue(ProgramActions0.EnmHandlingStatusType.InProcess).
                    SetProperty(x => x.EventCode).WithValue("LV1").

                AddBlock<InsertManualEventTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMinutes(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.AddMinutes(2)).
                    SetProperty(x => x.Limit).WithValue(10).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.InProcess).
                    //WithValueFromBlockIndex("InsertManualEventTestBlock").
                    //        FromDeepProperty<InsertManualEventTestBlock>(x => x.InsertManualEventRequest.HandlingStatus).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<InsertManualEventsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_InsertManualEvent_Handled()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgInsertManualEventRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Comment).WithValue("Manual event").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.HandlingStatus).WithValue(ProgramActions0.EnmHandlingStatusType.Handled).
                    SetProperty(x => x.EventCode).WithValue("LV1").

                AddBlock<InsertManualEventTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetDescription("Create get events request").
                    SetProperty(x => x.StartTime).WithValue(DateTime.Now.AddMinutes(-2)).
                    SetProperty(x => x.EndTime).WithValue(DateTime.Now.AddMinutes(2)).
                    SetProperty(x => x.Limit).WithValue(10).
                    SetProperty(x => x.Status).WithValue(Events0.EnmHandlingStatusType.Handled).
                    SetProperty(x => x.SortDirection).WithValue(System.ComponentModel.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().
                    SetDescription("Get events test").

                AddBlock<InsertManualEventsAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }
        #endregion

        #region Send download request
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_Immediate_1Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").
             
                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock< WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                SetName("GetQueueTestBlock_After").
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock_After").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EventCode).WithValue("S01").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_NotImmediate_1Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(false).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).


                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                SetName("GetQueueTestBlock_After").
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock_After").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("S01").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_Immediate_2Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                    SetDescription("Gets one active offender").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).
                            SetProperty(x => x.IsMultipleDownload).WithValue(false).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EventCode).WithValue("S01").

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_NotImmediate_2Piece_GPS()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                    SetDescription("Gets one active offender").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(false).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).
                            SetProperty(x => x.IsMultipleDownload).WithValue(false).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("S01").

                ExecuteFlow();
        }

        
        //private ITestingFlow Schedule_AddTimeFrame_Group_Isweekly(ITestingFlow testingFlow, Zones0.EnmLimitationType LimitationType, Schedule0.EnmTimeFrameType TimeFrameType)
        //{
        //    testingFlow = AddGroupWithOffender(TestingFlow);

        //    if (LimitationType == Zones0.EnmLimitationType.Inclusion)
        //        testingFlow = AddInclusionZoneToAGroup(testingFlow);
        //    else
        //        testingFlow = AddExclusionZoneToAGroup(testingFlow);

        //    testingFlow = AddTimeFrameToAGroup(testingFlow, TimeFrameType, true);

        //    testingFlow = AddWeeklyTimeFrameToAGroupAssert(testingFlow);

        //    return testingFlow;
        //}

        //private ITestingFlow AddGroupWithOffender(ITestingFlow testingFlow)
        //{
        //    TestingFlow.

        //         AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            SetDescription("Create Add Offender Request").
        //            SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

        //        AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
        //            SetDescription("Add Offender test block").

        //        AddBlock<CreateEntMsgAddGroupRequestBlock>().ShallGeneratePropertiesWithRandomValues().
        //            //SetProperty(x => x.Name).WithValue("New group").
        //            SetProperty(x => x.Description).WithValue("Group description").
        //            SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
        //            SetProperty(x => x.OffendersList).WithValue(new int[1]).
        //            SetProperty(x => x.OffendersList[0]).
        //                WithValueFromBlockIndex("AddOffenderTestBlock").
        //                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

        //        AddBlock<AddGroupTestBlock>().UsePreviousBlockData();

        //    return testingFlow;
        //}
        /// <summary>
        /// //////////
        /// </summary>
        #endregion

        #region Pre Production tests
        [TestCategory(CategoriesNames.Under_Construction)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_Immediate_Multiple_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(45).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_E4).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                //AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                //    SetDescription("Create request to get offenders list").
                //    SetProperty(x => x.Limit).WithValue(100).
                //    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                //    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                //AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                //    SetDescription("Request to get offenders list").

                //AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                //    SetDescription("Gets one active offender").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).WithValue(7023).
                            //WithValueFromBlockIndex("GetOneActiveOffender").
                            //    FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).
                            SetProperty(x => x.IsMultipleDownload).WithValue(true).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                //AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                //    SetDescription("Run XT Simulator Test Block").
                //    SetProperty(x => x.EquipmentSN).WithValue("116401").
                //WithValueFromBlockIndex("GetOneActiveOffender").
                //    FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Receiver).WithValue(116401).
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().

               AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("DW0").

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("J1V").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Under_Construction)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_NotImmediate_Multiple_E4()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(45).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_E4).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                //AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                //    SetDescription("Create request to get offenders list").
                //    SetProperty(x => x.Limit).WithValue(100).
                //    SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                //    SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.Two_Piece }).

                //AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                //    SetDescription("Request to get offenders list").

                //AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                //    SetDescription("Gets one active offender").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(false).
                    SetProperty(x => x.OffenderID).WithValue(7023).
                            //WithValueFromBlockIndex("GetOneActiveOffender").
                            //    FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).
                            SetProperty(x => x.IsMultipleDownload).WithValue(true).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                //AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                //    SetDescription("Run XT Simulator Test Block").
                //    SetProperty(x => x.EquipmentSN).WithValue("116401").
                //WithValueFromBlockIndex("GetOneActiveOffender").
                //    FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Receiver).WithValue(116401).
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("DW0").

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("J1V").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Under_Construction)]
        [TestMethod]
        public void ProgramActions_SendStartRangeTestRequest_Buzzer()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(45).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_E4).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgSendStartRangeRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Buzzer).WithValue(true).
                    SetProperty(x => x.OffenderID).WithValue(6930).
                    SetProperty(x => x.Range).WithValue(1).

                AddBlock<SendStartRangeTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendStartRangeRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendStartRangeTestRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendStartRangeRequestBlock").
                            FromDeepProperty<CreateEntMsgSendStartRangeRequestBlock>(x => x.EntMsgSendStartRangeTestRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Receiver).WithValue("515144").
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).
                    SetProperty(x => x.OffenderID).WithValue(null).

                AddBlock<GetEventsTestBlock>().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("SRT").

                //AddBlock<CreateEntMsgSendEndRangeRequestBlock>().UsePreviousBlockData().
                //    SetDescription("Create Send Download Request").
                //    SetProperty(x => x.OffenderID).WithValue(6930).

                //AddBlock<SendEndRangeTestBlock>().UsePreviousBlockData().
                //    SetDescription("Send Download Request Test Block").
                //    SetProperty(x => x.EntMsgSendEndRangeTestRequest).
                //    WithValueFromBlockIndex("CreateEntMsgSendEndRangeRequestBlock").
                //    FromDeepProperty<CreateEntMsgSendEndRangeRequestBlock>(x => x.EntMsgSendEndRangeTestRequest).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Under_Construction)]
        [TestMethod]
        public void ProgramActions_SendStartRangeTestRequest_NoBuzzer()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(45).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_E4).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgSendStartRangeRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Buzzer).WithValue(false).
                    SetProperty(x => x.OffenderID).WithValue(6930).
                    SetProperty(x => x.Range).WithValue(1).

                AddBlock<SendStartRangeTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendStartRangeRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendStartRangeTestRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendStartRangeRequestBlock").
                            FromDeepProperty<CreateEntMsgSendStartRangeRequestBlock>(x => x.EntMsgSendStartRangeTestRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Receiver).WithValue("515144").
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).
                    SetProperty(x => x.OffenderID).WithValue(null).

                AddBlock<GetEventsTestBlock>().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("SRT").

                //AddBlock<CreateEntMsgSendEndRangeRequestBlock>().UsePreviousBlockData().
                //    SetDescription("Create Send Download Request").
                //    SetProperty(x => x.OffenderID).WithValue(6930).

                //AddBlock<SendEndRangeTestBlock>().UsePreviousBlockData().
                //    SetDescription("Send Download Request Test Block").
                //    SetProperty(x => x.EntMsgSendEndRangeTestRequest).
                //    WithValueFromBlockIndex("CreateEntMsgSendEndRangeRequestBlock").
                //    FromDeepProperty<CreateEntMsgSendEndRangeRequestBlock>(x => x.EntMsgSendEndRangeTestRequest).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Under_Construction)]
        [TestMethod]
        public void ProgramActions_SendEndRangeTestRequest_Buzzer()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(45).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_E4).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgSendStartRangeRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Buzzer).WithValue(true).
                    SetProperty(x => x.OffenderID).WithValue(6930).
                    SetProperty(x => x.Range).WithValue(1).

                AddBlock<SendStartRangeTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendStartRangeRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendStartRangeTestRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendStartRangeRequestBlock").
                            FromDeepProperty<CreateEntMsgSendStartRangeRequestBlock>(x => x.EntMsgSendStartRangeTestRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Receiver).WithValue("515144").
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).
                    SetProperty(x => x.OffenderID).WithValue(null).

                AddBlock<GetEventsTestBlock>().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("SRT").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(120).

                AddBlock<CreateEntMsgSendEndRangeRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.OffenderID).WithValue(6930).

                AddBlock<SendEndRangeTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").
                    SetProperty(x => x.EntMsgSendEndRangeTestRequest).
                    WithValueFromBlockIndex("CreateEntMsgSendEndRangeRequestBlock").
                    FromDeepProperty<CreateEntMsgSendEndRangeRequestBlock>(x => x.EntMsgSendEndRangeTestRequest).

                    AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(50).

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.Receiver).WithValue("515144").
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).
                    SetProperty(x => x.OffenderID).WithValue(null).

                AddBlock<GetEventsTestBlock>().

                 AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("ERT").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Under_Construction)]
        [TestMethod]
        public void ProgramActions_ClearCurfewUnitTamper_OnePieceGPSRF()
        {
            TestingFlow.

                //  AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                //     SetDescription("Create EntMsgAddEquipmentRequest Type").
                //     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                //     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                //     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                //     SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                //     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                //     SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

                //AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                // SetName("AddEquipmentTestBlock_1Piece").
                //    SetDescription("Add equipment test block").

                //    AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                //     SetDescription("Create EntMsgAddCellularDataRequest Type").
                //     SetProperty(x => x.EquipmentID).
                //         WithValueFromBlockIndex("AddEquipmentTestBlock").
                //             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                // AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                //     SetDescription("Add Cellular Data test block").

                //    AddBlock<CreateEntMsgGetEquipmentListRequest>().
                //     SetProperty(x => x.EquipmentID).
                //         WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                //             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                //     SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                // AddBlock<GetEquipmentListTestBlock>().
                //     SetName("GetEquipmentListTestBlock_1Piece").

                // AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                //     SetDescription("create random EntMsgAddEquipmentRequest type").
                //     SetProperty(x => x.AgencyID).
                //     WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                //             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                //     SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                //     SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                //     SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                //     SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                //     SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                //     SetProperty(x => x.EquipmentSerialNumber).
                //         WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                // AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                //     SetDescription("Add equipment test block").
                //     SetName("AddEquipmentTestBlock_Beacon").

                // AddBlock<CreateEntMsgGetEquipmentListRequest>().
                // SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                //     SetProperty(x => x.EquipmentID).
                //         WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                //             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                //     SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                // AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                // SetName("GetEquipmentListTestBlock_Beacon").
                // SetProperty(x => x.GetEquipmentListRequest).
                //         WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                //             FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                // AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                //     SetDescription("Create Add Offender Request").
                //     SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                //     SetProperty(x => x.Language).WithValue("ENG").
                //     SetProperty(x => x.Receiver).
                //         WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                //             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                //     SetProperty(x => x.AgencyID).
                //         WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                //             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListRequest.AgencyID).
                //     SetProperty(x => x.HomeUnit).
                //         WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                //              FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

                //AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                //     SetDescription("Add Offender test block").

                // AddBlock<CreateMsgAddCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                //     SetDescription("Create Add Circular Zone Request").
                //     SetProperty(x => x.EntityID).
                //         WithValueFromBlockIndex("AddOffenderTestBlock").
                //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                //     //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                //     SetProperty(x => x.RealRadius).WithValue(50).
                //     //SetProperty(x => x.BufferRadius).WithValue(10).
                //     SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                //     SetProperty(x => x.GraceTime).WithValue(1).
                //     SetProperty(x => x.IsCurfewZone).WithValue(true).
                //     SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                // AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                //     SetDescription("Add Circular Zone test block").

                // AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                //     SetDescription("Create Send Download Request").
                //     SetProperty(x => x.Immediate).WithValue(true).
                //     SetProperty(x => x.OffenderID).
                //         WithValueFromBlockIndex("AddOffenderTestBlock").
                //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                // AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                //     SetDescription("Send Download Request Test Block").

                // AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                //     SetDescription("Run XT Simulator Test Block").
                //     SetProperty(x => x.EquipmentSN).
                //          WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                //             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.SerialNumber).
                //             SetProperty(x => x.ACKCOUNT).WithValue("1").

                // AddBlock<SleepTestingBlock>().
                //     SetProperty(x => x.SleepDurationSeconds).WithValue(20).

                // AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                //      SetDescription("Create Get Offenders Request").
                //      SetProperty(x => x.OffenderID).
                //          WithValueFromBlockIndex("AddOffenderTestBlock").
                //              FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                // AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                //      SetDescription("Get Offenders Test Block").

                // AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //      SetDescription("Is Active Assert Block").
                //      SetProperty(x => x.Expected).WithValue(true).

                // AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                //     SetDescription("Run XT Simulator Test Block").
                //     SetProperty(x => x.EquipmentSN).
                //         WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                //             FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.SerialNumber).
                //     //SetProperty(x => x.Rule).WithValue("Home_Unit_Tamper-violation").
                //     SetProperty(x => x.Rule).WithValue("Home_Unit_Tamper-first_violation").
                //     SetProperty(x => x.NUMPOINTS).WithValue("1").
                //     SetProperty(x => x.ACKCOUNT).WithValue("1").
                
                AddBlock<CreateEntMsgClearCurfewUnitTamperRequestBlock>()./*UsePreviousBlockData().*/
                    SetDescription("Create Ent Msg Clear Curfew Unit Tamper Request Block").
                    SetProperty(x => x.OffenderID).WithValue(7045).
                //WithValueFromBlockIndex("AddOffenderTestBlock").
                //    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ClearCurfewUnitTamperRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear Curfew Unit Tamper Request Test Block").

                //AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                //    SetDescription("Run XT Simulator Test Block").
                //    SetProperty(x => x.EquipmentSN).
                //         WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                //            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.SerialNumber).
                //            //SetProperty(x => x.NUMPOINTS).WithValue("1").
                //            SetProperty(x => x.ACKCOUNT).WithValue("1").
                //            SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                //    SetProperty(x => x.NUMPOINTS).WithValue("2").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).WithValue(7045).
                    //WithValueFromBlockIndex("AddOffenderTestBlock").
                    //   FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.EventCode).WithValue("P80").

                //AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                //    SetDescription("Create Send End Of Service Request").
                //    SetProperty(x => x.OffenderID).
                //         WithValueFromBlockIndex("AddOffenderTestBlock").
                //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                //    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                //    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                //    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                //AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                //    SetDescription("Send End Of Service Test Block").

                //AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                //    SetDescription("Run XT Simulator Test Block").
                //    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                //    SetProperty(x => x.EquipmentSN).
                //        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                //            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.SerialNumber).

                //AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                //     SetDescription("Create Get Offenders Request").
                //     SetProperty(x => x.OffenderID).
                //         WithValueFromBlockIndex("AddOffenderTestBlock").
                //             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                //AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                //     SetDescription("Get Offenders Test Block").

                //AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                //     SetDescription("Is Active Assert Block").
                //     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_GetEndOfServiceOptions()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211120").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).  
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<GetEndOfServiceOptionsTestBlock>().
                    SetDescription("Get End Of Service Options Test Block").

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).
                        WithValueFromBlockIndex("GetEndOfServiceOptionsTestBlock").
                             FromDeepProperty<GetEndOfServiceOptionsTestBlock>(x => x.EntMsgGetEndOfServiceOptionsResponse.Options[0].Code).
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(60).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_GetManualEventOptions()
        {
            TestingFlow.

                AddBlock<GetManualEventOptionsTestBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_GetProgramSuspensionsData()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211124").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(60).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<GetSuspendProgramOptionsTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Suspend Program Options Test Block").

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).
                        WithValueFromBlockIndex("GetSuspendProgramOptionsTestBlock").
                            FromPropertyName(EnumPropertyName.SuspendReason).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).
                        WithValueFromBlockIndex("GetSuspendProgramOptionsTestBlock").
                            FromPropertyName(EnumPropertyName.ReactivateReason).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_GetSuspendProgramOptions()
        {
            TestingFlow.

                AddBlock<GetSuspendProgramOptionsTestBlock>().

            ExecuteFlow();
        }

        #region Send message
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendMessageRequest_Immediate_TwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).//WithValue("35611121").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendMessageRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Message).WithValue("Send Message Request Test").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendMessageRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                SetName("GetQueueTestBlock_SendMessage").
                    SetDescription("Get Queue Test Block").

                AddBlock<SendMessageRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendMessageRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendMessageRequestBlock").
                            FromDeepProperty<CreateEntMsgSendMessageRequestBlock>(x => x.EntMsgSendMessageRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock_SendMessage").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendMessageRequest_NotImmediate_TwoPiece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).//WithValue("35611121").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendMessageRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Message).WithValue("Send Message Request Test").
                    SetProperty(x => x.Immediate).WithValue(false).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendMessageRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). 
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                SetName("GetQueueTestBlock_SendMessage").
                    SetDescription("Get Queue Test Block").

                AddBlock<SendMessageRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendMessageRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendMessageRequestBlock").
                            FromDeepProperty<CreateEntMsgSendMessageRequestBlock>(x => x.EntMsgSendMessageRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock_SendMessage").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }
        #endregion

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_ReactivateProgram_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_ReactivateProgram_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).  
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SuspendProgram_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211123").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("SUS").

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_Suspend").
                     SetDescription("Get Offenders Test Block").

                AddBlock<OffenderStatusAssertBlock>().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Suspended).
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Suspend").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SuspendProgram_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                    AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("SUS").

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                SetName("GetOffendersTestBlock_Suspend").
                     SetDescription("Get Offenders Test Block").

                AddBlock<OffenderStatusAssertBlock>().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Suspended).
                    SetProperty(x => x.GetOffendersResponse).
                        WithValueFromBlockIndex("GetOffendersTestBlock_Suspend").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse).

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendUploadRequest_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211122").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendUploadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendUploadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendUploadRequest_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                SetProperty(x => x.SecondsTimeOut).WithValue(30).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendUploadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendUploadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendVibrationRequest_OnePieceGPSRF()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46). // 46-> 1TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("create random EntMsgAddEquipmentRequest type").
                   SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                   SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                   SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                   SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                   SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                   SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.SecondsTimeOut).WithValue(30).

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgSendVibrationRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Vibration Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendVibrationRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                    WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendVibrationRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendVibrationRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendVibrationRequestBlock").
                            FromDeepProperty<CreateEntMsgSendVibrationRequestBlock>(x => x.EntMsgSendVibrationRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                ExecuteFlow();
        }

       

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_StartOnlineMode_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(false).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                SetName("GetQueueTestBlock_After").
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgStartOnlineModeRequestBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<StartOnlineModeTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("S29").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_StopOnlineMode_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgStartOnlineModeRequestBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<StartOnlineModeTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgStopOnlineModeRequestBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<StopOnlineModeTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("S30").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_StartOnlineMode_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgStartOnlineModeRequestBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<StartOnlineModeTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("S29").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_StopOnlineMode_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(false).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

                AddBlock<CreateEntMsgStartOnlineModeRequestBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<StartOnlineModeTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgStopOnlineModeRequestBlock>().
                    SetProperty(x => x.ReceiverSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<StopOnlineModeTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgGetEventsRequestBlock>().
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SortDirection).WithValue(Offenders0.ListSortDirection.Descending).
                    SetProperty(x => x.SortField).WithValue(Events0.EnmEventsSortOptions.EventTime).
                    SetProperty(x => x.LastTimeStamp).WithValue(null).
                    SetProperty(x => x.ProgramType).WithValue(null).

                AddBlock<GetEventsTestBlock>().UsePreviousBlockData().

                AddBlock<ProgramActionsEventAssertBlock>().UsePreviousBlockData().
                SetProperty(x => x.EventCode).WithValue("S30").

                ExecuteFlow();
        }

        #region Send EOS
        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendEndOfServiceRequest_Default_WithoutDeactivation_OnePiece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("create random EntMsgAddEquipmentRequest type").
                   SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                   SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                   SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                   SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                   SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                   SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

               AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                   SetDescription("Create Send End Of Service Request").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                   SetProperty(x => x.EndOfServiceCode).WithValue("AEOS").
                   SetProperty(x => x.Comment).WithValue("Send Manual EOS Request Test").
                   SetProperty(x => x.StopProgramWithoutDeactivation).WithValue(true).

               AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                   SetDescription("Send End Of Service Test Block").

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(76).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

               AddBlock<CreateEntMsgGetEndOfServiceDataRequestBlock>().
                   SetDescription("Create Ent Msg Get End Of Service Data Request Block").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetEndOfServiceDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get End Of Service Data Test Block").

               AddBlock<GetEndOfServiceDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get End Of Service Data Assert Block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                    SetDescription("Is Active Assert Block").
                    SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service_Regression)]
        [TestMethod]
        public void ProgramActions_SendEndOfServiceRequest_Default_WithoutDeactivation_TwoPiece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("create random EntMsgAddEquipmentRequest type").
                   SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                   SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                   SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                   SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                   SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                   SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<WaitForActiveRequestTestBlock>().UsePreviousBlockData().

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.Active).

               AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                   SetDescription("Create Send End Of Service Request").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                   SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                   SetProperty(x => x.EndOfServiceCode).WithValue("AEOS").
                   SetProperty(x => x.Comment).WithValue("Send Manual EOS Request Test").
                   SetProperty(x => x.StopProgramWithoutDeactivation).WithValue(true).

               AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                   SetDescription("Send End Of Service Test Block").

               AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                   SetDescription("Run XT Simulator Test Block").
                   SetProperty(x => x.EquipmentSN).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<GetLogTestBlock>().
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(50).
                    SetProperty(x => x.filterID).WithValue(77).
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<WaitForDoneRequestStatusAssertBlock>().UsePreviousBlockData().
                    SetProperty(x => x.RequestID).
                    WithValueFromBlockIndex("WaitForActiveRequestTestBlock").
                    FromDeepProperty<WaitForActiveRequestTestBlock>(x => x.EntQueueProgramTracker.RequestID).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<WaitForSpecificOffenderStatusTestBlock>().UsePreviousBlockData().
                    SetProperty(x => x.ExpectedStatus).WithValue(Offenders0.EnmProgramStatus.PostActive).

               AddBlock<CreateEntMsgGetEndOfServiceDataRequestBlock>().
                   SetDescription("Create Ent Msg Get End Of Service Data Request Block").
                   SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetEndOfServiceDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Get End Of Service Data Test Block").

               AddBlock<GetEndOfServiceDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get End Of Service Data Assert Block").

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                    SetDescription("Is Active Assert Block").
                    SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }
        #endregion

    }
}
