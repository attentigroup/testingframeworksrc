﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.QueueBlocks;
using TestBlocksLib.QueueBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using Common.Categories;
using AssertBlocksLib.ProgramActionsAsserts;
using TestBlocksLib.DevicesBlocks;
using AssertBlocksLib.OffendersAsserts;


#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Queue0 = TestingFramework.Proxies.EM.Interfaces.Queue12_0;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using ProgramActions0 = TestingFramework.Proxies.EM.Interfaces.ProgramActions12;
using TestBlocksLib;
#endregion

namespace TestingFramework.UnitTests.ProgramActionsSvcTests
{
    [TestClass]
    public class ProgramActionsSvcTests : BaseUnitTest
    {
        Offenders0.EnmProgramStatus[] status_Active = { Offenders0.EnmProgramStatus.Active };
        Offenders0.EnmProgramStatus[] status_Pre_Active = { Offenders0.EnmProgramStatus.PreActive };

        Offenders0.EnmProgramType[] oneP_program = { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_RF };
        Offenders0.EnmProgramType[] oneP_GPS_RF_program = { Offenders0.EnmProgramType.One_Piece_RF };
        Offenders0.EnmProgramType[] twoP_program = { Offenders0.EnmProgramType.Two_Piece };

        Offenders0.EnmProgramType[] all_Tracker_program = { Offenders0.EnmProgramType.One_Piece, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramType.Two_Piece };

        Offenders1.EnmProgramType[] all_Tracker_program_1 = { Offenders1.EnmProgramType.One_Piece, Offenders1.EnmProgramType.One_Piece_RF, Offenders1.EnmProgramType.Two_Piece };



        [TestCategory(CategoriesNames.Setup_Offenders)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffendersProgramEnds_Success()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(all_Tracker_program).
                    SetProperty(x => x.ProgramStatus).WithValue(status_Active).

               AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<UpdateOffendersProgramTimeTestBlock>().UsePreviousBlockData().
                    SetDescription("Update Offenders Program Time Test Block").
                    
               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Setup_Offenders)]
        [TestMethod]
        public void OffendersSvcTests_UpdateOffendersProgramEnds_Success_1()
        {
            TestingFlow.

               AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                    SetDescription("Create Get Offenders Request").
                    SetProperty(x => x.ProgramType).WithValue(all_Tracker_program_1).
                    SetProperty(x => x.ProgramStatus).WithValue(status_Active).

               AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Offenders Test Block").

               AddBlock<UpdateOffendersProgramTimeTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Update Offenders Program Time Test Block").
                    
               ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(status_Pre_Active).
                    SetProperty(x => x.ProgramType).WithValue(twoP_program).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                    SetDescription("Gets one active offender").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendDownloadRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendDownloadRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendDownloadRequestBlock").
                            FromDeepProperty<CreateEntMsgSendDownloadRequestBlock>(x => x.EntMsgSendDownloadRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

            /* Wait until there's a conversation abililty */
            //AddBlock<GpsDeviceProxyMakeConversationTestBlock>().UsePreviousBlockData().
            //    SetProperty(x => x.EquipmentID).
            //        WithValueFromBlockIndex("GetEquipmentListTestBlock").
            //            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[1].ID).

            //AddBlock<DeleteFromQueueTestBlock>().UsePreviousBlockData().
            //    SetDescription("Delete Request From Queue Test Block").

                ExecuteFlow();
        }

        
        [TestMethod]
        public void ProgramActions_SendDownloadRequest_ToAllPreActive_2Piece_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(47). // 47-> 2TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_2TRACK).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(status_Pre_Active).
                    SetProperty(x => x.ProgramType).WithValue(twoP_program).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").
                  
                AddBlock<SendDownloadRequestToAllPreActiveTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_SendVibrationRequest_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMessageGetQueueRequestBlock>().
                    SetDescription("Create Get Queue Request Block").
                    SetProperty(x => x.StartRowIndex).WithValue(0).
                    SetProperty(x => x.MaximumRows).WithValue(100).
                    SetProperty(x => x.filterID).WithValue(46). // 46-> 1TRACK
                    SetProperty(x => x.resultCode).WithValue(Queue0.EnmResultCode.REQ_PRG_1TRACKR).

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.Limit).WithValue(100).
                    SetProperty(x => x.ProgramStatus).WithValue(status_Active).
                    SetProperty(x => x.ProgramType).WithValue(oneP_program).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<GetOneActiveOffender>().UsePreviousBlockData().
                    SetDescription("Gets one active offender").

                AddBlock<CreateEntMsgSendVibrationRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Vibration Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOneActiveOffender").
                            FromDeepProperty<GetOneActiveOffender>(x => x.Offender.ID).

                AddBlock<SendVibrationRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                    WithValueFromBlockIndex("GetOneActiveOffender").
                        FromDeepProperty<GetOneActiveOffender>(x => x.Offender.EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMessageGetQueueRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Queue Request Block").

                AddBlock<GetQueueTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Queue Test Block").

                AddBlock<SendVibrationRequestAssertBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Assert Block").
                    SetProperty(x => x.EntMsgSendVibrationRequest).
                        WithValueFromBlockIndex("CreateEntMsgSendVibrationRequestBlock").
                            FromDeepProperty<CreateEntMsgSendVibrationRequestBlock>(x => x.EntMsgSendVibrationRequest).
                    SetProperty(x => x.GetQueueResponse).
                        WithValueFromBlockIndex("GetQueueTestBlock").
                            FromDeepProperty<GetQueueTestBlock>(x => x.GetQueueResponse).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_SendEOSRequest_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).//WithValue("35611112").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    //SetProperty(x => x.Rule).WithValue(/*"Device_Tamper-first_violation;*/"Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_SendUploadRequest_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211122").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<CreateEntMsgSendUploadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendUploadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_SendMessageRequest_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Two_Piece_3G).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_TwoPiece_Generation_6.ToString()).//WithValue("35611121").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<CreateEntMsgSendMessageRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Message).WithValue("Send Message Request Test").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendMessageRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(20).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_SuspendReactivateProgram_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211123").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).WithValue(null).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_GetSuspendProgramOptions_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211124").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<GetSuspendProgramOptionsTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Suspend Program Options Test Block").

                AddBlock<CreateEntMsgSuspendProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).
                        WithValueFromBlockIndex("GetSuspendProgramOptionsTestBlock").
                            FromPropertyName(EnumPropertyName.SuspendReason).

                AddBlock<SuspendProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgGetProgramSuspensionsDataRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgGetProgramSuspensionsDataRequest Block").
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.SuspendID).
                        WithValueFromBlockIndex("SuspendProgramRequestTestBlock").
                            FromDeepProperty<SuspendProgramRequestTestBlock>(x => x.SuspendID).

                AddBlock<GetProgramSuspensionsDataRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Program Suspensions Data Request Test Block").

                AddBlock<GetProgramSuspensionsDataAssertBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<CreateEntMsgReactivateProgramRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create EntMsgSuspendProgramRequest Block").
                    SetProperty(x => x.Comment).WithValue(null).
                    SetProperty(x => x.Reason).
                        WithValueFromBlockIndex("GetSuspendProgramOptionsTestBlock").
                            FromPropertyName(EnumPropertyName.ReactivateReason).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ReactivateProgramRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Suspend Program Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_GetEndOfServiceOptions_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211120").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    //SetProperty(x => x.ProviderID).WithValue(2).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<GetEndOfServiceOptionsTestBlock>().
                    SetDescription("Get End Of Service Options Test Block").

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).
                        WithValueFromBlockIndex("GetEndOfServiceOptionsTestBlock").
                             FromDeepProperty<GetEndOfServiceOptionsTestBlock>(x => x.EntMsgGetEndOfServiceOptionsResponse.Options[0].Code).
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_GetActionsListIsNotEmpty_Success()
        {
            TestingFlow.

                AddBlock<GetActionsListTestBlock>().
                    SetDescription("Get Actions List Test Block").

                AddBlock<GetActionsListIsNotEmptyAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Actions List Is Not Empty Assert Block").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_GetActionsListCheckValues_Success()
        {
            TestingFlow.

                AddBlock<GetActionsListTestBlock>().
                    SetDescription("Get Actions List Test Block").

                AddBlock<GetActionsListCheckValuesAssertBlock>().UsePreviousBlockData().
                    SetDescription("Get Actions List Is Not Empty Assert Block").

                ExecuteFlow();
        }


        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_GetEndOfServiceData_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).//WithValue("34211118").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(5).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send Manual EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").

                AddBlock<SleepTestingBlock>().
                    SetProperty(x => x.SleepDurationSeconds).WithValue(10).

                AddBlock<CreateEntMsgGetEndOfServiceDataRequestBlock>().
                    SetDescription("Create Ent Msg Get End Of Service Data Request Block").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetEndOfServiceDataTestBlock>().UsePreviousBlockData().
                     SetDescription("Get End Of Service Data Test Block").

                AddBlock<GetEndOfServiceDataAssertBlock>().UsePreviousBlockData().
                     SetDescription("Get End Of Service Data Assert Block").

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }

        [Ignore]
        [TestCategory(CategoriesNames.ProgramActions_Service)]
        [TestMethod]
        public void ProgramActions_ClearCurfewUnitTamper_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetName("One_Piece_GPS_RF").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("34311111").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddCellularDataRequestBlock>().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.ProviderID).WithValue(2).
                    SetProperty(x => x.VoicePrefixPhone).WithValue("97254").
                    SetProperty(x => x.VoicePhoneNumber).WithValue("1111118").
                    SetProperty(x => x.DataPrefixPhone).WithValue("97254").
                    SetProperty(x => x.DataPhoneNumber).WithValue("1111118").

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create EntMsgAddEquipmentRequest type Beacon").
                    SetName("Beacon").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(Equipment0.EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(Equipment0.EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(Equipment0.EnmModemType.NotDefined).
                    SetProperty(x => x.ProtocolType).WithValue(Equipment0.EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue("10000").

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.FirstName).WithValue("Auto_CLE").
                    SetProperty(x => x.RefID).WithValue("Auto_CLE").
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("One_Piece_GPS_RF").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("Beacon").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Send Download Request").
                    SetProperty(x => x.Immediate).WithValue(true).
                    SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(true).

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("One_Piece_GPS_RF").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                    SetProperty(x => x.Rule).WithValue("Home_Unit_Tamper-violation").

                AddBlock<CreateEntMsgClearCurfewUnitTamperRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Ent Msg Clear Curfew Unit Tamper Request Block").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<ClearCurfewUnitTamperRequestTestBlock>().UsePreviousBlockData().
                    SetDescription("Clear Curfew Unit Tamper Request Test Block").

                AddBlock<CreateEntMsgSendEndOfServiceRequest>().UsePreviousBlockData().
                    SetDescription("Create Send End Of Service Request").
                    SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EndOfServiceType).WithValue(ProgramActions0.EnmEOSType.Default).
                    SetProperty(x => x.EndOfServiceCode).WithValue("MEOS").
                    SetProperty(x => x.Comment).WithValue("Send EOS Request Test").

                AddBlock<SendEndOfServiceTestBlock>().UsePreviousBlockData().
                    SetDescription("Send End Of Service Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.Rule).WithValue("Device_Tamper-first_violation;Device_Tamper-restore").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                            FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                     SetDescription("Create Get Offenders Request").
                     SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("AddOffenderTestBlock").
                             FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                     SetDescription("Get Offenders Test Block").

                AddBlock<IsActiveAssertBlock>().UsePreviousBlockData().
                     SetDescription("Is Active Assert Block").
                     SetProperty(x => x.Expected).WithValue(false).

                ExecuteFlow();
        }

    }
}
