﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.ConfigurationBlocks;
using TestBlocksLib.ConfigurationBlocks;
using AssertBlocksLib.ConfigurationAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using TestBlocksLib.ProgramActions;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using Common.Categories;
using TestBlocksLib.DevicesBlocks;


#region API refs
using Configuration0 = TestingFramework.Proxies.EM.Interfaces.Configuration12_0;
using Configuration1 = TestingFramework.Proxies.EM.Interfaces.Configuration3_10;
using Configuration2 = TestingFramework.Proxies.EM.Interfaces.Configuration3_9;

#endregion

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
using Equipment0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.OffendersAsserts;
#endregion

namespace TestingFramework.UnitTests.ConfigurationSvcTests
{

        [TestClass]
        public class ConfigurationSvcTests : BaseUnitTest
    {
            public static int CURRENT_VERSION_NUMBER = -1;
            public static int PLANNED_VERSION_NUMBER = -2;
            Offenders0.EnmProgramStatus[] status = { Offenders0.EnmProgramStatus.Active };
            Offenders0.EnmProgramType[] oneP_program = { Offenders0.EnmProgramType.One_Piece };
            Offenders0.EnmProgramType[] twoP_program = { Offenders0.EnmProgramType.Two_Piece };

            public int OffenderId4Tests { get; set; }
            public int OffenderId4RFTests { get; set; }
            public int RuleID4Tests { get; set; }
            public ConfigurationSvcTests()
            {
                OffenderId4RFTests = 6160;
                RuleID4Tests = 100460;

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetGPSRuleConfiguration_sucsess()
            {
                TestingFlow.
                    AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                            SetDescription("Create request to get offenders list").
                            SetProperty(x => x.Limit).WithValue(1).
                            SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                            SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                            SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().UsePreviousBlockData().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                         SetProperty(x => x.ProgramType).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).   
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").
                        
                    AddBlock<GetGPSRuleConfigurationListAssertBlock>().

                ExecuteFlow();
            }
            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetGPSRuleConfiguration_1_sucsess()
            {
                TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders1.EnmProgramType[] { Offenders1.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_1>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_1_BeforeUpdate").


                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_1>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_1>().UsePreviousBlockData().
                    SetDescription("Get Configuration GPS Rule for offender").
            //assert for test
           AddBlock<GetGPSRuleConfigurationListAssertBlock_1>().

                ExecuteFlow();
            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetGPSRuleConfiguration_2_sucsess()
            {
                TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders2.EnmProgramType[] { Offenders2.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_2>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_2_BeforeUpdate").
                    

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_2>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                      
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").
               
               AddBlock<GetGPSRuleConfigurationListAssertBlock_2>().

                ExecuteFlow();
            }


            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_UpdateGPSRuleConfiguration_sucsess()
            {

                TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                    AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                        SetProperty(x => x.ProgramType).
                           WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).       
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).
                        SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).


                    AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock>().UsePreviousBlockData().
                          SetDescription("Update GPS Rule Configuration test block").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                AddBlock<UpdateGPSRuleConfigurationRequestAssertBlock>().

                ExecuteFlow();
            }
         
            [TestMethod]
            public void ConfigurationSvcTests_UpdateGPSRuleConfiguration_1_sucsess()
            {
                TestingFlow.

                  AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders1.EnmProgramType[] { Offenders1.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_1>().UsePreviousBlockData().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                         SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").
                        SetName("GetGPSRuleConfigurationListRequestTestBlocks_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).
                        SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).


                    AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock_1>().UsePreviousBlockData().
                          SetDescription("Update GPS Rule Configuration test block").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_1>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                    AddBlock<UpdateGPSRuleConfigurationRequestAssertBlock_1>().

                          ExecuteFlow();
            }
            
            [TestMethod]
            public void ConfigurationSvcTests_UpdateGPSRuleConfiguration_2_sucsess()
            {
                TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders2.EnmProgramType[] { Offenders2.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_2>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock_2").
                                FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                    AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock_2").
                                FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).
                        SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GPSDisappearTime.ToString()).

                    AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock_2>().UsePreviousBlockData().
                          SetDescription("Update GPS Rule Configuration test block").

                    AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_2>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                    AddBlock<UpdateGPSRuleConfigurationRequestAssertBlock_2>().

                          ExecuteFlow();
            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_ResetGPSRuleConfiguration_sucsess()
            {
                TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).WithValue(OffenderId4Tests).
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").
                        SetName("GetGPSRuleConfigurationListRequestTestBlocks_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                         SetProperty(x => x.ProgramType).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).
                        SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GraceTime.ToString()).

                    AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock>().UsePreviousBlockData().
                          SetDescription("Update GPS Rule Configuration test block").


                     AddBlock<CreateEntMsgResetGPSRuleConfigurationRequest>().
                        SetDescription("Create Reset GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID). 
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).

                          AddBlock<ResetGPSRuleConfigurationRequestTestBlock>().UsePreviousBlockData().
                          SetDescription("Reset GPS Rule Configuration Request").

                          AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest>().
                             SetDescription("Create Get GPS Rule Configuration Reuest").
                             SetProperty(x => x.OffenderID).
                                WithValueFromBlockIndex("GetOffendersTestBlock").
                                  FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                          
                             SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                          AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                             SetDescription("Get Configuration GPS Rule for offender").


                          AddBlock<GetGPSRuleConfigurationListRequestTestBlocks>().UsePreviousBlockData().
                             SetDescription("Get Configuration GPS Rule for offender").

                               AddBlock<GetGPSRuleConfigurationListIsDefaultAssertBlock>().UsePreviousBlockData().
                               SetDescription("Assert for Get GPS Rule Configuration List Is Default Assert Block>").
                          ExecuteFlow();

            }

            
            [TestMethod]
            public void ConfigurationSvcTests_ResetGPSRuleConfiguration_1_sucsess()
            {
                TestingFlow.

                AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders1.EnmProgramType[] { Offenders1.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_1>().UsePreviousBlockData().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                      
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                         AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                     
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).
                        SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GraceTime.ToString()).

                    AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock_1>().UsePreviousBlockData().
                          SetDescription("Update GPS Rule Configuration test block").

                     AddBlock<CreateEntMsgResetGPSRuleConfigurationRequest_1>().

                        SetDescription("Create Reset GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                          WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).

                     AddBlock<ResetGPSRuleConfigurationRequestTestBlock_1>().UsePreviousBlockData().
                     SetDescription("Reset GPS Rule Configuration Request").

                       AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_1>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                          WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID). 
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                     AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                          AddBlock<GetGPSRuleConfigurationListIsDefaultAssertBlock_1>().
                          SetDescription("Assert for Get GPS Rule Configuration List Is Default Assert Block>").

                          ExecuteFlow();

            }

            
            [TestMethod]
            public void ConfigurationSvcTests_ResetGPSRuleConfiguration_2_sucsess()
            {
                TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders2.EnmProgramType[] { Offenders2.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_2>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                          WithValueFromBlockIndex("GetOffendersTestBlock_2").
                              FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").
                        SetName("GetGPSRuleConfigurationListRequestTestBlocks_BeforeUpdate").

                         AddBlock<CreateEntMsgUpdateGPSRuleConfigurationRequest_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock_2").
                               FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).         
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).
                        SetProperty(x => x.GraceTime).WithValueFromDifferentDataGenerator(EnumPropertyType.GraceTime.ToString()).
                    

                    AddBlock<UpdateGPSRuleConfigurationRequestTestsBlock_2>().UsePreviousBlockData().
                          SetDescription("Update GPS Rule Configuration test block").

                     AddBlock<CreateEntMsgResetGPSRuleConfigurationRequest_2>().

                        SetDescription("Create Reset GPS Rule Configuration").
                        SetProperty(x => x.OffenderID).
                           WithValueFromBlockIndex("GetOffendersTestBlock_2").
                              FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.RuleID).WithValue(RuleID4Tests).

                     AddBlock<ResetGPSRuleConfigurationRequestTestBlock_2>().UsePreviousBlockData().
                     SetDescription("Reset GPS Rule Configuration Request").

                     AddBlock<CreateEntMsgGetGPSRuleConfigurationListRequest_2>().
                        SetDescription("Create Get GPS Rule Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                           WithValueFromBlockIndex("GetOffendersTestBlock_2").
                              FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                     AddBlock<GetGPSRuleConfigurationListRequestTestBlocks_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration GPS Rule for offender").

                     AddBlock<GetGPSRuleConfigurationListIsDefaultAssertBlock_2>().UsePreviousBlockData().
                          SetDescription("Assert for Get GPS Rule Configuration List Is Default Assert Block>").

                          ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetUpdateGPSOffenderConfigurationData_sucsess()
            {
                TestingFlow.

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                           WithValueFromBlockIndex("GetOffendersTestBlock").
                              FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update GPS offender Configuration").
                        SetProperty(x => x.BaseUnitRange).WithValue(Configuration0.EnmRFRange.Maximum).
                        SetProperty(x => x.OffenderID).
                           WithValueFromBlockIndex("GetOffendersTestBlock").
                              FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.ReceiverRange).WithValue(Configuration0.EnmRFRange.Short).

                    AddBlock<UpdateGPSOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("").

                    AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                           WithValueFromBlockIndex("GetOffendersTestBlock").
                                FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_AfterUpdate").

                    AddBlock<UpdateGPSOffenderConfigurationDataAssertBlock>().
                    SetProperty(x => x.GetOffenderConfigurationDataDefaultResponse).
                       WithValueFromBlockIndex("GetOffenderConfigurationDataTestBlock_BeforeUpdate").
                          FromDeepProperty<GetOffenderConfigurationDataTestBlock>(x => x.GetOffenderConfigurationDataResponse).

                    ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetUpdateGPSOffenderConfigurationData_1_sucsess()
            {
                TestingFlow.

                      AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders1.EnmProgramType[] { Offenders1.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_1>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                           WithValueFromBlockIndex("GetOffendersTestBlock_1").
                              FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_1_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock_1>().
                        SetDescription("Create Update GPS offender Configuration").
                        SetProperty(x => x.BaseUnitActive).WithValue(false).
                        SetProperty(x => x.BaseUnitRange).WithValue(Configuration1.EnmRFRange.Maximum).
                        SetProperty(x => x.DetectOtherTransmitters).WithValue(false).
                        SetProperty(x => x.DisplayOffenderName).WithValue(false).
                        SetProperty(x => x.DefaultMessage).WithValue("automation").
                        SetProperty(x => x.GPSDisappearTime).WithValue(180).
                        SetProperty(x => x.GPSProximity).WithValue(450).
                        SetProperty(x => x.GPSProximityBuffer).WithValue(650).
                        SetProperty(x => x.LBSActive).WithValue(false).
                        SetProperty(x => x.LBSForViolations).WithValue(false).
                        SetProperty(x => x.LBSNoPositionTimeout).WithValue(350).
                        SetProperty(x => x.LBSRequestTimeout).WithValue(40).
                        SetProperty(x => x.LBSSamplingRate).WithValue(170).
                        SetProperty(x => x.LandlineActive).WithValue(false).
                        SetProperty(x => x.LandlineCallerIDRequired).WithValue(false).
                        SetProperty(x => x.NormalLoggingRate).WithValue(40).
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.ReceiverRange).WithValue(Configuration1.EnmRFRange.Short).
                        SetProperty(x => x.PassiveMode).WithValue(false).
                        SetProperty(x => x.ReceptionIconsActive).WithValue(false).
                        SetProperty(x => x.ScheduleActive).WithValue(false).
                        SetProperty(x => x.SupportCSD).WithValue(false).
                        SetProperty(x => x.SupportGPRS).WithValue(true).
                        SetProperty(x => x.TimeBetweenUploads).WithValue(80).
                        SetProperty(x => x.ViolationLoggingRate).WithValue(40).
                        SetProperty(x => x.TimeBetweenBaseUnitUploads).WithValue(7).
                        SetProperty(x => x.TransmitterDisappearTime).WithValue(6).


                    AddBlock<UpdateGPSOffenderConfigurationDataTestBlock_1>().UsePreviousBlockData().
                        SetDescription("").

                    AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_1>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                         WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_1_AfterUpdate").

                    AddBlock<UpdateGPSOffenderConfigurationDataAssertBlock_1>().
                    SetProperty(x => x.GetOffenderConfigurationDataDefaultResponse).
                    WithValueFromBlockIndex("GetOffenderConfigurationDataTestBlock_1_BeforeUpdate").
                    FromDeepProperty<GetOffenderConfigurationDataTestBlock_1>(x => x.GetOffenderConfigurationDataResponse).

                    ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetUpdateGPSOffenderConfigurationData_2_sucsess()
            {
                TestingFlow.

                      AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders2.EnmProgramType[] { Offenders2.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_2>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_2_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateGPSOffenderConfigurationDataRequestBlock_2>().
                        SetDescription("Create Update GPS offender Configuration").
                       SetProperty(x => x.BaseUnitActive).WithValue(false).
                        SetProperty(x => x.BaseUnitRange).WithValue(Configuration1.EnmRFRange.Maximum).
                       SetProperty(x => x.DetectOtherTransmitters).WithValue(false).
                       SetProperty(x => x.DisplayOffenderName).WithValue(false).
                       SetProperty(x => x.DefaultMessage).WithValue("automation").
                        SetProperty(x => x.GPSDisappearTime).WithValue(180).
                        SetProperty(x => x.GPSProximity).WithValue(450).
                        SetProperty(x => x.GPSProximityBuffer).WithValue(650).
                        SetProperty(x => x.LBSActive).WithValue(false).
                        SetProperty(x => x.LBSForViolations).WithValue(false).
                        SetProperty(x => x.LBSNoPositionTimeout).WithValue(350).
                        SetProperty(x => x.LBSRequestTimeout).WithValue(40).
                        SetProperty(x => x.LBSSamplingRate).WithValue(170).
                        SetProperty(x => x.LandlineActive).WithValue(false).
                        SetProperty(x => x.LandlineCallerIDRequired).WithValue(false).
                        SetProperty(x => x.NormalLoggingRate).WithValue(40).
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.ReceiverRange).WithValue(Configuration2.EnmRFRange.Short).
                        SetProperty(x => x.PassiveMode).WithValue(false).
                        SetProperty(x => x.ReceptionIconsActive).WithValue(false).
                        SetProperty(x => x.ScheduleActive).WithValue(false).
                        SetProperty(x => x.SupportCSD).WithValue(false).
                        SetProperty(x => x.SupportGPRS).WithValue(true).
                        SetProperty(x => x.TimeBetweenUploads).WithValue(80).
                        SetProperty(x => x.ViolationLoggingRate).WithValue(40).
                        SetProperty(x => x.TimeBetweenBaseUnitUploads).WithValue(7).
                        SetProperty(x => x.TransmitterDisappearTime).WithValue(6).

                    AddBlock<UpdateGPSOffenderConfigurationDataTestBlock_2>().UsePreviousBlockData().
                        SetDescription("").

                    AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_2>().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_2_AfterUpdate").

                    AddBlock<UpdateGPSOffenderConfigurationDataAssertBlock_2>().
                        SetProperty(x => x.GetOffenderConfigurationDataDefaultResponse).
                        WithValueFromBlockIndex("GetOffenderConfigurationDataTestBlock_2_BeforeUpdate").
                        FromDeepProperty<GetOffenderConfigurationDataTestBlock_2>(x => x.GetOffenderConfigurationDataResponse).

                    ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetUpdateHandlingProcedure_sucsess()
            {
                TestingFlow.

                    AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Get handling procedure Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                        SetDescription("Get handling procedure test").
                        SetName("GetHandlingProcedureTestBlock_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create update handling procedure request").
                        SetProperty(x => x.AutoEmailNotificationToAgency).WithValue(true).
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<UpdateHandlingProcedureTestBlock>().UsePreviousBlockData().
                        SetDescription("Update handling procedure test").

                    AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock>().
                        SetDescription("Create Get handling procedure Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<GetHandlingProcedureTestBlock>().UsePreviousBlockData().
                        SetDescription("Get handling procedure test").
                        SetName("GetHandlingProcedureTestBlock_AfterUpdate").

                    AddBlock<UpdateHandlingProcedureAssertBlock>().
                        SetDescription("").


                ExecuteFlow();
            }
            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_UpdateRFOffenderConfigurationDataRequest_sucsess()
            {
                TestingFlow.

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders0.EnmProgramStatus[] { Offenders0.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders0.EnmProgramType[] { Offenders0.EnmProgramType.E4_Dual }).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock>().UsePreviousBlockData().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                        FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_BeforeUpdate").

                        AddBlock<CreateEntMsgUpdateRFOffenderConfigurationDataRequest>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                           SetDescription("Create Update RF Offender Configuration Data Request").
                           
                           SetProperty(x => x.OffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).

                            SetProperty(x => x.ProgramType).
                            WithValueFromBlockIndex("GetOffendersTestBlock").
                             FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                           SetProperty(x => x.GraceBeforeVoiceTestStart).WithValue(10).
                           SetProperty(x => x.GraceAfterVoiceTestEnd).WithValue(10).
                           SetProperty(x => x.NumberOfPhrases).WithValue(3).
                           SetProperty(x => x.ReceiverRange).WithValue(3).
                           SetProperty(x => x.CommunicationMethodForVoiceTests).WithValue(1).
                           SetProperty(x => x.SupportCSD).WithValue(false).
                           SetProperty(x => x.SupportGPRS).WithValue(true).
                           SetProperty(x => x.SupportLandline).WithValue(true).
                           SetProperty(x => x.CSDPriority).WithValue(0).
                           SetProperty(x => x.GPRSPriority).WithValue(1).
                           SetProperty(x => x.LandlinePriority).WithValue(2).
                           SetProperty(x => x.CallerIDRequired).WithValue(true).
                           SetProperty(x => x.SupportSMSCallback).WithValue(true).
                           SetProperty(x => x.VerifyAfterEnroll).WithValue(false).

                       AddBlock<UpdateRFOffenderConfigurationDataRequestTestsBlock>().UsePreviousBlockData().
                           SetDescription("Update RF Offender Configuration DataRequest TestsBlock").

                       AddBlock<CreateEntMsgGetOffenderConfigurationDataRequest>().UsePreviousBlockData().
                        SetDescription("Create EntMsg Get Offender Configuration Data Request").
                          SetProperty(x => x.OffenderID).
                            WithValueFromBlockIndex("GetOffendersTestBlock").
                              FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].ID).       
                           SetProperty(x => x.Version).WithValue(PLANNED_VERSION_NUMBER).

                       AddBlock<GetOffenderConfigurationDataRequestTestBlock>().UsePreviousBlockData().
                        SetDescription("Get Offender Configuration Data Request TestBlock").

                       AddBlock<GetRFOffenderConfigurationDataRequestAssertBlock>().
                          SetDescription("Get RF Offender Configuration Data Request AssertBlock").

                        ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetUpdateHandlingProcedure_1_sucsess()
            {
                TestingFlow.

                    AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders1.EnmProgramType[] { Offenders1.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock_1>().
                        SetDescription("Create Get handling procedure Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<GetHandlingProcedureTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get handling procedure test").
                        SetName("GetHandlingProcedureTestBlock_1_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock_1>().
                        SetDescription("Create update handling procedure request").
                        SetProperty(x => x.AutoEmailNotificationToAgency).WithValue(true).
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<UpdateHandlingProcedureTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Update handling procedure test").

                    AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock_1>().
                        SetDescription("Create Get handling procedure Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<GetHandlingProcedureTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get handling procedure test").
                        SetName("GetHandlingProcedureTestBlock_1_AfterUpdate").

                    AddBlock<UpdateHandlingProcedureAssertBlock_1>().
                        SetDescription("").


                ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_GetUpdateHandlingProcedure_2_sucsess()
            {
                TestingFlow.

                    AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders2.EnmProgramType[] { Offenders2.EnmProgramType.One_Piece_RF }).

                    AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                    AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock_2>().
                        SetDescription("Create Get handling procedure Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<GetHandlingProcedureTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get handling procedure test").
                        SetName("GetHandlingProcedureTestBlock_2_BeforeUpdate").

                    AddBlock<CreateEntMsgUpdateHandlingProcedureRequestBlock_2>().
                        SetDescription("Create update handling procedure request").
                        SetProperty(x => x.AutoEmailNotificationToAgency).WithValue(true).
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<UpdateHandlingProcedureTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Update handling procedure test").

                    AddBlock<CreateEntMsgGetHandlingProcedureRequestBlock_2>().
                        SetDescription("Create Get handling procedure Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).

                    AddBlock<GetHandlingProcedureTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get handling procedure test").
                        SetName("GetHandlingProcedureTestBlock_2_AfterUpdate").

                    AddBlock<UpdateHandlingProcedureAssertBlock_2>().
                        SetDescription("").


                ExecuteFlow();

            }



            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_UpdateRFOffenderConfigurationDataRequest_1_sucsess()
            {
                TestingFlow.

                 AddBlock<CreateEntMsgGetOffendersListRequestBlock_1>().
                        SetDescription("Create request to get offenders list").
                        SetProperty(x => x.Limit).WithValue(1).
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders1.EnmProgramStatus[] { Offenders1.EnmProgramStatus.Active }).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders1.EnmProgramType[] { Offenders1.EnmProgramType.E4_Dual}).

                    AddBlock<GetOffendersTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_1>().UsePreviousBlockData().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(PLANNED_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_1>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_1_BeforeUpdate").

                     AddBlock<CreateEntMsgUpdateRFOffenderConfigurationDataRequest_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update RF Offender Configuration Data Request").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).

                         SetProperty(x => x.ProgramType).
                            WithValueFromBlockIndex("GetOffendersTestBlock_1").
                             FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                           SetProperty(x => x.GraceBeforeVoiceTestStart).WithValue(10).
                           SetProperty(x => x.GraceAfterVoiceTestEnd).WithValue(10).
                           SetProperty(x => x.NumberOfPhrases).WithValue(3).
                           SetProperty(x => x.ReceiverRange).WithValue(3).
                           SetProperty(x => x.CommunicationMethodForVoiceTests).WithValue(1).
                           SetProperty(x => x.SupportCSD).WithValue(false).
                           SetProperty(x => x.SupportGPRS).WithValue(true).
                           SetProperty(x => x.SupportLandline).WithValue(true).
                           SetProperty(x => x.CSDPriority).WithValue(0).
                           SetProperty(x => x.GPRSPriority).WithValue(1).
                           SetProperty(x => x.LandlinePriority).WithValue(2).
                           SetProperty(x => x.CallerIDRequired).WithValue(true).
                           SetProperty(x => x.SupportSMSCallback).WithValue(true).
                           SetProperty(x => x.VerifyAfterEnroll).WithValue(false).

                         AddBlock<UpdateRFOffenderConfigurationDataRequestTestsBlock_1>().UsePreviousBlockData().
                          SetDescription("Update RF Offender Configuration DataRequest TestsBlock").

                        AddBlock<CreateEntMsgGetOffenderConfigurationDataRequest_1>().UsePreviousBlockData().
                         SetDescription("Create EntMsg Get Offender Configuration Data Request").
                         SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_1").
                        FromDeepProperty<GetOffendersTestBlock_1>(x => x.GetOffendersResponse.OffendersList[0].ID).
                         SetProperty(x => x.Version).WithValue(PLANNED_VERSION_NUMBER).

                          AddBlock<GetOffenderConfigurationDataRequestTestBlock_1>().UsePreviousBlockData().
                          SetDescription("Get Offender Configuration Data Request TestBlock").

                          AddBlock<GetRFOffenderConfigurationDataRequestAssertBlock_1>().
                          SetDescription("Get RF Offender Configuration Data Request AssertBlock").
                        ExecuteFlow();

            }

            [TestCategory(CategoriesNames.Configuration_Service)]
            [TestMethod]
            public void ConfigurationSvcTests_UpdateRFOffenderConfigurationDataRequest_2_sucsess()
            {
                TestingFlow.
                AddBlock<CreateEntMsgGetOffendersListRequestBlock_2>().
                        SetDescription("Create request to get offenders list").
                       
                        SetProperty(x => x.ProgramStatus).WithValue(new Offenders2.EnmProgramStatus[] { Offenders2.EnmProgramStatus.Active}).
                        SetProperty(x => x.ProgramType).WithValue(new Offenders2.EnmProgramType[] { Offenders2.EnmProgramType.E4_Dual}).

                    AddBlock<GetOffendersTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Request to get offenders list").

                     AddBlock<CreateEntMsgGetOffenderConfigurationDataRequestBlock_2>().UsePreviousBlockData().
                        SetDescription("Create Get offender Configuration Reuest").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.VersionNumber).WithValue(CURRENT_VERSION_NUMBER).

                    AddBlock<GetOffenderConfigurationDataTestBlock_2>().UsePreviousBlockData().
                        SetDescription("Get Configuration for offender").
                        SetName("GetOffenderConfigurationDataTestBlock_2_BeforeUpdate").

                     AddBlock<CreateEntMsgUpdateRFOffenderConfigurationDataRequest_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                        SetDescription("Create Update RF Offender Configuration Data Request").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).

                          SetProperty(x => x.ProgramType).
                            WithValueFromBlockIndex("GetOffendersTestBlock_2").
                             FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ProgramType).

                           SetProperty(x => x.GraceBeforeVoiceTestStart).WithValue(10).
                           SetProperty(x => x.GraceAfterVoiceTestEnd).WithValue(10).
                           SetProperty(x => x.NumberOfPhrases).WithValue(3).
                           SetProperty(x => x.ReceiverRange).WithValue(3).
                           SetProperty(x => x.CommunicationMethodForVoiceTests).WithValue(1).
                           SetProperty(x => x.SupportCSD).WithValue(false).
                           SetProperty(x => x.SupportGPRS).WithValue(true).
                           SetProperty(x => x.SupportLandline).WithValue(true).
                           SetProperty(x => x.CSDPriority).WithValue(0).
                           SetProperty(x => x.GPRSPriority).WithValue(1).
                           SetProperty(x => x.LandlinePriority).WithValue(2).
                           SetProperty(x => x.CallerIDRequired).WithValue(true).
                           SetProperty(x => x.SupportSMSCallback).WithValue(true).
                           SetProperty(x => x.VerifyAfterEnroll).WithValue(false).

                         AddBlock<UpdateRFOffenderConfigurationDataRequestTestsBlock_2>().UsePreviousBlockData().
                          SetDescription("Update RF Offender Configuration DataRequest TestsBlock").

                          AddBlock<CreateEntMsgGetOffenderConfigurationDataRequest_2>().
                        SetDescription("Create EntMsg Get Offender Configuration Data Request").
                        SetProperty(x => x.OffenderID).
                        WithValueFromBlockIndex("GetOffendersTestBlock_2").
                        FromDeepProperty<GetOffendersTestBlock_2>(x => x.GetOffendersResponse.OffendersList[0].ID).
                        SetProperty(x => x.Version).WithValue(PLANNED_VERSION_NUMBER).

                          AddBlock<GetOffenderConfigurationDataRequestTestBlock_2>().UsePreviousBlockData().
                          SetDescription("Get Offender Configuration Data Request TestBlock").

                           AddBlock<GetRFOffenderConfigurationDataRequestAssertBlock_2>().
                          SetDescription("Get RF Offender Configuration Data Request AssertBlock").
                        ExecuteFlow();

            }
        }
    }



   

