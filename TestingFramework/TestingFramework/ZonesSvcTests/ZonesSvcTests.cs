﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.EquipmentBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestingFramework.Proxies.EM.Interfaces.Equipment;
using AssertBlocksLib.EquipmentAsserts;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Enum;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.ZonesBlocks;
using TestBlocksLib.ZonesBlocks;
using AssertBlocksLib.OffendersAsserts;
using Common.Categories;

#region API Offenders refs
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Offenders1 = TestingFramework.Proxies.EM.Interfaces.Offenders3_10;
using Offenders2 = TestingFramework.Proxies.EM.Interfaces.Offenders3_9;
#endregion

#region API Zones refs
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
using Zones1 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;
using Zones2 = TestingFramework.Proxies.EM.Interfaces.Zones3_9;
#endregion

namespace TestingFramework.UnitTests.ZonesSvcTests
{
    [TestClass]
    public class ZonesSvcTests : BaseUnitTest
    {
        #region Tests Parameters for Zones0

        Zones0.EntPoint Point_AddCircularZone = new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones0.EntPoint[] Points_AddPolygonZone = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones0.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones0.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones0.EntPoint[] Points_UpdatePolygonZone = new Zones0.EntPoint[3] {
                            new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones0.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones0.EntPoint() { LAT = 31.105820, LON = 33.834568 } };


        #endregion


        #region Tests Parameters for Zones1

        Zones1.EntPoint Point_AddCircularZone_1 = new Zones1.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones1.EntPoint[] Points_AddPolygonZone_1 = new Zones1.EntPoint[3] {
                            new Zones1.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones1.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones1.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones1.EntPoint[] Points_UpdatePolygonZone_1 = new Zones1.EntPoint[3] {
                            new Zones1.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones1.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones1.EntPoint() { LAT = 31.105820, LON = 33.834568 } };

        #endregion


        #region Tests Parameters for Zones2

        Zones2.EntPoint Point_AddCircularZone_2 = new Zones2.EntPoint() { LAT = 32.1063605, LON = 34.8369437 };

        Zones2.EntPoint[] Points_AddPolygonZone_2 = new Zones2.EntPoint[3] {
                            new Zones2.EntPoint() { LAT = 32.107120, LON = 34.834436 },
                            new Zones2.EntPoint() { LAT = 32.106812, LON = 34.835636 },
                            new Zones2.EntPoint() { LAT = 32.105820, LON = 34.834568 } };

        Zones2.EntPoint[] Points_UpdatePolygonZone_2 = new Zones2.EntPoint[3] {
                            new Zones2.EntPoint() { LAT = 31.107120, LON = 33.834436 },
                            new Zones2.EntPoint() { LAT = 31.106812, LON = 33.835636 },
                            new Zones2.EntPoint() { LAT = 31.105820, LON = 33.834568 } };

        #endregion

        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_AddCircularZone_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                //SetProperty(x => x.RealRadius).WithValue(50).
                //SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_AddCircularZone_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).

                AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_AddCircularZone_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                    //SetProperty(x => x.RealRadius).WithValue(50).

                AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_AddPolygonZone_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_AddPolygonZone_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Points).WithValue(Points_AddPolygonZone_1).

                AddBlock<AddPolygonZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_AddPolygonZone_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Points).WithValue(Points_AddPolygonZone_2).

                AddBlock<AddPolygonZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_DeleteZone_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityType).
                        WithValueFromBlockIndex("CreateMsgAddInclusionCircularZoneRequestBlock").
                            FromDeepProperty<CreateMsgAddInclusionCircularZoneRequestBlock>(x => x.EntityType).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_DeleteZone_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).
                    SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_1").
                            FromDeepProperty<AddCircularZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityType).
                        WithValueFromBlockIndex("CreateMsgAddExclusionCircularZoneRequestBlock_1").
                            FromDeepProperty<CreateMsgAddExclusionCircularZoneRequestBlock_1>(x => x.EntityType).

                AddBlock<DeleteZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_DeleteZone_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                    SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).

                AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Delete Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_2").
                            FromDeepProperty<AddCircularZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityType).
                        WithValueFromBlockIndex("CreateMsgAddExclusionCircularZoneRequestBlock_2").
                            FromDeepProperty<CreateMsgAddExclusionCircularZoneRequestBlock_2>(x => x.EntityType).

                AddBlock<DeleteZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Delete Zone test block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_UpdateGetCircularZone_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                //SetProperty(x => x.RealRadius).WithValue(50).
                //SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone).

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateZoneRequestAssertBlock>().
                    SetDescription("Update Zone Request Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_UpdateGetCircularZone_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).

                AddBlock<AddCircularZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateExclusionCircularZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_1").
                            FromDeepProperty<AddCircularZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_1).
                //SetProperty(x => x.RealRadius).WithValue(100).
                //SetProperty(x => x.BufferRadius).WithValue(50).

                AddBlock<UpdateCircularZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateZoneRequestAssertBlock_1>().
                    SetDescription("Update Zone Request Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_UpdateGetCircularZone_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). 

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Work").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).

                AddBlock<AddCircularZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateExclusionCircularZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock_2").
                            FromDeepProperty<AddCircularZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Point).WithValue(Point_AddCircularZone_2).
                //SetProperty(x => x.RealRadius).WithValue(100).
                //SetProperty(x => x.BufferRadius).WithValue(50).

                AddBlock<UpdateCircularZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock_2>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateZoneRequestAssertBlock_2>().
                    SetDescription("Update Zone Request Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_UpdateGetPolygonZone_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders0.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Polygon Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Egypt").
                    SetProperty(x => x.Points).WithValue(Points_UpdatePolygonZone).

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateZoneRequestAssertBlock>().
                    SetDescription("Update Zone Request Assert Block").

            ExecuteFlow();
        }



        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_UpdateGetPolygonZone_1_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_1>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders1.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Points).WithValue(Points_AddPolygonZone_1).

                AddBlock<AddPolygonZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock_1>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Polygon Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock_1").
                            FromDeepProperty<AddPolygonZoneTestBlock_1>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Egypt").
                    SetProperty(x => x.Points).WithValue(Points_UpdatePolygonZone_1).

                AddBlock<UpdatePolygonZoneTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock_1>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_1").
                            FromDeepProperty<AddOffenderTestBlock_1>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones1.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock_1>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateZoneRequestAssertBlock_1>().
                    SetDescription("Update Zone Request Assert Block").

            ExecuteFlow();
        }


        [TestCategory(CategoriesNames.Zones_Service)]
        [TestMethod]
        public void ZonesSvcTests_UpdateGetPolygonZone_2_Success()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock_2>().ShallGeneratePropertiesWithRandomValues().
                    //SetDescription("Create Add Offender Request").
                    //SetProperty(x => x.CityID).WithValue("TLV").
                    //SetProperty(x => x.FirstName).WithValue("Yuval").
                    //SetProperty(x => x.LastName).WithValue("Lahav").
                    //SetProperty(x => x.MiddleName).WithValue("Forza").
                    //SetProperty(x => x.Gender).WithValue(Offenders0.EnmGender.Male). //Male
                    //SetProperty(x => x.Language).WithValue("ENG").
                    //SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    //SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF). //1Piece GPS/RF 
                                                                                                       //SetProperty(x => x.StateID).WithValue("IL").
                                                                                                       //SetProperty(x => x.Street).WithValue("Naharyim").
                                                                                                       //SetProperty(x => x.Title).WithValue(Offenders2.EnmTitle.Mr). // Mr

                AddBlock<AddOffenderTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Yuval's Home").
                    SetProperty(x => x.Points).WithValue(Points_AddPolygonZone_2).

                AddBlock<AddPolygonZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock_2>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Polygon Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock_2").
                            FromDeepProperty<AddPolygonZoneTestBlock_2>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Name).WithValue("Egypt").
                    SetProperty(x => x.Points).WithValue(Points_UpdatePolygonZone_2).

                AddBlock<UpdatePolygonZoneTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock_2>().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock_2").
                            FromDeepProperty<AddOffenderTestBlock_2>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones2.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock_2>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateZoneRequestAssertBlock_2>().
                    SetDescription("Update Zone Request Assert Block").

            ExecuteFlow();
        }
    }
}
