﻿using AssertBlocksLib.OffendersAsserts;
using AssertBlocksLib.ZonesAsserts;
using Common.Categories;
using Common.Enum;
using LogicBlocksLib.EquipmentBlocks;
using LogicBlocksLib.Group;
using LogicBlocksLib.OffendersBlocks;
using LogicBlocksLib.ProgramActions;
using LogicBlocksLib.ZonesBlocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBlocksLib.DevicesBlocks;
using TestBlocksLib.EquipmentBlocks;
using TestBlocksLib.GroupsBlocks;
using TestBlocksLib.OffendersBlocks;
using TestBlocksLib.ProgramActions;
using TestBlocksLib.ZonesBlocks;
using TestingFramework.Proxies.EM.Interfaces.APIExtensions;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.Interfaces;
using Offenders0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones12_1;
//using Zones0 = TestingFramework.Proxies.EM.Interfaces.Zones3_10;

namespace TestingFramework.UnitTests.ZonesSvcTests
{
    [TestClass]
    public class ZonesRegressionTests : BaseUnitTest
    {  

        #region Add circular zone
        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_NoFullSchedule_NoCerfewZone_inclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_NoFullSchedule_NoCerfewZone_exclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_FullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.FullSchedule).WithValue(true). 

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    //SetProperty(x => x.Version).WithValue(0).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_IsCerfewZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").
           
                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_BLP_ForOffender_FullSchedule_1Piece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_Beacon").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_Beacon").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_Beacon").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_Beacon").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_Beacon").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_Beacon").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(true).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_BLP_ForOffender_NotFullSchedule_1Piece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForGroup_NoFullSchedule_NoCerfewZone_inclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetProperty(x => x.GroupID).WithValue(null).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().
                    
                AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForGroup_NoFullSchedule_NoCerfewZone_exclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetProperty(x => x.GroupID).WithValue(null).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().

                    AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForGroup_FullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().

                AddBlock<CreateEntMsgAddGroupRequestBlock>().
                    SetProperty(x => x.GroupName).WithValue("Tracker Group").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                        FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Description).WithValue("New group").

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                            SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_NoFullSchedule_NoCerfewZone_inclusion_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_NoFullSchedule_NoCerfewZone_exclusion_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_FullSchedule_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_NoFullSchedule_NoCerfewZone_inclusion_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_NoFullSchedule_NoCerfewZone_exclusion_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddExclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.BufferRadius).WithValueFromDifferentDataGenerator(EnumPropertyType.BufferRadius.ToString()).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddCircularZone_ForOffender_FullSchedule_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddCircularZoneAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Add polygon zone
        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_NoFullSchedule_NoCerfewZone_inclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_NoFullSchedule_NoCerfewZone_exclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_FullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_IsCerfewZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_NoFullSchedule_NoCerfewZone_inclusion_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_NoFullSchedule_NoCerfewZone_exclusion_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_FullSchedule_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //setproperty(x => x.gracetime).withvalue(1).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_NoFullSchedule_NoCerfewZone_inclusion_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_NoFullSchedule_NoCerfewZone_exclusion_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForOffender_FullSchedule_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForGroup_NoFullSchedule_NoCerfewZone_inclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetProperty(x => x.GroupID).WithValue(null).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().

                AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForGroup_NoFullSchedule_NoCerfewZone_exclusion_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetProperty(x => x.GroupID).WithValue(null).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().

                AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Exclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_ForGroup_FullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetProperty(x => x.GroupID).WithValue(null).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().

                    AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Polygon Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<AddPolygonZoneAssertBlock>().UsePreviousBlockData().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_BLP_ForOffender_FullSchedule_1Piece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                 AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(true).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").
                
                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddPolygonZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_AddPolygonZone_BLP_ForOffender_NotFullSchedule_1Piece()
        {
            TestingFlow.
                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(false).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<AddPolygonZoneAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Delete zone
        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_DeleteZone_ForOffender_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<DeleteZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_DeleteZone_ForOffender_2Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<DeleteZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_DeleteZone_ForOffender_DV()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders0.EnmProgramConcept.Aggressor).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<DeleteZoneAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_DeleteZone_ForGroup_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgGetGroupRequestBlock>().
                    SetProperty(x => x.GroupID).WithValue(null).

                AddBlock<GetGroupsDataTestBlock>().UsePreviousBlockData().

                AddBlock<GetGroupWithOffendersTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgDeleteZoneRequestBlock>().UsePreviousBlockData().
                SetProperty(x => x.ZoneID).
                    WithValueFromBlockIndex("AddCircularZoneTestBlock").
                        FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).

                AddBlock<DeleteZoneTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                         WithValueFromBlockIndex("GetGroupWithOffendersTestBlock").
                            FromDeepProperty<GetGroupWithOffendersTestBlock>(x => x.GroupsWithOffendersLst[0].ID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<DeleteZoneAssertBlock>().

            ExecuteFlow();
        }
        #endregion

        #region Update circular zone
        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_FullSchedule_1Piece()
        {
            Zones_UpdateCircularZone_ForOffender_FullSchedule(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_FullSchedule_2Piece()
        {
            Zones_UpdateCircularZone_ForOffender_FullSchedule(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_FullSchedule_DV()
        {
            Zones_UpdateCircularZone_ForOffender_FullSchedule(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Aggressor).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_IsCerfewZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).


                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-3).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<UpdateCircularZoneAssertBlock>().

            ExecuteFlow();
        }

        private ITestingFlow Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Offenders0.EnmProgramConcept ProgramConcept, Zones0.EnmLimitationType LimitationType, int BufferRadius)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).
                    SetProperty(x => x.ProgramConcept).WithValue(ProgramConcept).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(LimitationType).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.RealRadius).WithRandomGenerateValue().
                    SetProperty(x => x.GraceTime).WithRandomGenerateValue().
                    SetProperty(x => x.Name).WithRandomGenerateValue().

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Inclusion_1Piece()
        {
            Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Exclusion_1Piece()
        {
            Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Exclusion, 10).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Inclusion_2Piece()
        {
            Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Exclusion_2Piece()
        {
            Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Exclusion, 10).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone_DV()
        {
            Zones_UpdateCircularZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Aggressor, Zones0.EnmLimitationType.Inclusion, 0).ExecuteFlow();
        }

        private ITestingFlow Zones_UpdateCircularZone_ForOffender_FullSchedule(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Offenders0.EnmProgramConcept ProgramConcept)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).
                    SetProperty(x => x.ProgramConcept).WithValue(ProgramConcept).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.FullSchedule).WithValue(true).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_FullSchedule_BLP_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    //SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Point).WithValue(Point_AddCircularZone).
                    //SetProperty(x => x.RealRadius).WithValue(50).
                    //SetProperty(x => x.BufferRadius).WithValue(10).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    //SetProperty(x => x.GraceTime).WithValue(1).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(true).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 }).

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullSchedule_BLP_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(false).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 }).

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block").

                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForOffender_NotFullScheduleToFullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.FullSchedule).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block").
                    
                ExecuteFlow();
        }

        private ITestingFlow Zones_UpdateCircularZone_ForGroup_NotFullSchedule_IsNotCerfewZone(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Offenders0.EnmProgramConcept ProgramConcept, Zones0.EnmLimitationType LimitationType, int BufferRadius)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).
                    SetProperty(x => x.ProgramConcept).WithValue(ProgramConcept).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateEntMsgAddGroupRequestBlock>().
                            SetProperty(x => x.GroupName).WithValue("New group11").
                            SetProperty(x => x.Description).WithValue("Group description").
                            SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                            SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                            SetProperty(x => x.OffendersListIDs[0]).
                                WithValueFromBlockIndex("AddOffenderTestBlock").
                                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                            SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    //SetProperty(x => x.BufferRadius).WithValue(BufferRadius).
                    SetProperty(x => x.Limitation).WithValue(LimitationType).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.RealRadius).WithRandomGenerateValue().
                    SetProperty(x => x.GraceTime).WithRandomGenerateValue().
                    SetProperty(x => x.Name).WithRandomGenerateValue().

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                                         WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForGroup_NotFullSchedule_IsNotCerfewZone_Inclusion_1Piece()
        {
            Zones_UpdateCircularZone_ForGroup_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion, 0).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdateCircularZone_ForGroup_NotFullSchedule_IsNotCerfewZone_Inclusion_2Piece()
        {
            Zones_UpdateCircularZone_ForGroup_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion, 0).ExecuteFlow();
        }
        #endregion

        #region Update polygon zone
        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_FullSchedule_1Piece()
        {
            Zones_UpdateCircularZone_ForOffender_FullSchedule(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_FullSchedule_2Piece()
        {
            Zones_UpdateCircularZone_ForOffender_FullSchedule(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_FullSchedule_DV()
        {
            Zones_UpdateCircularZone_ForOffender_FullSchedule(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Aggressor).ExecuteFlow();
        }

        private ITestingFlow Zones_UpdatePolygonZone_ForOffender_FullSchedule(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Offenders0.EnmProgramConcept ProgramConcept)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).
                    SetProperty(x => x.ProgramConcept).WithValue(ProgramConcept).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID). 

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdatePolygonZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_IsCerfewZone_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_2.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<UpdatePolygonZoneAssertBlock>().

            ExecuteFlow();
        }

        private ITestingFlow Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Offenders0.EnmProgramConcept ProgramConcept, Zones0.EnmLimitationType LimitationType)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).
                    SetProperty(x => x.ProgramConcept).WithValue(ProgramConcept).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(LimitationType).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdatePolygonZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Inclusion_1Piece()
        {
            Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Exclusion_1Piece()
        {
            Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Exclusion).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Inclusion_2Piece()
        {
            Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Exclusion_2Piece()
        {
            Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Exclusion).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone_Inclusion_DV()
        {
            Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Aggressor, Zones0.EnmLimitationType.Inclusion).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_FullSchedule_BLP_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    //SetProperty(x => x.Points).WithValue(Points_AddPolygonZone).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(true).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 }).

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdatePolygonZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block")
                    
                .ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullSchedule_BLP_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                SetName("AddEquipmentTestBlock_1Piece").
                   SetDescription("Add equipment test block").

                   AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddCellularDataRequest Type").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

                AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Cellular Data test block").

                   AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                    SetName("GetEquipmentListTestBlock_1Piece").

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("create random EntMsgAddEquipmentRequest type").
                    SetProperty(x => x.AgencyID).
                    WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.Beacon_Curfew_Unit).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.TwoG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_0).
                    SetProperty(x => x.EquipmentSerialNumber).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_Beacon_Curfew_Unit.ToString()).

                AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                    SetDescription("Add equipment test block").
                    SetName("AddEquipmentTestBlock_BTX").

                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                SetName("CreateEntMsgGetEquipmentListRequest_BTX").
                    SetProperty(x => x.EquipmentID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_BTX").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).
                    SetProperty(x => x.EquipmentSerialNumber).WithValue(null).

                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("GetEquipmentListTestBlock_BTX").
                    SetProperty(x => x.GetEquipmentListRequest).
                        WithValueFromBlockIndex("CreateEntMsgGetEquipmentListRequest_BTX").
                            FromDeepProperty<CreateEntMsgGetEquipmentListRequest>(x => x.GetEquipmentListRequest).

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_1Piece").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                    SetProperty(x => x.AgencyID).
                        WithValueFromBlockIndex("AddEquipmentTestBlock_1Piece").
                            FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentRequest.AgencyID).
                    SetProperty(x => x.HomeUnit).
                        WithValueFromBlockIndex("GetEquipmentListTestBlock_BTX").
                             FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).
                    SetProperty(x => x.IsCurfewZone).WithValue(true).
                    SetProperty(x => x.FullSchedule).WithValue(false).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 32.1063605, LON = 34.8369437 }).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.BLP).WithValue(new Zones0.EntPoint() { LAT = 31.107120, LON = 33.834436 }).

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdatePolygonZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block")

                .ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForOffender_NotFullScheduleToFullSchedule_1Piece()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.FullSchedule).WithValue(false).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdateInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddCircularZoneTestBlock").
                            FromDeepProperty<AddCircularZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.FullSchedule).WithValue(true).

                AddBlock<UpdateCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdateCircularZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block").
                    
                ExecuteFlow();
        }

        private ITestingFlow Zones_UpdatePolygonZone_ForGroup_NotFullSchedule_IsNotCerfewZone(ITestingFlow testingFlow, Offenders0.EnmProgramType ProgramType, Offenders0.EnmProgramConcept ProgramConcept, Zones0.EnmLimitationType LimitationType)
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(ProgramType).
                    SetProperty(x => x.ProgramConcept).WithValue(ProgramConcept).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateEntMsgAddGroupRequestBlock>().
                            SetProperty(x => x.GroupName).WithValue("New group1").
                            SetProperty(x => x.Description).WithValue("Group description").
                            SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                            SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                            SetProperty(x => x.OffendersListIDs[0]).
                                WithValueFromBlockIndex("AddOffenderTestBlock").
                                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddPolygonZoneRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Polygon Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                            SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Limitation).WithValue(LimitationType).

                AddBlock<AddPolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgUpdatePolygonZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Update Circular Zone Request").
                    SetProperty(x => x.ZoneID).
                        WithValueFromBlockIndex("AddPolygonZoneTestBlock").
                            FromDeepProperty<AddPolygonZoneTestBlock>(x => x.AddZoneResponse.ZoneID).
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).

                AddBlock<UpdatePolygonZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<UpdatePolygonZoneAssertBlock>().
                    SetDescription("Update Zone Request Assert Block");

            return testingFlow;
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForGroup_NotFullSchedule_IsNotCerfewZone_Inclusion_1Piece()
        {
            Zones_UpdatePolygonZone_ForGroup_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.One_Piece_RF, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion).ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_UpdatePolygonZone_ForGroup_NotFullSchedule_IsNotCerfewZone_Inclusion_2Piece()
        {
            Zones_UpdatePolygonZone_ForGroup_NotFullSchedule_IsNotCerfewZone(
                TestingFlow, Offenders0.EnmProgramType.Two_Piece, Offenders0.EnmProgramConcept.Regular, Zones0.EnmLimitationType.Inclusion).ExecuteFlow();
        }
        #endregion

        #region Get zone by entity ID
        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_GetZonesByEntityID_ForOffender_PlannedVersion()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<GetZoneByEntityIDAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_GetZonesByEntityID_ForGroup_PlannedVersion()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                AddBlock<CreateEntMsgAddGroupRequestBlock>().
                            SetProperty(x => x.GroupName).WithValue("New group1").
                            SetProperty(x => x.Description).WithValue("Group description").
                            SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                            SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                            SetProperty(x => x.OffendersListIDs[0]).
                                WithValueFromBlockIndex("AddOffenderTestBlock").
                                    FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                            SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-2).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<GetZoneByEntityIDAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_GetZonesByEntityID_ForOffender_CurrentVersion()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

               AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().UsePreviousBlockData().
                   SetDescription("Create Add Offender Request").
                   SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                   SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).

               AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Offender test block").

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                   AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

               AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").
 
                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Offender).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                AddBlock<GetZoneByEntityIDAssertBlock>().

            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.Zones_Regression)]
        [TestMethod]
        public void Zones_GetZonesByEntityID_ForGroup_CurrentVersion()
        {
            TestingFlow.

                AddBlock<CreateEntMsgAddEquipmentRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create EntMsgAddEquipmentRequest Type").
                    SetProperty(x => x.EquipmentEncryptionGSM).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EquipmentEncryptionRF).WithValue(EnmEncryptionType.NoEncryption).
                    SetProperty(x => x.EnmEquipmentModel).WithValue(EnmEquipmentModel.One_Piece_GPS_RF_G39).
                    SetProperty(x => x.ModemType).WithValue(EnmModemType.ThreeG).
                    SetProperty(x => x.ProtocolType).WithValue(EnmProtocolType.Protocol_5).
                    SetProperty(x => x.EquipmentSerialNumber).WithValueFromDifferentDataGenerator(EnumPropertyType.EquipmentSerialNumber_OnePiece_Generation_3_9.ToString()).

               AddBlock<AddEquipmentTestBlock>().UsePreviousBlockData().
                   SetDescription("Add equipment test block").

               AddBlock<CreateEntMsgAddCellularDataRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                   SetDescription("Create EntMsgAddCellularDataRequest Type").
                   SetProperty(x => x.EquipmentID).
                       WithValueFromBlockIndex("AddEquipmentTestBlock").
                           FromDeepProperty<AddEquipmentTestBlock>(x => x.AddEquipmentResponse.NewEquipmentID).

               AddBlock<AddCellularDataTestBlock>().UsePreviousBlockData().
                   SetDescription("Add Cellular Data test block").

                AddBlock<CreateEntMsgAddOffenderRequestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Offender Request").
                    SetProperty(x => x.ProgramType).WithValue(Offenders0.EnmProgramType.One_Piece_RF).
                    SetProperty(x => x.Receiver).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.EquipmentSerialNumber).
                           SetProperty(x => x.AgencyID).
                       WithValueFromBlockIndex("CreateEntMsgAddEquipmentRequestBlock").
                           FromPropertyName(EnumPropertyName.AgencyID).

                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Offender test block").

                    AddBlock<CreateEntMsgGetOffendersListRequestBlock>().
                    SetDescription("Create request to get offenders list").
                    SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<GetOffendersTestBlock>().UsePreviousBlockData().
                    SetDescription("Request to get offenders list").

                AddBlock<CreateEntMsgSendDownloadRequestBlock>().UsePreviousBlockData().
                   SetDescription("Create Send Download Request").
                   SetProperty(x => x.Immediate).WithValue(true).
                   SetProperty(x => x.OffenderID).
                       WithValueFromBlockIndex("AddOffenderTestBlock").
                           FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

               AddBlock<SendDownloadRequestTestBlock>().UsePreviousBlockData().
                   SetDescription("Send Download Request Test Block").

                AddBlock<RunXTSimulatorTestBlock>().UsePreviousBlockData().
                    SetDescription("Run XT Simulator Test Block").
                    SetProperty(x => x.EquipmentSN).
                        WithValueFromBlockIndex("GetOffendersTestBlock").
                            FromDeepProperty<GetOffendersTestBlock>(x => x.GetOffendersResponse.OffendersList[0].EquipmentInfo.ReceiverSN).

                AddBlock<CreateEntMsgAddGroupRequestBlock>().
                    SetProperty(x => x.GroupName).WithValue("New group1").
                    SetProperty(x => x.Description).WithValue("Group description").
                    SetProperty(x => x.ProgramGroup).WithValue(EnmProgramGroup.MTD).
                    SetProperty(x => x.OffendersListIDs).WithValue(new int[1]).
                    SetProperty(x => x.OffendersListIDs[0]).
                        WithValueFromBlockIndex("AddOffenderTestBlock").
                            FromDeepProperty<AddOffenderTestBlock>(x => x.AddOffenderResponse.NewOffenderID).

                AddBlock<AddGroupTestBlock>().UsePreviousBlockData().

                AddBlock<CreateMsgAddInclusionCircularZoneRequestBlock>().UsePreviousBlockData().ShallGeneratePropertiesWithRandomValues().
                    SetDescription("Create Add Circular Zone Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.RealRadius).WithValue(50).
                    SetProperty(x => x.Limitation).WithValue(Zones0.EnmLimitationType.Inclusion).

                AddBlock<AddCircularZoneTestBlock>().UsePreviousBlockData().
                    SetDescription("Add Circular Zone test block").

                

               

                AddBlock<CreateMsgGetZonesByEntityIDRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Get Zones By Entity ID Request").
                    SetProperty(x => x.EntityID).
                        WithValueFromBlockIndex("AddGroupTestBlock").
                            FromDeepProperty<AddGroupTestBlock>(x => x.GroupID).
                    SetProperty(x => x.EntityType).WithValue(Zones0.EnmEntityType.Group).
                    SetProperty(x => x.Version).WithValue(-1).

                AddBlock<GetZonesByEntityIDTestBlock>().UsePreviousBlockData().
                    SetDescription("Get Zones By Entity ID Test Block").

                    AddBlock<GetZoneByEntityIDAssertBlock>().

            ExecuteFlow();
        }
        #endregion
    }
}
 