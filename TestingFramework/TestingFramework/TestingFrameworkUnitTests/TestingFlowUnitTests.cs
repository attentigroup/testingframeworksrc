﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Factories;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestingFramework.TestsInfraStructure.Implementation;
using TestBlocksLib.TestingFrameworkBlocks;
using Common.Enum;
using TestingFramework.TestsInfraStructure.Exceptions;
using System.Collections.Generic;
using DataGenerators;
using System.Linq;
using AssertBlocksLib.TestingFrameworkAsserts;
using Common.Categories;
using AssertBlocksLib.OffendersAsserts;
using AssertBlocksLib.MaskingPrivateData;

namespace TestingFramework.UnitTests
{
    /// <summary>
    /// implementing unit tests for the testing framework
    /// </summary>
    [TestClass]
    public class TestingFlowUnitTests : BaseUnitTest
    {
        /// <summary>
        /// test that verify all blocks run only once
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestingFlowExecuteAllBlocksOnce()
        {
            TestingFlow.
                AddBlock<ExecutionCounterTestBlock>().
            ExecuteFlow();

            //assert that the counter as excpected
            Assert.AreEqual(1, ExecutionCounterTestBlock.Counter, "ExecutionCounterTestBlock counter not as expected");
        }
        /// <summary>
        /// test that verify that the property was set correct
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void SetPropertyTest()
        {
            string propertySetValue = "Property set";

            TestingFlow.
                AddBlock<SetPropertyTestBlock>().
                    SetDescription("Block used to check set property method").
                    SetProperty(x => x.PropertyValue).WithValue(propertySetValue).
            ExecuteFlow();
            //verify the value in the block changed
            string propertyValueAfter = SetPropertyTestBlock.GetPropertyValue();

            Assert.AreEqual(propertySetValue, propertyValueAfter);
        }

        /// <summary>
        /// Test check exception thrown when execute block with 
        /// mandatory field that wasn't set with value
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void MandatoryFieldTest()
        {
            bool gotExceptionAsExpected = false;
            try
            {
                TestingFlow.
                    AddBlock<SetPropertyTestBlock>().
                        SetDescription("Block used to check exception thrown when execute " +
                        "block with mandatory field that wasn't set with value").
                ExecuteFlow();
            }
            catch (MandatoryFieldNotSetException exception)
            {
                if( exception.PropertyNames.Count != 1 )
                {
                    var msg = string.Format("MandatoryFieldTest faild : expected to have one mandatory field in exception and got {0}.",
                        exception.PropertyNames.Count);
                    throw new AssertFailedException(msg, exception);
                }
                else if( exception.PropertyNames[0] != EnumPropertyName.PropertyValue )
                {
                    var msg = string.Format("MandatoryFieldTest faild : expected to have mandatory field from name EnumPropertyName.PropertyValue in exception and got {0}.",
                        exception.PropertyNames[0]);
                    throw new AssertFailedException(msg, exception);
                }
                gotExceptionAsExpected = true;
            }
            catch( Exception exception)
            {
                var msg = string.Format("MandatoryFieldTest supposed to get MandatoryFieldNotSetException and didn't get.insted it get {0} exception.", 
                    exception.GetType());
                throw new AssertFailedException(msg, exception);
            }

            if(gotExceptionAsExpected == false)
            {
                throw new AssertFailedException("MandatoryFieldTest supposed to get MandatoryFieldNotSetException and didn't get.");
            }
        }

        /// <summary>
        /// test that verify that an output property can't be set outside the block
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void OutputFieldTest()
        {
            bool exceptionWasThrowen = false;
            try
            {
                TestingFlow.
                        AddBlock<SetPropertyTestBlock>().
                            SetDescription("Block used to check exception thrown when execute block with output field that was set with value").
                                SetProperty(x => x.PropertyValue).WithRandomGenerateValue().
                                SetProperty(x => x.OutputPropertyValue).WithValue("Error while try to set output property").
                        ExecuteFlow();
            }
            catch (OutputFieldSetException outputEx)
            {
                Assert.AreEqual("SetPropertyTestBlock", outputEx.Block.BlockName, false, "Block name not the same");
                Assert.AreEqual(EnumPropertyType.String, outputEx.PropertyType,
                    string.Format("Propoerty type not as expected({0}). actual ({1}).", EnumPropertyType.String, outputEx.PropertyType));

                exceptionWasThrowen = true;
            }
            catch(Exception ex)
            {
                throw new AssertFailedException(
                    string.Format("OutputFieldTest supposed to get OutputFieldSetException and didn't get.actual exception: {0}", ex.Message), 
                    ex);
            }
            if( exceptionWasThrowen == false)
            {
                throw new AssertFailedException("OutputFieldTest supposed to get OutputFieldSetException and didn't exception");
            }
        }

        /// <summary>
        /// test that verify the clenaup method for blocks called in the correct order
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void CleanupTestBlockCalledTest()
        {
            string cleanupTestBlockName = "cleanup block name:";
            string cleanupTestBlockDescription = "use this block to check cleanup feature";
            int numberOfBlocks = 3;

            var stringGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.String);

            //save the time before start the execution of the flow to verify the order of the execution based on time
            var datetimeExecute = DateTime.Now;

            var lstBlockNames = new List<string>();
            for (int i = 0; i < numberOfBlocks; i++)
            {//generate random block name
                var randomBlockName = stringGenerator.GenerateData(string.Empty) as string;
                lstBlockNames.Add(randomBlockName);
                //add the block to the flow
                TestingFlow.
                    AddBlock<CleanupTestBlock>().
                        SetName(cleanupTestBlockName + randomBlockName).
                        SetDescription(cleanupTestBlockDescription);

            }
            //execute the flow
            TestingFlow.
                ExecuteFlow();
            //calling the cleanup manualy - it should called from method with [TestCleanup] attribute.
            TestingFlow.Cleanup();

            //save the time after the test execution to verify cleanup execution order by time is correct
            var datetimeCleanup = DateTime.Now.AddSeconds(1);

            //running on all blocks by index and verify that the time of execution correct
            for (int i = 0; i < numberOfBlocks; i++)
            {
                var currentBlockName = lstBlockNames[i];
                var block = TestingFlow.Blocks[i] as CleanupTestBlock;
                //verify the block is the block we need
                if (!block.CleanupResult.Contains(currentBlockName))
                {
                    throw new AssertFailedException(
                        string.Format("block index {0} name {1} desc {2} not contained expected block name {3}",
                            i, block.BlockName, block.BlockDescription, currentBlockName));
                }
                //verify that the cleanup was called
                if (!block.CleanupResult.Contains(cleanupTestBlockDescription) )
                {
                    throw new AssertFailedException(
                        string.Format("block index {0} name {1} desc {2} not contained expected block description {3}",
                            i, block.BlockName, block.BlockDescription, cleanupTestBlockDescription));
                }
                //verify execution time correct - start time not early then all-blocks-execution start time
                if( block.BlockStartTime.Ticks < datetimeExecute.Ticks)
                {//time incorrect - block start time early then expected
                    throw new AssertFailedException(
                        string.Format("block index {0} name {1} desc {2} start time({3}) early then expected ({4})",
                            i, block.BlockName, block.BlockDescription, block.BlockStartTime.Ticks, datetimeExecute.Ticks));
                }
                //verify execution time correct - start time not early then all-blocks-execution start time
                if (block.BlockStartTime.Ticks >= datetimeCleanup.Ticks)
                {//time incorrect - block start time later then cleanup end time
                    throw new AssertFailedException(
                        string.Format("block index {0} name {1} desc {2} start time({3}) later then cleanup end time ({4})",
                            i, block.BlockName, block.BlockDescription, block.BlockStartTime.Ticks, datetimeExecute.Ticks));
                }
                datetimeExecute = block.BlockStartTime;//update the time stamp for the next execution

                //verify cleanup block correct
                if( block.CleanupDateTime.Ticks > datetimeCleanup.Ticks)
                {//time incorrect - block Cleanup DateTime later then expected
                    throw new AssertFailedException(
                        string.Format("block index {0} name {1} desc {2} Cleanup DateTime ({3}) later then expected ({4})",
                            i, block.BlockName, block.BlockDescription, block.CleanupDateTime.Ticks, datetimeCleanup.Ticks));
                }
                datetimeCleanup = block.CleanupDateTime;//update the cleanuptime stamp for the next cleanup
            }
        }

        /// <summary>
        /// test that verify Set Property For Collection based on Index
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void SetPropertyForCollectionItemIndex()
        {
            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var index2Set = intGenerator.GenerateData(24, 42);
            var value2Set = DataGeneratorFactory.Instance.GenerateData(EnumPropertyType.String, string.Empty) as string;
            int listLength = index2Set + 1;
            var stringsList = new List<string>(Enumerable.Repeat(value2Set, listLength));

            TestingFlow.AddBlock<SetPropertyCollectionTestBlock>().ShallGeneratePropertiesWithRandomValues().
                SetProperty(x => x.Index2Set).WithValue(index2Set).
                SetProperty(x => x.Value2Set).WithValue(value2Set).
                SetProperty(x => x.StringsArray).WithValue(new string[index2Set+1]).
                SetProperty(x => x.StringsArray[index2Set]).WithValue(value2Set).
                SetProperty(x => x.ListLength).WithValue(listLength).
                SetProperty(x => x.StringsList).WithValue(stringsList).
            ExecuteFlow();

            var stringsArray = (TestingFlow.Blocks[0] as SetPropertyCollectionTestBlock).StringsArray;

            Assert.IsTrue(stringsArray.Length > 0, "string array length expected to be more then 0(actual {0})", stringsArray.Length);
            Assert.IsTrue(stringsArray.Length > index2Set, "string array length expected to be at least {0} (actual {0})", index2Set, stringsArray.Length);
            Assert.AreEqual(value2Set, stringsArray[index2Set], "string at position {0} ({1}) not as expected({2}", index2Set, stringsArray[index2Set], value2Set);
        }

        /// <summary>
        /// test that verify that assert block can't add to testing flow as first block
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestingFramework_AddAssertFirstBlock_Fail()
        {
            bool exceptionThrown = false;
            try
            {
                TestingFlow.
                        AddBlock<AssertEmptyAssert>().
                    ExecuteFlow();
            }
            catch (AssertFirstBlockException)
            {
                exceptionThrown = true;
            }
            catch(Exception exception)
            {
                throw new AssertFailedException(
                    string.Format("add assert as first block and expect to exception from type AssertFirstBlockException and got {0}.", 
                        exception.GetType()), 
                    exception);
            }
            if( exceptionThrown == false)
            {
                throw new AssertFailedException("add assert as first block and expect exception from type AssertFirstBlockException and no exception was thrown.");
            }
        }

        /// <summary>
        /// test that verify that exception that throwen in block passed to the following block for assertion
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestingFramework_ExceptionHandling()
        {
            TestingFlow.
                AddBlock<AlwaysFailTestBlock>().
                AddBlock<BaseExceptionAssertBlock>(TestingFrameworkAssertsFactory.AssertNotImplementedException()).
            ExecuteFlow();
        }

        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void UsePreviousBlockDataForFirstBlock_Failed()
        {
            bool usePreviousForFirstBlockExceptionThrowen = false;
            try
            {
                TestingFlow.
                        AddBlock<SetPropertyTestBlock>().
                            ShallGeneratePropertiesWithRandomValues().
                            UsePreviousBlockData().
                    ExecuteFlow();
            }
            catch (UsePreviousForFirstBlockException)
            {
                usePreviousForFirstBlockExceptionThrowen = true;
            }
            catch (Exception exception)
            {
                var msg = string.Format("UsePreviousForFirstBlockException supposed to get MandatoryFieldNotSetException and didn't get.insted it get {0} exception.",
                    exception.GetType());
                throw new AssertFailedException(msg, exception);
            }

            if (usePreviousForFirstBlockExceptionThrowen == false)
            {
                var msg = "UsePreviousForFirstBlockException supposed to get MandatoryFieldNotSetException and didn't get.";
                throw new AssertFailedException(msg);
            }
        }

        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void BlockNameNotExistException_WithValueFromBlockIndexFromDeepProperty_Failed()
        {
            bool blockNameNotExistExceptionThrowen = false;
            try
            {
                TestingFlow.
                        AddBlock<SetPropertyTestBlock>().ShallGeneratePropertiesWithRandomValues().
                            SetName("Existing Block Name").
                        AddBlock<SetPropertyTestBlock>().
                            SetProperty(x=>x.PropertyValue).
                                WithValueFromBlockIndex("Not Existing Block Name").
                                    FromDeepProperty<SetPropertyTestBlock>(x=>x.PropertyValue).
                    ExecuteFlow();
            }
            catch (BlockNameNotExistException)
            {
                blockNameNotExistExceptionThrowen = true;
            }
            catch (Exception exception)
            {
                var msg = $"BlockNameNotExistException supposed to thrown and didn't.insted it get {exception.GetType()} exception.";
                throw new AssertFailedException(msg, exception);
            }

            if (blockNameNotExistExceptionThrowen == false)
            {
                var msg = "BlockNameNotExistException supposed to thrown and didn't.";
                throw new AssertFailedException(msg);
            }
        }

        [Ignore]
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void SetDeeperProperty()
        {
            int listLength = 42;
            var stringsList = new List<string>(Enumerable.Repeat(listLength.ToString(), listLength));
            TestingFlow.
                AddBlock<DeepPropertyTestingBlock>().
                    SetProperty(x => x.DeepProperty.DeeperProperty).WithValue(42).
                    SetProperty(x => x.ListLength).WithValue(listLength).
                    SetProperty(x => x.StringsList).WithValue(stringsList).                    
                ExecuteFlow();
        }

        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void DeepPropertyTest()
        {
            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var index2Set = intGenerator.GenerateData(24, 42);
            var value2Set = DataGeneratorFactory.Instance.GenerateData(EnumPropertyType.String, string.Empty) as string;
            int listLength = index2Set + 1;
            var stringsList = new List<string>(Enumerable.Repeat(value2Set, listLength));

            TestingFlow.
                AddBlock<SetPropertyCollectionTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.Index2Set).WithValue(index2Set).
                    SetProperty(x => x.Value2Set).WithValue(value2Set).
                    SetProperty(x => x.StringsArray).WithValue(new string[index2Set + 1]).
                    SetProperty(x => x.StringsArray[index2Set]).WithValue(value2Set).
                    SetProperty(x => x.ListLength).WithValue(listLength).
                    SetProperty(x => x.StringsList).WithValue(stringsList).
                AddBlock<DeepPropertyTestingBlock>().UsePreviousBlockData().
                    SetProperty(x=>x.StringsList).WithValueFromBlockIndex("SetPropertyCollectionTestBlock").WithTheSamePropertyName().
                    SetProperty(x=>x.ListLength).WithValueFromBlockIndex("SetPropertyCollectionTestBlock").
                        FromDeepProperty<SetPropertyCollectionTestBlock>(x=>x.StringsList.Count).
                    SetProperty(x => x.DeepProperty).WithValue(new DeepPropertyClass()).
            ExecuteFlow();

            var stringsArray = (TestingFlow.Blocks[0] as SetPropertyCollectionTestBlock).StringsArray;

            Assert.IsTrue(stringsArray.Length > 0, "string array length expected to be more then 0(actual {0})", stringsArray.Length);
            Assert.IsTrue(stringsArray.Length > index2Set, "string array length expected to be at least {0} (actual {0})", index2Set, stringsArray.Length);
            Assert.AreEqual(value2Set, stringsArray[index2Set], "string at position {0} ({1}) not as expected({2}", index2Set, stringsArray[index2Set], value2Set);

            var deepPropertyBlockListLength = (TestingFlow.Blocks[1] as DeepPropertyTestingBlock).ListLength;
            Assert.AreEqual(listLength, deepPropertyBlockListLength);
        }

        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void FromPropertyNameTest()
        {
            var intGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.Int) as IntGenerator;
            var index2Set = intGenerator.GenerateData(24, 42);
            var value2Set = DataGeneratorFactory.Instance.GenerateData(EnumPropertyType.String, string.Empty) as string;
            int listLength = index2Set + 1;
            var stringsList = new List<string>(Enumerable.Repeat(value2Set, listLength));

            TestingFlow.
                AddBlock<SetPropertyCollectionTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.Index2Set).WithValue(index2Set).
                    SetProperty(x => x.Value2Set).WithValue(value2Set).
                    SetProperty(x => x.StringsArray).WithValue(new string[index2Set + 1]).
                    SetProperty(x => x.StringsArray[index2Set]).WithValue(value2Set).
                    SetProperty(x => x.ListLength).WithValue(listLength).
                    SetProperty(x => x.StringsList).WithValue(stringsList).
                AddBlock<DeepPropertyTestingBlock>().UsePreviousBlockData().
                    SetProperty(x => x.StringsList).WithValueFromBlockIndex("SetPropertyCollectionTestBlock").WithTheSamePropertyName().
                    SetProperty(x => x.ListLength).WithValueFromBlockIndex("SetPropertyCollectionTestBlock").FromPropertyName(EnumPropertyName.Index2Set).
                    SetProperty(x=>x.DeepProperty).WithValue(new DeepPropertyClass()).
            ExecuteFlow();

            var fromPropertyNameBlockListLength = (TestingFlow.Blocks[1] as DeepPropertyTestingBlock).ListLength;
            Assert.AreEqual(index2Set, fromPropertyNameBlockListLength);
        }

        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void MappingAllProxiesObjects()
        {
            var assemblyName = "TestingFramework.Proxies";
            var nameSpace = "TestingFramework.Proxies";

            var dicType2properties = new Dictionary<string, List<string>>();
            
            AppDomain.CurrentDomain.Load(assemblyName);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var assmebly = assemblies.First(x => x.FullName.StartsWith(assemblyName));
            var classesTypes = assmebly.GetTypes().Where(type => type.IsClass && type.Namespace != null && type.Namespace.Contains(nameSpace));
            foreach (var classType in classesTypes)
            {
                var CompilerGenAttr = classType.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true);
                if (CompilerGenAttr.Length > 0)
                {
                    Log.DebugFormat("classType {0} will not load because because this is not real class", classType.ToString());
                    continue;
                }

                if( dicType2properties.ContainsKey(classType.Name) == false)
                {
                    dicType2properties.Add(classType.Name, new List<string>());
                }
                List<string> propertiesList = dicType2properties[classType.Name];

                var properties = classType.GetProperties();
                foreach (var prop in properties)
                {
                    propertiesList.Add(prop.Name);
                }
            }
            string myDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            var filePath = System.IO.Path.Combine(myDocumentsPath, $"{DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_")}_AllProxiesObjects.csv");

            var sbData = new System.Text.StringBuilder();
            foreach (var kvp in dicType2properties)
            {
                var classTypeName = kvp.Key;
                var properties = kvp.Value;

                foreach (var prop in properties)
                {
                    sbData.AppendLine($"{classTypeName},{prop}");
                }
            }
            System.IO.File.WriteAllText(filePath, sbData.ToString());
        }
    }
}
