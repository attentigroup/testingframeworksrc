﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Factories;
using System.Collections.Generic;
using Common.Enum;
using System.Linq;
using Common.Categories;
using TestingFramework.TestsInfraStructure.Interfaces;
using TestBlocksLib.TestingFrameworkBlocks;
using TestingFramework.TestsInfraStructure.Implementation;
using TestingFramework.TestsInfraStructure.Exceptions;
using DataGenerators;

namespace TestingFramework.UnitTests
{
    /// <summary>
    /// test for data generators
    /// </summary>
    [TestClass]
    public class DataGeneratorsTests : BaseUnitTest
    {
        /// <summary>
        /// this test will generate data with all data generators
        /// in case one of the data generators will fail
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestSuccefullGetDataGenerator()
        {
            var dataGenerators = DataGeneratorFactory.Instance.DataGenerators;
            List<EnumPropertyType>  properties = dataGenerators.Keys.ToList();

            GetGeneratorForPropertyTypeList(properties);
        }

        //read all property types and verify a data generator exist for the property
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestAllPropertiesTypeGeneratorsExist()
        {
            var names = (EnumPropertyType[])Enum.GetValues(typeof(EnumPropertyType));
            var _propertyNamesForTesting = new List<EnumPropertyType>(names);

            //remove None...
            _propertyNamesForTesting.Remove(EnumPropertyType.None);
            GetGeneratorForPropertyTypeList(_propertyNamesForTesting);
        }

        private static void GetGeneratorForPropertyTypeList(List<EnumPropertyType> PropertyTypes)
        {
            List<Exception> exceptions = new List<Exception>();

            foreach (var property in PropertyTypes)
            {
                try
                {
                    var dataGenerator = DataGeneratorFactory.Instance.GetDataGenerator(property);
                    if (dataGenerator == null)
                    {
                        var msg = string.Format("Got null DataGenerator for property type:{0}", property);
                        throw new Exception(msg);
                    }
                }
                catch (KeyNotFoundException knfe)
                {
                    exceptions.Add(new Exception(string.Format("KeyNotFoundException not found for {0}", property), knfe));
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
            if (exceptions.Count > 0)
            {
                string joinedMessages = string.Join("\n", exceptions.Select(x => x.Message));
                var msg = string.Format("GetGeneratorForPropertyTypeList: failed:\n{0}", joinedMessages);
                var aggregateException = new AggregateException(msg, exceptions);
                throw new AssertFailedException(msg, aggregateException);
            }
        }

        /// <summary>
        /// test the feature of replacing data generator from the test level
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestUseDifferentDataGenerator()
        {
            TestingFlow.
                AddBlock<SetPropertyTestBlock>().ShallGeneratePropertiesWithRandomValues().
                    SetProperty(x => x.PropertyValue).
                        WithValueFromDifferentDataGenerator(EnumPropertyType.ControlledDataGenerator.ToString()).
            ExecuteFlow();

            string propertyValueAfter = SetPropertyTestBlock.GetPropertyValue();
            Assert.AreEqual(ControlledDataGenerator.LastValueGenerated, propertyValueAfter);

        }

        /// <summary>
        /// test the feature of replacing data generator from the test level to nonexist data generator
        /// </summary>
        [TestCategory(CategoriesNames.TestingFrameworkUntiTests)]
        [TestMethod]
        public void TestUseDifferentDataGenerator_NonExistDataGenerator_Fail()
        {
            bool dataGeneratorNotExistExceptionThrowen = false;
            try
            {
                TestingFlow.
                        AddBlock<SetPropertyTestBlock>().ShallGeneratePropertiesWithRandomValues().
                            SetProperty(x=> x.PropertyValue).WithValueFromDifferentDataGenerator("NOT EXIST_DATA GENERATOR").

                    ExecuteFlow();
            }
            catch (DataGeneratorNotExistException)
            {
                dataGeneratorNotExistExceptionThrowen = true;
            }
            catch (Exception exception)
            {
                var msg = $"TestUseDifferentDataGenerator_NonExistDataGenerator_Fail supposed to get DataGeneratorNotExistException and didn't get.insted it get {exception.GetType()} exception.";
                throw new AssertFailedException(msg, exception);
            }

            if (dataGeneratorNotExistExceptionThrowen == false)
            {
                var msg = "TestUseDifferentDataGenerator_NonExistDataGenerator_Fail supposed to get DataGeneratorNotExistException and didn't get.";
                throw new AssertFailedException(msg);
            }
        }
    }
}
