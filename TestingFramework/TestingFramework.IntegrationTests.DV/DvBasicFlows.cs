﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestingFramework.TestsInfraStructure.Implementation;
using LogicBlocksLib.OffendersBlocks;
using TestBlocksLib.OffendersBlocks;
using LogicBlocksLib.EquipmentBlocks;
using TestBlocksLib.EquipmentBlocks;
using TestingFramework.TestsInfraStructure.Factories;
using Common.Enum;
using DataGenerators;

using Offenders12_0 = TestingFramework.Proxies.EM.Interfaces.Offenders12_0;
using Equipment12_0 = TestingFramework.Proxies.EM.Interfaces.Equipment;
using Common.Categories;

namespace TestingFramework.IntegrationTests.DV
{
    [TestClass]
    public class DvBasicFlows : BaseUnitTest
    {
        [Ignore]
        [TestCategory(CategoriesNames.DvBasicFlow)]
        [TestMethod]
        public void AddDvPair()
        {
            var stringDataGenerator = DataGeneratorFactory.Instance.GetDataGenerator(EnumPropertyType.String) as StringGenerator;
            var namePostFix = stringDataGenerator.GenerateRandomStringWithLength(8);
            TestingFlow.
                //get agency and officer
                AddBlock<GetOffendersIdsByEventsTestBlock>().
                    SetDescription("Get agency and offender using the events list").
                    SetProperty(x => x.Limit).WithValue(1).
                //victim
                AddBlock<CreateEntMsgGetEquipmentListRequest>().
                    SetDescription("create msg for get equipment for 2P rcv").
                    SetProperty(x => x.AgencyID).WithValueFromBlockIndex("GetOffendersIdsByEventsTestBlock").
                        FromDeepProperty<GetOffendersIdsByEventsTestBlock>(x => x.AgenciesIdList[0]).
                    SetProperty(x => x.EquipmentModels).WithValue(new Equipment12_0.EnmEquipmentModel[] { Equipment12_0.EnmEquipmentModel.Two_Piece_GPS }).
                    SetProperty(x => x.EquipmentType).WithValue(Equipment12_0.EnmEquipmentType.Receiver).
                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("Get_rcv_2p_for_victim").
                    SetDescription("get equipment for 2P rcv for victim").
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request for victim").                    
                    SetProperty(x => x.OfficerID).WithValueFromBlockIndex("GetOffendersIdsByEventsTestBlock").
                        FromDeepProperty<GetOffendersIdsByEventsTestBlock>(x => x.OfficerIdsList[0]).
                    SetProperty(x => x.CityID).WithValue("TLV").
                    SetProperty(x => x.StateID).WithValue("IL").
                    SetProperty(x => x.Street).WithValue("Ha'barzel").
                    SetProperty(x => x.Language).WithValue("ENG").
                    SetProperty(x => x.FirstName).WithValue(string.Format("Vic_{0}", namePostFix)).
                    SetProperty(x => x.LastName).WithValue(string.Format("Vic_{0}", namePostFix)).
                    SetProperty(x => x.RefID).WithValue(string.Format("Vic_{0}", namePostFix)).
                    SetProperty(x => x.ProgramType).WithValue(Offenders12_0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders12_0.EnmProgramConcept.Victim).
                    SetProperty(x => x.Gender).WithValue(Offenders12_0.EnmGender.Female).
                    SetProperty(x => x.Title).WithValue(Offenders12_0.EnmTitle.Ms).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.Receiver).WithValueFromBlockIndex("Get_rcv_2p_for_victim").
                            FromDeepProperty<GetEquipmentListTestBlock>(x => x.GetEquipmentListResponse.EquipmentList[0].SerialNumber).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetName("Add_Offender_Request_for_victim").
                    SetDescription("Add Offender Request for victim").
                //aggressor
                AddBlock<CreateEntMsgGetEquipmentListRequest>().UsePreviousBlockData().
                    SetDescription("create msg for get equipment for 2P Transmitter").
                    SetProperty(x => x.EquipmentModels).WithValue(new Equipment12_0.EnmEquipmentModel[] { Equipment12_0.EnmEquipmentModel.Two_Piece_GPS }).
                    SetProperty(x => x.EquipmentType).WithValue(Equipment12_0.EnmEquipmentType.Transmitter).
                AddBlock<GetEquipmentListTestBlock>().UsePreviousBlockData().
                SetName("Get_Transmitter_2p_for_Aggressor").
                    SetDescription("get equipment for 2P rcv for Aggressor").
                AddBlock<CreateEntMsgAddOffenderRequestBlock>().UsePreviousBlockData().
                    SetDescription("Create Add Offender Request for Aggressor").
                    SetProperty(x => x.FirstName).WithValue(string.Format("Agg_{0}", namePostFix)).
                    SetProperty(x => x.LastName).WithValue(string.Format("Agg_{0}", namePostFix)).
                    SetProperty(x => x.RefID).WithValue(string.Format("Agg_{0}", namePostFix)).
                    SetProperty(x => x.ProgramType).WithValue(Offenders12_0.EnmProgramType.Two_Piece).
                    SetProperty(x => x.ProgramConcept).WithValue(Offenders12_0.EnmProgramConcept.Aggressor).
                    SetProperty(x => x.Gender).WithValue(Offenders12_0.EnmGender.Male).
                    SetProperty(x => x.Title).WithValue(Offenders12_0.EnmTitle.Mr).
                    SetProperty(x => x.ProgramStart).WithValue(DateTime.Now).
                    SetProperty(x => x.ProgramEnd).WithValue(DateTime.Now.AddYears(1)).
                    SetProperty(x => x.RelatedOffenderID).WithValueFromBlockIndex("Get_Transmitter_2p_for_Aggressor").
                        FromDeepProperty<AddOffenderTestBlock>(x=>x.AddOffenderResponse.NewOffenderID).
                AddBlock<AddOffenderTestBlock>().UsePreviousBlockData().ShallDoCleanUp(false).
                    SetDescription("Add Offender Request for Aggressor").
                ExecuteFlow();
        }
    }
}
